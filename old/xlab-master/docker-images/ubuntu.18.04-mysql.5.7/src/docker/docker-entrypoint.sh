#!/bin/bash

# cleanup quotation marks from .env variables
export MYSQL_DATABASE="$(sed 's/"//g' <<< $MYSQL_DATABASE)"
export MYSQL_USER="$(sed 's/"//g' <<< $MYSQL_USER)"
export MYSQL_PASSWORD="$(sed 's/"//g' <<< $MYSQL_PASSWORD)"

function exec_mysql_script {
    printf "%s\n" \
    "s/{{[[:space:]]*MYSQL_DATABASE[[:space:]]*}}/$MYSQL_DATABASE/g"   \
    "s/{{[[:space:]]*MYSQL_USER[[:space:]]*}}/$MYSQL_USER/g"   \
    "s/{{[[:space:]]*MYSQL_PASSWORD[[:space:]]*}}/$MYSQL_PASSWORD/g" \
    > mysql_vars.sed;

    sed -f mysql_vars.sed /usr/local/mysql-scripts/$1 | mysql -u root;
} 

chown -R mysql:root /var/lib/mysql/;
mysqld --initialize-insecure -u root;
service mysql restart;

exec_mysql_script "create_database.sql";
exec_mysql_script "create_user_local_access.sql";
exec_mysql_script "create_user_remote_access.sql";

echo "MySQL exposed on port $MYSQL_PORT"