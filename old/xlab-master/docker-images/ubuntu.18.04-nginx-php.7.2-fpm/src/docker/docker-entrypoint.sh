#!/bin/bash
service php7.2-fpm restart;
service nginx restart;
cron;
echo "Nginx exposed on port $NGINX_PORT"
echo "PHP-FPM socket at $(ls /run/php/php7.2-fpm.sock)"
