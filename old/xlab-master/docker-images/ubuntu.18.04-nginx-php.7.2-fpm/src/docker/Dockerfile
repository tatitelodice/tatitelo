FROM ubuntu:18.04

### SECTION SYSTEM-PRESETS
ENV DEBIAN_FRONTEND noninteractive
RUN echo 'debconf debconf/frontend select Noninteractive' | debconf-set-selections

RUN apt-get update && apt-get install -y \
    cron \
    curl \
    bash \
    nano \
    dirmngr \
    && rm -rf /var/lib/apt/lists/*
### ENDSECTION SYSTEM-PRESETS

### SECTION NGINX-SETUP
RUN echo "deb http://ppa.launchpad.net/nginx/stable/ubuntu artful main" >> /etc/apt/sources.list.d/nginx.list
RUN echo "deb-src http://ppa.launchpad.net/nginx/stable/ubuntu artful main" >> /etc/apt/sources.list.d/nginx.list

RUN apt-key adv --keyserver keyserver.ubuntu.com --recv-keys 8B3981E7A6852F782CC4951600A6F0A3C300EE8C

RUN apt-get update && apt-get install -y \
    nginx-extras \
    && rm -rf /var/lib/apt/lists/*
### ENDSECTION NGINX-SETUP

### SECTION PHP-SETUP
RUN apt-get update && apt-get install -y \
    php7.2-cli \
    php7.2-curl \
    php7.2-dev \
    php7.2-json \
    php7.2-fpm \
    php7.2-gd \
    php7.2-intl \
    php7.2-mbstring \
    php7.2-mysql \
    php7.2-readline \
    php7.2-sqlite3 \
    php7.2-xml \
    php7.2-zip \
    php-imagick \
    php-pear \
    php-redis \
    && rm -rf /var/lib/apt/lists/*

RUN curl -sS https://getcomposer.org/installer | \
    php -- --install-dir=/usr/local/bin --filename=composer
### ENDSECTION PHP-SETUP

### SECTION SYSTEM-CLEANUP
RUN apt-get clean && apt-get autoclean
### ENDSECTION SYSTEM-CLEANUP

### SECTION NGINX-CONFIG
RUN rm /etc/nginx/sites-enabled/default
COPY nginx/full.mime.types /etc/nginx/full.mime.types
COPY nginx/default.vhost.conf /etc/nginx/sites-enabled/default
### ENDSECTION NGINX-CONFIG

### SECTION PHP-FPM-CONFIG
# Fix unable to bind listening socket for address '/run/php/php7.2-fpm.sock
RUN mkdir -p /run/php/
### ENDSECTION PHP-FPM-CONFIG

### SECTION CRON-CONFIG
# Copy crontabs in /var/www/localhost/cron/crontabs to /etc/cron.d/ every 1 minute
COPY cron/crontab /etc/crontab
RUN chmod 0644 /etc/crontab && chown root:root /etc/crontab
### ENDSECTION CRON-CONFIG

### SECTION ENTRYPOINT
COPY docker/docker-entrypoint.sh /usr/bin/
RUN chmod +x /usr/bin/docker-entrypoint.sh

CMD /usr/bin/docker-entrypoint.sh && bash
### ENDSECTION ENTRYPOINT

EXPOSE 80
