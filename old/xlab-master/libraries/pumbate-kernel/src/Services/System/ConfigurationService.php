<?php

namespace Melquilabs\Pumbate\Services\System;

class ConfigurationService
{
    protected $parameters;

    public function __construct($settingsModel)
	{
        $this->settingsModel = $settingsModel;
        $this->parameters = [];
        $this->loadSavedSettings();
    }

    protected function loadSavedSettings()
    {
        $settingsModel = $this->settingsModel->getAll();

        foreach ($settingsModel as $item)
        {
            $this->parameters[$item->keyname] = $item->value;
        }
    }

    public function setParameter($keyname, $value)
    {
        $this->parameters[$keyname] = $value;
    }

    public function hasParameter($keyname)
    {
        return isset($this->parameters[$keyname]);
    }

    public function getParameter($keyname, $defaultValue=null)
    {
        if ($this->hasParameter($keyname))
        {
            return $this->parameter($keyname);
        }

        return $defaultValue;
    }

    /* Compatibility shim with CodeIgniter Configuration */
    public function item($keyname)
    {
        return $this->getParameter($keyname);
    }

    public function getParameterOrFail($keyname)
    {
        if (!$this->hasParameter($keyname))
        {
            throw new \OutOfBoundsException('Parameter not found: ' . $keyname);
        }

        return $this->parameter($keyname);
    }

    protected function parameter($keyname)
    {
        return $this->parameters[$keyname];
    }
}