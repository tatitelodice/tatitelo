<?php 

namespace Melquilabs\Pumbate\Services\Geolocation;

use Melquilabs\Pumbate\Models\LocationModel;

class LocationListService 
{
    protected $locationModel;
    protected $redis;

    public function __construct(LocationModel $locationModel, $redis)
    {
        $this->locationModel = $locationModel;
        $this->redis = $redis;
    }


    public function getRegions($countryIso)
    {
        $data = json_decode($this->redis->get('locations:' . $countryIso), true);
        return array_keys($data);
    }


    public function getDefaultRegion($countryIso)
    {
        return $this->locationModel->getDefaultRegion();
    }


    public function getCities($countryIso, $regionName)
    {
        $data = json_decode($this->redis->get('locations:' . $countryIso), true);
        
        if (!isset($data[$regionName]))
        {
            return null;
        }

        return $data[$regionName];
    }

    
    public function getDefaultCity($countryIso, $regionName)
    {
        return $this->locationModel->getDefaultCity();
    }
}