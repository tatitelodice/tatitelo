<?php 

namespace Melquilabs\Pumbate\Services\Geolocation;

use Melquilabs\Pumbate\Models\LocationModel;

class LocationCacheService 
{
    protected $redis;
    protected $locationModel;


    public function __construct(LocationModel $locationModel, $redis)
    {
        $this->redis = $redis;
        $this->locationModel = $locationModel;
    }

    
    public function updateLocationsCache()
    {
        $countries = $this->locationModel->getCountries();

        foreach ($countries as $countryIsoCode => $countryName)
        {
            $countryLocationsTree = $this->generateCountryLocationsTree($countryIsoCode);

            $locationsTreeJson = json_encode($countryLocationsTree);

            $this->redis->set('locations:' . $countryIsoCode, $locationsTreeJson);
        }
    }

    protected function generateCountryLocationsTree($countryIsoCode)
    {
        $countryLocationsTree = [];

        $regions = $this->locationModel->getRegions($countryIsoCode);

        foreach ($regions as $reg)
        {
            $regionName = $reg['regName'];

            $countryLocationsTree[$regionName] = [];

            $cities = $this->locationModel->getCities($countryIsoCode, $regionName);

            foreach ($cities as $city)
            {
                $countryLocationsTree[$regionName][] = $city['citName'];
            }
        }

        return $countryLocationsTree;
    }
}