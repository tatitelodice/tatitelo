<?php

namespace Melquilabs\Pumbate\Services;


class AdsBuilder {

	protected $base_path;
	
	public function __construct() {
		
		if (preg_match('!win!i', php_uname())) {
			$this->base_path = realpath(preg_replace('![^\\\]+\.php!i', '', __FILE__)) . '\\';
			$this->base_path = preg_replace('!(application[\\\]).*!', '', $this->base_path);
		} else {
			$this->base_path = preg_replace('![^/]+\.php!i', '', __FILE__);
			$this->base_path = preg_replace('!(application/).*!', '', $this->base_path);
		}
	}


	public function random_mail() {
		return substr( md5(time()), 0, rand(5,12) ) . '@gmail.com';
	}

	public function entitize($str) {
		$res = '';
		$len = strlen($str);

		for ($i=0; $i < $len; $i++) {
			$res .= '&#' . ord($str[$i]) . ';';
		}

		return $res;

	}

	public function mix_title($terms) {
		$mixedTerms = array();

		foreach ($terms as $term) {
			if (rand(0,20) % 2 == 0) {
				$mixedTerms[] = $term;
			} else {
				array_unshift($mixedTerms, $term);
			}
		}

		return implode(' ', $mixedTerms);
	}
	
	public function delete_phone($text) {
		return preg_replace('![0-9\s][0-9\s][0-9\s][0-9\s][0-9\s][0-9\s]+!', '', $text);
	}
	
	public function watermark_gallery($user) {
		
		$pics = array();
		
		$watermark_dir = 'userdata/' . $user . '/watermark';
		$optimized_dir = 'userdata/' . $user . '/optimized';
		
		@mkdir($watermark_dir);
		@chmod($watermark_dir, 0777);
		
		if (is_dir($watermark_dir) && is_dir($optimized_dir)) {
			
			foreach (scandir($optimized_dir) as $f) {
				if (preg_match('!\.(jpe?g|png|gif)!i', $f)) {
					
					$dst = $watermark_dir . '/' . $f;
					$src = $optimized_dir . '/' . $f;
					
					if (!file_exists($dst)) {
						$this->watermark_image($src, 'www.pumbate.com/'.$user, $dst);
					} 
					
					$pics[] = base_url($dst);
					
				}
			}
			
			foreach (scandir($watermark_dir) as $f) { 
				$opt = $optimized_dir . '/' . $f;
				
				if (!file_exists($opt)) {
					@unlink($watermark_dir . '/' . $f);
				}
			}
	
		}
		
		return $pics;
		
	}
	
	public function watermark_image ($SourceFile, $WaterMarkText, $DestinationFile) {
		list($width, $height) = getimagesize($SourceFile);
		$image_p = imagecreatetruecolor($width, $height);

		$ext = '';

		if (preg_match('!\.gif$!i', $SourceFile)) {
			$image = imagecreatefromgif($SourceFile);
			$ext = 'gif';
		} elseif (preg_match('!\.png$!i', $SourceFile)) {
			$image = imagecreatefrompng($SourceFile);
			$ext = 'png';
		} else {
			$image = imagecreatefromjpeg($SourceFile);
			$ext = 'jpg';
		}

		imagecopyresampled($image_p, $image, 0, 0, 0, 0, $width, $height, $width, $height);

		$colors = array(
			'black' => imagecolorallocate($image_p, 0, 0, 0),
			'white' => imagecolorallocate($image_p, 255, 255, 255),
			'rose' => imagecolorallocate($image_p, 247, 38, 141),
		);
		
		$sep = DIRECTORY_SEPARATOR;
		$font = $this->base_path . $sep . 'fonts' . $sep . 'quicksand-bold-webfont.ttf';

		$font_size = intval($width * 0.9 / strlen($WaterMarkText));
		$font_w = intval($font_size * 0.86);

		$padding = $font_size * 1.2;

		$pos_x = intval($width / 2 - strlen($WaterMarkText) * $font_w / 2);
		//$pos_y = intval($height / 2);
		$pos_y = intval($height - $font_size - $padding * 2);

		imagefilledrectangle($image_p, $pos_x - $padding, $pos_y - $padding - $font_size / 2, $pos_x + strlen($WaterMarkText) * $font_w + $padding, $pos_y + $padding, $colors['rose']);

		imagettftext($image_p, $font_size, 0, $pos_x, $pos_y, $colors['white'], $font, $WaterMarkText);

		if ($DestinationFile == '') {
			 header('Content-Type: image/jpeg');
			$DestinationFile = null;
		}

		if ($ext == 'gif') {
			imagegif($image_p, $DestinationFile);
		} elseif ($ext == 'png') {
			imagepng($image_p, $DestinationFile);
		} else {
			imagejpeg ($image_p, $DestinationFile, 95);
		}

		imagedestroy($image);
		imagedestroy($image_p);
	}
	
	public function output_optimized_image($path, $limit_width = false, $limit_height = false, $quality = false) {
		
		if (file_exists($path)) {
			
			if (!$quality) {
				$quality = 65;
			}
			
			if (!$limit_width) {
				$limit_width = 500;
			}
			
			if (!$limit_height) {
				$limit_height = 700;
			}
			
			list($width, $height) = getimagesize($path);
			
			if ($width > $limit_width) {
				$new_width = $limit_width;
				$new_height = intval($limit_width * $height / $width);
			
			} elseif ($height > $limit_height) {
				$new_height = $limit_height;
				$new_width = intval($limit_height * $width / $height);
				
			} else {
				$new_width = $width;
				$new_height = $height;
			}
			
			$image_p = imagecreatetruecolor($new_width, $new_height);
			
			if (preg_match('!\.gif$!i', $path)) {
				$image = imagecreatefromgif($path);
			} elseif (preg_match('!\.png$!i', $path)) {
				$image = imagecreatefrompng($path);
			} else {
				$image = imagecreatefromjpeg($path);
			}
			
			imagecopyresampled($image_p, $image, 0, 0, 0, 0, $new_width, $new_height, $width, $height );
			
			header('Content-Type: image/jpeg');
			imagejpeg ($image_p, null, $quality);
			
			imagedestroy($image_p);
			
		} else {
			echo $path . ' not found';
		}
	}
	
}

/* end of file */
