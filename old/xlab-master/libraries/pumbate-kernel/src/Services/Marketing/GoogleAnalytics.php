<?php

namespace Melquilabs\Pumbate\Services\Marketing;

use Google_Client;
use Google_Service_Analytics;

class GoogleAnalytics {

	protected $client;
	protected $analytics;
			
	protected $applicationName;
	protected $profileId;
	protected $authConfigPath;
	
	
	public function __construct($authConfigPath, $profileId, $applicationName)
	{
		$this->profileId = $profileId;
		$this->authConfigPath = $authConfigPath;
		$this->applicationName = $applicationName;
	}

	public function startClient() 
	{
		// create client object and set app name
		$this->client = new Google_Client();
		$this->client->setApplicationName($this->applicationName); // name of your app

		// set assertion credentials
		$this->client->setAuthConfig($this->authConfigPath);

		// Magic. Returns objects from the Analytics Service instead of associative arrays.
		//$this->client->setUseObjects(true);

		$this->client->setScopes(['https://www.googleapis.com/auth/analytics.readonly']);

		$this->analytics = new Google_Service_Analytics($this->client);			
	}
	
	
	public function getPageViewsForPath($path = '/', $startDate = false, $endDate = false) {

		/*  
		 * $optParams = array(
			'dimensions' => 'ga:source,ga:keyword',
			'sort' => '-ga:visits,ga:source',
			'filters' => 'ga:medium==organic',
			'max-results' => '25');
		*/
		$operator = '=';
		
		if (stripos($path, '~') === 0) {
			$operator = '';
		}

		$opts = array(
			'filters' => 'ga:pagePath=' . $operator . $path,
			'dimensions' => 'ga:date',
		);

	   $res = $this->analytics->data_ga->get(
		   'ga:' . $this->profileId,
		   $startDate,
		   $endDate,
		   //'ga:uniquePageviews',
			'ga:pageviews,ga:uniquePageviews',
			$opts
		);
		
		//print_r($res); die;
		
		return $res;
	}
	
	public function getFirstProfileId() 
	{
		$analytics = &$this->analytics;
		$accounts = $analytics->management_accounts->listManagementAccounts();

		//print_r($accounts); die;

		if (count($accounts->getItems()) > 0) {
			$items = $accounts->getItems();
			$firstAccountId = $items[0]->getId();

			$webproperties = $analytics->management_webproperties
					->listManagementWebproperties($firstAccountId);

			if (count($webproperties->getItems()) > 0) {
				$items = $webproperties->getItems();
				$firstWebpropertyId = $items[0]->getId();

				$profiles = $analytics->management_profiles
						->listManagementProfiles($firstAccountId, $firstWebpropertyId);

				if (count($profiles->getItems()) > 0) {
					$items = $profiles->getItems();
					return $items[0]->getId();
				} else {
					throw new Exception('No profiles found for this user.');
				}
			} else {
				throw new Exception('No webproperties found for this user.');
			}
		} else {
			throw new Exception('No accounts found for this user.');
		}
	}

}

/* end of file */
