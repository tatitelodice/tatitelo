<?php

namespace Melquilabs\Pumbate\Services\Marketing;

use Carbon\Carbon;
use DusanKasan\Knapsack\Collection;

use Melquilabs\Pumbate\Models\AnalyticsModel;
use Melquilabs\Pumbate\Services\Marketing\GoogleAnalytics;

class AnalyticsService
{
    protected $analyticsModel;
    protected $googleAnalytics;
    protected $refreshPeriodInMinutes = 15;

    public function __construct(AnalyticsModel $analyticsModel, GoogleAnalytics $googleAnalytics)
    {
        $this->analyticsModel = $analyticsModel;
        $this->googleAnalytics = $googleAnalytics;
    }

    public function getPageViewsByDate($user, $startDate, $endDate)
    {
        $self = $this;

        $pageViewsTable = $this->initPageViewsTable($startDate, $endDate);

		$storedPageViewsByDate = $this->analyticsModel->getUserAnalytics(
            $user, $startDate, $endDate
        );

        $storedPageViewsByDate = Collection::from($storedPageViewsByDate)->map(function($row) use ($self)
        {
            $item = (object)[];
            $item->date = new Carbon($row['date'], 'UTC');
            $item->pageViews = $row['pageViews'];
            $item->uniquePageViews = $row['uniquePageViews'];
            $item->lastUpdate = Carbon::parse($row['modifiedAt'])->setTimezone('UTC');
            $item->resultType = 'cached';

            if ($self->hasItemPartialResults($item))
            {
                $item->resultType = 'partial';
            }
            
            return $item;
        });

        $pageViewsTable = Collection::From($pageViewsTable)->replaceByKeys($storedPageViewsByDate);

        $emptyResultsTable = Collection::from($pageViewsTable)->filter(function($item)
        {
            return $item->resultType === 'empty';
        });

        $partialResultsTable = Collection::from($pageViewsTable)->filter(function($item)
        {
            return $item->resultType === 'partial';
        });

        if ($emptyResultsTable->isNotEmpty() || $partialResultsTable->isNotEmpty())
        {
            $realtimePageViews = $this->queryGoogleAnalyticsPageViews($user, $startDate, $endDate);
            $realtimePageViews = Collection::from($realtimePageViews);

            $pageViewsTable = $pageViewsTable->replaceByKeys($realtimePageViews);
            
            $this->storeGoogleAnalyticsPageViews(
                'create', $user, $realtimePageViews->only($emptyResultsTable->keys())
            );

            $this->storeGoogleAnalyticsPageViews(
                'update', $user, $realtimePageViews->only($partialResultsTable->keys())
            );
        }

        return $pageViewsTable;
    }

    protected function hasItemPartialResults($pageViewsTableItem)
    {
        $item = $pageViewsTableItem;
        $todayExpirationLimit = Carbon::now('UTC')->subMinutes($this->refreshPeriodInMinutes);

        if (!$item->date->isSameDay($item->lastUpdate))
        {
            return false;
        }

        $isFromToday = $item->date->isToday();
        $hasRecentUpdate = $item->lastUpdate->gte($todayExpirationLimit);

        return ($isFromToday && $hasRecentUpdate) === false;
    }

    public function initPageViewsTable($startDate, $endDate)
    {
        $data = [];

        $firstDt = Carbon::parse($startDate)->setTimezone('UTC');
        $lastDt = Carbon::parse($endDate)->setTimezone('UTC');
        $days = $firstDt->diff($lastDt)->days;

        $currentDt = $firstDt->copy();

        for ($i=0; $i <= $days; $i++)
        {
            $dateString = $currentDt->toDateString();

            $data[$dateString] = (object)[
                'date' => new Carbon($dateString, 'UTC')
            ,   'pageViews' => null
            ,   'uniquePageViews' => null
            ,   'resultType' => 'empty'
            ,   'lastUpdate' => null
            ];

            $currentDt->addDay();
        }

        return $data;
    }


    public function storeGoogleAnalyticsPageViews($operation, $user, $pageViewsTable)
    {
        $analyticsModel = $this->analyticsModel;        

        $pageViewsTable = Collection::from($pageViewsTable);

        $storeDataList = $pageViewsTable
            ->filter(function ($item) 
            { 
                return $item->resultType === 'new'; 
            })
            ->map(function ($item) use ($user)
            {
                return [
                    'user' => $user 
                ,   'date' => $item->date->toDateString()
                ,   'uniquePageViews' => $item->uniquePageViews
                ,   'pageViews' => $item->pageViews
                ];
            })
        ;

        if ($operation === 'create' && $storeDataList->isNotEmpty())
        {
            $analyticsModel->createMultiple($storeDataList);
        }
        else
        {
            foreach ($storeDataList as $data)
            {
                $analyticsModel->update($data['user'], $data['date'], $data);
            }
        }
    }

    public function getTotalPageViews($user, $start_date=false, $end_date=false)
    {
        return $this->analyticsModel->get_historical_visits_count($user);
    }
    
    public function queryGoogleAnalyticsPageViews($user, $start_date, $end_date) {
		$data = array();
		
        $this->googleAnalytics->startClient();
        
		$res = $this->googleAnalytics->getPageViewsForPath(
            '~^.*?/' . $user . '/?$'
        ,   $start_date
        ,   $end_date
        );
		
		//print_r($res); 
		
		if ($res && isset($res->rows)) {
			foreach ($res->rows as $row) {
				
				if (count($row) == 3 && preg_match('!(2\d\d\d)(\d\d)(\d\d)!', $row[0], $mdate)) {
					$formated_date = $mdate[1] . '-' . $mdate[2] . '-' . $mdate[3];
					
					$data[$formated_date] = (object)[
                        'date' => new Carbon($formated_date, 'UTC')
					,   'pageViews' => $row[1]
                    ,   'uniquePageViews' => $row[2]
                    ,   'resultType' => 'new'
                    ,   'lastUpdate' => Carbon::now()->setTimezone('UTC')
                    ];
				}
			}
        }
		
		return $data;
	}
}