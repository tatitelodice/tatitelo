<?php

namespace Melquilabs\Pumbate\Services\Admin;

use Melquilabs\Pumbate\Models\AdminModel;
use Melquilabs\Pumbate\Models\AdministratorAclModel;

class AdminPermissionService 
{
    
    protected $aclModel;
    protected $adminModel;
    
    public function __construct(AdminModel $adminModel, AdministratorAclModel $aclModel)
    {
        $this->adminModel = $adminModel;
        $this->aclModel = $aclModel;
    }

    public function isGranted($resource, $resourceId=null, $permission='full') 
    {
        $conditions = $this->buildQueryConditions($resource, $resourceId, $permission);
        
        $rules = $this->aclModel->findBy($conditions); 
        
        foreach ($rules as $rule)
        {
            if ($rule['permission'] === $permission)
            {
                return true;
            }
        }

        return false;
    }


    public function getPermissions($resource=false, $resourceId=null)
    {
        $conditions = $this->buildQueryConditions($resource, $resourceId, false);
        $rules = $this->aclModel->findBy($conditions);
        return $rules;
    }


    protected function buildQueryConditions($resource, $resourceId, $permission=false)
    {
        $adminId = $this->adminModel->get_id();

        $conditions = [
            'adminId' => $adminId
        ];

        if ($resource)
        {
            $conditions['resource'] = $resource;
        }

        if ($resource && $resourceId !== null)
        {
            $conditions['resourceId'] = $resourceId;
        }

        if ($permission)
        {
            $conditions['permission'] = $permission;
        }

        return $conditions;
    }

}

/* eof */