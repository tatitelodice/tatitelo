<?php

namespace Melquilabs\Pumbate\Services\Admin;

use Melquilabs\Pumbate\Models\PaymentAccountModel;
use Melquilabs\Pumbate\Services\Admin\AdminPermissionService;

class AdminPaymentAccountService 
{
    protected $paymentAccountModel;
    protected $permissionService;
    
    public function __construct(PaymentAccountModel $paymentAccountModel, AdminPermissionService $permissionService) 
    {
        $this->paymentAccountModel = $paymentAccountModel;
        $this->permissionService = $permissionService;
    }

    public function getAllowedPaymentAccounts($options=false)
    {
        $searchOptions = (is_array($options)) ? $options : [];

        $hasFullPermissions =$this->permissionService->isGranted('payment_account', null, 'full');

        if ($hasFullPermissions)
        {
            return $this->paymentAccountModel->findBy($searchOptions);
        }
        
        $accountPermissions = $this->permissionService->getPermissions('payment_account');

        $accountIds = [];

        foreach ($accountPermissions as $permission)
        {
            if ( empty($permission['resourceId']) ) continue;
            if (!in_array($permission['permission'], ['view', 'full'])) continue ;

            $accountIds[] = $permission['resourceId'];
        }

        if ( empty($accountIds) )
        {
            return [];
        }

        $searchOptions['id'] = $accountIds;
        
        $accounts = $this->paymentAccountModel->findBy($searchOptions);

        return $accounts;
    }

}

/* eof */