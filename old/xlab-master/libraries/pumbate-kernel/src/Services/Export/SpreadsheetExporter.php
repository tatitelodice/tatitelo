<?php

namespace Melquilabs\Pumbate\Services\Export;

use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;
use PhpOffice\PhpSpreadsheet\Cell\DataType as CellDataType;

class SpreadsheetExporter 
{

	public $cell_types = [
		'string'  => CellDataType::TYPE_STRING
	,	'formula' => CellDataType::TYPE_FORMULA
	,	'numeric' => CellDataType::TYPE_NUMERIC
	,	'bool'    => CellDataType::TYPE_BOOL
	,	'null'    => CellDataType::TYPE_NULL
	,	'inline'  => CellDataType::TYPE_INLINE
	,	'error'   => CellDataType::TYPE_ERROR
	];

	public function generate_and_flush_excel_file($doc_data) {
		
		$PHPExcel = new Spreadsheet();
		
		if (!empty($doc_data['creator']))
		{
			$PHPExcel->getProperties()->setCreator($doc_data['creator']);
		}

		$PHPExcel->getProperties()->setTitle($doc_data['title']);

		foreach ($doc_data['sheets'] as $sheet_index => $sheet_data)
		{
			$PHPExcel->setActiveSheetIndex($sheet_index);
			$PHPExcel->getActiveSheet()->setTitle($sheet_data['title']);

			$rows = $sheet_data['cells'];

			foreach ($rows as $row_index => $row)
			{
				$row_index = $row_index + 1;

				foreach ($row as $column_index => $cell_data)
				{
					$cell_val = $cell_data;

					if (is_array($cell_val) && isset($cell_val['cellDataType']))
					{
						$cell_val = $cell_val['value'];
						$cell_type = $this->cell_types[$cell_data['cellDataType']];

						$PHPExcel->getActiveSheet()->setCellValueExplicitByColumnAndRow(
							$column_index, $row_index, $cell_val, $cell_type
						);
					}
					else
					{
						$PHPExcel->getActiveSheet()->setCellValueByColumnAndRow(
							$column_index, $row_index, $cell_val
						);
					}
				}
			}
		
			for ($i = 0; $i < count($rows[0]); $i++) {
				$PHPExcel->getActiveSheet()->getColumnDimensionByColumn($i)->setAutoSize(true);
			}
		}
		
		$PHPExcelWriter = new Xlsx($PHPExcel);
		$ext = 'xlsx';
		$mime = 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet';

		$basename = $doc_data['filename'] . '.' . $ext;
		
		$warns = ob_get_contents();

		if ($warns)
		{
			echo $warns; die();
		}

		//$filename = sys_get_temp_dir() . '/' . $basename;
		//$PHPExcelWriter->save($filename);
		header('Content-type: ' . $mime);
		header('Content-Disposition: attachment; filename="' . $basename . '"');
		$PHPExcelWriter->save('php://output');
	}

}

/* eof */