<?php

namespace Melquilabs\Pumbate\Services\Billing;

class DepositNotificationService {

    protected $logModel;
    protected $profileModel;
    protected $customerMessageModel;
    protected $depositModel;
    protected $depositNotificationModel;


    public function __construct(
        $profileModel
    ,   $logModel
    ,   $customerMessageModel
    ,   $depositModel
    ,   $depositNotificationModel
    ) 
    {
        $this->profileModel = $profileModel;
        $this->logModel = $logModel;
        $this->customerMessageModel = $customerMessageModel;
        $this->depositModel = $depositModel;
        $this->depositNotificationModel = $depositNotificationModel;
    }


    public function get_submit_permissions($profile, $deposit)
    {
        $notificationsFromDate = false;

        $user = $profile['user'];

        $prevDeposit = $this->depositModel->get_profile_last_deposit($user);

        if ($prevDeposit)
        {
            $notificationsFromDate = preg_replace(
                '!(\d+)/(\d+)/(\d+)!', '$3-$2-$1', $prevDeposit['date_applied']
            );

            $m = new \Moment\Moment($notificationsFromDate); $m->addDays(4);
            $notificationsFromDate = $m->format('Y-m-d');
        }

        $notifications = $this->get_recent_notifications($profile, $notificationsFromDate);
        
        $submit = true;
        $activate = count($notifications) < 2;

        if (!$prevDeposit && count($notifications) >= 3)
        {
            $submit = false;
        }

        if ($prevDeposit && count($notifications) >= 5)
        {
            $submit = false;
        }

        return [ 'submit' => $submit, 'activate' => $activate ];
    }


    public function is_submit_duplicated($profile, $deposit)
    {
        $notifications = $this->get_recent_notifications($profile);

        if (count($notifications) > 0)
        {
            $lastNotification = $notifications[0];
            $lastNotificationDeposit = $lastNotification['deposit'];
            
            $sameDeposit = true;

            $attrs = ['photo', 'amount', 'date', 'name'];

            foreach ($attrs as $a)
            {
                $sameDeposit = $sameDeposit && ($deposit[$a] === $lastNotificationDeposit[$a]);
            }

            return $sameDeposit;
        }

        return false;
    }


    public function submit_deposit_notification($profile, $deposit, $activateProfile)
    {
        $attributes = $this->generate_attributes($profile, $deposit);

        $this->create_notification($profile, $deposit, $attributes);

        $this->create_inbox_message($profile, $deposit, $attributes);

        if ($activateProfile)
        {
            $this->activate_profile($profile, $deposit);
        }
    }


    protected function get_recent_notifications($profile, $fromDate=false)
    {
        $notifications = $this->depositNotificationModel->find_by([
            'user' => $profile['user']
        ,   'fromDate' => $fromDate
        ]);

        return $notifications;
    }


    protected function generate_attributes($profile, $deposit)
    {
        $attributes = [ 
            'deposit' => $deposit 
        ,   'profiles' => [
                [
                    'id' => $profile['id']
                ,   'user' => $profile['user']
                ,   'gender' => $profile['gender']
                ]
            ]
        ];

        return $attributes;
    }


    protected function create_notification($profile, $deposit, $attributes)
    {
        $user = $profile['user'];

        $this->logModel->insert_log(
            $profile['country'], 'deposit_notification', $user
        ,   true, false, $attributes
        );
    }


    protected function create_inbox_message($profile, $deposit, $attributes)
    {
        $message = 'depositado ' . $deposit['user'] 
        .   ' $' . $deposit['amount'] 
        .   ' a nombre de ' . $deposit['name'] 
        .   ' el dia ' . $deposit['date'] 
        .   ' a las ' . $deposit['time']
        .   '. ' . $deposit['comments']
        ;

        $message = trim($message);

        $this->customerMessageModel->create_inbox_message([
            'uuid' => \Ramsey\Uuid\Uuid::uuid4()
        ,   'message_text' => $message
        ,   'message_address' => $deposit['phone']
        ,   'system_address' => 'web'
        ,   'system_datetime' => date('c')
        ,   'attributes' => $attributes
        ]);
    }


    protected function activate_profile($profile, $deposit)
    {
        $user = $profile['user'];
        $this->profileModel->enable_profile($user);
        $this->profileModel->change_profile_panel_message($user, '');
    }
}