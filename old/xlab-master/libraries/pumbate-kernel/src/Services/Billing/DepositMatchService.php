<?php

namespace Melquilabs\Pumbate\Services\Billing;

class DepositMatchService 
{
    
    protected $depositModel;
    protected $paymentsModel;
    protected $profileModel;
    protected $logModel;
    
    public function __construct($depositModel, $paymentsModel, $profileModel, $logModel) 
    {
        $this->depositModel = $depositModel;
        $this->paymentsModel = $paymentsModel;
        $this->profileModel = $profileModel;
        $this->logModel = $logModel;
    }


    public function assign_deposits_to_profiles($deposits_date_from=false)
    {
        $depositModel = $this->depositModel;
        $logModel = $this->logModel;

        if (!$deposits_date_from)
        {
            $deposits_date_from = date('Y-m-d', strtotime('-15 day'));
        }

        $deposits = $depositModel->find_by([
            'proUser' => null
        ,   'depDate >=' => $deposits_date_from
        ], 200);

        $notifications = $logModel->find_by([
            'logDescription' => 'deposit_notification'
        ,   'logDate >=' => $deposits_date_from
        ], 200, 'logId desc');


        $matched_deposits = $this->resolve_deposit_users($deposits, $notifications);

        foreach ($matched_deposits as $deposit)
        {
            $this->apply_deposit_to_profile($deposit, $deposit['user']);
            $logModel->remove_notification($deposit['notification']['id']);
        }

        return $matched_deposits;
    }


    public function apply_deposit_to_profile($deposit, $user)
    {
        $depositModel = $this->depositModel;
        $paymentsModel = $this->paymentsModel;

        $concept = $depositModel->get_default_deposit_concept();

        $depositModel->apply_deposit(
                $deposit['id']
            ,   array(
                    'user' => $user
                ,   'concept' => $concept
                ,   'applied_at' => date('Y-m-d')
                )
            );
            
        $paymentsModel->amend_profile_payments_status(
            $user
        );

        $paymentsModel->update_profile_last_deposit(
            $user
        );
    }


    public function resolve_deposit_users($deposits, $notifications)
    {
        $deposits_by_data_key = [];
        $deposits_by_phone = [];

        foreach ($deposits as $dep)
        {
            $key = $this->generate_deposit_data_key($dep);
            $deposits_by_data_key[$key] = $dep;

            if (preg_match('!09\d{7}!', $key, $mp))
            {
                $deposits_by_phone[$mp[0]] = $dep;
            }
        }

        $resolved_deposits = [];

        foreach ($notifications as $notif)
        {
            $matched = false;

            $notification_deposit = $notif['meta']['deposit'];
            $key = $this->generate_deposit_data_key($notification_deposit);
            
            $same_user = $notification_deposit['user'] === $notif['user'];

            if (isset($deposits_by_data_key[$key]) && $same_user)
            {
                $deposit = $deposits_by_data_key[$key];
                $matched = true;
            }

            if (isset($deposits_by_phone[$notification_deposit['phone']]))
            {
                $deposit = $deposits_by_phone[$notification_deposit['phone']];
                $matched = true;
            }

            if ($matched)
            {
                $deposit['user'] = $notification_deposit['user'];
                $deposit['notification'] = $notif;
                $resolved_deposits[] = $deposit;
            }
        }

        return $resolved_deposits;
    }


    protected function generate_deposit_data_key($deposit)
    {
        $deposit_name = preg_replace('![^A-Za-z0-9]!', '', $deposit['name']);

        $deposit_date = preg_replace('!(\d{2})/(\d{2})/(\d{4})!', '$3-$2-$1', $deposit['date']);

        $deposit_date = date('Ymd', strtotime($deposit_date));

        /*
        $key = implode('--', [
            $deposit_name, $deposit_date, $deposit['amount']
        ]);
        */

        $key = $deposit_name;

        $key = strtolower($key);

        return $key;
    }
    
}

/* end of file */