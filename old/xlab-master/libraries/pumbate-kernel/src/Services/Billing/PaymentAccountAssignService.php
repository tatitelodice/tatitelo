<?php

namespace Melquilabs\Pumbate\Services\Billing;

class PaymentAccountAssignService 
{
    
    protected $db;
    protected $paymentAccountModel;
    
    public function __construct($db, $paymentAccountModel) 
    {
        $this->db = $db;
        $this->paymentAccountModel = $paymentAccountModel;
    }

    public function assignLastDepositAccountAsPaymentAccount()
    {
        $stm = 'update payments pay 
        inner join deposits dep on pay.lastDepositId = dep.depId 
        left outer join payment_account pa on dep.paymentAccountId = pa.id and pa.isActive=1
        set pay.paymentAccountId = pa.id;';

        $this->db->query($stm);
    }

}

/* eof */