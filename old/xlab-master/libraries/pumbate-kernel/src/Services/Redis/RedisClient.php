<?php

namespace Melquilabs\Pumbate\Services\Redis;

use Predis\Client as PredisClient;

class RedisClient
{

    private $client;

    public function __construct() {
        $this->client = null;
    }

    public function get_client() {
        if ($this->client === null)
        {
            $client = new PredisClient([
                'scheme' => 'tcp',
                'host'   => '0.0.0.0',
                'port'   => 6379,
            ]);
            
            $this->client = $client;
        }

        return $client;
    }

}


/* eof */