<?php

namespace Melquilabs\Pumbate\Services\Contact;

class ContactEmail 
{
	
	protected $agents_mails = array(
		'default' => array(
			'sgpence1990@gmail.com' => 'Santiago Pence',
		),
		'uy' => array(
			'sgpence1990@gmail.com' => 'Santiago Pence',
			'nicoquint@gmail.com' => 'Nicolás Quintana',
		),
		'co' => array(
			'colombia@pumbate.com' => "Pumbate Colombia",
		), 	
	);
	
	protected $contact_mails = array(
		'default' => 'rrpp@pumbate.com',
		'co' => 'colombia@pumbate.com',
	);
	
	public function get_agents_by_country($country) {
		if (isset($this->agents_mails[$country])) {
			return $this->agents_mails[$country];
		} else {
			return $this->agents_mails['default'];
		}
	}
	
	public function get_contact_by_country($country) {
		if (isset($this->contact_mails[$country])) {
			return $this->contact_mails[$country];
		} else {
			return $this->contact_mails['default'];
		}
	}
	
}

