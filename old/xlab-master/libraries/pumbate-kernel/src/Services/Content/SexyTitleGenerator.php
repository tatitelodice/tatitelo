<?php

namespace Melquilabs\Pumbate\Services\Content;

class SexyTitleGenerator {
		
	public function create_title($parts_limit = 3, $gender=false, $country=false) {
		$words_threesome = array(
			"puro morbo", "te esperamos", "combo explosivo", "tu sueño hecho realidad",
			"buscas un trio", "sale trio", "como soñaste", "para compartir", "lo que pedias"
		);
		
		$words_generic = array(
			"escort", "acompañante", "amante ideal", "masajista", 'sexy',  'puro placer',
			'ardiente', 'servicio completo', 'puro disfrute', 'pura lujuria', "infartante",
			"caliente", "puro morbo", "como lo que pedias"
		);
				
		$words = $words_generic;
						
		if ($gender) {
			if ($gender == "mujer") {
				$words_woman = array(
					"chica", "dama", "diablita", "nena", "bebota", "mimosa",
					'viciosa', 'pecadora', 'atrevida', 'completita', 'sexy', 'adicta al sexo'
				);
				
				$words = array_merge($words_generic, $words_woman);
				
			} elseif ($gender == "hombre") {
				$words_men = array(
					"chico", "taxiboy", "macho", "dotado",
					'vicioso', 'pecador', 'atrevido', 'completo', 
					'bien masculino', 'adicto al sexo', "escort masculino"
				);
						
				$words = array_merge($words_generic, $words_men);
				
			} elseif ($gender == "trans") {
				$words_trans = array(
					"traviesa", "dotada", "escort trans", "chica trans", "chica dotada",
					"trans morbosa", "traviesa morbosa"
				);				
				
				$words = array_merge($words_generic, $words_trans);
				
			} elseif ($gender == "duomm") {
				$words_threesome_female = array(
					"amigas intimas", "show lesbico", "tus nenas atrevidas",
					"juntas dinamita", "atrevidas", "divertidas"
				);
				
				$words = array_merge($words_threesome, $words_threesome_female);
				
			} elseif (preg_match("!^duo!", $gender)) {
				$words_threesome_male = array(
					"amigos intimos", "tus amigos atrevidos",
					"juntos dinamita"
				);
				
				$words = array_merge($words_threesome, $words_threesome_male);
			}
		}
		
		if ($parts_limit > $words) {
			$parts_limit = count($words);
		}
		
		if ($parts_limit < 2) {
			$parts_limit = 2;
		}
		
		$selected_keys = array_rand($words, $parts_limit);
		$selected_words = array();
		
		foreach ($selected_keys as $k) {
			$selected_words[] = $words[$k];
		}
		
		$title = implode(" ", $selected_words);
		
		$title = explode(" ", $title);
		$title = implode(' ', array_unique($title));
		
		if ($country && $country == 'co') {
			$title = str_replace("escort", "prepago", $title);
		}
		
		$title = preg_replace("!escort (chica|chico|dama|pareja)!", "$1 escort", $title);
		
		$title = mb_strtoupper(mb_substr($title, 0, 1)).mb_strtolower(mb_substr($title, 1));
		
		return $title;
	}
	
	public function create_phone($phone, $gender = false) {
		
		$words_generic = array(
			"Espero tu llamado",
			"Mi número es",
			"No te arrepentirás",
			"No lo dudes",
			"Te complaceré",
			"Cualquier consulta",
			"Ni lo pienses!! llámame",
			"Una experiencia única",
			"¿Te animas?",
			"¿Ya decidiste en pasarla genial?",
			"No dudes en llamar!!!! te espero!!!!!!!!!"
		);
		
		$words_threesome = array(
			"Esperamos tu llamado",
			"Nuestro número es",
			"No te arrepentirás",
			"No lo dudes",
			"Cualquier consulta",
			"Ni lo pienses!! llámanos",
			"Una experiencia única",
			"¿Ya decidiste en pasarla genial?",
			"No dudes en llamar!!! te Esperamos!!!",
		);
		
		$words = $words_generic;
		
		if ($gender) {					
			if (preg_match("!^duo!", $gender)) {
				$words = $words_threesome;
			}		
		}
		
		$title = $words[rand(0, count($words) - 1)];
		
		if (!stripos($title, "?") && !stripos($title, "!")) {
			$title .= "...";
		}
		
		$title .= " ". $phone;
		
		return $title;
	}
	
	
}

/* end of file */