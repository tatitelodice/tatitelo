<?php

namespace Melquilabs\Pumbate\Services\Content;

class CountrySeoGenerator
{
    protected $countries = array(
		'uy' => 'Uruguay',
		'co' => 'Colombia',
		'ar' => 'Argentina',
		'cl' => 'Chile',
		'mx' => 'México',
		've' => 'Venezuela',
		'py' => 'Paraguay',
	);

    public function name($iso) {
		return $this->countries[$iso];
	}
	
	
	public function is_valid($iso) {
		return isset($this->countries[$iso]);
	}
	
	public function get_countries() {
		return $this->countries;
	}
	
	public function get_country_iso($country_name) {
		$country_name = trim(strtolower($country_name));
		
		foreach ($this->countries as $iso => $name) {
			$name = strtolower($name);
			
			if ($country_name == $name) {
				return $iso;
			}
		}
		
		return false;
	}
	
	public function get_country_seo($country_iso) {
		return strtolower($this->countries[$country_iso]);
	}
	
	
	public function get_default_region($iso) {
		return $this->default_regions[$iso];
	}

}