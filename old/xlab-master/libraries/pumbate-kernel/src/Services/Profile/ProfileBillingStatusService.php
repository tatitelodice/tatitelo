<?php 

namespace Melquilabs\Pumbate\Services\Profile;


class ProfileBillingStatusService 
{
    protected $payments_model;
    protected $deposit_model;
    protected $log_model;
    protected $pricing_model;
    

    public function __construct(
        $depositModel, $logModel, $paymentsModel, $pricingModel
    ) 
    {
        $this->deposit_model = $depositModel;
        $this->log_model = $logModel;
        $this->payments_model = $paymentsModel;
        $this->pricing_model = $pricingModel;
    }

    public function get_billing_status($user)
    {
        $next_payment_date = null;
        $days_remaining = 0;
        $notify_due_date = false;

        $last_notification = $this->log_model->find_one_by([
            'proUser' => $user, 'logDescription' => 'deposit_notification'
        ], 'logId desc');

        $payments_status = $this->payments_model->get_profile_payments_status($user);
        $last_deposit = $this->deposit_model->get_profile_last_deposit($user);

        $not_deposit_notification = ! ($last_notification && $last_notification['notify']);

        if ($last_deposit && $not_deposit_notification)
        {
            $days_remaining = $this->deposit_model->get_deposit_due_days($last_deposit);

            if ($payments_status['days_remaining'] && $payments_status['days_remaining'] > $days_remaining)
            {
                $days_remaining = $payments_status['days_remaining'];
            }

            if ($days_remaining < 7)
            {
                $notify_due_date = true;
            }
        }

        if ($payments_status['nextdate'])
        {
            $next_payment_date = $payments_status['nextdate']->format('d/m/Y');
        }

        $data = [
            'last_deposit' => $last_deposit
        ,   'last_deposit_notification' => $last_notification
        ,   'notify_due_date' => $notify_due_date
        ,   'days_remaining' => $days_remaining
        ,   'next_payment_date' => $next_payment_date
        ];

        return $data;
    }

    public function get_pricing($profile)
    {
        return $this->pricing_model->get_pricing_for_profile($profile);
    }

}


        