<?php

namespace Melquilabs\Pumbate\Services\Profile;

use Melquilabs\Pumbate\Services\Upload\UploadHandler;

class ProfileUploadHandler extends UploadHandler {
	
	protected $error_messages = array(
		1 => 'El archivo es demasiado pesado, prueba achicar la imagen y volverla a subir.',
		2 => 'El archivo es demasiado pesado, prueba achicar la imagen y volverla a subir',
		3 => 'El archivo quedó mal subido. Intenta subirlo nuevamente.',
		4 => 'No se ha subido ningún archivo',
		6 => 'Missing a temporary folder',
		7 => 'Failed to write file to disk',
		8 => 'A PHP extension stopped the file upload',
		'post_max_size' => 'El archivo es demasiado pesado, prueba achicar la imagen y volverla a subir.',
		'max_file_size' => 'El archivo es demasiado pesado, prueba achicar la imagen y volverla a subir.',
		'min_file_size' => 'El archivo es demasiado pequeño, busca una imagen más grande.',
		'accept_file_types' => 'El tipo de archivo que intentas subir no es válido, fijate que sea una imagen JPG, JPEG ó PNG.',
		'max_number_of_files' => 'Has alcanzado el límite de fotos que puedes subir.',
		'max_width' => 'La imagen es demasiado grande, prueba achicarla y volverla a subir.',
		'min_width' => 'La imagen es demasiado pequeña, busca una más grande.',
		'max_height' => 'La imagen es demasiado grande, prueba achicarla y volverla a subir.',
		'min_height' => 'La imagen es demasiado pequeña, busca una más grande.'
	);
	
	function __construct($options = null, $initialize = true) {
		parent::__construct($options, $initialize);
	}
	
	public function delete($print_response = true) {
		$success = false;
		
		$file_name = $this->get_file_name_param();
		$file_path = $this->get_upload_path($file_name);
		
		$file_exists = is_file($file_path) && $file_name[0] !== '.';
		
		if ($file_exists) {
			$parent_dir = dirname($file_path);
			$deleted_dir = $parent_dir . DIRECTORY_SEPARATOR . "deleted";

			if (!is_dir($deleted_dir)) {
				@mkdir($deleted_dir, 0777);
			}

			if (is_dir($deleted_dir)) {
				$selected_file_path = $file_path;
				$optimized_file_path = $this->get_upload_path($file_name, 'optimized');
				
				if (is_file($optimized_file_path)) {
					$selected_file_path = $optimized_file_path;
					@unlink($file_path);
				}
				
				$deleted_filename = preg_replace("!\.(.*?)$!", '_' . time() . '.\1', $file_name);
				$file_deleted_path = $deleted_dir . DIRECTORY_SEPARATOR . $deleted_filename;
				$success = rename($selected_file_path, $file_deleted_path);
			}

			if ($success) {
				foreach($this->options['image_versions'] as $version => $options) {
					if (!empty($version)) {
						$file = $this->get_upload_path($file_name, $version);
						if (is_file($file)) {
							unlink($file);
						}
					}
				}
			}
		}
		
		return $this->generate_response($success, $print_response);
	}
	
	
	protected function handle_file_upload($uploaded_file, $name, $size, $type, $error,
			$index = null, $content_range = null) {
		
		$file = new \stdClass();
		$file->name = $this->trim_file_name($name, $type, $index, $content_range);
		$file->size = $this->fix_integer_overflow(intval($size));
		$file->type = $type;
		if ($this->validate($uploaded_file, $file, $error, $index)) {
			$this->handle_form_data($file, $index);
			$upload_dir = $this->get_upload_path();
			if (!is_dir($upload_dir)) {
				mkdir($upload_dir, $this->options['mkdir_mode'], true);
			}
			$file_path = $this->get_upload_path($file->name);

			$append_file = $content_range && is_file($file_path) &&
				$file->size > $this->get_file_size($file_path);
			if ($uploaded_file && is_uploaded_file($uploaded_file)) {
				// multipart/formdata uploads (POST method uploads)
				if ($append_file) {
					file_put_contents(
						$file_path,
						fopen($uploaded_file, 'r'),
						FILE_APPEND
					);
				} else {
					move_uploaded_file($uploaded_file, $file_path);
				}
			} else {
				// Non-multipart uploads (PUT method support)
				file_put_contents(
					$file_path,
					fopen('php://input', 'r'),
					$append_file ? FILE_APPEND : 0
				);
			}
			
			$file_size = $this->get_file_size($file_path, $append_file);
			
			if ($file_size === $file->size) {
				if ($this->options['orient_image']) {
					$this->orient_image($file_path);
				}
				
				$mod_original_pic = isset($this->options['image_versions']['']);
				
				if ($mod_original_pic) {
					$options = $this->options['image_versions'][''];
					unset($this->options['image_versions']['']);
					
					if ($this->create_scaled_image($file->name, '', $options)) {
						if (isset($options['force_jpeg']) && $options['force_jpeg']) {
							$file->name = preg_replace('!\.[a-z]+$!i', '.jpg', $file->name);
				
							$new_file_path = $this->get_upload_path($file->name);
							
							rename($file_path, $new_file_path);
							
							$file_path = $new_file_path;
						}
					}
					
					$file_size = $this->get_file_size($file_path, true);
				}
				
				// SET ORIGINAL FILE NAME 
				$file->url = $this->get_download_url($file->name);
				// IMPORTANT!!!
				
				foreach($this->options['image_versions'] as $version => $options) {
					if ($this->create_scaled_image($file->name, $version, $options)) {
						$file->{$version.'_url'} = $this->get_download_url(
							$file->name,
							$version
						);
					}
				}
				
			} else if (!$content_range && $this->options['discard_aborted_uploads']) {
				unlink($file_path);
				$file->error = 'abort';
			}
			$file->size = $file_size;
			$this->set_file_delete_properties($file);
		}
		return $file;
	}
	
	protected function create_scaled_image($file_name, $version, $options) {
		$file_path = $this->get_upload_path($file_name);
		return \Melquilabs\Pumbate\Helpers\ProfilePicturesHelper::create_scaled_image($file_path, $version, $options);
	}
}
	
/* end of file */	
