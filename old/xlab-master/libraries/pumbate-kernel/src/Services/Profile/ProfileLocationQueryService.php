<?php 

namespace Melquilabs\Pumbate\Services\Profile;


class ProfileLocationQueryService 
{
    protected $redis;
    
    public function __construct($redis)
    {
        $this->redis = $redis;
    }

    public function getActiveCities($countryIso, $regionName, $gender)
    {
        $locations = $this->getActiveLocationsTree($countryIso, $gender);
        return $locations[$regionName];
    }

    public function getActiveRegions($countryIso, $gender)
    {
        $locations = $this->getActiveLocationsTree($countryIso, $gender);
        return array_keys($locations);
    }

    public function getActiveLocationsTree($countryIso, $gender)
    {
        $rawData = $this->redis->get('profiles:locations:'.$countryIso.':'.$gender);
        return json_decode($rawData, true);
    }
}