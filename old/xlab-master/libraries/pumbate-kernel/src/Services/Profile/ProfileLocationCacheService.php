<?php 

namespace Melquilabs\Pumbate\Services\Profile;

use Melquilabs\Pumbate\Models\ProfileModel;


class ProfileLocationCacheService 
{
    protected $db;
    protected $redis;
    protected $locationModel;
    protected $genderModel;

    public function __construct($db, $redis, $locationModel, $genderModel)
    {
        $this->db = $db;
        $this->redis = $redis;
        $this->locationModel = $locationModel;
        $this->genderModel = $genderModel;
    }

    public function refreshActiveLocations() 
    {
        $countryIsos = array_keys($this->locationModel->getCountries());
		
        foreach ($countryIsos as $countryIso) 
        {
            $this->refreshCountryActiveLocations($countryIso);
		}
    }
    
    public function refreshCountryActiveLocations($countryIso)
    {
        $regions = $this->locationModel->getRegions($countryIso);
        $genderNames = array_keys($this->genderModel->get_genders());

        $locationsByGender = [];

        foreach ($genderNames as $gender)
        {
            $genderKey = $this->resolveGenderKey($gender);
            $locationsByGender[$genderKey] = [];
        }

        foreach ($regions as $region) 
        {
            $regionName = $region['regName'];
            $regionGenders = $this->queryGendersByRegionProfiles($countryIso, $regionName);

            foreach ($regionGenders as $regionGender)
            {
                $genderKey = $this->resolveGenderKey($regionGender);

                if (!isset($locationsByGender[$genderKey][$regionName]))
                {
                    $locationsByGender[$genderKey][$regionName] = [];
                }
                
                $cities = $this->locationModel->getCities($countryIso, $regionName);

                foreach ($cities as $city) 
                {
                    $cityName = $city['citName'];
                    $profileCount = $this->queryProfileCountByGenderCity($countryIso, $regionGender, $cityName);
                
                    if ($profileCount > 0 && !in_array($cityName, $locationsByGender[$genderKey][$regionName]))
                    {
                        $locationsByGender[$genderKey][$regionName][] = $cityName;
                    }
                }                
            }
        }

        foreach ($locationsByGender as $gender => $genderLocations)
        {
            $key = 'profiles:locations:' . $countryIso . ':' . $gender;
            $dataJson = json_encode($genderLocations);
            $this->redis->set($key, $dataJson);
        }
    }

    protected function queryGendersByRegionProfiles($countryIso, $region)
    {
        $genders = [];

        $escaped_region = str_replace("'", "\\'", $region);
        //$escaped_region = $this->db->escape_str($region);

        $q = "SELECT proGender FROM profiles p LEFT JOIN keywords k ON p.proUser = k.proUser ";
        $q .= "WHERE proCountry = '".$countryIso."' AND proVerified = " . ProfileModel::PROFILE_STATUS_VERIFIED . " AND (proRegion like '".$escaped_region."' OR keyLocations like '%".$escaped_region."%')";
        $q .= "GROUP BY proGender";
        
        $res = $this->db->query($q);
        
        foreach ($res->result_array() as $region_gender_row) 
        {
            $region_gender = array_shift($region_gender_row);
            $genders[] = $region_gender;
        }

        return $genders;
    }

    protected function queryProfileCountByGenderCity($countryIso, $gender, $city)
    {
        $escaped_city = str_replace("'", "\\'", $city);
        $qc = "SELECT COUNT(*) AS total FROM profiles p LEFT JOIN keywords k ON p.proUser = k.proUser ";
        $qc .= "WHERE proCountry = '".$countryIso."' AND proVerified = " . ProfileModel::PROFILE_STATUS_VERIFIED . " AND proGender = '".$gender."' AND (proCity like '".$escaped_city."' OR keyLocations like '%:".$escaped_city."%') ";
        
        $resc = $this->db->query($qc);
        
        if ($resc->num_rows()) 
        {
            $row = $resc->row();
            return intval($row->total);
        }

        return 0;
    }

    protected function resolveGenderKey($gender)
    {
        return (preg_match('!^duo[hmt]{2}$!', $gender)) ? 'duo' : $gender;
    }

}