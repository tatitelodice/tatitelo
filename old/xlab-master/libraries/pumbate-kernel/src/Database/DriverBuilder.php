<?php

namespace Melquilabs\Pumbate\Database;

/**
 * Initialize the database
 *
 * @category	Database
 * @author		ExpressionEngine Dev Team
 * @link		http://codeigniter.com/user_guide/database/
 * @param 	string
 * @param 	bool	Determines if active record should be used or not
 */

class DriverBuilder
{
	public static function create($driverClass, $params = '')
	{
		if (is_string($params))
		{

			$params = self::parseParamsFromString($params);
		}

		// No DB specified yet?  Beat them senseless...
		if ( ! isset($params['dbdriver']) OR $params['dbdriver'] == '')
		{
			throw new \Exception('You have not selected a database type to connect to.');
		}
		
		// Instantiate the DB adapter
		$DB = new $driverClass($params);

		if ($DB->autoinit == TRUE)
		{
			$DB->initialize();
		}

		if (isset($params['stricton']) && $params['stricton'] == TRUE)
		{
			$DB->query('SET SESSION sql_mode="STRICT_ALL_TABLES"');
		}

		return $DB;
	}

	public static function parseParamsFromString($params)
	{
		/* parse the URL from the DSN string
		*  Database settings can be passed as discreet
		*  parameters or as a data source name in the first
		*  parameter. DSNs must have this prototype:
		*  $dsn = 'driver://username:password@hostname/database';
		*/

		if (($dns = @parse_url($params)) === FALSE)
		{
			throw new \Exception('Invalid DB Connection String');
		}

		$params = array(
			'dbdriver'	=> $dns['scheme'],
			'hostname'	=> (isset($dns['host'])) ? rawurldecode($dns['host']) : '',
			'username'	=> (isset($dns['user'])) ? rawurldecode($dns['user']) : '',
			'password'	=> (isset($dns['pass'])) ? rawurldecode($dns['pass']) : '',
			'database'	=> (isset($dns['path'])) ? rawurldecode(substr($dns['path'], 1)) : ''
		);

		// were additional config items set?
		if (isset($dns['query']))
		{
			parse_str($dns['query'], $extra);

			foreach ($extra as $key => $val)
			{
				// booleans please
				if (strtoupper($val) == "TRUE")
				{
					$val = TRUE;
				}
				elseif (strtoupper($val) == "FALSE")
				{
					$val = FALSE;
				}

				$params[$key] = $val;
			}
		}
		
		return $params;
	}
}

/* eof */