<?php

namespace Melquilabs\Pumbate\Database\Driver\MySQLi;

use Melquilabs\Pumbate\Database\ActiveRecordTrait;

class ActiveRecord extends Driver {

    use ActiveRecordTrait;

}

/* End of file mysqli_driver.php */
/* Location: ./system/database/drivers/mysqli/mysqli_driver.php */