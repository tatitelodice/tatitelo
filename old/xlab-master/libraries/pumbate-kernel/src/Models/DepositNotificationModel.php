<?php

namespace Melquilabs\Pumbate\Models;


class DepositNotificationModel
{
    protected $db;
    
	public function __construct($db)
	{
        $this->db = $db;
	}

    public function find_by($params, $offset=10, $order_by=false)
    {
        $notifications = array();
        
        if (!empty($params['user']))
        {
            $this->db->where('proUser', $params['user']);
        }

        if (!empty($params['fromDate']))
        {
            $this->db->where('logDate >=', $params['fromDate']);
        }

        if (!empty($params['toDate']))
        {
            $this->db->where('logDate <=', $params['toDate']);
        }
        
        $this->db->where('logDescription', 'deposit_notification');

        $order_by = (!empty($order_by))? $order_by : 'logDate desc';
        $this->db->order_by($order_by);
        
        if (!empty($offset))
        {
            $this->db->limit($offset);
        }
        
        $res = $this->db->get('logs');
        
        if ($res->num_rows) {
            
            foreach ($res->result_array() as $row) {
                $notifications[] = $this->convert_row_to_entity($row);
            }
        }
        
        return $notifications;        
    }

    public function convert_row_to_entity($row)
    {
        $log = [];

        foreach ($row as $k => $v)
        {
            $newKey = preg_replace('!^log!', '', $k);
            $newKey = lcfirst($newKey);
            $log[$newKey] = $v;
        }

        $log['user'] = $row['proUser'];
        $log['meta'] = json_decode($log['meta'], true);
        $log['deposit'] = $log['meta']['deposit'];

        return $log;
    }

}

/* eof */