<?php

namespace Melquilabs\Pumbate\Models;


class DepositModel
{
	protected $db;
	protected $profile_model;    

	public function __construct($db, $profileModel)
	{
        $this->db = $db;
        $this->profile_model = $profileModel;
	}

    protected function appendDbWhere($field, $value)
    {
        if (is_array($value))
        {
            
            if (in_array(null, $value))
            {
                $value = array_diff($value, [null]);
                $subcondition = "$field IN (" . implode(",", $value) . ") OR $field IS NULL";
                $this->db->where("($subcondition)");
            }
            else
            {
                $this->db->where_in($field, $value);
            }
        }
        else
        {
            $this->db->where($field, $value);
        }
    }

    public function get_deposit($deposit_id)
    {
        $this->db->where('depId', $deposit_id);
        $this->db->limit(1);
        $res = $this->db->get('deposits');
        return $res->row_array();
    }


    public function find_by($params, $offset=10, $order_by=false)
    {
        $deposits = array();
        
        foreach ($params as $field => $value)
        {
            $this->appendDbWhere($field, $value);
        }
        
        $this->db->where('depDeleted', 0);

        $order_by = (!empty($order_by))? $order_by : 'depDate desc';
        $this->db->order_by($order_by);
        
        if (!empty($offset))
        {
            $this->db->limit($offset);
        }
        
        $res = $this->db->get('deposits');
        
        if ($res->num_rows) {
            
            foreach ($res->result_array() as $row) {
                $deposits[] = $this->convert_row_to_deposit($row);
            }
        }
        
        return $deposits;        
    }


    public function get_all_deposits($options)
    {
        $deposits = [];

        if (is_array($options['paymentAccountId']))
        {
            // filter null values
            $options['paymentAccountId'] = array_filter($options['paymentAccountId']);
            if (empty($options['paymentAccountId'])) return [];
        }
   
        $this->db->select('dp.*,pa.payComments');
        
        $this->db->from('deposits dp');
        $this->db->join('profiles pf', 'pf.proUser = dp.proUser', 'left outer');
        $this->db->join('payments pa', 'pa.proUser = dp.proUser', 'left outer');
        
        $this->db->where('dp.depDeleted', 0);
        //$this->db->where('dp.proCountry', $country);
        
        $this->db->where('dp.depDate >=', $options['startDate']);
        $this->db->where('dp.depDate <=', $options['endDate']);
        
        if (isset($options['paymentAccountId']))
        {
            $this->appendDbWhere('dp.paymentAccountId', $options['paymentAccountId']);
        }

        $this->db->order_by("depDate", "desc");
        
        $res = $this->db->get();

        //echo $this->db->last_query(); die;
        
        if ($res->num_rows()) {
            //print_r($res->result_array());
            foreach ($res->result_array() as $dbrow) {
                $deposit = $this->convert_row_to_deposit($dbrow);
                $deposit['comments'] = $dbrow['payComments'];
                $deposits[] = $deposit;
            }
        }
        
        return $deposits;
    }


    public function get_profile_last_deposit($user)
    {
        $deposits = $this->get_profile_deposits($user, array(
            'order_by' => 'depDate desc', 'limit' => 1
        ));
        return ($deposits)? $deposits[0] : null;
    }


    public function get_profile_deposits($user, $options=array())
    {
        $deposits = array();
        
        $this->db->where('proUser', $user);
        $this->db->where('depDeleted', 0);

        $order_by = (!empty($options['order_by']))? $options['order_by'] : 'depDate desc';
        $this->db->order_by($order_by);
        
        if (!empty($options['limit']))
        {
            $this->db->limit($options['limit']);
        }
        
        $res = $this->db->get('deposits');
        
        if ($res->num_rows) {
            
            foreach ($res->result_array() as $row) {
                $deposits[] = $this->convert_row_to_deposit($row);
            }
        }
        
        return $deposits;
    }


    public function convert_row_to_deposit($row)
    {
        $deposit = array();

        $deposit['id'] = $row['depId'];
        $deposit['user'] = $row['proUser'];

        $deposit['date'] = date('d/m/Y', strtotime($row['depDate']));
        $deposit['time'] = $row['depositTime'];
        $deposit['name'] = $row['depName'];
        $deposit['concept'] = $row['depConcept'];
        $deposit['amount'] = intval($row['depAmount']);
        $deposit['status'] = $row['depStatus'];
        $deposit['paymentAccountId'] = $row['paymentAccountId'];

        $date_applied = ($row['depAppliedAt']) ? date('d/m/Y', strtotime( $row['depAppliedAt'] )) : null;

        $deposit['date_applied'] = $date_applied;

        return $deposit;
    }

    public function apply_deposit($deposit_id, $data)
    {
        $this->db->where('depId', $deposit_id);
        $this->db->set('proUser', $data['user']);
        
        $this->db->set('depStatus', 'applied');
        $this->db->set('depAppliedAt', $data['applied_at']);
        $this->db->set('depConcept', $data['concept']);

        $this->db->limit(1);

        $this->db->update('deposits');
    }


    public function delete_deposit($deposit_id)
    {
        $this->db->where('depId', $deposit_id);
        $this->db->set('depDeleted', 1);
        $this->db->limit(1);

        $res = $this->db->update('deposits');

        return $res;
    }


    public function undo_deposit_apply($deposit_id, $user)
    {
        $this->db->where('depId', $deposit_id);
        $this->db->set('proUser', null);
        $this->db->set('depStatus', 'deposited');
        $this->db->set('depAppliedAt', null);
        $this->db->set('depConcept', '');

        $this->db->limit(1);

        $res = $this->db->update('deposits');
        
        return $res;
    }


    public function get_default_deposit_concept()
    {
        $m = new \Moment\Moment('now'); $m->setLocale('es_ES');
        $m = ($m->getDay() > 26) ? $m->addMonths(1) : $m;
        return $m->format('F');
    }

    public function get_deposit_due_days($deposit)
    {
        $today_dt = new \Moment\Moment();

        $deposit_dt = preg_replace('!(\d+)/(\d+)/(\d+)!', '$3-$2-$1', $deposit['date_applied']);
        $deposit_dt = new \Moment\Moment($deposit_dt);
        $deposit_threshold_dt = $deposit_dt->cloning()->addDays(30);

        $due_days = intval($today_dt->from($deposit_threshold_dt)->getDays());

        return $due_days;
    }

    
}

/* eof */