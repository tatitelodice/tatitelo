<?php

namespace Melquilabs\Pumbate\Models;

class ContentsModel
{
	protected $db;

	public function __construct($db)
	{
		$this->db = $db;
	}
	
	public function get_page_statuses() {
		return array(
			'published' => "Publicada",
			'pending' => "Pendiente",
			'oculta' => "Oculta",
		);		
	}
	
	public function get_page_types() {
		return array(
			'page' => "Pagina del Sitio",
			'blog_post' => "Post del Blog",
		);				
	}
	
	
}

/* end of file */
