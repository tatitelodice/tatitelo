<?php

namespace Melquilabs\Pumbate\Models;

use Carbon\Carbon;

class AnalyticsModel
{
	protected $db;

	public function __construct($db) {
		$this->db = $db;
	}
	
	public function getUserAnalytics($user, $start_date = false, $end_date = false) {
		$data = array();
		
		if (!$start_date) {
			$start_date = date('Y-m-d');
		}
		if (!$end_date) {
			$end_date = date('Y-m-d');
		}
		
		$this->db->where('anaDate >=', $start_date);
		$this->db->where('anaDate <=', $end_date);
		
		$this->db->where('proUser', $user);
		
		$this->db->order_by('anaDate', 'desc');
		
		$results = $this->db->get('analytics');

		if ($results->num_rows()) {
			foreach($results->result_array() as $result_row) {
				$data[$result_row['anaDate']] = array(
					'date' => $result_row['anaDate']
				,	'pageViews' => $result_row['anaPageViews']
				,	'uniquePageViews' => $result_row['anaUniquePageViews']
				,	'createdAt' => $result_row['createdAt']
				,	'modifiedAt' => $result_row['modifiedAt']
				);
			}
		}
			
		return $data;
	}

	
	public function getUserVisitsCount($user) {
		$this->db->select('SUM(anaPageViews) as pageviews');
		$this->db->where('proUser', $user);
		$result = $this->db->get('analytics');
		
		return intval($result->row()->pageviews);
	}
	

	public function createMultiple($analyticsData)
	{
		$insert_rows = [];
		$nowDt = Carbon::now('UTC')->toDateTimeString();

        foreach ($analyticsData as $item)
        {
            $row = [
				'proUser' => $item['user']
			,	'anaDate' => $item['date']
			,	'anaPageViews' => $item['pageViews']
			,	'anaUniquePageViews' => $item['uniquePageViews']
			,	'createdAt' => $nowDt
			,	'modifiedAt' => $nowDt
			];
            
            $insertRows[] = $row;
        }

        $result = $this->db->insert_batch('analytics', $insertRows);  
	}


	public function update($user, $date, $data)
	{
		$this->db->set('anaPageViews', $data['pageViews']);
		$this->db->set('anaUniquePageViews', $data['uniquePageViews']);

		$currentDt = Carbon::now('UTC')->toDateTimeString();
		$this->db->set('modifiedAt', "'" . $currentDt . "'", false);

		$this->db->where('proUser', $user);
		$this->db->where('anaDate', $data['date']);
		$this->db->limit(1);
		
		$this->db->update('analytics');
		return $this->db->affected_rows() > 0;
	}

}
	
/* end of file */
