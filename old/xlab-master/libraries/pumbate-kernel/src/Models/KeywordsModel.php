<?php

namespace Melquilabs\Pumbate\Models;


class KeywordsModel
{
	protected $db;
	protected $keywords;
    
	public function __construct($db, $keywordsLib)
	{
		$this->db = $db;
		$this->keywords = $keywordsLib;
	}
	
	public function get_keywords($user) {
		$user = strtolower($user);
		
		$keywords_data = false;
		
		$this->db->where('proUser', $user);
		$query = $this->db->get('keywords');
		
		if ($query->num_rows() > 0) {
			$result_data = $query->row_array();
			
			$prefix = 'key';
			$keywords_data = array();
			
			foreach($result_data as $name => $value) {
				$name = str_replace($prefix, '', $name);
				$name[0] = strtolower($name[0]);
				
				$keywords_data[$name] = $value;
			}
		}
		
		return $keywords_data;		
	}
	
	
	public function create_if_not_exists($user) {
		$this->db->where('proUser', $user);
		$this->db->limit(1);
		
		$user_in_db = $this->db->get('keywords')->num_rows() > 0;
		
		if ($user_in_db === false) {
			$this->db->set('proUser', $user);
			$this->db->insert('keywords');
		}
	}
	
	
	public function update($user, $data) {
		$extra_locations = array();
			
		for ($i=0; $i < 5; $i++) {

			if (isset($data['extra_region' . $i]) && $data['extra_region' . $i]) {

				$extra_location = $data['extra_region' . $i];

				if (isset($data['extra_city' . $i]) && $data['extra_city' . $i])
					$extra_location .= '::' . $data['extra_city' . $i];

				$extra_locations[] = $extra_location;
			}
		}

		$this->db->set('keyLocations', implode('#', $extra_locations));

		$raw_fulltext_keywords = '';
		$raw_fulltext_keywords .= ' ' . $data['user'] . ' ' . $data['name'];
		$raw_fulltext_keywords .= ' ' . preg_replace('!\D!', '', $data['phone']);
		$raw_fulltext_keywords .= ' ' . $data['place'] . ' ' . $data['city'] . ' ' . $data['region'];
		$raw_fulltext_keywords .= ' ' . implode(' ', $extra_locations);
		$raw_fulltext_keywords .= ' ' . $data['title'] . ' ' . $data['subtitle'] . ' ' . $data['subtitle2'];
		$raw_fulltext_keywords .= ' ' . $data['about'] . ' ' . $data['services'];

		if (isset($data['days']) && is_array($data['days'])) {
			$raw_fulltext_keywords .= ' ' . implode(' ', $data['days']);
		}

		if (isset($data['categories']) && is_array($data['categories'])) {
			$raw_fulltext_keywords .= ' ' . implode(' ', $data['categories']);
		}

		$raw_fulltext_keywords = preg_replace('!\[\s*cambialo.*?\]!i', '', $raw_fulltext_keywords);

		$fulltext_keywords = $this->keywords->generate($raw_fulltext_keywords);

		$this->db->set('keyFulltext', $fulltext_keywords );

		$this->db->where('proUser', $data['user']);
		
		$update_success = $this->db->update('keywords');
		
		return $update_success;
	}
	
}

/* end of file */
