<?php

namespace Melquilabs\Pumbate\Models;


class PricingModel
{
    protected $db;
    protected $config;
    
	public function __construct($db, $config)
	{
        $this->db = $db;
        $this->config = $config;
	}


    public function get_all_prices_by_country()
    {
        return $this->config->item('pricing');
    }


    public function get_pricing_for_profile($profile) {
        $country = $profile['country'];
        $gender = $profile['gender'];

        $pricing_config = $this->config->item('pricing');

        if (empty($pricing_config[$country]))
        {
            return false;
        }

        $country_pricing = $pricing_config[$country];
        $pricing = $country_pricing['default'];

        if (isset($country_pricing[$gender]))
        {
            $pricing = $country_pricing[$gender];
        }

        return $pricing;
    }


    public function get_profile_account_type_for_amount($profile, $amount)
    {
        $resolved_account_type = ($profile['accountType'] > PROFILE_BASIC) ? $profile['accountType'] : PROFILE_BASIC;

        $pricing = $this->get_pricing_for_profile($profile);

        $amount = intval($amount);

        foreach ($pricing as $price_item)
        {
            $same_or_greater_amount = $amount >= intval($price_item['price']);

            if ($same_or_greater_amount && $price_item['accountType'] > $resolved_account_type)
            {
                $resolved_account_type = $price_item['accountType'];
            }
        }

        return $resolved_account_type;
    }

}