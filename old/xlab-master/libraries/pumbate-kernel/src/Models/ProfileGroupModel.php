<?php

namespace Melquilabs\Pumbate\Models;


class ProfileGroupModel
{
	const PROFILE_GROUP_NONE = 0;
	const PROFILE_GROUP_GARBAGE = 10;
	
	const PROFILE_GROUP_RESERVED_MAX_ID = 1000;
	
    protected $db;
    
	public function __construct($db)
	{
        $this->db = $db;
	}
	
	public function get_all_groups($params=false) {
		$groups = array();
		
		$this->db->order_by('name', 'asc');
		
		$results = $this->db->get('profile_group');
		
		if ($results->num_rows()) {
			$groups = $results->result_array();
		}
		
		return $groups;
	}
	
	
	public function get_group_by_name($group_name) {
		$group_data = false;
		
		$this->db->where('name', $group_name);
		$this->db->limit(1);
		
		$query = $this->db->get('profile_group');
		
		if ($query->num_rows() > 0) {
			$group_data = $query->row_array();
		}
		
		return $group_data;
	}
	
	
	public function create_group($group_data) {
		$group_id = false;
		
		$required_fields = array('name');
		$has_required_fields = true;
		
		foreach ($required_fields as $required_field) {
			$has_required_fields = $has_required_fields && isset($group_data[$required_field]);
		}
		
		if ($has_required_fields) {
			$this->db->set('name', $group_data['name']);
			
			if ($this->db->insert('profile_group')) {
				$group_id = $this->db->insert_id();
			}
		}
		
		return $group_id;
	}

}

/* end of file */