<?php

namespace Melquilabs\Pumbate\Models;


class AdminModel
{
	protected $name;
	protected $login;
	protected $country;
	protected $settings;
	
	protected $db;
	protected $session;

	public function __construct($db, $session) {
		$this->db = $db;
		$this->session = $session;
		$this->_initialize();
	}
	
	
	private function _initialize() {
		$this->id = -1;
		$this->name = false;
		$this->login = false;
		$this->settings = array();
		$this->country = false;
		
		$admin_login_data = $this->session->userdata('a_');
		
		if ($admin_login_data !== false) {
			
			$data_parts = explode("::", $admin_login_data);
			
			if (count($data_parts) == 2) {
				list($name, $sess_hash) = $data_parts;
				
				$this->db->select('admId,admCountry,admSettings,admPassword');
				$this->db->where('admName', $name);
				$this->db->limit(1);

				$results = $this->db->get('administrators');

				 if ($results->num_rows() > 0) {
					$row = $results->row_array();
					
					//print_r($row); die;

					if ($sess_hash == $this->_get_sess_hash($name, $row["admPassword"])) {
						$this->id = $row["admId"];
						$this->name = $name; 
						$this->login = true;
						$this->country = $row['admCountry'];

						$settings_json = json_decode($row['admSettings'], true);

						if (is_array($settings_json)) {
							$this->settings = $settings_json;
						}
					}
				}
			}
		}
	}
	
	public function login_exists() {
		return $this->login;
	}
	
	public function get_id() {
		return $this->id;
	}
	
	public function get_name() {
		return $this->name;
	}
	
	public function get_country() {
		return $this->country;
	}
	
	public function login($name, $pass) {
		$sha_pass = sha1($pass);
		
		$result = $this->db->get_where('administrators', array(
			'admName' => $name,
			'admPassword' => $sha_pass,
		));

		//print_r($result->row_array()); die;

		if ($result->num_rows() > 0) {
			//print_r($this->_get_sess_hash($name, $pass)); die;
			$this->session->set_userdata('a_', $name . "::" . $this->_get_sess_hash($name, $sha_pass));
			return true;
		}
		
		return false;
	}
	
	private function _get_sess_hash($name, $sha_password) {
		$initial_var = sha1( $name . "CHOKOLATE" . $sha_password);
		$len_var = intval(strlen($name) / 2);
		
		return substr($initial_var, 2, $len_var) . $initial_var . substr(md5($initial_var), 0, 5);
	}
	
	public function logout() {
		$this->session->set_userdata('a_', false);
	}
	
	public function set_setting($name, $value) {
		$this->settings[$name] = $value;
	}
	
	public function get_setting($name) {
		if (isset($this->settings[$name])) {
			return $this->settings[$name];
		} else {
			return false;
		}
	}
	
	public function save_settings() {
		$json_settings = json_encode($this->settings);
		
		if ($json_settings) {
			$this->db->set('admSettings', $json_settings);
			$this->db->where('admName', $this->name);
			$this->db->update('administrators');
		}
	}
	
}

/* end of file */
