<?php

namespace Melquilabs\Pumbate\Models;


class SettingModel
{
    protected $db;
    
	public function __construct($db)
	{
        $this->db = $db;
    }

    public function getAll()
    {
        $result = $this->db->get('setting');
        $results = $result->result();
        
        $settings = [];

        foreach ($results as $row)
        {
            $settings[] = $this->rowToSetting($row);
        }
        
        return $settings;
    }

    public function getById($id)
    {
        $this->db->where('id', $id);
        $this->db->limit(1);
        $result = $this->db->get('setting');

        if ($result)
        {
            return $this->rowToSetting($result->row());
        }
        
        return null;
    }

    public function getByKeyname($keyname)
    {
        $this->db->where('keyname', $keyname);
        $this->db->limit(1);
        $result = $this->db->get('setting');
        
        if ($result)
        {
            return $this->rowToSetting($result->row());
        }
        
        return null;
    }

    protected function rowToSetting($row)
    {
        $row->value = json_decode($row->value, true);
        $row->metadata = json_decode($row->metadata, true);
        return $row;
    }
}