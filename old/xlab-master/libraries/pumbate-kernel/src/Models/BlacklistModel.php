<?php

namespace Melquilabs\Pumbate\Models;


class BlacklistModel
{
	protected $db;
	
	public function __construct($db)
	{
		$this->db = $db;
	}
	
	public function insert_entry($data) {
		$this->db->set('blaName', $data['name']);
		$this->db->set('blaDescription', $data['description']);
		$this->db->set('blaPhone', $data['phone']);
		$this->db->set('blaEmail', $data['email']);
		$this->db->set('blaDate', "'" . date('Y-m-d') . "'", false);

		$this->db->insert('blacklist');
	}
	
	
	public function is_blacklisted($data) {
		$blacklisted = false;
		
		if ($data['phone']) {
			$data['phone'] = preg_replace('!\D!', '', $data['phone']);
			$this->db->where('blaPhone', $data['phone']);
			$this->db->where('blaDeleted', 0);
			$blacklisted = $blacklisted || ($this->db->get('blacklist')->num_rows() > 0);
		}
		
		if ($data['email']) {
			$this->db->like('blaEmail', $data['email']);
			$this->db->where('blaDeleted', 0);
			$blacklisted = $blacklisted || ($this->db->get('blacklist')->num_rows() > 0);
		}
		
		return $blacklisted;
	}
	
	
}

/* end of file */
