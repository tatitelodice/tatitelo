<?php

namespace Melquilabs\Pumbate\Models;


class LocationModel
{
    protected $countries = [
		'uy' => 'Uruguay',
		'co' => 'Colombia',
		'ar' => 'Argentina',
		'cl' => 'Chile',
		'mx' => 'México',
		've' => 'Venezuela',
		'py' => 'Paraguay',
    ];
    
    
	protected $defaultRegions = [
		'uy' => 'Montevideo',
		'co' => 'Distrito Capital de Bogotá',
		'ar' => 'Capital Federal',
		'cl' => 'Santiago',
		'mx' => 'Distrito Federal',
		've' => 'Distrito Capital',
		'py' => 'Asunción',
    ];

    protected $db;
    

	public function __construct($db)
	{
        $this->db = $db;
    }


    public function getCountries()
    {
        return $this->countries;
    }


    public function getRegions($countryIso)
    {
        $this->db->select('regName');
        $this->db->where('regActive', 1);
        $this->db->where('regCountry', $countryIso);
        $this->db->order_by('regName', 'asc');

        $query = $this->db->get('regions');

        return $query->result_array();
    }


    public function getDefaultRegion($countryIso)
    {
		return $this->defaultRegions[$countryIso];
	}


    public function getCities($countryIso, $regionName)
    {
        $this->db->select('citName');
        $this->db->where('citActive', 1);
        $this->db->where('citCountry', $countryIso);
        $this->db->where('regName', $regionName);
        $this->db->order_by('citName', 'asc');
        $query = $this->db->get('cities');

        return $query->result_array();
    }
}
