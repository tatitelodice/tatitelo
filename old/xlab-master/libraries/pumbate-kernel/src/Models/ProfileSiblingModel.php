<?php

namespace Melquilabs\Pumbate\Models;


class ProfileSiblingModel
{
	protected $profile_model;
	
	public function __construct($profileModel)
	{
		$this->profile_model = $profileModel;
	}
	
	public function get_all_sibling_site_profiles($sibling_site) {
		
	}

	public function match_users_with_phones($phones_to_search, $users_by_phone, $profiles_by_user, $only_published=false) {
		$users_found = array();
		
		foreach ($phones_to_search as $phone_search) {
			if (isset($users_by_phone[$phone_search])) {
				
				foreach ($users_by_phone[$phone_search] as $user) {
					$append_user = !$only_published || ($profiles_by_user[$user]["verified"] == "1");
					
					if (!in_array($user, $users_found) && $append_user) {
						$users_found[] = $user;
					}
				}
				
			}
		}
		
		return $users_found;
	}
	
}

/* end of file */