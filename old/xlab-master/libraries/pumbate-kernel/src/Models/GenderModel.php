<?php

namespace Melquilabs\Pumbate\Models;

class GenderModel {

	protected $genders = array(
		'mujer' => 'Mujer',
		'trans' => 'Trans',
		'hombre' => 'Hombre',
		'duohm' => 'Dúo Mujer-Hombre',
		'duomt' => 'Dúo Mujer-Trans',
		'duomm' => 'Dúo Mujer-Mujer',
		'duohh' => 'Dúo Hombre-Hombre',
	);
	
	protected $genders_seo = array(
		'mujer' => 'chicas',
		'trans' => 'travestis-y-trans',
		'hombre' => 'hombres',
		'duohm' => 'trio-mujer-y-hombre',
		'duomt' => 'trio-mujer-y-trans',
		'duomm' => 'trio-lesbico',
		'duohh' => 'trio-gay',	
		'duo' => 'duos-y-trios',
	);
	
	protected $gender_symbols = array(
		'mujer' => '&#x2640;',
		'trans' => '&#x26a5;',
		'hombre' => '&#x2642;',
		'duohm' => '&#x2642;&#x2640;',
		'duomt' => '&#x2640;&#x26a5;',
		'duomm' => '&#x2640;&#x2640;',
		'duohh' => '&#x2642;&#x2642;',
	);
	
	public function is_gender($gender) {
		return isset($this->genders[$gender]);
	}
	
	public function get_genders() {
		return $this->genders;
	}
	
	public function get_available_genders($country = false) {
		$available_genders = array();
		
		foreach ($this->genders as $g_key => $g_val) {
			if (strpos($g_key, 'duo') === 0 ) {
				$add = in_array($country, array('uy'));
			} else {
				$add = true;
			}
			
			if ($add) {
				$available_genders[$g_key] = $g_val;
			}
		}
		
		return $available_genders;
	}
	
	public function get_genders_seo() {
		return $this->genders_seo;
	}
	
	public function get_gender_seo($gender) {
		return $this->genders_seo[$gender];
	}
	
	public function get_symbol($gender) {
		return $this->gender_symbols[$gender];
	}
	
}
	
/* end of class */
