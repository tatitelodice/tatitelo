<?php

namespace Melquilabs\Pumbate\Models;

use Melquilabs\Pumbate\Helpers\StringHelper;

class ProfileModel
{
	protected $db;
	protected $session;
	protected $phoneParser;
	protected $adminModel;

	const PROFILE_FREE      = 0;
	const PROFILE_BASIC     = 1;
	const PROFILE_PREMIUM   = 2;
	const PROFILE_TOP       = 3;
	const PROFILE_SUPERSTAR = 4;
	
	const PROFILE_STATUS_NOT_VERIFIED = 0;
	const PROFILE_STATUS_VERIFIED     = 1;
	const PROFILE_STATUS_HIDDEN       = 2;
			
	const PROFILE_NOT_DELETED = 0;
	const PROFILE_DELETED     = 1;
	
	const PICTURES_NOT_VERIFIED = 0;
	const PICTURES_VERIFIED     = 1;
    
	public function __construct($db, $session, $phoneParser, $adminModel)
	{
		$this->db = $db;
		$this->session = $session;
		$this->phoneparser = $phoneParser;
		$this->admin_model = $adminModel;
	}

	public function format_result($resultset)
	{
		$resultset = (method_exists($resultset, 'result_array')) ? $resultset->result_array() : $resultset;
		
		$profiles = [];

		foreach ($resultset as $result_row)
		{
			$profiles[] = $this->unprefix_fields($result_row);
		}

		return $profiles;
	}

	public function unprefix_fields($profile_row, $prefix='pro')
	{
		$profile_data = [];

		$profile_row = ((array)$profile_row);
				
		foreach($profile_row as $name => $value) {
			$name = str_replace($prefix, '', $name);
			$name[0] = strtolower($name[0]);
			$profile_data[$name] = $value;
		}

		return $profile_data;
	}

	
	public function full_get_all($country=false, $fields=false) {
		
		$profiles = array();
				
		if (!empty($fields)) {
			$select_fields = array();
			
			$valid_fields = array(
				"proId", "proUser", "proEmail", "proVerified",
				"proAccountType", "proPriority", "proDomain", 
				"proTheme", "proName", "proGender", "proAge",  
				"proPhone", "proNumericPhones", "proDays", "proCategories",
				"proCity", "proRegion", "proCountry", "proPlace", "proTitle", "proSubtitle", 
				"proSubtitle2", "proAbout", "proServices", "proPicture", "proPictureType",
				"proVerifiedPictures", "proAbsent", "proAbsentMessage", "proPanelMessage", 
				"proDeleted", "proCreatedDate", "proModifiedDate"
			);
			
			foreach ($fields as $field) {
				if (in_array($field, $valid_fields)) {
					$select_fields[] = $field;
				}
			}
			
			if (!empty($select_fields)) {
				$this->db->select(implode(",", $select_fields));
			}
		}
		
		if ($country) {
			$this->db->where('proCountry', $country);
		}
		
		$results = $this->db->get('profiles');
		
		if ($results->num_rows()) {
			foreach($results->result_array() as $result_row) {	
				$profiles[] = $this->unprefix_fields($result_row);
			}
		}
		
		return $profiles;
	}
	
	
	public function get_all($country = false, $gender = false, $include_unverified = false, $include_absent = false, $account_level = false) {		

		$profiles = array();
		
		$this->db->select(
			'proUser,proName,proGender,proPhone,proPlace,proAccountType,proPriority,proCity,proRegion,proTitle,proSubtitle,proSubtitle2,proAbout,proServices,proPicture'
		);
		
		if (!$include_unverified) {
			$this->db->where('proVerified', PROFILE_STATUS_VERIFIED);
		}
		
		if (!$include_absent) {
			$this->db->where('proAbsent', 0);
		}
		
		if ($account_level) {
			$this->db->where('proAccountType >=', $account_level);
		}
		
		
		$this->db->where('proDeleted', 0);
		
		if ($country) {
			$this->db->where('proCountry', $country);
		}
		
		if ($gender) {
			$this->db->where('proGender', $gender);
		}
		
		$results = $this->db->get('profiles');
		
		if ($results->num_rows()) {
			foreach($results->result_array() as $result_row) {
				$profiles[] = $this->unprefix_fields($result_row);
			}
		}
		
		return $profiles;
	}
	
	
	public function get_full_profile_by_id($proId) {
		$profile_data = false;
		
		$this->db->where('proId', $proId);
		
		$this->db->join('clients', 'clients.proUser = profiles.proUser');
				
		$query = $this->db->get('profiles');
		
		if ($query->num_rows() > 0) {
			$profile_data = $query->row_array();	
		}
		
		return $profile_data;
	}
	
	
	public function get_full_profile($user, $prefix_remove = false) {
		$user = strtolower($user);
		
		$profile_data = false;
		
		$this->db->where('profiles.proUser', $user);
		$this->db->join('clients', 'clients.proUser = profiles.proUser');
				
		$query = $this->db->get('profiles');
		
		if ($query->num_rows() > 0) {
			$result_data = $query->row_array();
			
			if ($prefix_remove) {
				$profile_data = $this->unprefix_fields($result_data);
				$phoneNumbers = $profile_data['numericPhones'];
				
			} else {
				$profile_data = $result_data;
				$phoneNumbers = $profile_data['proNumericPhones'];
			}

			$profile_data['phoneNumbers'] = explode(',', $phoneNumbers);
		}
		
		return $profile_data;
	}
	

	public function get_profile($user) {
		$user = strtolower($user);
		
		$profile_data = false;
		
		$this->db->where('proUser', $user);
		$query = $this->db->get('profiles');
		
		if ($query->num_rows() > 0) {
			$result_data = $query->row_array();
			$profile_data = $this->unprefix_fields($result_data);
			$profile_data['phoneNumbers'] = explode(',', $profile_data['numericPhones']);
		}

		return $profile_data;
	}
	
	
	public function get_verification_levels() {
		return array(
			PROFILE_STATUS_NOT_VERIFIED => 'No',
			PROFILE_STATUS_VERIFIED => 'Sí, en portada',
			PROFILE_STATUS_HIDDEN => 'Sí, oculto'
		);
	}
	
	
	public function get_account_types() {
		return array(
			PROFILE_FREE => 'Gratuita',
			PROFILE_BASIC => 'Basica',
			PROFILE_PREMIUM => 'Destacada',
			PROFILE_TOP => 'Top',
			PROFILE_SUPERSTAR => 'SuperStar'
		);
	}
	
	public function get_logged_user() {
		return $this->session->userdata('user');
	}
	
	public function logout() {
		$this->session->unset_userdata('user');
	    $this->session->unset_userdata('login');
	}

	public function login_restore($user, $code) {
		$user = strtolower($user);
		$code = strtoupper($code);

		$this->db->where('proUser', $user);
		$this->db->where('proRestoreCode', $code);
		$this->db->where('proRestoreExpires >=', "'" . date('Y-m-d H:i:s') . "'", FALSE);
		
		$result = $this->db->get('profiles_restores');
		
		return $result->num_rows() > 0;
	}
	
	public function login($user, $password) {
		$user = strtolower($user);
		$shapass = sha1($password);

		$result = $this->db->get_where('profiles', array(
			'proUser' => $user,
			'proPassword' => $shapass,
		));

		$login = $result->num_rows() > 0;

		if ($login) {
			$this->session->set_userdata('login', sha1($user . $shapass) );
			$this->session->set_userdata('user', $user);	
		}

		return $login;
	}
	
	
	public function login_exists($user) {		
		$has_session = false;
				
		$login_by_admin = $this->admin_model->login_exists();
		$login_hash = $this->session->userdata('login');
		
		if ($login_hash || $login_by_admin) {
			$this->db->select('proUser,proPassword,proCountry');
			$this->db->where('proUser', $user);
			$resultset = $this->db->get('profiles');

			if ($resultset->num_rows() > 0) {
				$row = $resultset->row_array();
				
				if ($login_by_admin) {
					$admin_country = $this->admin_model->get_country();

					if ($admin_country != '') {
						$has_session = $admin_country == $row['proCountry'];
					} else {
						$has_session = true;
					}
				} 
				
				if (!$has_session) {
					$has_session = $login_hash == sha1( $row['proUser'] . $row['proPassword']);	
				}		
			}	
		}
			
		return $has_session;
	}
	

	public function get_available_categories($gender = false) {
		$categories = array(
			'c_oral_sin' => 'Oral Sin Preserv.', 
			'c_anal' => 'Anal',
			'c_trios' => 'Trios',
			'c_libre_term' => 'Sin limite de terminaciones',
			'c_eventos' => 'Compañía en eventos',
			'c_alto_standing' => 'Alto Standing',
			'c_hotel' => 'Servicio a Hotel',
			'c_domicilio' => 'Servicio a domicilio',
			'c_lugar_propio_apartamento_privado' => 'Servicio en lugar propio',
			'c_acondicionado' => 'Lugar acondicionado',
			'c_acabado_cuerpo' => 'Acabado sobre el cuerpo permitido',
			'c_acabado_boca' => 'Acabado en la boca permitido',
			'c_sado_bondage_bsdm' => 'Sado, Bondage (BSDM)',
			'c_cambio_roles' => 'Cambio de Roles',
			'c_rol_activo' => 'Rol activo',
			'c_rol_pasivo' => 'Rol pasivo',
			'c_lluvia_dorada' => 'Lluvia Dorada',
			'c_juguetes' => 'Juguetes eróticos',
			'c_fetichismo' => 'Fetichismo',
			'c_parejas' => 'Atención a parejas',
			'c_atiende_chicas' => 'Atención a chicas',
			'c_atiende_chicos' => 'Atención a chicos',			
			'c_fines_de_semana' => 'Atención fines de semana',
			'c_fantasias' => 'Fantasías',
			'c_despedidas_solteros' => 'Despedidas solteros',
			'c_bailes_eroticos_striptease' => 'Bailes eróticos, Striptease',
			'c_orgias_fiestas' => 'Servicio para orgías y fiestas',
			'c_ojos_claros' => 'Ojos claros',
			'c_ojos_oscuros' => 'Ojos oscuros',
			'c_piel_blanca' => 'Piel blanca',
			'c_piel_negra' => 'Piel negra',
		);
		
		$extras = array();
		
		if ($gender == 'hombre') {
			$extras = array(
				'c_rubios' => 'Rubio',
				'c_morochos_morenos' => 'Moreno',
			);
		} else {
			$extras = array(
				'c_tetas_grandes' => 'Pechos grandes',
				'c_rubias' => 'Rubia',
				'c_pelirrojas' => 'Pelirroja',
				'c_morochas_morenas' => 'Morena',
			);
			
			if ($gender == 'mujer') {
				$extras['c_gordas_gorditas'] = 'Gordita';
			}
		}
		
		$categories = array_merge($categories, $extras);
		
		return $categories;
	}
	
	public function get_keywords($user) {
		$user = strtolower($user);
		
		$keywords_data = false;
		
		$this->db->where('proUser', $user);
		$query = $this->db->get('keywords');
		
		if ($query->num_rows() > 0) {
			$result_data = $query->row_array();
			
			$prefix = 'key';
			$keywords_data = array();
			
			foreach($result_data as $name => $value) {
				$name = str_replace($prefix, '', $name);
				$name[0] = strtolower($name[0]);
				
				$keywords_data[$name] = $value;
			}
		}
		
		return $keywords_data;		
	}
	
	
	public function insert_if_not_in_keywords($user) {
		$this->db->where('proUser', $user);
		$user_in_db = $this->db->get('keywords')->num_rows() > 0;
		
		if ($user_in_db === false) {
			$this->db->set('proUser', $user);
			$this->db->insert('keywords');
		}
	}
	
	
	public function rename_profile($user, $new_user) {
		$user = strtolower($user);
		$new_user = strtolower($new_user);
		
		$related_tables = array(
			'profiles', 'profiles_restores', 'keywords', 'clients', 'logs', 'ads',
			'analytics', 'canalytics', 'wallreplies', 'wallmessages',
			'payments', 'deposits'
		);

		foreach ($related_tables as $table) {
			$this->db->set('proUser', $new_user);
			$this->db->where('proUser', $user);
			$this->db->update($table);
		}

		@rename('userdata/' . $user, 'userdata/' . $new_user);
	}
	
	
	public function disable_profile($user) {
		$this->db->set('proVerified', PROFILE_STATUS_HIDDEN);
		$this->db->where('proUser', $user);
		$this->db->update('profiles');
		
		$profile = $this->get_profile($user);

		$this->fix_group_priorities(
			$profile['country']
		,	$profile['gender']
		,	$profile['accountType']
		,	false
		,	false
		);
	}
	
	public function enable_profile($user, $options=array()) {
		$this->db->set('proVerified', PROFILE_STATUS_VERIFIED);		

		if (isset($options['account_type']))
		{
			$this->db->set('proAccountType', $options['account_type']);
		}

		$this->db->where('proUser', $user);
		$this->db->update('profiles');
		
		$profile      = $this->get_profile($user);
		$country      = $profile['country'];
		$gender       = $profile['gender'];
		$priority     = $profile['priority'];
		$account_type = $profile['accountType'];

		if (isset($options['priority']))
		{
			$priority = $options['priority'];
		}

		if ($priority === 'last')
		{
			$this->make_last_priority($user, $country, $gender, $account_type);
		}
		else
		{
			$this->fix_group_priorities(
				$country
			,	$gender
			,	$account_type
			,	$user
			,	$priority
			);
		}
	}

	public function change_profile_panel_message($user, $new_message)
	{
		$this->db->set('proPanelMessage', $new_message);
		$this->db->where('proUser', $user);
		$this->db->limit(1);
		$this->db->update('profiles');
	}
	

	public function make_last_priority($user, $country, $gender, $account_type=null)
	{
		$last_priority = $this->get_positions_count($country, $gender, $account_type);
		//$last_priority = $max_priority + 1;

		$this->db->where('proUser', $user);
		$this->db->set('proPriority', $last_priority);
		$this->db->limit(1);
		$this->db->update('profiles');
		return $last_priority;
	}


	public function get_positions_count($country, $gender, $account_type=null) {
		$this->db->where('proCountry', $country);
		
		if (stripos($gender, 'duo') !== false) {
			$this->db->like('proGender', 'duo', 'after');
		} else {
			$this->db->where('proGender', $gender);
		}
		
		$this->db->where('proDeleted', PROFILE_NOT_DELETED);
		$this->db->where('proVerified', PROFILE_STATUS_VERIFIED);

		if ($account_type !== null)
		{
			$this->db->where('proAccountType', $account_type);
		}

		return $this->db->get('profiles')->num_rows();
	}
	
	
	public function fix_group_priorities($country, $gender, $account_type=null, $user = false, $new_position = false) {
		
		if (stripos($gender, 'duo') !== false) {
			$gender = 'duo';
		}
		
		if ($user) {
			// FIX ALL BROKEN POSITIONS
			$this->fix_group_priorities(
				$country, $gender, $account_type, false, false
			);

			$this->make_last_priority($user, $country, $gender, $account_type);
		}

		$num_rows = $this->get_positions_count(
			$country, $gender, $account_type
		);
				
		if ($user && ($new_position === 'last' || $new_position == 0) ) {
			$new_position = $num_rows;
			
		} else {
			if (!$user) {
				$new_position = 0;
			}
		
			$sql_1 = 'SET @priorityPosition := '.$new_position.';';
			$this->db->query($sql_1);

			$sql_2  = "UPDATE profiles p INNER JOIN ( ";
			$sql_2 .= "SELECT @priorityPosition := @priorityPosition + 1 AS newProPriority, proUser ";
			$sql_2 .= "FROM profiles WHERE proVerified = ".PROFILE_STATUS_VERIFIED." AND proDeleted=" .PROFILE_NOT_DELETED. " ";
			$sql_2 .= "AND proCountry = '".$country."' AND proGender LIKE '".$gender."%' ";

			if ($account_type !== null)
			{
				$sql_2 .= "AND proAccountType = " . $account_type . ' ';
			}

			if ($user) {
				$sql_2 .= "AND proPriority >= ".$new_position." AND proUser != '".$user."' ";
			}

			$sql_2 .= "ORDER BY proPriority ASC ";
			$sql_2 .= ") as np on p.proUser = np.proUser ";
			$sql_2 .= "SET p.proPriority = np.newProPriority;";

			$this->db->query($sql_2);
		}
		
		if ($user) {
			$this->db->set('proPriority', $new_position);
			$this->db->where('proUser', $user);
			$this->db->update('profiles');
		}

		return array($new_position, $num_rows);
	}
	
	
	public function get_current_restore_code($user) {
		$this->db->where('proUser', $user);
		$this->db->where('proRestoreExpires >', date('Y-m-d H:i:s'));
		$res = $this->db->get('profiles_restores');
		
		if ($res->num_rows() > 0) {
			
			$row = $res->row();
			
			return array(
				'user' => $row->proUser,
				'restore_code' => $row->proRestoreCode,
				'restore_hash' => $row->proRestoreHash,
				'restore_created' => $row->proRestoreCreated,
				'restore_expires' => $row->proRestoreExpires,
			);
		}
		
		return false;
	}
	
	public function delete_restore_code($user) {
		$this->db->where('proUser', $user);
		$this->db->delete('profiles_restores');
	}
		
	public function create_restore_code($user, $expire_days = 4 ) {
		$randomString = StringHelper::random_string('alnum', 6);

		$new_restore_code = str_replace('O', 'J', str_replace('0', 'P', strtoupper($randomString)));
		$new_restore_hash = substr(md5($user), 0, strlen($user)) . sha1($new_restore_code);

		$current_date = date('Y-m-d H:i:s');
		$days_ahead_date = date('Y-m-d H:i:s', strtotime('+'.$expire_days.' day'));						

		$this->db->set('proUser', $user);
		$this->db->set('proRestoreCode', $new_restore_code);
		$this->db->set('proRestoreHash', $new_restore_hash);
		$this->db->set('proRestoreExpires', "'" . $days_ahead_date . "'", false);
		$this->db->set('proRestoreCreated', "'" . $current_date . "'", false);		
		
		if ($this->db->insert('profiles_restores')) {
			return array(
				"restore_code" => $new_restore_code,
				"restore_hash" => $new_restore_hash,
			); 
		} else {
			return false;
		}
		
	}
	
	public function get_related_profiles($profile) {
		$profiles_found = array();
		
		if (!empty($profile["phone"])) {
			$phone_numbers = $this->phoneparser->find_numbers($profile["phone"], $profile["country"]);
		}
		
		$has_phone = isset($phone_numbers) && count($phone_numbers) > 0;
		
		$has_country = !empty($profile["country"]);
		$has_email = !empty($profile["email"]);
		
		if ($has_country && ($has_phone || $has_email)) {
			$fields = array(
				'profile_group' => 'pg.name as group_name, pg.id as group_id',
				'profiles'      => 'p.*',
			); 

			$this->db->select(implode(',', $fields));
			
			$this->db->join('clients c', 'c.proUser = p.proUser');
			$this->db->join('profile_group pg', 'c.profile_group_id = pg.id', 'left outer');

			if ($has_email) {
				$this->db->like( 'p.proEmail', $profile["email"] );
			}

			if ($has_phone) {	
				foreach ($phone_numbers as $phone) {
					$this->db->or_like('proNumericPhones', $phone);
					$this->db->or_like('c.cliPhone', $phone);
				}
			}

			$this->db->where('proCountry', $profile["country"]);

			$this->db->order_by('proModifiedDate desc');
			
			$res = $this->db->get("profiles p");
			
			if ($res->num_rows()) {
				$profiles_found = $res->result_array();
			}
		}
		
		return $profiles_found;
	}

	
	public function restore_profile_status($user, $clear_panel_message=true)
	{
		$profile = $this->get_profile($user);
			
		if (isset($profile['verified']) && $profile['verified'] == PROFILE_STATUS_HIDDEN) {
			$this->enable_profile($user);				
		}
		
		if ($clear_panel_message) {
			$this->change_profile_panel_message($user, '');
		}
	}

}

/* end of file */
