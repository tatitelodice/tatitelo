<?php

namespace Melquilabs\Pumbate\Models;

class ContactModel
{
	
	private $config;
	
	protected $phones;
	protected $mails;
	
	protected $agent_mails;
	
	
	public function __construct($config) {
		$this->config      = $config;
		// specific properties
		$this->phones      = $config->item('contact_phones');
		$this->mails       = $config->item('contact_emails');
		$this->agent_mails = $config->item('agent_emails');
	}
	
	
	public function get_mail($country=false) {
		$found = $this->search_mails($this->mails, $country);
		
		if (is_array($found) && count($found)) {
			$found_mails = array_values($found);
			$found = $found_mails[0];
		}
		
		return $found;
	}
	
	
	public function get_agent_mails($country=false) {
		return $this->search_mails($this->agent_mails, $country);
	}
	
	
	protected function search_mails($mails_list, $country=false) {
		$found = false;
		
		if (isset($mails_list['default'])) {
			$found = $mails_list['default'];
		}
		
		if ($country && isset($mails_list[$country])) {
			$found = $mails_list[$country];
		}
		
		return $found;
	} 
	
	
	public function get_phone($country, $tag=false, $fallback=true) {
		$found = false;
		
		if (isset($this->phones[$country])) {
			
			if (is_array($this->phones[$country])) {
				if ($tag && isset($this->phones[$country][$tag])){
					$found = $this->phones[$country][$tag];
				} elseif ($fallback && isset($this->phones[$country]['default'])) {
					$found = $this->phones[$country]['default'];
				}
			} else {
				$found = $this->phones[$country];
			}
		}
		
		return $found;
	}
	
	
	public function get_agent_mail() {
		return $this->get_mail();
	}
	
	public function get_agent_phone() {
		return $this->get_phone('uy');
	}
	
	public function get_admin_mail() {
		return 'sgpence1990@gmail.com';
	}
	
}
	
/* end of class */
