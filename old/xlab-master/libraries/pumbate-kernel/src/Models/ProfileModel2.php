<?php

namespace Melquilabs\Pumbate\Models;


class ProfileModel2
{
	private $camel_to_under = array(
		'createdDate'      => 'created_date',
		'modifiedDate'     => 'modified_date',
		'accountType'      => 'account_type',
		'pictureType'      => 'picture_type',
		'verifiedPictures' => 'verified_pictures',
		'absentMessage'    => 'absent_message',
		'panelMessage'     => 'panel_message',
		'numericPhones'    => 'numeric_phones',
	);
	
	protected $db;
    
	public function __construct($db)
	{
		$this->db = $db;
	}
	
	public function get_all($params) {
		$profiles = array();
		
		if (!empty($params['fields'])) {
			if (!is_array($params['fields'])) {
				$params['fields'] = explode(',', $params['fields']);
			}
			$this->db->select($params['fields']);
		}
		
		if (!empty($params['include_administration'])) {
			$this->db->join('clients', 'clients.proUser = profiles.proUser');
			$this->db->join('profile_group', 'clients.profile_group_id = profile_group.id', 'left outer');
		}
		
		if (!empty($params['where']) && is_array($params['where'])) {
			foreach ($params['where'] as $field => $value) {
				if (is_array($value)) {
					$this->db->where_in($field, $value);
				} else {
					$this->db->where($field, $value);
				}
			}
		}
		
		if (!empty($params['order_by'])) {
			$this->db->order_by($params['order_by']);
		}
		
		$resultset = $this->db->get('profiles');
		
		if ($resultset->num_rows()) {
			
			foreach($resultset->result_array() as $result_row) {
				$profile = array();
				
				foreach($result_row as $name => $value) {
					
					if (!empty($params['force_underscore'])) {
						$name = $this->underscore_it($name);
					}
					
					$profile[$name] = $value;
				}
				
				$profiles[] = $profile;
			}
		}
		
		return $profiles;
	}
	
	
	private function underscore_it($name) {
		$name = str_replace('pro', '', $name);
		$name[0] = strtolower($name[0]);

		if (isset($this->camel_to_under[$name])) {
			$name = $this->camel_to_under[$name];
		}
		
		return $name;
	}
	
	
}

/* end of file */
