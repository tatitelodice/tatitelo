<?php

namespace Melquilabs\Pumbate\Models;


class BannerModel
{
	protected $table_name = "banner";
	
	protected $enabled_enum = array(
		1 => 'sí', 0 => 'no',
	);
	
	protected $category_enum = array(
		'main' => 'principal'
	);
	
	protected $device_visibility_enum = array(
		'desktop'           => 'PC',
		'tablet'            => 'Tablet',
		'mobile'            => 'Celular'   
	);
	
	protected $db;
	
	public function __construct($db)
	{
		$this->db = $db;
	}

	public function get_enabled_enum() {
		return $this->enabled_enum;
	}
	
	public function get_category_enum() {
		return $this->category_enum;
	}
	
	public function get_device_visibility_enum() {
		return $this->device_visibility_enum;
	}
		
	public function get_all_banner($params=array()) {
		$banners = array();
		
		if ($params) {
			$this->_load_fields($params);
			$this->_load_conditions($params);
			$this->_load_limits($params);
		}
		
		$resultset = $this->db->get($this->table_name);
		
		if ($resultset->num_rows()) {
			$banners = $resultset->result_array();
			
			foreach ($banners as &$banner) {
				$banner['settings'] = json_decode($banner['settings'], true);
			}
		}
		
		return $banners;
	}
	
	
	public function count_all_banner($params=array()) {
		$this->_load_conditions($params);
		return $this->db->count_all_results($this->table_name);		
	}
	
	
	
	public function get_banner($id) {
		$banner_data = false;
		
		$this->db->where('id', $id);
		$this->db->limit(1);
		
		$query = $this->db->get($this->table_name);
		
		if ($query->num_rows() > 0) {
			$banner_data = $query->row_array();
			$banner_data['settings'] = json_decode($banner_data['settings'], true);
		}
		
		return $banner_data;
	}
	
	
	protected function _load_set_fields($data) {
		$this->db->set('country', $data['country']);
		$this->db->set('category', $data['category']);
		
		$this->db->set('title', $data['title']);
		$this->db->set('description', $data['description']);
		$this->db->set('url', $data['url']);
		
		$this->db->set('enabled', $data['enabled']);
				
		if (isset($data['settings']) && is_array($data['settings'])) {
			$settings_json = json_encode($data['settings']);

			if ($settings_json !== false) {
				$this->db->set('settings', $settings_json);
			}
		}		

		if (isset($data['position'])) {
			$this->db->set('position', $data['position']);
		}
	}
	
	
	public function create_banner($data) {
		$id = false;
		
		if (!isset($data['settings'])) {
			$data['settings'] = array();
		}
		
		$this->_load_set_fields($data);
		
		if ($this->db->insert($this->table_name)) {
			$id = $this->db->insert_id();
		}
		
		return $id;
	} 
	
	
	public function update_banner($id, $data) {
		$this->_load_set_fields($data);
		
		$this->db->where('id', $id);
		$success = $this->db->update($this->table_name);
		
		return $success;
	}
	
	
	
	public function delete_banner($id) {
		$this->db->where('id', $id);
		$this->db->set("deleted", 1);
		return $this->db->update($this->table_name);
	}
	
	
	protected function _load_fields($params) {
		if (!empty($params['fields'])) {
			if (!is_array($params['fields'])) {
				$params['fields'] = explode(',', $params['fields']);
			}
			$this->db->select($params['fields']);
		}
	}
	
	protected function _load_conditions($params) {
		
		if (!empty($params['where']) && is_array($params['where'])) {
			foreach ($params['where'] as $field => $value) {
				if (is_array($value)) {
					$this->db->where_in($field, $value);
				} else {
					$this->db->where($field, $value);
				}
			}
		}
		
		if (!empty($params['order_by'])) {
			$this->db->order_by($params['order_by']);
		}
	}
	
	protected function _load_limits($params) {
		if (!empty($params['limit'])) {
			if (!empty($params['offset'])) {
				$this->db->limit($params['limit'], $params['offset']);
			} else {
				$this->db->limit($params['limit']);
			}
		}
	}
	
}

/* end of file */
