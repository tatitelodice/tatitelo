<?php

use Melquilabs\Pumbate\Models;
use Symfony\Component\DependencyInjection\Reference as Ref;

$container
    ->register(Models\AdminModel::class, Models\AdminModel::class)
    ->setArguments([ new Ref('db'), new Ref('session') ])
;

$container
    ->register(Models\AdministratorAclModel::class, Models\AdministratorAclModel::class)
    ->setArguments([ new Ref('db') ])
;

$container
    ->register(Models\AnalyticsModel::class, Models\AnalyticsModel::class)
    ->setArguments([ new Ref('db') ])
;

$container
    ->register(Models\BannerModel::class, Models\BannerModel::class)
    ->setArguments([ new Ref('db') ])
;

$container
    ->register(Models\ContactModel::class, Models\ContactModel::class)
    ->setArguments([ new Ref('config') ])
;

$container
    ->register(Models\CountryModel::class, Models\CountryModel::class)
    //->setArguments([ new Ref('db') ])
;

$container
    ->register(Models\GenderModel::class, Models\GenderModel::class)
    //->setArguments([ new Ref('db') ])
;

$container
    ->register(Models\BlacklistModel::class, Models\BlacklistModel::class)
    ->setArguments([ new Ref('db') ])
;

$container
    ->register(Models\ContentsModel::class, Models\ContentsModel::class)
    ->setArguments([ new Ref('db') ])
;

$container
    ->register(Models\CustomerMessageModel::class, Models\CustomerMessageModel::class)
    ->setArguments([ new Ref('db') ])
;

$container
    ->register(Models\DepositModel::class, Models\DepositModel::class)
    ->setArguments([ new Ref('db'), new Ref(Models\ProfileModel::class) ])
;

$container
    ->register(Models\DepositNotificationModel::class, Models\DepositNotificationModel::class)
    ->setArguments([ new Ref('db') ])
;

$container
    ->register(Models\FlashSessionModel::class, Models\FlashSessionModel::class)
    ->setArguments([ 
        new Ref('db'), new Ref('session'), new Ref('input'), new Ref('temp_model') 
    ])
;

$container
    ->register(Models\KeywordsModel::class, Models\KeywordsModel::class)
    ->setArguments([ new Ref('db') ])
;

$container
    ->register(Models\LocationModel::class, Models\LocationModel::class)
    ->setArguments([ new Ref('db') ])
;

$container
    ->register(Models\LogModel::class, Models\LogModel::class)
    ->setArguments([ new Ref('db'), new Ref('input') ])
;

$container->register(Models\MessageTemplatesModel::class, Models\MessageTemplatesModel::class)
    ->setArguments([ new Ref('db') ])
;

$container->register(Models\PaymentAccountModel::class, Models\PaymentAccountModel::class)
    ->setArguments([ new Ref('db') ])
;

$container->register(Models\PaymentsModel::class, Models\PaymentsModel::class)
    ->setArguments([ 
        new Ref('db')
    ,   new Ref(Models\ProfileModel::class)
    ,   new Ref(Models\DepositModel::class) 
    ,   new Ref(Models\PricingModel::class)
    ,   new Ref(Models\PaymentAccountModel::class)
    ])
;

$container->register(Models\PricingModel::class, Models\PricingModel::class)
    ->setArguments([ new Ref('db'), new Ref('config') ])
;


$container->register(Models\ProfileGroupModel::class, Models\ProfileGroupModel::class)
    ->setArguments([ new Ref('db') ])
;


$container->register(Models\ProfileModel::class, Models\ProfileModel::class)
    ->setArguments([ new Ref('db'), new Ref('session'), new Ref('phoneparser'), new Ref('admin_model') ])
;

$container->register(Models\ProfileModel2::class, Models\ProfileModel2::class)
    ->setArguments([ new Ref('db'), new Ref('session') ])
;

$container->register(Models\ProfileModelExt::class, Models\ProfileModelExt::class)
    ->setArguments([ new Ref('db'), new Ref('session') ])
;

$container->register(Models\ProfileSiblingModel::class, Models\ProfileSiblingModel::class)
    ->setArguments([ new Ref('db'), new Ref('session') ])
;

$container
    ->register(Models\SecurityModel::class, Models\SecurityModel::class)
    ->setArguments([ new Ref('db'), new Ref('input') ])
;

$container
    ->register(Models\TempModel::class, Models\TempModel::class)
    ->setArguments([ new Ref('db') ])
;

// Old libraries
$container->setAlias('contact', Models\ContactModel::class);
$container->setAlias('country', Models\CountryModel::class);
$container->setAlias('gender', Models\GenderModel::class);
// Old models
$container->setAlias('admin_model', Models\AdminModel::class);
$container->setAlias('administrator_acl_model', Models\AdministratorAclModel::class);
$container->setAlias('analytics_model', Models\AnalyticsModel::class);
$container->setAlias('banner_model', Models\BannerModel::class);
$container->setAlias('blacklist_model', Models\BlacklistModel::class);
$container->setAlias('contents_model', Models\ContentsModel::class);
$container->setAlias('customer_message_model', Models\CustomerMessageModel::class);
$container->setAlias('deposit_model', Models\DepositModel::class);
$container->setAlias('deposit_notification_model', Models\DepositNotificationModel::class);
$container->setAlias('flashsession_model', Models\FlashSessionModel::class);
$container->setAlias('keywords_model', Models\KeywordsModel::class);
$container->setAlias('log_model', Models\LogModel::class);
$container->setAlias('message_templates_model', Models\MessageTemplatesModel::class);
$container->setAlias('payment_account_model', Models\PaymentAccountModel::class);
$container->setAlias('payments_model', Models\PaymentsModel::class);
$container->setAlias('pricing_model', Models\PricingModel::class);
$container->setAlias('profile_group_model', Models\ProfileGroupModel::class);
$container->setAlias('profile_model', Models\ProfileModel::class);
$container->setAlias('profile_model2', Models\ProfileModel2::class);
$container->setAlias('profile_model_ext', Models\ProfileModelExt::class);
$container->setAlias('profile_sibling_model', Models\ProfileSiblingModel::class);
$container->setAlias('temp_model', Models\TempModel::class);

