<?php

use Symfony\Component\DependencyInjection\Reference as Ref;

use Melquilabs\Pumbate;

$def = function ($class, $args=false) use ($container)
{
    $svc = $container->register($class, $class);
    if (is_array($args)) $svc = $svc->setArguments($args);
    return $svc;
};

$alias = function ($serviceName, $alias) use ($container)
{
    return $container->setAlias($serviceName, $alias);
};

$def( Pumbate\Api\PumbateApiClient::class );

$def( Pumbate\Services\Geolocation\LocationListService::class, [
    new Ref(Pumbate\Models\LocationModel::class)
,   new Ref('redis')
]);


$def( Pumbate\Services\Admin\AdminPaymentAccountService::class, [
    new Ref('payment_account_model'), new Ref(Pumbate\Services\Admin\AdminPermissionService::class)
]);

$def( Pumbate\Services\Admin\AdminPermissionService::class, [
    new Ref('admin_model'), new Ref(Pumbate\Models\AdministratorAclModel::class)
]);

$def( Pumbate\Services\AdsBuilder::class );
$def( Pumbate\Services\Fingerprint\HumanUserAgent::class );
$def( Pumbate\Services\Content\Keywords::class );
$def( Pumbate\Services\Image\ImageHandler::class );
$def( Pumbate\Services\Profile\ProfileUploadHandler::class );
$def( Pumbate\Services\Contact\ContactEmail::class );

$def( Pumbate\Services\Content\SexyTitleGenerator::class );
$def( Pumbate\Services\Contact\ContactPhone::class );
$def( Pumbate\Services\Redis\RedisClient::class );

$def( Pumbate\Services\WebFront\ProfileGalleryService::class, [
    new Ref('db')
,   new Ref(Pumbate\Services\Redis\RedisClient::class)
,   new Ref('config')
,   new Ref('profile_model')
,   new Ref('asset')
,   new Ref('keywords')
,   new Ref('country')
,   new Ref('gender')
]);

$def( Pumbate\Services\WebFront\BannerService::class );

$def( Pumbate\Services\Profile\ProfilePublicationStatusService::class );

$def( Pumbate\Services\Profile\ProfileBillingStatusService::class, [
    new Ref('deposit_model')
,   new Ref('log_model')
,   new Ref('payments_model')
,   new Ref('pricing_model')
]);

$def( Pumbate\Services\Profile\ProfileAccountService::class );
$def( Pumbate\Services\Profile\ProfileRegisterService::class );

$def( Pumbate\Services\Profile\ProfilePanelService::class, [
    new Ref('config'), new Ref('db') 
]);

$def( Pumbate\Services\Profile\ProfilePicturesService::class, [ 
    new Ref('config'), new Ref('db') 
]);

$def( Pumbate\Services\Upload\UploadHandler::class );
$def( Pumbate\Services\Marketing\Analytics::class );
$def( Pumbate\Services\Contact\PhoneParser::class );
$def( Pumbate\Services\Export\SpreadsheetExporter::class );

$def( Pumbate\Helpers\DateTransformHelper::class, [
    new Ref('config'), new Ref('db') 
]);

$def( Pumbate\Services\Messaging\MailNotification::class, [
    new Ref('config')
]);

$def( Pumbate\Services\Billing\PaymentAccountAssignService::class, [
    new Ref('db'), new Ref('payment_account_model')
]);

$def( Pumbate\Services\Billing\DepositQueryService::class, [
    new Ref('deposit_model'), new Ref('payment_account_model')
]);

$def( Pumbate\Services\Billing\DepositMatchService::class, [
    new Ref('deposit_model'), new Ref('payments_model')
,   new Ref('profile_model'), new Ref('log_model')
]);

$def( Pumbate\Services\Billing\DepositNotificationService::class, [
    new Ref('profile_model'), new Ref('log_model')
,   new Ref('customer_message_model'), new Ref('deposit_model')
,   new Ref('deposit_notification_model')
]);

$def( Pumbate\Services\Media\Asset::class );

$def( Pumbate\Services\Security\SecurityService::class, [
    new Ref(Pumbate\Models\TempModel::class), new Ref('input')
]);

/*
    Service Aliases
*/
$alias('admin/admin_payment_account_service', Pumbate\Services\Admin\AdminPaymentAccountService::class );
$alias('admin/admin_permission_service', Pumbate\Services\Admin\AdminPermissionService::class );
$alias('adsbuilder', Pumbate\Services\AdsBuilder::class );
$alias('human_useragent', Pumbate\Services\Fingerprint\HumanUserAgent::class );
$alias('keywords', Pumbate\Services\Content\Keywords::class );
$alias('gallery', Pumbate\Services\Image\ImageHandler::class );
//$alias('country', Models\CountryModel::class);
$alias('upload_handler_profile', Pumbate\Services\Profile\ProfileUploadHandler::class );
$alias('pumbatemail', Pumbate\Services\Contact\ContactEmail::class );
$alias('pumbate_api', Pumbate\Api\PumbateApiClient::class );
//$alias('contact', Models\ContactModel::class);
$alias('sexytitle', Pumbate\Services\Content\SexyTitleGenerator::class );
$alias('pumbatephone', Pumbate\Services\Contact\ContactPhone::class );
$alias('webfront/site_cache', Pumbate\Redis\RedisClient::class );
$alias('webfront/profile_gallery_service', Pumbate\Services\WebFront\ProfileGalleryService::class );
$alias('webfront/banner_service', Pumbate\Services\WebFront\BannerService::class );
$alias('profile/profile_publication_status_service', Pumbate\Services\Profile\ProfilePublicationStatusService::class );
$alias('profile/profile_billing_status_service', Pumbate\Services\Profile\ProfileBillingStatusService::class );
$alias('profile/profile_account_service', Pumbate\Services\Profile\ProfileAccountService::class );
$alias('profile/profile_register_service', Pumbate\Services\Profile\ProfileRegisterService::class );
$alias('profile/profile_panel_service', Pumbate\Services\Profile\ProfilePanelService::class );
$alias('profile/profile_pictures_service', Pumbate\Services\Profile\ProfilePicturesService::class);
$alias('upload_handler', Pumbate\Services\Upload\UploadHandler::class );
$alias('analytics', Pumbate\Services\Marketing\Analytics::class );
$alias('phoneparser', Pumbate\Services\Contact\PhoneParser::class );
$alias('export/spreadsheet', Pumbate\Services\Export\SpreadsheetExporter::class );
$alias('sdate', Pumbate\Helpers\DateTransformHelper::class );
$alias('mail_notification', Pumbate\Services\Messaging\MailNotification::class );
$alias('billing/payment_account_assign_service', Pumbate\Services\Billing\PaymentAccountAssignService::class );
$alias('billing/deposit_query_service', Pumbate\Services\Billing\DepositQueryService::class );
$alias('billing/deposit_match_service', Pumbate\Services\Billing\DepositMatchService::class );
$alias('billing/deposit_notification_service', Pumbate\Services\Billing\DepositNotificationService::class );
//$alias('gender', Models\GenderModel::class);
$alias('asset', Pumbate\Services\Media\Asset::class );
$alias('security_model', Pumbate\Services\Security\SecurityService::class);

/* eof */