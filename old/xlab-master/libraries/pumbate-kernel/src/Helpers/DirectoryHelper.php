<?php

namespace Melquilabs\Pumbate\Helpers;

class DirectoryHelper
{
    /**
     * Create a Directory Map
     *
     * Reads the specified directory and builds an array
     * representation of it.  Sub-folders contained with the
     * directory will be mapped as well.
     *
     * @access	public
     * @param	string	path to source
     * @param	int		depth of directories to traverse (0 = fully recursive, 1 = current dir, etc)
     * @return	array
     */

	public static function directory_map($source_dir, $directory_depth = 0, $hidden = FALSE)
	{
		if ($fp = @opendir($source_dir))
		{
			$filedata	= array();
			$new_depth	= $directory_depth - 1;
			$source_dir	= rtrim($source_dir, DIRECTORY_SEPARATOR).DIRECTORY_SEPARATOR;

			while (FALSE !== ($file = readdir($fp)))
			{
				// Remove '.', '..', and hidden files [optional]
				if ( ! trim($file, '.') OR ($hidden == FALSE && $file[0] == '.'))
				{
					continue;
				}

				if (($directory_depth < 1 OR $new_depth > 0) && @is_dir($source_dir.$file))
				{
					$filedata[$file] = self::directory_map($source_dir.$file.DIRECTORY_SEPARATOR, $new_depth, $hidden);
				}
				else
				{
					$filedata[] = $file;
				}
			}

			closedir($fp);
			return $filedata;
		}

		return FALSE;
	}

    /**
     * Copy a whole Directory
     *
     * Copy a directory recrusively ( all file and directories inside it )
     *
     * @access    public
     * @param    string    path to source dir
     * @param    string    path to destination dir
     * @return    array
     */  
	public static function directory_copy($srcdir, $dstdir)
    {
        //preparing the paths
        $srcdir=rtrim($srcdir,'/');
        $dstdir=rtrim($dstdir,'/');

        //creating the destination directory
        if(!is_dir($dstdir))mkdir($dstdir, 0777, true);
        
        //Mapping the directory
        $dir_map = self::directory_map($srcdir);

        foreach($dir_map as $object_key=>$object_value)
        {
            if(is_numeric($object_key))
                copy($srcdir.'/'.$object_value,$dstdir.'/'.$object_value);//This is a File not a directory
            else
                self::directory_copy($srcdir.'/'.$object_key,$dstdir.'/'.$object_key);//this is a directory
        }
    }
	
    public static function directory_public($srcdir, $chmod = 0777)
    {
		//preparing the paths
        $srcdir=rtrim($srcdir,'/');
		@chmod($srcdir, $chmod);
		
        //Mapping the directory
        $dir_map = self::directory_map($srcdir);

        foreach($dir_map as $object_key=>$object_value)
        {
            if(is_numeric($object_key))
				@chmod($srcdir.'/'.$object_value, $chmod);//This is a File not a directory
            else {
                self::directory_public($srcdir.'/'.$object_key, $chmod);//this is a directory
			}
        }
	}
}

/* End of file  */