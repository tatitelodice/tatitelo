<?php

namespace Melquilabs\Pumbate\Helpers;

class DateTransformHelper {

    public static function formatoYMDaLiteralCorta ($fechaYMD)
    {
        list($anio, $mes, $dia) = explode("-", $fechaYMD);

        $meses = array ("01" => "enero", "02" => "febrero", "03" => "marzo", "04" => "abril", "05" => "mayo", "06" => "junio", "07" => "julio", "08" => "agosto", "09" => "setiembre", "10" => "octubre", "11" =>"noviembre", "12" =>"diciembre");
        $mes = $meses[$mes];

        $dia = intval($dia);
        
        return "$dia de $mes, $anio";
            
        }

        
    public static function diaLiteral($numero)
    {
        $dias = array( "Domingo", "Lunes", "Martes", "Miércoles", "Jueves", "Viernes", "Sábado");
        
        if (isset($dias[$numero]))
            return $dias[$numero];
        else {
            return 'Todos los días';
        }
    }
        

    public static function formatoYMDaLiteral ($fechaYMD, $hora = false) {
        $resultado = '';
        
        if (preg_match('!(\d\d\d\d)-(\d+)-(\d+)!', $fechaYMD, $matches)) {
            $anio = $matches[1];
            $mes = $matches[2];
            $dia = $matches[3];
            
            $meses = array ("01" => "enero", "02" => "febrero", "03" => "marzo", "04" => "abril", "05" => "mayo", "06" => "junio", "07" => "julio", "08" => "agosto", "09" => "setiembre", "10" => "octubre", "11" =>"noviembre", "12" =>"diciembre");
            $dias = array( "Domingo", "Lunes", "Martes", "Miércoles", "Jueves", "Viernes", "Sábado");


            $diaSemana =  $dias[date("w", mktime(0, 0, 0, $mes, $dia, $anio))];

            $mes = $meses[$mes];

            $dia = intval($dia);

            $resultado = "$diaSemana, $dia de $mes, $anio";		
        }
        
        if ($hora && preg_match('!\d+:\d+!', $fechaYMD, $matches)) {
            $resultado .= ' a las ' . $matches[0];
        }
        
        return $resultado;

    }

    
    public static function formatoDMYaYMD ($fechaDMY) {

        if (preg_match("!(\d+)(?:\.|/|-)(\d+)(?:\.|/|-)(\d+)!", $fechaDMY, $match)) {

        $anio = $match[3];
        $mes = (strlen($match[2]) == 1)? "0".$match[2] : $match[2];
        $dia = (strlen($match[1]) == 1)? "0".$match[1] : $match[1];
        
        return "$anio-$mes-$dia";

        } else {
        return date("Y-m-d");
        }

    }
    
    
    public static function formatoYMDaDMY ($fechaYMD) 
    {
        if (preg_match("!(\d+)(?:\.|/|-)(\d+)(?:\.|/|-)(\d+)!", $fechaYMD, $match)) {

        $anio = $match[1];
        $mes = (strlen($match[2]) == 1)? "0".$match[2] : $match[2];
        $dia = (strlen($match[3]) == 1)? "0".$match[3] : $match[3];

        return "$dia/$mes/$anio";

        } else {
        return date("d/m/Y");
        }

    }

    public static function formatoYMDaMDY ($fechaYMD)
    {
        if (preg_match("!(\d+)(?:\.|/|-)(\d+)(?:\.|/|-)(\d+)!", $fechaDMY, $match))
            {

            $anio = $match[1];
            $mes = (strlen($match[2]) == 1)? "0".$match[2] : $match[2];
            $dia = (strlen($match[3]) == 1)? "0".$match[3] : $match[3];

            return "$mes-$dia-$anio";

        } else {
            return date("m-d-y");
        }
    }
}

