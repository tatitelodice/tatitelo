GRANT ALL PRIVILEGES ON {{ database_name }}.* TO '{{ database_user }}'@'%' identified by '{{ database_password }}';
FLUSH PRIVILEGES;