CREATE USER IF NOT EXISTS {{ database_user }} identified by '{{ database_password }}';
GRANT ALL PRIVILEGES ON {{ database_name }}.* TO '{{ database_user }}'@'localhost' identified by '{{ database_password }}';
GRANT ALL PRIVILEGES ON {{ database_name }}.* TO '{{ database_user }}'@'127.0.0.1' identified by '{{ database_password }}';
FLUSH PRIVILEGES;