# Letsencrypt certificates setup

## Initial setup

```sh
certbot --nginx -d example.com
```

```sh
certbot renew
```

## Cron setup

0 */12 * * * root test -x /usr/bin/certbot && perl -e 'sleep int(rand(3600))' && certbot -q renew --renew-hook "/etc/init.d/nginx reload"
