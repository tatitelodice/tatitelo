#  User Setup: wsadmin

## User Creation

```sh
# Create user
adduser wsadmin
```

## Sudo permissions and access

```sh
# Add to sudo group
usermod -aG sudo wsadmin
```

```sh
# Edit file /etc/sudoers
wsadmin ALL=(ALL) NOPASSWD:ALL
%sudo   ALL=(ALL) NOPASSWD:ALL
```

## WebServer files permissions

*Requires* http server (i.e: nginx) and /var/www dir

```sh
# Add to /var/www privileges group
HTTPDUSER=`ps axo user,comm | grep -E '[a]pache|[h]ttpd|[_]www|[w]ww-data|[n]ginx' | grep -v root | head -1 | cut -d\  -f1`

usermod -aG $HTTPDUSER wsadmin
sudo setfacl -R -m u:"$HTTPDUSER":rwX -m u:wsadmin:rwX "/var/www"
sudo setfacl -dR -m u:"$HTTPDUSER":rwX -m u:wsadmin:rwX "/var/www"
```