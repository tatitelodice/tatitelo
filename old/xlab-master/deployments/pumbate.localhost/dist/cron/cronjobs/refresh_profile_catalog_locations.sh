#!/bin/bash
APP_DIR="/var/www/localhost";
APP_CRON_LOG_DIR="$APP_DIR/var/cron";
APP_ENV_FILE="$APP_DIR/environment/production.env"
SERVICE_BIN_PATH="$APP_DIR/services/profile-catalog-cache/bin/profile-catalog-cache";
SERVICE_COMMAND="refresh:locations"

mkdir -p $APP_CRON_LOG_DIR;

function cronlog {
    log_format="[%s]: %s";
    date_now="$(date "+%Y-%m-%d %H:%M:%S")";
    printf "$log_format\n" "$date_now" "$*" | tee -a $APP_CRON_LOG_DIR/log.txt;
}

JOB_NAME=$(basename $0 .sh);
JOB_OUT_FILE="$APP_CRON_LOG_DIR/$JOB_NAME.txt";

cronlog Started $JOB_NAME

/usr/bin/env APP_ENV_FILE=$APP_ENV_FILE php $SERVICE_BIN_PATH $SERVICE_COMMAND > $JOB_OUT_FILE 2>&1;

cronlog Finished $JOB_NAME