# DISABLE WWW REDIRECT
#server {
#    server_name www.localhost;
#    return 301 $scheme://localhost$request_uri;
#}

server {
    server_name localhost;

    listen 12081 default_server;
    listen [::]:12081 default_server;

    # hide server version
    server_tokens off;

    root /var/www/localhost/web/webadmin/public;
    index index.php index.html index.htm;

    location / {
        try_files $uri /index.php$is_args$args;
    }

    location ~ ^/index\.php(/|$) {
        include fastcgi_params;

        # When you are using symlinks to link the document root to the
        # current version of your application, you should pass the real
        # application path instead of the path to the symlink to PHP
        # FPM.
        # Otherwise, PHP's OPcache may not properly detect changes to
        # your PHP files (see https://github.com/zendtech/ZendOptimizerPlus/issues/126
        # for more information).
        fastcgi_param  SCRIPT_FILENAME  $realpath_root$fastcgi_script_name;
        fastcgi_param DOCUMENT_ROOT $realpath_root;

        # CUSTOM $_SERVER VARS
        fastcgi_param APP_BASE_URL 'http://localhost:10080/padmin/';
        fastcgi_param APP_ENV dev;
        #fastcgi_param APP_ENV_FILE /var/www/localhost/web/website/development.env.php;

        fastcgi_pass unix:/run/php/php7.2-fpm.sock;
        fastcgi_split_path_info ^(.+\.php)(/.*)$;

        # Prevents URIs that include the front controller. This will 404:
        # http://{{ DOMAIN }}/index.php/some-path
        # Remove the internal directive to allow URIs like this
        internal;
    }

    # DISABLE ROGUE PHP SCRIPTS NOT CATCHED IN PREVIOUS STATEMENTS
    location ~ \.php$ {
        deny all;
        access_log off;
        log_not_found off;
    }

    location ~ /\.ht {
        deny all;
    }

    # Enable big files
    client_max_body_size 16M;

    # Disables serving gzip content to IE 6 or below
    gzip_disable        "MSIE [1-6]\.";
    gzip_static     on;

    # text/html is included without mention so it's not necessary to add it
    gzip_types text/plain text/css application/json application/javascript application/x-javascript text/xml application/xml application/xml+rss text/javascript;
 
    # Sets the default type to text/html so that gzipped content is served
    # as html, instead of raw uninterpreted data.
    default_type text/html;

    more_clear_headers 'Server';
    add_header X-Frame-Options "SAMEORIGIN";

    # check the custom file full.mime.types, that includes woff2 and others
    include full.mime.types;

    # LOGGING RULES
    access_log off;

    # CACHING DEV RULES
    location ~*  \.(css|js)$ {
        expires -1;
        sendfile off;
    }
}
