#!/bin/bash
source .env;

project_root=$PROJECT_ROOT_DIR;

db_dump_path="$project_root/var/legacy.pumbate.com/database/";
initial_dir=$(pwd);

if [[ $NO_DATABASE_BACKUP ]]; then
    echo 'Ignoring make database-backup';
else
    ( 
        cd $project_root/deployments/legacy.pumbate.com && make database-backup 
    );
fi;

cd $initial_dir;

last_dump_file="$(ls -t1 $db_dump_path/*.xz | head -n 1)";

xz -d $last_dump_file;

last_dump_sql_file="$db_dump_path/$(basename $last_dump_file .xz)";

exec_mysql() {
    mysql \
        -u $MYSQL_USER \
        --password=$MYSQL_PASSWORD \
        -h 127.0.0.1 \
        -P $MYSQL_PORT \
        $MYSQL_DATABASE;
}

# migrations_dir="$project_root/databases/pumbate_site/migrations";
exec_mysql < $last_dump_sql_file;