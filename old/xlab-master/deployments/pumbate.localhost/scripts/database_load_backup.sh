#!/bin/bash
backup_file_xz="$1";

if [ -z backup_file_xz ]
  then
    echo "Backup file .xz required."; 
    exit 1;
fi

echo "------------------";
echo "Using backup file:";
echo "------------------";
echo $backup_file_xz;
echo "";

backup_file_sql="$(dirname $backup_file_xz)/$(basename $backup_file_xz .xz)";

if [ ! -f $backup_file_sql ]; then
    echo "Uncompressing...";
    xz -d -k $backup_file_xz;
    echo "Done!";
fi

source .env;

exec_mysql() {
    MYSQL_PWD=$MYSQL_PASSWORD mysql -u $MYSQL_USER -h 127.0.0.1 -P $MYSQL_PORT $MYSQL_DATABASE;
}

exec_mysqladmin() {
    MYSQL_PWD=$MYSQL_PASSWORD mysqladmin -u $MYSQL_USER -h 127.0.0.1 -P $MYSQL_PORT $1;
}

echo "------------------";
echo "MySQL settings:";
echo "------------------";
echo "USER: $MYSQL_USER";
echo "PORT: $MYSQL_PORT";
echo "------------------";
echo "";

exec_mysqladmin version;

echo "";
echo "----------------------------------------------";
echo "Loading $(basename $backup_file_sql)"
echo "----------------------------------------------";
echo "";

SECONDS=0

exec_mysql < $backup_file_sql;

echo "Done! $SECONDS secs.";