<?php 

namespace App\Command\Cache;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class RefreshCacheCommand extends Command
{
    protected $config;

    public function __construct($config)
    {
        $this->config = $config;
        parent::__construct();
    }

    protected function configure()
    {
        $this
            ->setName('refresh:all')
            ->setAliases(['refresh'])
            ->setDescription('Refresh all cache entries')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $command = $this->getApplication()->find('refresh:search');
        $refreshSearchReturnCode = $command->run($input, $output);

        $command = $this->getApplication()->find('refresh:locations');
        $refreshLocationsReturnCode = $command->run($input, $output);
    }
}