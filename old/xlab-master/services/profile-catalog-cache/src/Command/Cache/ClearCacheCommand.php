<?php 

namespace App\Command\Cache;

use App\Persistence\MySQLClient;
use App\Persistence\RedisClient;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

use Melquilabs\Pumbate\Models\LocationModel;
use Melquilabs\Pumbate\Services\Geolocation\LocationCacheService;
use Melquilabs\Pumbate\Services\Geolocation\LocationListService;

use Predis\Collection\Iterator as RedisIterator;

class ClearCacheCommand extends Command
{
    protected $config;

    public function __construct($config)
    {
        $this->config = $config;
        parent::__construct();
    }

    protected function configure()
    {
        $this
            ->setName('clear:all')
            ->setAliases(['clear'])
            ->setDescription('Clear all cache entries')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    { 
        $command = $this->getApplication()->find('clear:search');
        $clearSearchReturnCode = $command->run($input, $output);

        $command = $this->getApplication()->find('clear:locations');
        $clearLocationsReturnCode = $command->run($input, $output);
    }
}