<?php 

namespace App\Command\Cache;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class StatusCacheCommand extends Command
{
    protected $config;

    public function __construct($config)
    {
        $this->config = $config;
        parent::__construct();
    }

    protected function configure()
    {
        $this
            ->setName('status:all')
            ->setAliases(['status'])
            ->setDescription('Show all cache entries status')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $command = $this->getApplication()->find('status:search');
        $statusSearchReturnCode = $command->run($input, $output);

        $command = $this->getApplication()->find('status:locations');
        $statusLocationsReturnCode = $command->run($input, $output);
    }
}