<?php 

namespace App\Command\SearchCache;

use App\Persistence\MySQLClient;
use App\Persistence\RedisClient;
use App\Mocks\SessionMock;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

use Melquilabs\Pumbate\Models\CountryModel;
use Melquilabs\Pumbate\Models\GenderModel;
use Melquilabs\Pumbate\Models\LocationModel;
use Melquilabs\Pumbate\Models\ProfileModel;
use Melquilabs\Pumbate\Models\SettingModel;
use Melquilabs\Pumbate\Services\Geolocation\LocationCacheService;
use Melquilabs\Pumbate\Services\Geolocation\LocationListService;
use Melquilabs\Pumbate\Services\WebFront\ProfileGalleryService;
use Melquilabs\Pumbate\Services\System\ConfigurationService;
use Melquilabs\Pumbate\Services\Content\CountrySeoGenerator;
use Melquilabs\Pumbate\Services\Content\Keywords;
use Melquilabs\Pumbate\Services\Media\Asset;


class RefreshSearchCacheCommand extends Command
{
    protected $config;

    public function __construct($config)
    {
        $this->config = $config;
        parent::__construct();
    }

    protected function configure()
    {
        $this
            ->setName('refresh:search')
            ->setDescription('Refresh locations cache')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $db = MySQLClient::create();
        $redis = RedisClient::create();
        $session = new SessionMock();

        $settingModel = new SettingModel($db);
        $configService = new ConfigurationService($settingModel);

        $profileModel = new ProfileModel($db, $session, null, null);
        $countrySeoGenerator = new CountrySeoGenerator();
        $genderModel = new GenderModel();
        $keywordsGenerator = new Keywords();
        $assetService = new Asset($configService->item('asset_packages'));

        $configService->setParameter('catalog_cache_enabled', true);

        if (!is_dir($configService->getParameter('media_dir_path')))
        {
            $configService->setParameter('media_dir_path', '../../web/media');
        }

        $profileGalleryService = new ProfileGalleryService(
            $db, $redis, $configService, $profileModel
        ,   $assetService, $keywordsGenerator, $countrySeoGenerator, $genderModel            
        );

        $this->queryTopSearches($profileGalleryService);
        $profileGalleryService->refresh_gallery_cache();

        //$configService->item('website_base_url');
        //$configService->item('media_dir_path');

        $command = $this->getApplication()->find('status:search');
        $statusSearchReturnCode = $command->run($input, $output);
    }

    protected function queryTopSearches($profileGalleryService)
    {
        $topSearches = [];

        $baseParams = [
            'country' => '' 
        ,   'gender' => ''
        ,   'region' => false
        ,   'city' => false
        ,   'keywords'=> ''
        ,   'limit' => 400
        ];

        $countries = ['uy', 'co'];
        $genders = ['mujer', 'hombre', 'trans', 'duo'];

        foreach ($countries as $countryIso)
        {
            foreach ($genders as $gender)
            {
                $searchOptions = new \ArrayObject($baseParams);
                $searchOptions['country'] = $countryIso;
                $searchOptions['gender'] = $gender;
                $topSearches[] = $searchOptions;
            }
        }

        foreach ($topSearches as $searchParams)
        {
            $profileGalleryService->get_gallery_data($searchParams);
            $searchStr = json_encode($searchParams);
            $searchHash = sha1($searchStr);
            $profileGalleryService->refresh_cache_entry($searchHash);
        }

        return $topSearches;
    }
}