<?php 

namespace App\Command\LocationsCache;

use App\Persistence\MySQLClient;
use App\Persistence\RedisClient;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

use Melquilabs\Pumbate\Models\LocationModel;
use Melquilabs\Pumbate\Services\Geolocation\LocationCacheService;
use Melquilabs\Pumbate\Services\Geolocation\LocationListService;

use Predis\Collection\Iterator as RedisIterator;

class ClearLocationsCacheCommand extends Command
{
    protected $config;

    public function __construct($config)
    {
        $this->config = $config;
        parent::__construct();
    }

    protected function configure()
    {
        $this
            ->setName('clear:locations')
            ->setDescription('Clear all cache entries')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    { 
        $redis = RedisClient::create();

        $outputData = [
            'items' => []
        ,   'query_database' => $redis->getConnection()->getParameters()->toArray()
        ];

        $dataKeysPatterns = [
            'profiles:locations:*'
        ];

        foreach ($dataKeysPatterns as $keyPattern)
        {
            foreach (new RedisIterator\Keyspace($redis, $keyPattern) as $key) 
            {
                $outputData['items'][] = $key;
                $redis->del($key);
            }
        }

        $output->writeLn(json_encode($outputData, JSON_PRETTY_PRINT));
    }
}