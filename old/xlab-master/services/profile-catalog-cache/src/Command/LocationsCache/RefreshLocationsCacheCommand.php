<?php 

namespace App\Command\LocationsCache;

use App\Persistence\MySQLClient;
use App\Persistence\RedisClient;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

use Melquilabs\Pumbate\Models\GenderModel;
use Melquilabs\Pumbate\Models\LocationModel;
use Melquilabs\Pumbate\Services\Profile\ProfileLocationQueryService;
use Melquilabs\Pumbate\Services\Profile\ProfileLocationCacheService;

class RefreshLocationsCacheCommand extends Command
{
    protected $config;

    public function __construct($config)
    {
        $this->config = $config;
        parent::__construct();
    }

    protected function configure()
    {
        $this
            ->setName('refresh:locations')
            ->setDescription('Refresh locations cache')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $db = MySQLClient::create();
        $redis = RedisClient::create();
        $locationModel = new LocationModel($db);
        $genderModel = new GenderModel();
        
        $profileLocationCacheService = new ProfileLocationCacheService(
            $db, $redis, $locationModel, $genderModel
        );

        $countryList = ['uy', 'co'];

        foreach ($countryList as $countryIsoCode)
        {
            $profileLocationCacheService->refreshCountryActiveLocations($countryIsoCode);
        }

        $command = $this->getApplication()->find('status:locations');
        $statusLocationsReturnCode = $command->run($input, $output);
    }
}