<?php 

namespace App\Boot;

use Symfony\Component\Console\Application as SymfonyApplication;
use Symfony\Component\Console\Event\ConsoleCommandEvent;
use Symfony\Component\Console\ConsoleEvents;
use Symfony\Component\EventDispatcher\EventDispatcher;


class Application
{
    public static function init($config)
    {
        // Load App
        $application = new SymfonyApplication();

        $dispatcher = new EventDispatcher();
        $application->setDispatcher($dispatcher);

        $dispatcher->addListener(ConsoleEvents::COMMAND, function (ConsoleCommandEvent $event) use ($config) 
        {
            // gets the input instance
            $input = $event->getInput();
            // gets the output instance
            $output = $event->getOutput();
            // gets the command to be executed
            $command = $event->getCommand();

            $config->loadFromDotEnv();
            $config->loadFromConsoleInput($input);
        });

        return $application;
    }
}