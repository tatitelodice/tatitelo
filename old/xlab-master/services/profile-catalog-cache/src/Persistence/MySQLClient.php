<?php 

namespace App\Persistence;

use Melquilabs\Pumbate\Database\DriverBuilder;
use Melquilabs\Pumbate\Database\Driver\MySQLi\ActiveRecord as MySQLiActiveRecord;

class MySQLClient
{
    public static function create()
    {
        $db_host = getenv('DATABASE_HOST');
        $db_port = getenv('DATABASE_PORT');
        $db_host = (!empty($db_port))? $db_host . ':' . $db_port : $db_host;
        
        $conf['hostname'] = $db_host;
        $conf['username'] = getenv('DATABASE_USER');
        $conf['password'] = getenv('DATABASE_PASSWORD');
        $conf['database'] = getenv('DATABASE_NAME');
        $conf['dbdriver'] = 'mysqli';
        $conf['dbprefix'] = '';
        $conf['pconnect'] = TRUE;
        $conf['db_debug'] = TRUE;
        $conf['cache_on'] = FALSE;
        $conf['cachedir'] = '';
        $conf['char_set'] = 'utf8mb4';
        $conf['dbcollat'] = 'utf8mb4_unicode_ci';
        $conf['swap_pre'] = '';
        $conf['autoinit'] = TRUE;
        $conf['stricton'] = FALSE;

        $db = DriverBuilder::create(
            MySQLiActiveRecord::class, $conf
        );

        return $db;
    }

}