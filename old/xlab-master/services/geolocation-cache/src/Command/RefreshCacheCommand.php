<?php 

namespace App\Command;

use App\Persistence\MySQLClient;
use App\Persistence\RedisClient;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

use Melquilabs\Pumbate\Models\LocationModel;
use Melquilabs\Pumbate\Services\Geolocation\LocationCacheService;
use Melquilabs\Pumbate\Services\Geolocation\LocationListService;


class RefreshCacheCommand extends Command
{
    protected $config;

    public function __construct($config)
    {
        $this->config = $config;
        parent::__construct();
    }

    protected function configure()
    {
        $this
            ->setName('refresh')
            ->setDescription('Refresh locations cache')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $db = MySQLClient::create();
        $redis = RedisClient::create();

        $locationModel = new LocationModel($db);
        $locationCacheService = new LocationCacheService($locationModel, $redis);
        $locationCacheService->updateLocationsCache();

        $locationListService = new LocationListService($locationModel, $redis);
    }
}