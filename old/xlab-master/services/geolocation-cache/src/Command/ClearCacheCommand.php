<?php 

namespace App\Command;

use App\Persistence\MySQLClient;
use App\Persistence\RedisClient;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

use Melquilabs\Pumbate\Models\LocationModel;
use Melquilabs\Pumbate\Services\Geolocation\LocationCacheService;
use Melquilabs\Pumbate\Services\Geolocation\LocationListService;

use Predis\Collection\Iterator as RedisIterator;

class ClearCacheCommand extends Command
{
    protected $config;

    public function __construct($config)
    {
        $this->config = $config;
        parent::__construct();
    }

    protected function configure()
    {
        $this
            ->setName('clear')
            ->setDescription('Clear all cache entries')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    { 
        $redis = RedisClient::create();

        $outputData = [
            'deleted' => []
        ,   'query_database' => $redis->getConnection()->getParameters()->toArray()
        ];

        $dataKeysPatterns = [
            'locations:*'
        ];

        foreach ($dataKeysPatterns as $keyPattern)
        {
            foreach (new RedisIterator\Keyspace($redis, $keyPattern) as $key) 
            {
                $outputData['deleted'][] = $key;
                $redis->del($key);
            }
        }

        $output->writeLn(json_encode($outputData, JSON_PRETTY_PRINT));
    }
}