<?php 

namespace App\Command;

use App\Persistence\MySQLClient;
use App\Persistence\RedisClient;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

use Melquilabs\Pumbate\Models\LocationModel;
use Melquilabs\Pumbate\Services\Geolocation\LocationCacheService;
use Melquilabs\Pumbate\Services\Geolocation\LocationListService;


class StatusCacheCommand extends Command
{
    protected $config;

    public function __construct($config)
    {
        $this->config = $config;
        parent::__construct();
    }

    protected function configure()
    {
        $this
            ->setName('status')
            ->setDescription('Show locations cache status')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $db = MySQLClient::create();
        $redis = RedisClient::create();

        $locationModel = new LocationModel($db);
        $locationCacheService = new LocationCacheService($locationModel, $redis);

        $locationListService = new LocationListService($locationModel, $redis); 
        
        $countryList = ['uy', 'co'];

        $outputData = [
            'data' => []
        ,   'source_database' => [
                'hostname' => $db->hostname
            ,   'port'     => $db->port
            ,   'user'     => $db->username
            ,   'database' => $db->database
            ]
        ,   'query_database' => $redis->getConnection()->getParameters()->toArray()
        ];

        foreach ($countryList as $countryIsoCode)
        {
            $regions = $locationListService->getRegions($countryIsoCode);
            $cities = [];

            foreach ($regions as $regionName)
            {
                $regionCities = $locationListService->getCities($countryIsoCode, $regionName);
                $cities[$regionName] = $regionCities;
            }  

            $totalCities = array_reduce($cities, function($total, $regionCities)
            {
                return $total + count($regionCities);
            }, 0);

            $outputData['data'][$countryIsoCode] = [
                'totalRegions' => count($regions)
            ,   'totalCities'  => $totalCities
            ];
        }

        $output->writeLn(json_encode($outputData, JSON_PRETTY_PRINT));
    }
}