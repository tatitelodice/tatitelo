<?php 

namespace App\Persistence;

use Predis\Client as PredisClient;

class RedisClient
{
    public static function create()
    {
        if (!getenv('APP_PREFIX'))
        {
            throw new \Exception("Missing application parameter: APP_PREFIX");
        }

        $redis = new PredisClient([
            'scheme' => 'tcp'
        ,   'host'   => getenv('REDIS_HOST')
        ,   'port'   => getenv('REDIS_PORT')
        ,   'prefix' => getenv('APP_PREFIX') . ':'
        ]);
        
        return $redis;
    }
}