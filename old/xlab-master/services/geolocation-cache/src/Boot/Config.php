<?php 

namespace App\Boot;

use Symfony\Component\Dotenv\Dotenv;
use Symfony\Component\Dotenv\Exception\PathException;

use Symfony\Component\Console\Input\InputInterface;

class Config
{
    protected $parameters;

    public function __construct()
    {
        $this->initParameters();
    }

    protected function initParameters()
    {
        $params = [];

        $params['REDIS_HOST'] = '127.0.0.1';
        $params['REDIS_PORT'] = 6379;
        
        $params['DATABASE_HOST'] = '127.0.0.1';
        $params['DATABASE_USER'] = 'root';
        $params['DATABASE_PASSWORD'] = '';
        $params['DATABASE_NAME'] = 'pumbate_site';
        $params['DATABASE_PORT'] = 3306;
        
        $params['APP_PREFIX'] = 'pumbate';

        $this->parameters = $params; 
    }

    protected function loadParameters($params)
    {
        $this->parameters = array_merge($this->parameters, $params);
    }

    public function getParameter($key)
    {
        return $this->params[$key];
    }

    public function getParameters()
    {
        return new \ArrayObject($this->params);
    }

    public function loadFromConsoleInput(InputInterface $input)
    {

    }

    public function loadFromDotEnv()
    {
        $dotenv = new Dotenv();

        $envFilePath = getenv('APP_ENV_FILE');
        
        if (empty($envFilePath))
        {
            $envFilePath = getcwd() . '/.env';
        }

        if (!is_readable($envFilePath) || is_dir($envFilePath)) {
            throw new PathException($envFilePath);
        }

        if (preg_match('!\.php$!i', $envFilePath))
        {
            $envVars = include($envFilePath);
        }
        else
        {
            $envVars = $dotenv->parse(file_get_contents($envFilePath), $envFilePath);
        }

        $dotenv->populate($envVars);
        $this->loadParameters($envVars);
    }

}