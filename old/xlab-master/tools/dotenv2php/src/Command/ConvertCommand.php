<?php 

namespace App\Command;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Dotenv\Dotenv;


class ConvertCommand extends Command
{
    protected function configure()
    {
        $this
            ->setName('convert')
            ->setDescription('Convert environment file to php putenv() calls.')
            ->addArgument(
                'input-file',
                InputArgument::REQUIRED,
                'Input env file'
            )
            ->addArgument(
                'output-file',
                InputArgument::OPTIONAL,
                'Output php file'
            )
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $inputFilePath = $input->getArgument('input-file');
        $outputFilePath = $input->getArgument('output-file');

        //var_dump($inputFilePath);
        //var_dump($outputFilePath);

        $dotenv = new Dotenv();
        $inputFileData = file_get_contents($inputFilePath);
        $parsedVars = $dotenv->parse($inputFileData, basename($inputFilePath));

        $outputCode  = "<?php " . PHP_EOL;
        $outputCode .= '// ENV VARS ' . PHP_EOL;

        $outputCode .= sprintf('return %s;', var_export($parsedVars, true));
        $outputCode .= PHP_EOL;
        $outputCode .= '// END ENV VARS ' . PHP_EOL;

        //var_dump($parsedVars);
        if ($outputFilePath)
        {
            file_put_contents($outputFilePath, $outputCode);
        }
        else
        {
            echo $outputCode;
        }

    }
}

/* eof */