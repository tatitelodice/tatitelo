<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

use Moment\Moment;

class Payments_Model extends CI_Model {

	public function __construct() {
		parent::__construct();
		$this->load->model('profile_model');
		$this->load->model('deposit_model');
		$this->load->model('pricing_model');
		$this->load->model('payment_account_model');
	}
	

	public function get_profiles_last_payments($params) {
		$country = $params['country'];
		$gender = $params['gender'];
		
		$order_by = (!empty($params['order_by']))? $params['order_by'] : false;
		$verified = (!empty($params['verified']))? $params['verified'] : 1; 
			
		$gender_cond = "= '" . $gender . "'";
		if (preg_match('!^duo!', $gender)) $gender_cond = "like '" . $gender . "%'";

		$profile_conditions = array(
			"proCountry = '" . $country . "'"
		,	"proDeleted = 0"
		,	"proVerified = " . $verified
		,	"proGender " . $gender_cond
		);	
		
		if ($verified > 1) {
			$profile_conditions[] = "proModifiedDate >= '" . date('Y-m-d', strtotime('-1 month')) . "'";
		}

		if (!empty($params['custom_conditions']))
		{
			$profile_conditions = array_merge($profile_conditions, $params['custom_conditions']);
		}
		
		$profile_conditions_sql = implode(" AND ", $profile_conditions);
				
		$selected_fields = array(
			"profile_group_id",
			'p.*',
			"deposits.depAppliedAt as depLastDateApplied", 
			"deposits.depDate as depLastDate", 
			"payAmount", "payNextdate",
			"pg.name as group_name",
			"deposits.depAmount"
		);
		
		$full_sql = "
			select " . implode(", ", $selected_fields) . " 
			from profiles p 
			inner join clients on clients.proUser = p.proUser
			left outer join payments on payments.proUser = p.proUser
			left outer join profile_group pg on (profile_group_id > 0 AND pg.id = profile_group_id)
			left outer join  deposits on deposits.depId = payments.lastDepositId
			where ${profile_conditions_sql}
		";
			
		$full_sql .= " order by ";
		
		switch ($order_by) {
			case "last_deposit_asc":
				$full_sql .= " depLastDate asc"; break;
			case "last_deposit_desc":
				$full_sql .= " depLastDate desc"; break;
			case "priority_asc":
				$full_sql .= " proAccountType asc, proPriority desc"; break;
			default :
				$full_sql .= " proAccountType desc, proPriority asc";
		}
		//echo $full_sql; die;
		
		$db_rows = array();

		$db_result = $this->db->query($full_sql);
		
		if ($db_result->num_rows()) {
			$db_rows = $db_result->result_array();
		}
		
		return $db_rows;		
	}


	public function get_profiles_expired($params)
	{
		$today = (new Moment())->setHour(0)->setMinute(0)->setSecond(0);
		/*
		$last_deposit_max_date = $today
			->cloning()
			->subtractDays(31)
			->format('Y-m-d')
		;
		*/
		$params = array_merge($params, [
			'custom_conditions' => [
			//	"deposits.depAppliedAt < '" . $last_deposit_max_date . "'",
				"payNextdate < '" . $today->format('Y-m-d') . "'"
			]
		]);

		return $this->get_profiles_last_payments($params);
	}


	public function get_profile_payments_status($user)
	{
		$payment_data = array();
		$payment_data['user'] = $user;
		
		$payment_data['amount'] = 0;
		$payment_data['comments'] = '';
		$payment_data['day'] = 1;
		$payment_data['nextdate'] = '';
		$payment_data['status'] = '';
		$payment_data['days_remaining'] = null;

		$payment_data['payment_account'] = $this
			->payment_account_model
			->get_default_account_for_profile([ 'user' => $user ])
		;
		
		$this->db->where('proUser', $user);
		$this->db->limit(1);
		
		$res = $this->db->get('payments');
		
		if ( $res->num_rows() ) {
			$dbrow = $res->row_array();
			
			$payment_data['amount'] = $dbrow['payAmount'];
			$payment_data['comments'] = $dbrow['payComments'];
			$payment_data['day'] = $dbrow['payDay'];
			$payment_data['nextdate'] = $dbrow['payNextdate'];

			if ($payment_data['nextdate'])
			{
				$payment_data['nextdate'] = new Moment($payment_data['nextdate']);

				$today_dt = new Moment();

				$payment_data['days_remaining'] = intval(
					$today_dt->from($payment_data['nextdate'])->getDays()
				);
			}

			$payment_data['status'] = $dbrow['payStatus'];

			if ($dbrow['paymentAccountId'])
			{
				$payment_account = $this->payment_account_model->get_by_id($dbrow['paymentAccountId']);

				if (!empty($payment_account) 
					&& $payment_account['isActive'] 
					&& !$payment_account['isDeleted']
				)
				{
					$payment_data['payment_account'] = $payment_account;
				}
			}

		}
		
		return $payment_data;
	}

	public function upsert_profile_payments_status($user, $data)
	{
		$this->db->where('proUser', $user);
		$payment_data_exists = $this->db->get('payments')->num_rows() > 0;

		$this->db->set( 'payAmount', $data['amount']);

		if (isset($data['day']))
		{
			$this->db->set( 'payDay', $data['day']);
		}
		
		if (isset($data['comments']))
		{
			$this->db->set( 'payComments', $data['comments']);
		}

		$this->db->set( 'payStatus', $data['status']);
		
		$nextDate = (!empty($data['nextDate'])) ? $data['nextDate'] : null;

		if ($nextDate)
		{
			$this->db->set( 'payNextdate', "'" . $nextDate  . "'", false );
			
			if (strtotime($nextDate) > time() && ( $data['status'] == 'debt' ) ) {
				$data['status'] = 'ok';	
			}
		}

		$lastDeposit = $this->deposit_model->get_profile_last_deposit($user);
		$lastDepositId = ($lastDeposit)? $lastDeposit['id'] : null;
		$this->db->set('lastDepositId', $lastDepositId);

		if ($payment_data_exists) {
			$this->db->where( 'proUser', $user);
			$success = $this->db->update('payments');
		} else {
			$this->db->set( 'proUser', $user);
			$success = $this->db->insert('payments');
		}

		return $success;
	}


	public function amend_profile_payments_status($user)
	{
		$last_deposit = $this->deposit_model->get_profile_last_deposit($user);
		if (!$last_deposit) return false;

		$profile = $this->profile_model->get_profile($user);
		$old_payments_status = $this->get_profile_payments_status($user);

		$payments = array();
		$payments['status'] = 'ok';
		$payments['amount'] = $last_deposit['amount'];
		//$payments['day'] = 1;
		//$payments['comments'] = '';

		$account_type = $this->pricing_model->get_profile_account_type_for_amount(
			$profile, $payments['amount']
		);

		$m_date_now = new Moment('now');
		$m_date_applied = $this->_dmy_date_to_moment($last_deposit['date_applied']);

		//$m = $m_date_now->cloning()->endOf('month');

		$pricing = $this->pricing_model->get_pricing_for_profile($profile);

		$daysOfMonth = $this->pricing_model->resolve_publication_days(
			$pricing, $account_type, $payments['amount']
		);

		// consider the payment day itself
		$daysOfMonth = $daysOfMonth + 1;

		$m_next_date = $m_date_applied->cloning()->addDays($daysOfMonth);

		if ($old_payments_status && !empty($old_payments_status['nextdate']))
		{
			$m_old_next_date = $this->_dmy_date_to_moment($old_payments_status['nextdate']);

			if ($m_old_next_date->isAfter($m_date_now)
				&& $m_old_next_date->isBefore($m_next_date))
			{
				$m_next_date = $m_old_next_date->cloning()->addDays($daysOfMonth);
			}
		}

		$next_date = $m_next_date->format('Y-m-d H:i:s');

		$payments['nextDate'] = $next_date;

		$priority = ($profile['accountType'] == $account_type) ? $profile['priority'] : 'last';

		$this->upsert_profile_payments_status($user, $payments);
		
		$this->profile_model->enable_profile($user, array(
			'account_type' => $account_type
		,	'priority' => $priority
		));

		$this->profile_model->change_profile_panel_message($user, '');

		if ($this->input->get('debug'))
		{
			echo '<pre>';
			print_r($profile);
			print_r($last_deposit);
			print_r($payments);
			echo '</pre>';
		}
	}


	public function update_profile_last_deposit($user)
	{
		$lastDeposit = $this->deposit_model->get_profile_last_deposit($user);
		$lastDepositId = ($lastDeposit)? $lastDeposit['id'] : null;
		$this->db->set('lastDepositId', $lastDepositId);
		$this->db->where( 'proUser', $user);
		$this->db->update('payments');
	}


	protected function _dmy_date_to_moment($dmy_date)
	{
		if ($dmy_date instanceof Moment)
		{
			return $dmy_date;
		}

		$ymd_date = preg_replace('!(\d+)/(\d+)/(\d+)!', '$3-$2-$1', $dmy_date);
		$m_date = new Moment($ymd_date); $m_date->setLocale('es_ES');
		return $m_date;
	}

}

/* end of file */
