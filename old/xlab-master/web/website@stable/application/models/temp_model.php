<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');


class Temp_Model extends CI_Model {

	
	public function __construct() {
		parent::__construct();
	}
	
	public function insert_temp($key, $value, $expire_date = false) {
		
		if (!$expire_date) {
			$expire_date = date('Y-m-d H:i:s', strtotime('+2 day'));
		}
		
		$this->db->set('tmpKey', $key);
		$this->db->set('tmpValue', $value);
		$this->db->set('tmpCreated', "'" . date('Y-m-d H:i:s') . "'", false);
		$this->db->set('tmpExpires', "'" . $expire_date . "'", false);
		
		return $this->db->insert('temps');
	}
	
	public function update_temp($key, $value, $expire_date = false) {
		
		$this->db->set('tmpValue', $value);
		
		if ($expire_date) {
			$this->db->set('tmpExpires', "'" . $expire_date . "'", false);
		}
		
		$this->db->where('tmpKey', $key);
		
		return $this->db->update('temps');
	}
	
	public function get_temp($key) {
		$temp_data = false;
		
		$this->db->where('tmpKey', $key);
				
		$query = $this->db->get('temps');
		
		if ($query->num_rows() > 0) {
			$raw_data = $query->row_array();
			
			$prefix = 'tmp';
			$temp_data = array();
			
			foreach($raw_data as $name => $value) {
				$name = str_replace($prefix, '', $name);
				$name[0] = strtolower($name[0]);
				
				$temp_data[$name] = $value;
			}
		}
		
		return $temp_data;
	}
	
	public function delete_temp($key) {
		$this->db->where('tmpKey', $key);
		$this->db->delete('temps');		
	}
	
	public function delete_expired_temp($key) {
		$this->db->where('tmpExpires <=', date("Y-m-d H:i:s"));
		$this->db->where('tmpKey', $key);
		$this->db->delete('temps');
	}
	
}

/* end of file */
