<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');


class Pricing_Model extends CI_Model {

    public function __construct() {
        parent::__construct();
    }


    public function get_all_prices_by_country()
    {
        return $this->config->item('pricing');
    }


    public function get_pricing_for_profile($profile) {
        $country = $profile['country'];
        $gender = $profile['gender'];
        return $this->get_pricing_by_country_gender($country, $gender);
    }


    public function get_pricing_by_country_gender($country, $gender)
    {
        $pricing_config = $this->config->item('pricing');

        if (empty($pricing_config[$country]))
        {
            return false;
        }

        $country_pricing = $pricing_config[$country];
        $pricing = $country_pricing['default'];

        if (isset($country_pricing[$gender]))
        {
            $pricing = $country_pricing[$gender];
        }

        return $pricing;
    }


    public function get_profile_account_type_for_amount($profile, $amount)
    {
        $resolved_account_type = ($profile['accountType'] > PROFILE_BASIC) ? $profile['accountType'] : PROFILE_BASIC;

        $pricing = $this->get_pricing_for_profile($profile);

        $amount = intval($amount);

        foreach ($pricing as $price_item)
        {
            $same_or_greater_amount = $amount >= intval($price_item['price']);

            if ($same_or_greater_amount && $price_item['accountType'] > $resolved_account_type)
            {
                $resolved_account_type = $price_item['accountType'];
            }
        }

        return $resolved_account_type;
    }

    public function filter_pricing_by_attribute($pricing, $attr, $attr_val)
    {
        $result = [];

        foreach ($pricing as $item)
        {
            if (strval($item[$attr]) !== strval($attr_val))
            {
                continue;
            }

            $result[] = $item;
        }

        return $result;
    }

    public function resolve_publication_days($pricing, $account_type, $deposit_amount)
    {
        $resolved_days = null;

        $resolved_days_limit = 45;

        $deposit_amount = intval($deposit_amount);

        $pricingLookup = $this->filter_pricing_by_attribute(
            $pricing, 'accountType', $account_type
        );

        // AVOIDS ERRORS WHEN ACCOUNT TYPE HAS NO DEFINED PRICING
        // ONLY APPLIED FILTERED PRICING IF THERE ARE RESULTS
        if (!empty($pricingLookup))
        {
            $pricing = $pricingLookup;
        }

        $base_days  = null;
        $base_price = null;
        
        foreach ($pricing as $item_pricing)
        {
            $item_pricing_price = intval($item_pricing['price']);
            $item_pricing_days = intval($item_pricing['days']);

            $same_or_greater_amount = $deposit_amount >= $item_pricing_price;

            if (!$base_price || $same_or_greater_amount)
            {
                $base_days = $item_pricing_days;
                $base_price = $item_pricing_price;
            }
        }

        $resolved_days = ceil($deposit_amount *  $base_days / $base_price);

        $resolved_days = ($resolved_days > $resolved_days_limit) ? $resolved_days_limit : $resolved_days;

        return $resolved_days;        
    }

}