<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');


class Log_Model extends CI_Model {

	
	public function __construct() {
		parent::__construct();
	}
	

    public function find_one_by($params, $order_by=false)
    {
        $res = $this->find_by($params, 1, $order_by);

        if (!empty($res))
        {
            return $res[0];
        }

        return null;
    }


    public function find_by($params, $offset=10, $order_by=false)
    {
        $logs = array();
        
        foreach ($params as $field => $value)
        {
            $this->db->where($field, $value);
        }
        
        $order_by = (!empty($order_by))? $order_by : 'logId desc';
        $this->db->order_by($order_by);
        
        if (!empty($offset))
        {
            $this->db->limit($offset);
        }
        
        $res = $this->db->get('logs');
        
        if ($res->num_rows) {
            foreach ($res->result_array() as $row) {
                $logs[] = $this->convert_row_to_log($row);
            }
        }
        
        return $logs;        
    }


	public function insert_log($country, $key_description, $user='', $notify = false, $date = false, $metadata=false) {
		
		if (!$date) {
			$date = date('Y-m-d H:i:s');
		}
		
		if ($notify) {
			$notify = 1;
		} else {
			$notify = 0;
		}

		$metadata = (empty($metadata)) ? [] : $metadata;

		$this->db->set('logCountry', $country);
		$this->db->set('logAddress', $this->input->ip_address());
		$this->db->set('logDescription', $key_description);
		
		$this->db->set('logMeta', json_encode($metadata));
		$this->db->set('logDate', "'" . $date . "'", false);
		$this->db->set('logNotify', $notify);
		$this->db->set('proUser', $user);
		
		return $this->db->insert('logs');
	}
	
	public function remove_notifications($user, $key_description = false) {
		$this->db->set('logNotify', 0);
		
		$this->db->where('proUser', $user);
		
		if ($key_description) {
			$this->db->where('logDescription', $key_description);
		}
		
		$this->db->update('logs');
	}


    public function remove_notification($logId)
    {
        $this->db->set('logNotify', 0);
        $this->db->where('logId', $logId);
        $this->db->limit(1);
        $this->db->update('logs');        
    }

	
	public function get_current_alerts_count($country = '') {		
		$this->db->where('logNotify', 1);
		$this->db->where('logDate <=', date('Y-m-d') . ' 23:59:59');

		if (strlen($country) == 2) {
			$this->db->where('logCountry', $country);
		}

		return $this->db->count_all_results('logs');
	}
	

	public function convert_row_to_log($row)
	{
		$log = [];

		foreach ($row as $k => $v)
		{
			$newKey = preg_replace('!^log!', '', $k);
			$newKey = lcfirst($newKey);
			$log[$newKey] = $v;
		}

		$log['user'] = $row['proUser'];
		$log['meta'] = json_decode($log['meta'], true);
		return $log;
	}
}

/* end of file */
