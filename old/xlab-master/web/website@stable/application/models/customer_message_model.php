<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');


class Customer_Message_Model extends CI_Model {

    public function get_message($message_id)
    {
        $result = null;

        $this->db->where('id', $message_id);
        $this->db->limit(1);
        $res_row = $this->db->get('customer_message')->row();

        if ($res_row)
        {
            $res_row->attributes = json_decode($res_row->attributes);
            $result = $res_row;
        }

        return $result;
    }


    public function create_inbox_message($msg_data)
    {
        $messageId = $msg_data['uuid']; 
        $packedMessageId = hex2bin(str_replace('-', '', $messageId));

        $this->db->set('uuid', $packedMessageId);

        $this->db->set('channel', 'sms');
        $this->db->set('category', 'support');
        $this->db->set('messageBox', 'inbox');
        $this->db->set('messageBoxStatus', 'unknown');
        $this->db->set('messageText', $msg_data['message_text']);

        $this->db->set('customerAddress', $msg_data['message_address']);
        $this->db->set('customerEntityId', null);
        $this->db->set('customerLabel', null);

        $this->db->set('systemAddress', $msg_data['system_address']);

        $df = 'Y-m-d H:i:s';
        
        $d = \DateTime::createFromFormat(\DateTime::ISO8601, $msg_data['system_datetime']);

        $this->db->set('systemDate', "'" . $d->format($df) . "'", false);
        $this->db->set('systemLabel', 'ceci');
        
        $attrs = [];

        if (isset($msg_data['attributes']) && is_array($msg_data['attributes'])) {
            $attrs = $msg_data['attributes'];
        };
        
        $this->db->set('attributes', json_encode($attrs));

        $this->db->set('tags', json_encode(array()));

        $currentDt = (new \DateTime())->format($df);

        $this->db->set('createdAt', "'" . $currentDt . "'", false);
        $this->db->set('modifiedAt', "'" . $currentDt . "'", false);

        $this->db->set('isPendingRead', 1);
        $this->db->set('isDeleted', 0);

        if ($this->db->insert('customer_message'))
        {
            return $this->db->insert_id();
        }

        return false;
    }


    public function create_outbox_message($message_data)
    {
        $target_addresses = $message_data["message_address"];

        if (is_array($target_addresses))
        {
            $message_data["message_address"] = '@template_target_address';
        }
        else
        {
            $target_addresses = [ $target_addresses ];
        }

        $template_row = $this->_get_outbox_message_insert_row($message_data);
        $insert_rows = [];

        foreach ($target_addresses as $addr)
        {
            $addr = trim($addr);
            if (empty($addr)) continue;

            $row = $template_row;
            $messageUuid = \Ramsey\Uuid\Uuid::uuid4();
            $row['uuid'] = $messageUuid->getBytes();
            $row['customerAddress'] = $addr;
            
            $insert_rows[] = $row;
        }

        $result = $this->db->insert_batch('customer_message', $insert_rows);        

        return $result;    
    }

    protected function _get_outbox_message_insert_row($message_data)
    {
        $row = [];

        $messageUuid = \Ramsey\Uuid\Uuid::uuid4();
        $row['uuid'] = $messageUuid->getBytes();
        $row['channel'] = 'sms';
        $row['category'] = $message_data['message_category'];

        $row['customerAddress'] = $message_data['message_address'];

        $row['messageBox'] = 'outbox';
        $row['messageBoxStatus'] = 'pending';
        $row['messageText'] = $message_data['message_text'];

        $row['attributes'] = json_encode([]);
        $row['tags'] = json_encode([]);

        $currentDt = (new \DateTime())->format('Y-m-d H:i:s');
        $row['createdAt'] = $currentDt;
        $row['modifiedAt'] = $currentDt;

        $row['isPendingRead'] = 1;
        $row['isDeleted'] = 0;

        return $row;
    }


    public function update_outbox_sms_status($id, $status, $prev_status=false)
    {
        $updated = false;

        $this->db->set('messageBoxStatus', $status);
        $this->db->where('channel', 'sms');
        $this->db->where('messageBox', 'outbox');

        if ($prev_status)
        {
            $this->db->where('messageBoxStatus', $prev_status);
        }

        $this->db->where('id', $id);
        $this->db->limit(1);
        
        if ($this->db->update('customer_message'))
        {
            $updated = $this->db->affected_rows() > 0;
        }

        return $updated;
    }


    public function get_outbox_messages ($options=[])
    {
        $messages = [];

        $this->db->where('messageBox', 'outbox');
        $this->db->where('channel', 'sms');
        
        if (!empty($options['status']))
        {
            $this->db->where('messageBoxStatus', $options['status']);
        }

        $limit = (!empty($options['limit']))? $options['limit'] : 1000;
        $offset =  (!empty($options['offset']))? $options['offset'] : 0;

        $sorting = (!empty($options['sort'])) ? $options['sort'] : 'id desc';

        $this->db->order_by($sorting);
        $this->db->limit($limit, $offset);    
        
        $res = $this->db->get('customer_message')->result();

        if ($res)
        {
            foreach ($res as $res_row) {
                $res_row->attributes = json_decode($res_row->attributes);
                $messages[] = $res_row;
            }
        }

        return $messages;
    }


}