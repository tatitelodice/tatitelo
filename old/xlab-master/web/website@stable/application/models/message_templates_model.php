<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');


class Message_Templates_Model extends CI_Model {

	
	public function __construct() {
		parent::__construct();
	}
	
	
	public function get_by_name($name) {
		$template_data = false;
		
		$this->db->where('name', $name);
		$query = $this->db->get('message_templates');
		
		if ($query->num_rows() > 0) {
			$template_data = $query->row_array();
		}
		
		return $template_data;	
	}
	
	
	public function set_template_values($template_text, $template_values, $clear_undefined=false) {
		$final_text = $template_text;
		
		if (preg_match_all('!\{\{(.*?)\}\}!', $template_text, $matches)) {
			for ($i=0; $i < count($matches[0]); $i++) {
				
				$var_name = $matches[1][$i];
				
				if (isset($template_values[$var_name])) {
					$var_value = $template_values[$var_name];
					$final_text = str_replace($matches[0][$i], $var_value, $final_text);
				} elseif ($clear_undefined) {
					$final_text = str_replace($matches[0][$i], '', $final_text);
				}
				
			}
		}
		
		return $final_text;
	}
	
	
	public function make_text_from_template($template_name, $template_values, $clear_undefined=false) {
		$result_text = '';
		
		$template = $this->get_by_name($template_name);
		
		if ($template && isset($template['template'])) {
			$result_text = $this->set_template_values($template['template'], $template_values, $clear_undefined);
		}
		
		return $result_text;
	}
	
}

/* end of file */
