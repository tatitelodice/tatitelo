<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');


class Profile_Model_Ext extends CI_Model {
	
	public function fix_account_type_priorities($country, $gender) {
		
		if (stripos($gender, 'duo') !== false) {
			$gender = 'duo';
		}
		
		$sql_1 = 'SET @priorityPosition := 0;';
		$this->db->query($sql_1);

		$sql_2  = "UPDATE profiles p INNER JOIN ( ";
		$sql_2 .= "SELECT @priorityPosition := @priorityPosition + 1 AS newProPriority, proUser ";
		$sql_2 .= "FROM profiles WHERE proVerified = ".PROFILE_STATUS_VERIFIED." AND proDeleted=" .PROFILE_NOT_DELETED. " ";
		$sql_2 .= "AND proCountry = '".$country."' AND proGender LIKE '".$gender."%' ";
		$sql_2 .= "ORDER BY proAccountType DESC, proPriority ASC ";
		$sql_2 .= ") as np on p.proUser = np.proUser ";
		$sql_2 .= "SET p.proPriority = np.newProPriority;";

		$this->db->query($sql_2);
	}
	
	
	public function shake_priorities($country, $gender, $limit_to_account_type=false) {
		
		if (stripos($gender, 'duo') !== false) {
			$gender = 'duo';
		}
		
		if ($limit_to_account_type !== false) {
			$this->fix_account_type_priorities($country, $gender);
		}
				
		$sql_shake  = "UPDATE profiles ";
		$sql_shake .= "SET proPriority = FLOOR(1 + RAND() * 1000)";
		$sql_shake .= "WHERE proVerified = ".PROFILE_STATUS_VERIFIED." AND proDeleted=" .PROFILE_NOT_DELETED. " ";
		
		if ($limit_to_account_type !== false) {
			$sql_shake .= "AND proAccountType <= " . $limit_to_account_type . " ";
		}
		
		$sql_shake .= "AND proCountry = '".$country."' AND proGender LIKE '".$gender."%';";		
		
		//echo $sql_shake;
		$this->db->query($sql_shake);
		
		$this->fix_account_type_priorities($country, $gender);
	}
}

/* END OF FILE */