<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Profiles_Groups extends CI_Controller {
	
	public function __construct() {
		parent::__construct();
		
		$this->load->library('country');
		$this->load->library('gender');
		
		$this->load->model('profile_model');
		$this->load->model('profile_group_model');
		
		$this->load->model('admin_model');
		
		if (!$this->admin_model->login_exists()) {
			redirect ('padmin');
		}
	}
	
	public function index() {
		$this->list_groups();
	}
	
	
	public function list_groups() {
		$list_data = array();
		
		$list_data['success_message'] = $this->session->flashdata('success_message');
		$list_data['error_message'] = $this->session->flashdata('error_message');
		
		$groups = $this->profile_group_model->get_all_groups();
		
		$list_data['groups'] = $groups;
		$list_data['total_groups'] = count($groups);
		
		
		$this->load->view('padmin/header', array(
			'title' => 'Grupos' 
		));
		
		$this->load->view('padmin/subheader', array( 
			'title' => 'Grupos',
			'subtitle' => 'Listado Completo',
			'btn_back_text' => "Página Principal",
			'btn_back_url' => site_url("padmin")
		));
		
		$this->load->view('padmin/profiles_groups/list', $list_data);
		$this->load->view('padmin/footer');
		
	}
	
	
	public function create_group() {
		$create_data = array();
		
		$create_data['success_message'] = $this->session->flashdata('success_message');
		$create_data['error_message'] = $this->session->flashdata('error_message');
		
		if ($this->input->post('send')) {
			$this->form_validation->set_rules('name', 'Nombre', 'required|trim|is_unique[profile_group.name]');
			$this->form_validation->set_message('required', 'Ha dejado el %s vacío.');
			$this->form_validation->set_message('is_unique', 'Parece ser que "'.$this->input->post('name').'" ya existe, elige otro.');
			
			if ($this->form_validation->run() !== false) {
				$post = $this->input->post();

				if ($this->profile_group_model->create_group($post)) {
					$this->session->set_flashdata('success_message', 'El grupo "'.$post["name"].'" ha sido creado con éxito!');
					return redirect('padmin/profiles_groups');
				}
				
				$create_data['error_message'] = "Ha habido un error con la base de datos, intenta de nuevo.";
				
			} else {
				$create_data['error_message'] = "No se ha podido crear el grupo, revisa los campos.";
			}
		
		}
		
		$this->load->view('padmin/header', array(
			'title' => 'Grupos' 
		));
		
		$this->load->view('padmin/subheader', array( 
			'title' => 'Grupos',
			'subtitle' => 'Crear Grupo',
			'btn_back_text' => "Listado de Grupos",
			'btn_back_url' => site_url("padmin/profiles_groups")
		));
		
		$this->load->view('padmin/profiles_groups/create', $create_data);
		$this->load->view('padmin/footer');
	}
	
	
	public function edit_group($profile_group_id) {
		
	}
	
	
	
}


/* end of file */