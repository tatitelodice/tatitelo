<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Profiles_Related extends CI_Controller {
	
	public function __construct() {
		parent::__construct();
		
		$this->load->library('country');
		$this->load->library('gender');
		
		$this->load->model('profile_model');
		$this->load->model('profile_model2');
		
		$this->load->model('admin_model');
		
		if (!$this->admin_model->login_exists()) {
			redirect ('padmin');
		}
	}
	
	public function index() {
		$this->list_siblings();
	}
	
	
	
	public function list_siblings() {
		$this->load->library('pumbate_api');
		
		$account_types = $this->profile_model->get_account_types();
		
		$remote_profiles = $this->pumbate_api->get_profiles_by_user('uy', 'mujer');
		
		//print_r($remote_profiles);
		
		//echo count($remote_profiles);
		
		$this->pumbate_api->use_last_query = true;
		$remote_phones = $this->pumbate_api->get_users_by_phone('uy', 'mujer');
		
		$this->pumbate_api->use_last_query = true;
		$remote_emails =  $this->pumbate_api->get_users_by_email('uy', 'mujer');
		
		
		$local_profiles = $this->profile_model2->get_all(array(
			'fields'   => array("proId", "profile_group.name as group_name", "profiles.proUser", "proName", "proGender","proPhone", "proEmail", "proNumericPhones",
				          "proAccountType", "proVerified", 'proModifiedDate', 'proDeleted', 'proPriority'),
			'where'    => array(
				'proGender'   => 'mujer',
				'proCountry'  => 'uy',
				'proVerified' => 1,
				'proDeleted'  => 0,
			),
			'order_by' => 'proPriority asc',
			'force_underscore' => true,
			'include_administration' => true,
		));
		
		
		foreach ($local_profiles as $i => $profile) {
			$profile['account_type'] = $account_types[$profile['account_type']];
			
			$related_users = array();
			
			if ($profile['numeric_phones']) {
				$phones = explode(',', $profile['numeric_phones']);
				
				foreach ($phones as $ph) {
					if (isset($remote_phones[$ph])) {
						$related_users = array_merge($related_users,$remote_phones[$ph]);
					}
				}
			}
			
			if ($profile['email']) {
				if (isset($remote_emails[$profile['email']])) {
					$related_users = array_merge($related_users,$remote_emails[$profile['email']]);
				}
			}
						
			$related_users = array_unique($related_users);
			
			$related_summary = array(
				'total' => 0,
				'published' => 0,
				'unpublished' => 0,
			);
			
			
			foreach ($related_users as $user) {
				if (isset($remote_profiles[$user])) {
					$remote_profile = $remote_profiles[$user];
					
					if ($remote_profile['verified'] == PROFILE_STATUS_VERIFIED) {
						$related_summary['published']++;
					} else {
						$related_summary['unpublished']++;
					}
					
					$related_summary['total']++;
				}
			}
			
			$profile['related_summary'] = $related_summary;
			
			$local_profiles[$i] = $profile;
		}
		
		
		
		$siblings_data['profiles'] = $local_profiles;
		
		$siblings_data['remote_site_name'] = preg_replace('!\.com.*!i', '', $this->config->item('remote_site_domain'));
		$siblings_data['account_types'] = $account_types;
		
		
		$this->load->view('padmin/header', array('title' => 'Perfiles Relacionados' ));
		
		$this->load->view('padmin/subheader', array(
			'title' => 'Perfiles Relacionados',
			'subtitle' => 'Listado',
			'btn_back_text' => "Página Principal",
			'btn_back_url' => site_url("padmin"),
		));
		
		$this->load->view('padmin/profiles_related/siblings', $siblings_data);
		$this->load->view('padmin/footer');
	}
	

	
	public function details($profile_user) {
		$main_profile = false;
		
		if ($profile_user) {
			$main_profile = $this->profile_model->get_profile($profile_user);
		}
		
		if (!$main_profile) {
			redirect('padmin/profiles');
		}
		
		$details_data = array();
		
		$details_data['main_profile'] = $main_profile;
		
		$details_data['profiles_related'] = $this->_get_formated_related_profiles($main_profile);
				
		$this->load->view('padmin/header', array('title' => 'Perfiles Relacionados' ));
		
		$this->load->view('padmin/subheader', array(
			'title' => $main_profile['user'],
			'subtitle' => 'Perfiles Relacionados',
			'btn_back_text' => "Página Principal",
			'btn_back_url' => site_url("padmin"),
		));
		
		$this->load->view('padmin/profiles_related/details', $details_data);
		$this->load->view('padmin/footer');
		
	}
	
		
	private function _get_formated_related_profiles($main_profile) {
		$profiles = array();
		
		$dbrows = $this->profile_model->get_related_profiles(array(
			'phone' => $main_profile["phone"],
			'email' => $main_profile["email"],
			'country' => $main_profile["country"],
		));
		
		$account_types = $this->profile_model->get_account_types();
		
		foreach ($dbrows as $dbrow) {
			
			if ($dbrow['proUser'] == $main_profile['user']) {
				continue;
			}
			
			$clean_phone = trim(preg_replace( '![^0-9\-\_\s]!', '', $dbrow['proPhone']));

			if (strlen($clean_phone) < 8) {
				$clean_phone = 'NO PUBLICA TEL';	
			}

			$verified = $dbrow['proVerified'];
			$absent = $dbrow['proAbsent'] == 1;
			$deleted = $dbrow['proDeleted'] == PROFILE_DELETED;
			$account_type = $account_types[$dbrow['proAccountType']];

			$profile = array(
				'id' => $dbrow['proId'],
				'country' => $dbrow['proCountry'],
				'gender' => $dbrow['proGender'],
				'gender_symbol' => $this->gender->get_symbol($dbrow['proGender']),
				'name' => $dbrow["group_name"] . ' ' . $dbrow['proName'],
				'user' =>  $dbrow['proUser'],
				'phone' => $clean_phone,
				'registered' => date('d/m/Y', strtotime($dbrow['proCreatedDate'])),
				'verified' => $verified, //<span class="label label-verificar">Verificar</span>
				'absent' => $absent,
				'deleted' => $deleted,
				'account_type' => $account_type,
			);


			$profiles[] = $profile;
		}
		
		return $profiles;
	}
	
	
}


/* end of file */