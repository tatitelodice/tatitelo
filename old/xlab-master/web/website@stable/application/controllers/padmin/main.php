<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Main extends CI_Controller {

	public function __construct() {
		parent::__construct();
		$this->load->library('session');
		$this->load->model('admin_model');
		$this->load->model('security_model');
	}


	public function index() {
		if ($this->admin_model->login_exists()) {
			$this->_in_login();
		} else {
			$this->login();
		}
	}
	
	private function _in_login() {
		$subheader = array(
			'title' => 'Administración',
			'subtitle' => '',
			'btn_back' => '<a style="float: right;" class="btn" href="' . site_url('padmin/main/logout') . '">Salir del Panel</a>'
		);
		
		$this->load->view('padmin/header', array('title' => 'Principal'));
		$this->load->view('padmin/subheader', $subheader);
		
		$main_data = array(
			'admin_country' => $this->admin_model->get_country(),
			'quote' => $this->_welcome_fortune(),
		);
		
		$this->load->view('padmin/main', $main_data);
		
		$this->load->view('padmin/footer');
	}

	
	public function login() {
		
		if ($this->security_model->on_limit_request("admin_login", 50)) {
			$forbidden_message = "Jewish Kaboom! Llegaste al límite de intentos, no podés entrar, llama al Sysadmin.";
			$forbidden_message .= anchor("", "Volver a la portada");
			show_error($forbidden_message, 403, "Has llegado al límite de intentos!");
		}
		
		$subheader = array(
			'title' => 'Administración',
			'subtitle' => '',
			'btn_back' => '<a style="float: right;" class="btn" href="' . site_url('') . '">Salir del Panel</a>'
		);
		
		$login = array(
			'error' => '',
		);
		
		if ($this->input->post('send')) {
			$this->form_validation->set_message('required', 'No olvide ingresar su %s');
			$this->form_validation->set_rules('name', 'Nombre', 'trim|required');
			$this->form_validation->set_rules('password', 'Contraseña', 'trim|required');
			
			if ($this->form_validation->run() !== false) {
				
				$name = $this->input->post('name');
				$pass = $this->input->post('password');
				
				if ( $this->admin_model->login($name, $pass) ) {
					redirect('padmin');
				} else {
					$login['error'] = '<div class="alert alert-error input-large text-center"><button class="close" data-dismiss="alert">×</button>Su nombre ó contraseña ingresados no son correctos.</div>';
					$this->security_model->penalize_request("admin_login");
				}
			}
		}
		
		
		$this->load->view('padmin/header', array('title' => 'Login' ));
		$this->load->view('padmin/subheader', $subheader);
		$this->load->view('padmin/login', $login);
		$this->load->view('padmin/footer');
	}
	
	public function logout() {
		$this->admin_model->logout();
		redirect('padmin');
	}
	
	private function _welcome_fortune() {
		$fancy_domain = ucfirst(preg_replace('!^www\.!i', '', $this->config->item('site_domain')));
		
		$quotes = array(
			array( '¿Puedo palpar un poco a la señora?', 'Silvio Berlusconi'),
			array('Otra razón de peso para invertir en Italia es que tenemos bellísimas secretarias… chicas soberbias.', 'Silvio Berlusconi'),
			array('La izquierda no tiene gusto, ni siquiera cuando se trata de mujeres. Nuestras candidatas son más hermosas. En el Parlamento, no hay comparación.', 'Silvio Berlusconi'),
			array('Acá si que se coge.', 'Wilson acerca de ' . $fancy_domain),
			array('Un sondeo dice que el 33% de las jóvenes italianas sí se acostarían conmigo. El resto de las chicas contesta: ¿Otra vez?.', 'Silvio Berlusconi'),
			array('En medio de un trío con la señora Simon de Beavoir nos quedamos sin chicas, oh lala, busquemos más cognac en mi bodequín y más mujeres en '.$fancy_domain, 'Jean Paul Sartre'),
			array('...y las fiestas en Marbella, con todas esas chicas mojadas... ¡¿Acaso las putas no son alegría?!', "Natxo 'Torbe' Allende" ),
			array('...sacar y poner, es lo que cualquiera sabe hacer, pero no es lo único que hace al buen arte del coger', 'Anónimo Garchador'),
		);
		
		$pos = rand(0, count($quotes) - 1);
		
		return '“' . $quotes[$pos][0] . '”<small><cite>'.$quotes[$pos][1].'</cite></small>';
	}
	
	
}

/* end of file */