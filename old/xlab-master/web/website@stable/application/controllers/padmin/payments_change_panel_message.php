<?php

if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Payments_Change_Panel_Message extends CI_Controller {

	protected $admin_country = '';
	
	public function __construct() {
		parent::__construct();
	
		$this->load->library('gender');
		$this->load->library('country');
		
		$this->load->model('admin_model');

		if (!$this->admin_model->login_exists()) {
			redirect ('padmin');
		}

		$this->admin_country = $this->admin_model->get_country();
	}


	public function index()
	{
		$this->confirm_action();
	}


	public function confirm_action()
	{
		$users = $this->input->post("users");
		$new_panel_message = $this->input->post("new_panel_message");

		if (!$users)
		{
			redirect("padmin/payments/list_published");
		}

		$subheader_data = array(
			'title' => 'Pagos',
			'subtitle' => 'Setear mensaje en panel',
		);
				
		$done = false;
		
		if ($this->input->post("apply") == "yes") {
			$done = $this->apply_action($users, $new_panel_message);
		}
		
		$status_options = array(
			'' => "No cambiar",
			'hidden' => "Cambiar a OCULTOS",
			'visible' => "Cambiar a EN PORTADA"
		);
		
		$lpub_data = array(
			'users' => $users,
			'done' => $done,
			'new_panel_message' => $new_panel_message,
			'status_options' => $status_options,
		);
		
		$this->load->view('padmin/header', array('title' => 'Setear mensaje en panel'));
		$this->load->view('padmin/subheader', $subheader_data);
		$this->load->view('padmin/payments/page_change_panel_message', $lpub_data);
		$this->load->view('padmin/footer');
	}


	protected function apply_action($users, $new_panel_message)
	{
		$users_countries = array();
		$filtered_users = array();
		$this->db->where_in('proUser', $users);

		if ($this->admin_country) {
			$this->db->where('proCountry', $this->admin_country);
		}

		$res = $this->db->get('profiles');

		if ($res->num_rows()) {
			$res_array = $res->result_array();
			foreach ($res_array as $row) {
				$filtered_users[] = $row["proUser"];
				$users_countries[$row["proUser"]] = $row["proCountry"];
			}
		}

		$users = $filtered_users;
		
		$new_status = $this->input->post("new_status");
		
		$profiles_hidden = $new_status && $new_status == 'hidden';
		$profiles_visible = $new_status && $new_status == 'visible';
		
		if ($profiles_hidden) {
			$this->db->set('proVerified', PROFILE_STATUS_HIDDEN);
			$log_entry_type = 'profile_hidden';
		} elseif ($profiles_visible) {
			$this->db->set('proVerified', PROFILE_STATUS_VERIFIED);
			$log_entry_type = 'profile_visible';
		}
		
		$this->db->set("proPanelMessage", $new_panel_message);
		$this->db->where_in('proUser', $users);
		$done = $this->db->update("profiles");
		
		if ($profiles_hidden || $profiles_visible) {
			$this->load->model("log_model");
			
			foreach ($users as $user) {
				$this->log_model->insert_log($users_countries[$user], $log_entry_type, $user);
			}
		}

		return $done;
	}
}

/* eof */