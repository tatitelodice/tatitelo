<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Description of routes
 *
 * @author Santiago
 */
class Routes extends CI_Controller {

	public function __construct() {
		parent::__construct();
		$this->load->library('session');
		
		$this->load->model('admin_model');
		
		if (!$this->admin_model->login_exists()) {
			redirect ('padmin');
		}
	}

	public function index() {
		$this->_view();
	}
	
	private function _view(&$data = array()) {
		$subheader = array(
			'title' => 'Administración',
			'subtitle' => 'Rutas de usuarios',
			'btn_back' => '<a style="float: right;" class="btn" href="' . site_url('padmin/main/logout') . '">Salir del Panel</a>'
		);
		
		$this->load->view('padmin/header', array('title' => 'Rutas de Usuario'));
		$this->load->view('padmin/subheader', $subheader);
		
		$data['admin_country'] = $this->admin_model->get_country();
		
		$data['site_domain'] = preg_replace('!^www\.!i', '', $this->config->item('site_domain'));
		
		$this->load->view('padmin/routes', $data);
		
		$this->load->view('padmin/footer');
	}
	
	
	public function update() {
		//$this->load->library('routeupdater');
		//$this->routeupdater->update();
		
		$data['message'] = 'success_route';
		
		$this->_view($data);
	}
	
	public function remod() {
		$user = $this->input->post('user');
		
		if ($user && file_exists( './userdata/' . $user)) {
			$this->load->helper('directory');
			$this->load->helper('directorycopy');

			directory_public('userdata/'.$user, 0777);

			$data['message'] = 'success_chmod';
			
		} else {
			$data['message'] = 'error_chmod';
		}
		
		$this->_view($data);

	}
	
}

?>
