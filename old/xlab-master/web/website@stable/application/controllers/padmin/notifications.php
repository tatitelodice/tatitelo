<?php

if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Notifications extends CI_Controller {

	public function __construct() {
		parent::__construct();
		
		$this->load->library("human_useragent");
		
		$this->load->helper("profile_helper");
		
		$this->load->model('profile_model');
		$this->load->model('admin_model');
		
		if (!$this->admin_model->login_exists()) {
			redirect ('padmin');
		}
	}	
	
	public function index() {
		$this->load->library('gender');
		$this->load->library('sdate');
		
		$this->db->select('logId,logDate,logDescription,logAddress,logMeta,logs.proUser,proCountry,proGender,proPhone,proVerified,proId,proAccountType,proEmail');
		$this->db->where('logNotify', 1);
		$this->db->where('logDate <=', date('Y-m-d') . ' 23:59:59');
		$this->db->join('profiles', 'profiles.proUser = logs.proUser');
		
		$this->db->order_by('logId', 'desc');
		
		$admin_country = $this->admin_model->get_country();
		
		if ($admin_country) {
			$this->db->where('proCountry', $admin_country);
		}
		
		$results = $this->db->get('logs');
		
		$notifications_data = array(
			'notifications' => array(),
			'admin_country' => $admin_country,
		);
		
		if ($results->num_rows() > 0) {
			
			foreach ($results->result_array() as $row) {
				//print_r($row); die;
				
				$notification = array(
					'id' => $row['logId'],
					'description' => $row['logDescription'],
					'user' => $row['proUser'],
					'profile_id' => $row['proId'],
					'country' => $row['proCountry'],
					'phone' => $row["proPhone"],
					'email' => $row["proEmail"],
					'gender_symbol' => $this->gender->get_symbol($row['proGender']),
					'date' =>  $this->sdate->formatoYMDaLiteral($row['logDate'], true),
					'ip' => $row['logAddress'],
					'verified' => $row['proVerified'],
					'uploaded_pictures' => 0,
					'text' => '',
					'css_class' => '',
					'meta' => array(),
					'account_type' => $row['proAccountType'],
				);
				
				$json_meta = json_decode($row["logMeta"], true);
				if (is_array($json_meta)) {
					$notification["meta"] = $json_meta;
				}
								
				switch ($notification['description']) {
					case "profile_created":
					case "profile_notice_change":
					case "profile_checktime":
						$this->_fill_profile_status_notification($notification);
						break;
				}
				
				$notifications_data['notifications'][] = $notification;	
			}
		}
		
		
		$this->load->view('padmin/header', array('title' => 'Notificaciones' ));
		
		$subheader = array(
			'title' => 'Notificaciones',
			'subtitle' => 'Listado Completo',
			'btn_back' => '<a style="float: right;" class="btn" href="' . site_url('padmin') . '">Página Principal</a>'
		);
		
		$this->load->view('padmin/subheader', $subheader);
		
		$this->load->view('padmin/notifications/page_index', $notifications_data);
		
		$this->load->view('padmin/footer');
		
	}
	
	
	protected function _fill_profile_status_notification(&$notification) {
		$user = $notification['user'];
		$edit_profile_path = 'padmin/profiles/edit/' . $notification['profile_id'];
		
		$templates = array();
		$templates["profile_created"] = "Nuevo usuario ";
		
		if ($notification['description'] == 'profile_notice_change') {
			$templates["profile_routine"] = "Admin modificó el perfil ";
		} else {
			$templates["profile_routine"] = "Revisión de rutina ";
		}
		
		$templates['user_link'] = '<b>' . anchor($notification['user'], $notification['user'], 'target="_blank"') . '</b>.';
		$templates['profile_hidden'] =  'Está ' . anchor('padmin/profiles/edit/' . $notification['profile_id'], ' oculto', 'target="_blank"') . '.';
		$templates['profile_verified'] =  'Está ' . anchor($edit_profile_path, 'verificado', 'target="_blank"') . '.';
		$templates['profile_unverified'] =  'Queda pendiente de ' . anchor($edit_profile_path, 'verificación', 'target="_blank"') . '.';
		$templates['trial_limit'] = '<b>Avisar límite de prueba.</b>';
		
		if (!empty($notification["meta"]["useragent"])) {
			$templates["human_user_agent"] = " Utilizó " . $this->human_useragent->get_human_ua( $notification["meta"]["useragent"]) . ".";
		}
		
		//css_class red para revision de rutina
		$notification["uploaded_pictures"] = count(get_profile_pictures($user));
		$has_uploaded_pictures = $notification['uploaded_pictures'] > 0;

		if ($has_uploaded_pictures) {
			$templates['pictures'] = '<b>Subió '.$notification['uploaded_pictures'].' foto' . (($notification['uploaded_pictures'] > 1)? "s" : "") .'.</b>';
		} else {
			$templates['pictures'] = '<b>NO Subió fotos.</b>';
		}
		
		$related_profiles = count($this->profile_model->get_related_profiles(array(
			'phone' => $notification["phone"],
			'email' => $notification["email"],
			'country' => $notification["country"],
		)));
		
		
		if ($related_profiles > 1) {
			$templates["related_profiles"]  = '<a style="color: inherit;" href="'.site_url('padmin/profiles_related/details/'.$notification['user']).'">';
			$templates["related_profiles"] .= '<b>Tuvo cuentas anteriormente.</b></a>';
		}
		
		if ($notification['description'] == 'profile_notice_change') {
			$notification['css_class'] = '';
		} else {
			$notification['css_class'] = 'green';
		}
		
		if ($notification['description'] == "profile_created") {
			$parts = array("profile_created", "user_link", "human_user_agent", "pictures", "related_profiles");
		} else {
			$parts = array("profile_routine", "user_link");
		}
		
		if ($notification['verified'] == PROFILE_STATUS_HIDDEN) {
			$parts[] = "profile_hidden";
			$notification['css_class'] = ($notification['description'] == 'profile_checktime')? 'green' : 'red';

		} elseif ($notification['verified']) {
			$parts[] = "profile_verified";

			if ($notification['description'] == 'profile_checktime') {
				if ($notification['account_type'] == PROFILE_FREE) {
					$parts[] = "trial_limit";
				}			
			}
		} else {
			$notification['css_class'] = ($notification['description'] == 'profile_checktime' && $has_uploaded_pictures)? 'green' : 'red';
			$parts[] = "profile_unverified";
		}
		
		foreach ($parts as $part) {
			if (!empty($templates[$part])) {
				$notification["text"] .= " " . $templates[$part];
			}
		}
	}
	
	
	public function mark_as_read($logId) {
		$result = 'failed';
		
		$is_ajax = $this->input->get('ajax') === 'true';
		$action_allowed = !$this->admin_model->get_setting('disallow_profile_upgrading');
		
		if ($action_allowed) {			
			$this->db->set('logNotify', 0);
			$this->db->where('logId', $logId);
			$this->db->update('logs');
			$result = 'success';	
		} else {
			$result = 'disallowed';
		}
				
		if ($is_ajax) {
			echo $result;
		} else {
			redirect('padmin/notifications');
		}
	}
	
}

/* end of file */
