<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Profiles extends CI_Controller {

	public function __construct() {
		parent::__construct();
		
		$this->load->library('country');
		$this->load->library('gender');
		$this->load->library('session');
		$this->load->library('contact');
		
		$this->load->model('profile_model');
		$this->load->model('profile_group_model');
		
		$this->load->model('admin_model');
		$this->load->model('message_templates_model');

		$this->load->library("profile/profile_pictures_service");  
		
		if (!$this->admin_model->login_exists()) {
			redirect ('padmin');
		}
	}
	
	
	public function index() {
		$this->list_accounts();
	}
	
	
	public function list_accounts($list_start_index = 0) {
		$account_types = $this->profile_model->get_account_types();
		

		$admin['terms'] = '';
		$admin['accounts'] = array();
		
		$admin['filter_country'] = $this->admin_model->get_setting('filter_country');
		$admin['country_options'] = array_merge( array( '' => 'cualquier país' ), $this->country->get_countries());
				
		$admin['filter_gender'] = $this->admin_model->get_setting('filter_gender');
		
		$admin['gender_options'] = array(
			'' => 'todos', 
			'mujer' => 'mujeres',
			'trans' => 'trans',
			'hombre' => 'hombres',
			'duo' => 'duos',
		);
			
		$admin['filter_order'] = $this->admin_model->get_setting('filter_order');
		$admin['order_options'] = array( 
			"galleryasc" => 'galería primeros a últimos',
			"gallerydesc" => 'galería últimos a primeros',
			"recentfirst" => 'más recientes primero', 
			"recentlast" => 'más antiguos primero',);
			
		$admin['filter_status'] = $this->admin_model->get_setting('filter_status');
		$admin['status_options'] = array(
			'' => 'cualquiera',
			"verified" => 'verificado portada',
			#"absent" => 'verificado y ausente',
			"hidden" => 'verificado oculto',
			'notverified' => 'no verificado',
			'deleted' => 'eliminado',
		);
		
		$fields = array(
			'profile_group' => 'profile_group.name as group_name',
			'clients'       => 'cliPhone,profile_group_id as group_id',
			'profiles'      => 'profiles.proUser, proId, proEmail,proCountry,proName,proAccountType,proVerified,proDeleted,proPhone,proAbsent,proGender,proCreatedDate',
		); 
		
		$this->db->select(implode(',', $fields));
		
		$this->db->from('clients');
		$this->db->join('profiles', 'clients.proUser = profiles.proUser');
		$this->db->join('profile_group', 'clients.profile_group_id = profile_group.id', 'left outer');
			
		$terms_get = $this->input->get('terms');
	
		if ($terms_get) {
			$terms = trim(urldecode($terms_get));
			$admin['terms'] = $terms;

			if (preg_match('!@!i', $terms)) {
				$this->db->like( 'proEmail', $terms );

			} elseif (preg_match('!^[0-9\s]+$!', $terms)) {
				$this->db->like( 'clients.cliPhone', $terms );
				$this->db->or_like( 'proNumericPhones', preg_replace('![^0-9]!', '', $terms) );

			} else {
				$terms = preg_split('! +!', $terms);
				
				$like = "( clients.proUser LIKE '%".$terms[0]."%' OR (";

				for ($i=0; $i < count($terms); $i++) {
					$concat = "CONCAT_WS(' ',profile_group.name,proName) LIKE '%".$terms[$i]."%'";
					
					if ($i > 0) {
						$like .= " AND ";
					}
					
					$like .= $concat;
				}

				$like .= "))";

				$this->db->where($like);				
			}
		}		

		if ($admin['filter_gender']) {
			$this->db->like('proGender', $admin['filter_gender'], 'after');
		}
		
		$admin_country = $this->admin_model->get_country();

		if ($admin['filter_country'] && !$admin_country) {
			$this->db->where('proCountry', $admin['filter_country']);
		}

		if ($admin['filter_status'] == 'deleted') {
			$this->db->where('proDeleted', PROFILE_DELETED);
		} else {
			$this->db->where('proDeleted', PROFILE_NOT_DELETED);

			switch ($admin['filter_status']) {		
				case 'verified' :
					$this->db->where('proVerified', PROFILE_STATUS_VERIFIED);
					break;
				
				case 'hidden' :
					$this->db->where('proVerified', PROFILE_STATUS_HIDDEN);
					break;
				
				case 'notverified' :
					$this->db->where('proVerified', PROFILE_STATUS_NOT_VERIFIED);
					break;
				
				/* QUEDA OCULTO POR AHORA DE PORTADA */
				case 'absent' :
					$this->db->where('proAbsent', 1);
					break;
			}
		}

		if ($admin['filter_order'] == 'recentfirst') {
			
			$this->db->order_by('proId', 'desc');
			
		} elseif ($admin['filter_order'] == 'galleryasc') {	

			$this->db->order_by('proVerified = 1 desc, proPriority asc');
			
		} elseif ($admin['filter_order'] == 'gallerydesc') {		
			
			$this->db->order_by('proVerified = 1 desc, proPriority desc');
			
		} else {
			$this->db->order_by('proId', 'asc');
		}
		
		if ($admin_country) {
			$admin['admin_country'] = $admin_country;
			$this->db->where('proCountry', $admin_country);
		}
		
		$this->db->limit(100, $list_start_index);
		
		$resultset = $this->db->get();
		$dbrows = $resultset->result_array();
		
		$sentencia_sql_ilimitada = preg_replace('!LIMIT +\d+.*!', '', $this->db->last_query());

		$list_total = $this->db->query($sentencia_sql_ilimitada)->num_rows();
		$admin['list_total'] = $list_total;
		
		if ($list_total) {
			$accounts = array();
			
			$admin['list_start_index'] = $list_start_index + 1;			
			$list_end_index = $list_start_index + 100;
			
			if ($list_end_index >= $list_total) {
				$list_end_index = $list_total;
			}
			
			$admin['list_end_index'] = $list_end_index;
			
			$index = $list_start_index;
			$move_index = 1;
			
			if ($admin['filter_order'] == 'recentfirst' || $admin['filter_order'] == 'gallerydesc' ) {
				
				$index = $list_total - $list_start_index + 1;
				$move_index = -1;
			}
			
			foreach ($dbrows as $dbrow) {
				$index += $move_index;
								
				$clean_phone = trim(preg_replace( '![^0-9\-\_\s]!', '', $dbrow['proPhone']));
				
				if (strlen($clean_phone) < 8) {
					$clean_phone = 'NO PUBLICA TEL';	
				}
				
				$verified = $dbrow['proVerified'];
				$absent = $dbrow['proAbsent'] == 1;
				$deleted = $dbrow['proDeleted'] == PROFILE_DELETED;
				$account_type = $account_types[$dbrow['proAccountType']];
				
				$row_account = array(
					'id' => $dbrow['proId'],
					'list_index' => $index,
					'country' => $dbrow['proCountry'],
					'gender' => $dbrow['proGender'],
					'gender_symbol' => $this->gender->get_symbol($dbrow['proGender']),
					'name' => $dbrow["group_name"] . ' ' . $dbrow['proName'],
					'user' =>  $dbrow['proUser'],
					'phone' => $clean_phone,
					'registered' => date('d/m/Y', strtotime($dbrow['proCreatedDate'])),
					'verified' => $verified, //<span class="label label-verificar">Verificar</span>
					'absent' => $absent,
					'deleted' => $deleted,
					'account_type' => $account_type,
				);
				
				
				$accounts[] = $row_account;
			}
			
			$admin["accounts"] = $accounts;
		}
		
		$this->load->view('padmin/header', array(
			'title' => 'Perfiles' )
		);
		
		$this->load->view('padmin/subheader', array(
			'title' => 'Perfiles',
			'subtitle' => 'Listado Completo',
			'btn_back_text' => "Página Principal",
			'btn_back_url' => site_url("padmin"),
		));
		
		$this->load->view('padmin/profiles/list', $admin);
		$this->load->view('padmin/footer');
	}
	
	
	
	public function change_filters() {
		$filters = array(
			'filter_country', 'filter_gender', 'filter_order', 'filter_status'
		);
		
		foreach ($filters as $filter) {
			$this->admin_model->set_setting($filter, $this->input->post($filter));
		}
		
		$this->admin_model->save_settings();
		
		redirect('padmin/profiles/list_accounts/0/?terms=' . urlencode($this->input->post('terms')));
	}
	
	
	
	public function delete($user = false) {
		$user = strtolower($user);
		
		if ($user) {
			$row = $this->profile_model->get_full_profile($user);
			
			if ($row && $row['proDeleted'] == PROFILE_NOT_DELETED) {
				$id = $row['proId'];
				
				$new_user = 'deleted-' . $id . '-' . $user;
				
				$this->db->set('proUser', $new_user);
				$this->db->set('proVerified', PROFILE_STATUS_NOT_VERIFIED);
				$this->db->set('proDeleted', PROFILE_DELETED);
				$this->db->set('proPriority', 0);
				
				$this->db->where('proUser', $user);
				$this->db->update('profiles');

				$this->profile_model->rename_profile($user, $new_user);
				
				if ($row['proVerified'] == PROFILE_STATUS_VERIFIED ) {
					$this->profile_model->fix_group_priorities(
						$row['proCountry']
					,	$row['proGender']
					,	$row['proAccountType']
					);
				}
				
				$this->load->model('log_model');
				$this->log_model->insert_log($row['proCountry'], 'profile_deleted', $new_user);
				
				echo 'El usuario '.$user.' ha sido marcado como borrado y renombrado a '.$new_user;
			} else {
				
				if (!$row) {
					echo 'El usuario no existe.';
				} else {
					echo 'El usuario ya está marcado como borrado.';
				}
			}
			
		} else {
			echo 'En espera de parametros.';
		}
	}
	
	
	public function undelete($user = false) {
		$user = strtolower($user);
		
		if ($user) {
			$row = $this->profile_model->get_full_profile($user);
			
			if ($row) {
				$this->_check_restricted_country($row['proCountry']);
				
				$new_user = preg_replace('!deleted-\d+-(.*)!i', '$1', $user);
								
				if ($this->profile_model->get_profile($new_user)) {
					$new_user = preg_replace('!deleted-(\d+-.*)!i', '$1', $user);
				}
				
				$new_position = $this->profile_model->get_positions_count($row['proCountry'], $row['proGender']) + 1;
				$this->db->set('proPriority', $new_position);
				
				$this->db->set('proVerified', 0);
				$this->db->set('proDeleted', 0);
				$this->db->where('proUser', $user);
				
				$this->db->update('profiles');

				$this->profile_model->rename_profile($user, $new_user);
				
				$this->load->model('log_model');
				$this->log_model->insert_log($row['proCountry'], 'profile_undeleted', $new_user);
				
				$redirect = $this->input->get('redirect');
				
				if ($redirect == 'edit') {
					return redirect('padmin/profiles/edit/'. $row['proId']);
					
				} else {
					echo 'El usuario <b>'.$user.'</b> ha sido restaurado a <b>'.$new_user . '</b>';
				}
					
			} else {
				echo 'En espera de parametros.';
			}
			
		}
	}
	
	
	public function information($user) {
		$data = $this->profile_model->get_full_profile($user);
		
		$this->load->library('table');
		$this->table->set_template( array( 'table_open' => '<table class="table table-condensed table-striped table-clients">',));
		$this->table->set_heading('name', 'value');
		
		$table_info = array();
		
		foreach ($data as $k => $v) {
			$table_info[] = array($k, $v);
		}
		
		$client_information['table'] = $this->table->generate($table_info);
		
		$subheader = array(
			'title' => $user,
			'subtitle' => 'Información de Usuario',
			'btn_back' => '<a style="float: right;" class="btn" href="' . site_url('padmin') . '">Página Principal</a>'
		);
		
		$this->load->view('padmin/header', array('title' => 'Información de ' . $user));
		$this->load->view('padmin/subheader', $subheader);
		
		$this->load->view('padmin/client_information', $client_information);
		
		$this->load->view('padmin/footer');
	}
	
	
	public function valid_username($username) {
		$banned_list = array(
			'application', 'system', 'userdata', 'admin', 'padmin', 'panel', 'cpanel',
			'main', 'promomain', 'escort', 'profile', 'site', 'login', 'welcome', 'comingsoon', 
			'uruguay', 'colombia', 'argentina', 'brasil',
		);
		
		return !in_array($username, $banned_list);
	}
	
	
	public function change_username($user = false) {
		
		$subheader = array(
			'title' => $user,
			'subtitle' => 'Cambiar usuario',
			'btn_back' => '<a style="float: right;" class="btn" href="' . site_url('padmin/main/logout') . '">Salir del Panel</a>'
		);
		
		$data = array(
			'user' => $user,
			'message' => '',
			'site_domain' => $this->config->item('site_domain'),
		);		
		
		$profile = $this->profile_model->get_full_profile($user);
		
		if ($user && $profile !== false) {		
			
			$this->_check_restricted_country($profile['proCountry']);
				
			$this->form_validation->set_rules('new_user', 'nuevo usuario', 'required|trim|alpha_dash|callback_valid_username|is_unique[clients.proUser]');

			$this->form_validation->set_message('required', 'El campo no puede quedar vacío.');
			$this->form_validation->set_message('is_unique', '"'.$this->input->post('new_user').'" ya existe, elige otro.');
			$this->form_validation->set_message('valid_username', '"'.$this->input->post('new_user').'" ya existe, elige otro.');
			$this->form_validation->set_message('alpha_dash', 'El %s sólo puede tener letras, números ó guiones (sin espacios).');

			if ( $this->form_validation->run() !== false ) {
				$new_user = $this->input->post('new_user');

				$this->profile_model->rename_profile($user, $new_user);
				
				if ($profile['proEmail'] != '' && $this->config->item("email_notification_users")) {
					
					$contact_mail = $this->contact->get_mail($profile['proCountry']);
					
					$template_vars = array(
						'name' => $profile["proUser"],
						'user' => $user,
						'new_user' => $new_user,
						'phone_line' => '',
						'email' => $contact_mail,
						'country' => $this->country->name($profile['proCountry']),
					);
					
					$site_phone = $this->contact->get_phone($profile['proCountry']);
					
					if ($site_phone) {
						$template_vars['phone_line'] = "También puedes llamarnos o mandarnos mensaje al: ".$site_phone;
					}
					
					$email_text = $this->message_templates_model->make_text_from_template('email_profile_renamed', $template_vars);
					$email_title = 'Tu usuario de ' . $this->config->item('site_domain_simple') . ' ahora es ' . $new_user;
					
					//print_r($email_text); die;
					
					$this->load->library('mail_notification');
					
					$contact_name = $this->config->item('site_name') . ' Escorts';
					$this->mail_notification->set_mail_from($contact_mail, $contact_name);
					$this->mail_notification->set_mail_reply_to($contact_mail, $contact_name);
					
					$this->mail_notification->send_mail( 
						array( $profile['proEmail'] => $profile['proName'] ),
						$email_title,
						$email_text
					);
				}
				
				$data['old_user'] = $user;
				$data['user'] = $new_user;
				$data['message'] = 'success';
				
				$subheader['title'] = $new_user;
			}
		} else {
			$data['message'] = 'no_user';
		}
		
		$this->load->view('padmin/header', array('title' => 'Cambiar usuario'));
		$this->load->view('padmin/subheader', $subheader);
		
		$this->load->view('padmin/change_username', $data);
		
		$this->load->view('padmin/footer');
	}
	

	
	public function reset_password($user) {
		
		$profile = $this->profile_model->get_profile($user);
		
		if ($profile) {
			$this->_check_restricted_country($profile['country']);
		} else {
			$this->_no_profile();
		}
		
		if ($this->input->post('reset') == 'yes' && $user) {
			$this->profile_model->delete_restore_code($user);
			$codes = $this->profile_model->create_restore_code($user, 10);
			
			if (is_array($codes)) {				
				redirect('padmin/profiles/reset_password/'.$user.'?success');
			} else {
				redirect('padmin/profiles/reset_password/'.$user);
			}
		}
		
		
		$subheader = array(
			'title' => $user,
			'subtitle' => 'Resetear password',
			'btn_back' => '<a style="float: right;" class="btn" href="' . site_url('padmin/main/logout') . '">Salir del Panel</a>'
		);
		
		$data = array(
			'user' => $user,
			'message' => '',
			'has_restore_code' => false,
		);		
		
		if ($this->input->get('success')) {
			$data['message'] = 'success';
		}

		$restore_info = $this->profile_model->get_current_restore_code($user);
		
		if ($restore_info) {
			$data['has_restore_code'] = true;
			$data['new_password_url'] = site_url("restore/new_password/" . $restore_info["restore_hash"]);
			$data['restore_code'] = $restore_info["restore_code"];

			$this->load->library('sdate');
			$data['restore_expires'] = $this->sdate->formatoYMDaLiteral($restore_info["restore_expires"]);
		}
				
		$this->load->view('padmin/header', array('title' => 'Resetear password'));
		$this->load->view('padmin/subheader', $subheader);
		
		$this->load->view('padmin/reset_password', $data);
		
		$this->load->view('padmin/footer');
		
	}
	
	
	private function _load_edit_validation_rules() {
		$this->form_validation->set_message('required', 'Ha puesto el campo %s como vacío.');
		$this->form_validation->set_message('valid_email', 'El e-mail ingresado no está bien escrito.');
		$this->form_validation->set_message('integer', '%s debe tener un valor numérico.');

		$this->form_validation->set_rules('id', 'Id', 'required|integer');

		$this->form_validation->set_rules('source', 'Fuente', 'trim');
		$this->form_validation->set_rules('group', 'Grupo', 'trim');
		$this->form_validation->set_rules('phone', 'Telefono', 'trim|required');
		$this->form_validation->set_rules('email', 'E-mail', 'trim|strtolower|valid_email');

		$this->form_validation->set_rules('panel_message', 'Mensaje personalizado para el panel', 'trim|min_length[10]');

		$this->form_validation->set_rules('account_type', 'Tipo de Cuenta', 'trim|required|integer');

		$this->form_validation->set_rules('gender', 'Género', 'trim|required|alpha');
		$this->form_validation->set_rules('country', 'País', 'trim|required|alpha|max_length[2]');

		$this->form_validation->set_rules('absent', 'Ausente', 'trim|integer');
		$this->form_validation->set_rules('absent_message', 'Mensaje de ausencia', 'trim');

		$this->form_validation->set_rules('verified', 'Verificado', 'trim|required|integer');
		$this->form_validation->set_rules('priority', 'Prioridad', 'trim|required|integer');
		$this->form_validation->set_rules('comments', 'Comentarios', 'trim');

		$this->form_validation->set_rules('verified_pictures', 'Estado de Fotos', 'trim|required|integer');
	}
	
	
	private function _do_edit_profile($previous_data) {
		$DISALLOW_CHANGES = $this->admin_model->get_setting('disallow_profile_upgrading');
		
		$user = $previous_data['user'];
		
		$post = $this->input->post();

		$this->db->set('profile_group_id', $post['group']);
		$this->db->set('cliPhone', $post['phone']);

		$this->db->set('cliComments', $post['comments']);
		$this->db->set('cliSource', $post['source']);

		$this->db->where('proUser', $user);
		$this->db->update('clients');

		$log_verification = false;
		$log_verification_hidden = false;
		$log_verification_visible = false;

		if ($previous_data['verified'] != $post['verified']) {
			$this->db->set('proVerified', $post['verified']);

			if ($post['verified'] == PROFILE_STATUS_NOT_VERIFIED) {
				$this->db->set('proPriority', 0);
				$post['priority'] = 0;

			} elseif ( $previous_data['verified'] == PROFILE_STATUS_NOT_VERIFIED && $post['verified'] == PROFILE_STATUS_VERIFIED) {

				$post['priority'] = 'last';

				/* SI NUNCA FUE VERIFICADO Y SE VERIFICA POR PRIMERA VEZ
				   MANDAMOS ALERTA POR E-MAIL */
				$this->load->helper('email');

				if (valid_email($post['email'])) {
					$this->load->library('mail_notification');

					$contact_mail = $this->contact->get_mail($post['country']);

					$template_vars = array(
						'name' => $user,
						'user' => $user,
						'phone_line' => '',
						'email' => $contact_mail,
						'country' => $this->country->name($post['country']),
					);

					$site_phone = $this->contact->get_phone($post['country']);

					if ($site_phone) {
						$template_vars['phone_line'] = "También puedes llamarnos o mandarnos mensaje al: ".$site_phone;
					}

					$alert_text = $this->message_templates_model->make_text_from_template('email_profile_verified', $template_vars);

					$alert_title = '¿Te enteraste? Ya apareces en ' . $this->config->item('site_domain_simple');

					$contact_name = $this->config->item('site_name') . ' Escorts';
					$this->mail_notification->set_mail_from($contact_mail, $contact_name);
					$this->mail_notification->set_mail_reply_to($contact_mail, $contact_name);

					$this->mail_notification->send_mail( 
						array( $post['email'] => $previous_data['name'] ),
						$alert_title,
						$alert_text
					);
				}

				$log_verification = true;

			} elseif ( $previous_data['verified'] == PROFILE_STATUS_HIDDEN && $post['verified'] == PROFILE_STATUS_VERIFIED) {
				$log_verification_visible = true;

			} elseif ($post['verified'] == PROFILE_STATUS_HIDDEN) {
				$log_verification_hidden = true;
			}
		}

		$this->db->set('proEmail', $post['email']);

		if (!$DISALLOW_CHANGES) {
			$this->db->set('proAccountType', $post['account_type']);
			$this->db->set('proVerifiedPictures', $post['verified_pictures']);
		}

		$this->db->set('proGender', $post['gender']);
		$this->db->set('proCountry', $post['country']);

		if ($previous_data['account_type'] > $post['account_type']) {
			$this->db->set('proTheme', 'amaranth');
		}

		$this->db->set('proAbsent', $post['absent']);
		$this->db->set('proAbsentMessage', $post['absent_message']);

		$panel_message = preg_replace('!^[\r\n\s\t]+|[\r\n\s\t]+$!', '', $post['panel_message']);
		$this->db->set('proPanelMessage', $panel_message);

		$this->db->where('proId', $post['id']);

		$this->db->update('profiles');

		/* LOG ACTIONS */
		$this->load->model('log_model');

		if ($log_verification && $post['country'] == 'uy') {
			/* REGISTRAMOS EN LOG DE VERIFICACION Y ALERTA A FUTURO */
			$this->log_model->insert_log($post['country'], 'profile_verified', $user, false);
			$future_date = date('Y-m-d H:i:s', strtotime('+7 day'));
			$this->log_model->insert_log($post['country'], 'profile_checktime', $user, true, $future_date);
		} elseif ($log_verification_hidden) {	
			$this->log_model->insert_log($post['country'], 'profile_hidden', $user);
		} elseif ($log_verification_visible) {	
			$this->log_model->insert_log($post['country'], 'profile_visible', $user);
		}
		/* END OF LOG ACTIONS */

		$changed_priority = $post['verified'] == PROFILE_STATUS_VERIFIED && $previous_data['priority'] != $post['priority'];
		$changed_verification = $previous_data['verified'] != $post['verified'];
		$changed_account_type = $previous_data['account_type'] != $post['account_type'];

		
		if ( $changed_priority || $changed_verification || $changed_account_type ) {
			//echo '<h1>Cambian verificaciones</h1>';

			if ($DISALLOW_CHANGES) {
				$post['priority'] = $previous_data['priority'];
				
				if ($changed_verification) {
					$this->log_model->insert_log($post['country'], 'profile_notice_change', $user, true);
				}
			}			
			
			if ($post['priority'] > $previous_data['max_priority']) {
				$post['priority'] = 'last';//$previous_data['max_priority'];
			}

			if ($changed_account_type)
			{
				$post['priority'] = 'last';
			}
			
			//echo  $post['priority'];
			if ($post['verified'] == PROFILE_STATUS_VERIFIED) {
				$positions = $this->profile_model->fix_group_priorities(
					$post['country']
				,	$post['gender']
				,	$post['account_type']
				,	$user
				,	$post['priority']
				);
			} else {
				/* SE AJUSTAN POSICIONES SIN EL USUARIO YA QUE NO APARECE EN GALERIA */
				$positions = $this->profile_model->fix_group_priorities(
					$post['country']
				,	$post['gender']
				,	$post['account_type']
				,	false
				,	false
				);
			}
			//echo $positions[0]; die;
		}
	}
	
	
	
	public function edit($proId=false) {
		$client = false;
		
		if ($proId) {
			$client = $this->profile_model->get_full_profile_by_id($proId);
		}
		
		if (!$client) {
			return redirect('padmin/profiles');
		}
			
		$this->_check_restricted_country($client['proCountry']);

		$priority_count = $this->profile_model->get_positions_count($client['proCountry'], $client['proGender'], $client['proAccountType']);

		if ($priority_count == 0) {
			$edit_account['max_priority'] = 1;
		} else {
			$edit_account['max_priority'] = $priority_count;
		}

		$header['title'] = $client["proUser"] . ' - Editar Cuenta';

		$subheader['title'] = 'Cuentas';
		$subheader['subtitle'] = 'Editar cuenta';

		$subheader['btn_back'] = '<a style="margin-left: 10px;" class="btn pull-right" href="' . site_url('padmin/profiles') . '">Listado de Cuentas</a>';
		$subheader['btn_back'] .= '<a class="btn pull-right" href="' . site_url('padmin/payments/view/' . $client['proUser'] . '?ref=payments') . '">Pagos de Usuario</a>';

		$edit_account['success_message'] = $this->session->flashdata('success_message');
		$edit_account['error_message'] = $this->session->flashdata('error_message');
		
		$edit_account['id'] = $proId;
		$edit_account['user'] = $client['proUser'];
		$edit_account['name'] = $client['proName'];

		$edit_account['source'] = $client['cliSource'];

		$this->load->library('sdate');
		$edit_account['createdDate'] = $this->sdate->formatoYMDaLiteral( date('Y-m-d', strtotime($client['proCreatedDate'])));
		$edit_account['createdDate'] .= ' a las ' .  date('H:i', strtotime($client['proCreatedDate'])) . ' horas.';

		$edit_account['group_options'] = array(
			0 => 'Ninguno'
		);
		
		foreach ($this->profile_group_model->get_all_groups() as $p_group) {
			$edit_account['group_options'][$p_group['id']] = $p_group['name'];
		}
		
		$edit_account['group'] = $client['profile_group_id'];
		
		
		$edit_account['phone'] = $client['cliPhone'];
		$edit_account['email'] = $client['proEmail'];

		$edit_account['absent_options'] = array(
			0 => 'No',
			1 => 'Sí',
		);

		$edit_account['absent'] = $client['proAbsent'];
		$edit_account['absent_message'] = $client['proAbsentMessage'];


		$edit_account['account_type_options'] = $this->profile_model->get_account_types();

		$edit_account['account_type'] = $client['proAccountType'];


		$edit_account['gender_options'] = $this->gender->get_genders();

		$edit_account['gender'] = $client['proGender'];

		$edit_account['country_options'] = $this->country->get_countries();

		$edit_account['country'] = $client['proCountry'];

		$edit_account['verified_options'] = array(
			PROFILE_STATUS_NOT_VERIFIED => 'No',
			PROFILE_STATUS_VERIFIED => 'Sí, en portada',
			PROFILE_STATUS_HIDDEN => 'Sí, oculto'
		);

		$edit_account['verified'] = $client['proVerified'];

		$edit_account['verified_pictures_options'] = array(
			PICTURES_NOT_VERIFIED => 'Fotos Sin verificar',
			PICTURES_VERIFIED => 'Fotos Verificadas',
		);

		$edit_account['verified_pictures'] = $client['proVerifiedPictures'];


		$edit_account['deleted'] = $client['proDeleted'];

		$edit_account['priority'] = $client['proPriority'];

		$edit_account['comments'] = $client['cliComments'];

		$edit_account['panel_message'] = $client['proPanelMessage'];
		

		if ($this->input->post('send') && $proId == $this->input->post('id')) {

			$this->_load_edit_validation_rules();

			if ($this->form_validation->run() !== false) {

				$this->_do_edit_profile($edit_account);
				
				$this->_on_edit_success($client['proUser']);

				$this->session->set_flashdata('success_message', 'Los datos han sido actualizados con éxito!');

				return redirect('padmin/profiles/edit/' . $client['proId']);

			} else {
				$edit_account['error_message'] = 'Los datos no han podido ser actualizados, revise por favor los campos.';
				$edit_account['apply_visible'] = true;
			}
		} 

		//print_r($edit_account);

		$this->load->view('padmin/header', $header);
		$this->load->view('padmin/subheader', $subheader);
		$this->load->view('padmin/profiles/edit', $edit_account);
		$this->load->view('padmin/footer');	

	}
	

	protected function _on_edit_success($user)
	{
		$profile = $this->profile_model->get_profile($user);
		$this->profile_pictures_service->set_profile_data($user, $profile);
		$this->profile_pictures_service->regenerate_profile_pictures($user);
	}

	
	public function fix_priorities() {
		
		$countries = array_keys($this->country->get_countries());
		
		$genders = array('mujer', 'trans');
		
		foreach ($countries as $country) {
			
			foreach ($genders as $gender) {
				$this->profile_model->fix_group_priorities($country, $gender);		
			}			
		}
		
	}
	
	private function _check_restricted_country($section_country) {
		$admin_country = $this->admin_model->get_country();

		if (strlen($admin_country) == 2 && $section_country != $admin_country) {
			$forbidden_message = "Tu cuenta de usuario administrador tiene acceso restringido a esta página. Si no es el caso, contáctate con el encargado técnico.";
			$forbidden_message .= "<br><br>" . anchor("padmin/profiles", "Volver al Listado de Cuentas");
			show_error($forbidden_message, 403, "No puedes administrar esta sección");
			die;
		}
	}
	
	private function _no_profile() {
		$forbidden_message = "No existe un perfil para el nombre de usuario especificado.";
		$forbidden_message .= "<br><br>" . anchor("padmin/profiles", "Volver al Listado de Cuentas");
		show_error($forbidden_message, 404, "El perfil no existe");
		die;
	}
		
}

/* end of file */