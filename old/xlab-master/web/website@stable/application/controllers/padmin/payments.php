<?php

if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Payments extends CI_Controller {

	protected $admin_country = '';
	
	public function __construct() {
		parent::__construct();
	
		$this->load->library('gender');
		$this->load->library('country');
		
		$this->load->model('profile_model');
		$this->load->model('admin_model');
		$this->load->model('payments_model');
		$this->load->model('deposit_model');

		if (!$this->admin_model->login_exists()) {
			redirect ('padmin');
		}
		
		$this->admin_country = $this->admin_model->get_country();

		if (!$this->admin_country && !$this->admin_model->get_setting('payments_country')) {
			$this->admin_model->set_setting('payments_country', 'uy');
		} elseif (!$this->admin_model->get_setting('payments_country')) {
			$this->admin_model->set_setting('payments_country', $this->admin_country);
		}
	}	
	
	public function index() {	
		$this->list_published();
	}
	
	
	public function list_published() {
		$subheader_data = array(
			'title' => 'Pagos',
			'subtitle' => 'Listado por perfiles publicados',
		);
		
		$lpub_data = array();
		$lpub_data['admin_country'] = $this->admin_country;
		$lpub_data['country_options'] = $this->country->get_countries();
		
		$lpub_data['country'] = $this->admin_model->get_setting('payments_country');
		
		$lpub_data['gender'] = $this->admin_model->get_setting('payments_gender');
		
		$lpub_data['gender_options'] = array(
			'mujer'  => 'Mujeres',
			'trans'  => 'Trans',
			'hombre' => 'Hombres',
			'duo'    => 'Duos',
		);
		
		$lpub_data["publication_status_options"] = array(
			'published' => 'En portada'
		,	'expired'   => 'Expirados'
		,	'suspended' => 'Suspendidos'
		);
		
		$lpub_data['publication_status'] = $this->admin_model->get_setting('payments_publication_status');
		
		$lpub_data['order_by'] = $this->admin_model->get_setting('payments_order_by');
		
		$lpub_data['order_by_options'] = array(
			'priority_desc' => 'Posición de portada descendiente'
		,	'priority_asc' => 'Posición de portada ascendiente'
		,	'last_deposit_desc' => 'Depositos más recientes primero'
		,	'last_deposit_asc' => 'Depositos más antiguos primero'
		);
		
		if (!isset($lpub_data['gender_options'][$lpub_data['gender']])) {
			$lpub_data['gender'] = 'mujer';
		}
		
		$profiles = $this->_generate_profiles_payment_view_data($lpub_data);

		$lpub_data['profiles'] = $profiles;
		$lpub_data['total_profiles'] = count($profiles);
		
		$this->load->view('padmin/header', array('title' => 'Pagos de perfiles publicados' ));
		$this->load->view('padmin/subheader', $subheader_data);
		$this->load->view('padmin/payments/list_published', $lpub_data);
		$this->load->view('padmin/footer');
	}
	
	
	public function list_published_select_action() {

		$action = $this->input->post("action");
		
		if ($users) {
			if ($action == 'panel_message') {
				$this->_set_panel_message($users);
				return true;
			}
		}
		
		
	}


	private function _generate_profiles_payment_view_data($options)
	{
		$profiles = [];

		$account_types = $this->profile_model->get_account_types();

		$options = (object) $options;

		$conditions = [
			'country' => $options->country 
		,	'gender'  => $options->gender
		,	'order_by'=> $options->order_by
		,	'verified'=> PROFILE_STATUS_VERIFIED
		];

		if ($options->publication_status === 'suspended')
		{
			$conditions['verified'] = PROFILE_STATUS_HIDDEN;
		}

		if ($options->publication_status === 'expired')
		{
			$db_rows = $this->payments_model->get_profiles_expired($conditions);
		}
		else
		{
			$db_rows = $this->payments_model->get_profiles_last_payments($conditions);
		}

		foreach ($db_rows as $db_row) {
			$profile = array();
			$profile['position'] = $db_row['proPriority'];
			$profile['account_type'] = $account_types[$db_row['proAccountType']];
			$profile['group'] = $db_row['group_name'];

			$profile['name'] = $db_row['proName'];
			$profile['user'] = $db_row['proUser'];

			$profile["phone"] = $db_row["proPhone"];

			if ($db_row['proCountry'] == 'uy') {
				$profile["phone"] = $db_row['proNumericPhones'];
			}

			$profile['last_payment_date'] = "";

			if ($db_row['depLastDateApplied']) {
				$profile['last_payment_date'] = date('d/m/Y', strtotime($db_row['depLastDateApplied']));
			} else if ($db_row['depLastDate']) {
				$profile['last_payment_date'] = date('d/m/Y', strtotime($db_row['depLastDate']));
			}

			$profile['last_payment_amount'] = $db_row['depAmount'];

			$profile['panel_message'] = $db_row['proPanelMessage'];

			$profiles[] = $profile;
		}

		return $profiles;
	}

	public function list_published_to_excel() {
		$this->load->library('export/spreadsheet');	

		$parameters = (object)[
			'country' => $this->input->get("country")
		,	'gender' => $this->input->get('gender')
		,	'order_by' => $this->input->get("order_by")
		,	'publication_status' => $this->input->get("publication_status")
		];
		
		if ($this->admin_country) {
			$country = $this->admin_country;
		}

		$profiles = $this->_generate_profiles_payment_view_data($parameters);
		
		$field_map = [
			[ 'field' => 'position'           , 'colheader' => 'Posicion' ]
		,	[ 'field' => 'account_type'       , 'colheader' => 'Cuenta'   ]
		,	[ 'field' => 'group'              , 'colheader' => 'Grupo'    ]
		,	[ 'field' => 'name'               , 'colheader' => 'Nombre'   ]
		,	[ 'field' => 'user'               , 'colheader' => 'Usuario'  ]
		,	[ 'field' => 'phone'              , 'colheader' => 'Tel.', 'cellDataType' => 'string' ]
		,	[ 'field' => 'last_payment_date'  , 'colheader' => 'Último pago'   ]
		,	[ 'field' => 'last_payment_amount', 'colheader' => 'Cantidad'      ]
		,	[ 'field' => 'panel_message'      , 'colheader' => 'Mensaje Panel' ]
		];
		
		$doc_data = [
			'title' => "Listado de perfiles ". $parameters->gender
		,	'creator' => $this->config->item('site_name')
		,	'sheets' => []
		,	'filename' => 'perfiles_' . $parameters->gender . '_' . $parameters->publication_status . '_' . date('d-m-Y_His')
		];
		
		$sheet = [];
		$sheet['title'] = 'Portada ' . $parameters->gender . ' ' . date('d.m.Y');
		$sheet['cells'] = [ array_column($field_map, 'colheader') ];

		foreach ($profiles as $prof)
		{
			$row = [];

			foreach ($field_map as $field_def)
			{
				if (isset($field_def['cellDataType']))
				{
					$row[] = [ 
						'value' => $prof[$field_def['field']]
					,	'cellDataType' => $field_def['cellDataType']
					];
				}
				else
				{
					$row[] = $prof[$field_def['field']];
				}
			}

			$sheet['cells'][] = $row;
		}

		$doc_data['sheets'][] = $sheet;

		$this->spreadsheet->generate_and_flush_excel_file($doc_data);
	}
	

	public function change_list_published_settings() {
		if (!$this->admin_model->get_country()) {
			$payments_country = $this->input->post('payments_country');

			if ( $this->country->is_valid($payments_country) ) {
				$this->admin_model->set_setting( 'payments_country', $payments_country);
			}
		}
		
		$more_settings = array('payments_gender', 'payments_order_by', 'payments_publication_status');
		
		foreach ($more_settings as $sett) {
			$sett_val = $this->input->post($sett);
			
			if ($sett_val) {
				$this->admin_model->set_setting( $sett, $sett_val);
			}
		}
		
		$this->admin_model->save_settings();
		
		redirect('padmin/payments');
	}
	
	
	public function view($user = false) {		
		if (!$user) return redirect('padmin/payments');
		
		$profile = $this->profile_model->get_full_profile($user);
		if (!$profile) return redirect('padmin/payments');
		
		$this->_check_restricted_country($profile['proCountry']);
		
		$subheader = array(
			'title' => 'Administrar Pagos',
			'subtitle' => $user,
		);
		
		$ref = $this->input->get('ref');
		$valid_refs = array(
			'payments' => "Listado de Pagos",
			'payments/list_deposits' => "Listado de Depósitos",
			'profiles' => "Listado de Clientes",
		);
		
		if (! ($ref && isset($valid_refs[$ref])) ) {
			$ref = "profiles";
		}
		
		$subheader['btn_back_url'] = site_url('padmin/' . $ref);
		$subheader['btn_back_text'] = $valid_refs[$ref];

		$data = array();
		$data['success'] = false;
		
		/* user actions */
		
		if ($this->input->post('action') == "update") {
			$data['success'] = $this->_update_payment_data($user);
			$profile = $this->profile_model->get_full_profile($user);
		}
		
		/* end of user actions */
		
		$data['admin_country'] = $this->admin_model->get_country();
		
		$account_types = $this->profile_model->get_account_types();
		$data['account_type'] = $account_types[$profile['proAccountType']];
		
		$data['absent'] = $profile['proAbsent'];
		$data['verified'] = $profile['proVerified'];
		$data['client_id'] = $profile['proId'];

		$payment_data = $this->payments_model->get_profile_payments_status($user);
		
		if ($payment_data) {
			$data = array_merge($data, $payment_data);
			$data['nextdate'] = ($data['nextdate']) ? $data['nextdate']->format('d/m/Y') : $data['nextdate'];
		}

		$data['default_deposit_concept'] = $this->deposit_model->get_default_deposit_concept();
		
		$data['status_options'] = array(
			'' => 'Coordinar',
			'ok' => 'Al día',
			'debt' => 'Deudor',
			'suspended' => 'Suspendido',
			'free' => 'Eximido',
		);
				
		$this->load->view('padmin/header', array('title' => 'Pagos'));
		$this->load->view('padmin/subheader', $subheader);
		$this->load->view('padmin/payments/view', $data);
		$this->load->view('padmin/footer');
	}
	
	
	public function remove_deposit($user, $deposit_id) {
		$redirect_url = 'padmin/payments/view/' . $user;
		$deposit = $this->deposit_model->get_deposit($deposit_id);

		if (!$deposit) return redirect($redirect_url);

		if ($deposit['depStatus'] === 'applied')
		{
			$this->deposit_model->undo_deposit_apply($deposit_id, $user);	
		}
		else
		{
			$this->deposit_model->delete_deposit($deposit_id);
		}

		$this->payments_model->update_profile_last_deposit($user);

		return redirect($redirect_url);
	}
	
	
	private function _update_payment_data($user) {
		
		$success = false;
				
		$post = $this->input->post();
		
		if (preg_match('!(\d\d?)/(\d\d?)/(\d\d\d\d)!', $post['nextdate'], $dparts)) {
			$date = $dparts[3] . '-' . $dparts[2] . '-' . $dparts[1];
			$post['nextDate'] = $date;
		}
		
		$success = $this->payments_model->upsert_profile_payments_status(
			$user, $post
		);
		
		if ($post['status'] == 'suspended') {
			$this->profile_model->disable_profile($user);
			
		} elseif ($post['status'] == 'ok') {
			$clear_panel_message = !empty($post["clear_panel_message"]);
			$this->_restore_profile_status($user, $clear_panel_message);
		}
		
		return $success;
	}


	protected function _restore_profile_status($user, $clear_panel_message=true)
	{
		$profile = $this->profile_model->get_profile($user);
			
		if (isset($profile['verified']) && $profile['verified'] == PROFILE_STATUS_HIDDEN) {
			$this->profile_model->enable_profile($user);				
		}
		
		if ($clear_panel_message) {
			$this->profile_model->change_profile_panel_message($user, '');
		}
	}
		
	public function suspend_account($user = false) {
		
		if ($user) {
			$this->db->set('payStatus', 'suspended');
			$this->db->where('proUser', $user);
			$this->db->update('payments');
			
			$this->load->model('profile_model');
			$this->profile_model->disable_profile($user);
		}
		
		redirect('padmin/payments');
	}
	
	public function change_country() {
		
		if (!$this->admin_model->get_country()) {
		
			$payments_country = $this->input->post('payments_country');

			if ( $this->country->is_valid($payments_country) ) {
				$this->admin_model->set_setting( 'payments_country', $payments_country);
				$this->admin_model->save_settings();
			}
			
		}
		
		redirect('padmin/payments');
	}
	
	
	public function list_deposits() {
		return redirect('padmin/deposits/list_deposits');
	}
	
	
	private function _check_restricted_country($section_country) {
		$admin_country = $this->admin_model->get_country();

		if (strlen($admin_country) == 2 && $section_country != $admin_country) {
			$forbidden_message = "Tu cuenta de usuario administrador tiene acceso restringido a esta página. Si no es el caso, contáctate con el encargado técnico.";
			$forbidden_message .= "<br><br>" . anchor("padmin/payments", "Volver a lista de clientes");
			show_error($forbidden_message, 403, "No puedes administrar esta sección");
			die;
		}
	}

}

/* end of file */
