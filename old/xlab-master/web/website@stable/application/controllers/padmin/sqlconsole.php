<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class SQLConsole extends CI_Controller {
	
	
	public function __construct() {
		parent::__construct();
		
		$this->load->library('country');
		$this->load->library('gender');
		$this->load->library('session');
		
		$this->load->model('profile_model');
		$this->load->model('admin_model');
		
		if (!$this->admin_model->login_exists()) {
			redirect ('padmin');
		}
		
		if ($this->admin_model->get_name() != 'santiago') {
			die('zona prohibida');
		}
	}
	
	public function index() {
		$this->console();
	}
	
	public function console() {
		$console_data = array(
			'results' => '',
			'current_query' => '',
			'total_rows' => 0,
		);
		
		$subheader_data = array(
			'title' => 'Consola SQL',
			'subtitle' => 'Consultas',
		);
		
		$sql_query = $this->input->post("sql_query");
		$sql_query = trim($sql_query);
		$sql_query = preg_replace('!;.*!msi', '', $sql_query);
		
		if ($sql_query) {
			$console_data['current_query'] = $sql_query;
			
			if (preg_match("!^select +!i", $sql_query)) {

				if (!preg_match('!limit *\d+ *;?$!i', $sql_query)) {
					$sql_query_limited = preg_replace("![\s\t;]*$!", ' limit 100', $sql_query, 1);
				} else {
					$sql_query_limited = $sql_query;
				}

				$result = $this->db->query($sql_query);
				$console_data['total_rows'] = $result->num_rows();

				$result = $this->db->query($sql_query_limited);
			
				if ($result->num_rows()) {
					$console_data['results'] = $result->result_array();
				}
			}
		}
		
		$this->load->view('padmin/header', array('title' => 'Consola SQL' ));
		$this->load->view('padmin/subheader', $subheader_data);
		$this->load->view('padmin/sqlconsole/console', $console_data);
		$this->load->view('padmin/footer');
	}
	
}
