<?php

if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Pricing extends CI_Controller {

    public function __construct() {
        parent::__construct();
    
        $this->load->library('gender');
        $this->load->library('country');
        $this->load->model('admin_model');
        $this->load->model('pricing_model');
    }

    public function index() {
        $view_data = [
            'title' => 'Precios'
        ,   'subtitle' => 'Listado'
        ];

        $prices_by_country = $this->pricing_model->get_all_prices_by_country();
        $view_data['prices'] = $prices_by_country;

        $this->load->view('padmin/header', $view_data);
        $this->load->view('padmin/subheader', $view_data);
        $this->load->view('padmin/pricing/index', $view_data);
        $this->load->view('padmin/footer');
    }

}

/* eof */