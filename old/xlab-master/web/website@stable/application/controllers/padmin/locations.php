<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Description of routes
 *
 * @author Santiago
 */
class Locations extends CI_Controller {

	public function __construct() {
		parent::__construct();

		$this->load->library('country');
		
		$this->load->model("admin_model");
		
		if (!$this->admin_model->login_exists()) {
			redirect('padmin');
		}
	}

	public function index() {
		
	}
	
	public function update() {
		$this->country->update_locations_cache();
		echo "Actualizadas ubicaciones activas e inactivas";
	}
	
	public function update_active() {
		$this->country->update_active_locations_cache();
		echo "Actualizadas ubicaciones en activo";
	}
	
	
}

/* end of file */
