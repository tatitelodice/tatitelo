<?php

if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class AdsMaterial extends CI_Controller {

	
	public function __construct() {
		parent::__construct();
	
		$this->load->model('profile_model');
		$this->load->model('admin_model');
		
		if (!$this->admin_model->login_exists()) {
			redirect ('padmin');
		}

		$this->load->library('adsbuilder');
	}	
	
	public function index() {	
		
	}
	
	
	public function get($user = false) {
		
		if (!$user) {
			redirect('padmin/profiles');
			return false;
		}
		
		$dbdata = $this->profile_model->get_profile($user);
		
		$data = array();
		$data['error'] = false;
		$data['user'] = $user;
		$data['site_name'] = $this->config->item('site_name');
		
		if ($dbdata) {
			$this->_check_restricted_country($dbdata['country']);
			
			$data = array_merge($data, $dbdata);
			$data['material'] = print_r($dbdata, true);
			
			$this->load->helper('text');
			
			$title = $this->adsbuilder->mix_title( array( $data['title'], $data['subtitle'] ) );
			$title = $this->adsbuilder->delete_phone($title);
			
			$data['title'] = $title;
			
			$description = $data['about'] . ' ' . $data['services'];
			
			$data['description'] = character_limiter( $description, 950, '…');
			
			$data['location'] = $data['region'];
			
			if ($data['city']) {
				$data['location'] = $data['city'] . ', ' . $data['region'];
			}
			
			$data['falsemail'] = $this->adsbuilder->random_mail();
			$data['site'] = $this->config->item('site_domain') . '/' . $user;
			
			$data['pictures'] = $this->adsbuilder->watermark_gallery($user);
			
			$promo = 'img/promos/' . $data['country'] . '.jpg';
			
			if (file_exists($promo)) {
				$data['promo'] = site_url($promo);
			} else {
				$data['promo'] = false;
			}
			
		} else {
			$data['error'] = 'nouser';
		}
		
		$subheader = array(
			'title' => 'Administración',
			'subtitle' => 'Material de Publicación',
			'btn_back' => '<a style="float: right;" class="btn" href="' . site_url('padmin/profiles') . '">Listado de Cuentas</a>'
		);

		$this->load->view('padmin/header', array('title' => 'Material de Publicación'));
		$this->load->view('padmin/subheader', $subheader);

		$data['admin_country'] = $this->admin_model->get_country();
		$this->load->view('padmin/adsmaterial', $data);

		$this->load->view('padmin/footer');
		
	}
	
	private function _check_restricted_country($section_country) {
		$admin_country = $this->admin_model->get_country();

		if (strlen($admin_country) == 2 && $section_country != $admin_country) {
			$forbidden_message = "Tu cuenta de usuario administrador tiene acceso restringido a esta página. Si no es el caso, contáctate con el encargado técnico.";
			$forbidden_message .= "<br><br>" . anchor("padmin/profiles", "Volver a Listado de Cuentas");
			show_error($forbidden_message, 403, "No puedes administrar esta sección");
			die;
		}
	}
	
}

/* end of file */
