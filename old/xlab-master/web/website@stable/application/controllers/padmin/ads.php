<?php

if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Ads extends CI_Controller {

	public function __construct() {
		parent::__construct();
		$this->load->library('session');
		
		$this->load->model('admin_model');
		
		if (!$this->admin_model->login_exists()) {
			redirect ('padmin');
		}
	}	
	
	public function index() {

		$admin_country = $this->admin_model->get_country();
		
		$this->load->library('sdate');
		
		if ($this->input->post('user')) {
			$this->db->where('ads.proUser', $this->input->post('user'));
		}
		
		if ($this->input->post('source')) {
			$this->db->like('adSource', $this->input->post('source'));
		}
		
		$this->db->join('profiles', 'profiles.proUser = ads.proUser');
		
		if ($admin_country) {
			$this->db->where('proCountry', $admin_country);
		}
		
		$full_results = $this->db->get('ads');
		$full_results_count = $full_results->num_rows();
		
		$this->db->select('adSourceAdId,adSource,adUrl,ads.proUser,adDatetime,proGender');
		
		if ($this->input->post('user')) {
			$this->db->where('ads.proUser', $this->input->post('user'));
		}
		
		if ($this->input->post('source')) {
			$this->db->like('ads.adSource', $this->input->post('source'));
		}
		
		if ($admin_country) {
			$this->db->where('proCountry', $admin_country);
		}
		
		$this->db->join('profiles', 'profiles.proUser = ads.proUser');
		
		$this->db->order_by('adId', 'desc');
		
		$count_limit = 500;
		$this->db->limit($count_limit);
		
		$results = $this->db->get('ads');
		
		$ads_data = array(
			'admin_country' => $admin_country,
			'ads_full_count' => $full_results_count,
			'ads_limit' => $count_limit,
			'ads_table' => '',
			'ads_count' => 0,
			'user' => $this->input->post('user'),
			'source' => $this->input->post('source'),
			'only_urls' => $this->input->post('only_urls'),
		);
		
		$found_ads = $results->num_rows();
		
		if ($found_ads > 0) {
			
			$ads_data['ads_count'] = $found_ads;
			
			if ($ads_data['only_urls']) {
				
				$ads_data['ads_table'] =  PHP_EOL . '<pre>';
				
				foreach ($results->result_array() as $row) {
					$ads_data['ads_table'] .= PHP_EOL . $row['adUrl'];
				}
				
				$ads_data['ads_table'] .= PHP_EOL . '</pre>';
				
			} else {
				foreach ($results->result_array() as $row) {
					//print_r($row); die;

					$ads[] = array(
						'user' => anchor( site_url($row['proUser']), $row['proUser'], 'target="_blank"'),
						'gender' => $row['proGender'],
						'url' => anchor($row['adUrl'], 'Ver Anuncio', 'target="_blank"'),
						'source' => $row['adSource'],
						'datetime' =>  $this->sdate->formatoYMDaLiteral($row['adDatetime'], true),
					);
				}
				
				$this->load->library('table');

				$this->table->set_template( array( 'table_open' => '<table class="table table-condensed table-striped">',));
				$this->table->set_heading( 'Usuario', 'Género', 'Anuncio', 'Fuente', 'Fecha y hora');

				$ads_data['ads_table'] = $this->table->generate($ads);
			}
		}
		
		
		$this->load->view('padmin/header', array('title' => 'Anuncios' ));
		
		$subheader = array(
			'title' => 'Anuncios',
			'subtitle' => 'Listado Completo',
			'btn_back' => '<a style="float: right;" class="btn" href="' . site_url('padmin') . '">Página Principal</a>'
		);
		
		$this->load->view('padmin/subheader', $subheader);
		
		$this->load->view('padmin/ads', $ads_data);
		
		$this->load->view('padmin/footer');
		
	}
	
	public function delete($limit_date = false, $source = false) {
		if (preg_match('!^\d\d\d\d-\d\d?-\d\d?$!', $limit_date)) {
			$this->db->where('adDatetime <=', $limit_date);
			$this->db->delete('ads');
			echo 'registros borrados exitosamente';
		} else {
			echo 'nada para borrar';
		}
	}
	
	public function autoclean() {
		$default_limit = date('Y-m-d', strtotime('-10 day'));
		$this->delete($default_limit);
	}
	
	
}

/* end of file */
