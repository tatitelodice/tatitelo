<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Profiles_Marketing extends CI_Controller {
	
	public function __construct()
	{
		parent::__construct();
			
		$this->load->library('export/spreadsheet');	

		$this->load->model('profile_model');
		$this->load->model('admin_model');
		
		if (!$this->admin_model->login_exists()) {
			redirect ('padmin');
		}
	}

	public function index()
	{
		$this->listNeverPaidProfiles();
	}


	public function listNeverPaidProfiles()
	{
		$view_data = $this->get_never_paid_profiles_data();

		if ($this->input->get('format') === 'xlsx')
		{
			return $this->generate_never_paid_profiles_excel_file($view_data);
		}

		$this->render('padmin/profiles_marketing/page_never_paid_profiles', $view_data);
	}


	protected function generate_never_paid_profiles_excel_file($view_data)
	{
		$gender = $view_data['filter_gender'];

		$date_from = $view_data['filter_date_from'];
		$date_from_n = str_replace('/', '-', $date_from);

		$date_to = $view_data['filter_date_to'];
		$date_to_n = str_replace('/', '-', $date_to);

		$doc_data = [
			'title' => "Perfiles no aprobados ". $gender
		,	'creator' => $this->config->item('site_name')
		,	'sheets' => []
		,	'filename' => 'perfiles_no_aprobados_'. $gender .'_' . $date_from_n . '_' . $date_to_n
		];
		
		$sheet = [];
		$sheet['title'] = 'perfiles no aprobados';
		$sheet['cells'] = [];

		foreach ($view_data['profiles'] as $prof)
		{
			$sheet['cells'][] = [
				$prof['id']
			,	[ 'value' => $prof['user'], 'cellDataType' => 'string' ]
			,	[ 'value' => $prof['name'], 'cellDataType' => 'string' ]
			,	$prof['gender']
			,	[ 'value' => $prof['numericPhones'], 'cellDataType' => 'string' ]
			,	$prof['email']
			,	$prof['createdDate']
			];
		}

		$doc_data['sheets'][] = $sheet;

		$this->spreadsheet->generate_and_flush_excel_file($doc_data);
	}


	protected function get_never_paid_profiles_data()
	{
		$filter_gender = $this->input->get('gender');
		$filter_gender = $filter_gender ?: 'mujer';

		$filter_date_from = $this->input->get('date_from');
		$filter_date_from = $filter_date_from ?: date('01/m/Y');

		$filter_date_to = $this->input->get('date_to');
		$filter_date_to = $filter_date_to ?: date('d/m/Y');

		$profiles = $this->get_never_paid_profiles(
			$filter_gender, $filter_date_from, $filter_date_to
		);

		return [
			'title' => 'Perfiles Nunca Pagos'
		,	'subtitle' => 'Listado'
		,	'btn_back_text' => "Página Principal"
		,	'btn_back_url' => site_url("padmin")
		,	'gender_options' => $this->gender->get_available_genders('uy')
		,	'filter_gender' => $filter_gender
		,	'filter_date_from' => $filter_date_from
		,	'filter_date_to' => $filter_date_to
		,	'profiles' => $profiles
		];
	}


	protected function get_never_paid_profiles($gender, $date_from, $date_to)
	{
		$date_from = DateTime::createFromFormat('d/m/Y', $date_from)->format('Y-m-d');
		$date_to = DateTime::createFromFormat('d/m/Y', $date_to)->format('Y-m-d');

		$this->db->where('proVerified', PROFILE_STATUS_NOT_VERIFIED);
		$this->db->where('proAccountType', PROFILE_FREE);
		$this->db->where('proGender', $gender);
		$this->db->where('proDeleted', 0);
		$this->db->where('proCountry', 'uy');
		$this->db->where('proCreatedDate >=', $date_from);
		$this->db->where('proCreatedDate <=', $date_to);
		$this->db->order_by('proId', 'desc');
		$this->db->limit(500);

		$profiles = $this->profile_model->format_result($this->db->get('profiles'));

		return $profiles;
	}


	protected function render($view_name, $data)
	{
		$this->load->view('padmin/header', $data);
		$this->load->view('padmin/subheader', $data);
		$this->load->view($view_name, $data);
		$this->load->view('padmin/footer');
	}

}

/* end of file */