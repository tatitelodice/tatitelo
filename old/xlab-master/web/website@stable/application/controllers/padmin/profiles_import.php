<?php

if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Profiles_Import extends CI_Controller {
	
	
	protected $from_site;
	protected $to_site;
	
	protected $general_output;
	
	protected $image_versions;
	
	
	public function __construct() {
		parent::__construct();
		
		/* MODELS */
		$this->load->model('admin_model');
		
		if (!$this->admin_model->login_exists()) {
			redirect ('padmin');
		}
		
		$this->load->model('profile_model');
		$this->load->model('profile_group_model');
		
		
		/* LIBRARIES AND HELPERS */
		$this->load->library("country");
		$this->load->helper('profile_helper');
		
		
		/* CLASS ATTRIBUTES */
		$this->general_output = "";
		
		$this->to_site = $this->config->item('site_domain_simple');
		$this->from_site = $this->config->item('remote_site_domain');
		
		$this->image_versions = $this->config->item('profile_pictures_versions');
	}


	public function index() {
		$this->sources_list();
	}
	
	
	public function sources_list() {
		$subheader_data = array(
			'title' => 'Datos',
			'subtitle' => 'Herramientas de importacion',
		);
		
		$sources_list_data = array(
			'result'    => '',
			'output'    => '',
			'from_site' => $this->from_site,
			'to_site'   => $this->to_site,
		);
		
		//$origin_user = $this->input->post('origin_user');
		$rename_user = $this->input->post('rename_user');

		$this->form_validation->set_rules('source', 'Sitio Fuente', 'required');
		
		$this->form_validation->set_rules('origin_user', 'Usuario Origen', 'trim|required');
		
		if ($rename_user) {
			$this->form_validation->set_rules('rename_user', 'Usuario Destino', 'trim|required|alpha_dash|callback_valid_username_chars|callback_valid_username|callback_available_username');
		}	
			
		$this->form_validation->set_message('available_username', 'Parece ser que "'.$rename_user.'" ya existe, elige otro.');
		$this->form_validation->set_message('valid_username', 'Parece ser que "'.$rename_user.'" no es valido, elige otro.');
		$this->form_validation->set_message('required', '%s es obligatorio.');
		$this->form_validation->set_message('alpha_dash', '%s sólo puede tener letras, números ó guiones (sin espacios).');
		$this->form_validation->set_message('valid_username_chars', 'El usuario chicasuy debe tener como mínimo 3 letras.');

		
		if ( $this->form_validation->run() !== false ) {	
			$postdata = $this->input->post();
						
			$success = $this->import($postdata["source"], $postdata["origin_user"], $postdata["rename_user"]);
			
			$import_output = $this->general_output;
			
			if ($success) {
				$sources_list_data["result"] = 'success';
			} else {
				$sources_list_data["result"] = 'fail';
			}
			
			$sources_list_data["output"] = $import_output;
		}	
		
		$this->load->view('padmin/header', array('title' => 'Datos' ));
		$this->load->view('padmin/subheader', $subheader_data);
		$this->load->view('padmin/profiles_import/sources_list', $sources_list_data);
		$this->load->view('padmin/footer');
	}
	
	
	protected function import($source, $user, $rename_to) {		
		$source = strtolower(trim($source));
		$user = strtolower(trim($user));
		
		switch ($source) {
			case "universal":
				$this->import_from_universal($user, $rename_to);
				break;
			default :
				$this->general_output .= PHP_EOL . "No se encontró '" . $source . "' en la lista de fuentes.";
		}
		
	}
	
	protected function import_from_universal($user, $rename_to) {		
		$json_result = $this->_get_json('http://www.'.$this->from_site.'/webservice/get_profile/' . $user . "?pictures_version=optimized");
		
		if (is_array($json_result)) {
			if ($json_result['message'] == 'success') {
		
				$profile = $json_result['data'];

				$profile['sourceUser'] = $user;

				if ($rename_to) {
					$profile['user'] = $rename_to;
				} else {
					$rename_to = $user;
				}

				//print_r($profile);

				if ($this->_register_profile($profile)) {
					$this->_download_profile_pictures($profile);
					$this->general_output .= PHP_EOL . '<hr>';
					$this->general_output .= PHP_EOL . 'Se ha importado <b>' . $user . '</b> con exito como <b>' . $rename_to . '</b>'; 
					return true;
				}
			} else {
				$this->general_output .= PHP_EOL . 'No existe el usuario ' . $user . ' en ' . $this->from_site;
				return false;
			}
		} else {
			$this->general_output .= PHP_EOL . "Error al obtener datos de " . $this->from_site;
			return false;
		}
	}
	
	public function available_username($user) {
		$this->load->model('profile_model');
		$res = $this->profile_model->get_profile($user);
		return ! (is_array($res) && count($res) > 0);
	}
	
	public function valid_username($username) {
		$username = strtolower($username);
		
		$valid = true;
		
		$banned_list = array(
			'index', 'application', 'system', 'userdata', 'admin', 'padmin', 'panel', 'cpanel', 'webmail',
			'main', 'promomain', 'escort', 'profile', 'site', 'login', 'welcome', 'comingsoon', 
			'chile', 'uruguay', 'colombia', 'argentina', 'brasil', 'miperfil', 'mujeres', 'hombres',
			'trans', 'chicos', 'chicas', 'pumbate', 'sexo', 'duos', 'trios', 'chicasuy'
		);
		
		$valid = $valid && !in_array($username, $banned_list);
		$valid = $valid && !file_exists( APPPATH . 'controllers/' . strtolower($username). '.php');
		$valid = $valid && !file_exists( APPPATH . 'controllers/' . strtolower($username));
		
		return $valid;
	}

	public function valid_username_chars($username) {
		$username = strtolower($username);
		$chars_len = strlen($username) > 2;
		
		return $chars_len && preg_match('![a-z]!i', $username);
	}	
	
	
	
	protected function _register_profile($profile) {
		//print_r($profile); 
		$country_iso = 'uy';

		$user = strtolower($profile['user']);
		
		$group_name = preg_replace('!^www\.|\.(com|uy|co)!i', '', $this->from_site) . '_stock';
		
		$group = $this->profile_group_model->get_group_by_name($group_name);
		
		if (is_array($group) && isset($group['id'])) {
			$group_id = $group['id'];
		} else {
			$new_group_data = array('name' => $group_name);
			$group_id = $this->profile_group_model->create_group($new_group_data);
		}
		
		$this->db->set('profile_group_id', $group_id);
		
		
		$this->db->set('cliPhone', $profile['phone']);
		$this->db->set('cliComments', $profile['cliComments']);

		$this->db->set('cliSource', $profile['cliSource']);

		$this->db->set('proUser', $user);

		$insert_clients = $this->db->insert('clients');

		$this->db->set('proUser', $profile['user']);
		$this->db->set('proName', $profile['name']);
		$this->db->set('proEmail', $profile['email']);

		$this->db->set('proAge', $profile['age']);
		$this->db->set('proDays', $profile['days']);
		$this->db->set('proPlace', $profile['place']);
		$this->db->set('proCategories', $profile['categories']);

		$this->db->set('proGender', $profile['gender']);
		$this->db->set('proPhone', $profile['phone']);
		$this->db->set('proNumericPhones', $profile['numericPhones'] );

		$this->db->set('proCountry', $profile['country']);
		$this->db->set('proRegion', $profile['region']);
		$this->db->set('proCity', $profile['city']);

		$this->db->set('proTitle', $profile['title']);
		$this->db->set('proSubtitle', $profile['subtitle']);
		$this->db->set('proSubtitle2', $profile['subtitle2']);

		$this->db->set('proAbout', $profile['about']);
		$this->db->set('proServices', $profile['services']);
		$this->db->set('proTheme', 'amaranth');

		$this->db->set('proVerified', PROFILE_STATUS_NOT_VERIFIED);
		$this->db->set('proAbsent', 0);

		$this->db->set('proPassword', $profile['password']);
		
		$operation_date = date('Y-m-d H:i:s');
		$this->db->set('proCreatedDate', "'" . $operation_date . "'", false);

		$insert_profiles = $this->db->insert('profiles');

	
		$this->load->model('log_model');
		
		$metadata = array( 
			'source_user' => $profile["sourceUser"], 
		);

		$insert_logs = $this->log_model->insert_log($country_iso, 'profile_imported', $user, false, false, $metadata);
		
		return $insert_clients && $insert_profiles && $insert_logs;
	}
	
	
	protected function _download_profile_pictures($profile) {
		$user = $profile['user'];
		$source_user = $profile['sourceUser'];
		
		$user_dir = FCPATH . 'userdata' . DIRECTORY_SEPARATOR . $user . DIRECTORY_SEPARATOR;

		if (!is_dir($user_dir)) {
			@mkdir($user_dir, 0777);
		}

		if (is_dir($user_dir)) {
			$url_list = array();
			$remote_md5_list = $this->_get_remote_md5($source_user . '/optimized');

			//print_r($remote_md5_list ); die;

			foreach ($profile['pictures'] as $p_url) {
				$p_local_file = preg_replace('!.*?/([^/]+)$!i', '\1', $p_url);
				$p_local_file = $user_dir . $p_local_file;

				if (file_exists($p_local_file) && md5_file($p_local_file) != $remote_md5_list[basename($p_local_file)]) {
					@unlink($p_local_file);
				}

				if (!file_exists($p_local_file)) {
					$url_list[] = array(
						'url' => $this->_fix_url($p_url),
						'dest' => $p_local_file,
					);
				}
			}

			//print_r($url_list); die;

			$this->_save_pictures($url_list);

			//die;

			/* make file versions */
			foreach ($url_list as $u) {
				if (file_exists($u['dest'])) {
					foreach ($this->image_versions as $version => $options) {
						create_scaled_image($u['dest'], $version, $options);
					}

					$this->general_output .= PHP_EOL . "<br>Descargado " . $u['url'] . PHP_EOL;
				}
			}

		} else {
			$this->general_output .= PHP_EOL . 'No se pudo crear ' . $user_dir . ' revisar permisos';
		}
	}
	
	
	
	private function _fix_url($url) {
		$url = trim($url);
		$url = str_replace(" ", "%20", $url);
		return $url;
	}
	
	private function _get_json($url) {
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($ch, CURLOPT_URL, $url);
		$result = curl_exec($ch);
		curl_close($ch);
		
		$json_result = json_decode($result, true);
		
		if (is_array($json_result)) {
			return $json_result;
		} else {
			return false;
		}
	}
	
	private function _get_remote_md5($user) {
		// check md5 of files
		$remote_md5_list = array();

		$json_md5 = $this->_get_json('http://www.'.$this->to_site.'/webservice/get_user_remote_files_md5/' . $user);

		//print_r($json_md5); die;
		
		if ($json_md5) {
			foreach ($json_md5['data'] as $fmd5) {
				$remote_md5_list[$fmd5['filename']] = $fmd5['md5'];
			}
		}

		return $remote_md5_list;
	}
	
	private function _save_pictures($url_list) {			
		$maxRequests = 5;
		$maxRequests = (sizeof($url_list) < $maxRequests) ? sizeof($url_list) : $maxRequests;

		$curl_multi = curl_multi_init();
		
		$save_to = array();
		
		for	($i=0; $i < $maxRequests; $i++) {
			$url = $url_list[$i]['url'];
			
			$save_to[$url] = $url_list[$i]['dest'];
					
			$ch = curl_init();
			curl_setopt($ch, CURLOPT_HEADER, 0);
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
			curl_setopt($ch, CURLOPT_BINARYTRANSFER,1);
			curl_setopt($ch, CURLOPT_URL, $url);
			
			curl_multi_add_handle($curl_multi, curl_copy_handle($ch));
		}

		do {
			while (($execrun = curl_multi_exec($curl_multi, $running)) == CURLM_CALL_MULTI_PERFORM);

			if ($execrun != CURLM_OK) break;

			// a request was just completed -- find out which one
			while($done = curl_multi_info_read($curl_multi)) {
				$info = curl_getinfo($done['handle']);
				$url = $info['url'];

				if ($info['http_code'] == 200)  {
					$handler = $done['handle'];
					$pic_result = curl_multi_getcontent($handler);
					
					$fp = fopen($save_to[$url],'x');
					@fwrite($fp, $pic_result);
					@fclose($fp);
					@chmod($save_to[$url], 0777);

					// start a new request (it's important to do this before removing the old one)
					if ($i < count($url_list)) {
						$url = $url_list[$i]['url'];
						$save_to[$url] = $url_list[$i]['dest'];

						$newHandler = curl_copy_handle($ch);
						curl_setopt($newHandler, CURLOPT_URL, $url);
						curl_multi_add_handle($curl_multi, $newHandler);
						
						$i++;
					}

					// remove the curl handle that just completed
					curl_multi_remove_handle($curl_multi, $handler);
					curl_close($handler);
					
				} else {
					$errorCode = $info['http_code'];
					$this->general_output .= PHP_EOL . '<br>Error '.$errorCode.' al descargar URL:' . $url . PHP_EOL;
				}
			}
		} while ($running);

	}	
	
}


/* end of file */