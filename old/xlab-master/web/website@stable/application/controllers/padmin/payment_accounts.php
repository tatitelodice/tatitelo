<?php

if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Payment_Accounts extends CI_Controller {

    protected $admin_country = '';
    
    public function __construct() {
        parent::__construct();
    
        $this->load->library('gender');
        $this->load->library('country');
        $this->load->library('admin/admin_payment_account_service');
        
        $this->load->model('admin_model');
        $this->load->model('payments_model');
        $this->load->model('payment_account_model');
        $this->load->model('deposit_model');

        if (!$this->admin_model->login_exists()) {
            redirect ('padmin');
        }
    }

    public function index() {
        $view_data = [
            'title' => 'Cuentas de Pago'
        ,   'subtitle' => 'Listado'
        ,   'alerts' => [
                'success' => $this->session->flashdata('success_message')
            ,   'error' => $this->session->flashdata('error_message')
            ]
        ];

        $paymentAccounts = $this->admin_payment_account_service->getAllowedPaymentAccounts();

        $paymentAccounts = $this->payment_account_model->attach_total_balance($paymentAccounts, [
            'start_date' => date('Y-m-01')
        ,   'end_date' => date('Y-m-t')
        ]);

        $activePaymentAccounts = $paymentAccounts->where('isActive', true)->all();

        $view_data['activePaymentAccounts'] = $this->generatePaymentAccountViewData($activePaymentAccounts);

        $inactivePaymentAccounts = $paymentAccounts->where('isActive', false)->all();

        $view_data['inactivePaymentAccounts'] = $this->generatePaymentAccountViewData($inactivePaymentAccounts);

        $this->render('padmin/payment_accounts/page_account_list', $view_data);
    }


    public function details($accountId)
    {
        $paymentAccount = $this->checkAndGetPaymentAccount($accountId);

        $view_data = [
            'title' => 'Cuenta de Pago'
        ,   'subtitle' => 'Detalles'
        ,   'paymentAccount' => $paymentAccount
        ];

        $this->render('padmin/payment_accounts/page_account_details', $view_data);
    }


    public function edit($accountId)
    {
        $paymentAccount = $this->checkAndGetPaymentAccount($accountId);

        if ($this->input->post('onFormSubmit') === 'edit')
        {
            $formSuccess = $this->processPaymentAccountForm();

            if ($formSuccess)
            {
                $this->session->set_flashdata('success_message', 'Cambios aplicados con éxito');
                return redirect(current_url());
            }   
        }

        $view_data = [
            'title' => 'Cuenta de Pago'
        ,   'subtitle' => 'Editar'
        ,   'paymentAccount' => $paymentAccount
        ,   'alerts' => [
                'success' => $this->session->flashdata('success_message')
            ,   'error' => $this->session->flashdata('error_message')
            ]
        ];

        $this->render('padmin/payment_accounts/page_account_edit', $view_data);
    }


    public function mass_assign_last_payment_account()
    {
        $this->load->library('billing/payment_account_assign_service');
        $accountAssignService = &$this->payment_account_assign_service;

        $accountAssignService->assignLastDepositAccountAsPaymentAccount();

        $this->session->set_flashdata('success_message', 'Cambios aplicados con éxito');
        return redirect('padmin/payment_accounts');            
    }


    protected function processPaymentAccountForm()
    {
        $data = [
            'id' => $this->input->post('id')    
        ,   'isActive' => intval($this->input->post('isActive')) === 1
        ,   'isDefault' => intval($this->input->post('isDefault')) === 1
        ,   'priority' => intval($this->input->post('priority'))
        ];

        $this->payment_account_model->save($data);

        return true;
    }

    protected function checkAndGetPaymentAccount($accountId)
    {
        $account = $this->payment_account_model->get_by_id($accountId);

        if (!$account)
        {
            throw new \Exception("Invalid PaymentAccount id " . $accountId);
        }

        return $account;
    }


    protected function generatePaymentAccountViewData($paymentAccounts)
    {
        $data = [];

        foreach ($paymentAccounts as $acc)
        {
            $acc['detailsUrl'] = site_url(
                'padmin/payment_accounts/details/' . $acc['id']
            );

            $data[] = $acc;
        }

        return $data;
    }


    protected function render($view_name, $data)
    {
        $this->load->view('padmin/header', $data);
        $this->load->view('padmin/subheader', $data);
        $this->load->view($view_name, $data);
        $this->load->view('padmin/footer');
    }


}

/* eof */