<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class ProfilesWebService extends CI_Controller {
    
    public function __construct() {
        parent::__construct();
        $this->load->model('profile_model');
    }

    public function index()
    {
        return $this->get();
    }

    private function get() {
        $out = array();
        $out['success'] = true;
        $out['message'] = 'running';
        $this->_to_output($out);
    }

    public function all() {
        $out = [];
        $profiles = [];

        $limit = $this->input->get('limit');
        $from_date = $this->input->get('fromDate');
        $to_date = $this->input->get('fromDate');
        $gender = $this->input->get('gender');

        if ($from_date)
        {
            $this->db->where('proCreatedDate >=', $from_date);    
        }

        if ($to_date)
        {
            $this->db->where('proCreatedDate >=', $from_date);    
        }

        if ($gender)
        {
            $this->db->where('proGender', $gender);
        }
        
        if ($this->input->get('verified') !== false)
        {
            $this->db->where('proVerified', $this->input->get('verified'));
        }
        
        $this->db->where('proCountry', 'uy');
        $this->db->where('proDeleted', 0);

        $this->db->order_by('proId desc');

        $limit = ($limit) ? $limit : 100;
        $this->db->limit($limit); 
        $res = $this->db->get('profiles')->result();

        if ($res)
        {
            foreach ($res as $res_row) {
                $phones = explode(',', $res_row->proNumericPhones);
                if (empty($phones)) continue;

                $profiles[] = [
                    'user' => $res_row->proUser
                ,   'phones' => $phones
                ,   'email' => $res_row->proEmail
                ,   'gender' => $res_row->proGender
                ,   'createdDate' => $res_row->proCreatedDate
                ];
            }
        }

        $out['success'] = true;
        $out['profiles'] = $profiles;

        $this->_to_output($out);
    }


    private function _to_output ($data) {
        header('Content-Type: application/json');
        echo json_encode($data);
        exit();
    }
}

/* eof */