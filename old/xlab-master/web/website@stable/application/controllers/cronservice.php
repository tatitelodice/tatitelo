<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class CronService extends CI_Controller {
	
	public function __construct() {
		parent::__construct();
	}
	
	protected $key = '736bdb7643158277223e722f8602c74c';
	
	public function index() {
		$out['message'] = 'running';
		$this->_to_output($out);
	}
	
	
	public function shake_priorities($key) {
		$out['message'] = 'no valid key';
		
		if ($key == $this->key) {
			$this->load->model("profile_model_ext");
			
			$this->profile_model_ext->shake_priorities('co', 'mujer', PROFILE_BASIC);
			$this->profile_model_ext->shake_priorities('co', 'trans', PROFILE_BASIC);
			$this->profile_model_ext->shake_priorities('co', 'hombre', PROFILE_BASIC);
			
			$out['message'] = 'priorities shaked successfuly';
		}
		
		$this->_to_output($out);
	}
	
	
	public function update_locations($key = false) {
		
		$out['message'] = 'no valid key';
		
		if ($key == $this->key) {
			$this->load->library('country');
			$this->country->update_locations_cache();
			$out['message'] = 'updated full locations successfuly';
		}
		
		$this->_to_output($out);
	}
	
	public function update_active_locations($key = false) {
		
		$out['message'] = 'no valid key';
		
		if ($key == $this->key) {
			$this->load->library('country');
			$this->country->update_active_locations_cache();
			$out['message'] = 'updated active locations successfuly';
		}
		
		$this->_to_output($out);
	}
	
	private function _to_output ($data) {
		$this->output
		->set_content_type('application/json')
		->set_output(json_encode($data));
	}
	
}

/* end of file */
