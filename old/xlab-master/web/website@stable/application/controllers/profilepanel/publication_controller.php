<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Publication_Controller extends CI_Controller {

    public function __construct()
    {
        parent::__construct();
        $this->load->model("admin_model");
        $this->load->model('payments_model');
        $this->load->model('profile_model');
        $this->load->model('pricing_model');
        $this->load->helper('profile_helper');
        $this->load->library('contact');
    }

    public function index()
    {
        return $this->status(false);
    }


    public function reactivate($user)
    {
        return $this->status($user);
    }


    public function status($user=false)
    {
        $user = $this->check_and_get_user($user);

        if (!$user)
        {
            return redirect('mipagina');
        }

        $view_data = [
            'title' => 'Publicación'
        ];

        $profile = $this->profile_model->get_profile($user);

        $view_data['user'] = $user;
        $view_data['profile'] = $profile;
        $view_data['pumbate_phone'] = $this->contact->get_phone($profile['country']);
        $view_data['is_published'] = intval($profile['verified']) !== PROFILE_STATUS_NOT_VERIFIED;
        $view_data['country'] = $profile['country'];

        $view_data['pricing'] = $this->pricing_model->get_pricing_for_profile($profile);

        $this->render('site/profile-panel/publication/page_index', $view_data);
    }


    public function activate($user=false)
    {
        $paymentStep = $this->input->get('step');

        $user = $this->check_and_get_user($user);

        if (!$user)
        {
            return redirect('mipagina');
        }

        $view_data = [
            'title' => 'Activar Publicación'
        ];

        $profile = $this->profile_model->get_profile($user);

        $view_data['user'] = $user;
        $view_data['profile'] = $profile;
        $view_data['pumbate_phone'] = $this->contact->get_phone($profile['country']);
        $view_data['is_published'] = intval($profile['verified']) !== PROFILE_STATUS_NOT_VERIFIED;
        $view_data['country'] = $profile['country'];

        $view_data['pricing'] = $this->pricing_model->get_pricing_for_profile($profile);

        $payment_status = $this->payments_model->get_profile_payments_status($user);
        $payment_account = $payment_status['payment_account'];

        $view_data['payment_account'] = $payment_account;
        $view_data['payment_method']  = $payment_account['accountService'];

        if (!in_array($paymentStep, ['pricing', 'payment_method']))
        {
            die('invalid step');
        }

        $this->render('site/profile-panel/publication/page_step_' . $paymentStep, $view_data);
    }


    public function unsuscribe($user=false)
    {
        $user = $this->check_and_get_user($user);

        if (!$user)
        {
            return redirect('mipagina');
        }

        if ($this->input->post('confirm'))
        {
            $this->profile_model->disable_profile($user);
            return redirect('mipagina');
        }

        $this->render('site/profile-panel/publication/page_unsuscribe');
    }


    protected function check_and_get_user($user)
    {
        $user = ($user) ? $user : $this->profile_model->get_logged_user();

        if ($this->profile_model->login_exists($user))
        {
            return $user;
        }

        return false;
    }


    protected function render($template, $data=null)
    {
        $this->load->view('site/header', $data);
        $this->load->view($template, $data);
        $this->load->view('site/footer');
    }


}