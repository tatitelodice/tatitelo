<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Availability_Controller extends CI_Controller {

    public function __construct()
    {
        parent::__construct();
        $this->load->model('profile_model');
    }


    public function make_available($user)
    {
        $user = $this->check_and_get_user($user);

        if ($user)
        {
            $this->db->set('proAbsent', 0);            
            $this->db->where('proUser', $user);
            $this->db->limit(1);
            $this->db->update('profiles');            
        }

        return redirect('mipagina');
    }


    public function modify($user=false)
    {
        $user = $this->check_and_get_user($user);

        if (!$user)
        {
            return redirect('mipagina');
        }

        $profile = $this->profile_model->get_profile($user);

        $view_data = [
            'user' => $user
        ,   'profile' => $profile
        ]; 

        if ($this->submitAndValidateAvailabilityForm() !== false)
        {
            $absent = $this->input->post('absent');
            $absent = ($absent) ? 1 : 0;

            $absentMessage = $this->input->post('absent_message');

            $this->db->set('proAbsent', $absent);
            $this->db->set('proAbsentMessage', $absentMessage);
            
            $this->db->where('proUser', $user);
            $this->db->limit(1);
            $this->db->update('profiles');

            return redirect('/mipagina');
        }

        $this->render('site/profile-panel/availability/page_index', $view_data);
    }


    protected function submitAndValidateAvailabilityForm()
    {
        $this->form_validation->set_rules('absent', 'Ausente', 'integer');
        $this->form_validation->set_rules('absent_message', 'Mensaje de ausencia', 'trim');
        
        $validForm = $this->form_validation->run() !== false;

        return $validForm;
    }


    protected function check_and_get_user($user)
    {
        $user = ($user) ? $user : $this->profile_model->get_logged_user();

        if ($this->profile_model->login_exists($user))
        {
            return $user;
        }

        return false;
    }


    protected function render($template, $data=null)
    {
        $this->load->view('site/header', $data);
        $this->load->view($template, $data);
        $this->load->view('site/footer');
    }

}