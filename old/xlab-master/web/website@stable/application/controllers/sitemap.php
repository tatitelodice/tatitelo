<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Sitemap extends CI_Controller {

	protected $site_root;
	
	protected $frecuency = 'weekly';

	protected $base_urls = array(
		'uruguay',
		//'argentina',
		'colombia',
		'blog/',
		
		'trios-lesbicos-con-chicas-en-uruguay',
		'trios-gay-con-chicos-en-uruguay',
		
		'chicas-escorts-felinas-en-uruguay',
		'mujeres-escorts-y-sexo-en-uruguay',
		'chicas-escorts-y-sexo-en-uruguay',
		'travestis-escorts-y-sexo-en-uruguay',
		'hombres-escorts-y-sexo-en-uruguay',
		
		//'mujeres-escorts-y-sexo-en-argentina',
		//'chicas-escorts-y-sexo-en-argentina',
		//'travestis-escorts-y-sexo-en-argentina',
		//'hombres-escorts-y-sexo-en-argentina',
		
		'mujeres-escorts-y-sexo-en-colombia',
		'chicas-escorts-y-sexo-en-colombia',
		'travestis-escorts-y-sexo-en-colombia',
		'hombres-escorts-y-sexo-en-colombia',
	);
	
	protected $categories = array();
	
	
	public function __construct() {
		parent::__construct();
		$this->load->library('gender');
		$this->categories = $this->gender->get_genders_seo();
		$this->site_root = $this->config->item('base_url');	
	}
	
	public function index() {
		header("Content-type: text/xml");
		echo $this->_get_xml_string();

	}
	
	public function update_xml_file() {
		$file = 'sitemap.xml';
		
		if (file_exists($file)) {
			unlink($file);
		}
		
		file_put_contents($file, $this->_get_xml_string());
				
		@chmod($file, 0777);
	}
	
	private function _get_xml_string() {
		$string  = '<?xml version="1.0" encoding="UTF-8"?>' . PHP_EOL;
		$string .= '<urlset' . PHP_EOL;
		$string .= '	xmlns="https://www.sitemaps.org/schemas/sitemap/0.9"' . PHP_EOL;
		$string .= '	xmlns:xsi="https://www.w3.org/2001/XMLSchema-instance"' . PHP_EOL;
		$string .= '	xsi:schemaLocation="https://www.sitemaps.org/schemas/sitemap/0.9' . PHP_EOL;
		$string .= '	https://www.sitemaps.org/schemas/sitemap/0.9/sitemap.xsd">' . PHP_EOL;
		
		foreach ($this->_get_urls() as $url) {
			$string .= '	<url>' . PHP_EOL;
			$string .= '		<loc>'.$url.'</loc>' . PHP_EOL;
			$string .= '		<changefreq>' . $this->frecuency . '</changefreq>';
			$string .= '	</url>' . PHP_EOL;
		}
		
		$string .= '</urlset>';
		
		return $string;
	}
	
	
	private function _get_urls() {
		$this->load->library('country');
		
		$urls = array();
		
		$urls[] = $this->site_root;
		
		foreach ($this->base_urls as $url) {
			$urls[] = rtrim($this->site_root, '/') . '/' . $url;
		}
		
		$this->db->select('proUser,proGender,proCountry');
		$this->db->where('proDeleted', PROFILE_NOT_DELETED);
		$this->db->where('proVerified', PROFILE_STATUS_VERIFIED);
		
		$res = $this->db->get('profiles');
		
		if ($res->num_rows()) {
			foreach ($res->result_array() as $row) {
				//print_r($row); die;
				
				$ct = strtolower($this->country->name($row['proCountry']));
				$gd = $this->categories[$row['proGender']];
				
				$relativeUrl = '/sexo/'. $ct . '/'.$gd.'/' . $row['proUser'];

				$urls[] = rtrim($this->site_root, '/') . $relativeUrl;
			}
		}
		
		return $urls;
	}

	
}




/* End of file main.php */