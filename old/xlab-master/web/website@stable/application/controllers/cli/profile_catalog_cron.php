<?php 
if ( ! defined('BASEPATH')) exit('No direct script access allowed');
if (isset($_SERVER['REMOTE_ADDR'])) die('Permission denied.');

class Profile_Catalog_Cron extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->library('webfront/profile_gallery_service');
    }

    public function index()
    {
        $this->_to_output([
            'status'  => 'error'
        ,   'message' => 'no_method_specified'
        ]);
    }

    public function refresh_cache()
    {
        $this->profile_gallery_service->refresh_gallery_cache();

        $this->_to_output([
            'status'  => 'success'
        ,   'message' => 'gallery cache refreshed successfuly'
        ]);
    }


    public function shake_positions()
    {
        $this->load->model("profile_model_ext");
        
        $this->profile_model_ext->shake_priorities('co', 'mujer', PROFILE_BASIC);
        $this->profile_model_ext->shake_priorities('co', 'trans', PROFILE_BASIC);
        $this->profile_model_ext->shake_priorities('co', 'hombre', PROFILE_BASIC);
        
        $this->_to_output([
            'status'  => 'success'
        ,   'message' => 'priorities shaked successfuly'
        ]);
    }


    private function _to_output ($data) {
		$this->output
		->set_content_type('application/json')
		->set_output(json_encode($data));
	}

}


/* eof */