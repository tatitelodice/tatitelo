<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class TestPictures extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->library('session');    
        $this->load->library("profile/profile_pictures_service");    
        $this->load->model("profile_model");
    }

    public function test_versions($user)
    {
        $profile_data = $this->profile_model->get_profile($user);
        $pictures_service = $this->profile_pictures_service;
        $pictures_service->set_profile_data($user, $profile_data);
        var_dump(
            $pictures_service->get_profile_pictures_versions($user)
        );
    }

    public function test_regenerate($user)
    {
        $profile_data = $this->profile_model->get_profile($user);
        $pictures_service = $this->profile_pictures_service;
        $pictures_service->set_profile_data($user, $profile_data);
        var_dump(
            $pictures_service->regenerate_profile_pictures($user)
        );        
    }

}