<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class TestFlashSession extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->library('session');        
        $this->load->model("flashsession_model");
    }

    public function test_set()
    {
        //$this->session->set_userdata('fsk', 'boñato');
        $this->flashsession_model->set_encrypted_var('otra', 'cosamariposa');
        $this->flashsession_model->save();
        /*
        var_dump(
            $this->flashsession_model->get_encrypted_var('pum')
        );
        */
    }

    public function test_get()
    {
        //$this->session->set_userdata('fsk', 'boñato');
        var_dump($this->flashsession_model->get_encrypted_var('otra'));
    }

}