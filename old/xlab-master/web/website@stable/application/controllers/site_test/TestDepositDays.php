<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class TestDepositDays extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->library('session');    
        $this->load->model("pricing_model");
    }

    public function test_resolve_publication_days()
    {
        $items = [
            ['country' => 'uy', 'gender' => 'mujer', 'accountType' => PROFILE_BASIC, 'amount' => 390]
        ,   ['country' => 'uy', 'gender' => 'mujer', 'accountType' => PROFILE_BASIC, 'amount' => 490]
        ,   ['country' => 'uy', 'gender' => 'mujer', 'accountType' => PROFILE_BASIC, 'amount' => 690]
        ,   ['country' => 'uy', 'gender' => 'mujer', 'accountType' => PROFILE_PREMIUM, 'amount' => 1250]
        ,   ['country' => 'uy', 'gender' => 'mujer', 'accountType' => PROFILE_PREMIUM, 'amount' => 1450]
        ,   ['country' => 'uy', 'gender' => 'mujer', 'accountType' => PROFILE_PREMIUM, 'amount' => 3000]
        ];
        
        $results = [];

        foreach ($items as $item)
        {
            $pricing = $this->pricing_model->get_pricing_by_country_gender(
                $item['country'], $item['gender']
            );

            $publication_days = $this->pricing_model->resolve_publication_days(
                $pricing, $item['accountType'], $item['amount']
            );     
            
            $results[] = array_merge($item, [
                'days' => $publication_days
            ]);
        }        

        echo json_encode($results);
    }

}