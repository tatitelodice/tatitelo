<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

use Melquilabs\Pumbate\Models\ProfileModel;

class Advertiser_Archive_Controller extends CI_Controller {

	public function __construct() {
		parent::__construct();
		$this->load->library('session');
		$this->load->library('country');
		$this->load->library('gender');
		$this->load->library('contact');
		
		$this->load->model("flashsession_model");
		$this->load->model("security_model");
		$this->load->model("admin_model");
		$this->load->model("blacklist_model");
	}

	private function _get_escorts_data($page_params=[]) {
		$this->load->library('webfront/profile_gallery_service');

		$escorts_data = $this->generate_gallery_data([
				'country' => $page_params['country']
			,	'gender'  => $page_params['gender']
			,	'region'  => $page_params['region']
			,	'city'    => $page_params['city']
			,	'keywords'=> $page_params['keywords']
			,	'limit'   => $page_params['limit']
			]
		);
		
		return $escorts_data;
	}
	
	public function advertiser_index($country=false, $gender=false) {
		$country_iso = (strlen($country) > 2) ? $this->country->get_country_iso($country) : $country;
		$country_name = $this->country->name($country_iso);
		
		if (!$country_iso || !$gender)
		{
			return redirect('');
		}
		
		$city = $this->input->get('city');
		$region = $this->input->get('region');
		$keywords = trim($this->input->get('keywords'));

		$page_params = [
			'gender' => $gender
		,	'city' => $city
		,	'region' => $region
		,	'country' => $country_iso
		,	'keywords' => $keywords
		,	'limit' => 400
		];
		
		$advertisers = $this->_get_escorts_data($page_params);
		$advertisers_count = count($advertisers);

		$page_title = "Archivo de ";

		$keyword_map = [
			"mujer" => "prostitutas",
			"hombre" => "chicos gay y prostitutos",
			"trans" => "travestis y cross",
			"duo" => "parejas y trios swinger"
		];

		$fancy_gender = $keyword_map[$gender] ?? $gender;

		$page_title .= $fancy_gender . " en " . $country;

		$main_data = array(
			'page_title' => $page_title
		,	'page_header' => $page_title
		,	'advertisers' => $advertisers
		,	'country' => $country_name
		,	'country_iso' => $country_iso
		,	'gender' => $gender
		,	'fancy_gender' => $fancy_gender
		,	'city' => $city
		,	'region' => $region
		,	'keywords' => $keywords
		,	'total_results' => $advertisers_count
		,	'pumbate_phone' => $this->contact->get_phone($country_iso)
		,	'contact_phone_sms' => $this->contact->get_phone($country_iso, 'sms')			
		,	'pumbate_mail' => $this->contact->get_mail($country_iso)
		);

		$this->load->view("site/archive/advertiser_index_page", $main_data);
		
	}

	private function generate_gallery_data($options)
    {
        $allow_without_pictures = $this->config->item('catalog_display_profiles_without_pictures');

        $country_iso = $options['country'];

        $defaults = array(
            'gender'  => false
        ,   'region'  => false
        ,   'city'    => false
        ,   'keywords'=> false 
        ,   'limit'   => false
        ,   'account_level' => false
        );

        $settings = array_merge($defaults, $options);

        $gender   = $settings['gender'];
        $region   = $settings['region'];
        $city     = $settings['city'];
        $keywords = $settings['keywords'];
        $limit    = $settings['limit'];
        $account_level = $settings['account_level'];

        $escorts_data = array();
        
        $this->db->select('profiles.*');
        $this->db->join('keywords', 'profiles.proUser = keywords.proUser', 'left outer');
        
        $country_seo = $this->country->get_country_seo($country_iso);
        $this->db->where('proCountry', $country_iso);
        
        $this->db->where('proVerified', ProfileModel::PROFILE_STATUS_HIDDEN);
        
        if ($gender) {
            if (preg_match('!^duo!i', $gender)) {
                $this->db->like('proGender', 'duo', 'after');
            } else {
                $this->db->where('proGender', $gender);
            }

            $gender_seo = $this->gender->get_gender_seo($gender);           
        } else {
            $gender_seo = false;
        }
        
		$this->db->where('proDeleted', 0);

		$this->db->order_by('proId', 'desc');

		$query = $this->db->get('profiles');

		if ($query->num_rows() > 0) {
            $url_base = '';
            
            if ($country_seo !== false && $gender_seo !== false) {
                $url_base = '/sexo/' . $country_seo . '/' . $gender_seo . '/';
            }

            foreach($query->result_array() as $result_row) {
                $profile_data = $this->_row_to_profile_data($result_row, $url_base);
                $escorts_data[] = $profile_data;
            }
        }
        
        return $escorts_data;
	}

	protected function _row_to_profile_data($result_row, $url_base)
    {
        $profile_data = array();
                
        $prefix = 'pro';

        foreach($result_row as $name => $value) {
            $name = str_replace($prefix, '', $name);
            $name[0] = strtolower($name[0]);
            $profile_data[$name] = $value;
        }
                        
        $profile_data['about'] = \Stringy\StaticStringy::truncate( $profile_data['about'], 100);
        
        if (preg_match('!^http!i', $profile_data['domain'])) {
            $profile_data['publication_url'] = $profile_data['domain'];
        } else {
            $profile_data['publication_url'] = $url_base . $profile_data['user'];
        }
        
        //$profile_data['picture'] = $this->resolve_profile_picture($profile_data);

        $profile_data['verified_pictures'] = $profile_data['verifiedPictures'];
        $profile_data['account_type'] = $profile_data['accountType'];


        return $profile_data;
    }
}

/* End of file main.php */
