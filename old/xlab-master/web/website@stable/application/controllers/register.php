<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Register extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->library('session');
        $this->load->library('country');
        $this->load->library('gender');
        $this->load->library('contact');
        
        $this->load->model("flashsession_model");
        $this->load->model("security_model");
        $this->load->model("admin_model");
        $this->load->model("blacklist_model");
    }

    public function index()
    {
        $register_data = array();
        $register_data['promo'] = false;

        $profile_create_security_lock = $this->security_model->on_request_lock("profile_create");
        //var_dump($profile_create_security_lock); die;
        
        $profile_help = $this->flashsession_model->get_var("profile_help") || $profile_create_security_lock;
        $profile_complete = $this->flashsession_model->get_var("profile_complete") && $profile_create_security_lock === false;
        $profile_user = $this->flashsession_model->get_var("wait_for_login_user");
        $profile_is_garbage = $this->flashsession_model->get_var("profile_is_garbage");
        
        if ($this->input->post('send_form')) {
            if ($profile_help === false || ($profile_help && $profile_complete))
            {
                $this->_form_action($register_data);
            }
            else
            {
                return redirect('promomain');
            }
        }

        $this->load->view('site/header', array('title' => 'Anunciate en Pumbate'));
        
        $register_data = array(
            'pumbate_phone' => $this->contact->get_phone($this->_look_for_country_iso())
        ,   'admin_login' => $this->admin_model->login_exists()
        );
        
        $this->load->view('site/newprofile/index', $register_data);
        $this->load->view('site/footer'); 
    }

    public function new_profile_success() {     
        $this->load->view('site/header', array('title' => 'Registro exitoso!'));
        
        $user = $this->flashsession_model->get_var("wait_for_login_user");
        $country_iso = $this->_look_for_country_iso();
        $all_paid_profiles = ($country_iso === 'uy') && $this->config->item('all_paid_profiles');

        $is_garbage_profile = $this->flashsession_model->get_var("profile_is_garbage");

        $success_data = array(
            'user' => $user
        ,   'pumbate_phone' => $this->contact->get_phone($country_iso)
        ,   'profile_is_garbage' => $is_garbage_profile
        ,   'all_paid_profiles' => $all_paid_profiles
        ,   'country_iso' => $country_iso
        );
        
        $this->load->view('site/newprofile/success', $success_data);
        $this->load->view('site/footer');
    }
    

    public function new_profile_ban() {
        $this->load->view('site/header', array('title' => 'Has alcanzado el límite diario de registros!'));
        
        $ban_data = array(
            'pumbate_phone' => $this->contact->get_phone($this->_look_for_country_iso()),
        );
        
        $this->load->view('site/newprofile/ban', $ban_data);
        $this->load->view('site/footer');       
    }
    

    public function new_profile_blacklist() {
        $this->load->view('site/header', array('title' => 'Ya tienes una página en pumbate!'));
        
        $blacklist_data = array(
            'pumbate_phone' => $this->contact->get_phone($this->_look_for_country_iso()),
        );
        
        $this->load->view('site/newprofile/blacklist', $blacklist_data);
        $this->load->view('site/footer');
    }


    private function _form_action(&$view_data) {
        
        if ( $this->_massive_registration() ) {
            return redirect('register/new_profile_ban');
        }
        
        $pic_upload_data = $this->_check_pic_upload();
        $view_data['picture_uploaded'] = $pic_upload_data['success'];
        
        $is_good_profile = !in_array($this->input->post('clients_type'), array("mujeres", "mujeres2"));
        
        if (isset($view_data['country_iso']) && $view_data['country_iso'] == 'uy' ) {
            $this->form_validation->set_rules('email', 'E-mail', 'trim|valid_email');
            
            if ($pic_upload_data['success'] === false/* && !$is_good_profile*/) {
                $this->form_validation->set_rules('picture', 'foto de perfil', 'required');
            }
            
        } else {
            $this->form_validation->set_rules('email', 'E-mail', 'trim|required|valid_email');
            
            if ($pic_upload_data['success'] === false) {
                $this->form_validation->set_rules('picture', 'foto de perfil', 'required');
            }
        }
        
        $this->form_validation->set_rules('phone', 'Teléfono', 'trim|required|callback_valid_phone');
        $this->form_validation->set_rules('gender', 'Género', 'trim|required');
        $this->form_validation->set_rules('comments', 'Comentarios', 'trim');
        $this->form_validation->set_rules('user', 'Usuario', 'required|trim|alpha_dash|callback_valid_username_chars|callback_valid_username|is_unique[profiles.proUser]');
        
        $this->form_validation->set_rules('password1', 'Contraseña', 'trim|required|min_length[5]|alpha_dash');
        $this->form_validation->set_rules('password2', 'Repetir Contraseña', 'trim|required|matches[password1]');
        
        if ($this->input->post('gender') == 'hombre') {
            $this->form_validation->set_rules('clients_type', 'Tipo de clientes', 'trim|required');
        }
        
        $this->form_validation->set_rules('about', 'descripción', 'trim|required');
        
        $this->form_validation->set_message('required', 'Has olvidado tu %s.');
        $this->form_validation->set_message('valid_email', 'La dirección de e-mail no está bien escrita.');
        $this->form_validation->set_message('alpha_numeric', 'Tu %s sólo puede ser con letras y números (sin espacios).');
        
        $this->form_validation->set_message('valid_phone', 'Parece que faltan algunos números.');
        
        $this->form_validation->set_message('is_unique', 'Parece ser que "'.$this->input->post('user').'" ya existe, elige otro.');
        $this->form_validation->set_message('valid_username_chars', 'Tu usuario debe tener como mínimo 3 letras.');
        $this->form_validation->set_message('valid_username', 'Parece ser que "'.$this->input->post('user').'" ya existe, elige otro.');
        
        $this->form_validation->set_message('matches', 'Las contraseñas escritas en los dos campos deben ser iguales.');
        $this->form_validation->set_message('alpha_dash', 'Tu %s sólo puede tener letras, números ó guiones (sin espacios).');
        $this->form_validation->set_message('min_length', 'Tu %s debe tener un mínimo de 5 caracteres.');
        
            
        if ( $this->form_validation->run() !== false ) {
            
            $postdata = $this->input->post();
            
            if ($this->blacklist_model->is_blacklisted($postdata)) {
                redirect('register/new_profile_blacklist');
                
            } else {
                $this->load->library('country');

                $country_iso = $this->session->userdata('country');

                if ($country_iso === false) {
                    $country_iso = 'uy';
                }
                
                if (!isset($postdata['comments'])) {
                    $postdata['comments'] = 'Se registró por cuenta propia.';
                }
                
                $user = strtolower($postdata['user']);
                $name = mb_strtoupper(mb_substr($user, 0, 1)).mb_strtolower(mb_substr($user, 1));

                if ($is_good_profile) {
                    $this->db->set('profile_group_id', 0);
                } else {
                    $this->db->set('profile_group_id', 10); // GARBAGE GROUP
                }
                
                $this->db->set('cliPhone', $postdata['phone']);
                $this->db->set('cliComments', $postdata['comments']);

                $this->db->set('cliSource', 'pumbate.com');
                
                $this->db->set('proUser', $user);

                $insert_clients = $this->db->insert('clients');

                $this->db->set('proDomain', '');
                $this->db->set('proPlace', '');
                $this->db->set('proPicture', '');

                $this->db->set('proUser', $user);
                $this->db->set('proName', $name);
                $this->db->set('proEmail', strtolower($postdata['email']));
                
                $this->db->set('proGender', $postdata['gender']);
                
                if ($postdata['gender'] == 'hombre') {
                    switch($postdata['clients_type']) {
                        case 'mujeres' :
                        case 'mujeres2':    
                            $categories = array('c_atiende_chicas'); break;
                        case 'hombres':
                            $categories = array('c_atiende_chicos'); break;
                        default:
                            $categories = array('c_parejas', 'c_atiende_chicas', 'c_atiende_chicos');
                    }
                    
                    $categories_str = implode(' ', $categories);
                    $this->db->set('proCategories', $categories_str);
                }   
                
                $this->db->set('proPhone', $postdata['phone']);
                
                $this->load->library('phoneparser');
                $phones_only_numbers = $this->phoneparser->get_numbers_string($postdata['phone'], $country_iso);
                
                $this->db->set('proNumericPhones', $phones_only_numbers );

                $this->db->set('proCountry', $country_iso);
                $this->db->set('proRegion', $this->country->get_default_region($country_iso));
                $this->db->set('proCity', '');

                $this->db->set('proTitle', $name);
                
                $this->load->library("sexytitle");
                
                $subtitle = $this->sexytitle->create_title(rand(1,3), $postdata['gender'], $country_iso);
                $this->db->set('proSubtitle', $subtitle);
                
                $subtitle2 =  $this->sexytitle->create_phone($postdata['phone'], $postdata['gender']);
                $this->db->set('proSubtitle2', $subtitle2);
                
                
                $this->db->set('proAbout', $postdata["about"]);
                $this->db->set('proServices', '');

                if ($postdata['gender'] == 'hombre') {
                    $this->db->set('proTheme', 'h_fitmen');
                } else {
                    $this->db->set('proTheme', 'amaranth');
                }

                $this->db->set('proVerified', PROFILE_STATUS_NOT_VERIFIED);
                $this->db->set('proAbsent', 0);

                $this->db->set('proPassword', sha1( $this->input->post('password1') ));

                
                $operation_date = date('Y-m-d H:i:s');
                $this->db->set('proCreatedDate', "'" . $operation_date . "'", false);
                
                $insert_profiles =$this->db->insert('profiles');
                
                
                $this->load->model('log_model');
                
                $useragent = isset($_SERVER['HTTP_USER_AGENT'])? $_SERVER['HTTP_USER_AGENT'] : "";

                $metadata = array(
                    'useragent' => $useragent, 
                    'password' => $this->input->post('password1'),
                    'email' => $this->input->post('email')
                );
                
                if ($country_iso === 'uy')
                {
                    $notifyLog = !$this->config->item('all_paid_profiles') && $is_good_profile;
                }
                else
                {
                    $notifyLog = false; // fuck pasion
                    //$notifyLog = $is_good_profile;
                }

                $insert_logs = $this->log_model->insert_log(
                    $country_iso, 'profile_created', $user, $notifyLog, false, $metadata
                );

                
                $this->security_model->penalize_request("profile_create");
                
                
                if ($insert_clients && $insert_profiles && $insert_logs) {

                    /* generate user directories */
                    
                    $this->load->helper('directory');
                    $this->load->helper('directorycopy');

                    //directory_copy('pumbate_master_profile', 'userdata/'.$user);
                    $user_path = 'userdata/'.$user;
                    
                    @mkdir($user_path, 0777);
                    directory_public($user_path, 0777);             
                    
                    if ($pic_upload_data["success"]) {
                        $this->_copy_pics_to_userdata($pic_upload_data["temp_path"], $user_path);
                    }
                    
                    /* save session data */
                    
                    if (!$is_good_profile) {
                        $this->flashsession_model->set_var("profile_is_garbage", true);
                    }
                    
                    $this->flashsession_model->set_var("profile_help", $is_good_profile);
                    $this->flashsession_model->set_var("wait_for_login_user", $user);
                    $this->flashsession_model->set_encrypted_var("wait_for_login_pass", $postdata['password1']);
                    
                    $this->flashsession_model->save(10);
                    
                    if ($this->admin_model->login_exists() === false) {
                        $this->security_model->lock_request("profile_create", 1440);
                    }   

                    $this->load->library('mail_notification');

                    $alert_text = "Estás recibiendo este mail porque el usuario '" . $name . "'\r\n";
                    $alert_text .= "se ha registrado en pumbate.com:\r\n\r\n";
                    $alert_text .= "http://www.pumbate.com/padmin/notifications";

                    if ($is_good_profile) {
                        $this->mail_notification->send_mail( 
                            $this->contact->get_agent_mails($country_iso),
                            $user . ' se ha registrado en pumbate.com',
                            $alert_text
                        );
                    } else {
                        $bl_data = array(
                            'name' => $name, 'description' => "solo_mujeres", "phone" => "", "email" => strtolower($postdata['email'])
                        );
                        
                        foreach (explode(",", $phones_only_numbers) as $num) {
                            if (strlen($num) > 5) {
                                $bl_data["phone"] = $num;
                                $this->blacklist_model->insert_entry($bl_data);
                            }
                        }
                    }


                    if ($postdata['email'] != '') {
                        $welcome_text = "Hola " . $name . ",\r\n\r\nmuchas gracias por registrarte, es un gusto para nosotros darte la bienvenida a Pumbate Escorts.\r\n";
                        $welcome_text .= "Pronto nuestro equipo verificará tu perfil y publicará tu página en nuestra galería de escorts.\r\n\r\n";
                        $welcome_text .= "Recuerda que tu página YA ha sido creada y ya la puedes ir completando, puedes hacerlo desde la sección 'Mi página'\r\n";
                        $welcome_text .= "que se encuentra arriba a la derecha en la portada de www.pumbate.com o entrando en la siguiente dirección:\r\n\r\n";
                        $welcome_text .= "www.pumbate.com/mipagina\r\n\r\n";
                        $welcome_text .= "Por favor, no olvides tus datos de registro para editar tu página:\r\n";
                        $welcome_text .= "Usuario: ".$postdata['user']."\r\n";
                        $welcome_text .= "Contraseña: ".$postdata['password1']."\r\n\r\n";
                        $welcome_text .= "¿Tienes alguna duda? Siempre puedes escribirnos un mail a rrpp@pumbate.com\r\n";

                        /*
                        if ($view_data['pumbate_phone']) {
                            $welcome_text .= "Ó llamarnos al fono-pumbate: ".$view_data['pumbate_phone']."\r\n";
                        }
                        */

                        $welcome_text .= "\r\n\r\n¡Esperamos que puedas mostrar lo mejor de ti con tu nueva página!";
                        $welcome_text .= "\r\nSiempre a tu servicio, el equipo de Pumbate.com.";

                        $this->mail_notification->send_mail( 
                            array( $postdata['email'] => $name ),
                            'Te damos la bienvenida a Pumbate Escorts',
                            $welcome_text
                        );
                    }

                    $view_data['user'] = $postdata['user'];
                    
                    redirect('register/new_profile_success');
                }
            }
        }
    }
        
    
    private function _copy_pics_to_userdata($temp_path, $user_path) {
        $this->load->helper("profile");
        
        $image_versions = $this->config->item('profile_pictures_versions');
        $force_jpeg = isset($image_versions['']['force_jpeg']) && $image_versions['']['force_jpeg'];        
        
        $found_files = scandir($temp_path);
        
        foreach ($found_files as $found_file) {
            $found_file_path = $temp_path . "/" . $found_file;
                        
            if (is_file($found_file_path)) {
                $new_file_path = $user_path . "/" . $found_file;
                rename($found_file_path, $new_file_path);
                                
                if ($force_jpeg) {
                    orient_image($new_file_path);
                    
                    create_scaled_image($new_file_path, '', $image_versions['']);
                    unset($image_versions['']);
                    
                    $jpeg_file_path = preg_replace('!\.[a-z]+$!i', '.jpg', $new_file_path);
                    
                    rename($new_file_path, $jpeg_file_path); 
                    $new_file_path = $jpeg_file_path;
                }
                
                foreach ($image_versions as $version => $options) {
                    create_scaled_image($new_file_path, $version, $options);
                }
            }   
        }
    }
    
    
    private function _check_pic_upload() {
        $pic_upload_data = array(
            'success' => false,
            'error'   => 'no_pic_uploaded',
        );
        
        $temp_dir = "./tempdata/" . sha1($this->input->ip_address()) . "/";
        
        $pic_upload_data["temp_path"] = $temp_dir;
        
        if (is_dir($temp_dir) && count(scandir($temp_dir)) > 2) {
            $pic_upload_data["success"] = true;
            $pic_upload_data["error"] = "file_already_uploaded"; 
        } else {
            @mkdir($temp_dir);

            $upload_config['upload_path'] = $temp_dir;
            $upload_config['allowed_types'] = 'gif|jpg|jpeg|bmp|png';
            $upload_config['encrypt_name']  = true;
            $upload_config['overwrite'] = true;
            $upload_config['remove_spaces'] = true;

            $this->load->library('upload', $upload_config);

            $successful_upload = $this->upload->do_upload("picture");

            if ($successful_upload) {
                $pic_upload_data['success'] = true;
            } else {
                $raw_data = $this->upload->data();

                if (!$raw_data["is_image"]) {
                    $pic_upload_data["error"] = "invalid_file";
                }
            }
            //print_r($this->upload->data()); die;
        }
        
        return $pic_upload_data;
    }


    private function _massive_registration() {
        
        if ($this->session->userdata('reseller_login') === true || $this->admin_model->login_exists()) {
            return false;
            
        } else {
            return $this->security_model->on_limit_request("profile_create", 3);
        }
    }

    private function _look_for_country_iso() {
        $country_iso = $this->session->userdata('country');

        if ($country_iso === false) {
            $country_iso = 'uy';
        }
        
        return $country_iso;
    }   

}

/* eof */