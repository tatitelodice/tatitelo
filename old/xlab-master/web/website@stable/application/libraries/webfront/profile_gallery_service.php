<?php

class Profile_Gallery_Service {

    private $CI;

    private $db;
    private $redis;

    private $asset;
    private $country;
    private $gender;
    private $keywords;


    public function __construct() {
        $this->CI = &get_instance();

        $this->CI->load->model('profile_model');

        $this->CI->load->library('asset');
        $this->CI->load->library('keywords');
        $this->CI->load->library('country');
        $this->CI->load->library('gender');
        $this->CI->load->library('webfront/site_cache');

        $this->CI->load->helper('text');

        $this->db = &$this->CI->db;

        $this->redis = $this->CI->site_cache->get_client();

        $this->asset = &$this->CI->asset;
        $this->country = &$this->CI->country;
        $this->gender = &$this->CI->gender;
        $this->keywords = &$this->CI->keywords;
    }

    public function get_gallery_data_by_account_type($options)
    {
        $escorts_data = $this->get_gallery_data($options);
        return $this->categorize_gallery_data_by_account_type($escorts_data);
    }


    public function categorize_gallery_data_by_account_type($escorts_data)
    {
        $categorized_escorts = array(
            'superstar' => array()
        ,   'top'       => array()
        ,   'premium'   => array()
        ,   'basic'     => array()
        );

        foreach ($escorts_data as $i => $e)
        {
            $k = 'basic';

            switch($e['account_type'])
            {
                case PROFILE_SUPERSTAR: 
                    $k = 'superstar'; break;
                case PROFILE_TOP:
                    $k = 'top'; break;
                case PROFILE_PREMIUM:
                    $k = 'premium'; break;
            }

            $categorized_escorts[$k][] = $e;
        }

        return $categorized_escorts;
    }


    public function get_gallery_data($options)
    {
        $data_from_cache = false;

        $cache_enabled = $this->CI->config->item('catalog_cache_enabled');
        $logged_user = $this->CI->profile_model->get_logged_user();

        if ($cache_enabled && empty($logged_user))
        {
            $data_from_cache = $this->get_data_from_cache($options);
        }

        if (is_array($data_from_cache))
        {
            $gallery_data = $data_from_cache;
        }
        else
        {
            $gallery_data = $this->generate_gallery_data($options);
        }

        $this->resolve_gallery_data_urls($gallery_data);

        return $gallery_data;
    }


    protected function resolve_gallery_data_urls(&$gallery_data)
    {
        /*
            This must be done in this way because the url used by crondaemon
            user the incorrect domain (localhost) when cache is refreshed.
            Full url is generated on execution time to prevent the 
            aforementioned issue.
        */
        foreach ($gallery_data as &$entry)
        {
            $entry['site'] = site_url($entry['site']);
            $entry['picture'] = $this->asset->url($entry['picture'], 'img');

            if (!empty($entry['thumbs']))
            {
                foreach ($entry['thumbs'] as $t_idx => $t_val)
                {
                    $entry['thumbs'][$t_idx] = $this->asset->url($t_val, 'img');
                }
            }
        }
    }


    public function refresh_gallery_cache()
    {
        $redis = $this->redis;

        $ranking = $redis->zrevrangebyscore(
            'cache:priority:frontgallery'
        ,   '+inf', '(15', 'WITHSCORES'
        ,   'LIMIT', '0', '15'
        );
        //zrevrangebyscore cache:priority:frontgallery +inf (10

        foreach ($ranking as $hash => $points)
        {
            $this->refresh_cache_entry($hash);
        }
    }


    public function refresh_cache_entry($hash)
    {
        $redis = $this->redis;

        $search_params_key = 'cache:params:frontgallery:'.$hash;
        $search_params_str = $redis->get($search_params_key);

        $search_params = json_decode($search_params_str, true);

        if (!is_array($search_params))
        {
            // TODO: remove entry
            return false;
        }

        $search_results = $this->generate_gallery_data(
            $search_params
        );

        $redis->set(
            'cache:result:frontgallery:'.$hash
        ,   json_encode($search_results)
        );
    }

    public function get_data_from_cache($options)
    {
        $redis = $this->redis;

        $options_str = json_encode($options);
        $options_hash = sha1($options_str);

        $data_cachekey = 'cache:result:frontgallery:' . $options_hash;

        $data_from_cache = $redis->exists($data_cachekey);

        if ($data_from_cache)
        {
            $data_from_cache=json_decode($redis->get($data_cachekey), true);
        }
        else
        {
            $redis->zincrby('cache:priority:frontgallery', 1, $options_hash);

            $redis->set(
                'cache:params:frontgallery:' . $options_hash
            ,   $options_str
            );
            //zrevrange cache:priority:frontgallery  0 -1 WITHSCORES
            //zrevrangebyscore cache:priority:frontgallery +inf (10
        }

        return $data_from_cache;
    }


    public function generate_gallery_data($options)
    {
        $allow_without_pictures = $this->CI->config->item('catalog_display_profiles_without_pictures');

        $country_iso = $options['country'];

        $defaults = array(
            'gender'  => false
        ,   'region'  => false
        ,   'city'    => false
        ,   'keywords'=> false 
        ,   'limit'   => false
        ,   'account_level' => false
        );

        $settings = array_merge($defaults, $options);

        $gender   = $settings['gender'];
        $region   = $settings['region'];
        $city     = $settings['city'];
        $keywords = $settings['keywords'];
        $limit    = $settings['limit'];
        $account_level = $settings['account_level'];

        $escorts_data = array();
        
        $this->db->select('profiles.proUser,proAccountType,proDomain,proName,proPhone,proGender,proAbout,proPicture,proPictureType,proVerifiedPictures,proAbsent');
        $this->db->join('keywords', 'profiles.proUser = keywords.proUser', 'left outer');
        
        $country_seo = $this->country->get_country_seo($country_iso);
        $this->db->where('proCountry', $country_iso);
        
        $this->db->where('proVerified', PROFILE_STATUS_VERIFIED);
        
        if ($gender) {
            if (preg_match('!^duo!i', $gender)) {
                $this->db->like('proGender', 'duo', 'after');
            } else {
                $this->db->where('proGender', $gender);
            }

            $gender_seo = $this->gender->get_gender_seo($gender);           
        } else {
            $gender_seo = false;
        }
        
        $this->db->where('proDeleted', 0);
                
        if ($account_level === false && $region) {
            if ($city) {                
                $esc_city = $this->db->escape_like_str($city); 
                $likeloc = "( proCity LIKE '%".$esc_city."%' OR keyLocations LIKE '%".$esc_city."%' )";
                
                $this->db->where($likeloc);
                
            } else {
                $esc_region = $this->db->escape_like_str($region); 
                $likeloc = "( proRegion LIKE '%".$esc_region."%' OR keyLocations LIKE '%".$esc_region."%' )";
                
                $this->db->where($likeloc);
            }
        }
        
        if ($keywords) {
            $keywords = $this->keywords->format_words($keywords);
            
            $keywords = preg_split('!\s+!', $keywords);

            if ( count($keywords) ) {
                
                $keywords_like = "( ";
                
                for ($i=0; $i < count($keywords); $i++) {
                    
                    if ($i > 0) {
                        $keywords_like .= " AND ";
                    }
                    
                    $term = $this->db->escape_like_str($keywords[$i]);
                    
                    if (preg_match('!^\d+$!', $keywords[$i])) {
                        $term = " %$term%";
                    } elseif (strlen($keywords[$i]) < 5) {
                        $term = "% $term%";
                    } else {
                        $term = " %$term%";
                    }
                    
                    $keywords_like .= "keywords.keyFulltext LIKE '" . $term . "'";
                }

                $keywords_like .= " )";

                $this->db->where($keywords_like);   
            }
        }

        if ($account_level) {
            $this->db->where('proAccountType >=', $account_level);
            $this->db->order_by('proAccountType desc, proPriority desc');
        } else {
            $this->db->order_by('proPriority', 'asc');
        }
        
        if ($limit) {
            $this->db->limit($limit);
        }
        
        $query = $this->db->get('profiles');
            
        if ($query->num_rows() > 0) {
            $url_base = '';
            
            if ($country_seo !== false && $gender_seo !== false) {
                $url_base = 'sexo/' . $country_seo . '/' . $gender_seo . '/';
            }

            foreach($query->result_array() as $result_row) {
                $profile_data = $this->_row_to_profile_data($result_row, $url_base);
                
                if (!$profile_data['picture'] && !$allow_without_pictures)
                {
                    continue;
                } 
                else if (!$profile_data['picture'])
                {
                    $profile_data['picture'] = $this->get_demo_picture(
                        $profile_data['gender']
                    );
                }

                $escorts_data[] = $profile_data;
            }
        }
        
        return $escorts_data;
    }


    protected function _row_to_profile_data($result_row, $url_base)
    {
        $profile_data = array();
                
        $prefix = 'pro';

        foreach($result_row as $name => $value) {
            $name = str_replace($prefix, '', $name);
            $name[0] = strtolower($name[0]);
            $profile_data[$name] = $value;
        }
                        
        $profile_data['about'] = character_limiter( $profile_data['about'], 100);
        
        if (preg_match('!^http!i', $profile_data['domain'])) {
            $profile_data['site'] = $profile_data['domain'];
        } else {
            $profile_data['site'] = $url_base . $profile_data['user'];
        }
        
        $profile_data['picture'] = $this->resolve_profile_picture($profile_data);

        // TODO: KILL THIS FIELD
        $profile_data['picture_type'] = 'vertical';
        
        $profile_data['verified_pictures'] = $profile_data['verifiedPictures'];
        $profile_data['account_type'] = $profile_data['accountType'];
        
        
        if ($profile_data['accountType'] > PROFILE_BASIC) {
            $profile_data['thumbs'] = $this->get_profile_thumbs(
                $profile_data
            );
        }

        return $profile_data;
    }


    public function resolve_profile_picture($profile_data)
    {
        $resolved_pic_path = false;

        $pic_path = 'userdata/'.$profile_data['user'].'/w200/'.$profile_data['picture'];

        if ($profile_data['picture'] != '' && file_exists($pic_path)) {
            $resolved_pic_path = $pic_path;
        } else {
            $resolved_pic_path = $this->get_any_profile_picture(
                $profile_data['user']
            );
        }

        return $resolved_pic_path;
    }


    public function get_profile_thumbs($profile_data)
    {
        $thumbs = array();

        $basethumb = 'userdata/'.$profile_data['user'].'/w200';
        
        if (is_dir($basethumb)) {
            $thumbs_files = scandir($basethumb);

            $pic_excluded = preg_replace("!.*/!", "", $profile_data['picture']);

            for ($f=2; count($thumbs) < 3 && $f < count($thumbs_files); $f++) {

                if ($thumbs_files[$f] != $pic_excluded) {
                    $thumbs[] = $basethumb . '/'. $thumbs_files[$f];
                }

            }

            if (count($thumbs) < 3 ) {
                $thumbs[] = $profile_data["picture"];
            }
        }

        return $thumbs;
    }


    public function get_demo_picture($gender=false)
    {
        $gender_letter = ($gender == 'hombre')? 'h' : '';
        $pic = 'img/muestra/'.$gender_letter . rand(1,4).'.jpg';
        return $pic;
    }


    public function get_any_profile_picture($user)
    {
        $pic = false;

        $user_dir = 'userdata/'.$user.'/';
        
        if (is_dir($user_dir)) {
            foreach (scandir($user_dir) as $f) {
                if (preg_match('!\.(jpe?g|png|gif)$!i', $f)) {
                    $pic = 'userdata/'.$user.'/w200/'.$f;
                    break;
                }
            }           
        }
        
        return $pic;
    }

}

/* eof */