<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of phone
 *
 * @author Santiago
 */
	class PhoneParser {
		
		protected $country_regexps = array(
			'uy' => array(
				array( 'regexp' => '[42]\d{7}', 'length' => 8 ),
				array( 'regexp' => '09\d{7}', 'length' => 9 ),
			),
			'ar' => array(
				
			),
			'co' => array(
				array( 'regexp' => '3\d{9}', 'length' => 10 ),
				array( 'regexp' => '[2-8]\d{6}', 'length' => 7 ),
			),
			'cl' => array(
				
			)
		);
		
		public function get_numbers_string($phones_string, $country_iso) {
			
			$numbers_string = preg_replace('![^0-9]!', '', $phones_string);;
			
			switch($country_iso) {
				case 'uy':
					$numbers_found = $this->find_numbers($numbers_string, $country_iso);
					
					if (count($numbers_found)) {
						$numbers_string =  implode(',', $numbers_found);
					}
					
					break;
					
				default :
					break;
			}
			
			return $numbers_string;
		}
		
		public function find_numbers($numbers_string, $country_iso) {
			$numbers = array();

			//echo '<br>' . $numbers_string . '<br>';
			
			$country_regexps = $this->get_country_regexps($country_iso);
			
			if (count($country_regexps)) {
				$min_required_chars = 0;
				$searches_limit = 10;
				$searches_count = 0;
				
				while ($min_required_chars  <= strlen($numbers_string) && $searches_count < $searches_limit) {
					foreach ( $country_regexps as $regexp_record) {
						$found = false;
						
						if ($min_required_chars == 0 || $regexp_record['length'] < $min_required_chars) {
							$min_required_chars = $regexp_record['length'];
						}
						
						if (preg_match('!^' . $regexp_record['regexp'] . '!', $numbers_string, $match)) {
							$numbers[] = $match[0];
							$numbers_string = substr($numbers_string, $regexp_record['length']);
							$found = true;
							break;
						}
					}
					
					if (!$found) {
						$numbers_string = substr($numbers_string, $min_required_chars);
					}
					
					//echo $numbers_string . '<br>';
					
					$searches_count++;
				}
			}
			
			return $numbers;
		}
		
		public function get_country_regexps($country_iso) {
			if (isset($this->country_regexps[$country_iso])) {
				return $this->country_regexps[$country_iso];
			} else {
				return array();
			}
		}
	}

/* end of file */
