<?php

class keywords {
	
	private $CI;

	public function __construct() {
		$this->CI = &get_instance();
	}
	
	public function generate($raw_fulltext_keywords) {
		$raw_fulltext_keywords = $this->format_words($raw_fulltext_keywords);

		$raw_fulltext_keywords .= $this->find_extra_keywords($raw_fulltext_keywords);
		
		$raw_fulltext_keywords = explode(' ', $raw_fulltext_keywords);
		$raw_fulltext_keywords = array_unique($raw_fulltext_keywords);

		$fulltext_keywords = array_filter($raw_fulltext_keywords, function ($word)
		{
			return strlen($word) > 2 || preg_match('!\d!', $word);
		});

		$fulltext_keywords = implode(" ", $fulltext_keywords);

		return $fulltext_keywords;
	}
	

	public function find_extra_keywords($raw_fulltext_keywords) {
		
		$found = '';
		
		$patterns_to_keywords = array(
			'colita' => 'anal',
			'pete' => 'oral',
			'lesbi' => 'trio',
			'pareja' => 'trio',
			'apto|apartamento|departamento|privado|lugar|propio' => 'apartamento departamento chicas chicos apto con lugar propio privado',
			'bucal' => 'oral',
			'morena' => 'morocha',
			'trio' => 'duo',
			'sado' => 'fetichismo',
		);
		
		
		foreach ($patterns_to_keywords as $patt => $key) {
			
			if (preg_match('!'.$patt.'!i', $raw_fulltext_keywords)) {
				$found .= ' ' . $key;
			}
		}
		
		return $found;
		
	}
	
	
	public function format_words($keywords) {
		$keywords = trim($keywords);
		
		$this->CI->load->helper('text');
		$keywords = convert_accented_characters($keywords);
		
		$keywords = strtolower($keywords);
		$keywords = preg_replace('![^a-zA-Z0-9]!', ' ', $keywords);
		$keywords = preg_replace('!\s+!', ' ', $keywords);
		$keywords = trim($keywords);

		return $keywords;
	}
	
}

/* end of file */
