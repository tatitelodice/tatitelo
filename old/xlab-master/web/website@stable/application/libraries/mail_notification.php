<?php

use \Swift_Mailer as Swift_Mailer;
use \Swift_Message as Swift_Message;
use \Swift_SmtpTransport as Swift_SmtpTransport;

class Mail_Notification {
	
	private $CI;
	
	protected $use_ssl; // deprecated
	protected $encryption;
	
	protected $mail_server;   // 'ibe.iberoemprende.com'
	protected $mail_port;     // '465'
	
	protected $mail_user;     // 'rrpp@pumbate.com'
	protected $mail_password; //'rrpp.'
	
	protected $mail_from;
	protected $mail_reply_to;
	
	
	public function __construct() {
		$this->CI = &get_instance();
		
		$this->use_ssl       = $this->CI->config->item('notification_mail_use_ssl');
		$this->encryption    = $this->CI->config->item('notification_mail_encryption');
		
		$this->mail_server   = $this->CI->config->item('notification_mail_server');
		$this->mail_port     = $this->CI->config->item('notification_mail_port');
		$this->mail_user     = $this->CI->config->item('notification_mail_user');
		$this->mail_password = $this->CI->config->item('notification_mail_password');
		
		$this->mail_from = array(
			$this->mail_user => $this->CI->config->item('notification_mail_name')
		);
		
		$this->mail_reply_to = false;
	}
	
	
	public function set_mail_from($sender_account, $sender_name=false) {
		$this->mail_from = $this->format_accounts($sender_account, $sender_name);
	}
	
	
	public function set_mail_reply_to($reply_account, $reply_name=false) {
		$this->mail_reply_to = $this->format_accounts($reply_account, $reply_name);
	}
	
	
	public function format_accounts($account_address, $account_name = false) {
		
		if (!is_array($account_address)) {
			if ($account_name) {
				$account_address = array( $account_address => $account_name);
			} else {
				$account_address = array($account_address);
			}
		}
		
		return $account_address;
	}
	
	
	public function send_mail($addresses, $title, $text) {		
		$security = null;
		
		if ($this->use_ssl) {
			$security = 'tls';
		}

		if (!empty($this->encryption))
		{
			$security = $this->encryption;
		}
		
		$transport = new Swift_SmtpTransport($this->mail_server, $this->mail_port, $security);

		$transport
			->setUsername($this->mail_user)
			->setPassword($this->mail_password)
		;

		if (!empty($this->encryption))
		{
			$transport->setStreamOptions([
				'ssl' => [
					'allow_self_signed' => true
				,	'verify_peer' => false
				,   "verify_peer_name" => false
				]
			]);
		}

		$message = new Swift_Message();

		$message
			// Give the message a subject
			->setSubject($title)
			// Set the From address with an associative array
			->setFrom($this->mail_from)
			// Set the To addresses with an associative array
			->setTo($addresses)
			// Give it a body
			->setBody($text);

		if (is_array($this->mail_reply_to)) {
			$message->setReplyTo($this->mail_reply_to);
		}
		
		//Create the Mailer using your created Transport
		$mailer = new Swift_Mailer($transport);

		//Send the message
		try {
			$result = $mailer->send($message);
		} catch(Exception $e){
			$result = false;
		}
		
		return $result;
	}
	
	
	public function show_settings() {
		$settings = get_object_vars($this);
		unset($settings['CI']);
		var_dump($settings);
	}
	
}




/* end of notification_mail */
