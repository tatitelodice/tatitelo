<?php

use \Google_Client as Google_Client;
use \Google_Service_Analytics as Google_Service_Analytics;

class Analytics {

	protected $client;
	protected $analytics;
			
	protected $profileId = '71509256';
	
	
	public function startClient() {
		// create client object and set app name
		$this->client = new Google_Client();
		$this->client->setApplicationName('Pumbate'); // name of your app

		if (getenv('GOOGLE_APPLICATION_CREDENTIALS'))
		{
			$this->client->useApplicationDefaultCredentials();
		}
		else
		{
			$this->client->setAuthConfig('../api-keys/google-analytics.json');
		}

		$this->client->setScopes(['https://www.googleapis.com/auth/analytics.readonly']);
		
		$this->analytics = new Google_Service_Analytics($this->client);	
	}
	
	
	public function getResultsByPage($path = '/', $startDate = false, $endDate = false) {

		/*  
		 * $optParams = array(
			'dimensions' => 'ga:source,ga:keyword',
			'sort' => '-ga:visits,ga:source',
			'filters' => 'ga:medium==organic',
			'max-results' => '25');
		*/
		$operator = '=';
		
		if (stripos($path, '~') === 0) {
			$operator = '';
		}

		$opts = array(
			'filters' => 'ga:pagePath=' . $operator . $path,
			'dimensions' => 'ga:date',
		);

	   $res = $this->analytics->data_ga->get(
		   'ga:' . $this->profileId,
		   $startDate,
		   $endDate,
		   //'ga:uniquePageviews',
			'ga:pageviews,ga:uniquePageviews',
			$opts
		);
		
		//print_r($res); die;
		
		return $res;
	}
	
	public function getFirstProfileId(&$analytics) {
		$accounts = $analytics->management_accounts->listManagementAccounts();

		//print_r($accounts); die;

		if (count($accounts->getItems()) > 0) {
			$items = $accounts->getItems();
			$firstAccountId = $items[0]->getId();

			$webproperties = $analytics->management_webproperties
					->listManagementWebproperties($firstAccountId);

			if (count($webproperties->getItems()) > 0) {
				$items = $webproperties->getItems();
				$firstWebpropertyId = $items[0]->getId();

				$profiles = $analytics->management_profiles
						->listManagementProfiles($firstAccountId, $firstWebpropertyId);

				if (count($profiles->getItems()) > 0) {
					$items = $profiles->getItems();
					return $items[0]->getId();
				} else {
					throw new Exception('No profiles found for this user.');
				}
			} else {
				throw new Exception('No webproperties found for this user.');
			}
		} else {
			throw new Exception('No accounts found for this user.');
		}
	}

}

/* end of file */
