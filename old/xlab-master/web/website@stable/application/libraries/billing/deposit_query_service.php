<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Deposit_Query_Service {
    
    protected $depositModel;
    protected $paymentAccountModel;
    
    public function __construct() {
        $CI = &get_instance();
        $CI->load->model('deposit_model');
        $CI->load->model('payment_account_model');
        $this->depositModel = &$CI->deposit_model;
        $this->paymentAccountModel = &$CI->payment_account_model;
    }

    public function makeReport($options)
    {
        $options = $this->initReportOptions($options);

        if ($options['paymentAccountIds'] !== false)
        {
            $paymentAccounts = $this->paymentAccountModel->findBy([
                'id' => $options['paymentAccountIds']
            ]);
            $options['paymentAccountId'] = collect($paymentAccounts)->pluck('id')->all();

            if ($options['includeNullAccount'])
            {
                $options['paymentAccountId'][] = null;
            }
        }
        else
        {
            $paymentAccounts = $this->paymentAccountModel->getAll();
        }

        $deposits = $this->depositModel->get_all_deposits($options);

        $deposits = $this->pairDepositswithPaymentAccounts(
            $deposits, $paymentAccounts
        );

        $totalAmount = 0;
            
        foreach ($deposits as $deposit) {           
            $totalAmount += $deposit['amount'];
        }

        $report = [
            'deposits' => $deposits
        ,   'startDate' => $options['startDate']
        ,   'endDate' => $options['endDate']
        ,   'totalAmount' => $totalAmount
        ];

        return $report;
    }


    protected function pairDepositswithPaymentAccounts($deposits, $paymentAccounts)
    {
        $paymentAccountsById = collect($paymentAccounts)->keyBy('id');

        foreach ($deposits as $depKey => $depData)
        {
            $depData['paymentAccount'] = null;
            $paymentAccountId = $depData['paymentAccountId'];
            if ($paymentAccountId && $paymentAccountsById->get($paymentAccountId))
            {
                $depData['paymentAccount'] = $paymentAccountsById->get($paymentAccountId);
            }

            $deposits[$depKey] = $depData;
        }

        return $deposits;
    }


    protected function initReportOptions($options)
    {
        $startDate = date('Y-m-01');
        $startDate = (!empty($options['startDate'])) ? $options['startDate'] : $startDate;
        
        if ( preg_match('!(\d+)/(\d+)/(\d\d\d\d)!', $startDate, $dp ) )
        {
            $startDate = $dp[3] . '-' . $dp[2] . '-' . $dp[1];
        }
        
        $endDate = date("Y-m-t");
        $endDate = (!empty($options['endDate'])) ? $options['endDate'] : $endDate;
        
        if ( preg_match('!(\d+)/(\d+)/(\d\d\d\d)!', $endDate, $dp ) )
        {
            $endDate = $dp[3] . '-' . $dp[2] . '-' . $dp[1];
        }

        $paymentAccountIds = false;

        if (isset($options['paymentAccountIds']) && is_array($options['paymentAccountIds']))
        {
            $paymentAccountIds = $options['paymentAccountIds'];
        }

        $includeNullAccount = true;
        
        if (isset($options['includeNullAccount']) && !$options['includeNullAccount'])
        {
            $includeNullAccount = false;
        }

        return [
            'startDate' => $startDate    
        ,   'endDate' => $endDate
        ,   'paymentAccountIds' => $paymentAccountIds
        ,   'includeNullAccount' => $includeNullAccount
        ];
    }

}

/* eof */