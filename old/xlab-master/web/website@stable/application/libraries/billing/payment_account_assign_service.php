<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Payment_Account_Assign_Service {
    
    protected $db;
    protected $paymentAccountModel;
    
    public function __construct() {
        $CI = &get_instance();
        $CI->load->model('payment_account_model');
        $this->db = &$CI->db;
        $this->paymentAccountModel = &$CI->payment_account_model;
    }

    public function assignLastDepositAccountAsPaymentAccount()
    {
        $stm = 'update payments pay 
        inner join deposits dep on pay.lastDepositId = dep.depId 
        left outer join payment_account pa on dep.paymentAccountId = pa.id and pa.isActive=1
        set pay.paymentAccountId = pa.id;';

        $this->db->query($stm);
    }

}

/* eof */