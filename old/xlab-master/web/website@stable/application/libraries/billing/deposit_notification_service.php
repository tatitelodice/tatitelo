<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Deposit_Notification_Service {

    protected $log_model;
    protected $profile_model;
    protected $customer_message_model;
    protected $deposit_model;
    protected $deposit_notification_model;


    public function __construct() {
        $CI = &get_instance();

        $CI->load->model('profile_model');
        $this->profile_model = &$CI->profile_model;

        $CI->load->model('log_model');
        $this->log_model = &$CI->log_model;

        $CI->load->model('customer_message_model');
        $this->customer_message_model = &$CI->customer_message_model;

        $CI->load->model('deposit_model');
        $this->deposit_model = &$CI->deposit_model;

        $CI->load->model('deposit_notification_model');
        $this->deposit_notification_model = &$CI->deposit_notification_model;
    }


    public function get_submit_permissions($profile, $deposit)
    {
        $notificationsFromDate = false;

        $user = $profile['user'];

        $prevDeposit = $this->deposit_model->get_profile_last_deposit($user);

        if ($prevDeposit)
        {
            $notificationsFromDate = preg_replace(
                '!(\d+)/(\d+)/(\d+)!', '$3-$2-$1', $prevDeposit['date_applied']
            );

            $m = new \Moment\Moment($notificationsFromDate); $m->addDays(4);
            $notificationsFromDate = $m->format('Y-m-d');
        }

        $notifications = $this->get_recent_notifications($profile, $notificationsFromDate);
        
        $submit = true;
        $activate = count($notifications) < 2;

        if (!$prevDeposit && count($notifications) >= 3)
        {
            $submit = false;
        }

        if ($prevDeposit && count($notifications) >= 5)
        {
            $submit = false;
        }

        return [ 'submit' => $submit, 'activate' => $activate ];
    }


    public function is_submit_duplicated($profile, $deposit)
    {
        $notifications = $this->get_recent_notifications($profile);

        if (count($notifications) > 0)
        {
            $lastNotification = $notifications[0];
            $lastNotificationDeposit = $lastNotification['deposit'];
            
            $sameDeposit = true;

            $attrs = ['photo', 'amount', 'date', 'name'];

            foreach ($attrs as $a)
            {
                $sameDeposit = $sameDeposit && ($deposit[$a] === $lastNotificationDeposit[$a]);
            }

            return $sameDeposit;
        }

        return false;
    }


    public function submit_deposit_notification($profile, $deposit, $activateProfile)
    {
        $attributes = $this->generate_attributes($profile, $deposit);

        $messageId = $this->create_inbox_message($profile, $deposit, $attributes);

        $deposit['messageId'] = $messageId;
        $attributes['deposit']['messageId'] = $messageId;

        $attributes['message'] = [
            'id' => $messageId
        ];

        $this->create_notification($profile, $deposit, $attributes);

        if ($activateProfile)
        {
            $this->activate_profile($profile, $deposit);
        }
    }


    protected function get_recent_notifications($profile, $fromDate=false)
    {
        $notifications = $this->deposit_notification_model->find_by([
            'user' => $profile['user']
        ,   'fromDate' => $fromDate
        ]);

        return $notifications;
    }


    protected function generate_attributes($profile, $deposit)
    {
        $attributes = [ 
            'deposit' => $deposit 
        ,   'profiles' => [
                [
                    'id' => $profile['id']
                ,   'user' => $profile['user']
                ,   'gender' => $profile['gender']
                ]
            ]
        ];

        return $attributes;
    }


    protected function create_notification($profile, $deposit, $attributes)
    {
        $user = $profile['user'];

        $this->log_model->insert_log(
            $profile['country'], 'deposit_notification', $user
        ,   true, false, $attributes
        );
    }


    protected function create_inbox_message($profile, $deposit, $attributes)
    {
        $message = 'depositado ' . $deposit['user'] 
        .   ' $' . $deposit['amount'] 
        .   ' a nombre de ' . $deposit['name'] 
        .   ' el dia ' . $deposit['date'] 
        .   ' a las ' . $deposit['time']
        .   '. ' . $deposit['comments']
        ;

        $message = trim($message);

        $messageId = $this->customer_message_model->create_inbox_message([
            'uuid' => \Ramsey\Uuid\Uuid::uuid4()
        ,   'message_text' => $message
        ,   'message_address' => $deposit['phone']
        ,   'system_address' => 'web'
        ,   'system_datetime' => date('c')
        ,   'attributes' => $attributes
        ]);

        return $messageId;
    }


    protected function activate_profile($profile, $deposit)
    {
        $user = $profile['user'];
        $this->profile_model->enable_profile($user);
        $this->profile_model->change_profile_panel_message($user, '');
    }
}