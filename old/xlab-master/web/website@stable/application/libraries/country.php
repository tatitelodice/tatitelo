<?php

class Country {
	
	protected $countries = array(
		'uy' => 'Uruguay',
		'co' => 'Colombia',
		'ar' => 'Argentina',
		'cl' => 'Chile',
		'mx' => 'México',
		've' => 'Venezuela',
		'py' => 'Paraguay',
	);
	
	protected $default_regions = array(
		'uy' => 'Montevideo',
		'co' => 'Distrito Capital de Bogotá',
		'ar' => 'Capital Federal',
		'cl' => 'Santiago',
		'mx' => 'Distrito Federal',
		've' => 'Distrito Capital',
		'py' => 'Asunción',
	);
	 	
	
	private $CI;

	public function __construct() {
		$this->CI = &get_instance();
		$this->CI->load->library('webfront/site_cache');
	}
	

	public function name($iso) {
		return $this->countries[$iso];
	}
	
	
	public function is_valid($iso) {
		return isset($this->countries[$iso]);
	}
	
	public function get_countries() {
		return $this->countries;
	}
	
	public function get_country_iso($country_name) {
		$country_name = trim(strtolower($country_name));
		
		foreach ($this->countries as $iso => $name) {
			$name = strtolower($name);
			
			if ($country_name == $name) {
				return $iso;
			}
		}
		
		return false;
	}
	
	public function get_country_seo($country_iso) {
		return strtolower($this->countries[$country_iso]);
	}
	
	
	public function get_default_region($iso) {
		return $this->default_regions[$iso];
	}
	
	
	public function get_cities($countryIso, $regionName) {
		$redis = $this->CI->site_cache->get_client();
		$data = json_decode($redis->get('locations:' . $countryIso), true);
        
        if (!isset($data[$regionName]))
        {
            return null;
        }

        return $data[$regionName];
	}
	
	public function get_active_cities($countryIso, $regionName, $gender) {
		$redis = $this->CI->site_cache->get_client();
		$rawData = $redis->get('profiles:locations:'.$countryIso.':'.$gender);
		$locationsTree = json_decode($rawData, true);
		return $locationsTree[$regionName];
	}
	
		
	public function get_regions($countryIso) {
		$redis = $this->CI->site_cache->get_client();	
		$data = json_decode($redis->get('locations:' . $countryIso), true);
        return array_keys($data);	
	}
	
	public function get_active_regions($countryIso, $gender) {
		$redis = $this->CI->site_cache->get_client();
		$rawData = $redis->get('profiles:locations:'.$countryIso.':'.$gender);
		$locationsTree = json_decode($rawData, true);
		return array_keys($locationsTree);
	}
	
	
	public function update_active_locations_cache($arg_country = false) {
		@mkdir(APPPATH . "cache/active_location/");

		$this->CI->load->library('gender');
		$genders = array_keys($this->CI->gender->get_genders());
		
		foreach (array_keys($this->countries) as $iso) {
			
			if ($arg_country && $arg_country != $iso) {
				continue;
			}
			
			$regions = array();

			$this->CI->db->select('regName');
			$this->CI->db->where('regActive', 1);
			$this->CI->db->where('regCountry', $iso);
			$this->CI->db->order_by('regName', 'asc');

			$query = $this->CI->db->get('regions');

			if ($query->num_rows() > 0) {

				foreach ($query->result_array() as $result_row) {
					$regions[] = $result_row['regName'];
				}
				
				$data = array();
			
				foreach ($genders as $gender) {
					$data[$gender] = array('<?php' . PHP_EOL);
					$data[$gender][] = '$active_location["'.$iso.'"] = array();';
				}
				
				foreach ($regions as $region) {
					$escaped_region = str_replace("'", "\\'", $region);
					$q = "SELECT proGender FROM profiles p LEFT JOIN keywords k ON p.proUser = k.proUser ";
					$q .= "WHERE proCountry = '".$iso."' AND proVerified = " . PROFILE_STATUS_VERIFIED . " AND (proRegion like '".$escaped_region."' OR keyLocations like '%".$escaped_region."%')";
					$q .= "GROUP BY proGender";
					
					$res = $this->CI->db->query($q);
					
					if ($res->num_rows()) {
						foreach ($res->result_array() as $region_gender_row) {
							$region_gender = array_shift($region_gender_row);
							
							$this->CI->db->select('citName');
							$this->CI->db->where('citActive', 1);
							$this->CI->db->where('citCountry', $iso);
							$this->CI->db->where('regName', $region);
							$this->CI->db->order_by('citName', 'asc');
							$query = $this->CI->db->get('cities');
							
							$cities = array();
							
							if ($query->num_rows() > 0) {
								foreach ($query->result_array() as $result_row) {
									$cities[] = $result_row['citName'];
								}
							}
							
							$region_line = '$active_location["'.$iso.'"]["'.$region.'"] = array(';
							
							foreach ($cities as $city) {
								$escaped_city = str_replace("'", "\\'", $city);
								$qc = "SELECT COUNT(*) AS total FROM profiles p LEFT JOIN keywords k ON p.proUser = k.proUser ";
								$qc .= "WHERE proCountry = '".$iso."' AND proVerified = " . PROFILE_STATUS_VERIFIED . " AND proGender = '".$region_gender."' AND (proCity like '".$escaped_city."' OR keyLocations like '%:".$escaped_city."%') ";
								
								$resc = $this->CI->db->query($qc);
								
								if ($resc->num_rows()) {
									if (array_shift($resc->row_array()) > 0) {
										$region_line .= ' "' . trim(str_replace('"', '\"', $city)) . '",';
									}
								}
							}
							
							$region_line .= ');';
							
							$data[$region_gender][] = $region_line;
						}
					}
				}
				

				foreach ($genders as $gender) {
					$data[$gender][] = '/* end of file */';
					
					$output = implode(PHP_EOL, $data[$gender]);

					$this->CI->load->helper('file');
					write_file(APPPATH . "cache/active_location/".$iso."_".$gender.".php", $output);
				}
			}
		}
	}
	
	
	public function update_locations_cache() {
		@mkdir(APPPATH . "cache/location/");

		foreach (array_keys($this->countries) as $iso) {
			$regions = array();

			$this->CI->db->select('regName');
			$this->CI->db->where('regActive', 1);
			$this->CI->db->where('regCountry', $iso);
			$this->CI->db->order_by('regName', 'asc');

			$query = $this->CI->db->get('regions');

			if ($query->num_rows() > 0) {
				$data = array('<?php' . PHP_EOL);

				$data[] = '$location["'.$iso.'"] = array();';

				foreach ($query->result_array() as $result_row) {
					$regions[] = $result_row['regName'];
				}


				foreach ($regions as $region) {
					$this->CI->db->select('citName');
					$this->CI->db->where('citActive', 1);
					$this->CI->db->where('citCountry', $iso);
					$this->CI->db->where('regName', $region);
					$this->CI->db->order_by('citName', 'asc');

					$query = $this->CI->db->get('cities');

					$line = '$location["'.$iso.'"]["'.$region.'"] = array(';

					if ($query->num_rows() > 0) {

						foreach ($query->result_array() as $result_row) {
							$line .= ' "' . trim(str_replace('"', '\"', $result_row['citName'])) . '",';
						}
					}

					$line .= ');';

					$data[] = $line;

				}

				$data[] = '/* end of file */';

				$output = implode(PHP_EOL, $data);

				$this->CI->load->helper('file');
				write_file(APPPATH . "cache/location/".$iso.".php", $output);

				//file_put_contents(APPPATH . "cache/routes.php", $output);
			}
		}
	}
	
}

/* end of file */
