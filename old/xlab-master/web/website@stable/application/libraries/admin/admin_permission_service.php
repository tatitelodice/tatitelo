<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Admin_Permission_Service {
    
    protected $aclModel;
    protected $adminModel;
    
    public function __construct() {
        $CI = &get_instance();
        $CI->load->model('administrator_acl_model');
        $CI->load->model('admin_model');

        $this->aclModel = &$CI->administrator_acl_model;
        $this->adminModel = &$CI->admin_model;
    }

    public function isGranted($resource, $resourceId=null, $permission='full') 
    {
        $conditions = $this->buildQueryConditions($resource, $resourceId, $permission);
        
        $rules = $this->aclModel->findBy($conditions); 
        
        foreach ($rules as $rule)
        {
            if ($rule['permission'] === $permission)
            {
                return true;
            }
        }

        return false;
    }


    public function getPermissions($resource=false, $resourceId=null)
    {
        $conditions = $this->buildQueryConditions($resource, $resourceId, false);
        $rules = $this->aclModel->findBy($conditions);
        return $rules;
    }


    protected function buildQueryConditions($resource, $resourceId, $permission=false)
    {
        $adminId = $this->adminModel->get_id();

        $conditions = [
            'adminId' => $adminId
        ];

        if ($resource)
        {
            $conditions['resource'] = $resource;
        }

        if ($resource && $resourceId !== null)
        {
            $conditions['resourceId'] = $resourceId;
        }

        if ($permission)
        {
            $conditions['permission'] = $permission;
        }

        return $conditions;
    }

}

/* eof */