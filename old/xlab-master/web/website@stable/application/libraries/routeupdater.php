<?php
	if ( ! defined('BASEPATH')) exit('No direct script access allowed');

	class RouteUpdater {
		private $CI;

		public function __construct() {
			$this->CI = &get_instance();
		}
		
		public function update() {
			$this->CI->db->select('proUser');
			$query = $this->CI->db->get('profiles');

			if ($query->num_rows() > 0) {
				$data = array('<?php' . PHP_EOL);

				foreach ($query->result_array() as $result_row) {
					$row = $result_row['proUser'];

					$data[] = '$route["(?i)' . $row . '"] = "profilesite/view/'.$row.'";';
					$data[] = '$route["(?i)' . $row . '/panel"] = "profile/panel/'.$row.'";';
				}

				$output = implode(PHP_EOL, $data);

				$this->CI->load->helper('file');
				write_file(APPPATH . "cache/routes.php", $output);

				//file_put_contents(APPPATH . "cache/routes.php", $output);
			}
		}
	}

?>
