<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Profile_Publication_Status_Service {
    
    protected $CI;
    
    public function __construct() {
        $this->CI = &get_instance();
    }

    public function get_publication_status($profile)
    {
        $data = [];

        $data['is_active'] = $profile['verified'] == PROFILE_STATUS_VERIFIED;
        $data['is_available'] = $profile['absent'] == 0;

        $data['level'] = $this->get_publication_level_text(
            $profile['gender'], $profile['accountType']
        );

        $data['status'] = $this->get_publication_status_text(
            $profile['absent']
        );
 
        return $data;
    }

    public function get_publication_level_text($gender, $accountType)
    {
        $texts = $this->get_publication_level_texts();

        $genderTexts = $texts[$gender];

        $publicationLevelText = false;

        foreach ($genderTexts as $item)
        {
            if ($item['accountType'] === intval($accountType))
            {
                $publicationLevelText = $item['text'];
                break;
            }
        }

        return $publicationLevelText;
    }


    public function get_publication_status_text($absentVal)
    {
        if ($absentVal)
        {
            return 'no disponible';
        }

        return 'disponible';
    }


    public function get_publication_level_texts()
    {
        return [
            'mujer'  => [
                [ 'accountType' => 0, 'text' => 'básica' ]
            ,   [ 'accountType' => 1, 'text' => 'básica' ]
            ,   [ 'accountType' => 2, 'text' => 'destacada' ]
            ,   [ 'accountType' => 3, 'text' => 'top']
            ,   [ 'accountType' => 4, 'text' => 'super top']
            ]
        ,   'hombre' => [
                [ 'accountType' => 0, 'text' => 'básica' ]
            ,   [ 'accountType' => 1, 'text' => 'básica' ]
            ,   [ 'accountType' => 2, 'text' => 'destacado' ]
            ,   [ 'accountType' => 3, 'text' => 'top' ]
            ,   [ 'accountType' => 4, 'text' => 'super top']
            ]
        ,   'trans'  => [
                [ 'accountType' => 0, 'text' => 'básica' ]
            ,   [ 'accountType' => 1, 'text' => 'básica' ]
            ,   [ 'accountType' => 2, 'text' => 'destacada' ]
            ,   [ 'accountType' => 3, 'text' => 'top']
            ,   [ 'accountType' => 4, 'text' => 'super top']
            ]
        ];
    }


}
