<?php
    
class Profile_Panel_Service {

    private $CI;
    protected $db;

    public function __construct() {
        $this->CI = &get_instance();
        $this->db = &$this->CI->db;
    }


    public function get_themes($user, $user_selected = false, $gender = 'mujer', $account_type = PROFILE_FREE ) {
        $themes = array();

        $theme_dirs = scandir(APPPATH . '/views/themes');

        for ($d=2; $d < count($theme_dirs); $d++) {
            $theme_dir = $theme_dirs[$d];

            if ($theme_dir[0] != '_' && $theme_dir[0] != '.') { 
                
                $fixed_name = preg_replace('!^[pdh]_!', '', $theme_dir);
                $fixed_name = ucfirst(str_replace('_', ' ', $fixed_name));
                
                $theme_data = array(
                    'dir' => $theme_dir,
                    'name' => $fixed_name,
                    'thumbnail' => base_url('assets/themes/'.$theme_dir . '/thumbnail.jpg'), 
                    'screenshot' => base_url('assets/themes/'.$theme_dir . '/screenshot.jpg'),
                    'preview' => site_url('profilesite/view/' . $user . "?theme=" . $theme_dir),
                );
                
                if ( $gender != 'hombre' && substr($theme_dir, 0, 2) != 'h_' ) {
                    
                    $valid_free = substr($theme_dir, 0, 2) != 'p_' && $account_type >= PROFILE_FREE;
                    $valid_basic = substr($theme_dir, 0, 2) == 'p_' && $account_type > PROFILE_FREE;
                    
                    if ($valid_free || $valid_basic) {
                        $themes[] = $theme_data;
                    }
                    
                } elseif ( $gender == 'hombre' && substr($theme_dir, 0, 2) == 'h_' ) {
                    $themes[] = $theme_data;
                }
                
            }
        }

        for ($p=0; $p < count($themes); $p++) {
            if ($user_selected == $themes[$p]['dir']) {
                list($themes[$p], $themes[0]) = array($themes[0], $themes[$p]);
                break;
            }
        }

        return $themes;
    }

}

/* eof */