<?php
    
class Profile_Pictures_Service {

    protected $CI;
    protected $db;
    protected $_profiles_data;

    public function __construct() {
        $this->CI = &get_instance();
        $this->db = &$this->CI->db;
        $this->_profiles_data = array();
    }

    public function update_main_picture($user, $pic_path)
    {
        $pic_path = ($pic_path) ? $pic_path : '';
        $this->db->set('proPicture', $pic_path);
        $this->db->where('proUser', $user);
        $this->db->limit(1);
        $this->db->update('profiles');
    }


    public function get_profile_pictures_files($user)
    {
        $pictures = array();
        
        $user_dir = 'userdata/'.$user.'/';
        
        if (!is_dir($user_dir)) {
            @mkdir($user_dir, 0777);
            return $pictures;
        }

        foreach (scandir($user_dir) as $f) {
            if (preg_match('!\.(jpe?g|png|gif|bmp)!i', $f)) {
                $pic_path = 'userdata/'.$user.'/'.$f;
                $pictures[] = $pic_path;
            }
        }

        return $pictures;
    }


    public function get_profile_pictures($user, $with_info = false) {
        $pictures = array();

        $picture_files = $this->get_profile_pictures_files($user);

        foreach ($picture_files as $pic_path)
        {
            $url = base_url($pic_path);

            if ($with_info) {
                $img_info = getimagesize($pic_path);
                
                $type = 'vertical';

                if ($img_info[0] > $img_info[1] && $img_info[1] * 100 / $img_info[0] < 90 ) {   
                    $type = 'horizontal';   
                }

                $pictures[] = array(
                    'url' => $url
                ,   'path' => $pic_path
                ,   'type' => $type
                ,   'width' => $img_info[0]
                ,   'height' => $img_info[1]
                );
            } else {
                $pictures[] = $url;
            }
        }

        return $pictures;
    }


    public function regenerate_profile_pictures($user, $force=false)
    {
        //create_scaled_image($file_path, $version, $options);
        $picture_files = $this->get_profile_pictures_files($user);
        $versions = $this->get_pictures_versions($user);

        foreach ($picture_files as $picture_path)
        {
            $pic_fname = basename($picture_path);
            $pic_dirname = dirname($picture_path);

            foreach ($versions as $version => $version_opts)
            {
                $pic_version_path = $pic_dirname . '/' . $version . '/' . $pic_fname;

                if (!$force && file_exists($pic_version_path))
                {
                    continue;
                }

                $this->create_scaled_image(
                    $picture_path, $version, $version_opts
                );
            }
        }
    }


    public function get_pictures_versions($user)
    {
        $conf_versions = $this->CI->config->item('profile_pictures_versions');

        $profile_data = $this->get_profile_data($user);
        $account_type = PROFILE_FREE;
        
        if ($profile_data && isset($profile_data['accountType']))
        {
            $account_type = $profile_data['accountType'];
        }

        $filtered_versions = array();

        foreach ($conf_versions as $k => $v)
        {
            if (isset($v['required_account_type']))
            {
                $req_type = $v['required_account_type'];

                $invalid_type1 = is_array($req_type) && !in_array($account_type, $req_type);
                $invalid_type2 = is_int($req_type) && ($account_type < $req_type);
                
                if ($invalid_type1 || $invalid_type2)
                {
                    continue;
                }
            }

            $filtered_versions[$k] = $v;
        }

        return $filtered_versions;
    }


    public function handle_update_pictures_request($user)
    {
        require( APPPATH . 'libraries/upload_handler_profile.php');

        $options = array(
            'script_url' => current_url(),
            'upload_dir' => 'userdata/' . $user . '/',
            'upload_url' => base_url('userdata/' . $user) . '/',
            'user_dirs' => false,
            'mkdir_mode' => 0777,
            'param_name' => 'files',
            // Set the following option to 'POST', if your server does not support
            // DELETE requests. This is a parameter sent to the client:
            'delete_type' => 'DELETE',
            'access_control_allow_origin' => '*',
            'access_control_allow_credentials' => false,
            'access_control_allow_methods' => array(
                'OPTIONS',
                'HEAD',
                'GET',
                'POST',
                'PUT',
                'PATCH',
                'DELETE'
            ),
            'access_control_allow_headers' => array(
                'Content-Type',
                'Content-Range',
                'Content-Disposition'
            ),
            // Enable to provide file downloads via GET requests to the PHP script:
            'download_via_php' => false,
            // Defines which files can be displayed inline when downloaded:
            'inline_file_types' => '/\.(gif|jpe?g|png)$/i',
            // Defines which files (based on their names) are accepted for upload:
            'accept_file_types' => '/.+\.(jpe?g|png|gif)$/i',
            // The php.ini settings upload_max_filesize and post_max_size
            // take precedence over the following max_file_size setting:
            'max_file_size' => null,
            'min_file_size' => 1,
            // The maximum number of files for the upload directory:
            'max_number_of_files' => 100,
            // Image resolution restrictions:
            'max_width' => null,
            'max_height' => null,
            'min_width' => 1,
            'min_height' => 1,
            // Set the following option to false to enable resumable uploads:
            'discard_aborted_uploads' => true,
            // Set to true to rotate images based on EXIF meta data, if available:
            'orient_image' => true,
            'image_versions' => $this->get_pictures_versions($user)
        );

        $upload_handler = new UploadHandlerProfile($options);

        $this->CI->load->helper('directory');
        $this->CI->load->helper('directorycopy');
        directory_public('userdata/' . $user . '/', 0777);
    }


    public function set_profile_data($user, $data)
    {
        $this->_profiles_data[$user] = $data;
    }


    protected function get_profile_data($user)
    {
        if (!empty($this->_profiles_data[$user]))
        {
            return $this->_profiles_data[$user];
        }

        return null;
    }

    public function create_scaled_image($file_path, $version, $options) {
        $file_name = basename($file_path);
        
        $img_ext = strtolower(substr(strrchr($file_name, '.'), 1));
        $force_jpeg = isset($options["force_jpeg"]) && $options["force_jpeg"];
        $format_change = !in_array($img_ext, array('jpg', 'jpeg'));
        $crop_picture = isset($options["center_crop"]) && $options["center_crop"];
        $expand_picture = !empty($options['min_width']) || !empty($options['min_height']);

        if (!empty($version)) {
            $version_dir = dirname($file_path) . DIRECTORY_SEPARATOR . $version;
            
            if (!is_dir($version_dir)) {
                mkdir($version_dir, 0777, true);
            }
            $new_file_path = $version_dir . '/' . $file_name;
        } else {
            $new_file_path = $file_path;
        }

        list($img_width, $img_height) = @getimagesize($file_path);

        if (!$img_width || !$img_height) {
            return false;
        }
        
        /* GET WIDTH SCALE */

        //print_r($options);

        if (!empty($options['min_width']) && $options['min_width'] > $img_width) {
            $width_scale =  $options['min_width'] / $img_width;
        } else {
            $width_scale =  $options['max_width'] / $img_width;
        }

        /* GET HEIGHT SCALE */
        
        if (!empty($options['min_height']) && $options['min_height'] > $img_height) {
            $height_scale =  $options['min_height'] / $img_height;
        } else {
            $height_scale =  $options['max_height'] / $img_height;
        }


        /* SELECT PREFERED SCALE */

        if ($crop_picture) {
            $scale = ($width_scale < $height_scale) ? $height_scale : $width_scale;
        } else {
            $scale = min($width_scale, $height_scale);
        }
        
        
        if (!$expand_picture && $scale >= 1 && !$format_change) {
            if ($file_path !== $new_file_path) {
                return copy($file_path, $new_file_path);
            }
            return true;
        }
        
        $new_width  = round($img_width * $scale);
        $new_height = round($img_height * $scale);

        $new_img = @imagecreatetruecolor($new_width, $new_height);

        switch ($img_ext) {
            case 'jpg':
            case 'jpeg':
                $src_img = @imagecreatefromjpeg($file_path);
                break;
            case 'gif':
                $src_img = @imagecreatefromgif($file_path);
                break;
            case 'png':
                $src_img = @imagecreatefrompng($file_path);
                break;
            default:
                $src_img = null;
        }

        if ($force_jpeg) {
            $img_ext = "jpg";
        }

        switch ($img_ext) {
            case 'jpg':
            case 'jpeg':
                $write_image = 'imagejpeg';
                $image_quality = isset($options['jpeg_quality']) ?
                        $options['jpeg_quality'] : 75;
                break;
            case 'gif':
                @imagecolortransparent($new_img, @imagecolorallocate($new_img, 0, 0, 0));
                $write_image = 'imagegif';
                $image_quality = null;
                break;
            case 'png':
                @imagecolortransparent($new_img, @imagecolorallocate($new_img, 0, 0, 0));
                @imagealphablending($new_img, false);
                @imagesavealpha($new_img, true);
                $write_image = 'imagepng';
                $image_quality = isset($options['png_quality']) ?
                        $options['png_quality'] : 9;
                break;
            default:
                $src_img = null;
        }

        $resampling_done = @imagecopyresampled(
            $new_img, $src_img, 0, 0, 0, 0, $new_width, $new_height, $img_width, $img_height
        );

        if ($resampling_done && $crop_picture) {
            $width = $options["max_width"];
            $height = $options["max_height"];

            $new_img2 = @imagecreatetruecolor($width, $height);

            $s = @imagecopyresampled(
                $new_img2, $new_img,
                0, 0,
                floor(($new_width - $width) / 2),
                floor(($new_height - $height) / 2),
                floor(($new_width - $width) / 2) + $width,
                floor(($new_height - $height) / 2) + $height,
                floor(($new_width - $width) / 2) + $width,
                floor(($new_height - $height) / 2) + $height
            );

            @imagedestroy($new_img);
            $new_img = $new_img2;
        }

        $success = $src_img && $resampling_done && $write_image($new_img, $new_file_path, $image_quality);
        // Free up memory (imagedestroy does not delete files):

        @imagedestroy($src_img);
        @imagedestroy($new_img);

        return $success;
    }

}

/* eof */