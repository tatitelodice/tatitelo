<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Profile_Billing_Status_Service {
    
    protected $CI;

    protected $payments_model;
    protected $deposit_model;
    protected $log_model;
    protected $pricing_model;
    protected $profile_model;
    
    public function __construct() {
        $this->CI = &get_instance();

        $this->CI->load->model('deposit_model');
        $this->CI->load->model('log_model');
        $this->CI->load->model('payments_model');
        $this->CI->load->model('pricing_model');

        $this->deposit_model = $this->CI->deposit_model;
        $this->log_model = $this->CI->log_model;
        $this->payments_model = $this->CI->payments_model;
        $this->pricing_model = $this->CI->pricing_model;
        $this->profile_model = $this->CI->profile_model;
    }

    public function get_billing_status($user)
    {
        $next_payment_date = null;
        $days_remaining = 0;
        $notify_due_date = false;

        $last_notification = $this->log_model->find_one_by([
            'proUser' => $user, 'logDescription' => 'deposit_notification'
        ], 'logId desc');

        $profile = $this->profile_model->get_profile($user);

        $payments_status = $this->payments_model->get_profile_payments_status($user);
        $last_deposit = $this->deposit_model->get_profile_last_deposit($user);
        
        $has_pending_deposit = $last_notification && $last_notification['notify'];

        if ($last_deposit && !$has_pending_deposit)
        {
            /*
            $days_remaining = $this->get_deposit_due_days($profile, $last_deposit);

            if ($payments_status['days_remaining'] && $payments_status['days_remaining'] > $days_remaining)
            {
                $days_remaining = $payments_status['days_remaining'];
            }
            */
            $days_remaining = $payments_status['days_remaining'];

            if ($days_remaining < 7)
            {
                $notify_due_date = true;
            }
        }

        if ($payments_status['nextdate'])
        {
            $next_payment_date = $payments_status['nextdate']->format('d/m/Y');
        }

        $data = [
            'last_deposit' => $last_deposit
        ,   'last_deposit_notification' => $last_notification
        ,   'notify_due_date' => $notify_due_date
        ,   'days_remaining' => $days_remaining
        ,   'next_payment_date' => $next_payment_date
        ,   'has_pending_deposit' => $has_pending_deposit
        ];

        return $data;
    }

    public function get_pricing($profile)
    {
        return $this->pricing_model->get_pricing_for_profile($profile);
    }

    public function get_deposit_due_days($profile, $deposit)
    {
        $today_dt = new \Moment\Moment();

        $deposit_dt = preg_replace('!(\d+)/(\d+)/(\d+)!', '$3-$2-$1', $deposit['date_applied']);
        $deposit_dt = new \Moment\Moment($deposit_dt);

        $pricing = $this->get_pricing($profile);
        $account_type = $profile["accountType"];

		$due_days = $this->pricing_model->resolve_publication_days(
			$pricing, $account_type, $deposit['amount']
		);

        $deposit_threshold_dt = $deposit_dt->cloning()->addDays($due_days);

        $due_days = intval($today_dt->from($deposit_threshold_dt)->getDays());

        return $due_days;
    }

}


        