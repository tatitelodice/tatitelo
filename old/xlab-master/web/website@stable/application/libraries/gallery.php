<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of gallery
 *
 * @author Santiago
 */
class Gallery {
	
	/*
	 * GET A LIBGD HANDLER
	 */
	public function get_image_handler($filepath) {
		$image = null;
		
		if (preg_match('!\.gif$!i', $filepath)) {
			$image = imagecreatefromgif($filepath);
		} elseif (preg_match('!\.png$!i', $filepath)) {
			$image = imagecreatefrompng($filepath);
		} else {
			$image = imagecreatefromjpeg($filepath);
		}
		
		return $image;
	}
	
	public function save_image(&$image, $filepath) {
		
		if (preg_match('!\.gif$!i', $filepath)) {
			imagegif($image, $filepath);
			
		} elseif (preg_match('!\.png$!i', $filepath)) {
			imagepng($image, $filepath);
			
		} else {
			imagejpeg($image, $filepath);
		}
	}
	
	public function image_fix_orientation(&$image, $filepath) {
		$exif = exif_read_data($filepath);

		if (!empty($exif['Orientation'])) {
			switch ($exif['Orientation']) {
				case 3:
					$image = imagerotate($image, 180, 0);
					break;

				case 6:
					$image = imagerotate($image, -90, 0);
					break;

				case 8:
					$image = imagerotate($image, 90, 0);
					break;
			}
		}
	}
}

/* end of file */