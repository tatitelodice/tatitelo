<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/******************************************************************************/
/* CATALOG                                                                    */
/******************************************************************************/
$config['catalog_display_profiles_without_pictures'] = false;
$config['catalog_cache_enabled'] = true;

/******************************************************************************/
/* Email users                                                                */
/******************************************************************************/

$config["email_notification_users"] = true;
$config["email_notification_admins"] = true;
