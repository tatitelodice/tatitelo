<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');


/******************************************************************************/
/* Email users                                                                */
/******************************************************************************/

$config["email_notification_users"] = false;
$config["email_notification_admins"] = false;

/******************************************************************************/
/* CATALOG                                                                    */
/******************************************************************************/
$config['catalog_display_profiles_without_pictures'] = true;
$config['catalog_cache_enabled'] = true;