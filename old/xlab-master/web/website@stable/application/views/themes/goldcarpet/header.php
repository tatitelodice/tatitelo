<!DOCTYPE html>
<html lang="es">
	<head>
		<meta charset="utf-8">
		
		<title><?php echo ( isset($head_title) && $head_title)? $head_title : strip_tags($title . ' - ' . $subtitle); ?></title>
		
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<meta name="description" content="<?php echo strip_tags("$title $subtitle $phone $about $services"); ?>">
		<meta name="author" content="Pumbate Escorts - www.pumbate.com">

		<link rel="shortcut icon" href="<?php echo $common_path; ?>img/favicon.png">
		
		<!-- Le styles -->
		<link href="<?php echo $common_path; ?>css/initialize.css" rel="stylesheet">
		<link href="<?php echo $common_path; ?>css/font_baskerville.css" rel="stylesheet">
		<link href="<?php echo $path; ?>style.css" rel="stylesheet">
		
		<script src="<?php echo $common_path; ?>js/common.js"></script>
	</head>

	<body>