
<div class="container">
	
	<!--<div class="row">
		<div class="span10 offset1">
			<div class="navbar navbar-main">
				<div class="navbar-inner">
					<ul class="nav text-center">
						<li><a onClick="moveTo('galeria')" href="#galeria"><i class="glyphicon glyphicon-star"></i>Galería de Fotos</a></li>
						<li><a onClick="moveTo('acerca')" href="#acerca"><i class="glyphicon glyphicon-star"></i>Acerca de Mí</a></li>
						<li><a onClick="moveTo('servicios')" href="#servicios"><i class="glyphicon glyphicon-star"></i>Mis Servicios</a></li>
					</ul>
				</div>
			</div>
		</div>
	</div>-->
	
	<div class="row">

		<div class="span12 text-center">
			<div class="hero">
				<h1><?php echo $title; ?></h1>

				<h2><?php echo $subtitle; ?></h2>
				<h3 class="text-center"><?php echo $subtitle2; ?></h3>
			</div>
		</div>

		<div class="span12">
			<hr class="hr-main">
		</div>
	</div>


	
	<div class="row">
		<div class="span10 offset1">
			
			<h3 id="galeria" class="sectitle text-left">
				Mis Fotos
				<?php 
					if (isset($verified_pictures)) {
						$about_verification_aopen = '';
						$about_verification_aclose = '';

						if (isset($about_verification_url) && $about_verification_url) {
							$about_verification_aopen = '<a href="'.$about_verification_url.'">';
							$about_verification_aclose = '</a>';
						}

						if ($verified_pictures) {
							$verification_pic = '<img class="verified-banner" src="' . $path . 'img/verifiedclub.png">';
						} else {
							$verification_pic = '<img class="verified-banner" src="' . $path . 'img/notverifiedclub.png">';
						}

						echo $about_verification_aopen . $verification_pic . $about_verification_aclose;
					}
				?>
			</h3>
			
			<div class="gallery">
				<div class="row-fluid row-gallery">
				<?php
					$hidden_block = '';
					$pics_per_row = 6;
					$positions = count($pictures);
				
					for ($p=0; $p < $positions; $p++) {
						$pic_url = preg_replace('!/[^/]+\.[A-Z0-9]+$!i', "/optimized/$0", $pictures[$p]['src']);
						$thumb_url = preg_replace('!/[^/]+\.[A-Z0-9]+$!i', "/w200/$0", $pictures[$p]['src']);
						$pic_class = $pictures[$p]['type'];

						$hide = '';
						
						$pic_block  = '<div class="span2"><div class="block-gallery'.$hidden_block.'">';
						
						if ($positions > $pics_per_row) {
							
							if ($p == $pics_per_row - 1) {
								$pic_block .= '<div id="morepics-pic" class="img-wrap"><img class="vertical" src="'.$path.'img/morepics.png" alt="Más Fotos"></div>';
								$hide = ' hide';
							}
							
							if ($p > $pics_per_row - 2) {
								$hidden_block = ' hidden-block';
							}
						}
						
						$pic_block .= '<a class="fancybox'.$hide.'" rel="gallery" href="'.$pic_url.'" title="">';
						$pic_block .= '<div class="img-wrap"><img class="'.$pic_class.'" src="'.$thumb_url.'" alt="Foto '.($p + 1).' de '.count($pictures).'"></div>';
						$pic_block .= '</a>';
						
						$pic_block .= '</div></div>';
						
						echo $pic_block;

						if ($p < $positions - 1 && $p > 0 && ($p+1) % $pics_per_row  == 0) {
							echo '</div><div class="row-fluid row-gallery'.$hidden_block.'">';
						}
					}
				?>
				</div>
			
				<div class="clearfix"></div>
			</div>
		</div>
		
	</div>
	
	<div class="row">

		<div class="span4">
			<h3 id="acerca" class="sectitle text-center">Acerca de mí</h3>
			<div class="well well-large">
				<ul class="information">
					<li><i class="icon glyphicon glyphicon-star"></i>&nbsp;<?php echo $name; ?>&nbsp;<i class="icon glyphicon glyphicon-star"></i></li>
					<?php if ($age > 17): ?>
					<li>
						<i class="icon glyphicon glyphicon-user"></i>&nbsp;Edad:&nbsp;<br>
						<div class="value">
							<?php echo $age; ?> años
						</div>
					</li>
					<?php endif;?>
				</ul>
				<p class="lead"><?php echo $about; ?></p>
			</div>
		</div>	
		
		<div class="span4">
			<h3 id="servicios" class="sectitle text-center">Mis servicios</h3>
			<div class="well well-large">
				<ul class="information">
				<?php if ($place): ?>
					<li>
						<i class="icon glyphicon glyphicon-map-marker"></i>&nbsp;Lugar de atención:&nbsp;<br>
						<div class="value">
							<?php echo $place; ?>
						</div>
					</li>
					<?php endif;?>
										
					<li>
						<i class="icon glyphicon glyphicon-map-marker"></i>&nbsp;Ubicación:&nbsp;<br>
						<div class="value">
							<?php echo $location; ?>
						</div>
					</li>
				</ul>
				<p class="lead"><?php echo $services; ?></p>
			</div>
		</div>
		
		<div class="span4">
			<h3 id="info" class="sectitle">Contactate conmigo</h3>
			
			<div class="well well-large">
				
				<ul class="information">
					
					<li>
						<i class="icon glyphicon glyphicon-asterisk"></i>&nbsp;Teléfono:&nbsp;<br>
						<div class="value">
							<?php echo '<span class="big-phone">'  . $phone . '</span>' . $phone_ad; ?>
						</div>
					</li>
				</ul>
			</div>
		</div>
	</div>
	

	
	<div class="row">
		<div class="span12">
			<hr>
			<?php if ( isset($footer_big_text) && $footer_big_text ): ?>
			<h2 class="outer-header"><?php echo $footer_big_text ?></h2>
			<?php endif; ?>
			<h5 class="text-center"><a class="pumbate-link" href="http://www.pumbate.com"><span class="logo-text"><?php echo $name; ?>&nbsp;publica en</span><img src="<?php echo $path; ?>img/pumbatelogo.png"></a></h5>
		</div>
	</div>
	
</div> <!-- /container -->
