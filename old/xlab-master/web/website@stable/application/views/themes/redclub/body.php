
<div class="container">
	<div class="row">
		
		<div class="span10 offset1 background-container">
			<div class="row-fluid">
					<div class="navbar navbar-main">
						<div class="navbar-inner">
							<ul class="nav text-center">
								<li><a onClick="moveTo('galeria')" href="#galeria"><i class="glyphicon glyphicon-star-empty"></i>Galería de Fotos</a></li>
								<li><a onClick="moveTo('acerca')" href="#acerca"><i class="glyphicon glyphicon-star-empty"></i>Acerca de Mí</a></li>
								<li><a onClick="moveTo('servicios')" href="#servicios"><i class="glyphicon glyphicon-star-empty"></i>Mis Servicios</a></li>
							</ul>
						</div>
					</div>
			</div>
			<div class="row-fluid">
				<div class="span12">
					<div class="text-center">
						<h1><?php echo $title; ?></h1>
						<h2><?php echo $subtitle; ?></h2>
						<hr>
						<h3><?php echo $subtitle2; ?></h3>
						<hr>
					</div>
				</div>
			</div>

			<div class="row-fluid">
				<div class="span8">
					<h3 id="galeria" class="sectitle">
						Galería de Fotos
						<?php 
							if (isset($verified_pictures)) {
								$about_verification_aopen = '';
								$about_verification_aclose = '';

								if (isset($about_verification_url) && $about_verification_url) {
									$about_verification_aopen = '<a href="'.$about_verification_url.'">';
									$about_verification_aclose = '</a>';
								}

								if ($verified_pictures) {
									$verification_pic = '<img class="verified-banner" src="' . $path . 'img/verifiedrose.png">';
								} else {
									$verification_pic = '<img class="verified-banner" src="' . $path . 'img/notverifiedrose.png">';
								}

								echo $about_verification_aopen . $verification_pic . $about_verification_aclose;
							}
						?>
					</h3>
					<div class="gallery">
						<div class="row-fluid row-gallery">
						<?php
							$hidden_block = '';

							for ($p=0, $c=count($pictures); $p < $c; $p++) {
								$pic_url = preg_replace('!/[^/]+\.[A-Z0-9]+$!i', "/optimized/$0", $pictures[$p]['src']);
								$thumb_url = preg_replace('!/[^/]+\.[A-Z0-9]+$!i', "/w200/$0", $pictures[$p]['src']);
								$pic_class = $pictures[$p]['type'];

								echo '<div class="span4"><div class="block-gallery'.$hidden_block.'">';
								echo '<a class="fancybox" rel="gallery" href="'.$pic_url.'" title="">';
								echo '<div class="img-wrap"><img class="'.$pic_class.'" src="'.$thumb_url.'" alt="Foto '.($p + 1).' de '.count($pictures).'"></div>';
								echo '</a>';
								echo '</div></div>';

								if ($p > 4 && $c > 6) {
									$hidden_block = ' hidden-block';
								}

								if ($p < $c - 1 && $p > 0 && ($p+1) % 3 == 0) {
									echo '</div><div class="row-fluid row-gallery'.$hidden_block.'">';
								}

							}
						?>
						</div>

						<?php if ($hidden_block != '') : ?>
						<div class="row-fluid">
							<div class="span12 text-center">
								<button id="morepics" class="btn btn-large btn-main">Ver más fotos!</button>
							</div>	
						</div>
						<?php endif; ?>

					</div>
				</div>

				<div class="span4">
					<h3 id="info" class="sectitle">Contactate conmigo</h3>
					<div class="well well-large">
						<ul class="information">
							<li class="text-center"><i class="glyphicon glyphicon-star"></i>&nbsp;<?php echo $name; ?>&nbsp;<i class="glyphicon glyphicon-star"></i></li>

							<?php if ($age > 17): ?>
							<li>
								<i class="glyphicon glyphicon-user"></i>&nbsp;Edad:&nbsp;<br>
								<div class="value">
									<?php echo $age; ?> años
								</div>
							</li>
							<?php endif;?>

							<li>
								<i class="glyphicon glyphicon-map-marker"></i>&nbsp;Ubicación:&nbsp;<br>
								<div class="value">
									<?php echo $location; ?>
								</div>
							</li>

							<?php if ($place): ?>
							<li>
								<i class="glyphicon glyphicon-map-marker"></i>&nbsp;Lugar de atención:&nbsp;<br>
								<div class="value">
									<?php echo $place; ?>
								</div>
							</li>
							<?php endif;?>

							<li>
								<i class="glyphicon glyphicon-asterisk"></i>&nbsp;Teléfono:&nbsp;<br>
								<div class="value">
									<?php echo $phone . $phone_ad; ?>
								</div>
							</li>
						</ul>
					</div>
				</div>

			</div>

			<div class="row-fluid">
				<div class="span12">
					<h3 id="acerca" class="sectitle">Acerca de mí</h3>
					<div class="well well-large">
						<br><p class="lead"><?php echo $about; ?></p>
					</div>
					<br>
				</div>	
			</div>

			<div class="row-fluid">
				<div class="span12">
					<h3 id="servicios" class="sectitle">Mis servicios</h3>
					<div class="well well-large">
						<br><p class="lead"><?php echo $services; ?></p>
					</div>
					<br>
				</div>	
			</div> 	

			<div class="row-fluid">
				<div class="span12">
					<hr>
					<?php if ( isset($footer_big_text) && $footer_big_text ): ?>
					<h2 class="outer-header"><?php echo $footer_big_text ?></h2>
					<?php endif; ?>
					<h5 class="text-center"><a class="pumbate-link" href="http://www.pumbate.com"><span class="logo-text"><?php echo $name; ?>&nbsp;publica en</span><img src="<?php echo $path; ?>img/pumbatelogo.png"></a></h5>
				</div>
			</div>
		</div>
	</div>
</div> <!-- /container -->
