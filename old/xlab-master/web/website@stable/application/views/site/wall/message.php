<div class="container header-container">
	<div class="row">
		<div class="span5">
			<h1 class="logo">
				<a title="Muro Pumbate" href="<?php echo site_url('wall'); ?>"><img title="Muro Pumbate" alt="Muro Pumbate Logo" src="<?php echo base_url('img/logomuro.png'); ?>"></a>
			</h1>
			<h2 class="logo">
				Avisos e información de interés
			</h2>
		</div>
		<div id="main-nav" class="span7">
			<h3>
				<a href="<?php echo site_url('wall'); ?>"><strong>Volver a la página principal del Muro</strong></a>
			</h3>
		</div>
	</div>
</div>

<div class="container page-container">
	<div class="row">
		<div class="span12">
			<div class="page-container-inner">
				<h5>Viendo mensaje completo: </h5>
				<h2 class="walltitle">
					<?php echo $message['title']; ?>
					<small> &mdash; por <?php echo $message['gender_symbol']; ?> <?php echo $message['user']; ?></small>
				</h2>
				<div class="wallmessage">
					<?php if ($admin): ?>
						<div class="actions">
							<?php if ($message['sticky']) : ?>
								<a title="Despegar" href="<?php echo site_url('wall/unstick_message/'.$message['id']) ?>"><i class="glyphicon glyphicon-minus-sign"></i></a>
							<?php else: ?>
								<a title="Pegar arriba del todo" href="<?php echo site_url('wall/stick_message/'.$message['id']) ?>"><i class="glyphicon glyphicon-plus-sign"></i></a>
							<?php endif; ?>

							<?php if ($message['deleted']) : ?>
								<a title="Recuperar" href="<?php echo site_url('wall/restore_message/'.$message['id']) ?>"><i class="glyphicon glyphicon-refresh"></i></a>
							<?php else: ?>
								<a title="Eliminar" href="<?php echo site_url('wall/delete_message/'.$message['id']) ?>"><i class="glyphicon glyphicon-trash"></i></a>
							<?php endif; ?>
						</div>
					<?php endif; ?>
					
					<span class="date">El <?php echo $message['date'] . ' ' . $message['user']; ?> escribió:</span>
					<div class="text" style="padding-bottom: 20px;">
						<?php echo $message['text']; ?>
					</div>
				</div>
				
				<?php if ($message['replies'] > 0): ?>
					<h3><?php echo $message['replies']; ?>&nbsp;Comentario<?php echo ($message['replies'] > 1)? 's' : ''; ?>:</h3>
					
					<?php for ($c=0, $cc = count($comments); $c < $cc; $c++): ?>
					
					<?php
						$comment = $comments[$c];
					?>
					
					<div id="comm<?php echo $comment['id']?>" class="wallcomment <?php if ($comment['deleted']) echo 'wallcomment-deleted'; ?>">
						<?php if ($admin): ?>
							<div class="actions">
								<?php if ($comment['deleted']) : ?>
									<a title="Recuperar" href="<?php echo site_url('wall/restore_comment/'.$comment['id']. '/' . $message['id']) ?>"><i class="glyphicon glyphicon-refresh"></i></a>
								<?php else: ?>
									<a title="Eliminar" href="<?php echo site_url('wall/delete_comment/'.$comment['id'] . '/' . $message['id']) ?>"><i class="glyphicon glyphicon-trash"></i></a>
								<?php endif; ?>
							</div>
						<?php endif; ?>
						<h5 <?php if ($c == $cc - 1) echo 'id="last"'?>>
							<?php
								if ($admin && $comment['deleted']) {
									echo '<span class="label label-deleted">Borrado</span>';
								}
							?>
							<span style="font-weight: normal; margin-right: 5px;"><?php echo $comment['gender_symbol']; ?></span><?php echo $comment['user']; ?> dijo:
						</h5>

						<div class="text">
							<?php echo $comment['text']; ?>
						</div>

						<hr>

						<div class="row-fluid">
							<div class="span12 date" style="text-align: right;">Publicado el <?php echo $comment['date']; ?></div>
						</div>

					</div>
					
					<?php endfor; ?>
				<?php endif; ?>

					
				<h3>Deja un comentario</h3>
				
				<form id="comment" class="form-horizontal" method="post" action="<?php echo site_url('wall/message/'.$id . '#comment'); ?>">
					<div class="row-fluid">
						<div class="offset1 span8">
							<input type="hidden" name="id" value="<?php echo $id; ?>">
							
							<textarea rows="6" class="input-xxlarge" style="min-width: 99%" name="text"><?php echo set_value('description'); ?></textarea>
							<?php echo form_error('text', '<br><br><span class="label label-error label-inline"><i class="glyphicon glyphicon-warning-sign"></i></span><span class="help-inline help-inline-white">', '</span>'); ?>
							
							<p style="margin-top: 10px;" class="text-right">
								<input type="submit" class="btn btn-rose btn-large" value="COMENTAR">
							</p>
						</div>
					</div>
				</form>
					
			</div>
		</div>
	</div>


</div>	