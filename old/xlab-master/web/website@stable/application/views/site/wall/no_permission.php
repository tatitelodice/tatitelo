<div class="container header-container">
	<div class="row">
		<div class="span5">
			<h1 class="logo">
				<a title="Muro Pumbate" href="<?php echo site_url('wall'); ?>"><img title="Muro Pumbate" alt="Muro Pumbate Logo" src="<?php echo base_url('img/logomuro.png'); ?>"></a>
			</h1>
		</div>
	</div>
</div>

<div class="container page-container">
	<div class="row">
		<div class="span12">
			<div class="page-container-inner">
				<h3 class="text-center">No puedes entrar aquí</h3>
				<hr style="margin-bottom: 10px;">
			</div>
		</div>
		
		<div class="span10 offset1">
			<div class="page-container-inner">
				<h4 class="text-center">
					Parece ser que no tienes permiso para ver el muro ya que tu cuenta de usuario Pumbate no está verificada.
				</h4>
				<br>
				<p class="text-center">
					<a class="btn btn-rose btn-large" href="<?php echo site_url('mipagina'); ?>">Volver a Editar Mi Página</a>
				</p>
			</div>
		</div>
		
	</div>
</div>	