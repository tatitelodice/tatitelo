<div class="container header-container">
	<div class="row">
		<div class="span5">
			<h1 class="logo">
				<a title="Muro Pumbate" href="<?php echo site_url('wall'); ?>"><img title="Muro Pumbate" alt="Muro Pumbate Logo" src="<?php echo base_url('img/logomuro.png'); ?>"></a>
			</h1>
			<h2 class="logo">
				Avisos e información de interés
			</h2>
		</div>
		<div id="main-nav" class="span7">
			<h3>
				<a href="<?php echo site_url('wall'); ?>"><strong>Volver a la página principal del Muro</strong></a>
			</h3>
		</div>
	</div>
</div>

<div class="container page-container">
	<div class="row">
		<div class="span12">
			<div class="page-container-inner">
				<h3 class="text-center">Tu comentario ha sido publicado con éxito!</h3>
				<hr style="margin-bottom: 10px;">
			</div>
		</div>
		
		<div class="span10 offset1">
			<div class="page-container-inner">
				<h4 class="text-center">
					Puedes verlo ya en el listado de comentarios del mensaje:<br>
					"<?php echo $title; ?>"
				</h4>
				<br>
				<p class="text-center">
					<a class="btn btn-rose btn-large" href="<?php echo $url; ?>">Ver mi comentario publicado</a>
					<a class="btn btn-rose btn-large" href="<?php echo site_url('wall'); ?>">Volver a la página principal del Muro</a>
				</p>
			</div>
		</div>
		
	</div>
</div>	