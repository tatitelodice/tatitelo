<div class="row">
		<h2 style="text-align: center;">Modo Reseller activado para <?php echo $reseller; ?><h2>	
</div>

<div class="row">
	<div class="span12">
		<h4 class="text-center">
			El modo está activo, por lo que puede registrar
			desde la página principal de pumbate todas las cuentas
			que desee sin limite.
		</h4>
		
	</div>
</div>

<div class="row" style="text-align: right; margin-bottom: 50px;">
	<br><br>
	<h4><a class="btn btn-large btn-main" href="<?php echo site_url('reseller/logout')?>">Cerrar Sesión</a></h4>
</div>