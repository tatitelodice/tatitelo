<?php
	if (!isset($blocks_by_row)) {
		$blocks_by_row = 8;
	}
	
	$premium_rows = 0;
	
	//echo $premium_escorts_count; die;
	
	if ( $premium_escorts_count > $blocks_by_row ) {
		$premium_rows = ceil( $premium_escorts_count / $blocks_by_row );
	}
	
	$premium_titles = array(
		'muj' => 'Chicas Destacadas',
		'hom' => 'Chicos Destacados',
		'tra' => 'Chicas Trans Destacadas',
		'duo' => 'Dúos Destacados',
	);
	
	$gallery_more_title = count($top_escorts)? 'Más ' : '';
	
	$gallery_base_title = '';
	
	if ($keywords) {
		if ($total_results > 0) {
			$gallery_base_title .= $total_results . ' resultado' . (($total_results == 1)? '' : 's' ) . ' para "' . $keywords . '"';
		} else {
			$gallery_base_title .= 'No hay resultados para "' . $keywords . '"';
		}
	} else {

		if ($escorts_gender == 'hombre') {
			$gallery_base_title .= ' Escorts masculinos ';
		} elseif ($escorts_gender == 'trans') {
			$gallery_base_title .= ' Escorts Trans ';
		} elseif (substr ($escorts_gender, 0, 3) == 'duo') {	
			$gallery_base_title .= ' <strong>Dúos</strong> para hacer realidad tu <strong>fantasía</strong> del <strong>trío</strong>';
		} else {
			$gallery_base_title .= ' Chicas Escort ';
		}

		if ($city || $region) {
			$gallery_base_title .= ' que trabajan ';
		}
	}
	
	$gallery_loc_title = '';
	$gallery_loc_title_full = '';
	
	if ($city) {
		$gallery_loc_title .= ' en ' . $city . ', ' . $region;
		$gallery_loc_title_full = $gallery_loc_title;
	} elseif ($region) {
		$gallery_loc_title .= ' en ' . $region;
		$gallery_loc_title_full = $gallery_loc_title;
	} else {
		$gallery_loc_title_full .= ' en ' . $country;
	}
	
?>

<?php if ($keywords): ?>
	<h3 class="gallery-keywords-title"><?php echo $gallery_more_title . $gallery_base_title . $gallery_loc_title_full; ?></h3>

<?php elseif ($premium_rows > 0): ?>
	<div class="gallery-premium-content">		
		<div class="gallery-premium-title">
			<i class="glyphicon glyphicon-star"></i>
			<?php echo $premium_titles[substr ($escorts_gender, 0, 3)] . ' ' . $gallery_loc_title; ?>
			<i class="glyphicon glyphicon-star"></i>
		</div>
			
<?php else: ?>	
	<div class="gallery-normal-content">
		<h3 class="gallery-normal-title"><?php echo $gallery_more_title . $gallery_base_title . $gallery_loc_title_full; ?></h3>
<?php endif; ?>

		
<?php for ($e = 0, $ec = count($escorts); $e < $ec; $e++) : ?>

	<?php
		$escort = $escorts[$e];

		if ($e == 0 || ($e % $blocks_by_row == 0)) {
			
			$ad_block = '';
			
			if (stripos($escort['name'], 'gratis')) {
				$ad_block = ' hidden-phone';
			}
			
			echo '<div class="row-escort'.$ad_block.'">';
		}
		
		$span_escort_classes = "";
		
		if ($escort['account_type'] > PROFILE_BASIC && !$escort['absent'] ) {
			$span_escort_classes .= " premium-tooltip";
		}
		
	?>

	<div class="span-escort<?php echo $span_escort_classes; ?>" position="<?php echo ($e + 1); ?>">

		<?php
			$thumb_url = preg_replace('!/[^/]+\.[A-Z0-9]+$!i', "/w200/$0", $escort['picture']);
			$target = (preg_match('!#!', $escort['site']))? '_self' : '_blank';
			
			$absent_class = '';
			if ($escort['absent']) $absent_class = 'pumbate-adpic-block-absent';
				
			$featured_class = '';
			
			if ($escort["account_type"] > PROFILE_BASIC ) {
				$featured_class = 'pumbate-adpic-block-featured';
				echo '<span class="hide about">' . $escort['about'] . '</span>';
				echo '<span class="hide user">' . $escort['user'] . '</span>';
				echo '<span class="hide phone">' . $escort['phone'] . '</span>';
				
				if (!$escort['absent']) {
					echo '<div class="hide thumbs">';
						
					foreach ($escort['thumbs'] as $th) {
						echo '<img src="' .$th . '">';
					}
					
					echo '</div>';
				}
				
			}
			
		?>
		
		
		<div class="pumbate-adpic-block pumbate-adpic-block-main <?php echo $absent_class . ' ' . $featured_class ?>">
			<div class="pumbate-adpic-wrap pumbate-adpic-wrap-main">
				
				<?php 
					if ($escort['account_type'] > PROFILE_FREE && $escort['verified_pictures']) {
						echo '<div class="featured-star"></div>';
					}
					
					$picture_classes = "vertical";
					
					if ($escort['account_type'] < PROFILE_PREMIUM || $escort['absent'] ) {
						$picture_classes .= " simple-tooltip";	
					} else {
						$picture_classes .= " alt-tooltip";	
					}
				?>
				
				<a target="<?php echo $target; ?>" href="<?php echo $escort['site']; ?>">
					<img class="<?php echo $picture_classes ?>" title="<?php echo $escort['about']; ?>" alt="Foto de perfil de <?php echo $escort['name']; ?>" src="<?php echo $escort['picture'] ?>">
				</a>
				
				<?php if ($escort['absent']): ?>
					<span class="label label-absent">Estaré de viaje</span>
				<?php endif; ?>
			</div>
			<div class="pumbate-adpic-name">
				<a target="<?php echo $target; ?>" href="<?php echo $escort['site']; ?>">
					<?php 
						if (mb_strlen($escort['name']) > 12) {
							$escort['name'] = trim(mb_substr($escort['name'], 0, 12)) . '...';
						}
					
						echo $escort['name']; 
					?>
				</a>
			</div>
		</div>

	</div>

	<?php 
		if ($e+1 == $ec || ( ($e+1) % $blocks_by_row == 0)) {
			echo '</div>';
			
			$row_number = floor( ($e + 1) / $blocks_by_row); 
			
			if ($row_number == $premium_rows && $e+1 != $ec) {
				echo '</div><div class="gallery-normal-content">';
				echo '<h3 class="gallery-normal-title">Más ' . $gallery_base_title . $gallery_loc_title_full . '</h3>';
			}
		}
	?>
<?php endfor; ?>

</div>