<div class="container front-container promopop <?php if (!$promo) echo ' hidden';?>">
	
	<h3 class="news">¡Enterate de lo que pasa en Pumbate!</h3>
	
	<div class="row-fluid">
		<div class="span3 block-banner blog-entries2">
			<h4 class="latest-entries">Noticias Pumbate</h4>
			<div id="scrollbar2">
				<div class="scrollbar"><div class="track"><div class="thumb"><div class="end"></div></div></div></div>
				<div class="viewport">
					<div class="overview">
						<?php if (count($blog_entries)): ?>
							<?php foreach ($blog_entries as $entry) : ?>
								<div class="well">
									<h4>
										<a target="_blank" href="<?php echo $entry['url']; ?>"><?php echo $entry['title'] ?></a>
									</h4>
								</div>
							<?php endforeach; ?>
						<?php endif; ?>
					</div>
				</div>
			</div>
		</div>

		<div class="span3 block-banner hidden-phone">
			<a target="_blank" href="<?php echo site_url('blog/ayuda-como-contactar-debidamente-a-una-pre-paga/'); ?>"><img alt="¿No te atienden? ¿No encuentras cuando llamar? Haz click aquí ¡No te pierdas nuestros consejos a la hora de contactar prepagos!" title="Visitá el blog de Pumbate Escorts Prepagos para enterarte" class="full" src="<?php echo base_url('img/banners/colread.jpg'); ?>"></a>
		</div>
		
		<div class="span3 block-banner hidden-phone">
			<a target="_blank" href="<?php echo site_url('blog/destacate-en-pumbate-prepagos/'); ?>"><img alt="Destaca tu anuncio en Pumbate Prepagos" title="Destaca tu anuncio en Pumbate Prepagos" class="full" src="<?php echo base_url('img/banners/co_pagatop.jpg'); ?>"></a>
		</div>
		
		<div class="span3 block-banner hidden-phone">
			<a ui-effect="scroll" href="#advertise"><img alt="¿Eres prepago?, anunciate en Pumbate" title="¿Eres prepago?, anunciate en Pumbate" class="full" src="<?php echo base_url('img/banners/advertise.jpg'); ?>"></a>
		</div>

	</div>
	
	<?php if (!$maintenance && $suscribe_enabled) : ?>
	<div class="row">
		<div class="span12">
			<div class="newsletter">
				<a href="<?php echo site_url('suscribe'); ?>"><img alt="E-mail Suscripción Pirata" title="Suscribete y recibe las chicas, chicos y trans más hot a tu e-mail" src="<?php echo base_url("img/alertapirata.png"); ?>"></a>
				<a href="<?php echo site_url('suscribe'); ?>" class="btn btn-mini btn-rose ">Dale Acá</a>&nbsp;&nbsp;
				<div class="newsletter-lead">
				y enterate de lo nuevo de Pumbate en <b>
				<?php 
					if ($escorts_gender == 'hombre') {
						echo ' escorts masculinos nuevos ';
					} elseif ($escorts_gender == 'hombre') {
						echo ' trans ';
					} else {
						echo ' chicas escort ';
					}
					
				?></b> directo a tu E-mail Pirata</div>
			</div>
		</div>
	</div>
	<?php endif; ?>	
	
</div> <!-- /container -->