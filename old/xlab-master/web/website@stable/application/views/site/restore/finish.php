	<div class="container">
		<div class="row">
			<div class="span6 offset3 header-container">
				<h3 class="text-center" style="margin-top: 30px;">Sigue los pasos a continuación!</h3>
			</div>
		</div>
	</div>	
		
	<div class="container">
		
		<div class="row">
			<div class="span6 offset3 white-container round-bottom-10">
				<div class="padding-20">
					<p class="text-center big padding-10">
						No olvides lo siguiente! Pronto recibirás tu usuario y una nueva contraseña temporal.
					</p>
					
					<?php if ($success_phone): ?>
					<div class="alert alert-panel alert-pink-noborder padding-20">
						<img class="alert-panel-picture-icon" src="<?php echo base_url('img/phone.png');?>">
						<p class="medium alert-panel-text-picture" style="margin-bottom: 0px;">
							Revisa los mensajes del teléfono <?php echo $success_phone ?> en las próximas 24 horas ya que te
							enviaremos un sms con tu usuario y una contraseña temporal.
						</p>
					</div>
					<?php endif; ?>
					
					<?php if ($success_email): ?>
					<div class="alert alert-panel alert-pink-noborder padding-20">
						<img class="alert-panel-picture-icon" src="<?php echo base_url('img/mail.png');?>">
						<p class="medium alert-panel-text-picture" style="margin-bottom: 0px;">
							Revisa la bandeja de entrada de <?php echo $success_email ?> en unos minutos para encontrar un e-mail con tu usuario y un enlace para ingresar tu
							nueva contraseña.
						</p>
					</div>
					<?php elseif($success_anon_email_sent): ?>
					<div class="alert alert-panel alert-pink-noborder padding-20">
						<img class="alert-panel-picture-icon" src="<?php echo base_url('img/mail.png');?>">
						<p class="medium alert-panel-text-picture" style="margin-bottom: 0px;">
							Revisa la bandeja de entrada de tu mail en unos minutos para encontrar un e-mail con tu usuario y un enlace para ingresar tu
							nueva contraseña.
						</p>
					</div>					
					<?php endif; ?>

					<p class="text-center big" style="margin-top: 20px;">
						<a class="btn btn-main" href="<?php echo site_url('')?>">Entendido! Volver a la portada de Pumbate</a>
					</p>
				</div>
			</div>
		</div>
	</div>
	
	