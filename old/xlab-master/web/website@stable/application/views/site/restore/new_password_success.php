	<div class="container">
		<div class="row">
			<div class="span6 offset3 header-container">
				<h3 style="text-align: center; margin-top: 30px;">Éxito!</h3>
			</div>
		</div>
	</div>	
		
	<div class="container">
		
		<div class="row">
			<div class="span6 offset3 white-container round-bottom-10">
				<div class="padding-20">
					<p class="text-center big padding-10">
						Tu nueva contraseña ha sido registrada
						con éxito, ahora puedes usarla
						para entrar a editar los cambios.
					</p>

					<p class="text-center big">
						<a class="btn btn-main btn-large input-xlarge" href="<?php echo site_url('mipagina')?>">Continuar</a>
					</p>
				</div>
			</div>
		</div>
	</div>
	
	