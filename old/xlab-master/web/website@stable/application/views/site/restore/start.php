	<div class="container">
		<div class="row">
			<div class="span6 offset3 header-container">
				<h3 class="text-center margin-top-50">
					Recupera tu usuario y contraseña
				</h3>
			</div>
		</div>
	</div>	
		
	<div class="container">
		
		<div class="row">
			<div class="span6 offset3 white-container round-bottom-10">
				<form class="form-horizontal form-login" method="post" action="">
					<?php
						$form_error_start = '<div><span class="label label-error label-inline"><i class="glyphicon glyphicon-warning-sign"></i></span><span class="help-inline help-inline-white">';
						$form_error_end = '</span></div>';
					?>
					
					<p class="medium text-center margin-bottom-20">
						Rellena los datos a continuación y sigue las
						instrucciones para recuperar tu usuario y contraseña.
					</p>
					
					<div class="control-group">
						<label class="control-label" for="country">País</label>
						<div class="controls">
							<?php
								echo form_dropdown('country', $country_options, set_value('country', $country), ' id="country" class="input-xlarge" onChange="country_check()"');
								echo form_error('country', '<span class="label label-important">Atención</span><span class="help-inline">', '</span>');
							?>
						</div>			
					</div>
						
					<div class="control-group">
						<label class="control-label" for="user">Usuario</label>
						<div class="controls">
							<input type="text" class="input-xlarge" id="user" name="user" value="<?php echo set_value('user'); ?>" placeholder="(o tu último nombre artistico)">
							<?php echo form_error('user', $form_error_start, $form_error_end); ?>
						</div>
					</div>

					<div class="control-group">
						<label class="control-label" for="email">E-mail<span class="required all not-uy"> (requerido)</span></label>
						<div class="controls">
							<input type="text" class="input-xlarge" id="email" name="email" value="<?php echo set_value('email'); ?>">
							<?php echo form_error('email', $form_error_start, $form_error_end); ?>
						</div>			
					</div>

					<div class="control-group">
						<label class="control-label" for="phone">Teléfono publicado<span class="required uy"> (requerido)</span></label>
						<div class="controls">
							<input type="text" class="input-xlarge" id="phone" placeholder="(número con el que trabajas)" name="phone" value="<?php echo set_value('phone'); ?>">
							<?php echo form_error('phone', $form_error_start, $form_error_end); ?>
						</div>			
					</div>

					<div class="control-group">
						<div class="controls">
							<?php if ($fail): ?>
								<div class="alert alert-pink-noborder input-large text-center">
									<button class="close" data-dismiss="alert">×</button>
									No se han encontrado cuentas de usuario a recuperar
									con los datos ingresados.
								</div>
							<?php endif; ?>

							<button id="aplicar" type="submit" name="send" value="1" class="btn input-xlarge btn-main">Recuperar</button>
						</div>
					</div>
				</form>
				<div class="padding-10"></div>
			</div>
		</div>
	</div>

	<script>
		function country_check() {
			var country = $('#country').val();
			
			$('.required').each(function() {
				if ($(this).hasClass(country)) {
					$(this).removeClass('hidden');
				} else {
					if ($(this).hasClass('all')) {
						if ($(this).hasClass('not-' + country)) {
							$(this).addClass('hidden');
						} else {
							$(this).removeClass('hidden');
						}
					} else {
						$(this).addClass('hidden');
					}
				}
			});
		}
		
		country_check();
	</script>
