	<div class="container">
		<div class="row">
			<div class="span6 offset3 header-container">
				<h3 style="text-align: center; margin-top: 30px;">Tu contraseña ya ha sido reestablecida!</h3>
			</div>
		</div>
	</div>	
		
	<div class="container">
		
		<div class="row">
			<div class="span6 offset3 white-container round-bottom-10">
				<div class="padding-20">
					<p class="text-center big padding-10">
						Ooops! Este enlace no existe porque...
						<br><br>
						- Ya lo has usado para recuperar tu contraseña.
						<br><br>
						- Han pasado demasiados días sin ser usado y ha sido
						  automáticamente borrado.
						<br><br>
						Si aún no has recuperado tu usuario/contraseña, vuelve a hacer
						los pasos para recuperarlos.
					</p>
					
					<p class="text-center big">
						<a class="btn btn-main btn-large input-xlarge" href="<?php echo site_url('mipagina')?>">Ir a Mi Pagina</a>
					</p>
				</div>
			</div>
		</div>
	</div>
	
	