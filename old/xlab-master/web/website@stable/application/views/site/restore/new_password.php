	<div class="container">
		<div class="row">
			<div class="span6 offset3 header-container">
				<h3 style="text-align: center; margin-top: 30px;">Nueva Contraseña</h3>
			</div>
		</div>
	</div>	
		
	<div class="container">
		<div class="row">
			<div class="span6 offset3 white-container round-bottom-10">
				<form class="form-horizontal form-login" method="post" action="">		
					<div class="control-group">
						<h1 class="text-center">
							<?php echo $user; ?>
						</h1>

						<h4 class="text-center padding-10">
							Tu página de cambios debe tener una contraseña para
							que sólo tu la puedas modificar.<br><br>
							Ponle una nueva a continuación (usando letras, números ó guiones). Debe tener como mínimo 5 caracteres<br><br>
							No la compartas con nadie!!!
						</h4>
					</div>

					<div class="control-group">
						<label class="control-label" for="password1">Nueva contraseña</label>
						<div class="controls">
							<input 
								type="text" style="width: 240px;" id="password1" name="password1" value=""
								autocomplete="off" 
								autocorrect="off" 
								autocapitalize="off" 
								spellcheck="false">
							<?php echo form_error('password1', '<span class="label label-error">Atención</span><span class="help-inline">', '</span>'); ?>
						</div>			
					</div>

					<div class="control-group">
						<label class="control-label" for="password2">Repite tu nueva contraseña</label>
						<div class="controls">
							<input 
								type="text" style="width: 240px;" id="password2" name="password2" value=""
								autocomplete="off" 
								autocorrect="off" 
								autocapitalize="off" 
								spellcheck="false">
							<?php echo form_error('password2', '<span class="label label-error">Atención</span><span class="help-inline">', '</span>'); ?>
						</div>			
					</div>

					<div class="control-group">
						<div class="controls">
							<?php echo $error; ?>
							<button id="aplicar" type="submit" name="send" value="1" class="btn input-xlarge btn-main" style="width: 255px;">Registrar contraseña</button>
						</div>
					</div>
				</form>
			</div>
		</div>
	</div>
	
	