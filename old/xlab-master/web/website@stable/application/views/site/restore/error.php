	<div class="container">
		<div class="row">
			<div class="span6 offset3 header-container">
				<h3 class="text-center margin-top-50">
					Recupera tu usuario y contraseña
				</h3>
			</div>
		</div>
	</div>	
		
	<div class="container">
		<div class="row">
			<div class="span6 offset3 white-container round-bottom-10">
				<div class="padding-30">
					<p class="medium text-center">
						<?php echo $message; ?>
					</p>
					<br>
					<p class="text-center big">
						<a class="btn btn-main input-xlarge" href="<?php echo site_url('')?>">
							Volver a la portada
						</a>
					</p>
				</div>
			</div>
		</div>
	</div>
