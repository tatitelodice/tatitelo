

<div class="container"> 
    <div class="row">
        <div class="span6 offset3 section-panel section-panel-light" style="padding: 0.5rem;">
            
            <?php if ($is_published): ?>
                <p class="text-center">
                    Sigue los pasos a continuación para extender tu publicación:
                </p>
            <?php else: ?>
                <p class="text-center">
                    Hola! tener tu página no tiene costo pero no te servirá de mucho si no la estamos promocionando.
                </p>

                <p class="text-center" style="font-weight: bold;">
                    Ganá clientes, activá tu publicación y promocionate en la portada de Pumbate,
                    es bien sencillo:
                </p>
            <?php endif; ?>

            <table class="table table-condensed table-visits" style="background: white;">
                <tbody>
                    <tr>
                        <td style="text-align: center;  font-size: 1.5rem;">
                            <a class="color-violet" style="border-bottom: 0.1rem dotted #9C0075;"
                                href="<?php echo site_url('/profile-panel/publication/activate/' . $user)?>?step=pricing"
                            >
                                ¿Cuánto cuesta?
                            </a>
                        </td>
                    </tr>
                    <tr>
                        <td style="text-align: center;  font-size: 1.5rem;">
                            <a class="color-violet" style="border-bottom: 0.1rem dotted #9C0075;"
                               href="<?php echo site_url('/profile-panel/publication/activate/' . $user)?>?step=payment_method"
                            >
                                ¿Cómo se paga?
                            </a>
                        </td>
                    </tr>
                    <tr>
                        <td style="text-align: center;">
                            <?php 
                                $this->load->view('site/profile-panel/publication/partial_payment_button.php')
                            ?>
                        </td>

                    </tr>
                </tbody>
            </table>

            <?php
                $this->load->view('site/profile-panel/publication/partial_contact');
            ?>

            <hr>
            
            <p class="text-center" style="margin-top: 1.5rem;">
                <a class="color-violet" href="<?php echo site_url('/mipagina')?>">
                    Volver a mi panel
                </a>
            </p>
        </div>
    </div>
</div>