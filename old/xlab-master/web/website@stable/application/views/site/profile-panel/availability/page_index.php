<div class="container"> 
    <div class="row">
        <div class="span6 offset3 section-panel section-panel-light" style="padding: 0.5rem;">
            
            <div class="row-fluid">
            <?php
                $this->load->view('site/profile-panel/availability/form_availability', $profile);
            ?>
            </div>

            <hr>
            
            <p class="text-center" style="margin-top: 1.5rem;">
                <a class="color-violet" href="<?php echo site_url('/mipagina')?>">
                    Cancelar y volver a mi panel
                </a>
            </p>
        </div>
    </div>
</div>