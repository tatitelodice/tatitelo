<?php
	$upper_user = mb_strtoupper($user);
?>

<div class="container header-container">
	<?php
		$top_buttons = '';
		
		if ($verified) {
			$top_buttons .= '<a href="' . site_url('wall') . '" class="btn btn-rose"><i class="glyphicon glyphicon-share"></i>&nbsp;&nbsp;Muro de usuarios</a>&nbsp;';
		}
		
		$top_buttons .= '<a href="' . site_url('profile/logout') . '" class="btn btn-rose"><i class="glyphicon glyphicon-share"></i>&nbsp;&nbsp;Salir del panel</a>';
	?>
	
	<div class="row">
		<div class="hidden-phone" style="padding-top: 40px;">
			<div class="span6 text-left">
				<h3><?php echo $upper_user . ' - ';?> Edita tu página</h3>
			</div>
			
			<div class="span6">
				<div class="text-right" style="padding-top: 15px; padding-bottom: 10px;"><?php echo $top_buttons; ?></div>		
			</div>
		</div>
		
		<div class="display-phone hidden-desktop hidden-tablet">
			<div class="span12 text-center" style="padding-top: 15px; padding-bottom: 10px;"><?php echo $top_buttons; ?></div>
			
			<div class="span12">
				<h4 class="color-white text-center"><?php echo $upper_user . ' - ';?> Edita tu página</h4>
			</div>
		</div>	
		
	</div>
</div>
<div class="container white-container panel-container">
	<div class="row">
		<div class="span12">
			<?php 
				if (isset($old_windows_phone) && $old_windows_phone === true) {
					$this->load->view("site/profile-panel/main/panel_actionbuttons");
				}

				if ($verified == PROFILE_STATUS_HIDDEN && $admin_login) {
					$forbidden_text = "Estas viendo esto porque eres **admin**, el usuario en realidad ve su panel clausurado.";
					echo '<div class="alert alert-pink-noborder"><p class="medium margin-bottom-0 text-center"><b>'.$forbidden_text.'</b></p></div>';
				}
			
				if (!(isset($profile_is_garbage) && $profile_is_garbage)) {
					$this->load->view('site/profile-panel/messages/panel_inbox');
				}
			?>
		</div>

		<div class="span12">

			<ul class="nav nav-tabs panel-nav-tabs" style="margin-bottom: 0px">
				<li id="information-tab" class="active"><a>Acerca de ti</a></li>
				<li class="divider-vertical"></li>
				<li id="themes-tab"><a>Estilo de página</a></li>
				<li class="divider-vertical"></li>
				<li id="gallery-tab"><a>Fotos</a></li>
				<li class="divider-vertical"></li>

				<li id="ad-tab" style="display:none;"><a>Foto de Portada</a></li>
				
				<li class="divider-vertical"></li>
				<li id="publication-tab"><a>Publicación</a></li>
				
				<?php if ($account_type > PROFILE_FREE) : ?>	
					<li class="divider-vertical"></li>
					<li id="visits-tab"><a>Visitas</a></li>
				<?php endif; ?>
			</ul>


			<div id="information-content" class="tab-content">
				<?php 
					$this->load->view('site/profile-panel/main/panel_info');
				?>
			</div>

			<div id="publication-content" class="tab-content">
				<?php 
					$this->load->view('site/profile-panel/publication/panel_tab');
				?>
			</div>	
			
			<?php if ($account_type > PROFILE_FREE) : ?>
				<div id="visits-content" class="tab-content">
					<?php 
						$this->load->view('site/profile-panel/main/panel_visits');
					?>
				</div>
			<?php endif; ?>

			<div id="themes-content" class="tab-content">
				<?php 
					$this->load->view('site/profile-panel/main/panel_themes');
				?>
			</div>

			<div id="gallery-content" class="tab-content">
				<?php
					$this->load->view("site/profile-panel/main/panel_gallery");
				?>
			</div>

			<div id="ad-content" class="tab-content">
				<form class="form-horizontal" method="POST" action="">
					<h4>
					Elige la foto con la que apareces en nuestra página principal de pumbate.com
					</h4>

					<div id="adpics">
						<?php $this->load->view('site/profile-panel/main/panel_adpics', array( 'pictures' => $pictures, 'picture' => $picture)); ?>
					</div>
				</form>
			</div>
			
			<?php 
				if (isset($old_windows_phone) && $old_windows_phone === true) {
					$this->load->view("site/profile-panel/main/panel_actionbuttons");
				}
			?>

		</div>

	</div>
</div>


<?php 
	if (! (isset($old_windows_phone) && $old_windows_phone === true)) {
		$this->load->view("site/profile-panel/main/panel_actionbuttons");
	}
?>

<script src="<?php echo base_url('js/panel.js'); ?>?v=20180708"></script>



	
