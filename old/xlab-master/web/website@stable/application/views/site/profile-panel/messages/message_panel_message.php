<div class="alert alert-panel alert-panel-warning" style="margin-top: 18px; margin-right: 10px; min-height: 48px;">
    <button type="button" class="close" data-dismiss="alert">&times;</button>
    <a class="alert-panel-picture-icon"><img src="<?php echo base_url('img/icons/pumbate_message.png');?>"></a>
    <p class="alert-panel-text alert-panel-text-picture">
        <small class="hidden-phone alert-panel-author">Mensaje del Equipo de Pumbate.com</small>
        <?php echo $panel_message; ?>
    </p>
</div>