<div class="alert alert-panel alert-panel-success" style="margin-top: 18px; margin-right: 10px;">
    <button type="button" class="close" data-dismiss="alert">&times;</button>
    <img class="alert-panel-picture-icon" src="<?php echo base_url('img/icons/done.png');?>">
    <p class="alert-panel-text alert-panel-text-picture">Tu Página Web ha sido actualizada con exito!<br>
    Los clientes que entren a partir de ahora a tu página ya verán la nueva información.
    </p>
</div>