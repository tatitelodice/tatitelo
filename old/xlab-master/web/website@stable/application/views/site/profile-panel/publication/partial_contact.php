<p class="text-center text-small" style="margin-top: 1.5rem">
    El proceso es 100% automático y si lo sigues paso a paso aparecerá
    tu publicación en portada.
</p>

<?php if (false && $pumbate_phone): ?>
<p class="text-center">
    Por sms al:<br>
    <a class="color-violet" href="sms:<?php echo $pumbate_phone; ?>">
        <?php echo $pumbate_phone; ?>
    </a>
</p>
<?php endif; ?>

<p class="text-center text-small">
    Por cualquier inconveniente puedes enviarnos un mail a:
    <br>
    <?php 
        echo safe_mailto('rrpp@pumbate.com', 'rrpp@pumbate.com', 'class="color-violet"'); 
    ?>
</p>
