<form 
	id="fileupload" 
	action="<?php echo current_url(); ?>" 
	data-component="fileupload"
	data-fileupload-url="<?php echo site_url('profile/update_gallery/'.$user); ?>"
	method="POST" 
	enctype="multipart/form-data"
>
	<input type="hidden" name="action" value="gallery"> 
	<fieldset>
		<!-- <legend>Tus fotos</legend> -->
		<!-- Redirect browsers with JavaScript disabled to the origin page -->
		<noscript><input type="hidden" name="redirect" value="<?php echo current_url(); ?>"></noscript>
		
		<?php if (isset($old_windows_phone) && $old_windows_phone === true): ?>
			<?php
				$this->load->view('site/profile-panel/main/panel_gallery_oldbrowser_warn');
			?>
		<?php else: ?>
			<!-- The fileupload-buttonbar contains buttons to add/delete files and start/cancel the upload -->
			<div class="row-fluid fileupload-buttonbar">
				<div class="span5">
					<!-- The fileinput-button span is used to style the file input field as button -->
					<span class="btn btn-primary fileinput-button">
						<i class="glyphicon glyphicon-plus"></i>
						<span>Subir foto...</span>
						<input type="file" name="files[]" multiple>
					</span>

					<button class="btn btn-success" 
					        type="button" 
					        data-action="navigate-cover-picture-page"
					        style="font-family: 'Quicksand', 'Helvetica Neue', Helvetica, Arial, sans-serif;"
					>

						Elegir Portada
					</button>
				</div>
			</div>
			<!-- The loading indicator is shown during file processing -->
			<div class="fileupload-loading"></div>
		<?php endif; ?>	
			
		<br>
		<!-- The table listing the files available for upload/download -->
		<div data-component="gallery" data-gallery-user="<?php echo $user; ?>">
			<table role="presentation" class="table table-striped"><tbody class="files" data-toggle="modal-gallery" data-target="#modal-gallery"></tbody></table>
		</div>
	</fieldset>
</form>

<p class="text-center bold">
	Has borrado alguna foto por accidente? <a target="_blank" class="color-violet" href="<?php echo site_url("profile/view_deleted_pictures/" . $user) ?>">Ver fotos borradas</a>
</p>


<!-- modal-gallery is the modal dialog used for the image gallery -->
<div id="modal-gallery" class="modal modal-gallery hide fade" data-filter=":odd" tabindex="-1">
	<div class="modal-header">
		<a class="close" data-dismiss="modal">&times;</a>
		<h3 class="modal-title"></h3>
	</div>
	<div class="modal-body"><div class="modal-image"></div></div>
	<div class="modal-footer">
		<a class="btn modal-download" target="_blank">
			<i class="glyphicon glyphicon-download"></i>
			<span>Descargar</span>
		</a>
		<a class="btn btn-success modal-play modal-slideshow" data-slideshow="5000">
			<i class="glyphicon glyphicon-play"></i>
			<span>Pasar galería</span>
		</a>
		<a class="btn btn-info modal-prev">
			<i class="glyphicon glyphicon-arrow-left"></i>
			<span>Anterior</span>
		</a>
		<a class="btn btn-primary modal-next">
			<span>Siguiente</span>
			<i class="glyphicon glyphicon-arrow-right"></i>
		</a>
	</div>
</div>



<div class="alert alert-info alert-uploading">
	<h2 class="text-center">Subiendo...</h2>
</div>

<div class="alert alert-success alert-uploading-success">
	<h2 class="text-center">Foto subida con éxito!</h2>
</div>

<div class="alert alert-danger alert-uploading-error">
	<h2 class="text-center">Error al subir foto</h2>
</div>

<!-- The template to display files available for upload -->
<script id="template-upload" type="text/x-tmpl">
{% for (var i=0, file; file=o.files[i]; i++) { %}
	<tr class="template-upload fade">
		<td class="preview"><span class="fade"></span></td>
		<td class="name"><span>{%=file.name%}</span></td>
		<td class="size"><span>{%=o.formatFileSize(file.size)%}</span></td>
		{% if (file.error) { %}
			<td class="error" colspan="2"><span class="label label-important">Error</span> {%=file.error%}</td>
		{% } else if (o.files.valid && !i) { %}
			<td>
				<div class="progress progress-success progress-striped active" role="progressbar" aria-valuemin="0" aria-valuemax="100" aria-valuenow="0"><div class="bar" style="width:0%;"></div></div>
			</td>
			<td class="start">{% if (!o.options.autoUpload) { %}
				<button class="btn btn-primary">
					<i class="glyphicon glyphicon-upload"></i>
					<span>Start</span>
				</button>
			{% } %}</td>
		{% } else { %}
			<td colspan="2"></td>
		{% } %}
		<td class="cancel">{% if (!i) { %}
			<button class="btn btn-warning">
				<i class="glyphicon glyphicon-ban-circle"></i>
				<span>Cancel</span>
			</button>
		{% } %}</td>
	</tr>
{% } %}
</script>
<!-- The template to display files available for download -->
<script id="template-download" type="text/x-tmpl">
{% for (var i=0, file; file=o.files[i]; i++) { %}
	<tr class="template-download fade">
		{% if (file.error) { %}
			<td></td>
			<td class="name"><span>{%=file.name%}</span></td>
			<td class="size"><span>{%=o.formatFileSize(file.size)%}</span></td>
			<td class="error" colspan="2"><span class="label label-important">Error</span> {%=file.error%}</td>
		{% } else { %}
			<td class="preview">{% if (file.thumbnail_url) { %}
				<a href="{%=file.url%}" title="{%=file.name%}" rel="gallery" download="{%=file.name%}"><img src="{%=file.thumbnail_url%}"></a>
			{% } %}</td>
			<td class="name">
				<a href="{%=file.url%}" title="{%=file.name%}" rel="{%=file.thumbnail_url&&'gallery'%}" download="{%=file.name%}">{%=file.name%}</a>
			</td>
			<td class="size"><span>{%=o.formatFileSize(file.size)%}</span></td>
			<td colspan="2"></td>
		{% } %}
		<td class="delete">
			<button class="btn btn-danger" data-type="{%=file.delete_type%}" data-url="{%=file.delete_url%}"{% if (file.delete_with_credentials) { %} data-xhr-fields='{"withCredentials":true}'{% } %}>
				<i class="glyphicon glyphicon-trash"></i>
				<span>Borrar</span>
			</button>

			<input type="hidden" name="delete" value="1">
		</td>
	</tr>
{% } %}
</script>

<!-- CSS adjustments for browsers with JavaScript disabled -->
<noscript><link rel="stylesheet" href="<?php echo base_url('css/jquery.fileupload-ui-noscript.css'); ?>"></noscript>
