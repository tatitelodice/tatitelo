<p class="big big-phone text-center">
    El celular que estás utilizando tiene Windows Phone y no permite subir fotos :(
</p>
            
<p class="medium text-center">
    - Intenta entrar con cualquier PC, IPhone ó celular con Android.
</p>

<p class="medium text-center">
    - Intenta instalarte y entrar con el App UCBrowser:
      <br>
      <a class="btn btn-rose" href="https://windowsphone.com/s?appid=6cda5651-56b9-48b0-8771-91dbc188f873">
          Instalar UCBrowser
      </a>
</p>

<p class="medium text-center">
    - También puedes enviarnos las fotos por mail a:<br>
    <?php echo safe_mailto('rrpp@pumbate.com', 'rrpp@pumbate.com', 'class="color-violet"'); ?><br>
</p>

<p class="medium text-center">
    - También puedes enviarnos las fotos por whatsapp al:<br>
    <a class="color-violet"><?php echo $pumbate_phone; ?></a><br>
</p>

<p class="medium text-center">
    No olvides comentarnos que son para el perfil <span class="color-violet"><?php echo $user; ?></span>
</p>