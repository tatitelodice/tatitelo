<div class="container">	
	<div class="row">
		<div class="span6 offset3 section-panel section-panel-light">
            <h3 class="section-panel-title">
                Has realizado un pago?
            </h3>

            <p class="text-center">
                Completa los datos a continuación para acreditarlo y publicarte.
            </p>
            <hr>
            <?php
                $this->load->view('site/profile-panel/billing/form_new_deposit_' . $payment_method);
            ?>
		</div>
	</div>
</div>