
<input type="hidden" name="adpic" value="<?php echo $picture; ?>">

<?php for ($p=0, $cp = count($pictures); $p < $cp; $p++): ?>
								
	<?php
		if ($p == 0 || ($p % 6 == 0)) echo '<div class="row-fluid">';
	?>
	<div class="span2">

		<?php
			$thumb_url = preg_replace('!/[^/]+\.[A-Z0-9]+$!i', "/w200/$0", $pictures[$p]['src']);
			$pic_name = preg_replace('!.*/!i', '', $pictures[$p]['src']);
		?>

		<div class="pumbate-adpic-block <?php echo ($picture == $pic_name)? 'pumbate-adpic-block-selected' : ''; ?>">
			<div class="pumbate-adpic-wrap">
				<img class="<?php echo $pictures[$p]['type']; ?>" src="<?php echo $thumb_url; ?>">
			</div>
			<div class="pumbate-adpic-selection">
				<div class="in-use">Foto actual</div>
				<button type="button" class="btn btn-success btn-adpic" value="<?php echo $pic_name; ?>"><b>Usar</b></button>
			</div>
		</div>

	</div>

	<?php 
		if ($p+1 == $cp || ( ($p+1) % 6 == 0)) {
			echo '</div>';
		}
	?>

<?php endfor; ?>


