<?php
	$old_windows_phone_style = "";

	if (isset($old_windows_phone) && $old_windows_phone === true) {
		$old_windows_phone_style = ' style="position: static;"';
	}
?>

<div class="navbar navbar-panel"<?php echo $old_windows_phone_style ?>>
	<div class="navbar-inner">
		<ul class="nav">
			<li>
				<a class="btn btn-main btn-nav-save-changes"><i class="glyphicon glyphicon-ok"></i>&nbsp;&nbsp;Guardar Cambios</a>
			</li>
			<li>
				<a href="<?php echo site_url($user) ?>" target="_blank" class="btn btn-main"><i class="glyphicon glyphicon-eye-open"></i>&nbsp;&nbsp;Ver mi página actual</a>
			</li>
		</ul>
	</div>
</div>
