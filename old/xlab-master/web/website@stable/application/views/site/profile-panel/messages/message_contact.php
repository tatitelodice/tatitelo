<div style="margin-top: 10px; padding-left: 10px; padding-right: 10px;">    
    <p class="medium text-center"><a id="contact_more_btn_phone" href="#" class="color-violet">Necesitas ayuda? Haz click aquí para ver los medios para contactarnos</a></p>
    <div id="contact_more_phone" class="hidden text-center">
        <p class="medium margin-bottom-0">Te podremos ayudar si nos contactas de las siguientes maneras:</p>
        <?php if ($pumbate_phone): ?>
        <p class="medium margin-bottom-0">Enviando sms al <a class="color-violet" href="tel:<?php echo $pumbate_phone; ?>"><?php echo $pumbate_phone; ?></a>.</p>
        <?php endif; ?>
        <!--
        <p class="medium margin-bottom-0">Agregando <a class="color-violet" target="_blank" href="https://www.facebook.com/pumbate.rrpp">facebook.com/pumbate.rrpp</a> y preguntandonos al chat.</p>
        -->
        <p class="medium">Escribiendo un mail a <?php echo safe_mailto('rrpp@pumbate.com', 'rrpp@pumbate.com', 'class="color-violet"'); ?></p>
    </div>
</div>

<script>
    $('#contact_more_btn_phone').click(function(e) {
        e.preventDefault();
        $("#contact_more_phone").removeClass('hidden');
        $(this).parent().addClass('hidden');
    });
</script>