<form class="form-horizontal" method="POST" action="">
                
    <?php if ($absent): ?>          
    <h3 class="text-center">Cambiar Disponibilidad</h3>
    <?php else: ?>
    <h3 class="text-center">Disponibilidad</h3>
    <?php endif; ?>

    <div id="absent_form">

        <p class="text-center">Mensaje de ausencia para tus visitantes:</p>

        <textarea id="absent_message_visible" type="text" style="width: 98%" rows="3" name="absent_message" placeholder="Estaré de viaje, vuelvo para mediadados de mayo :)"><?php echo $absentMessage; ?></textarea>

        <br><br>

        <p class="text-center">
        <?php if ($absent): ?>

            <button type="submit" id="btn-absent-change" class="btn btn-primary">
                Cambiar Mensaje
            </button>

            <button type="submit" id="btn-absent-return" class="btn btn-success" name="absent" value="0">
                Volver a estar Disponible
            </button>

        <?php else: ?>

            <button type="submit" id="btn-absent-away" class="btn btn-danger" name="absent" value="1">
                Cambiar mi estado a No Disponible
            </button>

        <?php endif; ?>
        </p>
    </div>
</form>
