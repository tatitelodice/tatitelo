<div data-ui-role="message-inbox">
<?php 
    if ($verified == PROFILE_STATUS_NOT_VERIFIED)
    {
        if (!$profile_complete && $accountType == PROFILE_FREE)
        {
            $this->load->view('site/profile-panel/messages/message_incomplete_profile');
        }
        else if ($country === 'uy')
        {
            $this->load->view('site/profile-panel/messages/message_activate_profile');
        }
    }
    else if ($publication['is_active'] && $billing['notify_due_date'])
    {
        $this->load->view('site/profile-panel/messages/message_extend_publication');
    }

    if ($panel_message)
    {
        $this->load->view('site/profile-panel/messages/message_panel_message');
    }

    if ($absent)
    {
        $this->load->view('site/profile-panel/messages/message_absent');
    }
?>
</div>

<div data-ui-role="flashmessages">
<?php
    if ($success === true)
    {
        $this->load->view('site/profile-panel/messages/message_update_success');
    }
    /*
    else
    {
        $this->load->view('site/profile-panel/messages/message_contact');
    }
    */
?>
</div>
