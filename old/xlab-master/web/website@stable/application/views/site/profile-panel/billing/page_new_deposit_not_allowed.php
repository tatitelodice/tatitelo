<div class="container"> 
    <div class="row">
        <div class="span6 offset3 section-panel section-panel-light">
            <h3 class="section-panel-title">
                Ya has subido demasiadas notificaciones de pago
            </h3>

            <p style="margin-top: 1.5rem;">
                Has enviado demasiadas notificaciones de pago y el sistema
                no ha podido encontrar ningún pago con esa información.
            </p>

            <p style="margin-top: 1.5rem;">
                Envía la foto del ticket o la información del 
                pago para que revisemos tu caso por mail a:
            </p>

            <p class="text-center" style="margin-top: 1.5rem; font-size: 1.8rem;">
                rrpp@pumbate.com
            </p>


            <p class="text-center" style="margin-top: 1.5rem;">
                <a 
                    class="btn btn-main"
                    href="<?php echo site_url('/mipagina')?>"
                >
                    Volver al Panel
                </a>
            </p>
        </div>
    </div>
</div>