<div class="container"> 
    <div class="row">
        <div class="span6 offset3 section-panel section-panel-light" style="padding: 0.5rem;">
            
            <h3 class="text-center">
                Dar de Baja
            </h3>

            <p>
                Recuerda que al bajar tu publicación...
            </p>

            <ul style="font-size: 1.2rem; line-height: 1.4rem;">
                <li style="margin: 0.8rem 0">
                    Dejarás de aparecer en la portada del sitio
                </li>
                <li style="margin: 0.8rem 0;">
                    Tus fotos e información de contacto como teléfonos dejarán de estar disponibles para los visitantes que hayan compartido tu página
                </li>
                <li style="margin: 0.8rem 0;">
                    Si vuelves deberás volver a pagar la publicación
                </li>
                <li style="margin: 0.8rem 0;">
                    Puedes recuperar tu perfil en cualquier momento en los próximos 8 meses,
                    luego lo borraremos definitivamente.
                </li>
            </ul>

            <form method="POST">
                <p class="text-center" style="margin-top: 1.8rem;">
                    <button type="submit" class="btn btn-large btn-danger" name="confirm" value="1">
                        Sí, quiero darme de baja
                    </button>
                </p>
            </form>

            <hr>
            
            <p class="text-center" style="margin-top: 1.5rem;">
                <a class="color-violet" href="<?php echo site_url('/mipagina')?>">
                    Cancelar y volver a mi panel
                </a>
            </p>
        </div>
    </div>
</div>