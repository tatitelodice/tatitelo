<p class="text-center" style="font-weight: bold;"> 
    Se paga en cualquier red de Cobranza ABITAB, depositando
    el dinero en la siguiente cuenta:
</p>

<p style="background: #ffff9f; text-align: center; font-size: 1.3rem; line-height: 1.6rem; font-weight: bold; margin-top: 1rem;">
    Colectivo ABITAB <?php echo $payment_account['accountId'] ?> (<?php echo $payment_account['accountAlias'] ?>)
</p>

<table class="table table-condensed table-visits" style="background: white;">
    <tbody>
        <tr>
            <td style="text-align: center;">
                Da <strong>tu celular de trabajo</strong> o el nombre <strong><?php echo $user; ?></strong> como referencia para identificar mejor tu pago
            </td>
        </tr>
        <tr>
            <td style="text-align: center;">
                <strong>Saca foto del comprobante</strong>, si no recibimos foto o nombre exacto del depositante no activamos publicaciones.
            </td>
        </tr>
    </tbody>
</table>