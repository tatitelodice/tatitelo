 <div 
    class="abitab-message-box text-center" 
>
    Se paga depositando en:

    <p style="background: #ffff9f; text-align: center; font-size: 1.3rem; line-height: 1.6rem; font-weight: bold; margin-top: 1rem;">
        Colectivo <?php echo $colectivo_abitab ?>
    </p>

    A nombre de tu celular o nombre de trabajo para identificarte
</div>

<p class="text-center" style="margin-top: 0.5rem">
    <a 
        class="btn btn-main btn-large"
        href="<?php echo site_url('/profile-panel/billing/new_deposit/' . $user)?>"
    >
        Depositaste?<br>Registra tu pago acá
    </a>
</p>