<div class="container header-container">
	<div class="row">
		<div style="padding-top: 40px;">
			<div class="span8">
				<h3>
					<?php echo $name . ' - ';?> Administrar Mi página
				</h3>
			</div>
			<div class="span4" style="text-align: right;">
				<img src="<?php echo base_url('img/logo-complete3.png');?>">
			</div>
		</div>
	</div>
</div>

<div class="container glass-container-dark">

	<div class="row">
		<div class="span12 text-center">
			<div style="padding: 30px 30px 10px 30px;">
				<h3 class="color-white">
					Perfil dado de baja...
				</h3>
				<img alt=":(" src="<?php echo base_url('img/emote/sad_small.png');?>">	

				<?php if ($panel_message): ?>
				<div class="alert alert-panel-warning">
					<?php echo $panel_message ?>
				</div>
				<?php endif; ?>
				
				<?php if ($country === 'uy') : ?>
				<div class="text-center alert">
			        <a 
			            class="btn btn-main btn-large"
			            href="<?php echo site_url('/profile-panel/publication/reactivate/' . $user)?>"
			        >
			            Quiero reactivar este Perfil
			        </a>
			    </div>
				<?php endif; ?>
				
				<hr>

				<p class="big color-white text-justify">
					Todos los perfiles nuevos que intentes registrar tú o anunciantes que trabajen en el mismo lugar no serán activados hasta que
					contacten a nuestro equipo.
				</p>
				
				<hr>
				
				<p class="big color-white text-justify">
					Nuestro equipo puede demorar hasta 24 horas en reactivarte el perfil nuevamente una
					vez que te has puesto en contacto.
				</p>

			</div>
		</div>
		
	</div>
</div>


<div class="navbar navbar-panel">
	<div class="navbar-inner">
		<ul class="nav">
			<li>
				<a href="<?php echo site_url('profile/logout') ?>" class="btn btn-main"><i class="glyphicon glyphicon-share"></i>&nbsp;&nbsp;Salir del panel</a>
			</li>
			<li>
				<a href="<?php echo site_url($user) ?>" target="_blank" class="btn btn-main"><i class="glyphicon glyphicon-eye-open"></i>&nbsp;&nbsp;Ver lo que ven mis clientes</a>
			</li>
		</ul>
	</div>
</div>





	