<div class="alert alert-panel alert-panel-warning" style="margin-top: 18px; margin-right: 10px; min-height: 48px;">
    <button type="button" class="close" data-dismiss="alert">&times;</button>
    <a class="alert-panel-picture-icon"><img src="<?php echo base_url('img/icons/pumbate_message.png');?>"></a>
    <p class="alert-panel-text alert-panel-text-picture">
        <span class="hidden-phone">HOLA! RECUERDA QUE TIENES QUE SUBIR FOTOS Y RELLENAR TODOS LOS CAMPOS PARA QUE APROBEMOS TU PERFIL Y ESTE PUBLICADO EN LA PORTADA DEL SITIO!</span>
        <span class="visible-phone">PARA QUE APROBEMOS TU PERFIL TIENES QUE SUBIR FOTOS Y RELLENAR TODOS LOS CAMPOS MAS ABAJO!</span>
    </p>
</div>