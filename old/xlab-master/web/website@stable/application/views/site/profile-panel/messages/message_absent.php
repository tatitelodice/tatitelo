<div class="alert alert-panel alert-panel-warning" style="margin-top: 18px; margin-right: 10px;">
    <button type="button" class="close" data-dismiss="alert">&times;</button>
    <a class="alert-panel-picture-icon"><img src="<?php echo base_url('img/travel.png');?>"></a>
    <p class="alert-panel-text alert-panel-text-picture">
        Apareces en la galería de Pumbate.com como <u>NO DISPONIBLE</u>, puedes entrar a Publicación
        y volver a estar disponible.
    </p>
</div>