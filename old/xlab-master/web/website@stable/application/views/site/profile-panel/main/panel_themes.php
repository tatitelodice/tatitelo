<form class="form-horizontal" method="POST" action="">
	<h4>A continuación podrás cambiar la apariencia de tu página con un solo click,
	remarcado en rosa aparece el diseño que actualmente está usando tu página,
	para cambiarlo, desplazate hasta otro cuadro y dale en <u>usar</u> para
	que instantáneamente tu página web tenga la apariencia que elegiste!<br><br>
	Puedes ver tu página con el nuevo diseño apretando el botón <u>VER MI PÁGINA WEB ACTUAL</u>.
	</h4>

	<hr>

	<input type="hidden" name="action" value="theme">
	<input type="hidden" name="user" value="<?php echo $user; ?>">
	<input type="hidden" name="theme" value="<?php echo $theme; ?>">

	<fieldset>

	<?php for ($t=0, $ct = count($themes); $t < $ct; $t++): ?>

		<?php
			$theme_data = $themes[$t];
			if ($t == 0 || ($t % 4 == 0)) echo '<div class="row-fluid">';
		?>
		<div class="span3">
			<div class="theme-foto <?php echo ($theme_data['dir'] == $theme)? 'theme-foto-selected' : ''; ?>" value="<?php echo $theme_data['dir'] ?>">
				<img src="<?php echo $theme_data['thumbnail']; ?>">
				<div class="theme-name">
					<?php echo $theme_data['name']; ?>
					<div class="in-use">
						(Diseño actual)
					</div>
				</div>
				<div class="theme-options">
					<a class="btn btn-info" href="<?php echo $theme_data['preview']; ?>" target="_blank">Vista previa</a>
					<button type="button" class="btn btn btn-success btn-theme" value="<?php echo $theme_data['dir'] ?>"><b>Usar</b></button>
				</div>
			</div>
		</div>
		<?php 
			if ($t+1 == $ct || ( ($t+1) % 4 == 0)) {
				echo '</div>';
			}
		?>

	<?php endfor; ?>
	</fieldset>
</form>

<script>
	function change_theme(theme) {
		var post_data = {
			theme: theme,
			user: '<?php echo $user; ?>',
			action: 'theme'
		};

		$.post('', post_data);
	}

	$('.btn-theme').click( function(event) {
		var theme = $(this).attr('value');

		change_theme(theme);

		$('.theme-foto-selected').removeClass('theme-foto-selected');
		$(this).parent().parent().addClass('theme-foto-selected');
	});

	$('.theme-foto .btn-info').click(function(event) {
		event.stopPropagation();
	});

	$('.theme-foto').click(function() {
		if (!$(this).hasClass('theme-foto-selected')) {
			var theme = $(this).attr('value');
			change_theme(theme);
			$('.theme-foto-selected').removeClass('theme-foto-selected');
			$(this).addClass('theme-foto-selected');
		}
	});
</script>
