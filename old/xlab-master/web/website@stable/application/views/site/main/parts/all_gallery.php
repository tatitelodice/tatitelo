<div id="gallery" class="container white-container">
	<div class="row-fluid">
		<div class="span12 gallery-header">
			<h3>Galería de Escorts ¡Seguí mirando abajo!</h3>
		</div>
	</div>

	<div class="row-fluid">
		<div class="gender-select-row">
			<?php
				$seo_url = 'sexo/' . strtolower($country); 

				$couples = false;
				$tabspan = '4';

				if ($country_iso == 'uy') {
					$couples = true;
					$tabspan = '3';
				}

			?>
			<div class="span<?php echo $tabspan ?>">
				<a class="gallery-tab <?php echo ($escorts_gender != 'mujer')? 'gallery-tab-disabled' : ''; ?>" href="<?php echo site_url($seo_url . '/mujeres'); ?>#gallery-start">
					 <?php echo ($escorts_gender != 'mujer')? 'Ver' : ''; ?> Mujeres
				</a>
			</div>

			<div class="span<?php echo $tabspan ?>">
				<a class="gallery-tab gallery-tab-trans <?php echo ($escorts_gender != 'trans')? 'gallery-tab-disabled' : ''; ?>" href="<?php echo site_url($seo_url . '/travestis-y-trans'); ?>#gallery-start">
					 <?php echo ($escorts_gender != 'trans')? 'Ver' : ''; ?> Trans
				</a>
			</div>
			<div class="span<?php echo $tabspan ?>">
				<a class="gallery-tab gallery-tab-men <?php echo ($escorts_gender != 'hombre')? 'gallery-tab-disabled' : ''; ?>" href="<?php echo site_url($seo_url . '/hombres'); ?>#gallery-start">
					 <?php echo ($escorts_gender != 'hombre')? 'Ver' : ''; ?> Hombres
				</a>
			</div>

			<?php if ($couples): ?>
				<div class="span<?php echo $tabspan ?>">
					<a class="gallery-tab gallery-tab-men <?php echo ($escorts_gender != 'duo')? 'gallery-tab-disabled' : ''; ?>" href="<?php echo site_url($seo_url . '/duos-y-trios'); ?>#gallery-start">
						 <?php echo ($escorts_gender != 'duo')? 'Ver' : ''; ?> Dúos
					</a>
				</div>
			<?php endif; ?>
		</div>
	</div>

	<div class="row-fluid row-gallery-head">
		<div class="span8 location-filter-box">
		<?php
			$this->load->view('site/main/parts/gallery_search');
		?>
		</div>
		<div class="span4">
		<?php
			$this->load->view('site/main/parts/gallery_pics_legend');
		?>
		</div>
	</div>

	<div class="row" id="gallery-start">
		<?php if (!empty($banners)): ?>
		<div class="span12">
			<?php
				$this->load->view('site/main/parts/all_gallery_superstar');
			?>
		</div>
		<?php endif; ?>

		<div class="span12">
			<div class="gallery-wrap">
				<?php
					$gallery_base_title = '';
					
					if ($keywords) {
						if ($total_results > 0) {
							$gallery_base_title .= $total_results . ' resultado' . (($total_results == 1)? '' : 's' ) . ' para "' . $keywords . '"';
						} else {
							$gallery_base_title .= 'No hay resultados para "' . $keywords . '"';
						}
					} else {

						if ($escorts_gender == 'hombre') {
							$gallery_base_title .= ' Escorts masculinos ';
						} elseif ($escorts_gender == 'trans') {
							$gallery_base_title .= ' Escorts Trans ';
						} elseif (substr ($escorts_gender, 0, 3) == 'duo') {	
							$gallery_base_title .= ' <strong>Dúos</strong> para hacer realidad tu <strong>fantasía</strong> del <strong>trío</strong>';
						} else {
							$gallery_base_title .= ' Chicas Escort ';
						}

						if ($city || $region) {
							$gallery_base_title .= ' que trabajan ';
						}
					}
					
					$gallery_loc_title = '';
					$gallery_loc_title_full = '';
					
					if ($city) {
						$gallery_loc_title .= ' en ' . $city . ', ' . $region;
						$gallery_loc_title_full = $gallery_loc_title;
					} elseif ($region) {
						$gallery_loc_title .= ' en ' . $region;
						$gallery_loc_title_full = $gallery_loc_title;
					} else {
						$gallery_loc_title_full .= ' en ' . $country;
					}
					
				?>

				<?php if ($keywords): ?>
					<h3 class="gallery-keywords-title"><?php echo $gallery_base_title . $gallery_loc_title_full; ?>
					</h3>
					<?php
						$escorts_basic = array_merge(
							$escorts_top, $escorts_premium, $escorts_basic
						);
						$escorts_top = array();
						$escorts_premium = array();
					?>
				<?php endif; ?>

				<?php 
					$has_escorts_top = count($escorts_top) > 0;
					$has_escorts_premium = count($escorts_premium) > 0;

					if ($has_escorts_top) {
						$this->load->view('site/main/parts/all_gallery_top'); 
					}

					if ($has_escorts_premium) {
						$this->load->view(
							'site/main/parts/all_gallery_premium'
						,	array(
								'gallery_title_location' => $gallery_loc_title
							,	'escorts_gender' => $escorts_gender
							,	'escorts' => $escorts_premium
							)
						);
					}

					$is_partitioned = $has_escorts_top || $has_escorts_premium;

					$escorts_gallery_data = array(
						'escorts' => $escorts_basic
					,	'blocks_by_row' => 8
					,	'is_partitioned_gallery' => $is_partitioned
					,	'gallery_title_base' => ($keywords)? '' : $gallery_base_title
					,	'gallery_title_location' => $gallery_loc_title
					,	'gallery_title_location_full' => $gallery_loc_title_full
					,	'show_gallery_title' => !$keywords
					);

					$this->load->view('site/main/parts/all_gallery_basic', $escorts_gallery_data); 

				?>

				<?php if (isset($archive_url)):  ?>
				<p class="lead text-center" style="font-size: 1.2rem; font-weight: bold;">
					<a href="<?php echo $archive_url ?>">
						Ver Archivo
					</a>
				</p>
				<?php endif; ?>

				<?php if (false && $country_iso == 'uy' && $escorts_gender == "mujer"): ?>
					<div class="row-fluid">
						<h3 class="gallery-normal-title">Buscás más chicas? ¡Probá en CHICASUY.COM!
							<a class="btn btn-large btn-main" href="https://www.chicasuy.com">Ir a ChicasUy.com &gt;&gt;&gt;</a>
						</h3>
					</div>
				<?php endif; ?>	
			</div>
		</div>	
	</div>
</div>
