<?php 
    $banner_visibility_classmap = array(
        'mobile' => 'hidden-phone', 'tablet' => 'hidden-tablet'
    );
?>

<div class="banners-content">

<?php foreach ($banners as $banner): ?>
    <?php
        $banner_css_class = array('span3', 'block-banner');

        if (!empty($banner['settings']['device_visibility'])) {
            foreach ($banner_visibility_classmap as $k_vis => $v_vis) {
                if (!in_array($k_vis, $banner['settings']['device_visibility'])) {
                    $banner_css_class[] = $v_vis;
                }
            }
        }

        $banner_css_attr = implode(' ', $banner_css_class);

        $banner_target = (!empty($banner['settings']['open_in']))? 'target="' . $banner['settings']['open_in'] . '"' : '';
    ?>

    <div class="<?php echo $banner_css_attr; ?>">
        <a <?php echo $banner_target; ?> title="<?php echo $banner["title"]; ?>" href="<?php echo $banner['url']; ?>" class="block-banner-img-link">
            <?php if (!empty($banner['is_escort'])): ?>
                <img alt="<?php echo $banner['description']; ?>" class="hidden-phone hidden-tablet" src="<?php echo $banner["settings"]["main_picture"] ?>">
                <img alt="<?php echo $banner['description']; ?>" class="visible-phone visible-tablet" src="<?php echo str_replace('banner-md', 'banner-xs', $banner["settings"]["main_picture"]) ?>">
            <?php else: ?>
            <img alt="<?php echo $banner['description']; ?>" class="full" src="<?php echo $banner["settings"]["main_picture"] ?>">
            <?php endif; ?>
        </a>
        <a class="block-banner-title">
            <?php echo ucwords(mb_strtolower($banner["title"])); ?>
        </a>
        
        <?php if (!empty($banner['verified_pictures'])): ?>
            <div class="featured-star"></div>
        <?php endif; ?>
    </div>

<?php endforeach; ?>

</div>