<?php
	if (!isset($blocks_by_row)) {
		$blocks_by_row = 8;
	}
		
	$gallery_title_more = (!empty($is_partitioned_gallery))? 'Más ' : '';
?>

<div class="gallery-normal-content">

	<?php if ($show_gallery_title): ?>
	<h3 class="gallery-normal-title">
		<?php
			echo $gallery_title_more . $gallery_title_base . $gallery_title_location_full;
		?>
	</h3>
	<?php endif; ?>
		
	<div class="row-escort">

<?php for ($e = 0, $ec = count($escorts); $e < $ec; $e++) : ?>

	<?php
		$escort = $escorts[$e];
		
		$span_escort_classes = "";

		/*
		if (stripos($escort['name'], 'gratis')) {
			$span_escort_classes .= ' hidden-phone';
		}
		*/
		
		if ($escort['account_type'] > PROFILE_BASIC && !$escort['absent'] ) {
			$span_escort_classes .= " premium-tooltip";
		}
		
	?>

	<div class="span-escort<?php echo $span_escort_classes; ?>" position="<?php echo ($e + 1); ?>">

		<?php
			$thumb_url = preg_replace('!/[^/]+\.[A-Z0-9]+$!i', "/w200/$0", $escort['picture']);
			$target = (preg_match('!#!', $escort['site']))? '_self' : '_blank';
			
			$absent_class = '';
			if ($escort['absent']) $absent_class = 'pumbate-adpic-block-absent';
				
			$featured_class = '';
			
			if ($escort["account_type"] > PROFILE_BASIC ) {
				$featured_class = 'pumbate-adpic-block-featured';
				echo '<span class="hide about">' . $escort['about'] . '</span>';
				echo '<span class="hide user">' . $escort['user'] . '</span>';
				echo '<span class="hide phone">' . $escort['phone'] . '</span>';
				
				if (!$escort['absent']) {
					echo '<div class="hide thumbs">';
						
					foreach ($escort['thumbs'] as $th) {
						echo '<img src="' .$th . '">';
					}
					
					echo '</div>';
				}
				
			}
			
		?>
		
		
		<div class="pumbate-adpic-block pumbate-adpic-block-main <?php echo $absent_class . ' ' . $featured_class ?>">
			<div class="pumbate-adpic-wrap pumbate-adpic-wrap-main">
				
				<?php 
					if ($escort['account_type'] > PROFILE_FREE && $escort['verified_pictures']) {
						echo '<div class="featured-star"></div>';
					}
					
					$picture_classes = "vertical";
										
					if ($escort['account_type'] < PROFILE_PREMIUM || $escort['absent'] ) {
						$picture_classes .= " simple-tooltip";	
					} else {
						$picture_classes .= " alt-tooltip";	
					}
				?>
				
				<a target="<?php echo $target; ?>" href="<?php echo $escort['site']; ?>">
					<img class="<?php echo $picture_classes ?>" title="<?php echo $escort['about']; ?>" alt="Foto de perfil de <?php echo $escort['name']; ?>" src="<?php echo $escort['picture'] ?>">
				</a>
				
				<?php if ($escort['absent']): ?>
					<span class="label label-absent">Estaré de viaje</span>
				<?php endif; ?>
			</div>
			<div class="pumbate-adpic-name">
				<a target="<?php echo $target; ?>" href="<?php echo $escort['site']; ?>">
					<?php 
						if (mb_strlen($escort['name']) > 12) {
							$escort['name'] = trim(mb_substr($escort['name'], 0, 12)) . '...';
						}
					
						echo $escort['name']; 
					?>
				</a>
			</div>
		</div>

	</div>

    <?php if ($e > 1 && ( ($e + 1) % $blocks_by_row == 0)): ?>
        </div><div class="row-escort">
    <?php endif; ?>

<?php endfor; ?>

</div><!-- end row-escort -->

</div><!-- end container -->