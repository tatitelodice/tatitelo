<?php
    if (!isset($blocks_by_row)) {
        $blocks_by_row = 8;
    }

    $premium_titles = array(
        'muj' => 'Chicas Destacadas',
        'hom' => 'Chicos Destacados',
        'tra' => 'Chicas Trans Destacadas',
        'duo' => 'Dúos Destacados',
    );
?>

<div class="gallery-premium-content">       
    <div class="gallery-premium-title">
        <i class="glyphicon glyphicon-star"></i>
        <?php echo $premium_titles[substr ($escorts_gender, 0, 3)] . ' ' . $gallery_title_location; ?>
        <i class="glyphicon glyphicon-star"></i>
    </div>

    <div class="row-escort">

    <?php for ($e = 0, $ec = count($escorts); $e < $ec; $e++) : ?>

        <?php
            $escort = $escorts[$e];
            
            $span_escort_classes = "";
            $absent_class = '';

            if (!$escort['absent'] ) {
                $span_escort_classes .= " premium-tooltip";
            }
            else
            {
                $absent_class = 'pumbate-adpic-block-absent';
            }

            $thumb_url = preg_replace('!/[^/]+\.[A-Z0-9]+$!i', "/w200/$0", $escort['picture']);
            $target = (preg_match('!#!', $escort['site']))? '_self' : '_blank';
        ?>

        <div class="span-escort premium-tooltip" position="<?php echo ($e + 1); ?>">
            <?php
                $this->load->view(
                    'site/main/parts/escort_img_tooltip', $escort
                );
            ?>
            
            <div class="pumbate-adpic-block pumbate-adpic-block-main pumbate-adpic-block-featured <?php echo $absent_class; ?>">
                <div class="pumbate-adpic-wrap pumbate-adpic-wrap-main">
                    
                    <?php 
                        if ($escort['verified_pictures']) {
                            echo '<div class="featured-star"></div>';
                        }
                        
                        $picture_classes = "vertical";
                        
                        if ( $escort['absent'] ) {
                            $picture_classes .= " simple-tooltip";  
                        } else {
                            $picture_classes .= " alt-tooltip"; 
                        }
                    ?>
                    
                    <a target="<?php echo $target; ?>" href="<?php echo $escort['site']; ?>">
                        <img class="<?php echo $picture_classes ?>" title="<?php echo $escort['about']; ?>" alt="Foto de perfil de <?php echo $escort['name']; ?>" src="<?php echo $escort['picture'] ?>">
                    </a>
                    
                    <?php if ($escort['absent']): ?>
                        <span class="label label-absent">Estaré de viaje</span>
                    <?php endif; ?>
                </div>

                <div class="pumbate-adpic-name">
                    <a target="<?php echo $target; ?>" href="<?php echo $escort['site']; ?>">
                        <?php 
                            if (mb_strlen($escort['name']) > 12) {
                                $escort['name'] = trim(mb_substr($escort['name'], 0, 12)) . '...';
                            }
                        
                            echo $escort['name']; 
                        ?>
                    </a>
                </div>
            </div>
        </div>
        
        <?php if ($e > 1 && ( ($e + 1) % $blocks_by_row == 0)): ?>
            </div><div class="row-escort">
        <?php endif; ?>

    <?php endfor; ?>

    </div><!-- end row-escort -->

</div><!-- end gallery-premium-content -->