
<div class="container header-container">
	<div class="row">
		<div class="span5">
			<h1 class="logo">
				<a title="Pumbate Escorts <?php echo $country; ?>" href="<?php echo site_url(''); ?>"><img title="Pumbate Escorts <?php echo $country; ?>" alt="Pumbate Escorts <?php echo $country; ?>" src="<?php echo base_url('img/logo-complete2.png'); ?>"></a>
			</h1>
			<h2 class="logo">
				Tu guía de <strong>sexo y placer VIP</strong> en <strong><u><?php echo $country; ?></u></strong>.
			</h2>
		</div>
		<div id="main-nav" class="span7">
			<h3>
				<a href="<?php echo site_url(); ?>"><strong>Volver a Portada</strong></a>
			</h3>
		</div>
	</div>
</div>

<div class="container">
	<div class="row">
		<div class="span12">
			<div class="well toc-well textcontent">
				
				<?php if ($limit_error) : ?>
					<h2>No podemos suscribirte en este momento</h2>
					<hr>
					<h2 class="text-center">
						Por motivos de seguridad no permitimos suscribir más de cierta cantidad de correos por hoy. Inténtalo en otro momento por favor.
						<br><br>
						<a class="btn btn-large" href="<?php echo site_url(''); ?>">Volver a portada de Pumbate Escorts</a>
					</h2>
				
				<?php elseif ($success): ?>
					<h2>Listo! Te has suscrito con éxito a las novedades de Pumbate</h2>
					<hr>
					<h2 class="text-center">
						Desde ahora en adelante recibirás semanalmente en <?php echo $email ?> las novedades de Pumbate Escorts.<br>
						No dejes de estar al tanto de las mismas!
						<br><br>
						<a class="btn btn-large" href="<?php echo site_url(''); ?>">Volver a portada de Pumbate Escorts!</a>
					</h2>
				
				<?php else: ?>
					<h2>En Pumbate.com queremos que no te pierdas de nada!.</h2>
					<hr>
					<p>Con sólo escribir tu dirección de E-mail a continuación podrás recibir semanalmente
					en la misma todas las novedades de Pumbate.com, incluyendo las nuevas incorporaciones
					a nuestras galerías de escorts.</p>


					<?php if ($maintenance) : ?>
					
						<div class="alert alert-panel-warning text-justify">
							Estamos haciendo mantenimiento al sitio para mejorar su funcionamiento por lo
							que debemos pedirte que vuelvas en unas horas para encontrar en este mismo
							lugar el formulario para recibir en tu correo nuestras novedades!
						</div>
					
					<?php else: ?>
					
						<form class="form-inline form-suscribe" method="post" action="<?php echo site_url('suscribe') ?>">
							<p class="text-center">
								Tu E-Mail Pirata:&nbsp;&nbsp;<input name="email" type="text" class="input-xlarge" placeholder="tu_email@ejemplo.com" value="<?php echo set_value('email', $email); ?>">
								<input type="hidden" name="country" value="<?php echo $country_iso; ?>">
								<button type="submit" class="btn">Empezar a recibir las novedades</button>
								<?php echo form_error('email', '<br><br><span class="label label-error label-inline"><i class="glyphicon glyphicon-warning-sign"></i></span><span class="help-inline help-inline-white">', '</span>'); ?>
							</p>
						</form>
					
					<?php endif; ?>
					
				<?php endif; ?>
			</div>
		</div>
	</div>
</div>