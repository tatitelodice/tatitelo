<div id="advertise" class="container rose-container advertise-container">   
    <div class="row">
        <div class="span12">
            <div class="promo-result">

                <h2 class="text-center">
                    Tu página ha sido creada con éxito!
                </h2>

                <p class="lead text-center" style="margin-top: 2rem;">
                    <strong>
                        Para verla publicada, sube más fotos y ACTIVALA entrando a la sección <a target="_self" href="#ubicacion-mipagina" class=" nobreak" style="color: yellow;">"MI PÁGINA"</a> ubicada arriba del todo cuando entras al sitio:
                    </strong>
                </p>

                <div
                    id="ubicacion-mipagina" 
                    class="text-center" 
                    style="margin-top: 2rem; margin-bottom: 2rem;"
                >
                    <span class="visible-desktop">
                        <img src="<?php echo base_url('img/info/mipagina.png')?>">
                    </span>

                    <span class="hidden-desktop">
                        <img src="<?php echo base_url('img/info/mipagina_mobile.png')?>">
                    </span>
                </div>
                
                <div class="row">
                    <div class="span6 offset3">
                        <a  style="width: 100%; box-sizing: border-box;"
                            class="btn btn-main btn-large" 
                            target="_blank" 
                            href="<?php echo site_url('')?>"
                        >
                            Continuar
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>