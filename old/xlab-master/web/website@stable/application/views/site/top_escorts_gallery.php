
<?php
	if (!isset($blocks_by_row)) {
		$blocks_by_row = 8;
	}
?>

<div class="gallery-top-content">
	
	<div class="gallery-top-title">
		<i class="glyphicon glyphicon-star"></i>&nbsp;<i class="glyphicon glyphicon-star"></i>&nbsp;
		<?php
			$top_titles = array(
				'muj' => '¡No te pierdas las Chicas TOP!',
				'hom' => '¡No te pierdas los Chicos TOP!',
				'tra' => '¡No te pierdas las Trans TOP!',
				'duo' => 'No te pierdas los Dúos TOP',
			);
		
			echo $top_titles[substr ($escorts_gender, 0, 3)];
		?>
		&nbsp;<i class="glyphicon glyphicon-star"></i>&nbsp;<i class="glyphicon glyphicon-star"></i>
	</div>
	
	<div class="row-escort row-top-escorts row-top-escorts-first">

		<?php for ($e = 0, $ec = count($top_escorts); $e < $ec; $e++) : ?>

			<?php
				$escort = $top_escorts[$e];

				$span_escort_classes = "";

				if ($escort['account_type'] > PROFILE_BASIC && !$escort['absent'] ) {
					$span_escort_classes .= " premium-tooltip";
				}
			?>

			<div class="span-escort<?php echo $span_escort_classes; ?>" position="<?php echo ($e + 1); ?>">

				<?php
					$thumb_url = preg_replace('!/[^/]+\.[A-Z0-9]+$!i', "/w200/$0", $escort['picture']);
					$target = (preg_match('!#!', $escort['site']))? '_self' : '_blank';

					$absent_class = '';
					if ($escort['absent']) $absent_class = 'pumbate-adpic-block-absent';

					if ($escort["account_type"] > PROFILE_BASIC ) {
						echo '<span class="hide about">' . $escort['about'] . '</span>';
						echo '<span class="hide user">' . $escort['user'] . '</span>';
						echo '<span class="hide phone">' . $escort['phone'] . '</span>';

						if (!$escort['absent']) {
							echo '<div class="hide thumbs">';

							foreach ($escort['thumbs'] as $th) {
								echo '<img src="' .$th . '">';
							}

							echo '</div>';
						}
					}
				?>
				
				<div class="pumbate-adpic-block pumbate-adpic-block-main pumbate-adpic-block-top <?php echo $absent_class ?>">
					<div class="pumbate-adpic-wrap pumbate-adpic-wrap-main">

						<?php 
							if ($escort['account_type'] > PROFILE_FREE && $escort['verified_pictures']) {
								echo '<div class="featured-star"></div>';
							}

							$picture_classes = "vertical";

							if ($escort['account_type'] < PROFILE_PREMIUM || $escort['absent'] ) {
								$picture_classes .= " simple-tooltip";	
							} else {
								$picture_classes .= " alt-tooltip";	
							}
						?>

						<a target="<?php echo $target; ?>" href="<?php echo $escort['site']; ?>">
							<img class="<?php echo $picture_classes ?>" title="<?php echo $escort['about']; ?>" alt="Foto de perfil de <?php echo $escort['name']; ?>" src="<?php echo $escort['picture'] ?>">
						</a>

						<?php if ($escort['absent']): ?>
							<span class="label label-absent">Estaré de viaje</span>
						<?php endif; ?>
					</div>
					<div class="pumbate-adpic-name">
						<a target="<?php echo $target; ?>" href="<?php echo $escort['site']; ?>">
							<?php 
								if (mb_strlen($escort['name']) > 10) {
									$escort['name'] = trim(mb_substr($escort['name'], 0, 9)) . '...';
								}	
								
								echo $escort['name']; 
							?>
						</a>
					</div>
				</div>

			</div>
		
			<?php if ($e > 1 && ( ($e + 1) % $blocks_by_row == 0)): ?>
				</div><div class="row-escort row-top-escorts">
			<?php endif; ?>
			
		<?php endfor; ?>
	</div>
</div>