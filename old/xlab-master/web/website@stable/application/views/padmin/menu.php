<?php
	$admin_name = $this->admin_model->get_name();
	$show_private_admin_sections = in_array($admin_name, ['santiago', 'florenciapence']);
?>

	<div class="span2">
		<div class="visible-phone menu-nav-btn-container">
			<button id="menu-nav-btn" class="btn btn-menu">Menu</button>
		</div>
			
		<ul id="menu-nav" class="nav nav-list hidden-phone">
			<li class="nav-header hidden-phone">
				Menu de Navegación
			</li>
			
			<?php			
				if (!isset($admin_country)) {
					$admin_country = $this->admin_model->get_country();
				}
				
				$super_admin = '' == $admin_country;
				
				$this->db->where('logNotify', 1);
				$this->db->where('logDate <=', date('Y-m-d') . ' 23:59:59');

				if (strlen($admin_country) == 2) {
					$this->db->where('logCountry', $admin_country);
				}

				$notification_count = $this->db->count_all_results('logs');
			?>
			
			<li>
				<a href="<?php echo site_url('padmin/notifications')?>">
					Notificaciones
					<?php if ($notification_count > 0): ?>
					<span class="notification-count label label-rose"><?php echo $notification_count; ?></span>
					<?php endif; ?>
				</a>
			</li>
			
			<li>
				<a href="<?php echo site_url('padmin/profiles')?>">Perfiles</a>
			</li>
						
			<li>
				<a href="<?php echo site_url('padmin/payments')?>">Pagos</a>
			</li>

			<li>
				<a href="<?php echo site_url('padmin/pricing')?>">Precios</a>
			</li>

			<?php if ($show_private_admin_sections): ?>
			<li>
				<a href="<?php echo site_url('padmin/payment_accounts')?>">
					Cuentas de Pago
				</a>
			</li>

			<li>
				<a href="<?php echo site_url('padmin/deposits/list_deposits')?>?realTotal=T">
					Depositos
				</a>
			</li>

			<li>
				<a href="<?php echo site_url('padmin/messages')?>">
					Mensajes
				</a>
			</li>

			<li>
				<a href="<?php echo site_url('padmin/profiles_marketing')?>">
					Perfiles nunca pagos
				</a>
			</li>
			<?php endif; ?>

			<li>
				<a href="<?php echo site_url('padmin/profiles_groups')?>">Grupos</a>
			</li>
			
			<li>
				<a href="<?php echo site_url('padmin/profiles_related')?>">Perfiles relacionados</a>
			</li>
			
			<li>
				<a href="<?php echo site_url('padmin/profiles_import')?>">Importar perfiles</a>
			</li>
			
			<li>
				<a href="<?php echo site_url('padmin/notes')?>">Notas</a>
			</li>
			
			<?php if ($super_admin): ?>
			<li>
				<a href="<?php echo site_url('padmin/routes')?>">Rutas</a>
			</li>
			<?php endif; ?>
			
			<!-- deprecated -->
			<!--
			<li>
				<a href="<?php echo site_url('padmin/ads')?>">Anuncios</a>
			</li>
			-->
			
			<li>
				<a href="<?php echo site_url('padmin/blacklist')?>">Lista negra</a>
			</li>
			
			<li>
				<a href="<?php echo site_url('padmin/contents')?>">Contenidos</a>
			</li>
			
			<li>
				<a href="<?php echo site_url('padmin/banners')?>">Banners</a>
			</li>
			
			<li>
				<a target="_blank" href="<?php echo site_url('wall')?>">Muro de mensajes</a>
			</li>
			
			<li>
				<a href="<?php echo site_url('padmin/logs')?>">Logs</a>
			</li>
			
		</ul>
		
		<div class="visible-phone nav-phone-separator"><hr></div>
		
	</div>
