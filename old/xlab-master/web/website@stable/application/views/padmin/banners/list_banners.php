<div class="row">

	<?php
		$this->load->view('padmin/menu');
	?>

	<div class="span10">
		
		<?php if (array_filter(array($success_message, $error_message))): ?>
		<div class="row-fluid">
			<div class="span12">
				<?php $this->load->view('padmin/partials/status_messages'); ?>
			</div>
		</div>
		<?php endif; ?>
		
		<div class="row-fluid" style="margin-bottom: 15px;">	
			<div class="span8">
				<h4 class="text-left" style="padding-top: 4px;">
					<b><?php echo $total_list_elements; ?> banner<?php echo ($total_list_elements > 1)? "s" : "" ?></b>
				</h4>
			</div>
			<div class="span4">
				<p class="text-right">
					<a class="btn" style="margin-top: 4px;" href="<?php echo site_url("padmin/banners/create_banner");?>">
						<span class="glyphicon glyphicon-plus"></span>&nbsp;Crear banner
					</a>
				</p>
			</div>
		</div>	
		
		<?php if (count($list_elements)): ?>
			<table class="table table-condensed">
				<thead>
					<tr>
						<?php
							$thead = array( 'Titulo', 'Pais', 'Categoría', 'Activo', 'Visible en', 'Posición' );
							
							foreach ($thead as $i => $th) {
								
								if ($i > 2) {
									echo '<th class="text-center">' . $th . "</th>";
								} else {
									echo "<th>" . $th . "</th>";
								}
							}
						?>
					</tr>
				</thead>
				
				<tbody>
					<?php foreach ($list_elements as $element): ?>
						<tr>
							<td>
								<a href="<?php echo site_url("padmin/banners/edit_banner/" . $element["id"]); ?>">
									<b><?php echo $element["title"]; ?></b>
								</a>
							</td>
							
							<td>
								<?php
									$flag_url = base_url('img/flags/'.$element["country"].'_24.png');
									echo '<img class="flag16" title="'.$element["country"].'" alt="'.$element["country"].'" src="' . $flag_url .'">';
								?>
							
							</td>
							<td><?php echo $element["category"]; ?></td>
							<td class="text-center">
								<?php 
									echo $banner_enabled_options[$element["enabled"]]; 
								?>
							</td>
							
							<td class="text-center">
								<?php
									if ($element["enabled"] > 0 && is_array($element['settings']['device_visibility'])) {
	
										$dev_vis = array();
										
										foreach ($device_visibility_options as $dvk => $dvv) {
											$dev_vis[] = $device_visibility_options[$dvk];
										}
										
										if ($dev_vis) {
											echo implode(' - ', $dev_vis);
										}
									}
 								?>
							</td>
							
							<td class="text-center"><?php echo $element['position']; ?></td>
						</tr>
					<?php endforeach; ?>
				</tbody>
			</table>
		<?php endif; ?>
	</div>
</div>

