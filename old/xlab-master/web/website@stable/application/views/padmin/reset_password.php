<div class="row">
	<?php
		$this->load->view('padmin/menu');
	?>
	
	<div class="span10">
		
		<?php if ($message == 'success'): ?>
			<div class="alert alert-success" style="text-align: center;">
				<button class="close" data-dismiss="alert">×</button>
				La nueva password temporal de la cuenta <?php echo $user; ?> ha sido creada con éxito.
			</div>
		<?php endif; ?>	

		<?php if ($has_restore_code): ?>
			<h3 class="text-center">
				Usuario: <?php echo $user; ?><br>
				Contraseña temporal: <?php echo $restore_code; ?>
			</h3>

			<h4 class="text-center"><?php echo anchor($new_password_url, "Link directo para crear nueva contraseña"); ?></h4>
			<br>
			<h4 class="text-center">Expira el <?php echo $restore_expires; ?></h4>
		<?php else: ?>
			<h4 class="text-center">El usuario <?php echo $user; ?> no tiene una contraseña temporal para recuperar su cuenta.</h4>
		<?php endif ?>
			
		<br>
		
		<form class="text-center" action="<?php echo site_url('padmin/profiles/reset_password/'.$user); ?>" method="POST">
			<input type="hidden" name="reset" value="yes">
			<button type="submit" class="btn btn-large"><b>Crear nueva contraseña temporal</b></button>
		</form>
	</div>
</div>