<?php
	$form_error_start = '<br><span class="label label-important">Atención</span><span class="help-inline">';
	$form_error_end = '</span><br><br>';
?>

<div class="row">

	<?php
		$this->load->view('padmin/menu');
	?>

	<div class="span10">
		<form method="POST">
			<?php
				if (!$admin_country) {
					echo form_dropdown('country', $country_options, set_value('country', $note["country"]), ' class="input-large"');
					echo form_error('country', $form_error_start, $form_error_end);
				}
			?>
			
			<input name="title" style="width: 98%;" type="text" value="<?php echo set_value("title", $note["title"]); ?>" placeholder="Titulo">
			<?php echo form_error('title', $form_error_start, $form_error_end); ?>
				
			<textarea name="content" style="width: 98%;" rows="8" placeholder="Texto de la nota..."><?php echo set_value("content", $note["content"]); ?></textarea>
			<?php echo form_error('content', $form_error_start, $form_error_end); ?>
			
			<div class="row-fluid"></div>

			<div class="row-fluid">
				<div class="span12 text-right">
					<input type="submit" class="btn" value="Guardar Cambios">
				</div>
			</div>
		</form>
	</div>
	
</div>
