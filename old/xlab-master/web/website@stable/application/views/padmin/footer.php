			</div>
			
			<?php if (! (isset($nofooter) && $nofooter) ): ?>
			<footer>
				<div class="container">
					<div class="row">
						<div class="span12">
							<hr>
							<p>&copy; <?php echo date('Y') . ' - ' . $this->config->item('site_author'); ?></p>
							<!-- Página generada el <?php echo date('Y-m-d H:i:s'); ?> -->
						</div>
					</div>
				</div>

			</footer>
			<?php endif; ?>
	</body>
</html>