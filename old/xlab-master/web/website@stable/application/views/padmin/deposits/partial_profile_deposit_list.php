<div data-role="profile-deposits-list">

</div>

<div data-template="profile-deposits-table" style="display: none;">
    <table class="table table-condensed table-striped">
        <thead>
            <tr>
                <th>Fecha</th>
                <th>Cantidad</th>
                <th>Nombre</th>
                <th>Concepto</th>
                <th></th>
            </tr>
        </thead>
        <tbody>
            <tr data-template="profile-deposits-table-row">
                <td>{{deposit_date}}</td>
                <td>{{deposit_amount}}</td>
                <td>{{deposit_name}}</td>
                <td>{{deposit_concept}}</td>
                <td>
                    <a onclick="modal_delete({{deposit_id}})" class="btn btn-mini" data-toggle="modal">
                        <i class="glyphicon glyphicon-remove"></i>
                    </a>
                </td>
            </tr>
        </tbody>
    </table>
</div>

<div data-template="profile-deposits-list-empty" style="display: none;">
    <h4>Historial de depósitos</h4>
    <br>
    <p class="text-center">No hay depósitos hasta la fecha.</p>
</div>

<!-- Modal -->
<div id="confirm-delete-modal" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
        <h3 id="myModalLabel">Confirmar</h3>
    </div>
    <div class="modal-body">
        <p>¿Seguro que quieres borrar este pago del historial?</p>
    </div>
    <div class="modal-footer">
        <button class="btn" data-dismiss="modal" aria-hidden="true">No</button>
        <a id="confirm-delete-link" class="btn btn-primary" href="">Sí, borrarlo</a>
    </div>
</div>

<script>
    function reload_deposits_list()
    {
        var profileDepositsUrl = "<?php echo site_url('padmin/deposits/get_profile_deposits/' . $user ) ?>";

        $.get(profileDepositsUrl, function(data)
        {
            var deposits = data && data.deposits;

            var emptyList = !(Array.isArray(deposits) && deposits.length);
            
            if (emptyList)
            {
                $('[data-role="profile-deposits-list"]').html(
                    $('[data-template="profile-deposits-list-empty"]').html()
                );
                return false;
            }
            else
            {
                var rowTemplate = $('[data-template="profile-deposits-table-row"]')
                    .clone()
                    .removeAttr('data-template')
                    .prop('outerHTML');

                var rowsHtml = "";

                deposits.forEach(function(dep)
                {
                    var rowHtml = rowTemplate;

                    Object.keys(dep).forEach(function(depField)
                    {
                        rowHtml = rowHtml.replace('{{deposit_' + depField + '}}', dep[depField]);
                    });

                    rowsHtml += rowHtml;
                });

                var $depositsTable = $(
                    $('[data-template="profile-deposits-table"]').html()
                );

                $depositsTable.find('tbody').html(rowsHtml);

                $('[data-role="profile-deposits-list"]').html(
                    $depositsTable
                );
            }
            
        });
    }

    function modal_delete(deposit_id)
    {
        $('#confirm-delete-link').attr('href', '<?php echo site_url('padmin/payments/remove_deposit/' . $user); ?>' + '/' + deposit_id);
        $('#confirm-delete-modal').modal('toggle');
    }

    $('#confirm-delete-link').click(function(evt)
    {
        evt.preventDefault();
        var deleteDepositUrl = $('#confirm-delete-link').attr('href');
        $.get(deleteDepositUrl, function(data)
        {
            $('#confirm-delete-modal').modal('toggle');
            reload_deposits_list();
        });
    })

    $(document).ready(function()
    {
        reload_deposits_list();
    });
</script>