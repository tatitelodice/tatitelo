<table class="table table-condensed">
    <thead>
        <tr>
            <th class="text-left">Fecha</th>
            <th class="text-left">Hora</th>
            <th class="text-left" style="white-space: nowrap; padding-left: 1rem;">
                Cuenta
            </th>
            <th class="text-left" style="white-space: nowrap;">
                Identificador
            </th>
            <th class="text-left">Acreditado a</th>
            <th class="text-left">Concepto</th>
            <th class="text-left">Comentarios</th>
            <th class="text-right">Cantidad</th>
        </tr>
    </thead>
    
    <tbody>
        <?php foreach ($deposits as $deposit): ?>
        <tr>
            <td>
                <?php echo $deposit["date"]; ?>
            </td>

            <td>
                <?php 
                    if ($deposit["time"])
                    {
                        $timeDt = DateTime::createFromFormat("H:i:s", $deposit["time"]);
                        echo $timeDt->format("H:i");
                    }
                ?>
            </td>
            
            <td style="white-space: nowrap; padding-left: 1rem; padding-right: 1rem;">
                <?php if (!empty($deposit['paymentAccount'])): ?>
                <?php 
                    echo $deposit["paymentAccount"]['accountId']; 
                ?>
                <?php endif; ?>
            </td>
            
            <td style="white-space: nowrap;">
                <a href="<?php echo site_url("padmin/deposits/view_details/".$deposit["id"]) ?>"
                   style="color:inherit;" 
                >
                    <?php echo $deposit["name"]; ?>
                </a>
            </td>

            <td>
                <?php if ($deposit["user"]): ?>
                    <?php
                        echo anchor(
                            "padmin/payments/view/".$deposit["user"]."?ref=payments/list_deposits"
                            , $deposit["user"]
                        ); 
                    ?>
                <?php else: ?>

                    <?php if ($deposit['status'] === 'deposited'): ?>
                        <button
                            class="btn btn-mini"
                            data-action="apply-deposit"
                            data-deposit-id="<?php echo $deposit['id']; ?>"
                        >
                            Acreditar a...
                        </button>
                    <?php else: ?>
                    <a 
                       href="<?php echo site_url("padmin/deposits/view_details/".$deposit["id"]) ?>"
                       style="color:inherit;" 
                    >
                        no requiere
                    </a>
                    <?php endif; ?>

                <?php endif; ?>
            </td>

            <td><?php echo $deposit["concept"]; ?></td>

            <td><?php echo $deposit["comments"]; ?></td>

            <td class="text-right">
                <?php echo $deposit["amount"]; ?>
            </td>
        </tr>
        <?php endforeach; ?>

        <tr>
            <td data-auto-colspan="-1">
                <h4 class="text-right">Total</h4>
            </td>

            <td class="text-right">
                <h4 class="text-right"><?php echo $totalAmount; ?></h4>
            </td>
        </tr>
    </tbody>
</table>

<script>
    $(document).ready(function()
    {
        $('table').each(function(i, e)
        {
            $(e).find("td[data-auto-colspan]").each(function()
            {
                var colCount = 0;

                $(this).parents("table").find('tr').eq(0).children().each(function(){
                    if ($(this).attr('colspan')){
                        colCount += +$(this).attr('colspan');
                    } else {
                        colCount++;
                    }
                });

                var autoColSpan = parseInt($(this).attr('data-auto-colspan'));

                $(this).attr("colspan", colCount + autoColSpan);
            });
        })
    });
</script>