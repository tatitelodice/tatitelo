<?php
    $inputTagClass = (!isset($inputTagClass)) ? 'input-large' : $inputTagClass;
    $label = (!isset($label)) ? $fieldName : $label;
?>

<div class="control-group">
    <label class="control-label" for="<?php echo $fieldName ?>">
        <?php echo $label ?>
    </label>
    <div class="controls">
        <select 
            name="<?php echo $fieldName ?>"
            class="<?php echo $inputTagClass ?>"     
        >
        <?php foreach ($fieldOptions as $optionLabel => $optionVal): ?>
            <option 
                value="<?php echo $optionVal ?>" 
                <?php echo ($optionVal === $fieldValue) ? ' selected="selected"' : "" ?>
            >
                <?php echo $optionLabel ?>
            </option>
        <?php endforeach ?>
        </select>

        <?php
            echo form_error($fieldName, '<span class="label label-important">Atención</span><span class="help-inline">', '</span>');
        ?>
    </div>			
</div>