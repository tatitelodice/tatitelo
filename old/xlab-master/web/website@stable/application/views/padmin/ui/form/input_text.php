<?php
    $inputTagClass = (!isset($inputTagClass)) ? 'input-large' : $inputTagClass;
    $label = (!isset($label)) ? $fieldName : $label;
?>
<div class="control-group">
    <label class="control-label" for="<?php echo $fieldName ?>">
        <?php echo $label ?>
    </label>
    <div class="controls">
        <input 
            type="text" 
            class="<?php echo $inputTagClass ?>" 
            <?php echo (!empty($disabled))? 'disabled="disabled"' : '' ?> 
            name="<?php echo $fieldName ?>" 
            value="<?php echo $fieldValue ?>"
        ?>
        <?php echo form_error($fieldName, '<span class="label label-important">Atención</span><span class="help-inline">', '</span>'); ?>
    </div>			
</div>