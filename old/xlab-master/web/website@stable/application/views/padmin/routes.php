<div class="row">
	<?php
		$this->load->view('padmin/menu');
	?>
	
	<div class="span10">
		<h4 style="text-align: center;">
			<?php if (isset($message) && $message == 'success_route'): ?>
				<div class="alert alert-success" style="text-align: center;"><button class="close" data-dismiss="alert">×</button>Las rutas han sido actualizadas con éxito.</div>
			<?php endif; ?>
			
			<a class="btn" href="<?php echo site_url('padmin/routes/update'); ?>">Actualizar redirecciones personales (<?php echo $site_domain ?>/usuario)</a>
		</h4>
		
		<hr style="margin-top: 30px; margin-bottom: 30px;">
		
		<h4 style="text-align: center;">
			<?php if (isset($message) && $message == 'success_chmod'): ?>
				<div class="alert alert-success" style="text-align: center;"><button class="close" data-dismiss="alert">×</button>Los permisos han sido actualizadas con éxito.</div>
			<?php elseif (isset($message) && $message == 'error_chmod'): ?>
				<div class="alert alert-error" style="text-align: center;"><button class="close" data-dismiss="alert">×</button>El usuario ingresado debe estar registrado en <?php echo $site_domain ?>.</div>
			<?php endif; ?>
			
			Corrección de permisos de usuario (imágenes guardadas):<br><br>
			<form class="form-inline" method="post" action="<?php echo site_url('padmin/routes/remod') ?>">
				<input name="user" type="text" class="input-medium" placeholder="usuario">
				<button type="submit" class="btn">Corregir</button>
			</form>
		</h4>
	</div>
</div>