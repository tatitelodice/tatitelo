<table class="table table-condensed table-striped table-accounts">
	<thead>
		<tr>
			<?php
				$col_headers = array(
					'' /*country*/, '' /* gender */, 'Nombre', 'Usuario', 'Teléfono Actual', 
					'Registrado', 'Estado', ''/*ausente*/, '&nbsp;&nbsp;&nbsp;Acciones'
				);

				foreach ($col_headers as $col_header) {
					echo "<th>" . $col_header . "</th>";
				}
			?>
		</tr>
	</thead>
	<tbody>
		<?php foreach($profiles_related as $profile): ?>
			<tr>
				<td class="<?php echo $profile['gender']; ?>">
					<?php  
						$flag_url = base_url('img/flags/'.$profile["country"].'_24.png');
						echo '<img class="flag16" title="'.$profile["country"].'" alt="'.$profile["country"].'" src="' . $flag_url .'">';
					?>
				</td>

				<td class="<?php echo $profile['gender']; ?>">
					<strong><?php echo $profile["gender_symbol"]; ?></strong>
				</td>

				<td class="<?php echo $profile['gender']; ?>">
					<div class="text-ellipsis-md">
						<?php echo $profile["name"]; ?>
					</div>
				</td>

				<td>
					<div class="text-ellipsis-md">
						<?php echo anchor( $profile['user'], $profile['user'], 'target="_blank"') ?>
					</div>
				</td>

				<td>
					<div class="text-ellipsis-md">
						<?php echo $profile["phone"]; ?>
					</div>
				</td>

				<td><?php echo $profile["registered"]; ?></td>

				<td>
					<?php if ($profile['deleted']): ?>
						<span class="label label-rose">Eliminada</span>
					<?php else: ?>
						<?php if ($profile['verified'] != PROFILE_STATUS_NOT_VERIFIED): ?>

							<?php if ($profile['verified'] == PROFILE_STATUS_VERIFIED): ?>
								<span class="label label-verificado">Verificada</span>
							<?php elseif ($profile['verified'] == PROFILE_STATUS_HIDDEN): ?>
								<span class="label label-oculto">Oculta</span>
							<?php endif; ?>

							<span class="label label-<?php echo strtolower($profile['account_type']); ?>">
								<?php echo $profile["account_type"]; ?>
							</span>
						<?php else: ?>
							<span class="label label-verificar">Verificar</span>
						<?php endif; ?>
					<?php endif; ?>
				</td>

				<td>
					<?php if ($profile['absent']): ?>
						<span class="label label-rose">Ausente</span>
					<?php endif; ?>
				</td>

				<td class="actions">
					&nbsp;&nbsp;

					<a target="_blank" title="Editar cuenta" href="<?php echo site_url("padmin/profiles/edit/" . $profile["id"]) ?>">
						<i class="glyphicon glyphicon-user"></i>
					</a>

					<a target="_blank" title="Entrar al panel" href="<?php echo site_url($profile['user'] . "/panel") ?>">
						<i class="glyphicon glyphicon-log-in"></i>
					</a>

					<a target="_blank" title="Pagos" href="<?php echo site_url("padmin/payments/view/".$profile['user']) ?>">
						<i class="glyphicon glyphicon-usd"></i>
					</a>

					<a target="_blank" title="Copiapega" href="<?php echo site_url("padmin/adsmaterial/get/".$profile['user']) ?>">
						<i class="glyphicon glyphicon-paperclip"></i>
					</a>
					
					<?php if (false): ?>
					<a target="_blank" title="Eliminar" href="<?php echo site_url("padmin/profiles/delete/".$profile['user']) ?>">
						<i class="glyphicon glyphicon-trash"></i>
					</a>
					<?php endif; ?>
				</td>
			</tr>
		<?php endforeach; ?>
	</tbody>
</table>


<script>
	$('.table-accounts tr').children('td:last-child').addClass('table-accounts-actions').children().css('visibility', 'hidden');

	$('.table-accounts tr').hover(
		function() {
		$(this).children('td:last-child').children().css('visibility', 'visible');
		},

		function() {
			$(this).children('td:last-child').children().css('visibility', 'hidden');
		}
	);
	
</script>