<div class="row">
	<?php
		$this->load->view('padmin/menu');
	?>
	
	<div class="span10">		
		<div class="row-fluid">
			<?php if (count($profiles_related)): ?>
			
				<div class="hidden-phone">
					<?php $this->load->view("padmin/profiles_related/details_list_desktop");?>
				</div>					
				
				<div class="hidden-desktop">
					<?php $this->load->view("padmin/profiles_related/details_list_phone");?>
				</div>
			
			<?php else: ?>
				<h4 class="text-center">
					No se encontraron perfiles relacionados
				</h4>
			<?php endif?>
			
		</div>		
	</div>
	
</div>

