<div class="modal hide fade">
    <div class="modal-header">
		<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
		<h3 class="modal-title">Modal Title</h3>
    </div>
    <div class="modal-body">
		<p>One fine body…</p>
    </div>
    <div class="modal-footer">
		<a href="#" data-dismiss="modal" class="btn modal-btn-dismiss">Cerrar</a>
		<a href="#" class="btn btn-primary modal-btn-accept">Aceptar</a>
    </div>
</div>
