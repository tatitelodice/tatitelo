<?php
    $form_error_start = '<span class="label label-important">Atención</span><span class="help-inline">';
    $form_error_end = '</span>';
?>

<form class="form-horizontal" method="post" action="<?php echo site_url('padmin/messages/write'); ?>">

    <?php if ($message_to_reply): ?>
    <div class="control-group">
        <label class="control-label">
            En respuesta a
        </label>
        <div class="controls">
            <p style="margin-top: 6px;">
                <?php echo $message_to_reply->messageText; ?>
            </p>
            <?php if (!empty($message_to_reply->attributes->profiles) && strlen($message_to_reply->customerAddress) > 8): ?>
                Por: 
                <?php foreach ($message_to_reply->attributes->profiles as $message_to_reply_profile): ?>
                    <a target="_blank" href=<?php echo site_url('padmin/profiles/edit/' . $message_to_reply_profile->id) ?>>
                        <?php echo $message_to_reply_profile->user; ?>
                    </a>
                <?php endforeach; ?>
                <br>
            <?php endif; ?>
            
            Recibido desde <?php echo $message_to_reply->customerAddress; ?> 
            el <?php echo $message_to_reply->createdAt; ?>      
        </div>
    </div>
    <?php endif; ?>


    <div class="control-group">
        <label class="control-label" for="message_category">
            Tipo de mensaje
        </label>
        <div class="controls">
        <?php
            echo form_dropdown('message_category', $message_category_options, set_value('message_category', $default_message_category), ' class="input-large"');
            
            echo form_error('message_category', $form_error_start, $form_error_end);
        ?>
        </div>
    </div>

    <div class="control-group">
        <label class="control-label" for="message_text">Número(s)</label>
        <div class="controls">
            <textarea rows="1" name="message_address" class="input-xlarge"><?php
                echo set_value('message_address', $message_address);
            ?></textarea>
            <?php echo form_error('message_address', $form_error_start, $form_error_end); ?>
        </div>          
    </div>
    
    
    <div class="control-group">
        <label class="control-label" for="message_text">Texto del mensaje</label>
        <div class="controls">
            <textarea name="message_text" style="width: 97%;"><?php echo set_value('message_text'); ?></textarea>

            <?php
                echo form_error('message_text', $form_error_start, $form_error_end);
            ?>
            <p data-role="character-count-summary" style="margin-top: 1rem;">
                El mensaje tiene <span data-role="total-chars"></span>
                caracteres.
            </p> 
        </div>       
    </div>



    <div class="control-group">
        <div class="controls">
            <input type="submit" class="btn btn-default btn-large" value="Enviar">
        </div>
    </div>

</form>

<script>
    AppRequire('padmin/messages').attachCharCounter();
</script>