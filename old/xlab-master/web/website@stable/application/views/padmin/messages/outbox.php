<style>
    .alert-customer-message {
        border: 1px solid #c6c6c6;
        background: transparent;
        color: #414141;
    }

    .alert-customer-message[data-message-status="unread"]
    {
        border: 2px solid #59d178;
    }
</style>


<div class="row">

    <?php
        $this->load->view('padmin/menu');
    ?>
    
    <div class="span10">
        <div class="row-fluid" style="margin-bottom: 1rem;">
            <div class="span6">
                <p style="font-size: 1.2rem; padding: 0.4rem 0; margin-bottom: 0;">
                    <?php echo count($outbox_messages);?> mensajes
                    <?php echo ($status_filter==='pending')? 'pendientes':''; ?>
                </p>
            </div>

            <div class="span6 text-right">
                <a href="<?php echo site_url("padmin/messages/index"); ?>" 
                   class="btn btn-default">
                   Ver Recibidos
                </a>
                <a href="<?php echo site_url("padmin/messages/write"); ?>" 
                   class="btn btn-default">
                   Nuevo Mensaje
                </a>
            </div>
        </div>

        <?php if (count($outbox_messages) == 0) : ?>
            <div class="row-fluid">
                <div class="span12">
                    <h4 class="text-center" style="padding-top: 20px;">No hay mensajes.</h4>
                </div>
            </div>
        
        <?php else: ?>
            <?php foreach ($outbox_messages as $cm): ?>
            <div class="alert-notification-row row-fluid">
                <div class="span12">
                    <div class="alert alert-customer-message"
                         data-message-status="<?php echo ($cm->isPendingRead)? 'unread' : 'read' ?>"
                    >
                        <?php echo $cm->messageText; ?>
                        <hr style="margin: 10px 0;">
                        <?php if (!empty($cm->attributes->profiles) && strlen($cm->customerAddress) > 8): ?>
                            Enviado a: 
                            <?php foreach ($cm->attributes->profiles as $cm_profile): ?>
                                <a target="_blank" href=<?php echo site_url('padmin/profiles/edit/' . $cm_profile->id) ?>>
                                    <?php echo $cm_profile->user; ?>
                                </a>
                            <?php endforeach; ?>
                            <br>
                        <?php endif; ?>
                        
                        Enviado a <?php echo $cm->customerAddress; ?> 
                        el <?php echo $cm->createdAt; ?>

                        <a class="mini-btn-account-action" target="_blank" href="<?php echo site_url("padmin/messages/resend") . '?id=' . $cm->id ; ?>">Reenviar</a>
                    </div>
                </div>  

            </div>
            <?php endforeach; ?>
        <?php endif; ?>
    </div>
</div>