<div class="row">
	<?php
		$this->load->view('padmin/menu');
	?>

	<div class="span10">
		
		<?php $this->load->view('padmin/partials/status_messages'); ?>		

		<div class="row-fluid" style="margin-bottom: 15px;">
			<div class="span8">
				<h4 class="text-left" style="padding-top: 4px">
					<?php echo $total_groups; ?> grupo<?php echo ($total_groups > 1)? "s" : "" ?>
				</h4>
			</div>
			<div class="span4">
				<p class="text-right">
					<a class="btn" style="margin-top: 4px;" href="<?php echo site_url("padmin/profiles_groups/create_group");?>">
						<span class="glyphicon glyphicon-plus"></span>&nbsp;Crear grupo
					</a>
				</p>
			</div>
		</div>
		
		<div class="row-fluid">
			
			<?php if (count($total_groups)): ?>

				<table class="table table-condensed table-accounts">
					<thead>
						<tr>
							<th class="text-left">Nombre</th>
							<th>Titular</th>
							<th>Tel.</th>
							<th>Email</th>
						</tr>
					</thead>
					<tbody>
						<?php foreach ($groups as $group): ?>
						<tr>
							<td>
								<a href="<?php echo site_url('padmin/profiles_groups/edit_group/' . $group['id']); ?>">
									<b><?php echo $group['name']; ?></b>
								</a>	
							</td>
							<td><?php echo $group['owner']; ?></td>
							<td><?php echo $group['phone']; ?></td>
							<td><?php echo $group['email']; ?></td>
							
						</tr>
						<?php endforeach; ?>
					</tbody>
				</table>
						
			<?php else: ?>
				<h4 class="text-center">
					No se encontraron grupos.
				</h4>
			<?php endif?>
			
		</div>
	</div>
</div>