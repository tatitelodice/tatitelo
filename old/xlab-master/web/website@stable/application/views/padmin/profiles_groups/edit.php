<?php
	$form_error_start = '<span class="label label-important">Atención</span><span class="help-inline">';
	$form_error_end = '</span>';
?>

<div class="row">

	<?php
		$this->load->view('padmin/menu');
	?>

	<div class="span10">
		
		<div class="row-fluid">
			<div class="span12">
		
				<?php if ($success_message): ?>
					<div class="alert alert-success text-center">
						<button class="close" data-dismiss="alert">×</button>
						<?php echo $success_message ?>
					</div>

				<?php elseif ($error_message): ?>
					<div class="alert alert-error text-center">
						<button class="close" data-dismiss="alert">×</button>
						<?php echo $error_message ?>
					</div>

				<?php endif; ?>

			</div>
		</div>	
		
		<form method="POST">
			<div class="control-group">
				<label class="control-label" for="phone">Nombre del grupo</label>
				<div class="controls">
					<input type="text" class="input-xlarge editable" id="name" name="name" value="<?php echo set_value('name'); ?>">
					<?php echo form_error('name', $form_error_start, $form_error_end); ?>
				</div>			
			</div>
			
			<div class="control-group">
				<label class="control-label" for="phone">Teléfono Original</label>
				<div class="controls">
					<input type="text" class="input-xlarge editable" id="phone" name="phone" value="<?php echo set_value('phone'); ?>">
					<?php echo form_error('phone', $form_error_start, $form_error_end); ?>
				</div>			
			</div>

			<div class="control-group">
				<label class="control-label" for="email">E-mail</label>
				<div class="controls">
					<input type="text" class="input-xlarge editable" id="email" name="email" value="<?php echo set_value('email'); ?>">
					<?php echo form_error('email', $form_error_start, $form_error_end); ?>
				</div>			
			</div>
			
			<div class="control-group">
				<label class="control-label" for="panel_message">Mensaje a mostrar SÓLO en el panel del titular</label>
				<div class="controls">
					<textarea name="panel_message" class="editable" style="width: 97%; height: 100px;" ><?php echo set_value('panel_message'); ?></textarea>
					<?php echo form_error('panel_message', $form_error_start, $form_error_end); ?>
				</div>			
			</div>
			
		</form>
	</div>
	
</div>
