<?php
    $formErrorStart = '<p><span class="label label-important">Error</span><span class="help-inline">';
    $formErrorEnd   = '</span></p>';
?>

<form name="form-new-deposit" id="form-deposit" class="form-inline" method="POST" style="margin-bottom: 0">
    <div class="row-fluid">
        <div class="control-group">
            <label class="control-label" for="deposit_payment_account">Cuenta de Pago</label>
            <div class="controls">
                <select name="deposit_payment_account" style="width: 102%">
                    <option value="">Seleccionar...</option>
                    <?php foreach ($paymentAccounts as $pa): ?>
                        <option 
                            value="<?php echo $pa['id']; ?>"
                            <?php 
                                if (set_value('deposit_payment_account') === $pa['id']) 
                                {
                                    echo ' selected';
                                }
                            ?>
                        >
                        <?php echo $pa['accountId']; ?> (<?php echo $pa['name']; ?>)
                        </option>
                    <?php endforeach ?>
                </select>
                <?php echo form_error('deposit_payment_account', $formErrorStart, $formErrorEnd); ?>
            </div>
        </div>
    </div>

    <div class="row-fluid">
        <div class="control-group">
            <label class="control-label" for="deposit_name">Número de Autorización o Transacción</label>
            <div class="controls">
                <input 
                    type="text" 
                    class="" 
                    placeholder="Número del ticket" 
                    name="deposit_name" 
                    value="<?php echo set_value('deposit_name'); ?>" 
                    style="width: 100%;"
                >
                <?php echo form_error('deposit_name', $formErrorStart, $formErrorEnd); ?>
            </div>
        </div>
    </div>

    <div class="row-fluid">
        <div class="control-group">
            <label class="control-label" for="deposit_concept">Concepto</label>
            <div class="controls">
                <input 
                    type="text" 
                    class="" 
                    placeholder="Concepto" 
                    name="deposit_concept" 
                    value="<?php echo set_value('deposit_concept',  (!empty($depositDefaults['concept']))  ? $depositDefaults['concept'] : ''); ?>"
                    style="width: 100%"
                >
                <?php echo form_error('deposit_concept', $formErrorStart, $formErrorEnd); ?>
            </div>
        </div>
    </div>

    <div class="row-fluid">
        <div class="control-group">
            <label class="control-label" for="deposit_date">Fecha</label>
            <div class="controls" style="width:25%;">
                <input 
                    id="depositdate" 
                    type="text" 
                    class="input-medium" 
                    placeholder="Fecha" 
                    name="deposit_date" 
                    value="<?php echo set_value('deposit_date', date('d/m/Y')); ?>"
                    style="width: 100%"
                >
                <?php echo form_error('deposit_date', $formErrorStart, $formErrorEnd); ?>
            </div>
        </div>
    </div>

    <div class="row-fluid">
        <div class="control-group">
            <label class="control-label" for="deposit_time">Hora</label>
            <div class="controls" style="width:25%;">
                <input 
                    id="deposittime" 
                    type="text" 
                    class="input-medium" 
                    placeholder="00:00" 
                    name="deposit_time" 
                    value="<?php echo set_value('deposit_time'); ?>"
                    style="width: 100%"
                >
                
            </div>
            <?php echo form_error('deposit_time', $formErrorStart, $formErrorEnd); ?>
        </div>
    </div>

    <div class="row-fluid">
        <div class="control-group">
            <label class="control-label" for="deposit_amount">Monto</label>
            <div class="controls" style="width:25%;">
                <div class="input-prepend">
                    <span class="add-on">$</span>
                    <input 
                        type="text" 
                        class="input-medium" 
                        placeholder="Monto" 
                        name="deposit_amount" 
                        value="<?php echo set_value('deposit_amount', ''); ?>" 
                        style="width:100%;"
                    >
                </div>
            </div>
            <?php echo form_error('deposit_amount', $formErrorStart, $formErrorEnd); ?>
        </div>
    </div>

    <div class="row-fluid" style="margin-top:1rem">
        <div class="control-group">
            <div class="controls">
                <button type="submit" name="send" value="1" class="btn" style="width:100%;">
                    Registrar Pago
                </button>
            </div>
        </div>
    </div>
</form>

<script>
    $(document).ready(function()
    {
        $( '[name="deposit_date"]' ).datepicker();
    });
</script>
