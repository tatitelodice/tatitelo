<?php if (!empty($flashmessages)): ?>
<div data-ui-role="flashmessages">
    <?php foreach ($flashmessages as $fm): ?>
    <div class="alert alert-<?php echo $fm['type'] ?>">
        <button type="button" class="close" data-dismiss="alert">&times;</button>
        <strong><?php echo $fm['message'] ?></strong>
    </div>
    <?php endforeach ?> 
</div>
<?php endif ?>

<div class="row-fluid">
    <div class="span4">
        <h4>
            Información subida por usuario
        </h4>
        <table class="table">
            <tr>
                <td style="font-weight: bold;">Monto:</td>
                <td style="width:50%">
                    $<?php echo $depositUserSubmittedData->amount ?>
                </td>
                <td>
                    <button class="btn btn-mini" 
                        data-action="copy-value" 
                        data-value="<?php echo $depositUserSubmittedData->amount ?>" 
                        data-target-input="deposit_amount">
                        copiar
                    </button>
                </td>
            </tr>

            <tr>
                <td style="font-weight: bold;">Fecha:</td>
                <td style="width:50%">
                    <?php echo preg_replace("!(\d+)-(\d+)-(\d+)!", "$3/$2/$1", $depositUserSubmittedData->date) ?>
                </td>
                <td>
                    <button class="btn btn-mini" 
                        data-action="copy-value" 
                        data-value="<?php echo preg_replace("!(\d+)-(\d+)-(\d+)!", "$3/$2/$1", $depositUserSubmittedData->date) ?>" 
                        data-target-input="deposit_date">
                        copiar
                    </button>
                </td>
            </tr>
            <?php if (!empty($depositUserSubmittedData->time)): ?>
            <tr>
                <td style="font-weight: bold;">Hora:</td>
                <td style="width:50%">
                    <?php echo $depositUserSubmittedData->time ?>
                </td>
                <td>
                    <button class="btn btn-mini" 
                        data-action="copy-value" 
                        data-value="<?php echo $depositUserSubmittedData->time ?>" 
                        data-target-input="deposit_time">
                        copiar
                    </button>
                </td>
            </tr>
            <?php endif; ?>

            <tr>
                <td style="font-weight: bold;">Número de autorización o transacción:</td>
                <td style="width:50%">
                    <?php echo $depositUserSubmittedData->name ?>
                </td>
                <td>
                    <button class="btn btn-mini" 
                        data-action="copy-value" 
                        data-value="<?php echo $depositUserSubmittedData->name ?>" 
                        data-target-input="deposit_name">
                        copiar
                    </button>
                </td>
            </tr>
            <tr>
                <td style="font-weight: bold;">Telefono:</td>
                <td style="width:50%">
                    <?php echo $depositUserSubmittedData->phone ?>
                </td>
                <td></td>
            </tr>
            <tr>
                <td style="font-weight: bold;">Usuario:</td>
                <td style="width:50%">
                    <?php echo $depositUserSubmittedData->user ?>
                </td>
                <td></td>
            </tr>
            <tr>
                <td style="font-weight: bold;">Comentarios:</td>
                <td style="width:50%">
                <?php if (!empty($depositUserSubmittedData->comments)): ?>
                    <?php echo $depositUserSubmittedData->comments ?>
                <?php else: ?>
                    ( ninguno )
                <?php endif; ?>
                </td>
                <td></td>
            </tr>
        </table>

        <script>
            $('[data-action="copy-value"]').on('click', function() {
                var $el = $(this);
                var newInputValue = $el.attr("data-value");
                var targetInputName = $el.attr("data-target-input");
                console.log(targetInputName);
                $('[name="' + targetInputName + '"]').val(newInputValue);
            });
        </script>
    </div>
    <div class="span4">
        <h4>
            Foto de ticket
        </h4>
        <?php if (!empty($depositUserSubmittedData->photo)): ?>
        <div class="text-center" style="width: 100%;" >
            <a href="<?php echo base_url($depositUserSubmittedData->photo) ?>" target="_blank">
                <img src="<?php echo base_url($depositUserSubmittedData->photo) ?>">
            </a>
        </div>
        <?php else: ?>
        <p>
            No se adjuntó foto.
        </p>
        <?php endif; ?>        
    </div>

    <div class="span4">
    <h4>
        Registro de pago
    </h4>
    <?php
        $this->load->view("padmin/profile_deposits/form_create_deposit");
    ?>
    </div>
</div>
