<div class="row">

	<?php
		$this->load->view('padmin/menu');
	?>

	<div class="span10">
		<div class="note-well-view-title">
			<h4>
				<span class="glyphicon glyphicon-asterisk"></span>&nbsp;
				<?php echo $note["title"] ?>
			</h4>
		</div>

		<div id="note-content" class="note-well note-well-view-content"><?php echo $note["content"]; ?></div>
		
		<div class="row-fluid">
			<br>
		</div>
		
		<div class="row-fluid">
			<div class="span8">
				<p class="text-left note-well-view-footer">
					<?php echo "Última modificación: " . $note["created"] . ". Creada por " . $note["creator"] . " (" . $note["country"]. ")"; ?>
				</p>
			</div>
			<div class="span4 text-right">
				<a class="btn" href="<?php echo site_url("padmin/notes/edit/" . $note["id"]); ?>">Editar Nota</a>
			</div>
		</div>

	</div>
	
</div>

