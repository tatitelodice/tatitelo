<?php
	$current_url = current_url();
	$param_download_as_excel = 'format=xlsx';
	$url_download_as_excel = $current_url . ((empty($_GET)) ? '?' : '?' . $_SERVER['QUERY_STRING'] . '&') . $param_download_as_excel;
?>

<div class="row">

	<?php
		$this->load->view('padmin/menu');
	?>

	<div class="span10">
		
		<div class="row-fluid">
				
			<div class="span10">
				<form class="form-inline" style="margin-bottom: 0" method="get" action="<?php echo current_url(); ?>">
					
					<label><h5>Filtros:</h5></label>
					<?php 
						echo form_dropdown('gender', $gender_options, $filter_gender, ' class="input-medium"');
					?>

					<input name="date_from" value="<?php echo $filter_date_from; ?>" type="text" class="input-small"> 
					&nbsp;al&nbsp;
					<input name="date_to" value="<?php echo $filter_date_to; ?>"  type="text" class="input-small">
					
					<button class="btn" type="submit">Cambiar</button>
					&nbsp;
				</form>
			</div>

			<div class="span2 text-right">				
				<a class="btn btn-success" href="<?php echo $url_download_as_excel; ?>">
					 <span class="glyphicon glyphicon-download" style="margin-right: 4px;"></span>
					 <b>Bajar Excel</b>
				</a>
			</div>
		</div>
		
		<div class="row-fluid">
			<?php
				$this->load->view('padmin/profiles_marketing/table_never_paid_profiles');
			?>
		</div>
	</div>
</div>

<script>
	$(document).ready(function()
	{
		$( '[name="date_from"]' ).datepicker();
		$( '[name="date_to"]' ).datepicker();
	});
</script>