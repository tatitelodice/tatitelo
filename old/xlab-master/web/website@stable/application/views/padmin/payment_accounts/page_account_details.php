<div class="row">

    <?php
        $this->load->view('padmin/menu');
    ?>

    <div class="span10">
        
        <div class="row-fluid">
            <dl class="dl-horizontal">
            <?php foreach ($paymentAccount as $f => $v): ?>
                <dt><?php echo $f ?></dt>
                <dd><?php echo $v ?></dd>
            <?php endforeach; ?>
            </dl>
        </div>
    </div>
    
</div>