<div class="row">

	<?php
		$this->load->view('padmin/menu');
	?>

	<div class="span10">
		<?php if ($done): ?>
			<div class="alert alert-success">
				<h4 class="text-center">
					Se ha actualizado la cuenta de pago con éxito.
				</h4>
			</div>

			<p><b>Usuarios:</b></p>
			<blockquote><?php echo implode(", ", $users); ?></blockquote>
		
		<?php else: ?>
			<form method="POST" action="<?php echo site_url("padmin/payments_change_payment_account"); ?>">
				<input type="hidden" name="apply" value="yes">
				<input type="hidden" name="action" value="change_payment_account">

				<p class="text-center">
					Usuarios seleccionados (<?php echo '<span id="total_checked">' . count($users) . '</span> de ' . count($users); ?>):
				</p>
				
				<br>

				<p class="text-center">
					<?php foreach($users as $user): ?>
					<label class="checkbox inline">
						<input class="user_check" type="checkbox" name="users[]" value="<?php echo $user; ?>" checked="checked"><?php echo $user; ?>
					</label>
					<?php endforeach; ?>
				</p>

				<br>

				<p class="text-center">
					<b>Cuenta de Pago:</b>
					&nbsp;
					<select name="payment_account_id" style="margin-bottom: 0px;">
					<?php foreach ($payment_accounts as $pa): ?>
						<option value="<?php echo $pa['id']; ?>">
							<?php echo $pa['name']; ?>
						</option>
					<?php endforeach ?>
					</select>
					&nbsp;
					<button type="submit" class="btn input-medium">Aplicar</button>
				</p>
				
			</form>
		<?php endif ;?>
	</div>
	
	<script>
		function update_users_checked() {
			total_checked = $('.user_check').filter(':checked').length.toString();
			$("#total_checked").html(total_checked);
		}
		
		update_users_checked();
		
		$('.user_check').change(function() {
			update_users_checked();
		});
		
	</script>	
	
	
	
	
	
	
	
	
	
	
	