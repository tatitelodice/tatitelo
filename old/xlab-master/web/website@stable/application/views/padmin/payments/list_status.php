<div class="row">

	<?php
		$this->load->view('padmin/menu');
	?>


	<div class="span4 text-left">
		<h4 style="margin: 5px 0px 0px 0px"><a href="<?php echo site_url('padmin/payments/list_deposits/'); ?>">Ver depósitos</a></h4>
	</div>
	
	<div class="span6 text-right">
		
		<form class="form-inline pull-right" style="margin-bottom: 0" method="get" action="<?php echo site_url('padmin/payments/list_status') ?>">
			<div class="input-append">
				<input class="input-medium" type="text" name="search" placeholder="usuario" value="<?php echo $search; ?>">
				<button class="btn" type="submit">Buscar</button>
			</div>
		</form>
		
		
		<?php if (! (isset($admin_country) && $admin_country) ) : ?>
		<form class="form-inline pull-right" style="margin-bottom: 0" method="post" action="<?php echo site_url('padmin/payments/change_country') ?>">
			<?php 
				echo form_dropdown('payments_country', $country_options, $country, ' id="country-change" class="input-medium"');
			?>
			<button class="btn hidden" type="submit">Cambiar</button>

			<script>
				$(function() {
					$('#country-change').change(function() {
						this.form.submit();
					});
				});
			</script>
			&nbsp;
		</form>
		<?php endif; ?>

	</div>

	<div class="span10">
		<hr style="margin: 25px 0;">
	</div>

	<?php

		$status_reference = array(
			'debt' => 'Deudores',
			'ok' => 'Al día',
			'suspended' => 'Suspendidos',
			'pending' => 'A coordinar',
			'free' => 'Eximidos',
		);
		
		
		foreach ($status_reference as $k => $v) {
			
			echo '<div class="span2">';
			
			$status_count = count($users[$k]);
			
			echo '<h4>'.$v.' <small>('.$status_count.')</small></h4>';
			
			if ($status_count > 0) {
				
				echo anchor('padmin/payments/phones/' . $country . '/' . $k, '<i class="glyphicon glyphicon-chevron-right"></i>Teléfonos');
				
				echo '<br><br><ul class="payments-nav-stacked">';
				
				foreach ($users[$k] as $user) {
					$name = $user['gender'] . ' ' . $user['user'];
					
					echo '<li>';
					
					echo anchor( 'padmin/payments/view/' . $user['user'] . '?ref=payments', $name, '');
					
					if ($k == 'debt') {
						echo '&nbsp;&nbsp;&nbsp;' . anchor('padmin/payments/suspend_account/' . $user['user'], '<i class="glyphicon glyphicon-thumbs-down"></i>', 'class="suspend" title="Suspender"');
					}
					
					echo '</li>';
				}
				
				echo '</ul>';
				
			} else {
				echo '<p>Ninguno</p>';
			}
			
			echo '</div>';
		}
	
	?>
	

</div>