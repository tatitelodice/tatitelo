<div class="row">

	<?php
		$this->load->view('padmin/menu');
	?>

	<div class="span10">
		<?php if ($done): ?>
			<div class="alert alert-success">
				<h4 class="text-center">
					Se ha actualizado el mensaje del panel con éxito.
				</h4>
			</div>
				
			<p><b>Nuevo mensaje:</b></p>
			<blockquote><?php echo ($new_panel_message)? $new_panel_message : "(ninguno)"; ?></blockquote>

			<p><b>Usuarios:</b></p>
			<blockquote><?php echo implode(", ", $users); ?></blockquote>
		
		<?php else: ?>
			<form method="POST" action="<?php echo site_url("padmin/payments_change_panel_message"); ?>">
				<input type="hidden" name="apply" value="yes">
				<input type="hidden" name="action" value="panel_message">

				<div class="control-group">
					<label class="control-label" for="verified">Estado de usuarios:</label>
					<div class="controls">
						<?php
							echo form_dropdown('new_status', $status_options, '', ' id="new_status" class="input-large"');
							echo form_error('new_status', '<span class="label label-important">Atención</span><span class="help-inline">', '</span>');
						?>
					</div>			
				</div>
				
				<p>Mensaje para mostrar a los usuarios seleccionados:</p>
				<textarea name="new_panel_message" style="width: 98.5%" rows="10"></textarea>

				<p class="text-right">
					<button type="submit" class="btn input-medium">Aplicar</button>
				</p>

				<p>Usuarios seleccionados (<?php echo '<span id="total_checked">' . count($users) . '</span> de ' . count($users); ?>):</p>
				<?php foreach($users as $user): ?>
				<label class="checkbox inline">
					<input class="user_check" type="checkbox" name="users[]" value="<?php echo $user; ?>" checked="checked"><?php echo $user; ?>
				</label>
				<?php endforeach; ?>
			</form>
		<?php endif ;?>
	</div>
	
	<script>
		function update_users_checked() {
			total_checked = $('.user_check').filter(':checked').length.toString();
			$("#total_checked").html(total_checked);
		}
		
		update_users_checked();
		
		$('.user_check').change(function() {
			update_users_checked();
		});
		
	</script>	
	
	
	
	
	
	
	
	
	
	
	