<div class="row">

	<?php
		$this->load->view('padmin/menu');
	?>

	<div class="span10">
		<div class="row-fluid">
			<div class="span12 text-center">
				<h4 class="text-center">
					<a href="<?php echo site_url('padmin/payments/list_deposits/'); ?>">Ver lista de depósitos por fecha</a>
				</h4>
			</div>
		</div>
		
		<div class="row-fluid">
			<hr class="margin-top-20 margin-bottom-20">
		</div>
		
		<div class="row-fluid">
				
			<div class="span12">
				<form class="form-inline" style="margin-bottom: 0" method="post" action="<?php echo site_url('padmin/payments/change_list_published_settings') ?>">					
					
					<label><h5>Filtros:</h5></label>
					<?php 
						echo form_dropdown('payments_gender', $gender_options, $gender, ' id="gender-change" class="input-medium"');

						echo ' ';
						
						if (! (isset($admin_country) && $admin_country) ) {
							echo form_dropdown('payments_country', $country_options, $country, ' id="country-change" class="input-medium"');
						}
						
						echo ' ';
						
						echo form_dropdown('payments_publication_status', $publication_status_options, $publication_status, 'class="input-medium"');

						echo ' ';
						
						echo form_dropdown('payments_order_by', $order_by_options, $order_by, ' id="order-change" class="input-xlarge"');
					?>
					
					<button class="btn" type="submit">Cambiar</button>
					&nbsp;
				</form>
			</div>
		</div>
		
		<div class="row-fluid">
			<br>
			
			<?php if ($publication_status === 'suspended'): ?>
			<div class="alert alert-warning text-center">
				
				<b>Recuerda que estas viendo sólo los perfiles SUSPENDIDOS del ultimo mes</b>
			</div>
			<?php endif;?>
		</div>
				
		<?php if ($total_profiles): ?>
		<form method="POST" name="user-list-action" data-action-url-template="<?php echo site_url("padmin/payments_{{action}}"); ?>">
			
			<?php
				$this->load->view('padmin/payments/list_published_actions');
			?>
			<br>
			
			<div class="row-fluid">
				<div class="span12">
					<table class="table table-bordered table-condensed">
						<thead>
							<tr>
								<th class="text-center">Pos.</th>
								<th class="text-center">Cuenta</th>
								<th class="text-center">Grupo</th>
								<th>Nombre</th>
								<th>Usuario</th>
								<th>Tel.</th>
								<th class="text-center">Ultimo pago</th>
								<th class="text-center">Cantidad</th>
								<th class="text-center">
									<input id="select-all-checkboxes" type="checkbox">
								</th>
							</tr>
						</thead>
						<tbody>
						<?php foreach ($profiles as $profile): ?>
							<tr class="row-profile">
								<?php $colored_class = 'bg-' . strtolower($profile['account_type']); ?>

								<td class="text-center <?php echo $colored_class ?>">
									<b><?php echo $profile['position']; ?></b>
								</td>

								<td class="text-center <?php echo $colored_class ?>">
									<?php echo $profile['account_type']; ?>
								</td>
																	
								<td class="text-center <?php echo ($profile['group']) ? 'bg-grupo' : '' ?>">
									<?php echo $profile['group']; ?>	
								</td>

								<?php
									$user_payments_link = 'padmin/payments/view/'.$profile['user'].'?ref=payments';
									
									echo '<td>' . anchor($user_payments_link, $profile['name'], ' target="_blank"') . '</td>';
									echo '<td>' . anchor($user_payments_link, $profile['user'], ' target="_blank"') . '</td>';
									echo '<td>' . $profile['phone'] . '</td>';
									echo '<td class="text-center">' . $profile['last_payment_date'] . '</td>';
									echo '<td class="text-right">' . $profile['last_payment_amount'] . '</td>';
								?>

								<td class="text-center">
									<input type="checkbox" name="users[]" value="<?php echo $profile["user"] ?>">
								</td>
							</tr>
						<?php endforeach ?>
						</tbody>
					</table>
				</div>
			</div>
		
			<?php
				$this->load->view('padmin/payments/list_published_actions');
			?>
			
		</form>
		<?php else: ?>
			<h4 class="text-center">No se encontraron perfiles publicados</h4>
		<?php endif; ?>	
	</div>
</div>

<?php 
	$this->load->view('padmin/general/modal');
?>

<div class="modal-users-list hide">
	<textarea style="width: 98%; height: 100%;" rows="10" name="raw_userlist"></textarea>	
</div>


<script>
	$("input[name='users[]']").each(function(index, elem) {
		if ($(elem).is(":checked")) {
			$(elem).parent().parent().addClass("row-profile-selected");
		}
	});
	
	
	$("input[name='users[]']").click(function() {
		$(this).parent().parent().toggleClass("row-profile-selected");
	});
	
	
	$("#select-all-checkboxes").click(function() {
		var set_as_checked = $(this).is(":checked");
		
		$("input[name='users[]']").each(function(index, elem) {
			
			if ($(elem).prop("checked") !== set_as_checked) {
				$(elem).prop("checked", set_as_checked);
				$(elem).parent().parent().toggleClass("row-profile-selected");
			}
		});
	});
	
	$('[data-action=selection-dialog]').click(function() {
		$('.modal-title').html('Seleccionar usuarios');
		
		var users_textarea = $('.modal-users-list').html();
		$('.modal-body').html(users_textarea);
		
		$('.modal-btn-accept')
			.text('Seleccionar')
			.click(function() {
				var raw_users = $('[name=raw_userlist]').val().split(/\r?\n/g);
				
				var users = [];
				
				for (var i=0; i < raw_users.length; i++) {
					var clean_user = raw_users[i].trim();
					if (clean_user.length > 0) {
						users.push(clean_user);
					}
				}
				
				$("input[name='users[]']").each(function(index, elem) {
					var in_array = users.indexOf($(elem).val()) >= 0;
					
					if (in_array && $(elem).prop("checked") !== true) {
						$(elem).prop("checked", true);
						$(elem).parent().parent().toggleClass("row-profile-selected");
					}
				});
			
				$(this).parents('.modal').modal('hide');
			})
		;
		
		$('.modal').modal();
	});
		
</script>


<script>
	$('.profile-list-actions-submit').click(function(evt) {
		var parent = $(this).parents('.profile-list-actions');
		parent.addClass('profile-list-actions-current');
		
		$('.profile-list-actions').each(function(i) {
			var elem = $(this);
			
			if (!elem.hasClass('profile-list-actions-current')) {
				elem.remove();
			}
		});
		
		return true;
	});

	var $formUserListAction = $('form[name="user-list-action"]');

	$formUserListAction.on('change', 'select[name="action"]', function(ev)
	{
		var selectedActionName = $(this).val();

		var actionUrl = $formUserListAction.attr('data-action-url-template');

		actionUrl = actionUrl.replace('{{action}}', selectedActionName);

		$formUserListAction.attr('action', actionUrl);
	});

	
	
</script>

