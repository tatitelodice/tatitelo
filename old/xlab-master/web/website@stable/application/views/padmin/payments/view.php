<div class="row">
	<?php if ($success): ?>
	<div class="span12">
		<div class="alert alert-success text-center">
			<button data-dismiss="alert" class="close">×</button>Los datos han sido actualizados con éxito!
		</div>
	</div>
	<?php endif; ?>
	
	<div class="span12">
		<h5 class="text-left">
			<small>
				El perfil 
			</small>	
			
			<?php echo $user; ?> 
			
			<small>se encuentra 
			<?php if ($verified == PROFILE_STATUS_VERIFIED): ?>
				<span class="label label-verificado">Verificado en Portada</span>
			<?php elseif ($verified == PROFILE_STATUS_HIDDEN): ?>
				<span class="label label-oculto">Verificado Oculto</span>
			<?php else: ?>
				<span class="label label-verificar">NO verificado</span>		
			<?php endif; ?>
				
			<?php if ($absent): ?>
				<span class="label label-rose">Ausente</span>
			<?php endif; ?>
				
			<span class="label label-<?php echo strtolower($account_type) ?>"><?php echo $account_type ?></span>
			
			<a class="btn btn-mini" href="<?php echo site_url('padmin/profiles/edit/' . $client_id ) ?>">Editar</a>
			</small>
		</h5>
	</div>
	
	<div class="span6">
		<h4>Estado de pagos</h4>
		
		<div class="well">
		<?php
			$this->load->view('padmin/payments/form_payment_status');
		?>
		</div>
	</div>
	
	<div class="span6">
		<div class="control-group">
			<h4>Ingresar depósito</h4>
			
			<div class="well">
				<?php
					$this->load->view('padmin/deposits/form_new_deposit');
				?>
			</div>
			
			<?php
				$this->load->view('padmin/deposits/partial_profile_deposit_list');
			?>
		</div>
	</div>
</div>

<script>
	var newDepositBaseUrl = <?php echo json_encode(site_url('padmin/deposits/new_profile_deposit/' . $user)) ?>;

	$(document).ready(function() {
		$( "#nextdate" ).datepicker();

		var $formNewDeposit = $('form[name="form-new-deposit"]');

		$('[data-action="submit-form-new-deposit"]').click(function(e)
		{
			e.preventDefault();
			$formNewDeposit.submit();
		});

		$formNewDeposit.on('submit', function(evt) {
			evt.preventDefault(); // prevent default form submit

			var $thisForm = $(evt.target);
			var $thisAlert = $thisForm.find('.alert');

			$.ajax({
				url: newDepositBaseUrl, // form action url
				type: 'POST', // form submit method get/post
				dataType: 'json', // request type html/json/xml
				data: $thisForm.serialize(), // serialize form data 
				beforeSend: function() {
					$thisAlert.fadeOut();
					//submit.html('Sending....'); // change submit button text
				},
				success: function(data) {
					//	alert.html(data).fadeIn(); // fade in response data
					//	submit.html('Send Email'); // reset submit button text
					if (data.success)
					{
						$thisForm.trigger('reset'); // reset form
					}
					else
					{
						$thisAlert.html(
							'<p style="font-weight: bold;">Errores encontrados:</p>' +
							data.error
						);

						$thisAlert.fadeIn();
					}

					reload_deposits_list();
				},
				error: function(e) {
				//	console.log(e)
				}
			});
		});
	});
</script>