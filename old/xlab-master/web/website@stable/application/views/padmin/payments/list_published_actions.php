<div class="profile-list-actions row-fluid">
	<div class="span6 text-left">
		<button type="button" class="btn" data-action="selection-dialog">
			<b><span class="glyphicon glyphicon-th-list"></span></b>
		</button>

		<select name="action" style="line-height: 20px; margin-right: 8px; margin-bottom: 0;">
			<option>Elige acción...</option>
			<option value="change_panel_message">Cambiar mensaje de panel</option>
			<option value="change_payment_account">Cambiar cuenta de pago</option>
		</select>

		<button type="submit" class="profile-list-actions-submit btn">
			 <span class="glyphicon glyphicon-ok-circle" style="line-height: 20px; margin-right: 4px;"></span>
			 <b>Aplicar a Selección</b>
		</button>
	</div>	
	
	<div class="span4">
		<h5 class="text-center"><?php echo $total_profiles; ?>
		<?php
			if ($publication_status === 'suspended') {
				echo 'SUSPENDIDOS ESTE MES';
			} else {
				echo 'PUBLICADOS';
			}
		?>
		</h5>
			
	</div>

	<div class="span2 text-right">				
		<a class="btn btn-success" href="<?php echo site_url("padmin/payments/list_published_to_excel?country=". $country. "&gender=" . $gender . "&publication_status=" . $publication_status . "&order_by=" . $order_by) ?>">
			 <span class="glyphicon glyphicon-download" style="line-height: 20px; margin-right: 4px;"></span>
			 <b>Bajar Excel</b>
		</a>
	</div>
</div>

