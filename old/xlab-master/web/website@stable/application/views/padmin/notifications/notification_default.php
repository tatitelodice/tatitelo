<div class="alert alert-notification alert-notification-<?php echo $notification['css_class']; ?>">
    <a 
        class="close" 
        href="<?php echo site_url('padmin/notifications/mark_as_read/'.$notification['id']) ?>"
    >
        &times;
    </a>

    <strong><?php echo $notification['date']; ?></strong>
    &nbsp;&nbsp;&nbsp;
    ( IP <?php echo $notification['ip']; ?> )
    <hr>
    <img 
        class="flag16" 
        title="<?php echo $notification['country'] ?>" 
        alt="<?php echo $notification['country'] ?>" 
        src="<?php echo site_url('img/flags/'.$notification['country'].'_24.png') ?>"
    >
    &nbsp;
    <?php echo $notification['gender_symbol'] ?>

    <?php echo $notification['text']; ?>
</div>