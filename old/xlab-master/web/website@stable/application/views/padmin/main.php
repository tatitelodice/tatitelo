<div class="row">

	<?php
		$this->load->view('padmin/menu');
	?>
	
	<div class="span10" style="text-align: center;">
		<h4>Puede consultar las Bases de Datos disponibles en el menú izquierdo.</h4>
		<br><br>
		<blockquote class="nice-quote">
			<p>
				<?php echo $quote; ?>
			</p>
		</blockquote>
		
	</div>
	
</div>


<div class="row" style="text-align: right; margin-bottom: 50px;">
<br><br>
<h4><a class="btn btn-large btn-rose" href="<?php echo site_url('padmin/main/logout')?>">Cerrar Sesión</a></h4>
</div>