
<table class="table table-condensed table-striped table-accounts-phone">
	<thead>
		<tr>
			<th colspan="2">Cuentas encontradas:</th>
		</tr>
	</thead>
	<tbody>
		<?php foreach($accounts as $account): ?>
			<tr>
				<td class="<?php echo $account['gender']; ?>">
					<?php  
						$flag_url = base_url('img/flags/'.$account["country"].'_24.png');
						echo '<img class="flag16" title="'.$account["country"].'" alt="'.$account["country"].'" src="' . $flag_url .'">';
					?>
					<br>
					<strong><?php echo $account["gender"]; ?></strong>
				</td>				
				
				<td>
					<div class="name">
						<?php  					
							echo $account["name"];
							echo ' ( ' . anchor( $account['user'], $account['user'], 'target="_blank"') . " )";	
						?>
					</div>
					
					<br>
					
					<?php if ($account['deleted']): ?>
						<span class="label label-rose">Eliminada</span>
					<?php else: ?>
						<?php if ($account['verified'] != PROFILE_STATUS_NOT_VERIFIED): ?>

							<?php if ($account['verified'] == PROFILE_STATUS_VERIFIED): ?>
								<span class="label label-verificado">Verificada</span>
							<?php elseif ($account['verified'] == PROFILE_STATUS_HIDDEN): ?>
								<span class="label label-oculto">Oculta</span>
							<?php endif; ?>

							<span class="label label-<?php echo strtolower($account['account_type']); ?>">
								<?php echo $account["account_type"]; ?>
							</span>
						<?php else: ?>
							<span class="label label-verificar">Verificar</span>
						<?php endif; ?>
					<?php endif; ?>

					<?php if ($account['absent']): ?>
						<span class="label label-rose">Ausente</span>
					<?php endif; ?>
						
					<br>
					
					<div class="actions">
						<a target="_blank" title="Editar cuenta" href="<?php echo site_url("padmin/profiles/edit/" . $account["id"]) ?>">
							<i class="glyphicon glyphicon-user"></i>
						</a>

						<a target="_blank" title="Entrar al panel" href="<?php echo site_url($account['user'] . "/panel") ?>">
							<i class="glyphicon glyphicon-log-in"></i>
						</a>

						<a target="_blank" title="Pagos" href="<?php echo site_url("padmin/payments/view/".$account['user']) ?>">
							<i class="glyphicon glyphicon-usd"></i>
						</a>

						<a target="_blank" title="Copiapega" href="<?php echo site_url("padmin/adsmaterial/get/".$account['user']) ?>">
							<i class="glyphicon glyphicon-paperclip"></i>
						</a>
					</div>
					
				</td>
			</tr>
		<?php endforeach; ?>
	</tbody>
</table>