<?php
	if (isset($list_start_index)) {
		if ($list_total > 100) {

			if ($terms != '') {
				$terms = '?terms=' . urlencode($terms);
			}

			if ($list_start_index > 100) {
				echo anchor('padmin/profiles/list_accounts/' . ($list_start_index - 101) . $terms, 'Anterior', 'class="btn btn-mini"' ) . '&nbsp;&nbsp;&nbsp;';
			}

			echo '<span>Mostrando '.$list_start_index.' - '.$list_end_index.' de '.$list_total . '</span>';

			if ($list_start_index + 100 < $list_total) {
				echo '&nbsp;&nbsp;&nbsp;'.anchor('padmin/profiles/list_accounts/' . $list_end_index . $terms, 'Siguiente', 'class="btn btn-mini"' );
			}
		} else {
			echo '<span>Mostrando '.$list_total . ' encontrados.</span>';
		}
	}	
?>