<div class="row">
	<?php
		$this->load->view('padmin/menu');
	?>
	
	<div class="span10">
		<h4 style="text-align: center;">
			
			<?php if ($message == 'no_user'): ?>
				<div class="alert alert-error" style="text-align: center;">
					Ooops, el usuario '<?php echo $user; ?>' no existe!.
				</div>
			
			<?php elseif ($message == 'success'): ?>
				<div class="alert alert-success" style="text-align: center;">
					Éxito, la cuenta '<?php echo $old_user; ?>' ha pasado a llamarse '<?php echo $user; ?>'.
				</div>
				<br>
				<?php echo anchor($user, $site_domain . '/' . $user, 'target="_blank"'); ?>
				
			<?php else: ?>	
				Se cambiará el nombre de la cuenta '<?php echo $user; ?>' por el escrito en el campo de texto
				al apretar Cambiar.
				<br><br>

				<form class="form-inline" method="post" action="<?php echo site_url('padmin/profiles/change_username/'.$user) ?>">
					<input name="new_user" type="text" class="input-large" placeholder="nuevo nombre de cuenta">
					<button type="submit" class="btn">Cambiar</button>
					<?php 
						if (form_error('new_user')) {
							echo form_error('new_user', '<br><br><span class="label label-warning"><i class="glyphicon glyphicon-warning-sign"></i>&nbsp;Ooops!</span><span class="help-inline">', '</span>');
						} else {
							echo '<br><br><span class="help-inline">(sólo letras, números ó guiones)</span>';
						}
					?>
				</form>
				
			<?php endif; ?>
		</h4>
		
	</div>
</div>