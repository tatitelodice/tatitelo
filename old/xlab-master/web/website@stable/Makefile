
# HELP
# This will output the help for each task
# thanks to https://marmelab.com/blog/2016/02/29/auto-documented-makefile.html
.PHONY: help

help: ## This help.
	@awk 'BEGIN {FS = ":.*?## "} /^[a-zA-Z_-]+:.*?## / {printf "\033[36m%-30s\033[0m %s\n", $$1, $$2}' $(MAKEFILE_LIST)

.DEFAULT_GOAL := help

SRC_DIR        = src/
DIST_DIR       = public/
CSS_DIST_DIR   = $(DIST_DIR)/css/
JS_DIST_DIR    = $(DIST_DIR)/js/
IMG_DIST_DIR   = $(DIST_DIR)/img/
FONTS_DIST_DIR = $(DIST_DIR)/fonts/

HTTPD_USER      = $(shell ps axo user,comm | grep -E '[a]pache|[h]ttpd|[_]www|[w]ww-data|[n]ginx' | grep -v root | head -1 | cut -d\  -f1)

BROWSERIFY_DEV  = NODE_ENV=development NODE_PATH="javascript/" npx browserify 
BROWSERIFY_PROD = NODE_ENV=production NODE_PATH="javascript/" npx browserify -t envify -t uglifyify

SASS_INCLUDES   = -I node_modules/ -I sass/
SASSC_DEV       = sassc -m inline -t expanded $(SASS_INCLUDES) 
SASSC_PROD      = sassc -t compressed $(SASS_INCLUDES) 

.PHONY: release-prod
release-prod: javascript-build-prod css-build-prod images fonts 


.PHONY: watch
watch:
	npx concurrently \
	"make sass-watch" \
	"make javascript-watch" \
	--kill-others 

.PHONY: javascript-watch ## Compile and Watch javascript files
javascript-watch:
	@NODE_PATH="$(SRC_DIR)" \
	npx watchify $(SRC_DIR)/AppBundle/javascript/chicasuy.js -o $(JS_DIST_DIR)/chicasuy.js --verbose


.PHONY: javascript-build
javascript-build: javascript-clean ## Compile javascript files with sourcemaps
	@mkdir -p $(JS_DIST_DIR)
	$(BROWSERIFY_DEV) javascript/common/index.js -o $(JS_DIST_DIR)/common.js
	$(BROWSERIFY_DEV) javascript/home/index.js -o $(JS_DIST_DIR)/home.js
	$(BROWSERIFY_DEV) javascript/panel/index.js -o $(JS_DIST_DIR)/panel.js
	$(BROWSERIFY_DEV) javascript/padmin/index.js -o $(JS_DIST_DIR)/padmin.js
	$(BROWSERIFY_DEV) assets/themes/common/js/common.js -o $(DIST_DIR)/assets/themes/common/js/common.js
	$(BROWSERIFY_DEV) assets/themes/common/js/initialize.js -o $(DIST_DIR)/assets/themes/common/js/initialize.js


.PHONY: javascript-build-prod
javascript-build-prod: javascript-clean ## Compile and minify javascript files without sourcemaps
	@mkdir -p $(JS_DIST_DIR)
	$(BROWSERIFY_PROD) javascript/common/index.js -o $(JS_DIST_DIR)/common.js
	$(BROWSERIFY_PROD) javascript/home/index.js -o $(JS_DIST_DIR)/home.js
	$(BROWSERIFY_PROD) javascript/panel/index.js -o $(JS_DIST_DIR)/panel.js
	$(BROWSERIFY_PROD) javascript/padmin/index.js -o $(JS_DIST_DIR)/padmin.js
	$(BROWSERIFY_PROD) assets/themes/common/js/common.js -o $(DIST_DIR)/assets/themes/common/js/common.js
	$(BROWSERIFY_PROD) assets/themes/common/js/initialize.js -o $(DIST_DIR)/assets/themes/common/js/initialize.js


javascript-clean: ## Clean compiled javascript files
	@rm -rf $(JS_DIST_DIR)
	@mkdir -p $(JS_DIST_DIR)


.PHONY: sass-watch 
sass-watch: ## Compile and Watch css files
	npx sane 'make css-build' $(SASS_DIR) --glob "**/*.scss"


.PHONY: css-build
css-build: css-clean ## Clean compiled css files
	$(SASSC_DEV) sass/pumbate.scss $(CSS_DIST_DIR)/pumbate.css
	$(SASSC_DEV) sass/padmin.scss $(CSS_DIST_DIR)/padmin.css
	$(SASSC_DEV) assets/themes/common/sass/initialize.scss $(DIST_DIR)/assets/themes/common/css/initialize.css

.PHONY: css-build-prod
css-build-prod: css-clean ## Compile and minify css files without sourcemaps
	$(SASSC_PROD) sass/pumbate.scss $(CSS_DIST_DIR)/pumbate.css
	$(SASSC_PROD) sass/padmin.scss $(CSS_DIST_DIR)/padmin.css
	$(SASSC_PROD) assets/themes/common/sass/initialize.scss $(DIST_DIR)/assets/themes/common/css/initialize.css

.PHONY: css-clean
css-clean: ## Compile and Watch css files
	@rm -rf $(CSS_DIST_DIR)
	@mkdir -p $(CSS_DIST_DIR)

png2jpeg = magick mogrify -quality 75 -format jpg
jpegoptim = jpegoptim --all-progressive --strip-all

.PHONY: images
images: images-clean ## Copy images to public dir
	mkdir -p $(IMG_DIST_DIR)
	cp -R assets/img/* $(IMG_DIST_DIR)
	#$(png2jpeg) $(IMG_DIST_DIR)/profile-catalog/*.png
	find $(IMG_DIST_DIR) -name *.jpg -exec $(jpegoptim) '{}' \;


.PHONY: images-clean
images-clean: ## Clean images from public dir
	rm -rf $(IMG_DIST_DIR)

.PHONY: fonts
fonts: fonts-clean ## Copy fonts to public dir
	mkdir -p $(FONTS_DIST_DIR)
	cp -R assets/fonts/quicksand $(FONTS_DIST_DIR)/quicksand
	cp -R assets/fonts/glyphicons $(FONTS_DIST_DIR)/glyphicons

.PHONY: fonts-clean 
fonts-clean: ## Clean fonts from public dir
	rm -rf $(FONTS_DIST_DIR)


.PHONY: themes
themes: ## Build all theme assets
	mkdir -p $(DIST_DIR)/assets/themes/
	cp -R assets/themes/common $(DIST_DIR)/assets/themes/
	cp -R application/views/themes/* $(DIST_DIR)/assets/themes/
	find $(DIST_DIR)/assets/themes/ -type f -name *.php -exec rm '{}' \;
	$(SASSC_DEV) assets/themes/common/sass/initialize.scss $(DIST_DIR)/assets/themes/common/css/initialize.css
	$(BROWSERIFY_DEV) assets/themes/common/js/common.js -o $(DIST_DIR)/assets/themes/common/js/common.js
	$(BROWSERIFY_DEV) assets/themes/common/js/initialize.js -o $(DIST_DIR)/assets/themes/common/js/initialize.js

.PHONY: dev-server ## Start php local server
dev-server:
	php -S 0.0.0.0:8000 -t public/

.PHONY: php-server
php-server: ## Start php local server
	php -S 0.0.0.0:8000 -t public/


.PHONY: dirs
dirs: ## Create required dirs
	mkdir -p var/log/
	mkdir -p var/cache/
	# userdata
	cd public/ && ln -sf ../../media/userdata;
	# tempdata
	mkdir -p public/tempdata/
	chown -R $(HTTPD_USER):$(HTTPD_USER) public/tempdata
	chmod -R 775 public/tempdata
	# sitedata
	mkdir -p public/sitedata/
	chown -R $(HTTPD_USER):$(HTTPD_USER) public/sitedata
	chmod -R 775 public/sitedata
	

.PHONY: test
test: ## Run all tests
	./vendor/bin/phpunit --bootstrap vendor/autoload.php --testdox tests