ALTER TABLE `pumbate_site`.`deposits` 
ADD COLUMN `depositTime` TIME NULL AFTER `paymentAccountId`,
ADD COLUMN `modifiedAt` DATETIME NOT NULL AFTER `depositTime`,
ADD COLUMN `createdAt` DATETIME NOT NULL AFTER `modifiedAt`;
UPDATE `pumbate_site`.`deposits`  SET createdAt=depDate, modifiedAt=depDate;