CREATE TABLE `pumbate_site`.`administrator_permission` (
  `id` INT UNSIGNED NOT NULL AUTO_INCREMENT,
  `adminId` INT NOT NULL,
  `permissionType` VARCHAR(255) NOT NULL,
  `entityType` VARCHAR(255) NULL,
  `entityId` INT UNSIGNED NULL,
  `isGranted` INT NOT NULL DEFAULT 0,
  PRIMARY KEY (`id`));


INSERT INTO `pumbate_site`.`administrator_permission` (`adminId`, `permissionType`, `entityType`, `entityId`, `isGranted`) VALUES ('1', 'payment_account_view', 'payment_account', '2', '1');
INSERT INTO `pumbate_site`.`administrator_permission` (`adminId`, `permissionType`, `entityType`, `entityId`, `isGranted`) VALUES ('1', 'payment_account_view', 'payment_account', '3', '1');
INSERT INTO `pumbate_site`.`administrator_permission` (`adminId`, `permissionType`, `entityType`, `isGranted`) VALUES ('4', 'payment_account_full', 'payment_account', '1');
INSERT INTO `pumbate_site`.`administrator_permission` (`adminId`, `permissionType`, `entityType`, `isGranted`) VALUES ('6', 'payment_account_full', 'payment_account', '1');
INSERT INTO `pumbate_site`.`administrator_permission` (`adminId`, `permissionType`, `entityType`, `isGranted`) VALUES ('1', 'payment_account_full', 'payment_account', '0');