ALTER TABLE `pumbate_site`.`deposits` 
ADD COLUMN `paymentAccountId` INT UNSIGNED NULL DEFAULT NULL AFTER `proUser`;

ALTER TABLE `pumbate_site`.`payments` 
ADD COLUMN `paymentAccountId` INT UNSIGNED NULL DEFAULT NULL AFTER `proUser`;

CREATE TABLE `payment_account` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `keyname` varchar(128) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `name` varchar(128) COLLATE utf8mb4_unicode_ci NOT NULL,
  `accountService` varchar(64) COLLATE utf8mb4_unicode_ci NOT NULL,
  `accountId` varchar(128) COLLATE utf8mb4_unicode_ci NOT NULL,
  `accountAlias` varchar(128) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `tags` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `isActive` tinyint(4) NOT NULL DEFAULT '0',
  `isDeleted` tinyint(4) NOT NULL DEFAULT '0',
  `createdAt` datetime NOT NULL,
  `modifiedAt` datetime NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `keyname_UNIQUE` (`keyname`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


INSERT INTO `pumbate_site`.`payment_account` 
(`keyname`, `name`, `accountService`, `accountId`, `accountAlias`, `tags`, `isActive`, `isDeleted`, `createdAt`, `modifiedAt`) VALUES 
('colectivo_abitab_2017', 'Colectivo Santi Cursos', 'colectivo_abitab', '81706', 'Santi Cursos', '[]', '0', '0', NOW(), NOW()),
('cuenta_redbrou_default', 'RedBrou Santiago', 'redbrou', '200-0186850', '200 018 6850', '[]', '1', '0', NOW(), NOW()),
('cuenta_redbrou_secundaria', 'RedBrou Nico', 'redbrou', '177-1161533', '177 116 1533', '[]', '1', '0', NOW(), NOW()),
('cuenta_redbrou_terciaria', 'RedBrou Santiago', 'redbrou', '177-1143200', '177 114 3200', '["system_managed"]', '1', '0', NOW(), NOW()),
('colectivo_abitab_default', 'Colectivo Mario Eventos', 'colectivo_abitab', '84257', 'Mario Eventos', '[]', '1', '0', NOW(), NOW())
;


ALTER TABLE `pumbate_site`.`payment_account` 
ADD COLUMN `priority` INT NOT NULL DEFAULT 100 AFTER `accountAlias`,
ADD COLUMN `monthlyLimit` INT NOT NULL DEFAULT 0 AFTER `priority`,
ADD COLUMN `currentBalance` INT NOT NULL DEFAULT 0 AFTER `monthlyLimit`,
ADD COLUMN `currencyIsoCode` VARCHAR(3) NOT NULL AFTER `currentBalance`,
ADD COLUMN `attributes` TEXT NOT NULL AFTER `tags`,
ADD COLUMN `isDefault` TINYINT NOT NULL DEFAULT 0 AFTER `attributes`;

update payment_account set currencyIsoCode='UYU' limit 100;
update payment_account set attributes='{}' limit 100;

UPDATE `pumbate_site`.`payment_account` SET `priority`='1', `monthlyLimit`='90000', `isDefault`='1' WHERE `id`='2';
UPDATE `pumbate_site`.`payment_account` SET `monthlyLimit`='90000' WHERE `id`='3';
UPDATE `pumbate_site`.`payment_account` SET `monthlyLimit`='50000' WHERE `id`='4';


SET SQL_SAFE_UPDATES = 0;

update deposits set paymentAccountId=2 where depExternalId like 'brou%';

update payments py inner join profiles p on p.proUser = py.proUser set paymentAccountId=3 
where proGender != 'mujer' and proCountry='uy';

update payments py inner join profiles p on p.proUser = py.proUser set paymentAccountId=2
where proGender = 'mujer' and proCountry='uy';

update payments py inner join profiles p on p.proUser = py.proUser set paymentAccountId=3
where proGender = 'mujer' and proCountry='uy' and proCreatedDate <  '2016-06-01';


