DROP TABLE `pumbate_site`.`administrator_permission`;

CREATE TABLE `administrator_acl_rule` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `adminId` int(11) NOT NULL,
  `resource` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `resourceId` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `permission` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

INSERT INTO `pumbate_site`.`administrator_acl_rule` (`adminId`, `permission`, `resource`, `resourceId`) VALUES ('1', 'view', 'payment_account', '2');
INSERT INTO `pumbate_site`.`administrator_acl_rule` (`adminId`, `permission`, `resource`, `resourceId`) VALUES ('1', 'view', 'payment_account', '3');
INSERT INTO `pumbate_site`.`administrator_acl_rule` (`adminId`, `permission`, `resource`) VALUES ('4', 'full', 'payment_account');
INSERT INTO `pumbate_site`.`administrator_acl_rule` (`adminId`, `permission`, `resource`) VALUES ('6', 'full', 'payment_account');