var jq = require('jquery');
global.$ = global.jQuery = jq;

require('jquery.scrollto');
require('jquery.easing');
require('@fancyapps/fancybox');
require('countable');
require('bootstrap-sass');
require('autosize');
