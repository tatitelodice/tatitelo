function moveTo(tag) {
	$.scrollTo('#' + tag, 1000, {easing:'easeOutCirc'});
}

$('#morepics').click(function () {
	$(this).addClass('hidden');
	$('.hidden-block').fadeIn();
});


$('#morepics-pic').click(function () {
	$(this).addClass('hide');
	$(this).siblings().removeClass('hide');
	$('.hidden-block').fadeIn();
});

$(document).ready(function(){
	$("#back-top").hide();

	$(function () {
		$(window).scroll(function () {
			if ($(this).scrollTop() > 100) {
				$('#back-top').fadeIn();
			} else {
				$('#back-top').fadeOut();
			}
		});

		// scroll body to 0px on click
		$('#back-top a').click(function () {
			$('body,html').animate({
				scrollTop: 0
			}, 800);
			return false;
		});
	});
	
	$('.fancybox').each(function(i, elem)
	{
		$(elem).attr('data-fancybox', 'gallery');
	});

	$(".fancybox").fancybox({
		nextClick: true,
		scrolling: 'no',
		helpers: {
			overlay: {
				locked: false
			}
		}
	});
	
	var cached_pics = new Array();
		
	/* picture preload */
	$(".block-gallery a").each(function(index) {
		if (cached_pics.length < 6) {
			cached_pics[index] = new Image();
			cached_pics[index].src = $(this).attr('href');
		} else {
			return true;
		}
	});
		
});

(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
})(window,document,'script','//www.google-analytics.com/analytics.js','ga');

ga('create', 'UA-40271044-1', 'pumbate.com');
ga('send', 'pageview');



