var countable = require('countable')

module.exports = {
    onDocumentReady: function() {
        if (!this.attachOnReady)
        {
            return false;
        }

        this._attach();
    }

,   attachCharCounter: function() {
        this.attachOnReady = true;
    }

,   _attach: function()
    {
        var self = this;

        var messageTextElem = document.querySelectorAll('[name="message_text"]');
        var $characterTotalCount = $('[data-role="character-count-summary"] [data-role="total-chars"]');

        countable.live(messageTextElem, function (counter) {
            $characterTotalCount.html(
                counter.characters
            );
        });
    }
}