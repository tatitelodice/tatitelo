var jQuery = require('jquery');
global.jQuery = global.$ = jQuery;

var jQueryScrollTo = require('jquery.scrollto')
,   jQueryUI = require('jquery-ui/ui/widget')
,   jQueryUI_DatePicker = require('jquery-ui/ui/widgets/datepicker')
,   jQueryUI_DatePicker_ES = require('jquery-ui/ui/i18n/datepicker-es')
;

var countable = require('countable')
,   bootstrap2 = require('bootstrap-sass')
,   autosize = require('autosize')
;

// found on notes view, not in use
function SelectAll(id) {
    document.getElementById(id).focus();
    document.getElementById(id).select();
}

// padmin navbar
$(document).ready(function()
{
    autosize(document.querySelectorAll('textarea'));
});

var appModules = {
    "padmin/messages": require('padmin/Messages')
,   "padmin/navbar": require('padmin/Navbar')
}

Object.keys(appModules).forEach(function(modKey)
{
    var modEntry = appModules[modKey];

    if (modEntry.onDocumentReady)
    {
        $(document).ready(modEntry.onDocumentReady.bind(modEntry));
    }
});

global.AppRequire = function(modPath)
{
    return appModules[modPath];
}