module.exports = {
    onDocumentReady: function() {
        $("#menu-nav-btn").click(function() {
            var menu_nav = $("#menu-nav")
            
            if (menu_nav.hasClass("hidden-phone")) {
                menu_nav.removeClass("hidden-phone");
            } else {
                menu_nav.addClass("hidden-phone");
            }
        });
    }
}