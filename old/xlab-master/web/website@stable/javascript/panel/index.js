'use strict';

var jQueryScrollTo = require('jquery.scrollto')
,   jQueryUI = require('jquery-ui/ui/widget')
,   jQueryUI_DatePicker = require('jquery-ui/ui/widgets/datepicker')
,   jQueryUI_DatePicker_ES = require('jquery-ui/ui/i18n/datepicker-es')
;

var PanelGallery = require('./Gallery');
var PanelGalleryFileUpload = require('./Gallery.FileUpload');

$(document).ready(function()
{
	$('#themes-content').hide();
	$('#gallery-content').hide();
	$('#ad-content').hide();
	$('#publication-content').hide();
	$('#visits-content').hide();

	$('.nav-tabs').children().click( function(event) { 
		$(this).siblings().removeClass('active');
		$(this).addClass('active');

		$('[id$=-content]').hide();

		var element_id = $(this).attr('id').replace('-tab', '-content');
		$('#' + element_id).fadeIn(1000);
	});

    $('[data-action="navigate-cover-picture-page"]').click(function()
    {
        $("#ad-tab").click();
    });

	$('.btn-nav-save-changes').click( function() {
		$('#save-changes').click();
	});

	PanelGallery.onDocumentReady();
	PanelGalleryFileUpload.onDocumentReady();
});