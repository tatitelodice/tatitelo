module.exports = {
    onDocumentReady: function()
    {
        var self = this;

        $('body').on('click', '.pumbate-adpic-block', function() {

            if (!$(this).hasClass('pumbate-adpic-block-selected')) {
                var pic = $(this).children().children('.btn-adpic').first().val();
                self.setMainPicture(pic);	
                $("input[name='adpic']").val(pic);
                $('.pumbate-adpic-block-selected').removeClass('pumbate-adpic-block-selected');
                $(this).addClass('pumbate-adpic-block-selected');
            }
        });
    }

,   getUser: function()
    {
        if (!this._user)
        {
            var user = $('[data-component="gallery"]').attr('data-gallery-user');
            this._user = user;
        }
        
        return this._user;
    }

,   refreshMainPicturePicker: function()
    {
        $.post( '', {
            picture: $("input[name='adpic']").val(),
            user: this.getUser(),
            action: 'html_adpics'
        }, function(html_adpics) {
            $('#adpics').html(html_adpics);
        });	
    }

,   setMainPicture: function(picture)
    {
        var post_data = {
            picture: picture,
            user: this.getUser(),
            action: 'adpic'
        };

        $.post('', post_data);
    }

,   fixMainPicturePickerSizes: function() {
        $('.pumbate-adpic-wrap img').each(function() {
            var img_w = $(this).width();
            var img_h = $(this).height();

            if (img_w > img_h) {
                $(this).addClass('horizontal');
            } else {
                $(this).addClass('vertical');
            }

        });
    }
}