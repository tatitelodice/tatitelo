//var jQuery = require('jquery');
var PanelGallery = require('./Gallery');

require('thirdparty/jquery.fileupload@5.19.7/jquery.ui.widget');

global.tmpl = require("thirdparty/jquery.fileupload@5.19.7/tmpl.min").tmpl;
global.loadImage = require("thirdparty/jquery.fileupload@5.19.7/load-image.min").loadImage;

require("thirdparty/jquery.fileupload@5.19.7/canvas-to-blob.min");
require("thirdparty/jquery.fileupload@5.19.7/bootstrap-image-gallery.min");
require("thirdparty/jquery.fileupload@5.19.7/jquery.iframe-transport");
require("thirdparty/jquery.fileupload@5.19.7/jquery.fileupload");
require("thirdparty/jquery.fileupload@5.19.7/jquery.fileupload-fp");
require("thirdparty/jquery.fileupload@5.19.7/jquery.fileupload-ui");

require("thirdparty/jquery.fileupload@5.19.7/bootstrap-image-gallery.min");

var uploaderComponentSelector = '[data-component="fileupload"]'; 

module.exports = {

    onDocumentReady: function()
    {
        this._createUploader();
        this._loadExistingPictures();
    }

,   _loadExistingPictures: function()
    {
        // Load existing files:
        $.ajax({
            // Uncomment the following to send cross-domain cookies:
            //xhrFields: {withCredentials: true},
            url: $(uploaderComponentSelector).fileupload('option', 'url'),
            dataType: 'json',
            context: $(uploaderComponentSelector)[0]
        }).done(function (result) {
            if (result && result.length) {
                $(uploaderComponentSelector)
                    .fileupload('option', 'done')
                    .call(this, null, {result: result})
                ;
            }
        });
    }

,   _createUploader: function()
    {
        // Initialize the jQuery File Upload widget:
        $(uploaderComponentSelector).fileupload({
            // Uncomment the following to send cross-domain cookies:
            //xhrFields: {withCredentials: true},
            url: $(uploaderComponentSelector).attr('data-fileupload-url')
        ,   add: function (e, data) {
                $('.alert-uploading').fadeIn(1000);

                var jqXHR = data.submit();

                jqXHR.done(function (result, textStatus, jqXHR)
                {
                    $('.alert-uploading').fadeOut(1000);
                   
                    if (result && result[0] && !result[0].error)
                    {
                        $('.alert-uploading-success').fadeIn(1000).delay(1500).fadeOut(1000);
                    }
                    else
                    {
                        $('.alert-uploading-error').fadeIn(1000).delay(1500).fadeOut(1000);
                    }

                    PanelGallery.refreshMainPicturePicker();
                });
            }
        ,   destroyed: function (e, data) {
                PanelGallery.refreshMainPicturePicker();
            }
        });
    }

}

