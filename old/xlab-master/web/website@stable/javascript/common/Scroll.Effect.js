require('jquery.scrollto');
require('jquery.easing');

jQuery(document).ready(function()
{
    jQuery('body').on('click', '[ui-effect="scroll"]', function()
    {
        var targetSelector = jQuery(this).attr('ui-effect-target');
        targetSelector = targetSelector ? targetSelector : jQuery(this).attr('href');
        jQuery.scrollTo(targetSelector, 1000, {easing:'easeOutCirc'});
    });    
});

