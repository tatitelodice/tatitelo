var jq = require('jquery');
global.$ = global.jQuery = jq;

require('./Scroll.Effect');

require('countable');
require('bootstrap-sass');

var autosize = require('autosize');

$(document).ready(function()
{
    autosize(document.querySelectorAll('textarea'));
});