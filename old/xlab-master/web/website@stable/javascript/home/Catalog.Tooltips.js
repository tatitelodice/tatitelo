/* PUMBATE MAIN PAGE SCRIPT */

// ON DOCUMENT READY

var wsize = $(window).width();

function loadCatalogTooltips()
{
	$('.gallery-wrap .simple-tooltip').tooltip({
		template: '<div class="pumbate-tooltip"><div class="tooltip-arrow"></div><div class="tooltip-inner"></div></div>'
	});

	$(".premium-tooltip").hover(

		function() {
			$('.gallery-wrap .alt-tooltip').tooltip('destroy');

			el = $(this);

			if ($('.popover').hasClass('in') === false) {
				thumb_pics_html = el.children('.thumbs').html();

				el.popover({
					content: el.children('.about').text() + '<div class="phone-big">' + el.children('.phone').text() + '</div>',
					title: '<div class="popover-img-container">' + thumb_pics_html + '</div>',
					html: true,
					placement: function(context, source) {
						if (wsize > 768) {
							var pos = el.attr('position');

							if ( pos % 8 === 0) {
								return 'left';
							} else if ( pos % 8 === 1 ) {
								return 'right';
							}
						}

						return 'top';
					},

					trigger: 'manual',
					delay: { show: 1000, hide: 100 }
				});	

			}

			el.popover('show');

		},

		function() {
			el = $(this);
			el.popover('hide');
		}	

	);
}

if (wsize > 768)
{
	loadCatalogTooltips();
}
	
/* END OF FILE */