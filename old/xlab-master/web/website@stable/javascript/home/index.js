require('common/index');

var jQueryScrollTo = require('jquery.scrollto')
,   jQueryUI = require('jquery-ui/ui/widget')
,   jQueryUI_DatePicker = require('jquery-ui/ui/widgets/datepicker')
,   jQueryUI_DatePicker_ES = require('jquery-ui/ui/i18n/datepicker-es')
;

$(document).ready(function()
{
    require('home/Register.Form');
    require('home/Catalog.LocationSelector');
    require('home/Catalog.Tooltips');
    require('home/Catalog.PromoPopup');
    require('tracking/Analytics');
});