var spacers = $('.promopop'),
	menu = $('.navbar-tryit'),
	$window = $(window),
	showMenu = function() {
		if (isHidden) {
			isHidden = false;
			menu.show();
		}
	},
	hideMenu = function() {
		if (!isHidden) {
			isHidden = true;
			menu.hide();
		}
	},
	isHidden = true,
	scrollTop;

if (spacers.length) {
	$window.on('scroll', function() {
		var flag = false;

		scrollTop = $window.scrollTop();
		$.each(spacers, function(i, spacer) {
			var $spacer = $(spacer),
				offset = $spacer.offset().top;

			if (offset < scrollTop && offset + $spacer.height() - 50 > scrollTop ) {
				flag = true;
				return false;
			}
		});

		if (flag) {
			showMenu();
		} else {
			hideMenu();
		}
	});
}	