$('#region').change( function() {									
	var cityobj = $('#city');

	if ($(this).val() === '') {
		cityobj.addClass('hide');
	} else {

		var post_data = {
			country: $('[name=country_iso]').val(),
			region: $(this).val(),
			gender: $('[name=escorts_gender]').val(),
			action: 'city'
		};

		$.post('', post_data, function(html_cities) {

			if (html_cities !== '') {
				cityobj.html(html_cities);
				cityobj.removeClass('hide');
			} else {
				cityobj.addClass('hide');
			}
		});
	}
});