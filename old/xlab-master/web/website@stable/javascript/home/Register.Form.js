// ON DOCUMENT READY
var gender_field_selector = 'select[name="gender"]';

function gender_select_change() {
	if ($(gender_field_selector).val() === "hombre") {
		$('#clients_type_block').removeClass('hide');
	} else {
		$('#clients_type_block').addClass('hide');
	}
}

$(gender_field_selector).change(gender_select_change);
gender_select_change();


$("select[name=clients_type]").change(function () {
	//console.log($(this).val());
	selected_val = $(this).val();
	
	if (selected_val === "mujeres" || selected_val === "mujeres2") {
		$('#woman-truth').removeClass("hide");
	} else {
		$('#woman-truth').addClass("hide");
	}
});

$("#advertise-pic").change(function () {
	$(this).next(".help-inline").html("Has seleccionado tu foto con éxito, si tienes más no te preocupes, luego las podrás subir!");
});
