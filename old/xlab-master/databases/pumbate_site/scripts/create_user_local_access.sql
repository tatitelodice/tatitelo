CREATE USER IF NOT EXISTS pumbate_site identified by 'pumbate_site';
GRANT ALL PRIVILEGES ON pumbate_site.* TO 'pumbate_site'@'localhost' identified by 'pumbate_site';
GRANT ALL PRIVILEGES ON pumbate_site.* TO 'pumbate_site'@'127.0.0.1' identified by 'pumbate_site';
FLUSH PRIVILEGES;