<?php


use Phinx\Migration\AbstractMigration;

class AddSettingTable extends AbstractMigration
{
    /**
     * Change Method.
     *
     * Write your reversible migrations using this method.
     *
     * More information on writing migrations is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-abstractmigration-class
     *
     * The following commands can be used in this method and Phinx will
     * automatically reverse them when rolling back:
     *
     *    createTable
     *    renameTable
     *    addColumn
     *    addCustomColumn
     *    renameColumn
     *    addIndex
     *    addForeignKey
     *
     * Any other distructive changes will result in an error when trying to
     * rollback the migration.
     *
     * Remember to call "create()" or "update()" and NOT "save()" when working
     * with the Table class.
     */
    public function change()
    {
        $table = $this->table('setting',[
            'id' => true
        ,   'primary_key' => 'id'
        ,   'collation' => 'utf8mb4_unicode_ci'
        ]);

        $table
            ->addColumn('keyname', 'string', ['limit' => 255])
            ->addColumn('value', 'json')
            ->addColumn('metadata', 'json')
            ->addColumn('createdAt', 'datetime')
            ->addColumn('modifiedAt', 'datetime')
            ->addIndex(['keyname'], ['unique' => true])
            ->save()
        ;
    }
}
