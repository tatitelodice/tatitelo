<?php

use Carbon\Carbon;
use Phinx\Migration\AbstractMigration;

class AddDefaultSettings extends AbstractMigration
{
	const PROFILE_FREE      = 0;
	const PROFILE_BASIC     = 1;
	const PROFILE_PREMIUM   = 2;
	const PROFILE_TOP       = 3;
	const PROFILE_SUPERSTAR = 4;
	
	const PROFILE_STATUS_NOT_VERIFIED = 0;
	const PROFILE_STATUS_VERIFIED     = 1;
	const PROFILE_STATUS_HIDDEN       = 2;
			
	const PROFILE_NOT_DELETED = 0;
	const PROFILE_DELETED     = 1;
	
	const PICTURES_NOT_VERIFIED = 0;
	const PICTURES_VERIFIED     = 1;
    /**
     * Change Method.
     *
     * Write your reversible migrations using this method.
     *
     * More information on writing migrations is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-abstractmigration-class
     *
     * The following commands can be used in this method and Phinx will
     * automatically reverse them when rolling back:
     *
     *    createTable
     *    renameTable
     *    addColumn
     *    addCustomColumn
     *    renameColumn
     *    addIndex
     *    addForeignKey
     *
     * Any other distructive changes will result in an error when trying to
     * rollback the migration.
     *
     * Remember to call "create()" or "update()" and NOT "save()" when working
     * with the Table class.
     */
    public function up()
    {
        $nowDt = Carbon::now('UTC');
        $settings = $this->getDefaultSettings();

        $data = [];

        foreach ($settings as $k => $v)
        {
            $data[] = [
                'keyname' => $k
            ,   'value' => json_encode($v)
            ,   'metadata' => json_encode((object)[])
            ,   'createdAt' => $nowDt
            ,   'modifiedAt' => $nowDt
            ];
        }

        $settingsTable = $this->table('setting');
        $settingsTable
            ->insert($data)
            ->save();
    }

    public function down()
    {
        $settings = $this->getDefaultSettings();

        $builder = $this->getQueryBuilder();
        $statement = $builder
            ->delete()
            ->from('setting')
            ->where(function ($exp) use ($settings) {
                return $exp->in('keyname', array_keys($settings));
            })
            ->execute();
    }


    protected function getDefaultSettings()
    {
        $config = [];
        /******************************************************************************/
        /*  PATHS                                                                     */
        /******************************************************************************/
        $config['media_dir_path'] = "../../media/";

        /******************************************************************************/
        /*  MODO MANTENIMIENTO                                                        */
        /******************************************************************************/

        $config['maintenance_mode'] = false;
        $config['suscribe_enabled'] = false;
        $config['profile_login_limit'] = 100;


        /******************************************************************************/
        /*  OPCIONES DE IMAGENES                                                      */
        /******************************************************************************/

        $config['profile_pictures_versions'] = array(
            '' => array(
            'max_width' => 1920,
            'max_height' => 1200,
            'jpeg_quality' => 80,
            'force_jpeg' => true,
            ),

            'optimized' => array(
            'max_width' => 1920,
            'max_height' => 1200,
            'jpeg_quality' => 75,
            ),

            'w200' => array(
                'min_height' => 250,
                'min_width' => 200,
                'max_height' => 250,
                'max_width' => 200,
                'jpeg_quality' => 75,
                'center_crop' => true,
            ),

            'thumbnail' => array(
                'max_width' => 80,
                'max_height' => 80,
                'jpeg_quality' => 75,
            )

        ,	'banner-md' => array(
                'min_height' => 300
            ,	'min_width' => 360
            ,	'max_height' => 300
            ,	'max_width' => 360
            ,	'jpeg_quality' => 80
            ,	'center_crop' => true
            ,	'required_account_type' => [self::PROFILE_SUPERSTAR, self::PROFILE_TOP]
            )

        ,	'banner-xs' => array(
                'min_height' => 180
            ,	'min_width' => 360
            ,	'max_height' => 180
            ,	'max_width' => 360
            ,	'jpeg_quality' => 75
            ,	'center_crop' => true
            ,	'required_account_type' => [self::PROFILE_SUPERSTAR, self::PROFILE_TOP]
            )

        );



        /******************************************************************************/
        /*  Datos para el envío automatizado de mail de notificacion de CONTACTO     */
        /******************************************************************************/

        $config['notification_mail_use_ssl'] = true;
        $config['notification_mail_server'] = 'mail.pumbate.com';
        $config['notification_mail_port'] = 587;


        $config['notification_mail_name'] = 'Pumbate Escorts';
        $config['notification_mail_user'] = 'rrpp@pumbate.com';
        $config['notification_mail_password'] = 'contact0.';

        /******************************************************************************/
        /* Email users                                                                */
        /******************************************************************************/

        $config["email_notification_users"] = true;
        $config["email_notification_admins"] = true;


        /******************************************************************************/
        /* SITE VARS                                                                 */
        /******************************************************************************/

        $config['site_name'] = "Pumbate";

        $config['site_domain'] = "www.pumbate.com";
        $config['site_domain_simple'] = "pumbate.com";

        $config['site_author'] = "Pumbate Escorts Latinoamérica";

        $config['site_admin_title_tag'] = "Pumbate Admin";

        $config['remote_site_domain'] = 'chicasuy.com';

        $config['website_base_url'] = 'https://www.pumbate.com/';

        /******************************************************************************/
        /* SITE PHONES                                                                */
        /******************************************************************************/

        $config['contact_phones'] = array(
            'uy' => array(
                'default' => '094 332 135',
                'sms'     => '094 332 135',
                'whatsapp'=> '094 332 135',
            )
        );

        /******************************************************************************/
        /* SITE EMAILS                                                                */
        /******************************************************************************/

        $config['contact_emails'] = array(
            'default' => 'rrpp@pumbate.com',
            'uy'      => 'rrpp@pumbate.com',
            'co'      => 'colombia@pumbate.com',
        );

            
        /******************************************************************************/
        /* AGENT EMAILS                                                               */
        /******************************************************************************/

        $config['agent_emails'] = array(
            
            'default' => array(
                'raveenz.debian@gmail.com' => 'Santiago',
            ),
            'uy' => array(
                'raveenz.debian@gmail.com' => 'Santiago',
        //		'nicoquint@gmail.com' => 'Nicolás',
            ),
            'co' => array(
                'colombia@pumbate.com' => "Pumbate Colombia",
            ), 	
        );


        /******************************************************************************/
        /* CATALOG                                                                    */
        /******************************************************************************/
        $config['catalog_display_profiles_without_pictures'] = false;
        $config['catalog_cache_enabled'] = true;

        $config['banner_min_count'] = 3;
        $config['banner_max_count'] = 5;

        $config['banner_sections'] = [
            'uy' => [ 
                ['gender' => 'mujer', 'keywords' => '']
            ,	['gender' => 'trans', 'keywords' => '']
            //,	['gender' => 'hombre', 'keywords' => '']
            ]
        ];


        /******************************************************************************/
        /* BILLING                                                                    */
        /******************************************************************************/

        $config['all_paid_profiles'] = true;


        $config['pricing'] = [
            'uy' => [
                'mujer'  => [
                    [ 'price' =>  390, 'accountType' => 1, 'text' => '2 SEMANAS Básica' ]
                ,	[ 'price' =>  750, 'accountType' => 1, 'text' => '1 MES Básica' ]
                ,	[ 'price' => 1250, 'accountType' => 2, 'text' => '1 MES Destacada' ]
                ,	[ 'price' => 1800, 'accountType' => 3, 'text' => '1 MES TOP']
                ,	[ 'price' => 2500, 'accountType' => 4, 'text' => '1 MES SUPER TOP (banner arriba)']
                ]
            ,	'hombre' => [
                    [ 'price' =>  250, 'accountType' => 1, 'text' => '2 SEMANAS Básico' ]
                ,	[ 'price' =>  500, 'accountType' => 1, 'text' => '1 MES Básico' ]
                ,	[ 'price' => 1000, 'accountType' => 2, 'text' => '1 MES Destacado' ]
                ,	[ 'price' => 1500, 'accountType' => 3, 'text' => '1 MES TOP' ]
                ]
            ,	'trans'  => [
                    [ 'price' =>  500, 'accountType' => 1, 'text' => '1 MES Básica' ]
                ,	[ 'price' => 1000, 'accountType' => 2, 'text' => '1 MES Destacada' ]
                ,	[ 'price' => 1800, 'accountType' => 3, 'text' => '1 MES TOP']
                ,	[ 'price' => 2500, 'accountType' => 4, 'text' => '1 MES SUPER TOP (banner arriba)']
                ]
            ,	'default' => [
                    [ 'price' =>  390, 'accountType' => 1, 'text' => '2 SEMANAS Básica' ]
                ,	[ 'price' =>  750, 'accountType' => 1, 'text' => '1 MES Básica' ]
                ]
            ]
        ];

        /******************************************************************************/
        /* ASSETS                                                                    */
        /******************************************************************************/
        $config['asset_packages'] = [
            'img' => [
                'base_url' => 'https://img.pumbate.com'
            ]
        ];

        return $config;
    }

}
