CREATE TABLE `administrator_acl_rule` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `adminId` int(11) NOT NULL,
  `resource` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `resourceId` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `permission` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

CREATE TABLE `administrators` (
  `admId` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `admName` varchar(64) NOT NULL,
  `admPassword` varchar(64) NOT NULL,
  `admCountry` varchar(2) DEFAULT NULL,
  `admSettings` text,
  PRIMARY KEY (`admId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `ads` (
  `adId` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `adSourceAdId` varchar(255) DEFAULT NULL,
  `adUrl` varchar(255) DEFAULT NULL,
  `adSource` varchar(255) DEFAULT NULL,
  `adDatetime` datetime DEFAULT NULL,
  `proUser` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`adId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `analytics` (
  `anaId` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `anaPageViews` int(10) unsigned DEFAULT NULL,
  `anaUniquePageViews` int(10) unsigned DEFAULT NULL,
  `anaDate` date DEFAULT NULL,
  `proUser` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  PRIMARY KEY (`anaId`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

CREATE TABLE `blacklist` (
  `blaId` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `blaCountry` varchar(2) DEFAULT NULL,
  `blaPhone` varchar(128) DEFAULT NULL,
  `blaEmail` varchar(255) DEFAULT NULL,
  `blaName` varchar(255) DEFAULT NULL,
  `blaDescription` text,
  `blaDate` date DEFAULT NULL,
  `blaDeleted` tinyint(3) unsigned DEFAULT '0',
  PRIMARY KEY (`blaId`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

CREATE TABLE `canalytics` (
  `canId` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `canPageViews` int(10) unsigned DEFAULT NULL,
  `canUniquePageViews` int(10) unsigned DEFAULT NULL,
  `canDatetime` datetime DEFAULT NULL,
  `proUser` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`canId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `cities` (
  `citId` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `citName` varchar(64) NOT NULL,
  `citCountry` varchar(2) DEFAULT NULL,
  `citActive` tinyint(1) NOT NULL DEFAULT '1',
  `regName` varchar(64) DEFAULT NULL,
  PRIMARY KEY (`citId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `clients` (
  `profile_group_id` int(10) unsigned NOT NULL DEFAULT '0',
  `cliGroup` varchar(128) NOT NULL DEFAULT '',
  `cliPhone` varchar(128) DEFAULT NULL,
  `cliComments` text,
  `cliSource` varchar(255) DEFAULT NULL,
  `proUser` varchar(255) DEFAULT NULL,
  UNIQUE KEY `proUser_UNIQUE` (`proUser`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `customer_message` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `uuid` binary(16) NOT NULL COMMENT '(DC2Type:uuid_binary)',
  `category` varchar(128) COLLATE utf8_unicode_ci NOT NULL,
  `channel` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `messageText` longtext COLLATE utf8_unicode_ci NOT NULL,
  `messageBox` varchar(10) COLLATE utf8_unicode_ci NOT NULL,
  `messageBoxStatus` varchar(32) COLLATE utf8_unicode_ci NOT NULL,
  `systemAddress` varchar(64) COLLATE utf8_unicode_ci DEFAULT NULL,
  `systemLabel` varchar(32) COLLATE utf8_unicode_ci DEFAULT NULL,
  `systemDate` datetime DEFAULT NULL,
  `customerEntityId` int(11) DEFAULT NULL,
  `customerAddress` varchar(64) COLLATE utf8_unicode_ci DEFAULT NULL,
  `customerLabel` varchar(32) COLLATE utf8_unicode_ci DEFAULT NULL,
  `tags` longtext COLLATE utf8_unicode_ci NOT NULL COMMENT '(DC2Type:json_array)',
  `attributes` longtext COLLATE utf8_unicode_ci NOT NULL COMMENT '(DC2Type:json_array)',
  `isPendingRead` tinyint(1) NOT NULL,
  `isDeleted` tinyint(1) NOT NULL,
  `createdAt` datetime NOT NULL,
  `modifiedAt` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `customer_message_uuid_idx` (`uuid`),
  KEY `customer_message_created_at_idx` (`createdAt`),
  KEY `customer_message_customer_address_idx` (`customerAddress`),
  KEY `customer_message_category_idx` (`category`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

CREATE TABLE `deposits` (
  `depId` int(11) NOT NULL AUTO_INCREMENT,
  `depExternalId` varchar(255) DEFAULT NULL,
  `depDate` date NOT NULL,
  `depAmount` int(11) NOT NULL,
  `depName` varchar(255) NOT NULL,
  `depConcept` varchar(255) DEFAULT NULL,
  `depStatus` varchar(45) DEFAULT 'not_defined',
  `depAppliedAt` datetime DEFAULT NULL,
  `depDeleted` tinyint(4) NOT NULL DEFAULT '0',
  `proUser` varchar(255) DEFAULT NULL,
  `paymentAccountId` int(10) unsigned DEFAULT NULL,
  `depositTime` time DEFAULT NULL,
  `modifiedAt` datetime NOT NULL,
  `createdAt` datetime NOT NULL,
  PRIMARY KEY (`depId`),
  UNIQUE KEY `depExternalId_UNIQUE` (`depExternalId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `horoscopes` (
  `horId` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `horSign` varchar(45) NOT NULL,
  `horDate` date NOT NULL,
  `horText` text NOT NULL,
  `horHash` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`horId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `keywords` (
  `proUser` varchar(255) NOT NULL,
  `keyLocations` text,
  `keyServices` text,
  `keyAppearance` text,
  `keyFulltext` text,
  PRIMARY KEY (`proUser`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `logs` (
  `logId` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `logCountry` varchar(2) NOT NULL,
  `logDescription` varchar(255) DEFAULT NULL,
  `logAddress` varchar(45) DEFAULT NULL,
  `logDate` datetime DEFAULT NULL,
  `logMeta` text,
  `logNotify` tinyint(3) unsigned DEFAULT NULL,
  `proUser` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`logId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `message_templates` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(128) NOT NULL,
  `template` text,
  PRIMARY KEY (`id`),
  UNIQUE KEY `name_UNIQUE` (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `notes` (
  `notId` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `notTitle` varchar(255) DEFAULT NULL,
  `notCategory` varchar(255) DEFAULT NULL,
  `notContent` text,
  `notCountry` varchar(2) DEFAULT NULL,
  `admName` varchar(255) DEFAULT NULL,
  `notCreated` datetime NOT NULL,
  `notModified` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `notDeleted` tinyint(3) unsigned DEFAULT '0',
  PRIMARY KEY (`notId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `payment_account` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `keyname` varchar(128) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `name` varchar(128) COLLATE utf8mb4_unicode_ci NOT NULL,
  `accountService` varchar(64) COLLATE utf8mb4_unicode_ci NOT NULL,
  `accountId` varchar(128) COLLATE utf8mb4_unicode_ci NOT NULL,
  `accountAlias` varchar(128) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `priority` int(11) NOT NULL DEFAULT '100',
  `monthlyLimit` int(11) NOT NULL DEFAULT '0',
  `currentBalance` int(11) NOT NULL DEFAULT '0',
  `currencyIsoCode` varchar(3) COLLATE utf8mb4_unicode_ci NOT NULL,
  `tags` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `attributes` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `isDefault` tinyint(4) NOT NULL DEFAULT '0',
  `isActive` tinyint(4) NOT NULL DEFAULT '0',
  `isDeleted` tinyint(4) NOT NULL DEFAULT '0',
  `createdAt` datetime NOT NULL,
  `modifiedAt` datetime NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `keyname_UNIQUE` (`keyname`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

CREATE TABLE `payments` (
  `payId` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `payDay` int(2) DEFAULT NULL,
  `payAmount` int(6) DEFAULT NULL,
  `payNextdate` date DEFAULT NULL,
  `payStatus` varchar(32) NOT NULL,
  `payComments` text,
  `proUser` varchar(255) NOT NULL,
  `paymentAccountId` int(10) unsigned DEFAULT NULL,
  `lastDepositId` int(10) unsigned DEFAULT NULL,
  `hasNewDepositNotifications` tinyint(3) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`payId`,`proUser`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `posts` (
  `posId` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `posUrl` varchar(255) DEFAULT NULL,
  `posTitle` varchar(255) DEFAULT NULL,
  `posCountry` varchar(2) DEFAULT NULL,
  `posShow` tinyint(3) unsigned DEFAULT '1',
  PRIMARY KEY (`posId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `profile_group` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `user` varchar(255) DEFAULT NULL,
  `password` varchar(255) DEFAULT NULL,
  `owner` varchar(255) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `phone` varchar(45) DEFAULT NULL,
  `panel_message` text,
  PRIMARY KEY (`id`),
  UNIQUE KEY `name_UNIQUE` (`name`),
  UNIQUE KEY `user_UNIQUE` (`user`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `profiles` (
  `proId` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `proUser` varchar(255) NOT NULL,
  `proEmail` varchar(128) DEFAULT NULL,
  `proVerified` tinyint(3) unsigned DEFAULT NULL,
  `proAccountType` int(10) unsigned DEFAULT '0',
  `proPriority` int(10) unsigned DEFAULT '0',
  `proPassword` varchar(255) NOT NULL,
  `proDomain` varchar(255) NOT NULL,
  `proTheme` varchar(64) NOT NULL,
  `proName` varchar(255) NOT NULL,
  `proGender` varchar(64) NOT NULL,
  `proAge` tinyint(2) unsigned NOT NULL DEFAULT '0',
  `proPhone` varchar(64) NOT NULL,
  `proNumericPhones` varchar(255) DEFAULT NULL,
  `proDays` varchar(64) DEFAULT NULL,
  `proCategories` text,
  `proCity` varchar(64) DEFAULT NULL,
  `proRegion` varchar(64) DEFAULT NULL,
  `proCountry` varchar(45) NOT NULL,
  `proPlace` varchar(128) NOT NULL,
  `proTitle` varchar(255) NOT NULL,
  `proSubtitle` varchar(255) NOT NULL,
  `proSubtitle2` varchar(255) NOT NULL,
  `proAbout` text NOT NULL,
  `proServices` text NOT NULL,
  `proPicture` varchar(255) NOT NULL,
  `proPictureType` varchar(45) DEFAULT 'vertical',
  `proVerifiedPictures` tinyint(4) NOT NULL DEFAULT '0',
  `proAbsent` tinyint(3) unsigned DEFAULT '0',
  `proAbsentMessage` varchar(255) DEFAULT NULL,
  `proPanelMessage` text,
  `proDeleted` tinyint(3) unsigned DEFAULT '0',
  `proCreatedDate` datetime DEFAULT NULL,
  `proModifiedDate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`proId`,`proUser`) USING BTREE,
  UNIQUE KEY `proUser_UNIQUE` (`proUser`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `profiles_restores` (
  `proUser` varchar(255) NOT NULL,
  `proRestoreCode` varchar(45) NOT NULL,
  `proRestoreHash` varchar(255) NOT NULL,
  `proRestoreCreated` datetime NOT NULL,
  `proRestoreExpires` datetime NOT NULL,
  PRIMARY KEY (`proUser`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `regions` (
  `regId` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `regName` varchar(64) NOT NULL,
  `regCountry` varchar(2) DEFAULT NULL,
  `regActive` tinyint(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`regId`),
  KEY `regName` (`regName`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `resellers` (
  `resId` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `resName` varchar(64) NOT NULL,
  `resPassword` varchar(64) NOT NULL,
  PRIMARY KEY (`resId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `suscribers` (
  `susId` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `susEmail` varchar(255) DEFAULT NULL,
  `susHash` varchar(60) DEFAULT NULL,
  `susCountry` varchar(2) DEFAULT NULL,
  `susDeleted` tinyint(3) unsigned DEFAULT '0',
  PRIMARY KEY (`susId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `temps` (
  `tmpId` int(11) NOT NULL AUTO_INCREMENT,
  `tmpKey` varchar(255) NOT NULL,
  `tmpValue` text,
  `tmpCreated` datetime NOT NULL,
  `tmpModified` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `tmpExpires` datetime DEFAULT NULL,
  PRIMARY KEY (`tmpId`,`tmpKey`),
  UNIQUE KEY `tmpKey_UNIQUE` (`tmpKey`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `wallmessages` (
  `wmesId` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `wmesTitle` varchar(255) NOT NULL,
  `wmesCategory` varchar(255) NOT NULL,
  `wmesCountry` varchar(2) DEFAULT NULL,
  `wmesText` text,
  `wmesDatetime` datetime DEFAULT NULL,
  `wmesSticky` tinyint(3) unsigned DEFAULT '0',
  `wmesReplies` int(10) unsigned DEFAULT '0',
  `wmesDeleted` tinyint(3) unsigned DEFAULT '0',
  `proUser` varchar(255) NOT NULL,
  PRIMARY KEY (`wmesId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `wallreplies` (
  `wrepId` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `wrepText` text,
  `wrepDatetime` datetime DEFAULT NULL,
  `wrepDeleted` tinyint(3) unsigned DEFAULT '0',
  `proUser` varchar(255) NOT NULL,
  `wmesId` int(10) unsigned NOT NULL,
  PRIMARY KEY (`wrepId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
