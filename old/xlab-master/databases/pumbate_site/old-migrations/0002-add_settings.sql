SET SQL_SAFE_UPDATES = 0;

USE pumbate_site;

CREATE TABLE `setting` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `keyname` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `value` longtext COLLATE utf8mb4_unicode_ci NOT NULL COMMENT '(DC2Type:json_object)',
  `metadata` longtext COLLATE utf8mb4_unicode_ci NOT NULL COMMENT '(DC2Type:json_object)',
  `createdAt` datetime NOT NULL,
  `modifiedAt` datetime NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `setting_keyname` (`keyname`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


