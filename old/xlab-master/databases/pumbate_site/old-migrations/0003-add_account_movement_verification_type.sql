SET SQL_SAFE_UPDATES = 0;

USE pumbate_site;

CREATE TABLE `payment_account_verification_type` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `value` VARCHAR(64) NOT NULL,
  `displayName` VARCHAR(128) NOT NULL,
  PRIMARY KEY (`value`),
  UNIQUE INDEX `id_UNIQUE` (`id` ASC));

INSERT INTO `payment_account_verification_type` (`value`, `displayName`) VALUES 
('manual', 'Manual'),
('manual_by_receipt', 'Manual con comprobante'),
('automatic_by_bot', 'Automática por bot');


ALTER TABLE `payment_account` 
ADD COLUMN `accountMovementVerificationType` VARCHAR(64) NOT NULL DEFAULT 'manual' AFTER `accountAlias`;

ALTER TABLE `payment_account` 
ADD CONSTRAINT `payment_account_verification_type`
  FOREIGN KEY (accountMovementVerificationType)
  REFERENCES payment_account_verification_type(`value`)
  ON DELETE NO ACTION
  ON UPDATE NO ACTION;


UPDATE `payment_account` SET `accountMovementVerificationType` = 'automatic_by_bot' WHERE 
`id` IN (2,6);
