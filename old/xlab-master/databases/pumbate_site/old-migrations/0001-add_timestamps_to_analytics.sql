SET SQL_SAFE_UPDATES = 0;

USE pumbate_site;

ALTER TABLE `analytics` 
ADD COLUMN `modifiedAt` DATETIME AFTER `proUser`,
ADD COLUMN `createdAt` DATETIME AFTER `modifiedAt`;
UPDATE `analytics`  SET createdAt=NOW(), modifiedAt=NOW();

DROP TABLE `canalytics`;