<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');


class Administrator_Acl_Model extends CI_Model {


    public function __construct() {
        parent::__construct();
    }

    public function get_by_id($id)
    {
        $this->db->where('id', $id);
        $this->db->limit(1);
        $permissions = $this->db->get('administrator_acl_rule')->result_array();

        if (empty($permissions))
        {
            return null;
        }

        $permissions = $this->castRowsValues($permissions);
        return $permissions[0];
    }

    public function save($data)
    {
        $data = (object) $data;

        if (!property_exists($data, 'id'))
        {
            throw new \Exception("No Administrator Permission Id specified");
        }

        $validProps = [
            "adminId", 
            "permission", 
            "resource",
            "resourceId", 
            "createdAt",
            "modifiedAt"
        ];

        foreach ($validProps as $fieldName)
        {
            if (property_exists($data, $fieldName))
            {
                $this->db->set($fieldName, $data->{$fieldName});
            }
        }

        $currentDt = date('Y-m-d H:i:s');
        $this->db->set('modifiedAt', "'" . $currentDt . "'", false);

        if ($data->id)
        {
            $this->db->where('id', $data->id);
            $this->db->limit(1);
            $this->db->update('administrator_acl_rule');
        }
        else
        {
            $this->db->set('createdAt', "'" . $currentDt . "'", false);
            $this->db->insert('administrator_acl_rule');
            $data->id = $this->db->insert_id();
        }
       
        return $data;
    }


    public function findBy ($options)
    {
        $options = (empty($options)) ? [] : $options; 

        if (!empty($options['adminId']))
        {
            $this->db->where('adminId', $options['adminId']);
        }

        if (!empty($options['resource']))
        {
            $this->db->where('resource', $options['resource']);
        }

        if (!empty($options['resourceId']))
        {
            $this->db->where('resourceId', $options['resourceId']);
        }

        $limit = (!empty($options['limit']))? $options['limit'] : 1000;
        $offset =  (!empty($options['offset']))? $options['offset'] : 0;

        $sorting = (!empty($options['sort'])) ? $options['sort'] : 'id desc';
        $this->db->order_by($sorting);

        $this->db->limit($limit, $offset);
        
        $accounts = $this->db->get('administrator_acl_rule')->result_array();
        $accounts = $this->castRowsValues($accounts);

        return $accounts;     
    }


    protected function castRowsValues($rows)
    {
        $castedRows = [];

        foreach ($rows as $row)
        {
            //$row['isActive'] = $this->castBool($row, 'isActive');
            //$row['isDeleted'] = $this->castBool($row, 'isDeleted');
            //$row['isDefault'] = $this->castBool($row, 'isDefault');
            $castedRows[] = $row;
        }

        return $castedRows;
    }

    protected function castBool($row, $field)
    {
        return (strval($row[$field]) === '1') ? true : false;
    }

}