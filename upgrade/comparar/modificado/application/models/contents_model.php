<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');


class Contents_Model extends CI_Model {

	
	public function __construct() {
		parent::__construct();
	}
	
	
	public function get_page_statuses() {
		return array(
			'published' => "Publicada",
			'pending' => "Pendiente",
			'oculta' => "Oculta",
		);		
	}
	
	
	public function get_page_types() {
		return array(
			'page' => "Pagina del Sitio",
			'blog_post' => "Post del Blog",
		);				
	}
	
	
}

/* end of file */
