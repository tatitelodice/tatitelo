<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');


class Analytics_Model extends CI_Model {

	public function __construct() {
		parent::__construct();
	}
	
	
	public function get_analytics($user, $start_date = false, $end_date = false) {
		$data = array();
		
		if (!$start_date) {
			$start_date = date('Y-m-d');
		}
		if (!$end_date) {
			$end_date = date('Y-m-d');
		}
		
		$this->_check_historical($user);
		
		$this->db->where('anaDate >=', $start_date);
		$this->db->where('anaDate <=', $end_date);
		
		$this->db->where('proUser', $user);
		
		$this->db->order_by('anaDate', 'desc');
		
		$results = $this->db->get('analytics');

		if ($results->num_rows()) {
			foreach($results->result_array() as $result_row) {
				$data[$result_row['anaDate']] = array(
					'pageviews' => $result_row['anaPageViews'],
					'unique_pageviews' => $result_row['anaUniquePageViews'],
				);
			}
		}
		
		$today = date('Y-m-d');
		
		if (strtotime($end_date) >= strtotime($today)) {
			/* add today analytics */
			$data = array_merge( array( $today => $this->get_today_analytics($user)), $data);
		}
		
		return $data;
	}
	
	public function get_today_analytics($user) {
		$data = array(
			'pageviews' => 0,
			'unique_pageviews' => 0,
		);
		
		$this->db->where('proUser', $user);
		$this->db->order_by('canDatetime', 'desc');
		$this->db->limit(1);
		$result = $this->db->get('canalytics');
		
		if ($result->num_rows) {
			$first_row_data = $result->row_array();
			
			/* if current analytics are 5 minutes old, update them */
			if ( time() - strtotime($first_row_data['canDatetime']) > 60 * 5 ) {
				$this->db->where('proUser', $user);
				$this->db->delete('canalytics');
				
				$data = $this->_fill_today_analytics($user);
			} else {
				$data = array(
					'pageviews' => $first_row_data['canPageViews'],
					'unique_pageviews' => $first_row_data['canUniquePageViews'],
				);
			}
		} else {
			$data = $this->_fill_today_analytics($user);
		}
		
		return $data;
		
	}
	
	private function _fill_today_analytics($user) {
		$today = date('Y-m-d');
		
		$google_data = $this->_get_google_data($user, $today, $today);
		$data = array_shift($google_data);
				
		$this->db->set('proUser', $user);
		$this->db->set('canDatetime', "'".date('Y-m-d H:i:s')."'", false);
		$this->db->set('canPageViews', $data['pageviews']);
		$this->db->set('canUniquePageViews', $data['unique_pageviews']);
		$this->db->insert('canalytics');
		
		return $data;
	}
	

	private function _check_historical($user) {
		$fill_historical = true;
		
		$this->db->where('proUser', $user);
		$this->db->order_by('anaDate', 'desc');
		$this->db->limit(1);
		$result = $this->db->get('analytics');
		
		$last_record_date = false;
		
		if ($result->num_rows) {
			$first_row_data = $result->row_array();
			
			$last_record_date = $first_row_data['anaDate'];
			$yesterday_date = date('Y-m-d', strtotime('-1 day'));
			
			$fill_historical = $last_record_date != $yesterday_date;
			
			//start filling from next day that has not any record
			$last_record_date = date('Y-m-d', strtotime('+1 day', strtotime($last_record_date)));
		}
		
		if ($fill_historical) {
			$data = $this->_fill_historical($user, $last_record_date);
		}
		
	}
	
	private function _fill_historical($user, $start_date = false) {
		
		//yesterday
		$end_date_timestamp = strtotime('-1 day');
		$end_date = date('Y-m-d', $end_date_timestamp);
		
		/* get join date and set it as start_date */
		if (!$start_date) {
			$start_date = $end_date;
			
			/* if user has registration date, start from it */
			$this->db->select('proCreatedDate');
			$this->db->where('proUser', $user);
			$result = $this->db->get('profiles');

			if ($result->num_rows()) {
				$row_data = $result->row_array();
				//print_r($row_data); die;
				
				$join_timestamp = strtotime($row_data['proCreatedDate']);
				
				/* FIX GOOGLE START DATE BUG WHEN START DATE IS LARGER THAN END DATE */
				if ($join_timestamp < $end_date_timestamp) {
					$start_date = date('Y-m-d', $join_timestamp);
				}
			}			
		}
		
		//echo $start_date . ' ' .$end_date; die;
		$google_data = $this->_get_google_data($user, $start_date, $end_date);
		
		#print_r($google_data);
		#die;
		
		foreach ($google_data as $date => $data) {
			$this->db->set('proUser', $user);
			$this->db->set('anaDate', "'$date'", false);
			$this->db->set('anaPageViews', $data['pageviews']);
			$this->db->set('anaUniquePageViews', $data['unique_pageviews']);
			$this->db->insert('analytics');
		} 
		//print_r($google_data); die;
	}
	
	
	private function _get_google_data($user, $start_date, $end_date) {
		
		$data = array();
		
		$this->load->library('analytics');
		$this->analytics->startClient();
		$res = $this->analytics->getResultsByPage('~^.*?/' . $user . '/?$', $start_date, $end_date);
		
		//print_r($res); 
		
		if ($res && isset($res->rows)) {
			foreach ($res->rows as $row) {
				
				if (count($row) == 3 && preg_match('!(2\d\d\d)(\d\d)(\d\d)!', $row[0], $mdate)) {
					$formated_date = $mdate[1] . '-' . $mdate[2] . '-' . $mdate[3];
					
					$data[$formated_date] = array(
						'pageviews' => $row[1],
						'unique_pageviews' => $row[2],
					);
				}
			}
		}
		
		return $data;
	}
	
	
	public function get_historical_visits_count($user) {
		$this->db->select('SUM(anaPageViews) as pageviews');
		$this->db->where('proUser', $user);
		$result = $this->db->get('analytics');
		
		return intval($result->row()->pageviews);
	}

}
	
/* end of file */
