<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');


class Payment_Account_Model extends CI_Model {

    protected $accounts;
    protected $defaultAccount;


    public function __construct() {
        parent::__construct();

        $this->defaultAccount = $this->resolveDefaultAccount();
    }

    protected function resolveDefaultAccount()
    {
        $activeAccounts = $this
            ->get_all()
            ->filter(function($item)
            {
                return $item['isActive'];
            })
        ;
        
        $explicitAccount = $activeAccounts
            ->filter(function($item) { return $item['isDefault']; })
            ->first()
        ;

        if ($explicitAccount)
        {
            return $explicitAccount;
        }

        return $activeAccounts->first();
    }


    public function save($data)
    {
        $data = (object) $data;

        if (!property_exists($data, 'id'))
        {
            throw new \Exception("No PaymentAccountId specified");
        }

        $validProps = [
            "keyname", 
            "name", 
            "accountService", 
            "accountId", 
            "accountAlias", 
            "priority", 
            "monthlyLimit", 
            "currentBalance", 
            "currencyIsoCode", 
            "tags", 
            "attributes", 
            "isDefault", 
            "isActive", 
            "isDeleted", 
            "createdAt", 
            "modifiedAt"
        ];

        foreach ($validProps as $fieldName)
        {
            if (property_exists($data, $fieldName))
            {
                $this->db->set($fieldName, $data->{$fieldName});
            }
        }

        $currentDt = date('Y-m-d H:i:s');
        $this->db->set('modifiedAt', "'" . $currentDt . "'", false);

        if ($data->id)
        {
            $this->db->where('id', $data->id);
            $this->db->limit(1);
            $this->db->update('payment_account');
        }
        else
        {
            $this->db->set('createdAt', "'" . $currentDt . "'", false);
            $this->db->insert('payment_account');
            $data->id = $this->db->insert_id();
        }
       
        return $data;
    }

    public function get_all($options=false)
    {
        return $this->getAll($options);
    }

    public function getAll($options=false)
    {
        $options = (empty($options)) ? [] : $options; 

        if (empty($this->accounts))
        {
            $accounts = $this->get_all_from_db($options);
            $this->accounts = collect($accounts);
        }

        return $this->accounts;
    }


    public function findBy($conditions)
    {
        $res = $this->getAll();

        foreach ($conditions as $field => $value)
        {
            $strict = true; // prevent string / int mismatch

            if (is_array($value))
            {
                $res = $res->whereIn($field, $value, $strict);
            }
            else
            {
                $res = $res->where($field, $value, $strict);
            }
        }

        return $res;
    }


    public function get_all_from_db($options)
    {
        if (!empty($options['keyname']))
        {
            $this->db->where('keyname', $options['keyname']);
        }

        if (!empty($options['accountId']))
        {
            $this->db->where('accountId', $options['accountId']);
        }

        if (!empty($options['accountService']))
        {
            $this->db->where('accountService', $options['accountService']);
        }

        $limit = (!empty($options['limit']))? $options['limit'] : 1000;
        $offset =  (!empty($options['offset']))? $options['offset'] : 0;

        $sorting = (!empty($options['sort'])) ? $options['sort'] : 'id desc';
        $this->db->order_by($sorting);

        $this->db->limit($limit, $offset);
        
        $accounts = $this->db->get('payment_account')->result_array();
        $accounts = $this->castRowsValues($accounts);
        
        return $accounts;     
    }


    public function get_total_balance_by_account($options)
    {
        $this->db->select('pa.*, SUM(depAmount) as totalDepositsBalance');
        
        $this->db->from('deposits dp');
        $this->db->join('payment_account pa', 'pa.id = dp.paymentAccountId');
        
        $this->db->where('dp.depDeleted', 0);

        if (!empty($options['end_date']))
        {
            $this->db->where('dp.depDate <=', $options['end_date']);
        }

        if (!empty($options['start_date']))
        {
            $this->db->where('dp.depDate >=', $options['start_date']);
        }
                
        $this->db->group_by('paymentAccountId');
    
        $accounts = $this->db->get()->result_array();
        $accounts = $this->castRowsValues($accounts);

        return $accounts;  
    }


    public function attach_total_balance($paymentAccounts, $options)
    {
        $balance = $this->get_total_balance_by_account($options);

        $balanceById = collect($balance)->keyBy('id');

        $paymentAccounts = collect($paymentAccounts)->map(function($item) use ($balanceById)
        {
            $item['totalDepositsBalance'] = 0;
            $balanceItem = $balanceById->get($item['id']);

            if ($balanceItem)
            {
                $item['totalDepositsBalance'] = $balanceItem['totalDepositsBalance'];
            }

            return $item;
        });

        return $paymentAccounts;
    }


    protected function castRowsValues($rows)
    {
        $castedRows = [];

        foreach ($rows as $row)
        {
            $row['isActive'] = $this->castBool($row, 'isActive');
            $row['isDeleted'] = $this->castBool($row, 'isDeleted');
            $row['isDefault'] = $this->castBool($row, 'isDefault');
            $castedRows[] = $row;
        }

        return $castedRows;
    }

    protected function castBool($row, $field)
    {
        return (strval($row[$field]) === '1') ? true : false;
    }


    public function get_by_id($id)
    {
        return $this->get_all()->where('id', $id)->first();
    }


    public function get_by_keyname($accountKeyname)
    {
        return $this->get_all()->where('keyname', $id)->first();
    }


    public function get_default_account_for_profile($profile)
    {
        return $this->defaultAccount;
    }

}