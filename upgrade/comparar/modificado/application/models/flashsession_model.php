<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');


class Flashsession_Model extends CI_Model {
	
	protected $key;	
	protected $vars;
	protected $exists;
	
	protected $default_encrypt_key = 'PAD3N7R0';
	protected $default_cipher = 'AES-256-CBC';
	//openssl_random_pseudo_bytes(16)
	//openssl_cipher_iv_length('');
	protected $default_iv = "78f12b598fa9ee59ea7ab5dbf5743263";
	
	public function __construct() {
		parent::__construct();
		
		if(!$this->load->is_loaded('session')) {
			$this->load->library('session');
		} 
		
		if(!$this->load->is_loaded('temp_model')) {
			$this->load->model("temp_model");
		} 		
		
		$this->_initialize();
	}
	
	private function _initialize() {
		$this->exists = false;
		$this->key = $this->session->userdata('fsk');
		$this->vars = array();
		
		if ($this->key) {
			$db_key = $this->get_db_key();
			
			$this->temp_model->delete_expired_temp($db_key);
			$temp = $this->temp_model->get_temp($db_key);
			
			$temp_vars = json_decode($temp["value"], true);
			
			if (is_array($temp_vars)) {
				$this->vars = $temp_vars;
				$this->exists = true;
			} else {
				$this->temp_model->delete_temp($db_key);
			}	
		}
	}
	
	private function get_db_key() {
		return "FSK::" . $this->key;
	}
	
	
	public function exists() {
		return $this->exists;
	}
	
	public function get_var($name) {
		if (isset($this->vars[$name])) {
			return $this->vars[$name];
		} else {
			return false;
		}
	}
	
	public function get_encrypted_var($name) {
		$encrypted_value = $this->get_var($name);
		$decrypted_value = $encrypted_value;
		
		if ($encrypted_value) {
			//$this->load->library('encrypt');			
			//$this->encrypt->set_cipher(MCRYPT_RIJNDAEL_256);
			//$decrypted_value = $this->encrypt->decode($encrypted_value, $this->default_encrypt_key);
			$encrypted_value = hex2bin($encrypted_value);
			
			$decrypted_value = openssl_decrypt(
				$encrypted_value
			,	$this->default_cipher
			,	$this->default_encrypt_key
			,	OPENSSL_RAW_DATA
			,	hex2bin($this->default_iv)
			);
		}
		
		return $decrypted_value;
	}
		
	
	public function set_var($name, $value) {
		$this->vars[$name] = $value;
	}
	
	public function set_encrypted_var($name, $value) {
		//$this->load->library('encrypt');
		//$this->encrypt->set_cipher(MCRYPT_RIJNDAEL_256);
		//$encrypted_value = $this->encrypt->encode($value, $this->default_encrypt_key);
		
		$encrypted_value = openssl_encrypt(
			$value
		,	$this->default_cipher
		,	$this->default_encrypt_key
		,	OPENSSL_RAW_DATA
		,	hex2bin($this->default_iv)
		);

		$encrypted_value = bin2hex($encrypted_value);

		$this->set_var($name, $encrypted_value);
	}
	
	
	public function unset_var($name) {
		if (isset($this->vars[$name])) {
			unset($this->vars[$name]);
		}
	}
	
	public function save($extend_days = false) {
		$json_vars = json_encode($this->vars);
		
		if ($json_vars) {
			$expire_date = false;
			
			if (intval($extend_days) > 0) {
				$expire_date = date('Y-m-d H:i:s', strtotime("+" . $extend_days . " day"));
			}
			
			if ($this->exists) {
				$this->temp_model->update_temp($this->get_db_key(), $json_vars, $expire_date);
				
			} else {
				$new_key = sha1( "PADENTRO" . time() . $this->input->ip_address());
				$this->session->set_userdata('fsk', $new_key);
				$this->key = $new_key;	
				
				$this->temp_model->insert_temp($this->get_db_key(), $json_vars, $expire_date);
				$this->exists = true;
			}
		}		
	}

	
}

/* end of file */
