<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');


class Security_Model extends CI_Model {
	
	public function __construct() {
		parent::__construct();
		$this->load->model('temp_model');
	}
	
	public function on_limit_request($request_name, $limit) {
		$limit_requests = false;
		
		$addr = $this->input->ip_address();
		
		if ($addr) {
			$temp_key = $request_name . '::' . date('Ymd') . '::' . $addr;
			
			$temp = $this->temp_model->get_temp($temp_key);
			
			if ($temp && intval($temp['value']) >= $limit) {
				$limit_requests = true;
			}
		}
		
		return $limit_requests;
	}
	
	public function penalize_request($request_name) {
		$addr = $this->input->ip_address();
		
		if ($addr) {
			$temp_key = $request_name . '::' . date('Ymd') . '::' . $addr;
			
			$temp = $this->temp_model->get_temp($temp_key);
			
			if ($temp) {
				$new_val = intval($temp['value']) + 1;
				$this->temp_model->update_temp($temp_key, $new_val);
			} else {
				$this->temp_model->insert_temp($temp_key, '1', date('Y-m-d',strtotime('+2 day')) . ' 00:00:00');
			}
		}
	}
	
	
	public function lock_request($request_name, $minutes) {
		$addr = $this->input->ip_address();
		
		if ($addr) {
			$temp_key = $request_name . '::lock::' . $addr;
			
			$this->temp_model->delete_temp($temp_key);
			$this->temp_model->insert_temp($temp_key, '', date('Y-m-d H:i:s',strtotime('+'.$minutes.' minute')));
		}
	}
	
	
	public function unlock_request($request_name) {
		$addr = $this->input->ip_address();
		
		if ($addr) {
			$temp_key = $request_name . '::lock::' . $addr;
			$this->temp_model->delete_temp($temp_key);
		}
	}
	
	
	public function on_request_lock($request_name) {
		$addr = $this->input->ip_address();
		
		if ($addr) {
			$temp_key = $request_name . '::lock::' . $addr;
			
			$this->temp_model->delete_expired_temp($temp_key);
			$temp = $this->temp_model->get_temp($temp_key);
			
			if ($temp) {
				return $temp["expires"];
			}
		}
		
		return false;
	}
	
}

/* end of file */
