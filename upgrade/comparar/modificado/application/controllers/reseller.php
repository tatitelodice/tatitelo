<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Reseller extends CI_Controller {

	public function __construct() {
		parent::__construct();
		$this->load->library('session');
	}


	public function index() {
		if ($this->session->userdata('reseller_login')) {
			$this->_in_login();
		} else {
			$this->login();
		}
	}
	
	private function _in_login() {
		$subheader = array(
			'title' => '',
			'subtitle' => '',
			'btn_back' => '<a style="float: right;" class="btn" href="' . site_url('') . '">Volver a principal de Pumbate.com</a>'
		);
		
		$this->load->view('site/padmin/header', array('title' => 'Reseller - Pumbate Escorts'));
		$this->load->view('site/padmin/subheader', $subheader);
		
		$reseller_data = array(
			'reseller' => $this->session->userdata('reseller_name'),
		);
		
		$this->load->view('site/reseller', $reseller_data);
		
		$this->load->view('site/padmin/footer');
	}

	
	public function login() {		
		$subheader = array(
			'title' => '',
			'subtitle' => '',
			'btn_back' => '<a style="float: right;" class="btn" href="' . site_url('') . '">Salir del Panel</a>'
		);
		
		$login = array(
			'error' => '',
		);
		
		if ($this->input->post('send')) {
			$this->form_validation->set_message('required', 'No olvide ingresar su %s');
			$this->form_validation->set_rules('name', 'Nombre', 'trim|required');
			$this->form_validation->set_rules('password', 'Contraseña', 'trim|required');
			
			if ($this->form_validation->run() !== false) {
				$result = $this->db->get_where('resellers', array(
					'resName' => $this->input->post('name'),
					'resPassword' => sha1($this->input->post('password')),
				));
				
				//print_r($result->row_array()); die;
				
				$activeLogin = $result->num_rows() > 0;
				
				if ( $activeLogin ) {
					$this->session->set_userdata('reseller_login', true);
					$this->session->set_userdata('reseller_name', $this->input->post('name'));
					redirect('reseller');
					
				} else {
					$login['error'] = '<div class="alert alert-error input-large" style="text-align: center;"><button class="close" data-dismiss="alert">×</button>Su nombre ó contraseña ingresados no son correctos.</div>';
				}
			}
		}
		
		
		$this->load->view('site/padmin/header', array('title' => 'Reseller - Pumbate Escorts' ));
		$this->load->view('site/padmin/subheader', $subheader);
		$this->load->view('site/reseller_login', $login);
		$this->load->view('site/padmin/footer');
	}
	
	
	public function logout() {
		/*$this->session->sess_destroy();*/
		$this->session->set_userdata('reseller_login', false);
		redirect('reseller');
	}
	
	
}

/* end of file */