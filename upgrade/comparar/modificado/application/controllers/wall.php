<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Wall extends CI_Controller {

	protected $profile = false;
	
	protected $admin_login = false;
	protected $admin_country = false;
	
	protected $categories = array(
		'general' => 'General',
		'lugares' => 'Lugares',
		'clientes' => 'Alertas sobre clientes peligrosos',
		'pumbate' => 'Avisos del Equipo de Pumbate.com',
	);
	
	public function __construct() {
		parent::__construct();
		$this->load->library('country');
		$this->load->model('profile_model');
		$this->load->model('admin_model');
		
		$this->_check_session();
	}
	
	private function _check_session() {
		
		if ($this->admin_model->login_exists()) {
			$this->admin_login = true;
			$this->admin_country = $this->admin_model->get_country();
			
		} else {
			$user = $this->profile_model->get_logged_user();
		
			if ($user && $this->profile_model->login_exists($user)) {
				$this->profile = $this->profile_model->get_profile($user);		
			} else {
				redirect('mipagina');
			}
		}
	}
	
	public function index() {
		$this->category();
	}
	
	public function category($category = false) {
		
		if ( !$this->admin_login && ( $this->profile !== false && $this->profile['verified'] == 0)) {
			$this->_no_permission();
			return false;
		}
			
		if ($category && !isset($this->categories[$category])) {
			$category = false;
		}

		$list_data = array();
		$list_data['categories'] = $this->categories;
		$list_data['category'] = $category;
		$list_data['user'] = $this->profile['user'];
		$list_data['admin'] = $this->admin_login;

		$list_data['messages'] = $this->_get_messages($category);

		$this->load->view('site/header');
		$this->load->view('site/wall/list', $list_data);	
		$this->load->view('site/footer');
		
	}
	
	public function new_message() {
		
		if ( !$this->admin_login && ( $this->profile !== false && $this->profile['verified'] == 0)) {
			$this->_no_permission();
			return false;
		}
		
		$this->form_validation->set_message('required', '%s no puede quedar vacío.');
		$this->form_validation->set_message('min_length', 'El contenido del campo %s es demasiado corto.');
		
		$this->form_validation->set_rules('title', 'Titulo', 'trim|required|min_length[4]');
		$this->form_validation->set_rules('text', 'Texto del Mensaje', 'trim|required|min_length[30]');
		
		if ($this->form_validation->run() !== false) {
			$post = $this->input->post();
			
			if ( $this->admin_login ) {
				if ($this->admin_country) {
					$country = $this->admin_country;
				} else {
					$country = $post['country'];
				}
				
				$user = $this->admin_model->get_name() . ' (Equipo Pumbate)';
				
			} else {
				$user = $this->profile['user'];
				$country = $this->profile['country'];
			}
			
			
			$this->db->set('wmesTitle', $post['title']);
			$this->db->set('wmesText', $post['text']);
			$this->db->set('wmesCategory', $post['category']);			
			$this->db->set('wmesCountry', $country);
			
			$this->db->set('wmesDatetime', "'" . date('Y-m-d H:i:s') . "'", false);
			$this->db->set('proUser', $user);
			
			if ($this->db->insert('wallmessages')) {
				redirect('wall/new_message_success');
				return true;
			}
			
		} 
		
		$view_data = array();
		$view_data['admin'] = $this->admin_login;
		$view_data['country'] = $this->admin_country;
		$view_data['country_options'] = $this->country->get_countries();
		
		$this->load->view('site/header');
		$this->load->view('site/wall/new_message', $view_data);	
		$this->load->view('site/footer');
	}
	
	public function new_message_success() {
		$this->load->view('site/header');
		$this->load->view('site/wall/new_message_success');	
		$this->load->view('site/footer');
	}
	
	private function _no_permission() {
		$this->load->view('site/header');
		$this->load->view('site/wall/no_permission');	
		$this->load->view('site/footer');
	}

	private function _get_messages($category = false) {
		
		$messages = array();
		
		$this->db->select('wmesId,wmesTitle,wmesCategory,wmesText,wmesDatetime,wmesCountry,wmesReplies,wmesDeleted,wmesSticky,wallmessages.proUser,proGender');
		$this->db->join('profiles', 'profiles.proUser = wallmessages.proUser', 'left outer');
		
		if ($this->admin_login && $this->admin_country) {
			$this->db->where('wmesCountry', $this->admin_country);
		} elseif ($this->profile) {
			$this->db->where('wmesCountry', $this->profile['country']);
		}

		if ($category) {
			$this->db->where('wmesCategory', $category);
		}
		
		if (!$this->admin_login) {
			$this->db->where('wmesDeleted', 0);
		}
		
		$this->db->limit(200);
		$this->db->order_by('wmesSticky desc, wmesId desc');
		
		
		$results = $this->db->get('wallmessages');
		
		if ($results->num_rows()) {
			$this->load->library('gender');
			$this->load->library('sdate');
			$this->load->helper('text');
			
			$results_array = $results->result_array();
			
			foreach ($results_array as $row) {

				$message = array();
				$message['id'] = $row['wmesId'];
				$message['title'] = $row['wmesTitle'];
				$message['text'] = character_limiter($row['wmesText'], 200);
				$message['date'] =  $this->sdate->formatoYMDaLiteral($row['wmesDatetime'], true);
				
				$message['deleted'] = $row['wmesDeleted'] > 0;
				$message['sticky'] = $row['wmesSticky'] > 0;
				
				$message['country'] = '';
				
				if (isset( $row['wmesCountry'] )) {
					$message['country'] = $row['wmesCountry'];
				}
				
				$message['gender_symbol'] = '';
				
				if ($row['proGender']) {
					$message['gender_symbol'] = $this->gender->get_symbol($row['proGender']); 
				}

				$message['replies'] = $row['wmesReplies'];
				$message['category'] = $row['wmesCategory'];
				$message['user'] = $row['proUser'];
				$message['url'] = site_url('wall/message/' . $row['wmesId']);
				
				$messages[] = $message;
			}
		}
		
		return $messages;
	}
	
	public function _post_comment($parent_message) {
		
		$this->form_validation->set_message('required', '%s no puede quedar vacío.');
		$this->form_validation->set_message('min_length', 'El contenido del campo %s es demasiado corto.');

		$this->form_validation->set_rules('id', 'Id del Mensaje al que se responde', 'required|integer');
		$this->form_validation->set_rules('text', 'Texto del Comentario', 'trim|required|min_length[10]');

		if ($this->form_validation->run() !== false) {
			$post = $this->input->post();
			
			$this->db->set('wrepText', $post['text']);
			$this->db->set('wrepDatetime', "'" . date('Y-m-d H:i:s') . "'", false);
			
			if ( $this->admin_login ) {
				$user = $this->admin_model->get_name() . ' (Equipo Pumbate)';
			} else {
				$user = $this->profile['user'];
			}
			
			$this->db->set('proUser', $user);
			
			$this->db->set('wmesId', $parent_message['id']);
			$this->db->set('wrepDeleted', 0);
			
			$success = $this->db->insert('wallreplies');
			
			if ($success) {
				$this->db->set('wmesReplies', $parent_message['replies'] + 1);
				$this->db->where('wmesId', $parent_message['id']);
				$this->db->update('wallmessages');
			}
			
			redirect('wall/new_comment_success/' . $parent_message['id']);
			return true;
		}
	}
	
	
	public function new_comment_success($id = false) {
		$success = false;
		
		if ($id) {
			$this->db->select('wmesTitle');
			$this->db->where('wmesId', $id);
			$results = $this->db->get('wallmessages');

			if ($results->num_rows()) {
				$row = $results->row_array();
				
				$view_data = array();
				$view_data['title'] = $row['wmesTitle'];
				$view_data['id'] = $id;
				$view_data['url'] = site_url('wall/message/' . $id . '#last');
				
				$this->load->view('site/header');
				$this->load->view('site/wall/new_comment_success', $view_data);	
				$this->load->view('site/footer');
				
				$success = true;
			}
		}
		
		if ($success === false) {
			redirect('wall');
		}	
	}
	
	
	public function message($id) {
				
		$message_data = array();
		
		$message_data['id'] = $id;
		$message_data['message'] = false;
		$message_data['admin'] = $this->admin_login;
		$message_data['comments'] = array();
		
		$this->db->select('wmesId,wmesTitle,wmesCategory,wmesText,wmesDatetime,wmesReplies,wmesSticky,wmesDeleted,wallmessages.proUser,proGender');
		$this->db->join('profiles', 'profiles.proUser = wallmessages.proUser', 'left outer');
		
		$this->db->where('wmesId', $id);
		$results = $this->db->get('wallmessages');
		
		if ($results->num_rows()) {
			
			$row = $results->row_array();

			$message = array();
			$message['id'] = $row['wmesId'];
			$message['replies'] = $row['wmesReplies'];
			
			$message['deleted'] = $row['wmesDeleted'] > 0;
			$message['sticky'] = $row['wmesSticky'] > 0;
			
			if ($this->input->post('id')) {
				$this->_post_comment($message);
			}
			
			$this->load->library('gender');
			$this->load->library('sdate');
			
			$message['title'] = $row['wmesTitle'];
			$message['text'] = nl2br($row['wmesText']);
			$message['date'] =  $this->sdate->formatoYMDaLiteral($row['wmesDatetime'], true);
			
			$message['gender_symbol'] = '';
			
			if ($row['proGender']) {
				$message['gender_symbol'] = $this->gender->get_symbol($row['proGender']); 
			}

			$message['category'] = $row['wmesCategory'];
			$message['user'] = $row['proUser'];
			$message['url'] = site_url('wall/message/' . $row['wmesId']);

			$message_data['message'] = $message;
			
			$this->db->select('wrepId,wrepText,wrepDatetime,wrepDeleted,wallreplies.proUser,proGender');
			$this->db->join('profiles', 'profiles.proUser = wallreplies.proUser', 'left outer');

			if (!$this->admin_login) {
				$this->db->where('wrepDeleted', 0);
			}
			
			$this->db->where('wmesId', $id);
			$replies_results = $this->db->get('wallreplies');

			if ($replies_results->num_rows()) {
				$reprows = $replies_results->result_array();
				
				foreach ($reprows as $reprow) {
					$comment = array();
					
					$comment['id'] = $reprow['wrepId'];
					$comment['text'] = nl2br($reprow['wrepText']);
					$comment['date'] =  $this->sdate->formatoYMDaLiteral($reprow['wrepDatetime'], true);
					
					$comment['deleted'] = $reprow['wrepDeleted'] > 0;
					
					$comment['gender_symbol'] = '';
					
					if ($reprow['proGender']) {
						$comment['gender_symbol'] = $this->gender->get_symbol($reprow['proGender']); 
					}
					
					$comment['user'] = $reprow['proUser'];

					$message_data['comments'][] = $comment;				
				}
			}
			
			$this->load->view('site/header');
			$this->load->view('site/wall/message', $message_data);	
			$this->load->view('site/footer');
		} else {
			redirect('wall');
		}
	
	}
	
	public function full_delete($wmesid) {
		
		if ($this->admin_login) {
			$this->db->where('wmesId', $wmesid);
			$this->db->delete('wallreplies');

			$this->db->where('wmesId', $wmesid);
			$this->db->delete('wallmessages');
			
			echo 'El mensaje #' . $wmesid . ' y sus comentarios fueron completamente borrados de la Base de Datos';
		} else {
			echo 'No se hace nada';
		}
		
		echo '<br>' . anchor('wall', 'Volver al Muro');
	}
	
	public function delete_message($id) {
		if ($this->admin_login) {
			$this->db->set('wmesDeleted', 1);
			$this->db->where('wmesId', $id);
			$this->db->update('wallmessages');
		}
		
		redirect('wall');
	}
	
	public function restore_message($id) {
		if ($this->admin_login) {
			$this->db->set('wmesDeleted', 0);
			$this->db->where('wmesId', $id);
			$this->db->update('wallmessages');
		}
		
		redirect('wall');
	}
	
	public function delete_comment($wrepid, $wmesid) {
		if ($this->admin_login) {
			$this->db->set('wrepDeleted', 1);
			$this->db->where('wrepId', $wrepid);
			$this->db->update('wallreplies');
			
			$this->_update_replies($wmesid);
			
			redirect('wall/message/' . $wmesid . '#comm' . $wrepid);
		}
		
		redirect('wall');
	}
	
	public function restore_comment($wrepid, $wmesid) {
		if ($this->admin_login) {
			$this->db->set('wrepDeleted', 0);
			$this->db->where('wrepId', $wrepid);
			$this->db->update('wallreplies');
			
			$this->_update_replies($wmesid);
			
			redirect('wall/message/' . $wmesid . '#comm' . $wrepid);
		}
		
		redirect('wall');
	}
	
	private function _update_replies($wmesid) {
		$this->db->where('wmesId', $wmesid);
		$this->db->where('wrepDeleted', 0);
		$replies = $this->db->get('wallreplies')->num_rows();
		
		if ($replies) {
			$this->db->where('wmesId', $wmesid);
			$this->db->set('wmesReplies', $replies);
			$this->db->update('wallmessages');
		}
	}
	
	public function stick_message($id) {
		if ($this->admin_login) {
			$this->db->set('wmesSticky', 1);
			$this->db->where('wmesId', $id);
			$this->db->update('wallmessages');
		}
		
		redirect('wall');
	}
	
	public function unstick_message($id) {
		if ($this->admin_login) {
			$this->db->set('wmesSticky', 0);
			$this->db->where('wmesId', $id);
			$this->db->update('wallmessages');
		}
		
		redirect('wall');
	}
	
	
}




/* End of file wall.php */