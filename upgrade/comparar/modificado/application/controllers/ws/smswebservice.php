<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class SmsWebService extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->model('profile_model');
        $this->load->model('customer_message_model');
    }

    public function index() {
        $httpMethod = $this->input->server('REQUEST_METHOD');
        if ('POST' === $httpMethod) return $this->inbox_post();
        $this->inbox_get();
    }

    public function outbox($id=false) {
        $httpMethod = $this->input->server('REQUEST_METHOD');
        if ('POST' === $httpMethod) return $this->outbox_post($id);
        if ('PUT' === $httpMethod) return $this->outbox_put($id);
        $this->outbox_get($id);
    }

    private function inbox_get() {
        $out = array();
        $out['success'] = true;
        $out['message'] = 'running';
        $this->_to_output($out);
    }

    /*
    {
        "uuid": "f1f64593-6caa-11e6-afa6-000c295630e4"
    ,   "inboxStatus": "undefined"
    ,   "messageText": "Don't forget the marshmallows!"
    ,   "recipientDatetime": "2016-08-14T15:47:11Z"
    ,   "recipientNumber": "15555215554"
    ,   "senderNumber":"6505551212"
    }
    */
    private function inbox_post() {
        $jsonData = $this->_get_json_input();

        $out = array();

        $requiredFields = array(
            "uuid", "messageText", "senderNumber"
        ,   "recipientNumber", "recipientDatetime"
        );

        $this->_check_json_fields($jsonData, $requiredFields);

        $messageId = null;
        $error = null;

        $existingMessage = $this->_get_existing_message($jsonData->uuid);

        if ($existingMessage)
        {
            $messageId = $existingMessage->id;
        }
        else
        {
            try
            {
                $messageId = $this->customer_message_model->create_inbox_message([
                    'uuid' => $jsonData->uuid
                ,   'message_text' => $jsonData->messageText
                ,   'message_address' => $jsonData->senderNumber
                ,   'system_address' => $jsonData->recipientNumber
                ,   'system_datetime' => $jsonData->recipientDatetime
                ,   'attributes' => [
                        'profiles' => $this->_get_sender_profiles($jsonData->senderNumber)
                    ]
                ]);
            }
            catch (\Exception $ex)
            {
                $error = $ex;
            }
        }

        if ($messageId)
        {
            $out['success'] = true;
            $out['id'] = $messageId;
        }
        else
        {
            $out['success'] = false;
            $out['error'] = 'db_error';

            if ($error) {
                $out['error_message'] = $e->getMessage();
            }
        }

        return $this->_to_output($out);
    }


    private function _get_existing_message($uuid)
    {
        $packedMessageId = hex2bin(str_replace('-', '', $uuid));
        $this->db->where('uuid', $packedMessageId);
        $this->db->limit(1);
        $res = $this->db->get('customer_message');
        return $res->row();
    }


    private function _get_sender_profiles($senderPhone) {
        $profiles = array();

        $this->db->where('proCountry', 'uy');
        $this->db->like('proNumericPhones', $senderPhone);
        $res = $this->db->get('profiles');

        foreach($res->result_array() as $row)
        {
            $profiles[] = array(
                'id' => $row['proId']
            ,   'user' => $row['proUser']
            ,   'gender' => $row['proGender']
            );
        }

        return $profiles;
    }


    private function outbox_get($id=false)
    {
        $out = array();
        $out['success'] = true;
        
        $get_param = $this->input->get();
        $limit = (!empty($get_param['limit']))? $get_param['limit'] : 30; 

        $out['messages'] = $this->_get_outbox_messages(array(
            'limit' => $limit
        ));
        
        $this->_to_output($out);  
    }


    private function outbox_post()
    {
        $jsonData = $this->_get_json_input();

        $out = array();

        $requiredFields = array(
            "messageText", "messageAddress", "messageCategory"
        );

        $this->_check_json_fields($jsonData, $requiredFields);

        $result = null;
        $error = null;

        try
        {
            if (is_string($jsonData->messageAddress))
            {
                $jsonData->messageAddress = preg_split('!\r?\n|\t|,!', $jsonData->messageAddress);
            }

            $result = $this->customer_message_model->create_outbox_message([
                'message_address' => $jsonData->messageAddress
            ,   'message_text' => $jsonData->messageText
            ,   'message_category' => $jsonData->messageCategory
            ]);
        }
        catch (\Exception $ex)
        {
            $error = $ex;
        }

        if ($result)
        {
            $out['success'] = true;
            $out['result'] = $result;
        }
        else
        {
            $out['success'] = false;
            $out['error'] = 'db_error';

            if ($error) {
                $out['error_message'] = $e->getMessage();
            }
        }

        return $this->_to_output($out);
    }


    private function outbox_put($id=false)
    {
        $out = array();
        $out['success'] = false;
        $out['error'] = null;

        $jsonData = $this->_get_json_input();

        $this->_check_json_fields($jsonData, array(
            'queueId', 'outboxStatus'
        ));

        if ($jsonData->outboxStatus === 'sent')
        {
            $res = $this->_set_message_as_sent($jsonData->queueId);
        }
        else
        {
            $res = $this->_set_message_as_sending($jsonData->queueId);
        }

        $out['success'] = $res['success'];
        $out['error'] = $res['error'];

        if ($res['success'])
        {
            $message = $this->customer_message_model->get_message($jsonData->queueId);
            $out['queueId'] = $message->id;
            $out['outboxStatus'] = $message->messageBoxStatus; 
        }
        
        //$out['messages'] = $this->_get_outbox_messages();
        $this->_to_output($out);  
    }

    private function _set_message_as_sent($messageId)
    {
        $result = array( 'error' => null );
        
        $result['success'] = $this->customer_message_model->update_outbox_sms_status(
            $messageId, 'sent', 'sending'
        );

        if (!$result['success'])
        {
            $result['error'] = 'already_sent';
        }

        return $result;
    }

    private function _set_message_as_sending($messageId)
    {   
        $result = array( 'error' => null );

        $result['success'] = $this->customer_message_model->update_outbox_sms_status(
            $messageId, 'sending', 'pending'
        );

        if (!$result['success'])
        {
            $result['error'] = 'sending_from_another_resource';
        }

        return $result;
    }

    private function _get_outbox_messages($options=array())
    {
        $messages = array();
        $queue_limit = (!empty($options['limit'])) ? $options['limit'] : 30;


        $this->db->where('channel', 'sms');
        $this->db->where('messageBox', 'outbox');
        $this->db->where('messageBoxStatus', 'pending');
        $this->db->where('customerAddress !=', '');

        $this->db->limit($queue_limit);

        $res = $this->db->get('customer_message');

        foreach($res->result_array() as $row)
        {
            $messages[] = array(
                'queueId' => $row['id']
            ,   'uuid' => \Ramsey\Uuid\Uuid::fromBytes  ($row['uuid'])
            ,   'targetNumber' => $row['customerAddress']
            ,   'messageText' => $row['messageText']
            ,   'outboxStatus' => $row['messageBoxStatus']
            );
        }

        return $messages;
    }


    private function _check_json_fields($jsonData, $requiredFields)
    {
        foreach ($requiredFields as $f)
        {
            if (!property_exists($jsonData, $f))
            {
                $this->_to_output(array(
                    'success' => false
                ,   'error' => $f . "_required"
                ));
            };
        }
    }


    private function _get_json_input()
    {
        $jsonData = json_decode(file_get_contents('php://input'));

        if (!is_object($jsonData)) {
            $this->_to_output(array(
                'success' => false
            ,   'error' => 'invalid_json'
            ));
        }

        return $jsonData;
    }

    private function _to_output ($data) {
        header('Content-Type: application/json');
        echo json_encode($data);
        exit();
    }

}

/* eof */