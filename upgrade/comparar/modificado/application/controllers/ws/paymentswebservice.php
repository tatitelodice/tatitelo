<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class PaymentsWebService extends CI_Controller {

    public function __construct()
    {
        parent::__construct();
        $this->load->model('payment_account_model');
        $this->load->library('billing/deposit_match_service');
    }


    public function index() {
        $httpMethod = $this->input->server('REQUEST_METHOD');

        if ('POST' === $httpMethod) return $this->post();

        $this->get();
    }

    private function get() {
        $out = array();
        $out['success'] = true;
        $out['message'] = 'running';
        $this->_to_output($out);
    }

    /*
    {
        "uuid": "f1f64593-6caa-11e6-afa6-000c295630e4"
    ,   "inboxStatus": "undefined"
    ,   "messageText": "Don't forget the marshmallows!"
    ,   "recipientDatetime": "2016-08-14T15:47:11Z"
    ,   "recipientNumber": "15555215554"
    ,   "senderNumber":"6505551212"
    }
    */
    private function post() {
        $deposit = json_decode(file_get_contents('php://input'));
        $out = array();

        if (!is_object($deposit)) {
            $out['success'] = false;
            $out['error'] = "invalid_json";
            return $this->_to_output($out);
        }

        $requiredFields = array(
            "externalId", "name", "date", "amount"
        ,   "paymentAccountService", "paymentAccountId"
        );

        foreach ($requiredFields as $f)
        {
            if (!property_exists($deposit, $f))
            {
                $out['success'] = false;
                $out['error'] = $f . "_required";
                return $this->_to_output($out);
            };
        }

        $paymentId = null;
        $error = null;

        $existingPayment = $this->_get_existing_payment($deposit->externalId);

        if ($existingPayment)
        {
            $paymentId = $existingPayment->depId;
        }
        else
        {
            try
            {
                $paymentAccounts = $this->payment_account_model->get_all_from_db([
                    'accountService' => $deposit->paymentAccountService
                ,   'accountId' => $deposit->paymentAccountId
                ,   'limit' => 1
                ]);

                if (empty($paymentAccounts))
                {
                    throw new \Exception(
                        "Non Existent PaymentAccount " . $deposit->paymentAccountService . " "
                    .   $deposit->paymentAccountId
                    );
                }

                $deposit->paymentAccountId = $paymentAccounts[0]['id'];

                $paymentId = $this->_create_payment($deposit);
                $this->deposit_match_service->assign_deposits_to_profiles(
                    date('Y-m-d', strtotime('-1 day'))
                );
            }
            catch (\Exception $ex)
            {
                $error = $ex;
            }
        }

        if ($paymentId)
        {
            $out['success'] = true;
            $out['id'] = $paymentId;
        }
        else
        {
            $out['success'] = false;
            $out['error'] = 'db_error';

            if ($error) {
                $out['error_message'] = $e->getMessage();
            }
        }

        return $this->_to_output($out);
    }


    private function _get_existing_payment($externalId)
    {
        $this->db->where('depExternalId', $externalId);
        $this->db->limit(1);
        $res = $this->db->get('deposits');
        return $res->row();
    }


    private function _create_payment($deposit)
    {
        $this->db->set( 'depExternalId', $deposit->externalId );
        $this->db->set( 'depStatus', 'deposited');

        $this->db->set( 'depDate', "'" . $deposit->date  . "'", false );
        $this->db->set( 'depAmount', $deposit->amount);
        $this->db->set( 'depName', $deposit->name);

        if (property_exists($deposit, 'time'))
        {
            $this->db->set('depositTime', $deposit->time);
        }

        $this->db->set( 'paymentAccountId', $deposit->paymentAccountId );
        
        if (property_exists($deposit, 'concept'))
        {
            $this->db->set( 'depConcept', $deposit->concept);
        }
        else
        {
            $this->db->set( 'depConcept', '');
        }

        $now = date("Y-m-d H:i:s");

        $this->db->set( 'createdAt', "'" . $now  . "'", false );
        $this->db->set( 'modifiedAt', "'" . $now  . "'", false );


        if ($this->db->insert('deposits'))
        {
            return $this->db->insert_id();
        }

        return false;
    }


    private function _to_output ($data) {
        $this->output
        ->set_content_type('application/json')
        ->set_output(json_encode($data));
    }

}

/* eof */