<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Empomar extends CI_Controller {

	public function __construct() {
		parent::__construct();
		$this->load->model('profile_model');
	}	

	public function index() { 
		redirect('');
	}
	
	private function _get_pictures($user, $with_info = false) {
		$pictures = array();
			
		$user_dir = 'userdata/'.$user.'/';
		
		if (is_dir($user_dir)) {
			foreach (scandir($user_dir) as $f) {
				if (preg_match('!\.(jpe?g|png|gif)!i', $f)) {
					$src = base_url('/userdata/'.$user.'/'.$f);

					if ($with_info) {
						$img_info = getimagesize('userdata/'.$user.'/'.$f);

						if ($img_info[0] > $img_info[1]) {
							$type = 'horizontal';	
						} else {
							$type = 'vertical';
						}

						$pictures[] = array(
							'src' => $src,
							'type' => $type,
							'width' => $img_info[0],
							'height' => $img_info[1],
						);
					} else {
						$pictures[] = $src;
					}

				}
			}
		}
		
		return $pictures;
	}
	
	
	public function victima($name = false, $subtitle = false, $subtitle2 = false) {
		$this->db->select('proUser');
		
		$this->db->where('proGender', 'trans');
		$this->db->where('proDeleted', 0);
		$this->db->where('proVerified', 1);
		
		$this->db->order_by('rand()');
		$this->db->limit(1);
		
		$res = $this->db->get('profiles');
		
		if ($res->num_rows()) {
			$row = $res->row_array();
			$user = $row['proUser'];
			
			$profile_data = $this->profile_model->get_profile($user);

			if ($profile_data !== false) {

				$pictures = $this->_get_pictures($user, true);

				$theme = $profile_data['theme'];

				$site_data = array(
					'common_path' =>  base_url('assets/themes/common/') . '/',
					'path' => base_url('assets/themes/'.$theme). '/',
					'name' => $profile_data['name'],
					'title' => $profile_data['title'],
					'subtitle' => $profile_data['subtitle'],
					'subtitle2' => $profile_data['subtitle2'],
					'phone' => $profile_data['phone'],
					'phone_ad' => '<br><span class="label label-pumbate-phone">Sentite como en casa y no olvides comentarme que me llamas de <a href="'.site_url().'">Pumbate.com</a> :)</span>',
					'age' => $profile_data['age'],
					'location' => $profile_data['city'] . ', ' . $profile_data['region'],
					'place' => $profile_data['place'],
					'about' => $profile_data['about'],
					'services' => $profile_data['services'],
					'pictures' => $pictures,
				);
				
				if ($name) {
					$name = ucwords(urldecode($name));
					$site_data['name'] = $name;
					$site_data['title'] = $name;
				}
				
				if ($subtitle) {
					$site_data['subtitle'] = urldecode($subtitle);
				}
				
				if ($subtitle2) {
					$site_data['subtitle2'] = urldecode($subtitle2);
				}

				$this->load->view('themes/' .  $theme . '/header.php', $site_data);
				$this->load->view('themes/' .  $theme . '/body.php', $site_data);
				$this->load->view('themes/' .  $theme . '/footer.php', $site_data);
				//print_r($site_data); die;

			} else {
				redirect('');
			}
		}
		
		
	}
	
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */