<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Billing_Controller extends CI_Controller {

	public function __construct()
    {
		parent::__construct();

        $this->load->helper('directory');
        $this->load->helper('directorycopy');
        $this->load->helper('profile_helper');

		$this->load->model("admin_model");
		$this->load->model('profile_model');
        $this->load->model('log_model');
        $this->load->model('customer_message_model');
        $this->load->model('deposit_model');
        $this->load->model('payments_model');
		
		$this->load->library('contact');
        $this->load->library('billing/deposit_match_service');
        $this->load->library('billing/deposit_notification_service');
        $this->load->library('profile/profile_billing_status_service');
	}	

    /* 
    @route /profile-panel/billing
    */
    public function index($user=false)
    {
        $this->new_deposit($user);
	}


    /*
        @route /profile-panel/billing/get_status/$user
    */
    public function get_status($user=false)
    {
        $user = $this->check_and_get_user($user);
        $due_days = 0;
        $notify_due_date = false;

        if (!$user)
        {
            return $this->render_json(['error' => 'wrong user']);
        }

        $data = $this->profile_billing_status_service->get_billing_status($user);

        $this->render_json($data);
    }

    /*
    @route /profile-panel/billing/new_deposit/$user
    */
    public function new_deposit($user=false)
    {
        $user = $this->check_and_get_user($user);

        if (!$user)
        {
            return redirect('mipagina');
        }

        $profile = $this->profile_model->get_profile($user);

        $phoneNumbers = $profile['phoneNumbers'];
        $phone_default = (!empty($phoneNumbers)) ? $phoneNumbers[0] : '';

        $payment_status = $this->payments_model->get_profile_payments_status($user);
        $payment_account = $payment_status['payment_account'];

        $view_data = [
            'title' => 'Acreditar mi depósito'
        ,   'deposit_date_default' => date('d/m/Y')
        ,   'deposit_amount_default' => 500
        ,   'deposit_user_default' => $user
        ,   'deposit_phone_default' => $phone_default
        ];

        $view_data['user'] = $user;
        $view_data['profile'] = $profile;

        $view_data['payment_account'] = $payment_account;
        $view_data['payment_method']  = $payment_account['accountService'];

        if ($this->submitAndValidateDepositForm() !== false)
        {
            $deposit = $this->createDepositFromForm();
            
            $permissions = $this->deposit_notification_service->get_submit_permissions(
                $profile, $deposit
            );

            if (!$permissions['submit'])
            {
                return redirect('/profile-panel/billing/new_deposit_not_allowed');
            }

            if ( ! $this->deposit_notification_service->is_submit_duplicated($profile, $deposit) )
            {
                $this->deposit_notification_service->submit_deposit_notification(
                    $profile, $deposit, $permissions['activate']
                );
            }        

            $this->deposit_match_service->assign_deposits_to_profiles($deposit['date']);

            return redirect('/profile-panel/billing/new_deposit_success');
        }


        $this->render('site/profile-panel/billing/page_new_deposit', $view_data);
    }


    public function new_deposit_success()
    {
        $this->render('site/profile-panel/billing/page_new_deposit_success', [
            'title' => 'Deposito acreditado'
        ]);
    }


    public function new_deposit_not_allowed()
    {
        $this->render('site/profile-panel/billing/page_new_deposit_not_allowed', [
            'title' => 'Error de acreditación'
        ]);
    }


    protected function submitAndValidateDepositForm()
    {
        $this->form_validation->set_message('required', 'No olvides este campo');
        $this->form_validation->set_message('validate_user_exists', 'El usuario %s no existe');
        $this->form_validation->set_message('validate_date', 'Fecha incorrecta, debe ser del formato ' . date('d/m/Y'));
        $this->form_validation->set_message('validate_time', 'Hora incorrecta, debe ser del formato hora:minuto, ej: 23:15');

        $this->form_validation->set_rules('deposit_name', 'depositante', 'trim|required');
        $this->form_validation->set_rules('deposit_amount', 'monto', 'trim|required');
        $this->form_validation->set_rules('deposit_date', 'fecha', 'trim|required|callback_validate_date');
        $this->form_validation->set_rules('deposit_time', 'hora', 'trim|required|callback_validate_time');
        $this->form_validation->set_rules('deposit_user', 'usuario', 'trim|required|callback_validate_user_exists');
        $this->form_validation->set_rules('deposit_phone', 'teléfono', 'trim|required');

        $validForm = $this->form_validation->run() !== false;
        $validPhoto = false;

        if ($validForm)
        {
            $validPhoto = $this->submitDepositPhoto();
        }

        return $validForm;
    }


    protected function submitDepositPhoto()
    {
        $user = $this->profile_model->get_logged_user();

        $upload_dir_path = './userdata/' . $user . '/deposits/';

        @mkdir(dirname($upload_dir_path));
        @mkdir($upload_dir_path);
        
        $upload_config['upload_path'] = $upload_dir_path;
        $upload_config['allowed_types'] = 'gif|jpg|jpeg|bmp|png';
        $upload_config['encrypt_name']  = true;
        $upload_config['overwrite'] = true;
        $upload_config['remove_spaces'] = true;

        $this->load->library('upload', $upload_config);

        $upload_result = $this->upload->do_upload("deposit_photo");

        return $upload_result;
    }


    protected function getDepositPhoto() {
        $upload_data = $this->upload->data();

        if ($upload_data['is_image'] && file_exists($upload_data['full_path']))
        {
            $mediaDirPath = $this->config->item('media_dir_path');
            $mediaDirPath = realpath($mediaDirPath);

            $pic_path = str_replace($mediaDirPath,'',$upload_data['full_path']);
            return $pic_path;
        }
        
        return false;
    }


    protected function createDepositFromForm()
    { 
        $deposit_date_ymd = $this->transform_date($this->input->post('deposit_date'));

        $deposit_user = $this->input->post('deposit_user');
        $deposit_user = mb_strtolower($deposit_user);

        $deposit = [
            'amount'   => $this->input->post('deposit_amount')
        ,   'date'     => $deposit_date_ymd
        ,   'time'     => $this->input->post('deposit_time')
        ,   'name'     => $this->input->post('deposit_name')
        ,   'user'     => $deposit_user
        ,   'phone'    => $this->input->post('deposit_phone')
        ,   'comments' => $this->input->post('deposit_comments')
        ];

        $deposit['photo'] = $this->getDepositPhoto();

        return $deposit;
    }


    protected function render($template, $data=null)
    {
        $this->load->view('site/header', $data);
        $this->load->view($template, $data);
        $this->load->view('site/footer');
    }


    protected function render_json($data, $status_code=200)
    {
        header('Content-type:application/json;charset=utf-8');
        echo json_encode($data);
        exit();
    }

    public function validate_user_exists($user)
    {
        $profile = $this->profile_model->get_profile($user);
        return ($profile)? true : false;
    }

    public function validate_date($raw_date)
    {
        if (preg_match('!(\d\d?) */ *(\d\d?) */ *(\d\d\d\d)!', $raw_date))
        {
            return true;
        }

        return false;
    }

    public function validate_time($raw_date)
    {
        if (preg_match('!^\d{1,2}:\d{1,2}$!', $raw_date))
        {
            return true;
        }

        return false;
    }

    public function transform_date($raw_date)
    {
        if (preg_match('!(\d\d?) */ *(\d\d?) */ *(\d\d\d\d)!', $raw_date, $m))
        {
            return $m[3] . '-' . $m[2] . '-' . $m[1];
        }

        return false;
    }

    protected function check_and_get_user($user)
    {
        $user = ($user) ? $user : $this->profile_model->get_logged_user();

        if ($this->profile_model->login_exists($user))
        {
            return $user;
        }

        return false;
    }

}

/* eof */