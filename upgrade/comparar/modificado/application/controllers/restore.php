<?php

class Restore extends CI_Controller {
	
	protected $_global;
	
	public function __construct() {
		parent::__construct();
		$this->load->library('country');
		$this->load->model('profile_model');
		$this->load->model('security_model');
		
		$this->_fill_global();
	}	
	
	protected function _fill_global() {
		$available_countries = $this->country->get_countries();
		$session_country = $this->session->userdata('country');
		
		if ($session_country && in_array($session_country, $available_countries)) {
			$country = $session_country;
		} else {
			$country = 'uy';
		}
		
		$this->_global = array(
			'available_countries' => $available_countries,
			'session_country' => $country
		);
	}
	
	public function index() {			
		$this->start();
	}
	
	protected function start() {
				
		if ($this->security_model->on_limit_request("profile_try_restore", 60)) {
			$forbidden_message = "Hola! por motivos de seguridad no puedes probar más veces. ";
			$forbidden_message .= "Si tienes un problema en particular con recuperar tu perfil, te ayudaremos a solucionarlo.";
			$forbidden_message .= "<br><br>Sólo tienes que mandanos un correo a " . safe_mailto('rrpp@pumbate.com', 'rrpp@pumbate.com') . "<br><br>";
			$forbidden_message .= "Esperamos que sepas disculpar las molestias ocasionadas.<br>El equipo de pumbate.com<br><br>";
			$forbidden_message .= anchor("", "Volver a la portada de Pumbate.com");
			show_error($forbidden_message, 403, "Has llegado al límite de intentos!");
		}
		
		if ($this->security_model->on_limit_request("profile_restored", 3)) {
			redirect("restore/restore_limit");
		}
		
		if ($this->security_model->on_request_lock("profile_restored")) {
			redirect("restore/wait");
		}		
		
		$start_data = array(
			'country' => $this->_global['session_country'],
			'country_options' => $this->_global['available_countries'],
			'success' => false,
			'fail' => false,
		);
		
		if ($this->input->post('send')) {
			$action_result = $this->_action_send();
			
			if (is_array($action_result)) {
				if (count($action_result)) {
					$start_data['success'] = true;
					$start_data['success_email'] = $action_result['success_email'];
					$start_data['success_phone'] = $action_result['success_phone'];
					$start_data['success_anon_email_sent'] = $action_result['success_anon_email_sent'];
				} else {
					$start_data['fail'] = true;
					$this->security_model->penalize_request("start_restore");
				}
			}
		}
				
		$this->load->view('site/header');
		
		if ($start_data['success']) {
			$this->load->view('site/restore/finish', $start_data);
		} else {
			$this->load->view('site/restore/start', $start_data);
		}
		
		$this->load->view('site/footer');
	}

	
	protected function _action_send() {
		
		if ($this->input->post('country') == 'uy' ) {
			$this->form_validation->set_rules('phone', 'Teléfono', 'trim|required|callback_valid_phone');
			$this->form_validation->set_rules('email', 'E-mail', 'trim|valid_email|strtolower');
		} else {
			$this->form_validation->set_rules('phone', 'Teléfono', 'trim|callback_valid_phone_not_required');
			$this->form_validation->set_rules('email', 'E-mail', 'trim|required|valid_email|strtolower');
		}
		
		$this->form_validation->set_rules('country', 'País', 'trim|required|exact_length[2]');
		$this->form_validation->set_rules('user', 'Usuario', 'trim');
		
		$this->form_validation->set_message('required', 'Necesitamos al menos tu %s.');
		$this->form_validation->set_message('valid_email', 'La dirección de e-mail no está bien escrita.');
		$this->form_validation->set_message('valid_phone', 'Parece que faltan algunos números.');
		$this->form_validation->set_message('valid_phone_not_required', 'Parece que faltan algunos números.');
		
		if ( $this->form_validation->run() !== false ) {
			$post = $this->input->post();
			
			$this->db->select('p.proUser,proEmail,proPhone,proNumericPhones,proRestoreCode,proRestoreHash,proRestoreExpires');
			$this->db->from('profiles p');
			
			$this->db->join('profiles_restores pr', 'pr.proUser = p.proUser', 'left outer');
			
			$this->db->where('proCountry', $post['country']);
			$this->db->where('proDeleted', PROFILE_NOT_DELETED);
			
			$this->load->library('phoneparser');
			$phones_only_numbers = preg_replace('!,.*!', '',$this->phoneparser->get_numbers_string($post['phone'], $post['country']));
			
			if ($post['country'] == "uy") {	
				if ($post['phone'] && $post['email']) {
					$like_str = "( proEmail like '".$post['email']."' OR ";
					$like_str .= " proNumericPhones like '%" . $phones_only_numbers . "%' )";
					$this->db->where($like_str);
					
				} elseif ($post['phone']) {
					$this->db->like('proNumericPhones', $phones_only_numbers);
				} elseif ($post['email']) {
					$this->db->like('proEmail', $post['email'], 'none');
				}
			} else {
				$this->db->like('proEmail', $post['email'], 'none');
			}
			
			$this->db->order_by("proId", "desc");
			
			$this->db->limit(6);
			
			$res = $this->db->get();
			
			$users_found = array();
			
			if ($res->num_rows()) {
				//print_r($res->result_array());
				foreach ($res->result_array() as $row) {
					$restore_code = '';
					$restore_hash = '';
					
					$pro_user = $row['proUser'];
					
					if (isset($row['proRestoreExpires']) && time() < strtotime($row['proRestoreExpires'])) {
						$restore_code = $row['proRestoreCode'];
						$restore_hash = $row['proRestoreHash'];
					} else {
						$this->profile_model->delete_restore_code($pro_user);
						
						/* CREATE NEW RESTORE CODE */
						$new_codes = $this->profile_model->create_restore_code($pro_user);

						if (is_array($new_codes)) {
							$restore_code = $new_codes['restore_code'];
							$restore_hash = $new_codes['restore_hash'];
						}
					}

					if ($restore_code && $restore_hash) {
						$users_found[$pro_user] = array(
							'user' => $pro_user,
							'restore_code' => $restore_code,
							'restore_hash' => $restore_hash,
							'email' => $row['proEmail'],
							'phone' => $row['proPhone'],
							'numeric_phone' => $row['proNumericPhones'],
							"form_input_country" => $post["country"],
							"form_input_user" => $post["user"],
							"form_input_phone" => $post["phone"],
							"form_input_email" => $post["email"],
						);
					}
				}
			}
			
			$possible_user = strtolower($this->input->post('user'));
			
			if (isset($users_found[$possible_user])) {
				$possible_user_data = $users_found[$possible_user];
				
				$users_found = array(
					$possible_user => $possible_user_data
				);
			}
						
			//print_r($users_found); die;
			
			$matched_email = false;
			$matched_phone = false;
			$anon_email_sent = false;
			
			foreach ($users_found as $pro_user => $user_data) {
				
				$log_notify = false;
				
				/*
				if (stripos($user_data['numeric_phone'], $phones_only_numbers) !== false) {
					$matched_phone = $post["phone"];
				}
				
				if ($post['country'] == "uy") {
					$log_notify = true;
				}
				*/
				
				if ($user_data['email']) {
					
					if (strtolower($user_data['email']) == strtolower($post['email'])) {
						$matched_email = $post['email'];
					}
					
					$this->_send_restore_email($user_data);
					$anon_email_sent = true;
				}
				/*
				if ($post['country'] === 'uy' && $matched_phone)
				{
					$this->_send_restore_sms($user_data);
				}
				*/

				$this->load->model('log_model');

				$this->log_model->insert_log(
					$post['country'], 'profile_restore_code', $pro_user, $log_notify, false, $user_data
				);
			}
				
			//print_r($users_found); die;
			
			if (count($users_found)) {
				$this->security_model->lock_request("profile_restored", 5);
				$this->security_model->penalize_request("profile_restored");
				
				return array(
					'success_email' => $matched_email,
					'success_phone' => $matched_phone,
					'success_anon_email_sent' => $anon_email_sent,
				);				
			} else {
				$this->security_model->penalize_request("profile_try_restore");

				return array();
			}
			
		}
		
		return false;
	}
	
	
	protected function _send_restore_email($user_data) {
		
		$this->load->library('mail_notification');
		
		$restore_text = array(
			"Hola! Estás recibiendo este e-mail para recuperar tu usuario o contraseña de pumbate.com",
			"", 
			"",
			"Tu usuario actual es: " . $user_data['user'],
			"",
			"Haz click en el enlace a continuación para ingresar una nueva contraseña:",
			site_url("restore/new_password/".$user_data["restore_hash"]),
			"",
			"",
			"¿No te ha funcionado? Prueba lo siguiente...",
			"",
			"Entra a la sección Mi Página (www.pumbate.com/mipagina) con los siguientes datos:",
			"",
			"Usuario: ".$user_data['user'],
			"Contraseña: ".$user_data['restore_code'],
			"",
			"Y te dejará ingresar una nueva contraseña.",
			"",
			"",
			"¿Tienes alguna duda? Siempre puedes escribirnos un mail a rrpp@pumbate.com",
			"El equipo de Pumbate.com",	
		);

		$restore_text = implode("\r\n", $restore_text);
		
		$this->mail_notification->send_mail( 
			array( $user_data['email'] => $user_data['user'] ),
			'Recupera tu perfil '. $user_data['user'],
			$restore_text
		);
	}


	protected function _send_restore_sms($user_data)
	{
		$this->load->model('customer_message_model');

		$restore_message  = "USUARIO: ". $user_data['user'];
		$restore_message .= " CONTRASEÑA TEMPORAL: ". $user_data['restore_code'];

		$this->customer_message_model->create_outbox_message([
            'message_address' => $user_data['numeric_phone']
        ,   'message_text' => $restore_message
        ,   'message_category' => 'support'
        ]);
	}

	
	public function wait() {
		$error_data = array();
		
		$mess = array(
			"No olvides revisar tu e-mail o teléfono para encontrar el ",
			"mensaje para recuperar tu usuario y contraseña.<br><br>",
			"Si quieres recuperar otra cuenta, debes esperar unos minutos."
		);
		
		$error_data["message"] = implode("", $mess);

		$this->load->view('site/header');
		$this->load->view('site/restore/error', $error_data);
		$this->load->view('site/footer');	
	}
	
	public function restore_limit() {
		$error_data = array();
		
		$mess = array(
			"Lo sentimos pero por motivos de seguridad sólo puedes restaurar hasta 3 cuentas ",
			"por día.<br><br>",
			"Si tienes algún problema en particular, ",
			anchor("#contact", "contáctanos."),
			"!<br><br>",
		);
		
		$error_data["message"] = implode("", $mess);

		$this->load->view('site/header');
		$this->load->view('site/restore/error', $error_data);
		$this->load->view('site/footer');	
	}	
		
	public function new_password($restore_hash = false) {
		if ($this->security_model->on_limit_request("new_password", 60)) {
			$forbidden_message = "Hola! por motivos de seguridad no puedes probar más veces. ";
			$forbidden_message .= "Si tienes un problema en particular con recuperar tu perfil, te ayudaremos a solucionarlo.";
			$forbidden_message .= "<br><br>Sólo tienes que mandanos un correo a " . safe_mailto('rrpp@pumbate.com', 'rrpp@pumbate.com') . "<br><br>";
			$forbidden_message .= "Esperamos que sepas disculpar las molestias ocasionadas.<br>El equipo de pumbate.com<br><br>";
			$forbidden_message .= anchor("", "Volver a la portada de Pumbate.com");
			show_error($forbidden_message, 403, "Has llegado al límite de intentos!");
		}
		
		$restore_exists = false;
		
		if ($restore_hash) {
			$this->db->where('proRestoreHash', $restore_hash);
			$this->db->where('proRestoreExpires >', date('Y-m-d H:i:s'));
			$res = $this->db->get('profiles_restores');
			
			if ($res->num_rows() > 0) {
				$user = $res->row()->proUser;
				$restore_exists = true;
			}
		}
		
		if ($restore_exists) {
			$panel_password_data = array(
				'restore_hash' => $restore_hash,
				'user' => $user,
				'error' => false,
			);

			if ($this->input->post('send')) {
				$this->form_validation->set_message('required', 'No olvides rellenar este campo.');
				$this->form_validation->set_message('matches', 'Las contraseñas escritas en los dos campos deben ser iguales.');
				$this->form_validation->set_message('alpha_dash', 'La contraseña sólo puede tener letras, números, guiones ó guiones bajos.');
				$this->form_validation->set_message('min_length', 'La contraseña debe tener un mínimo de 5 caracteres.');


				$this->form_validation->set_rules('password1', 'Nueva Contraseña', 'trim|required|min_length[5]|alpha_dash|matches[password2]');
				$this->form_validation->set_rules('password2', 'Repetir Contraseña', 'trim|required');

				if ($this->form_validation->run() !== false) { 
					$pass = $this->input->post('password1');
					$shapass= sha1($pass);

					$this->db->set('proPassword', $shapass);
					$this->db->where('proUser', $user);

					if ($this->db->update('profiles')) {
						$this->db->where('proUser', $user);
						$this->db->delete('profiles_restores');
						
						$this->load->model("flashsession_model");
					
						$this->flashsession_model->set_var("wait_for_login_user", $user);
						$this->flashsession_model->set_encrypted_var("wait_for_login_pass", $pass);

						$this->flashsession_model->save(2);
						
						$this->load->model("log_model");
						$this->log_model->remove_notifications($user, 'profile_restore_code');

						redirect('restore/new_password_success');
						return true;
					} else {
						$panel_password_data['error'] = true;
					}
				}
			}

			$this->load->view('site/header', array('title' => 'Cambia la password de tu página en Pumbate Escorts'));
			$this->load->view('site/restore/new_password', $panel_password_data);
			$this->load->view('site/footer');	
			
		} else {
			$this->security_model->penalize_request("new_password");
			
			$this->load->view('site/header', array('title' => 'Ya has recuperado tu contraseña'));
			$this->load->view('site/restore/new_password_disallowed');
			$this->load->view('site/footer');
		}
	}
	
	public function new_password_success() {
		$this->load->view('site/header', array('title' => 'Has cambiado exitosamente tu contraseña - Pumbate Escorts'));
		$this->load->view('site/restore/new_password_success');
		$this->load->view('site/footer');	
	}
	

	public function valid_phone($phone) {
		$phone = preg_replace('![^0-9]!', '', $phone);
		
		if ( strlen($phone) > 5 ) {
			return true;
		} else {
			return false;
		}
	}	
	
	public function valid_phone_not_required($phone) {
		$phone = preg_replace('![^0-9]!', '', $phone);
		
		if ( strlen($phone) == 0 || strlen($phone) > 5) {
			return true;
		} else {
			return false;
		}
	}	
	
}
/* end of file */