<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Welcome extends CI_Controller {

	
	public function __construct() {
		parent::__construct();
		$this->load->library('session');
	}

	public function index() {
		
		$country = $this->session->userdata('country');
		
		if ($country) {
			$this->load->library('country');
			
			redirect(strtolower($this->country->name($country)));
		} else {
			$this->load->view('site/header');
			$this->load->view('site/welcome');
			$this->load->view('site/footer');			
		}
		
	}
	
	public function change() {
		$this->session->set_userdata('country', false);
		redirect('');
	}
	
}




/* End of file main.php */