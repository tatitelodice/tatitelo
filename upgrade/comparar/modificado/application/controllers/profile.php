<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Profile extends CI_Controller {
	
	protected $pictures_service;
	protected $panel_service;
	protected $billing_service;
	protected $publication_service;

	public function __construct() {
		parent::__construct();
		$this->load->model("admin_model");
		$this->load->model('profile_model');
		$this->load->model('log_model');

		$this->load->helper('profile_helper');
		$this->load->library('contact');

		$this->load->library('profile/profile_pictures_service');
		$this->pictures_service = &$this->profile_pictures_service;

		$this->load->library('profile/profile_panel_service');
		$this->panel_service = &$this->profile_panel_service;

		$this->load->library('profile/profile_billing_status_service');
		$this->billing_service = &$this->profile_billing_status_service;

		$this->load->library('profile/profile_publication_status_service');
		$this->publication_service = &$this->profile_publication_status_service;
	}	

	public function index() { 
		redirect('');
	}
		
	public function panel($url_user = false) {
		
		if ($this->config->item('maintenance_mode')) {
			$this->_maintenance();
			return false;
		}
		
		$user = $this->profile_model->get_logged_user();
		
		if ($url_user) {
			$url_user = strtolower($url_user);
			
			if (!$this->profile_model->login_exists($url_user)) {
				$this->_login();
				return false;	
			} else {
				$user = $url_user;
			}
		}
			
		if ( ! ($user && $this->profile_model->login_exists($user) ) ) {
			$this->_login();
		} else {	
			
			# CORRECCION ANTI INYECCION DE USER
			if ($this->input->post('user') !== false && $this->input->post('user') != $user) {
				die('Error: operación no permitida GTFO!.');
			}

			$success = null;

			if ($this->input->post('action') == 'city') {
				$this->_update_cities();
				return true;
			}
			
			if ($this->input->post('action') == 'html_adpics') {
				$this->_html_adpics();
				return true;
			}
			
			if ($this->input->post('action') == 'adpic') {
				$success = $this->_update_adpic();
				return true;
			}

			if ($this->input->post('action') == 'theme') {
				$success = $this->_update_theme();
				return true;
			}		
			
			if ($this->input->post('action') == 'visits_table') {
				$success = $this->_update_visits_table();
				return true;
			}	

			$profile_data = $this->profile_model->get_profile($user);
			
			if ($profile_data !== false) {
				
				if ($this->input->post('action') == 'information') {
					$success = $this->_update_information($profile_data);
					$profile_data = $this->profile_model->get_profile($user);
					$profile_data['picture'] = $this->_check_for_pictures($user, $profile_data['picture']);
					
					// FEATURE AUTO EXPIRE PROFILE
					$billing_status = $this->profile_billing_status_service->get_billing_status($user);

					if (
						(intval($profile_data['accountType']) > PROFILE_FREE) &&
						($billing_status["has_pending_deposit"] === false) &&
						($billing_status["days_remaining"] < 0)
					)
					{
						$this->profile_model->disable_profile($user);
						$this->log_model->insert_log($profile_data["country"], "profile_hidden", $user);
						return redirect(current_url());
					}
					// END FEATURE AUTO EXPIRE PROFILE
				}
				
				$profile_data['success'] = $success;
				
				$this->_authenticated_panel($profile_data);
			} else {
				$this->_login();
			}			
		}	
	}

	private function _authenticated_panel($profile_data) {
		$this->load->model("flashsession_model");
		$this->load->model("security_model");
		
		$user = $profile_data['user'];
		
		$profile_data['publication'] = $this->publication_service->get_publication_status($profile_data);

		$profile_data['billing'] = $this->billing_service->get_billing_status($user);
		$profile_data['billing']['pricing'] = $this->billing_service->get_pricing($profile_data);

		$profile_data['themes'] = $this->panel_service->get_themes(
			$user
		,	$profile_data['theme']
		,	$profile_data['gender']
		,	$profile_data['accountType']
		);

		$profile_data['pictures'] = get_profile_pictures($user, true);


		$profile_data['ages'] = array('0' => 'No mostrar');

		for ($a=18; $a < 56; $a++) {
			$profile_data['ages'][$a] = $a;
		}

		$profile_data['week_days_options'] = array(
			'lunes', 'martes', 'miercoles', 'jueves', 'viernes', 'sabados', 'domingos'
		);

		$profile_data['days'] = explode(' ', $profile_data['days']);

		$this->load->library('country');
		$regions = $this->country->get_regions($profile_data['country']);

		$profile_data['regions'] = array();

		foreach ($regions as $reg) {
			$profile_data['regions'][$reg] = $reg;
		}

		$profile_data['cities'] = array();
		$profile_data['cities'][''] = 'Cualquier Zona/Ciudad';

		$cities = $this->country->get_cities($profile_data['country'], $profile_data['region'] );

		foreach ($cities as $cit) {
			$profile_data['cities'][$cit] = $cit;
		}

		/* extra locations */
		$profile_data['extra_locations'] = array();

		$keywords = $this->profile_model->get_keywords($profile_data['user']);

		if ($keywords && $keywords['locations']) {
			$raw_locations = explode('#', $keywords['locations']);

			//print_r($raw_locations); die;

			foreach ($raw_locations as $raw_loc) {
				$raw_loc = explode('::', $raw_loc);

				$extra_region = $raw_loc[0];
				$extra_city = '';

				$extra_cities = array();
				$extra_cities[''] = 'Cualquier Zona/Ciudad';

				foreach ($this->country->get_cities($profile_data['country'], $extra_region) as $cit) {
					$extra_cities[$cit] = $cit;
				}

				if (isset($raw_loc[1])) {
					$extra_city = $raw_loc[1];
				}

				$profile_data['extra_locations'][] = array(
					'region' => $extra_region,
					'city' => $extra_city,
					'cities' => $extra_cities,
				);
			}

		}

		$profile_data['categories_options'] = $this->profile_model->get_available_categories($profile_data['gender']);
		$profile_data['categories'] = explode(' ', $profile_data['categories']);


		/* destroy wait for first login only if user is verified */
		/* if profile is complete add respective alert */
		$profile_complete = false;

		if ($profile_data['verified'] == PROFILE_STATUS_NOT_VERIFIED  && $profile_data['accountType'] == PROFILE_FREE) {
			$default_texts_count = 0;

			$default_fields = array('subtitle', 'subtitle2', 'about', 'services', 'place');

			foreach ($default_fields as $default_field) {
				if (strlen($profile_data[$default_field]) < 5) {
					$default_texts_count++;
				}
			}
			//echo $default_texts_count; die;
			$profile_is_garbage = $this->flashsession_model->get_var("profile_is_garbage");
			
			if ($profile_is_garbage) {
				$profile_data['profile_is_garbage'] = $profile_is_garbage;
				
			} elseif (!$profile_data['picture'] || $default_texts_count > 2 || count($profile_data['pictures']) == 0) {
				$this->flashsession_model->unset_var('profile_complete');
				$this->flashsession_model->set_var('profile_help', true);
				$profile_complete = false;
				
				if ($this->admin_model->login_exists() === false) {
					$this->security_model->lock_request("profile_create", 1440);
				}
				
			} else {
				$this->flashsession_model->set_var('profile_complete', true);
				$profile_complete = true;
				
				$this->security_model->unlock_request("profile_create");
			}
			
			$this->flashsession_model->save(2);
			
		} else {
			$this->flashsession_model->unset_var('wait_for_login_user');
			$this->flashsession_model->unset_var('wait_for_login_password');
			$this->flashsession_model->save();
		}
		
		$profile_data['panel_message'] = $profile_data['panelMessage'];

		$profile_data['panel_message'] = preg_replace('!:+phone:+!i', $this->contact->get_phone($profile_data['country'])  ,$profile_data['panel_message']);
		$profile_data['panel_message'] = nl2br($profile_data['panel_message']);

		$profile_data['absent_message'] = $profile_data['absentMessage'];

		if (trim($profile_data['absent_message'] == '')) {
			$profile_data['absent_message'] = 'Hola, estaré de viaje por unos días, volveré pronto para complacerte.';
		}

		$profile_data['account_type'] = $profile_data['accountType'];

		if ($profile_data['accountType'] > PROFILE_FREE) {
			$this->load->model('analytics_model');

			$analytics_data = $this->analytics_model->get_analytics($profile_data['user'], date('Y-m-01'));

			if (isset($analytics_data[date('Y-m-d')])) {
				$today_visits = $analytics_data[date('Y-m-d')]['pageviews'];
			} else {
				$today_visits = $analytics_data[date('Y-m-d', strtotime('-1 day'))]['pageviews'];
			}

			$profile_data['total_visits'] = $today_visits + $this->analytics_model->get_historical_visits_count($profile_data['user']);
			$profile_data['today_visits'] = $today_visits;

			$profile_data['table_visits'] = $this->_table_visits($analytics_data);
		}

		$profile_data['pumbate_phone'] = $this->contact->get_phone($profile_data['country']);
		$profile_data['pumbate_phone_whatsapp'] = $this->contact->get_phone($profile_data['country'], 'whatsapp', false);

		
		$profile_data['admin_login'] = $this->admin_model->login_exists();
		
		$profile_data["profile_complete"] = $profile_complete;
		
		/* finally show the panel */
		$this->load->view('site/header', array('title' => 'Edita tu página en Pumbate Escorts'));

		if (!$profile_data['admin_login'] && $profile_data['verified'] == PROFILE_STATUS_HIDDEN) {
			$this->load->view('site/profile-panel/main/panel_disabled', $profile_data);
		} else {
			$this->load->view('site/profile-panel/main/panel', $profile_data);
		}

		$this->load->view('site/footer', array( 'nofooter' => true ));
	}
	
	
	private function _table_visits(&$analytics_data) {
		
		if (!count($analytics_data)) {
			return  '';
		}


		$visits = [];

		$this->load->library('table');
		$this->table->set_template( array( 'table_open' => '<table class="table table-condensed table-visits">',));
		$this->table->set_heading( 'Día', 'Visitas');

		$this->load->library('sdate');

		foreach ($analytics_data as $analytics_ymd => $analytics_row) {
			$visits[] = array(
				$this->sdate->formatoYMDaLiteral($analytics_ymd),
				$analytics_row['pageviews'],
			);
		}

		$table_visits = $this->table->generate($visits);

		return $table_visits;
	}
	
	
	private function _check_first_login() {
		$wfl_user = false;
		$wfl_pass = false;
		
		$this->load->model("flashsession_model");

		if ($this->flashsession_model->exists()) {
			$wfl_user = $this->flashsession_model->get_var("wait_for_login_user");
			$wfl_pass = $this->flashsession_model->get_encrypted_var("wait_for_login_pass");
		}
		
		$wait_for_first_login = $wfl_user && $wfl_pass;
		
		return array($wait_for_first_login, $wfl_user, $wfl_pass);
	}
	
	private function _login() {
		
		if ($this->_on_limit_wrong_login()) {
			redirect('profile/forbidden_login');
			return false;
		}
		
		/* logout and penalize userdata without password */
		if ($this->session->userdata('user')) {
			$this->profile_model->logout();
			$this->_penalize_wrong_login();
		}
		
		$country_iso = $this->session->userdata('country');
		
		list($has_first_login, $fl_user, $fl_pass) = $this->_check_first_login();
		
		$panel_login_data = array(
			'error' => false,
			'has_first_login' => $has_first_login,
			'fl_user' => $fl_user,
			'fl_pass' => $fl_pass,
			'country' => $country_iso,
			'contact_phone' => $this->contact->get_phone($country_iso, 'sms')
		);
		
		if ($this->input->post('send')) {			
			$this->form_validation->set_message('required', 'No olvides tu %s');
			$this->form_validation->set_rules('user', 'Usuario', 'trim|required');
			$this->form_validation->set_rules('password', 'Contraseña', 'trim|required');
			
			if ($this->form_validation->run() !== false) {
				
				$user = strtolower($this->input->post('user'));
				$user = preg_replace('![^a-z0-9\-_]!i', '', $user);

				$pass = $this->input->post('password');
				
				$restore_pass = strtoupper($pass);

				if ($this->profile_model->login_restore($user, $restore_pass)) {
					$restore_hash = substr(md5($user), 0, strlen($user)) . sha1($restore_pass);
					redirect('restore/new_password/' . $restore_hash);
					
				} elseif ($this->profile_model->login($user, $pass)) {
					redirect($user . '/panel');
						
				} else {
					$this->_penalize_wrong_login();
					$panel_login_data['error'] = true;
				}
			}
		}
		
		$this->load->view('site/header', array('title' => 'Accede y edita tu página en Pumbate Escorts'));
		$this->load->view('site/profile-panel/main/panel_login', $panel_login_data);
		$this->load->view('site/footer');
	}
	
	private function _on_limit_wrong_login() {
		$limit_login = false;
		
		$addr = $this->input->ip_address();
		
		if ($addr) {
			$this->load->model('temp_model');

			$temp_key = 'user_login::' . date('Ymd') . '::' . $addr;
			
			$temp = $this->temp_model->get_temp($temp_key);
			
			$login_limit_count = $this->config->item("profile_login_limit");
			
			if($login_limit_count < 50) {
				$login_limit_count = 50;
			}
			
			if ($temp && intval($temp['value']) >= $login_limit_count) {
				$limit_login = true;
			}
		}
		
		return $limit_login;
	}
	
	private function _penalize_wrong_login() {
		$addr = $this->input->ip_address();
		
		if ($addr) {
			$this->load->model('temp_model');

			$temp_key = 'user_login::' . date('Ymd') . '::' . $addr;
			
			$temp = $this->temp_model->get_temp($temp_key);
			
			if ($temp) {
				$new_val = intval($temp['value']) + 1;
				$this->temp_model->update_temp($temp_key, $new_val);
			} else {
				$this->temp_model->insert_temp($temp_key, '1', date('Y-m-d',strtotime('+2 day')) . ' 00:00:00');
			}
		}
	}
	
	
	public function logout() {
	    $this->profile_model->logout(); 
		redirect('profile/panel');
	}
		
	private function _update_information($original_profile) {
		$this->load->library('form_validation');
		
		$this->form_validation->set_message('required', 'El campo %s es obligatorio.');
		
		$this->form_validation->set_rules('user', 'Usuario', 'trim|required');
		$this->form_validation->set_rules('name', 'Nombre profesional', 'trim|required');
		$this->form_validation->set_rules('title', 'Título de la página', 'trim|required');
		
		//$this->form_validation->set_rules('absent', 'Ausente', 'integer');
		//$this->form_validation->set_rules('absent_message', 'Mensaje de ausencia', 'trim');
		
		if ($this->form_validation->run() !== false) {
			$post = $this->input->post();
			
			
			$post = $this->_security_clean($post);
			
			//print_r($post); die;
			
			$this->db->set('proName', $post['name']);
			$this->db->set('proPhone', $post['phone']);
			
			$this->load->library('phoneparser');
			$phones_only_numbers = $this->phoneparser->get_numbers_string($post['phone'], $post['country']);

			$this->db->set('proNumericPhones', $phones_only_numbers );
			
			$this->db->set('proCity', $post['city']);
			$this->db->set('proRegion', $post['region']);
			
			$this->db->set('proPlace', $post['place']);
			$this->db->set('proAge', $post['age']);
			
			if (isset($post['days']) && is_array($post['days'])) {
				$spaced_days = implode(' ', $post['days']);
				$this->db->set('proDays', $spaced_days);
			} else {
				$spaced_days = '';
				$this->db->set('proDays', '');
			}
			
			if (isset($post['categories']) && is_array($post['categories'])) {
				$spaced_categories = implode(' ', $post['categories']);
			} else {
				$spaced_categories = '';
			}
			
			$this->db->set('proCategories', $spaced_categories);
			
			$this->db->set('proTitle', $post['title']);
			$this->db->set('proSubtitle', $post['subtitle']);
			$this->db->set('proSubtitle2', $post['subtitle2']);
			$this->db->set('proAbout', $post['about']);
			$this->db->set('proServices', $post['services']);
			
			//$this->db->set('proAbsent', $post['absent']);
			//$this->db->set('proAbsentMessage', $post['absent_message']);
			
			$this->db->where('proUser', $post['user']);
			$this->db->update('profiles');
			
			/* add extra locations */
			
			$this->profile_model->insert_if_not_in_keywords( $post['user'] );
			
			$extra_locations = array();
			
			for ($i=0; $i < 5; $i++) {
				
				if (isset($post['extra_region' . $i]) && $post['extra_region' . $i]) {
					
					$extra_location = $post['extra_region' . $i];
					
					if (isset($post['extra_city' . $i]) && $post['extra_city' . $i])
						$extra_location .= '::' . $post['extra_city' . $i];
					
					$extra_locations[] = $extra_location;
				}
			}
			
			$this->db->set('keyLocations', implode('#', $extra_locations));
			
			$raw_fulltext_keywords = '';
			$raw_fulltext_keywords .= ' ' . $post['user'] . ' ' . $post['name'];
			$raw_fulltext_keywords .= ' ' . preg_replace('!\D!', '', $post['phone']);
			$raw_fulltext_keywords .= ' ' . $post['place'] . ' ' . $post['city'] . ' ' . $post['region'];
			$raw_fulltext_keywords .= ' ' . implode(' ', $extra_locations);
			$raw_fulltext_keywords .= ' ' . $post['title'] . ' ' . $post['subtitle'] . ' ' . $post['subtitle2'];
			$raw_fulltext_keywords .= ' ' . $post['about'] . ' ' . $post['services'];
						
			if ($spaced_days) {
				$raw_fulltext_keywords .= ' ' . $spaced_days;
			}
			
			if ($spaced_categories) {
				$raw_fulltext_keywords .= ' ' . $spaced_categories;
			}
			
			$raw_fulltext_keywords = preg_replace('!\[\s*cambialo.*?\]!i', '', $raw_fulltext_keywords);
			
			$this->load->library('keywords');
			$fulltext_keywords = $this->keywords->generate($raw_fulltext_keywords);
			
			$this->db->set('keyFulltext', $fulltext_keywords );
			
			$this->db->where('proUser', $post['user']);
			$this->db->update('keywords');

			
			/* ACTUALIZAMOS LOG */
			$entry_type = 'profile_updated_user';
			
			if ($this->admin_model->login_exists()) {
				$entry_type = 'profile_updated_admin';
			}
			
			
			$this->log_model->insert_log($original_profile['country'], $entry_type, $post['user']);
			
			return true;
		}
		
	}
	
	
	private function _security_clean($data) {
		$this->load->helper('spam_helper');
		
		$allowed_fields = array('title', 'subtitle', 'subtitle2', 'about', 'services', 'place', 'phone');
		
		foreach ($allowed_fields as $field) {
			if (isset($data[$field])) {
				$data[$field] = strip_tags_and_crap($data[$field]);
				$data[$field] = replace_website_words($data[$field]);
			}
		}

		return $data;
	}
	
	
	
	private function _check_for_pictures($user, $main_picture) {
		$alt_picture = $this->_leave_only_user_pictures($user);
		$has_main_picture = $main_picture && file_exists( 'userdata/'.$user.'/' . $main_picture );
		
		if ( !$has_main_picture ) {	
			$main_picture = $alt_picture;			
			$data = array( 'user' => $user, 'picture' => $alt_picture );
			$this->_update_adpic($data);	
		}
		
		return $main_picture;
	}
	
	
	private function _leave_only_user_pictures($user) {
		$default_pics_files = array();
		$first_user_pic = false;

		$user_dir = 'userdata/'.$user.'/';
		
		if (is_dir($user_dir)) {
			foreach (scandir($user_dir) as $f) {
				if (preg_match('!^pumdefpic!i', $f)) {
					$default_pics_files[] = $f;
				} elseif ( !$first_user_pic && preg_match('!\.(jpe?g|png|gif)!i', $f)) {
					$first_user_pic = $f;
				}
			}
		}

		if ($first_user_pic && count($default_pics_files)) {
			foreach ($default_pics_files as $f) {
				@unlink( 'userdata/'.$user.'/'.$f );
				@unlink( 'userdata/'.$user.'/w200/'.$f );
				@unlink( 'userdata/'.$user.'/optimized/'.$f );
				@unlink( 'userdata/'.$user.'/thumbnail/'.$f );
				@unlink( 'userdata/'.$user.'/watermark/'.$f );
			}
		}
		
		return $first_user_pic;
	}
	
	private function _update_cities() {
		echo '<option value="">Cualquier Zona/Ciudad</option>';
		
		$region = $this->input->post('region');
		$country = $this->input->post('country');
		
		if ($region && $country) {
			$this->load->library('country');
			
			foreach ($this->country->get_cities($country, $region) as $cit) {
				echo '<option value="'.$cit.'">'.$cit.'</option>';
			}			
		}
	}
	
		
	private function _update_visits_table() {
		$start_date = $this->input->post('start_date');
		$valid_start_date = preg_match('!\d\d\d\d-\d\d?-\d\d?!', $start_date);
		
		$end_date = $this->input->post('end_date');
		$valid_end_date = preg_match('!\d\d\d\d-\d\d?-\d\d?!', $end_date);
		
		$user = $this->input->post('user');
		
		if ($user && $valid_start_date && $valid_end_date) {
			$this->load->model('analytics_model');
			$analytics_data = $this->analytics_model->get_analytics($user, $start_date, $end_date);			
			echo $this->_table_visits($analytics_data);	
		} else {
			echo '';
		}
	}
	
	private function _html_adpics() {
		$post = $this->input->post();
		
		$panel_adpics_data = array(
			'pictures' => get_profile_pictures( $post['user'], true ),
			'picture' => $post['picture'],
		);
		
		$this->load->view('site/profile-panel/main/panel_adpics', $panel_adpics_data);
	}
	
	
	private function _update_adpic($data = false) {
		
		if (!is_array($data)) {
			$data = $this->input->post();
		}
		
		$this->pictures_service->update_main_picture(
			$data['user'], $data['picture']
		);
	}

	private function _update_theme() {
		$this->load->library('form_validation');
		
		$this->form_validation->set_message('required', 'El campo %s es obligatorio.');
		$this->form_validation->set_rules('user', 'Usuario', 'trim|required');
		
		if ($this->form_validation->run() !== false) {
			$post = $this->input->post();		
			$this->db->set('proTheme', $post['theme']);
			$this->db->where('proUser', $post['user']);
			$this->db->update('profiles');
			
			return true;
		}
	}
	

	public function update_gallery($user) {
		
		if (!$this->profile_model->login_exists($user)) {
			echo 'Error: operación no permitida GTFO!.';
			die();
		}

		$profile_data = $this->profile_model->get_profile($user);

		$this->pictures_service->set_profile_data($user, $profile_data);
		$this->pictures_service->handle_update_pictures_request($user);
    }
	
	
	public function view_deleted_pictures($user) {
		if ($this->profile_model->login_exists($user)) {
			
			$view_data = array(
				'pictures' => array()
			);
		
			$user_dir = 'userdata/'.$user.'/deleted';

			if (is_dir($user_dir)) {
				foreach (scandir($user_dir) as $f) {
					if (preg_match('!\.(jpe?g|png|gif)!i', $f)) {
						$src = base_url('/userdata/'.$user.'/deleted/'.$f);
						$view_data['pictures'][$f] = $src;
					}
				}
			}
			
			$this->load->view("site/profile-panel/main/panel_deleted_pictures", $view_data);
			
		} else {
			redirect("profile/login");
		}
	}
	
	
	private function _maintenance() {
		$panel_login_data = array(
			'error' => false,
			'maintenance' => true,
		);
		
		$this->load->view('site/header', array('title' => 'Accede y edita tu página en Pumbate Escorts'));
		$this->load->view('site/profile-panel/main/panel_login', $panel_login_data);
		$this->load->view('site/footer');
	}
	
		
	public function forbidden_login() {
		$forbidden_message = "Hola! por motivos de seguridad no puedes probar más contraseñas. ";
		$forbidden_message .= "Si tienes un problema en particular con tu perfil, te ayudaremos a solucionarlo.";
		$forbidden_message .= "<br><br>Sólo tienes que mandanos un correo a " . safe_mailto('rrpp@pumbate.com', 'rrpp@pumbate.com') . "<br><br>";
		$forbidden_message .= "Esperamos que sepas disculpar las molestias ocasionadas.<br>El equipo de pumbate.com<br><br>";
		$forbidden_message .= anchor("", "Volver a la portada de Pumbate.com");

		show_error($forbidden_message, 403, "Has llegado al límite de intentos!");
	}
	
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */