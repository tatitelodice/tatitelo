<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class ComingSoon extends CI_Controller {


	public function index() {
		
		$this->load->view('site/header');
		$this->load->view('site/comingsoon');
		$this->load->view('site/footer');
	}
	
	
}

/* End of file main.php */