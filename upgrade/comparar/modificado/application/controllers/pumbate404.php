<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Pumbate404 extends CI_Controller {

	
	public function __construct() {
		parent::__construct();
	}

	public function index() {
		
		//header("HTTP/1.1 404 Not Found");
		$this->load->view('site/header');
		$this->load->view('site/404');
		
	}
	
}




/* End of file main.php */