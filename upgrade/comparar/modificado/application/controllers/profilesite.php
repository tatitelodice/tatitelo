<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class ProfileSite extends CI_Controller {
	
	public function __construct() {
		parent::__construct();
		$this->load->model('profile_model');
		$this->load->helper('profile_helper');
		$this->load->helper('text_helper');
	}
	
	public function index() {
		redirect('');
	}
	
	public function view($user) {
		$user = strtolower($user);
		$theme = $this->input->get("theme");
		
		$profile_data = $this->profile_model->get_profile($user);
		
		if ($profile_data !== false) {
			
			$pictures = get_profile_pictures($user, true);

			foreach ($pictures as &$pic)
			{
				$pic['src'] = $this->asset->url($pic['src'], 'img');
			}
				
			if ($profile_data['verified'] == PROFILE_STATUS_HIDDEN) {
				$theme = '_penalty';
			} elseif ( ($theme == false) || ($theme && !file_exists(APPPATH . '/views/themes/'.$theme) ) ) {
				$theme = $profile_data['theme'];
			}
			
			$this->load->library('country');
			
			$site_data = array(
				'common_path' =>  base_url('assets/themes/common/') . '/',
				'path' => base_url('assets/themes/'.$theme). '/',
				'name' => $profile_data['name'],
				'title' => $profile_data['title'],
				'subtitle' => $profile_data['subtitle'],
				'subtitle2' => $profile_data['subtitle2'],
				'gender' => $profile_data['gender'],
				'phone' => $profile_data['phone'],
				'age' => $profile_data['age'],
				'city' => $profile_data['city'],
				'region' => $profile_data['city'],
				'location' => $profile_data['region'],
				'country' => $profile_data['country'],
				'country_name' => $this->country->name($profile_data['country']),
				'place' => $profile_data['place'],
				'about' => nl2br($profile_data['about']),
				'services' => nl2br($profile_data['services']),
				'pictures' => $pictures,
				'verified_pictures' => $profile_data['verifiedPictures'],
				'about_verification_url' => false,
			);	
			
			if ($profile_data['city']) {
				$site_data['location'] = $profile_data['city'] . ', ' . $profile_data['region'];
			}
			
			$site_data['head_title'] = $this->_get_head_title($site_data);
			
			$url_country = 'http://www.pumbate.com/' . strtolower($site_data['country_name']);
			
			$site_data['phone_ad'] = '<br><span class="label label-pumbate-phone">Sentite como en casa y no olvides comentarme que me llamas de <a href="'.$url_country.'">Pumbate.com</a> :)</span>';
			$site_data['url_country'] =  $url_country;
			
			$site_data['url_gallery'] =  $url_country  . '#gallery';
			
			$services_extra_data = '';
			
			
			if ($profile_data['days']) {				
				$services_extra_data .=  $this->_format_available_days($profile_data['gender'], $profile_data["days"]);
			}
			
			if ($profile_data['categories']) {
				$categories = explode(" ", $profile_data['categories']);
				$services_extra_data .= $this->_format_clients_orientation($profile_data['gender'], $categories);
			}
			
			$keywords = $this->profile_model->get_keywords($profile_data['user']);
			
			$extra_locations = array();
			
			if ($keywords && $keywords['locations']) {
				$raw_locations = explode('#', $keywords['locations']);

				foreach ($raw_locations as $raw_loc) {
					$raw_loc = explode('::', $raw_loc);
					$extra_region = $raw_loc[0];
					
					if (!isset($extra_locations[$extra_region])) {
						$extra_locations[$extra_region] = array();
					}
					
					$full_region = 'cualquier parte de <strong>' . $extra_region . '</strong>';
					
					if (isset($raw_loc[1])) {
						$full_city = '<strong>' . $raw_loc[1] . '</strong>';
								
						if (!in_array($full_city, $extra_locations[$extra_region])) {
							array_unshift($extra_locations[$extra_region], $full_city);
						}
					} elseif (!in_array($full_region, $extra_locations[$extra_region])) {
						$extra_locations[$extra_region][] = $full_region;
					}
				}
			}
			
			#print_r($extra_locations);
			
			$total_extra_locations = count($extra_locations);
			
			if ($total_extra_locations) {
				$services_extra_data .= 'También viajo a' . (($total_extra_locations > 1)? ':<br>'  : ' ');
				
				foreach ($extra_locations as $locs_reg => $locs_vals) {
					
					if ($total_extra_locations > 1) {
						$services_extra_data .= '- ';
					}
					
					for ($l=0, $lc = count($locs_vals); $l < $lc; $l++) {
						if ($l > 0 ) {
							if ($l >= $lc - 1) {
								$services_extra_data .= ' y ';
							} else {
								$services_extra_data .= ', ';
							}
						}

						$services_extra_data .= ($l == 0)? ucfirst($locs_vals[$l]) : $locs_vals[$l];
					}
					
					if (!in_array('cualquier parte de <strong>' . $locs_reg . '</strong>', $locs_vals)) {
						$services_extra_data .= ' en <strong>' . ucfirst($locs_reg) . '</strong>';
					}

					$services_extra_data .= '.<br>';
				}
				
				$services_extra_data .= '<br>';
			}
			
			if ($services_extra_data) {
				$site_data['services'] =  $services_extra_data . $site_data['services'];
			}
			
			if ($profile_data['absent']) {
				$site_data['subtitle2'] = $profile_data['absentMessage'];
				$site_data['location'] = 'Estaré de viaje.';
				$site_data['phone'] = 'Lo volveré a publicar apenas regrese.';
			}
			
			if ($profile_data['verified'] == PROFILE_STATUS_HIDDEN) {
				$site_data['title'] = $this->_penalize_content($site_data['title']);
				$site_data['subtitle'] = $this->_penalize_content($site_data['subtitle']);
				$site_data['subtitle2'] = $this->_penalize_content($site_data['subtitle2']);
				$site_data['about'] = $this->_penalize_content($site_data['about']);
				$site_data['services'] = $this->_penalize_content($site_data['services']);
			}
			
			$this->load->view('themes/' .  $theme . '/header.php', $site_data);
			$this->load->view('themes/' .  $theme . '/body.php', $site_data);
			$this->load->view('themes/' .  $theme . '/footer.php', $site_data);
			//print_r($site_data); die;
			
		} else {
			$this->_show_404(); 
		}
	}
	
	
	private function _format_available_days($gender, $csv_days) {
		if (preg_match('!^duo!', $gender)) {
			$available_days = 'Trabajamos';
		} else {
			$available_days = 'Trabajo';
		}
		
		$available_days .= " los días ";
		
		$db_days = explode(' ', $csv_days);
		$available_days .= $this->_plural_text($db_days, '<span class="label label-pumbate-phone">', '</span>');
		$available_days .= '.<br><br>';
		
		return $available_days;
	}
	
	
	private function _format_clients_orientation($gender, $profile_raw_cats) {
		$text = "";
		
		$profile_orientation_cats = array();
		
		$possible_orientation_cats = array(
			'c_atiende_chicos' => "hombres", 
			'c_atiende_chicas' => "mujeres", 
			'c_parejas'=> "parejas",
		);
		
		foreach ($possible_orientation_cats as $p_cat => $p_cat_text) {
			if (in_array($p_cat, $profile_raw_cats)) {
				$profile_orientation_cats[$p_cat] = $p_cat_text;
			}
		}
				
		$c_count = count($profile_orientation_cats);
		
		if ($c_count > 0) {
			if (preg_match('!^duo!', $gender)) {
				$text = "Atendemos ";
			} else {
				$text .= "Atiendo ";
			}
			
			if ($c_count == 1) {
				$text .= "únicamente ";
			}
			
			$text .= $this->_plural_text(array_values($profile_orientation_cats), '<strong>', '</strong>');
			$text .= '.';
		}
		
		if ($gender != 'mujer') {
			$profile_role_cats = array();

			$possible_role_cats = array(
				'c_rol_activo' => "activo",
				'c_rol_pasivo' => "pasivo", 
			);

			foreach ($possible_role_cats as $p_cat => $p_cat_text) {
				if (in_array($p_cat, $profile_raw_cats)) {
					$profile_role_cats[$p_cat] = $p_cat_text;
				}
			}

			$r_count = count($profile_role_cats);

			if ($r_count > 0) {
				$text .= " <strong>Servicio ";

				if ($r_count == 1) {
					$text .= "sólo ";
				}

				$text .= " rol ";

				$text .= $this->_plural_text(array_values($profile_role_cats));
				$text .= '</strong>.';
			}
		} else {
			if (in_array('c_cambio_roles', $profile_raw_cats)) {
				$text .= " Si me lo piden realizo <strong>cambio de roles</strong>.";
			}
		}
		
		if ($text) {
			$text .= '<br><br>';
			$text = trim($text);
		}
		
		return $text;
	}
	
	
	private function _plural_text($terms, $before_term = "", $after_term = "") {
		$plural_text = "";
		
		for ($t=0, $tc = count($terms); $t < $tc; $t++) {
			if ($t > 0 ) {
				if ($t >= $tc - 1) {
					$plural_text .= ' y ';
				} else {
					$plural_text .= ', ';
				}
			}

			$plural_text .= $before_term . $terms[$t] . $after_term;
		}
		
		return $plural_text;
	}
	
	
	private function _get_head_title($site_data) {
		
		$head_title = $site_data['name'];

		if ($site_data['gender'] == 'trans') {
			$head_title .= ' travesti escort';

		} elseif ($site_data['gender'] == 'mujer') {

			if ($site_data['country'] == 'co') {
				$head_title .= ' chica prepago';
			} else {
				$head_title .= ' chica escort';
			}
		} elseif ($site_data['gender'] == 'hombre') {
			$head_title .= ' chico escort masculino';
		} elseif ($site_data['gender'] == 'duohh') {
			$head_title .= ' trio gay chicos escort';
		} else {
			$head_title .= ' trio lesbico parejas escort';
		}

		$head_title .= ' en ';

		if ($site_data['country'] == 'uy') {
			if ($site_data['city'] && $site_data['region'] != 'Montevideo') {
				$head_title .= $site_data['city'];
			} else {
				$head_title .= $site_data['region'];
			}
		} else {
			$head_title .= $site_data['location'];
		}

		$head_title .= ' ' . $site_data['country_name'] . ' ' . $site_data['subtitle'];
		$head_title = mb_strtolower($head_title);
		
		$head_title = preg_split('![,\.]?\s+!i',$head_title);
		
		$head_title = implode(' ', array_unique($head_title));
		$head_title = ucfirst($head_title);
		
		$head_title = character_limiter($head_title,70, '');
		
		return $head_title;
	}
	
	
	private function _penalize_content($text) {
		$literal_numbers = array(
			'cero', 'uno', 'dos', 'tres', 'cuatro', 'cinco', 'seis', 'siete', 'ocho', 'nueve',
		);	
			
		$text = preg_replace('!'.  implode('|', $literal_numbers).'!i', 'XXX', $text);
		$text = preg_replace('!([0-9][^a-z]*){4,10}!', ' XXX XXX XXX ', $text);
		$text = preg_replace('!\s+!', ' ', $text);
		
		return $text;
	}
	
	
	private function _show_404() {
		//header("HTTP/1.1 404 Not Found");
		$this->load->view('site/header');
		$this->load->view('site/404');
	}
	
}

/* end of file */
