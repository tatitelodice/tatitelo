<?php

if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Notes extends CI_Controller {

	protected $admin_country;
	
	public function __construct() {
		parent::__construct();
		$this->load->library('session');
		
		$this->load->model('admin_model');
		
		if (!$this->admin_model->login_exists()) {
			redirect ('padmin');
		}
		
		$this->admin_country = $this->admin_model->get_country();
	}	
	
	
	public function index() {	
		$this->list_all();
	}
	
	
	public function list_all() {
		$this->load->helper('text');
		
		$notes_data["notes"] = array();
		
		$this->db->select("notId,notTitle,notContent");
		
		if ($this->admin_country) {
			$this->db->where("notCountry", $this->admin_country);
		}
		
		$this->db->where('notDeleted', 0);
		
		$this->db->order_by('notId', 'desc');
		$result = $this->db->get("notes");
	
		$notes_data["total_notes"] = $result->num_rows();
		
		if ($notes_data["total_notes"]) {
			foreach ($result->result_array() as $row) {				
				$note = array();
				$note["id"] = $row["notId"];
				$note['title'] = $row["notTitle"];
				$note["preview"] = character_limiter($row["notContent"], 50);
				
				$notes_data["notes"][] = $note;
			}
		}
		
		$subheader_data = array(
			'title' => 'Notas',
			'subtitle' => 'Listado Completo',
		);
		
		$this->load->view('padmin/header', array('title' => 'Notas' ));
		$this->load->view('padmin/subheader', $subheader_data);
		$this->load->view('padmin/notes/list', $notes_data);
		$this->load->view('padmin/footer');
	}
	
	
	private function _get_note($id, $country=false) {
		$note = false;
		
		$this->db->where("notId", $id);
		
		if ($country) {
			$this->db->where("notCountry", $country);
		}
		
		$res = $this->db->get("notes");
		
		if ($res->num_rows()) {
			$row = $res->row_array();
			
			$note = array();
			$note["id"] = $row["notId"];
			$note['title'] = $row["notTitle"];
			$note["content"] = $row["notContent"];
			$note["created"] = date("d/m/Y H:i:s", strtotime($row["notCreated"]));
			$note["modified"] = date("d/m/Y H:i:s", strtotime($row["notModified"]));
			$note["country"] = $row["notCountry"];
			$note["creator"] = $row["admName"];
		}
		
		return $note;
	}
	
	
	public function view($id) {
		$view_data = array();
		$view_data['error'] = false;
		
		$country = $this->admin_country;
		
		$note = $this->_get_note($id, $country);
	
		if ($note) {
			$view_data["note"] = $note;
		} else {
			$view_data['error'] = "not_found";
		}
		
		$subheader_data = array(
			'title' => 'Notas',
			'subtitle' => 'Ver nota',
			'btn_back_url' => site_url("padmin/notes"),
			'btn_back_text' => "Listado de Notas"
		);
		
		$this->load->view('padmin/header', array('title' => 'Ver Nota' ));
		$this->load->view('padmin/subheader', $subheader_data);
		$this->load->view('padmin/notes/view', $view_data);
		$this->load->view('padmin/footer');
		
	}
	
	
	public function create() {
		$this->load->library('country');
		
		$create_data = array();
		$create_data['admin_country'] = $this->admin_country;
		
		$create_data['country'] = $this->admin_country;
		$create_data['country_options'] = array_merge(array("" => "Elige país"), $this->country->get_countries());
		
		$this->form_validation->set_message('required', 'Has olvidado el campo %s.');
		$this->form_validation->set_rules('title', 'Título', 'trim|required');
		$this->form_validation->set_rules('content', 'Contenido', 'trim|required');
		
		if (!$this->admin_country) {
			$this->form_validation->set_rules('country', 'País', 'trim|required');
		}
		
		if ($this->form_validation->run() !== false) {
			$post = $this->input->post();
			
			$this->db->set('notTitle', $post['title']);
			$this->db->set('notContent', $post['content']);
			$this->db->set('notCreated', "'" . date("Y-m-d H:i:s") . "'", false);
			
			$this->db->set('admName', $this->admin_model->get_name());
			
			if ($this->admin_country) {
				$this->db->set('notCountry', $this->admin_country);
			} else {
				$this->db->set('notCountry', $post['country']);
			}
			
			$this->db->insert("notes");
			
			redirect("padmin/notes");
		}
		
		
		$subheader_data = array(
			'title' => 'Notas',
			'subtitle' => 'Crear nota',
			'btn_back_url' => site_url("padmin/notes"),
			'btn_back_text' => "Listado de Notas"
		);
		
		$this->load->view('padmin/header', array('title' => 'Crear Nota' ));
		$this->load->view('padmin/subheader', $subheader_data);
		$this->load->view('padmin/notes/create', $create_data);
		$this->load->view('padmin/footer');
	}
	
	
	public function edit($id) {
		$this->load->library('country');
		
		$edit_data = array();
		$edit_data['note'] = $this->_get_note($id, $this->admin_country);
		
		if ($edit_data['note']) {
			$edit_data['admin_country'] = $this->admin_country;

			$edit_data['country'] = $this->admin_country;
			$edit_data['country_options'] = array_merge(array("" => "Elige país"), $this->country->get_countries());

			$this->form_validation->set_message('required', 'Has olvidado el campo %s.');
			$this->form_validation->set_rules('title', 'Título', 'trim|required');
			$this->form_validation->set_rules('content', 'Contenido', 'trim|required');

			if (!$this->admin_country) {
				$this->form_validation->set_rules('country', 'País', 'trim|required');
			}

			if ($this->form_validation->run() !== false) {
				$post = $this->input->post();

				$this->db->set('notTitle', $post['title']);
				$this->db->set('notContent', $post['content']);
				$this->db->set('notCreated', "'" . date("Y-m-d H:i:s") . "'", false);

				$this->db->set('admName', $this->admin_model->get_name());

				if ($this->admin_country) {
					$this->db->set('notCountry', $this->admin_country);
				} else {
					$this->db->set('notCountry', $post['country']);
				}

				$this->db->where('notId', $id);

				$this->db->update("notes");
				
				redirect("padmin/notes/view/".$id);
			}
		}
		
		$subheader_data = array(
			'title' => 'Notas',
			'subtitle' => 'Editar nota',
			'btn_back_url' => site_url("padmin/notes"),
			'btn_back_text' => "Listado de Notas"
		);
		
		$this->load->view('padmin/header', array('title' => 'Editar Nota' ));
		$this->load->view('padmin/subheader', $subheader_data);
		$this->load->view('padmin/notes/edit', $edit_data);
		$this->load->view('padmin/footer');
	}
	
	
	public function delete($id) {
		$note = $this->_get_note($id, $this->admin_country);
		
		if ($note !== false) {
			$this->db->set("notDeleted", 1);
			$this->db->where("notId", $id);
			$this->db->update("notes");
		}
		
		redirect("padmin/notes");
	}
}

/* end of file */