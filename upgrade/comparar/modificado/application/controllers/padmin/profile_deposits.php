<?php

if ( ! defined('BASEPATH')) exit('No direct script access allowed');

use Melquilabs\Pumbate\Helpers\ProfilePicturesHelper;

class Profile_Deposits extends CI_Controller {

    protected $admin_country = '';
    
    public function __construct() {
        parent::__construct();
    
        $this->load->library('gender');
        $this->load->library('country');
        $this->load->library('billing/deposit_match_service');
        $this->load->library('billing/deposit_query_service');
        $this->load->library('admin/admin_payment_account_service');
        
        $this->load->model('log_model');
        $this->load->model('profile_model');
        $this->load->model('admin_model');
        $this->load->model('payment_account_model');
        $this->load->model('payments_model');
        $this->load->model('deposit_model');
        $this->load->model('customer_message_model');

        if (!$this->admin_model->login_exists()) {
            redirect ('padmin');
        }
        
        $this->admin_country = $this->admin_model->get_country();

        if (!$this->admin_country && !$this->admin_model->get_setting('payments_country')) {
            $this->admin_model->set_setting('payments_country', 'uy');
        } elseif (!$this->admin_model->get_setting('payments_country')) {
            $this->admin_model->set_setting('payments_country', $this->admin_country);
        }
    }

    public function create_deposit($profileId=false)
    {
        if (!$profileId)
        {
            echo 'Undefined create_deposit/{profileId} parameter'; die;
        }

        $viewData = $this->makeCreateDepositViewData();
        $this->renderView('profile_deposits/page_create_deposit', $viewData);      
    }


    public function create_deposit_from_notification($depositNotificationId)
    {
        $viewData = $this->makeCreateDepositViewData();

        $depositMessage = $this->customer_message_model->get_message($depositNotificationId);
        $user = $depositMessage->attributes->deposit->user;

        $this->_fix_picture_rotation($depositMessage);

        $viewData['depositUserSubmittedData'] = $depositMessage->attributes->deposit;

        $viewData['userBillingStatus'] = $this->payments_model
            ->get_profile_payments_status($user);

        $validForm = $this->checkPaymentForm();

        if ($validForm)
        {
            $depositData = $this->createPaymentFromForm($user);
            //var_dump($depositData);
            $success = $this->savePayment($depositData);

            $notificationsLink = $viewData['navigationLinks']['notifications'];

            if ($success) 
            {
                $viewData['flashmessages'][] = [
                    'type' => 'success'
                ,   'message' => 'Pago registrado con éxito! <a href="' .  $notificationsLink . '">Volver a notificaciones</a>'
                ];
            }
        }

        $this->renderView('profile_deposits/page_create_deposit_from_notification', $viewData);
    }


    public function discard_deposit_notification($depositNotificationId)
    {
        $user = null;

        $depositNotification = $this->customer_message_model->get_message($depositNotificationId);
        $depositUser = $depositNotification->attributes->deposit->user;

        $usernames = [$depositUser];

        foreach ($depositNotification->attributes->profiles as $profile)
        {
            $usernames[] = $profile->user;
        }

        $usernames = array_unique($usernames);
        // security, reduce checks to 10 profiles
        $usernames = array_slice($usernames, 0, 10);

        foreach ($usernames as $u)
        {
            $payment_status = $this->payments_model->get_profile_payments_status($u);

            $uProfile = $this->profile_model->get_profile($u);
            //var_dump($uProfile);
            if ($payment_status["days_remaining"] < 1)
            {
                $verificationStatus = PROFILE_STATUS_HIDDEN;

                if (strval($uProfile["accountType"]) === strval(PROFILE_FREE))
                {
                    $verificationStatus = PROFILE_STATUS_NOT_VERIFIED;
                }
                // then we deactivate the profile
                // TODO: support PROFILE_STATUS_FREE in disable_profile
                //$this->profile_model->disable_profile($u);
                $this->db->set('proVerified', $verificationStatus);
                $this->db->where('proUser', $u);
                $this->db->limit(1);
                $this->db->update('profiles');
            }
        }
        
        $eventNotificationId = $this->input->get('eventNotification');

        if ($eventNotificationId)
        {
            $this->log_model->remove_notification($eventNotificationId);
        }

        return redirect("padmin/notifications");
    }

    public function ignore_deposit_notification($depositNotificationId)
    {
        $depositNotification = $this->customer_message_model->get_message($depositNotificationId);

        $eventNotificationId = $this->input->get('eventNotification');

        if ($eventNotificationId)
        {
            $this->log_model->remove_notification($eventNotificationId);
        }

        return redirect("padmin/notifications");
    }


    protected function checkPaymentForm()
    {
        $this->form_validation->set_rules('deposit_payment_account', 'Cuenta de Pago', 'trim|required');
        $this->form_validation->set_rules('deposit_date', 'Fecha', 'trim|required|callback_validate_date');
        $this->form_validation->set_rules('deposit_time', 'Hora', 'trim|required');

        $this->form_validation->set_rules('deposit_amount', 'Cantidad', 'trim|required|numeric|greater_than[99]');

        $this->form_validation->set_rules('deposit_name', 'Número de transacción o autorización', 'trim|required');
        $this->form_validation->set_rules('deposit_concept', 'Concepto', 'trim');
        
        $this->form_validation->set_message('numeric', '%s sólo puede ser numérico.');
        $this->form_validation->set_message('required', 'Este campo es obligatorio.');
        $this->form_validation->set_message('greater_than', 'El depósito debe ser mayor a 100.');
    
        return $this->form_validation->run() !== false;
    }

    protected function createPaymentFromForm($user)
    {
        $post = $this->input->post();

        $paymentAccountId = $post['deposit_payment_account'];
        $paymentAccount = $this->payment_account_model->get_by_id($paymentAccountId);

        if (empty($paymentAccount)) 
        {
            throw new \Exception("invalid payment account: " . $paymentAccountId);
        }

        $deposit = (object) [];
        $deposit->date = $this->transform_date($post['deposit_date']);
        $deposit->name = $post['deposit_name'];
        $deposit->concept = $post['deposit_concept'];
        $deposit->amount = $post['deposit_amount'];
        $deposit->time = $post['deposit_time'];
        $deposit->user = $user;

        $paymentAccountService = $paymentAccount['accountService'];

        $externalIdParts = [ 
            ($paymentAccountService === 'redbrou') ? 'brou' :  $paymentAccountService
        ,   $deposit->date
        ,   intval($deposit->amount)
        ,   $deposit->name 
        ];

        $deposit->externalId = implode("--", $externalIdParts);
        $deposit->paymentAccountId = $paymentAccount['id'];

        return $deposit;
    }

    protected function savePayment($deposit)
    {
        $user = $deposit->user;

        $this->db->set( 'depExternalId', $deposit->externalId );
        $this->db->set( 'depStatus', 'deposited');

        $this->db->set( 'depDate', "'" . $deposit->date  . "'", false );
        $this->db->set( 'depAmount', $deposit->amount);
        $this->db->set( 'depName', $deposit->name);
        $this->db->set( 'depConcept', $deposit->concept);

        $this->db->set('depositTime', $deposit->time);

        $this->db->set( 'paymentAccountId', $deposit->paymentAccountId );

        $now = date("Y-m-d H:i:s");

        $this->db->set( 'createdAt', "'" . $now  . "'", false );
        $this->db->set( 'modifiedAt', "'" . $now  . "'", false );

        $this->db->set( 'proUser', $user);

        if ($this->db->insert('deposits'))
        {
            $depositPaymentId = $this->db->insert_id();

            $this->payments_model->amend_profile_payments_status(
                $user
            );
    
            $this->payments_model->update_profile_last_deposit(
                $user
            );

            $eventNotificationId = $this->input->get('eventNotification');

            if ($eventNotificationId)
            {
                $this->log_model->remove_notification($eventNotificationId);
            }

            return $depositPaymentId;
        }

        return false;
    }

    protected function makeCreateDepositViewData()
    {
        $paymentAccounts = $this->admin_payment_account_service->getAllowedPaymentAccounts([
            'isActive' => true
        ]);

        $viewData = [
            'title' => 'Depositos'
        ,   'subtitle' => 'Registrar depósito'
        ,   'paymentAccounts' => $paymentAccounts
        ,   'depositDefaults' => [
                'concept' => $this->deposit_model->get_default_deposit_concept()
            ]
        ,   'flashmessages' => []
        ,   'navigationLinks' => [
                'notifications' => site_url('padmin/notifications')
            ]
        ];

        return $viewData;
    }

    protected function renderView($viewPath, $viewData)
    {
        $this->load->view('padmin/header', $viewData);
        $this->load->view('padmin/subheader', $viewData);
        $this->load->view('padmin/' . $viewPath, $viewData);
        $this->load->view('padmin/footer');   
    }

    public function validate_user_exists($user)
    {
        $profile = $this->profile_model->get_profile($user);
        return ($profile)? true : false;
    }

    public function validate_date($raw_date)
    {
        if (preg_match('!(\d\d?) */ *(\d\d?) */ *(\d\d\d\d)!', $raw_date))
        {
            return true;
        }

        return false;
    }

    public function transform_date($raw_date)
    {
        if (preg_match('!(\d\d?) */ *(\d\d?) */ *(\d\d\d\d)!', $raw_date, $m))
        {
            return $m[3] . '-' . $m[2] . '-' . $m[1];
        }

        return false;
    }

    protected function _fix_picture_rotation($message)
    {
        $deposit = $message->attributes->deposit;

        if (empty($deposit->photo))
        {
            return false;
        }

        $deposit_photo_path = ltrim($deposit->photo, "/");
        $deposit_photo_optimized_dir = dirname($deposit_photo_path) . '/optimized/';
        $deposit_photo_optimized_path = $deposit_photo_optimized_dir . basename($deposit_photo_path);

        if (!is_dir($deposit_photo_optimized_dir))
        {
            $resize_opts = [
                'max_width' => 1920,
                'max_height' => 1200,
                'jpeg_quality' => 75,
            ];

            ProfilePicturesHelper::create_scaled_image(
                $deposit_photo_path, "optimized", $resize_opts
            );

            ProfilePicturesHelper::orient_image($deposit_photo_optimized_path);
        }

        if (file_exists($deposit_photo_optimized_path))
        {
            $message->attributes->deposit->photo = '/' . $deposit_photo_optimized_path;
        }
    }
}