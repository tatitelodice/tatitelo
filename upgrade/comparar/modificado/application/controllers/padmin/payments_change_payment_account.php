<?php

if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Payments_Change_Payment_Account extends CI_Controller {

	protected $admin_country = '';
	
	public function __construct() {
		parent::__construct();
	
		$this->load->library('gender');
		$this->load->library('country');
		$this->load->library('admin/admin_payment_account_service');
		
		$this->load->model('admin_model');
		$this->load->model('payment_account_model');

		if (!$this->admin_model->login_exists()) {
			redirect ('padmin');
		}

		$this->admin_country = $this->admin_model->get_country();
	}


	public function index()
	{
		$this->confirm_action();
	}


	public function confirm_action()
	{
		$users = $this->input->post("users");

		if (!$users)
		{
			redirect("padmin/payments/list_published");
		}

		$subheader_data = array(
			'title' => 'Pagos',
			'subtitle' => 'Cambiar Cuenta de Pago',
		);

		$done = false;
		
		$payment_account_id = $this->input->post('payment_account_id');

		if ($this->input->post("apply") == "yes") {
			$done = $this->apply_action($users, $payment_account_id);
		}
		
		$this->load->view('padmin/header', array('title' => 'Setear mensaje en panel'));
		$this->load->view('padmin/subheader', $subheader_data);

		$this->load->view('padmin/payments/page_change_payment_account', [
			'done' => $done
		,	'payment_accounts' => $this->admin_payment_account_service->getAllowedPaymentAccounts([
				'isActive' => true
			])
		,	'users' => $users
		]);

		$this->load->view('padmin/footer');
	}


	protected function apply_action($users, $payment_account_id)
	{
		$this->db->set("paymentAccountId", $payment_account_id);
		$this->db->where_in('proUser', $users);
		$done = $this->db->update("payments");

		return $done;
	}
}

/* eof */