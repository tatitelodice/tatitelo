<?php

if ( ! defined('BASEPATH')) exit('No direct script access allowed');

use Melquilabs\Pumbate\Helpers\ProfilePicturesHelper;

class Deposits extends CI_Controller {

    protected $admin_country = '';
    
    public function __construct() {
        parent::__construct();
    
        $this->load->library('gender');
        $this->load->library('country');
        $this->load->library('billing/deposit_match_service');
        $this->load->library('billing/deposit_query_service');
        $this->load->library('admin/admin_payment_account_service');
        
        $this->load->model('profile_model');
        $this->load->model('admin_model');
        $this->load->model('payments_model');
        $this->load->model('deposit_model');
        $this->load->model('customer_message_model');

        if (!$this->admin_model->login_exists()) {
            redirect ('padmin');
        }
        
        $this->admin_country = $this->admin_model->get_country();

        if (!$this->admin_country && !$this->admin_model->get_setting('payments_country')) {
            $this->admin_model->set_setting('payments_country', 'uy');
        } elseif (!$this->admin_model->get_setting('payments_country')) {
            $this->admin_model->set_setting('payments_country', $this->admin_country);
        }
    }


    public function index()
    {
        $this->list_deposits();
    }


    public function list_deposits() {
        
        $payments_country = $this->admin_model->get_setting('payments_country');
            
        if ($payments_country) {
            $country = $payments_country;
        } else {
            redirect('padmin/payments');
        }

        $paymentAccounts = $this->admin_payment_account_service->getAllowedPaymentAccounts();
        $paymentAccountIds = collect($paymentAccounts);

        $specificPaymentAccountId = $this->input->get('paymentAccount');

        if ($specificPaymentAccountId)
        {
            $paymentAccountIds = $paymentAccountIds->where('id', $specificPaymentAccountId);
        }

        $paymentAccountIds = $paymentAccountIds->pluck('id')->toArray();
        

        $depositsReport = $this->deposit_query_service->makeReport([
            'startDate' => $this->input->get('startDate')
        ,   'endDate' => $this->input->get('endDate')
        ,   'paymentAccountIds' => $paymentAccountIds
        ,   'includeNullAccount' => empty($specificPaymentAccountId)
        ]);

        $viewData = [
            'title' => 'Pagos',
            'subtitle' => 'Lista de Depósitos',
            'btn_back' => '<a class="btn pull-right" href="' . site_url('padmin/payments') . '">Volver a Listado de Pagos</a>'
        ];

        $viewData['default_deposit_concept'] = $this->deposit_model->get_default_deposit_concept();
        $viewData["admin_country"] = $this->admin_country;
        $viewData['startDate'] = date('d/m/Y', strtotime($depositsReport['startDate']));
        $viewData['endDate'] = date('d/m/Y', strtotime($depositsReport['endDate']));
        $viewData['totalAmount'] = $depositsReport['totalAmount'];
        $viewData['deposits'] = $depositsReport['deposits'];
        $viewData['defaultUser'] = $this->input->get("defaultUser");
        
        $this->load->view('padmin/header', $viewData);
        $this->load->view('padmin/subheader', $viewData);
        $this->load->view('padmin/deposits/page_list_deposits', $viewData);
        $this->load->view('padmin/footer');
    }
    
    public function view_details($depositId)
    {
        $deposits = $this->deposit_model->find_by([
            "depId" => $depositId
        ]);

        $deposit = (object) $deposits[0];

        $actionLinks = [
            'voidDepositValue' => site_url('padmin/deposits/void_deposit_value/' . $deposit->id)
        ];

        $viewData = [
            'title' => 'Pagos',
            'subtitle' => 'Ver Depósito',
            'btn_back' => '<a class="btn pull-right" href="' . site_url('padmin/deposits') . '">Volver a Listado de Depósitos</a>',
            'actionLinks' => $actionLinks,
            'deposit' => $deposit
        ];

        $this->load->view('padmin/header', $viewData);
        $this->load->view('padmin/subheader', $viewData);
        $this->load->view('padmin/deposits/page_view_deposit_details', $viewData);
        $this->load->view('padmin/footer');
    }


    public function void_deposit_value($depositId)
    {
        $this->db->set('depAmount', 0);
        $this->db->where('depId', $depositId);
        $this->db->limit(1);
        $this->db->update('deposits');
        return redirect('padmin/deposits/view_details/' . $depositId);
    }

    public function view_notification($messageId)
    {
        $message = $this->customer_message_model->get_message($messageId);
        $this->_fix_picture_rotation($message);
        
        $eventNotificationId = $this->input->get('eventNotification');

        $actionLinks = [
            'createDepositPayment' => site_url("padmin/profile_deposits/create_deposit_from_notification/" . $message->id) . '?eventNotification=' . $eventNotificationId
        ,   'discardDepositNotification' => site_url("padmin/profile_deposits/discard_deposit_notification/" . $message->id) . '?eventNotification=' . $eventNotificationId
        ,   'ignoreEventNotification' => site_url("padmin/profile_deposits/ignore_deposit_notification/" .  $message->id) . '?eventNotification=' . $eventNotificationId
        ];

        $usernames = [];

        $userBillingStatus = [];
        $userBillingLinks = [];

        if (!empty($message->attributes->profiles) && strlen($message->customerAddress) > 8)
        {
            foreach ($message->attributes->profiles as $messageProfile)
            {
                $usernames[] = $messageProfile->user;
            }
        }
        else
        {
            $usernames[] = $message->attributes->deposit->user;
        }

        foreach ($usernames as $user)
        {
            $userBillingStatus[$user] = $this->payments_model->get_profile_payments_status($user);

            $userBillingLinks[$user] = site_url(
                'padmin/payments/view/' . $user . '?ref=payments'
            );
        }

        $this->load->view('padmin/header', [
            'title' => 'Notificación de Depósito' 
        ]);

        $this->load->view('padmin/subheader', [
            'title' => 'Notificación de Depósito',
            'subtitle' => '',
            'btn_back_url' => site_url("padmin/messages"),
            'btn_back_text' => "Listado de Mensajes"
        ]);
        
        $this->load->view('padmin/deposits/page_view_deposit_notification', [
            'message' => $message
        ,   'deposit' => $message->attributes->deposit
        ,   'actionLinks'   => $actionLinks
        ,   'userBillingLinks' => $userBillingLinks
        ,   'userBillingStatus' => $userBillingStatus
        ]);

        $this->load->view('padmin/footer');
    }
    

    public function new_profile_deposit($user) {
        $result = false;
        
        $this->form_validation->set_rules('deposit_date', 'Fecha', 'trim|required|callback_validate_date|callback_transform_date');

        $this->form_validation->set_rules('deposit_amount', 'Cantidad', 'trim|required|numeric|greater_than[99]');

        $this->form_validation->set_rules('deposit_name', 'Nombre', 'trim|required');
        $this->form_validation->set_rules('deposit_concept', 'Concepto', 'trim');
        
        $this->form_validation->set_message('numeric', '%s sólo puede ser numérico.');
        $this->form_validation->set_message('required', 'Has olvidado %s.');
        $this->form_validation->set_message('greater_than', 'El depósito debe ser mayor a 100.');
        
        if ( $this->form_validation->run() !== false ) {
            $post = $this->input->post();
            
            $this->db->set( 'depDate', "'" . $post['deposit_date']  . "'", false );

            if (isset($post['time']))
            {
                $this->db->set('depositTime', $deposit->time);
            }

            $this->db->set( 'depAppliedAt', "'" . $post['deposit_date']  . "'", false );

            $this->db->set( 'depAmount', $post['deposit_amount']);
            $this->db->set( 'depName', $post['deposit_name']);
            $this->db->set( 'depConcept', $post['deposit_concept']);
            $this->db->set( 'proUser', $user);

            $now = date("Y-m-d H:i:s");

            $this->db->set( 'createdAt', "'" . $now  . "'", false );
            $this->db->set( 'modifiedAt', "'" . $now  . "'", false );

            $result = $this->db->insert('deposits');

            $this->payments_model->update_profile_last_deposit($user);
        }
        
        $this->json_response(array(
            'success' => $result
        ));
    }

    public function get_profile_deposits($user)
    {
        $result = array(
            'success' => false
        );

        if (!$user) {
            $result['error'] = 'no user specified';
            return $this->json_response($result);
        }

        $profile = $this->profile_model->get_full_profile($user);

        if (!$profile) {
            $result['error'] = 'invalid user';
            return $this->json_response($result);
        }

        $deposits = $this->deposit_model->get_profile_deposits($user);
        
        $result['success'] = true;
        $result['deposits'] = $deposits;

        return $this->json_response($result);
    }


    public function test_amend($user)
    {
        $this->payments_model->amend_profile_payments_status($user);
    }


    public function apply_deposit()
    {
        $this->form_validation->set_rules('deposit_id', 'Id', 'trim|required');

        $is_from_non_user = $this->input->post('deposit_from_non_user');
        
        if (!$is_from_non_user)
        {
            $this->form_validation->set_rules('deposit_user', 'Usuario', 'trim|required|strtolower|callback_validate_user_exists');
        }
        
        $this->form_validation->set_rules('deposit_apply_date'
        ,   'Fecha de acreditado'
        ,   'trim|required|callback_validate_date|callback_transform_date'
        );
        
        $this->form_validation->set_rules('deposit_concept', 'Concepto', 'trim');

        $this->form_validation->set_message('required', 'Has olvidado %s.');
        $this->form_validation->set_message('validate_user_exists', 'El usuario no existe');
        $this->form_validation->set_message('validate_date', '%s inválida');
        $this->form_validation->set_message('transform_date', '%s inválida');
                
        if ( $this->form_validation->run() !== false) {
            $deposit_data = $this->input->post();

            if ($is_from_non_user)
            {
                $deposit_data['deposit_user'] = null;
            }

            $this->deposit_model->apply_deposit(
                $deposit_data['deposit_id']
            ,   array(
                    'user' => $deposit_data['deposit_user']
                ,   'concept' => $deposit_data['deposit_concept']
                ,   'applied_at' => $deposit_data['deposit_apply_date']
                )
            );
            
            if ($deposit_data['deposit_user'])
            {
                $this->payments_model->amend_profile_payments_status(
                    $deposit_data['deposit_user']
                );

                $this->payments_model->update_profile_last_deposit(
                    $deposit_data['deposit_user']
                );
            }

            $result = json_encode(array(
                'success' => true
            ));
        }
        else
        {
            $result = json_encode(array(
                'success' => false
            ,   'error' => $this->form_validation->error_string()
            ));
        }

        echo $result;
    }


    public function resolve_owners()
    {
        $match_list = $this->deposit_match_service->assign_deposits_to_profiles();
        var_dump($match_list);
    }


    public function validate_user_exists($user)
    {
        $profile = $this->profile_model->get_profile($user);
        return ($profile)? true : false;
    }

    public function validate_date($raw_date)
    {
        if (preg_match('!(\d\d?) */ *(\d\d?) */ *(\d\d\d\d)!', $raw_date))
        {
            return true;
        }

        return false;
    }

    public function transform_date($raw_date)
    {
        if (preg_match('!(\d\d?) */ *(\d\d?) */ *(\d\d\d\d)!', $raw_date, $m))
        {
            return $m[3] . '-' . $m[2] . '-' . $m[1];
        }

        return false;
    }

    protected function _fix_picture_rotation($message)
    {
        $deposit = $message->attributes->deposit;

        if (empty($deposit->photo))
        {
            return false;
        }

        $deposit_photo_path = ltrim($deposit->photo, "/");
        $deposit_photo_optimized_dir = dirname($deposit_photo_path) . '/optimized/';
        $deposit_photo_optimized_path = $deposit_photo_optimized_dir . basename($deposit_photo_path);

        if (!is_dir($deposit_photo_optimized_dir))
        {
            $resize_opts = [
                'max_width' => 1920,
                'max_height' => 1200,
                'jpeg_quality' => 75,
            ];

            ProfilePicturesHelper::create_scaled_image(
                $deposit_photo_path, "optimized", $resize_opts
            );

            ProfilePicturesHelper::orient_image($deposit_photo_optimized_path);
        }

        if (file_exists($deposit_photo_optimized_path))
        {
            $message->attributes->deposit->photo = '/' . $deposit_photo_optimized_path;
        }
    }

    protected function json_response($data)
    {
        header('Content-Type: application/json');
        echo json_encode($data);
        die;
    }
}

/* end of file */
