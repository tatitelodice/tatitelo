<?php

if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Logs extends CI_Controller {

	public function __construct() {
		parent::__construct();
		$this->load->library('session');
		
		$this->load->model('admin_model');
		
		if (!$this->admin_model->login_exists()) {
			redirect ('padmin');
		}
	}	
	
	public function index() {	
		$this->_list_logs();
	}
	
	
	private function _list_logs() {
		$admin_country = $this->admin_model->get_country();
		
		$subheader = array(
			'title' => 'Logs',
			'subtitle' => 'Listado Completo',
			'btn_back' => '<a class="btn pull-right" href="' . site_url('padmin') . '">Página Principal</a>'
		);

		$this->load->view('padmin/header', array('title' => 'Logs'));
		$this->load->view('padmin/subheader', $subheader);
		
		$data['table'] = false;
		
		$user = $this->input->get('user');
		
		if ($user) {
			$this->db->where('proUser', $user);
		}
		
		if ($admin_country) {
			$this->db->where('logCountry', $admin_country);
		}
		
		$this->db->order_by('logId', 'desc');
		$this->db->limit(1000);
		$res = $this->db->get('logs');
		
		if ($res->num_rows()) {
			
			$dbresults = $res->result_array();
			$table_blacklist = array();
			
			foreach ($dbresults as $dbrow) {
				
				$row_blacklist = array(
					$dbrow['logDate'],
					$dbrow['logCountry'],
					$dbrow['proUser'],
					$dbrow['logDescription'],
					$dbrow['logAddress'],	
					$dbrow['logNotify'],
					anchor("padmin/logs/details/".$dbrow["logId"], "Ver"),
				);
				
				if ($admin_country) {
					unset($row_blacklist[1]);
				}
				
				$table_blacklist[] = $row_blacklist;
			}
			
			$this->load->library('table');
			$this->table->set_template( array( 'table_open' => '<table class="table table-condensed table-striped">',));
			
			if ($admin_country) {
				$this->table->set_heading( 'Fecha', 'Perfil', 'Descripción', 'IP', 'Alerta', 'Información Extra');
			} else {
				$this->table->set_heading( 'Fecha', 'País', 'Perfil', 'Descripción', 'IP', 'Alerta', 'Información Extra');
			}

			
			$data['table'] = $this->table->generate($table_blacklist);
			
		}
		
		$data['admin_country'] = $admin_country;
		$this->load->view('padmin/logs', $data);
		$this->load->view('padmin/footer');
	}
	
	public function details($id) {
		$this->db->where("logId", $id);
		$res = $this->db->get("logs");
		
		if ($res->num_rows()) {
			$row = $res->row();
			
			$metadata = $row->logMeta;
			
			$json_dec = json_decode($metadata, true);
			
			if (is_array($json_dec)) {
				echo "<pre>";
				print_r($json_dec);
				echo "</pre>";
			}
		}
	}
	
}

/* end of file */