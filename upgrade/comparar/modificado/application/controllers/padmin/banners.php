<?php

if (!defined('BASEPATH')) exit('No direct script access allowed');


class Banners extends CI_Controller {

    protected $admin_country;
	
    protected $upload_path;
    

    public function __construct() {
        parent::__construct();

		$this->load->library('country');
        $this->load->model('admin_model');
		$this->load->model('banner_model');
		
		$this->load->library('webfront/banner_service');
		
        if (!$this->admin_model->login_exists()) {
            redirect('padmin');
        }

        $this->admin_country = $this->admin_model->get_country();

		$this->_init_paths();
    }

	
	private function _init_paths() {
        $this->upload_path = "sitedata/banners";
        @mkdir($this->upload_path, 0777, true);
	}
	
	
    public function index() {
        $this->list_banners();
    }
	
	
	public function list_banners() {
		$this->load->helper('text');

		$section_data = array();
		
		$section_data['admin_country'] = $this->admin_country;
		
		$section_data['success_message'] = $this->session->flashdata('success_message');
		$section_data['error_message'] = $this->session->flashdata('error_message');
				
		$section_data['banner_enabled_options'] = $this->banner_model->get_enabled_enum();
		
		$section_data['list_elements'] = array();

        $list_params = array(
			'where'    =>  array(
				'deleted' => 0,
			),
			'order_by' => 'country asc, category asc, enabled desc, position asc',
		);

        if ($this->admin_country) {
            $list_params['where']['country'] = $this->admin_country;
        }
		
        $section_data["total_list_elements"] = $this->banner_model->count_all_banner($list_params);

        if ($section_data["total_list_elements"]) {
			$section_data['list_elements'] = $this->banner_model->get_all_banner($list_params);
        }
		
		$section_data['visible_list_elements'] = count($section_data['list_elements']);

		$section_data['device_visibility_options'] = $this->banner_model->get_device_visibility_enum();
		
        $subheader_data = array(
            'title' => 'Banners',
            'subtitle' => 'Listado Completo',
        );

        $this->load->view('padmin/header', array('title' => 'Banners'));
        $this->load->view('padmin/subheader', $subheader_data);
        $this->load->view('padmin/banners/list_banners', $section_data);
        $this->load->view('padmin/footer');
	}
	
	public function edit_banner($id) {
		
		$banner = $this->banner_model->get_banner($id);
		
		if ($banner) {
			$this->_check_restricted_country($banner['country']);
		} else {
			$this->session->set_flashdata('error_message', 'El banner de id ' . $id . ' no existe.' );
			redirect('padmin/banners');
		}
		
		$section_data = $this->_init_banner_section_data();
		$section_data['banner'] = $banner;
		
		if ($this->input->post()) {
			$this->_set_form_rules();
						
			if ($this->form_validation->run() !== false) {
				$post = $this->input->post();

				if ($this->admin_country) {
					$post['country'] = $this->admin_country;
				}

				if ($_FILES['picture']['error'] === UPLOAD_ERR_NO_FILE) {
					//echo 'mantener'; die;
					$rel_pic_path = $banner['settings']['main_picture'];
				} else {
					$upload_result = $this->_check_picture_upload();
					$section_data['upload_error'] = $upload_result['upload_error'];
					$upload_data = $upload_result['upload_data'];
					$rel_pic_path = $this->_reallocate_picture($id, $upload_data);
				}
			
				$post['settings'] = array(
					'main_picture' => $rel_pic_path,
					'device_visibility' => $post['device_visibility'],
					'open_in'           => $post['open_in'],
				);

				$success = $this->banner_model->update_banner($id, $post);

				if ($success) {
					$this->banner_service->save_banners_into_cache($post['country'], $post['category']);
					$success_message  = 'El banner ha sido editado con éxito.<br>';
					$success_message .= '<a style="color: inherit" href="'.site_url('padmin/banners').'"><b>Volver a listado de banners</b></a>';
					
					$this->session->set_flashdata('success_message', $success_message);
					
				} else {
					$this->session->set_flashdata('error_message', 'Error al editar el banner.');
				}
				
				redirect(current_url());

			}
		}
		
		$subheader_data = array(
            'title' => 'Banners',
            'subtitle' => 'Editar banner',
        );
		
        $this->load->view('padmin/header', array('title' => 'Banners'));
        $this->load->view('padmin/subheader', $subheader_data);
        $this->load->view('padmin/banners/edit_banner', $section_data);
        $this->load->view('padmin/footer');	
	}
	
	
	public function create_banner() {
		$section_data = $this->_init_banner_section_data();
		
		if ($this->input->post()) {
			$this->_set_form_rules();
			
			$upload_result = $this->_check_picture_upload();
			
			//print_r($upload_result); die;
			
			$section_data['upload_error'] = $upload_result['upload_error'];
			
			if ( ($this->form_validation->run() !== false) && $upload_result['success']) {
				$post = $this->input->post();

				if ($this->admin_country) {
					$post['country'] = $this->admin_country;
				}
				
				if (!$post['position']) {
					$post['position'] = 1;
				}

				$banner_id = $this->banner_model->create_banner($post);
				
				if ($banner_id) {
					$upload_data = $upload_result['upload_data'];
					$rel_pic_path = $this->_reallocate_picture($banner_id, $upload_data);
					
					$post['settings'] = array(
						'main_picture' => $rel_pic_path,
						'device_visibility' => $post['device_visibility'],
						'open_in'           => $post['open_in'],
					);
					
					$this->banner_model->update_banner($banner_id, $post);
					
					$this->banner_service->save_banners_into_cache($post['country'], $post['category']);
				
					$this->session->set_flashdata('success_message', 'El banner ha sido creado con éxito!');
					redirect('padmin/banners');
				}
			}
		}
		
		$subheader_data = array(
            'title' => 'Banners',
            'subtitle' => 'Crear banner',
        );
		
        $this->load->view('padmin/header', array('title' => 'Banners'));
        $this->load->view('padmin/subheader', $subheader_data);
        $this->load->view('padmin/banners/create_banner', $section_data);
        $this->load->view('padmin/footer');		
	}
	
	
	protected function _set_form_rules() {
		$this->form_validation->set_message('required', 'Has olvidado el campo %s.');
		$this->form_validation->set_rules('title', 'título', 'trim|required');
		$this->form_validation->set_rules('description', 'contenido', 'trim');
		$this->form_validation->set_rules('country', 'país', 'trim|required|max_length[2]|min_length[2]');
		$this->form_validation->set_rules('category', 'tipo', 'trim|required');
		$this->form_validation->set_rules('url', 'url', 'trim|required');
		$this->form_validation->set_rules('enabled', 'publicado', 'trim|required');
		$this->form_validation->set_rules('position', 'posición', 'is_natural');
	}
	
	
	protected function _init_banner_section_data() {
		$section_data = array();
		
		$section_data['admin_country'] = $this->admin_country;
		
		$section_data['success_message'] = $this->session->flashdata('success_message');
		$section_data['error_message'] = $this->session->flashdata('error_message');
		
		$section_data['upload_done']  = false;
		$section_data['upload_error'] = false;
		
		$section_data['country_options'] = $this->country->get_countries();
		$section_data['category_options'] = $this->banner_model->get_category_enum();
		$section_data['enabled_options'] = array_map('ucfirst', $this->banner_model->get_enabled_enum());
		$section_data['device_visibility_options'] = $this->banner_model->get_device_visibility_enum();
		
		$section_data['form'] = array();
		$section_data['form']['country_default'] = $this->admin_country;
		$section_data['form']['enabled_default'] = 1;
		$section_data['form']['device_visibility_default'] = $this->banner_model->get_device_visibility_enum();
		
		return $section_data;
	}
	
	

	protected function _check_picture_upload() {
		$result = array();
		$result['success'] = false;
		$result['upload_error'] = false;
		$result['upload_data']   = array();
		
		$upload_config['upload_path'] = $this->upload_path;
		$upload_config['allowed_types'] = 'gif|jpg|jpeg|bmp|png';
		$upload_config['encrypt_name']	= true;
		$upload_config['overwrite']	= true;
		$upload_config['remove_spaces'] = true;

		$this->load->library('upload', $upload_config);
		
		if ($this->upload->do_upload("picture")) {
			$result['success'] = true;
			$result['upload_data'] = $this->upload->data();
		} else {
			$result['upload_error'] = $this->upload->display_errors('', '');
		}

		return $result;
	}
	
	protected function _reallocate_picture($banner_id, $upload_data) {
		$old_path = $upload_data['full_path'];

		$new_dir_path =  $this->upload_path . '/' . str_pad($banner_id, 5, '0', 0); 
		@mkdir($new_dir_path);
		
		//echo $new_dir_path; die;

		$new_file_name = md5($upload_data['orig_name']) . $upload_data['file_ext'];

		$new_path = $new_dir_path . '/' . $new_file_name; 
		
		rename($old_path, $new_path);
		
		return $new_path;
	}
	
	
	private function _check_restricted_country($section_country) {
		$admin_country = $this->admin_model->get_country();

		if (strlen($admin_country) == 2 && $section_country != $admin_country) {
			$forbidden_message = "Tu cuenta de usuario administrador tiene acceso restringido a esta página. Si no es el caso, contáctate con el encargado técnico.";
			$forbidden_message .= "<br><br>" . anchor("padmin/profiles", "Volver al Listado de Cuentas");
			show_error($forbidden_message, 403, "No puedes administrar esta sección");
			die;
		}
	}

}

/* end of file */