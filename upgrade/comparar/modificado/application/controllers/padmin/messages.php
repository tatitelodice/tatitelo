<?php

if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Messages extends CI_Controller {

	public function __construct() {
		parent::__construct();
		
		$this->load->library("human_useragent");
		$this->load->library('gender');
		$this->load->library('sdate');

		$this->load->helper("profile_helper");
		
		$this->load->model('profile_model');
        $this->load->model('customer_message_model');

		$this->load->model('admin_model');
		
		if (!$this->admin_model->login_exists()) {
			redirect ('padmin');
		}
	}	
	
	public function index() {		
		$this->load->view('padmin/header', array('title' => 'Mensajes' ));
		
		$subheader = array(
			'title' => 'Mensajes',
			'subtitle' => 'Listado Completo',
			'btn_back' => '<a style="float: right;" class="btn" href="' . site_url('padmin') . '">Página Principal</a>'
		);

		$section_data = array();

		$offset = $this->input->get('offset');
		$offset = ($offset) ? $offset : 0;
		$offset_limit = 50;
		
		$this->db->where('category', 'support');
		$this->db->where('messageBox', 'inbox');
		$this->db->where('isDeleted', 0);
		//this->db->where('isPendingRead', 1);
		$this->db->limit($offset_limit, $offset);
		
		$this->db->order_by('id', 'desc');

		$customer_messages = array();

		foreach ($this->db->get('customer_message')->result() as $cm)
		{
			$cm->attributes = json_decode($cm->attributes);
			$customer_messages[] = $cm;
		}

		$section_data["customer_messages"] = $customer_messages;

		$section_data['offset'] = $offset;
		$section_data['offset_limit'] = $offset_limit;

		$this->load->view('padmin/subheader', $subheader);
		$this->load->view('padmin/messages/index', $section_data);
		$this->load->view('padmin/footer');
		
	}
	

    public function outbox()
    {
        $header = ['title' => 'Mensajes - Bandeja de Salida'];
        
        $subheader = [
            'title' => 'Mensajes'
        ,   'subtitle' => 'Bandeja de Salida'
        ];

        $offset = ($this->input->get('offset')) ? $this->input->get('offset') : 0;
        $limit = ($this->input->get('limit')) ? $this->input->get('limit') : 1000;
        $messages_status = $this->input->get('status');
        $sort_by = str_replace('-', ' ', $this->input->get('sort'));

        $messages = $this->customer_message_model->get_outbox_messages([
            'offset' => $offset
        ,   'limit' => $limit
        ,   'status' => $messages_status
        ,   'sort' => $sort_by
        ]);

        $section_data['outbox_messages'] = $messages;
        $section_data['offset'] = $offset;
        $section_data['limit'] = $limit;
        $section_data['status_filter'] = $messages_status;

        $this->load->view('padmin/header', $header);
        $this->load->view('padmin/subheader', $subheader);
        $this->load->view('padmin/messages/outbox', $section_data);
        $this->load->view('padmin/footer');
    }


	public function write()
	{
        $message_address = '';
        $message_to_reply = null;
        $message_to_reply_id = $this->input->get('replyTo');

        if ($message_to_reply_id)
        {
            $message_to_reply = $this->customer_message_model->get_message($message_to_reply_id);
        }

        if ((!empty($message_to_reply)))
        {
            $message_address = $message_to_reply->customerAddress;
        }

        $this->form_validation->set_rules('message_address', 'Número', 'trim|required');
        $this->form_validation->set_rules('message_text', 'Nombre', 'trim|required');


        if ( $this->form_validation->run() !== false ) {
            $post = $this->input->post();

            $post['message_address'] = preg_split('!\r?\n|\t|,!', $post['message_address']);

            $this->customer_message_model->create_outbox_message([
                'message_address' => $post['message_address']
            ,   'message_text' => $post['message_text']
            ,   'message_category' => $post['message_category']
            ]);                
        
            redirect(site_url('padmin/messages/outbox') . '?status=pending');
        }
        
		$this->load->view('padmin/header', [
			'title' => 'Escribir Mensaje' 
		]);

		$this->load->view('padmin/subheader', [
			'title' => 'Mensajes',
			'subtitle' => 'Escribir Mensaje',
			'btn_back_url' => site_url("padmin/messages"),
			'btn_back_text' => "Listado de Mensajes"
		]);
		
        $this->load->view('padmin/messages/write', [
            'default_message_category' => 'support'
        ,   'message_category_options' => $this->_get_message_categories()
        ,   'message_to_reply' => $message_to_reply
        ,   'message_address' => $message_address
        ]);

		$this->load->view('padmin/footer');
	}


	public function mark_as_read($logId) {
		$result = 'failed';
		
		$is_ajax = $this->input->get('ajax') === 'true';
		$action_allowed = !$this->admin_model->get_setting('disallow_profile_upgrading');
		
		if ($action_allowed) {			
			$this->db->set('logNotify', 0);
			$this->db->where('logId', $logId);
			$this->db->update('logs');
			$result = 'success';	
		} else {
			$result = 'disallowed';
		}
				
		if ($is_ajax) {
			echo $result;
		} else {
			redirect('padmin/notifications');
		}
	}


    protected function _get_message_categories()
    {
        return array(
            'support' => 'Soporte'
        );
    }

    protected function json_response($data)
    {
        header('Content-Type: application/json');
        echo json_encode($data);
        die;
    }
	
}

/* end of file */
