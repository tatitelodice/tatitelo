<?php

if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Blacklist extends CI_Controller {

	public function __construct() {
		parent::__construct();
		
		$this->load->model('profile_model');
		$this->load->model('admin_model');
		
		if (!$this->admin_model->login_exists()) {
			redirect ('padmin');
		}
	}	
	
	public function index() {	
		
		$data = array();
		$data['error'] = false;
		
		if ($this->input->post('add_action')) {
			if ($this->_add_blacklisted($data)) {
				redirect('padmin/blacklist');
			}
		}
		
		$this->_list_blacklisted($data);
	}
	
	
	private function _list_blacklisted(&$data) {
		$subheader = array(
			'title' => 'Lista Negra',
			'subtitle' => 'Listado Completo',
			'btn_back' => '<a style="float: right;" class="btn" href="' . site_url('padmin') . '">Página Principal</a>'
		);

		$this->load->view('padmin/header', array('title' => 'Lista Negra'));
		$this->load->view('padmin/subheader', $subheader);

		$fields = array('name', 'phone', 'email', 'description');
		
		foreach ($fields as $f) {
			$data[$f] = set_value($f, $this->input->get($f));
		}
		
		$data['values_from_get'] = count($this->input->get()) > 0;
		
		$data['table'] = false;
		
		$this->db->where('blaDeleted', 0);
		$this->db->order_by('blaId', 'desc');
		$res = $this->db->get('blacklist');
		
		if ($res->num_rows()) {
			
			$dbresults = $res->result_array();
			$table_blacklist = array();
			
			foreach ($dbresults as $dbrow) {
				
				$row_blacklist = array(
					$dbrow['blaPhone'],
					$dbrow['blaEmail'],
					$dbrow['blaName'],
					$dbrow['blaDescription'],
					$dbrow['blaDate'],
					'<div class="text-right">' . anchor('padmin/blacklist/delete/'.$dbrow['blaId'], '<i class="glyphicon glyphicon-remove"></i>', 'title="quitar"') . '</div>',
				);
				
				$table_blacklist[] = $row_blacklist;
			}
			
			$this->load->library('table');
			$this->table->set_template( array( 'table_open' => '<table class="table table-condensed table-striped">',));
			$this->table->set_heading( 'Teléfono', 'Email', 'Nombre', 'Motivo', 'Fecha', '');

			
			$data['table'] = $this->table->generate($table_blacklist);
			
		}
		
		$data['admin_country'] = $this->admin_model->get_country();
		$this->load->view('padmin/blacklist', $data);
		$this->load->view('padmin/footer');
	}
	
	private function _add_blacklisted(&$data) {
		
		$this->form_validation->set_message('required', 'Ha puesto el campo %s como vacío.');
		$this->form_validation->set_message('valid_email', 'El e-mail ingresado no está bien escrito.');
		$this->form_validation->set_message('integer', '%s debe tener un valor numérico.');
		
		$this->form_validation->set_rules('name', 'Nombre', 'trim|required');
		$this->form_validation->set_rules('phone', 'Telefono', 'trim|integer');
		$this->form_validation->set_rules('email', 'E-mail', 'trim|valid_email');
		$this->form_validation->set_rules('description', 'Motivo', 'trim|required');
		
		if ($this->form_validation->run() !== false) {
			$post = $this->input->post();
			
			$this->db->set('blaName', $post['name']);
			$this->db->set('blaDescription', $post['description']);
			$this->db->set('blaPhone', $post['phone']);
			$this->db->set('blaEmail', $post['email']);
			$this->db->set('blaDate', "'" . date('Y-m-d') . "'", false);
			
			$this->db->insert('blacklist');
			
			return true;
		} else {
			$data['error'] = true;
		}
		
		return false;
	}
	
	public function delete($blaId = false) {
		
		if ($blaId) {
			$this->db->set('blaDeleted', 1);
			$this->db->where('blaId', $blaId);
			$this->db->update('blacklist');
		}
		
		redirect('padmin/blacklist');
		
	}
	
	
	public function clear_user($user) {
		
		$profile = $this->profile_model->get_full_profile($user);
		
		if ($profile) {
			$this->db->select("logAddress,logDate");
			$this->db->where("proUser", $user);
			$this->db->where("logDescription", "profile_created");
			$res = $this->db->get("logs");

			// TODO CONTROLAR MULTIPLE IPS Y LOCKS AUN VALIDOS
			
			if ($res->num_rows()) {
				$row = $res->row();
				$ip = $row->logAddress;
				$date = date("Ymd", strtotime($row->logDate));

				/* REMOVE SECURITY LOCKS */
				$this->load->model("temp_model");

				$this->temp_model->delete_temp("profile_create::" . $date . "::" . $ip);
				$this->temp_model->delete_temp("profile_create::lock::" . $ip);
			}
			
			
			if ($profile['proEmail']) {
				$this->db->where('blaEmail', $profile['proEmail']);
				$this->db->set('blaDeleted', 1);
				$this->db->update('blacklist');
			}
			
			if (strlen($profile['proPhone']) > 6) {
				$this->db->where('blaPhone', $profile['proPhone']);
				$this->db->or_where('blaPhone', $profile['proNumericPhones']);
				$this->db->set('blaDeleted', 1);
				$this->db->update('blacklist');
			}
			
			/* REMOVE BAN ASSOCIATED LOCKS */
			
			echo "Eliminados todos los bans de " .  $user . " con éxito.";
			
		} else {
			echo "El usuario " . $user . " no existe.";
		}
	}
	
}

/* end of file */
