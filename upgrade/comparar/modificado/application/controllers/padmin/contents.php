<?php

if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Contents extends CI_Controller {

	protected $admin_country;
	
	protected $upload_root;
	protected $upload_dir;
	
	protected $thumb_dirs;
	
	
	public function __construct() {
		parent::__construct();
		$this->load->library('session');
		
		$this->load->model('admin_model');
		$this->load->model('contents_model');
		
		if (!$this->admin_model->login_exists()) {
			redirect ('padmin');
		}
		
		$this->admin_country = $this->admin_model->get_country();
		
		$this->upload_root = "sitedata/";
		$this->thumb_dirs = array("optimized", "thumbnail", "w200");
	}	
	
	
	public function index() {	
		$this->list_pages();
	}
	
	
	public function list_pages() {
		$this->load->helper('text');
		
		$pages_data["pages"] = array();
		
		$this->db->select("pagId,pagTitle,pagCountry,pagExcerpt,pagStatus,pagType,pagDate,pagCommentCount,admName");
		$this->db->join('administrators', 'administrators.admId = pages.admId');
		
		if ($this->admin_country) {
			$this->db->where("pagCountry", $this->admin_country);
		}
		
		$this->db->where('pagDeleted', 0);
		
		$this->db->order_by('pagId', 'desc');
		$result = $this->db->get("pages");
	
		$pages_data["total_pages"] = $result->num_rows();
		
		if ($pages_data["total_pages"]) {
			foreach ($result->result_array() as $row) {				
				$page = array();
				
				foreach($row as $field_name => $field_value) {
					$field_name = preg_replace("!^pag!", "", $field_name);
					$field_name[0] = strtolower($field_name[0]);
					$page[$field_name] = $field_value;
				}
				
				$page["author"] = $row["admName"];
				$page["date_only"] = substr($page["date"], 0, 10);
				
				$pages_data["pages"][] = $page;
			}
		}
		
		$subheader_data = array(
			'title' => 'Contenidos',
			'subtitle' => 'Listado Completo',
		);
		
		$this->load->view('padmin/header', array('title' => 'Contenidos' ));
		$this->load->view('padmin/subheader', $subheader_data);
		$this->load->view('padmin/contents/pages/list', $pages_data);
		$this->load->view('padmin/footer');
	}
	
	
	private function _get_page($id, $country=false) {
		$page = false;
		
		$this->db->select("pages.*, administrators.admName as author");
		$this->db->join('administrators', 'administrators.admId = pages.admId');
		$this->db->where("pagId", $id);
		
		if ($country) {
			$this->db->where("pagCountry", $country);
		}
		
		$res = $this->db->get("pages");
		
		if ($res->num_rows()) {
			$row = $res->row_array();
			
			foreach($row as $field_name => $field_value) {
				$field_name = preg_replace("!^pag!", "", $field_name);
				$field_name[0] = strtolower($field_name[0]);
				$page[$field_name] = $field_value;
			}
		}
		
		return $page;
	}
	
	
	public function view_page($id) {
		$view_data = array();
		$view_data['error'] = false;
		
		$country = $this->admin_country;
		
		$page = $this->_get_note($id, $country);
	
		if ($page) {
			$view_data["page"] = $page;
		} else {
			$view_data['error'] = "pag_found";
		}
		
		$subheader_data = array(
			'title' => 'Páginas',
			'subtitle' => 'Ver página',
			'btn_back_url' => site_url("padmin/pages"),
			'btn_back_text' => "Listado de Páginas"
		);
		
		$this->load->view('padmin/header', array('title' => 'Ver Página' ));
		$this->load->view('padmin/subheader', $subheader_data);
		$this->load->view('padmin/pages/view', $view_data);
		$this->load->view('padmin/footer');
		
	}
	
	
	public function create_page() {
		$this->load->library('country');
		
		$create_data = array();
		$create_data['admin_country'] = $this->admin_country;
		
		$create_data['country'] = $this->admin_country;
		$create_data['country_options'] = array_merge(array("" => "Elige país"), $this->country->get_countries());
		
		$create_data['type'] = '';
		$create_data['type_options'] = array_merge(array('' => "Elige tipo"), $this->contents_model->get_page_types());
		
		$create_data['status'] = '';
		$create_data['status_options'] = array_merge(array('' => "Elige estado"), $this->contents_model->get_page_statuses());
		
		$this->form_validation->set_message('required', 'Has olvidado el campo %s.');
		$this->form_validation->set_rules('title', 'Título', 'trim|required');
		$this->form_validation->set_rules('content', 'Contenido', 'trim|required');
		$this->form_validation->set_rules('type', 'Tipo', 'trim|required');
		$this->form_validation->set_rules('status', 'Estado', 'trim|required');
		
		if (!$this->admin_country) {
			$this->form_validation->set_rules('country', 'País', 'trim|required');
		}
		
		if ($this->form_validation->run() !== false) {
			$post = $this->input->post();
			
			$this->db->set('pagTitle', $post['title']);
			$this->db->set('pagContent', $post['content']);
			$this->db->set('pagDate', "'" . date("Y-m-d H:i:s") . "'", false);
			$this->db->set('pagModified', "'" . date("Y-m-d H:i:s") . "'", false);
			
			$this->db->set('admId', $this->admin_model->get_id());
			
			if ($this->admin_country) {
				$this->db->set('pagCountry', $this->admin_country);
			} else {
				$this->db->set('pagCountry', $post['country']);
			}
			
			$this->db->insert("pages");
			
			redirect("padmin/contents/list_pages");
		}
		
		
		$subheader_data = array(
			'title' => 'Contenidos',
			'subtitle' => 'Crear página',
			'btn_back_url' => site_url("padmin/contents/list_pages"),
			'btn_back_text' => "Listado de Páginas"
		);
		
		$this->load->view('padmin/header', array('title' => 'Crear Página' ));
		$this->load->view('padmin/subheader', $subheader_data);
		$this->load->view('padmin/contents/pages/create', $create_data);
		$this->load->view('padmin/footer');
	}
	
	
	public function edit_page($id) {
		$this->load->library('country');
		
		$edit_data = array();
		
		$edit_data['ajax_path'] = "../";
		
		$edit_data['page'] = $this->_get_page($id, $this->admin_country);
		
		if ($edit_data['page']) {
			$edit_data['admin_country'] = $this->admin_country;

			$edit_data['country'] = $this->admin_country;
			$edit_data['country_options'] = array_merge(array("" => "Elige país"), $this->country->get_countries());

			$create_data['type'] = '';
			$create_data['type_options'] = array_merge(array('' => "Elige tipo"), $this->contents_model->get_page_types());

			$create_data['status'] = '';
			$create_data['status_options'] = array_merge(array('' => "Elige estado"), $this->contents_model->get_page_statuses());

			$this->form_validation->set_message('required', 'Has olvidado el campo %s.');
			$this->form_validation->set_rules('title', 'Título', 'trim|required');
			$this->form_validation->set_rules('content', 'Contenido', 'trim|required');
			$this->form_validation->set_rules('type', 'Tipo', 'trim|required');
			$this->form_validation->set_rules('status', 'Estado', 'trim|required');

			if (!$this->admin_country) {
				$this->form_validation->set_rules('country', 'País', 'trim|required');
			}

			if ($this->form_validation->run() !== false) {
				$post = $this->input->post();

				$this->db->set('pagTitle', $post['title']);
				$this->db->set('pagContent', $post['content']);
				$this->db->set('pagCreated', "'" . date("Y-m-d H:i:s") . "'", false);

				$this->db->set('admName', $this->admin_model->get_name());

				if ($this->admin_country) {
					$this->db->set('pagCountry', $this->admin_country);
				} else {
					$this->db->set('pagCountry', $post['country']);
				}

				$this->db->where('pagId', $id);

				$this->db->update("pages");
				
				redirect("padmin/contents/view_page/".$id);
			}
		}
		
		$subheader_data = array(
			'title' => 'Contenidos',
			'subtitle' => 'Editar página',
			'btn_back_url' => site_url("padmin/contents/list_pages"),
			'btn_back_text' => "Listado de Páginas"
		);
		
		$this->load->view('padmin/header', array('title' => 'Editar Página'));
		$this->load->view('padmin/subheader', $subheader_data);
		$this->load->view('padmin/contents/pages/edit', $edit_data);
		$this->load->view('padmin/footer');
	}
	
	
	public function delete_page($id) {
		$page = $this->_get_note($id, $this->admin_country);
		
		if ($page !== false) {
			$this->db->set("pagDeleted", 1);
			$this->db->where("pagId", $id);
			$this->db->update("pages");
		}
		
		redirect("padmin/pages");
	}
		
	public function create_upload_dir() {
		$result = false;
		
		$rel_path = $this->input->get("dir");
		$rel_path = preg_replace("!\.\./!", "", $rel_path);
		
		if ($rel_path) {
			$abs_path = $this->upload_root . "/" . $rel_path;

			if (file_exists($abs_path)) {
				$result = is_dir($abs_path);
			} else {
				$result = mkdir($abs_path, 0777, true);
			}
		}
		
		echo json_encode($result);
	}
	
	
	public function delete_upload_dir() {
		$result = false;
		
		$rel_path = $this->input->get("dir");
		$rel_path = preg_replace("!\.\./!", "", $rel_path);
		
		$default_dirs = array("default", "trash");
		
		if ($rel_path && !in_array($rel_path, $default_dirs)) {
			$abs_path = $this->upload_root . "/" . $rel_path;
			
			if (is_dir($abs_path)) {
				$dir_empty = count($this->_internal_upload_dirs($rel_path)) == 0;
				
				foreach (scandir($abs_path) as $f) {
					if (is_file($abs_path . "/" . $f)) {
						$dir_empty = false;
						break;
					}
				}
				
				if ($dir_empty) {
					foreach ($this->thumb_dirs as $d) {
						if (file_exists($abs_path . '/' . $d)) {
							rmdir($abs_path . '/' . $d);
						}
					}
					
					$result = rmdir($abs_path);
				}
			} 
		}
		
		echo json_encode($result);
	}
	
	
	public function get_upload_dir($sub_dir="") {
		if (!$sub_dir || !is_dir($this->upload_root . "/" . $sub_dir)) {
			$sub_dir = "default";
		}
		
		$final_path = $this->upload_root . "/" . $sub_dir;
		$final_path = str_replace("//", "/", $final_path);
		
		return $final_path;
	}
	
	
	public function get_upload_dirs() {
		$available_dirs = $this->_internal_upload_dirs();

		$default_dirs = array();
		$extra_dirs = array();
		
		foreach ($available_dirs as $a_dir) {
			if ($a_dir === "default" || strpos($a_dir, "default/") === 0) {
				$default_dirs[] = $a_dir;
			} else {
				$extra_dirs[] = $a_dir;
			}
		}
		
		$ordered_available_dirs = array_merge($default_dirs, $extra_dirs);
		
		$json = str_replace('\\/', '/', json_encode($ordered_available_dirs));
		
		echo $json;
	}
	
	
	private function _internal_upload_dirs($base_path="") {
		$found_dirs = array();
		
		$not_allowed_dirs = $this->thumb_dirs;
		
		$full_path = $this->upload_root . "/" . $base_path;
		
		if (is_dir($full_path)) {
			foreach (scandir($full_path) as $f) {
				$file_path = $full_path . "/" . $f;
				
				if ($f[0] != "." && is_dir($file_path) && is_writable($file_path) && !in_array($f, $not_allowed_dirs)) {
					
					$found_dir = preg_replace("!^/+!", "", $base_path . "/" . $f);
					$found_dirs[] = $found_dir;
					
					$found_subdirs = $this->_internal_upload_dirs($base_path . "/" . $f);
					
					$found_dirs = array_merge($found_dirs, $found_subdirs);
				}	
			}
		}
		
		return $found_dirs;
	}
	
	
	public function file_upload() {
		require( APPPATH . 'libraries/upload_handler.php');

		$rel_dir = $this->input->get("dir");
		
		$upload_dir = $this->get_upload_dir($rel_dir);
		
		$this->load->helper('directory');
		$this->load->helper('directorycopy');
		directory_public($upload_dir, 0777);
		
		$options = array(
			'script_url' => current_url(),
			'upload_dir' => $upload_dir . "/",
			'upload_url' => base_url($upload_dir) . '/',
			'user_dirs' => false,
			'mkdir_mode' => 0777,
			'param_name' => 'files',
			// Set the following option to 'POST', if your server does not support
			// DELETE requests. This is a parameter sent to the client:
			'delete_type' => 'DELETE',
			'access_control_allow_origin' => '*',
			'access_control_allow_credentials' => false,
			'access_control_allow_methods' => array(
				'OPTIONS',
				'HEAD',
				'GET',
				'POST',
				'PUT',
				'PATCH',
				'DELETE'
			),
			'access_control_allow_headers' => array(
				'Content-Type',
				'Content-Range',
				'Content-Disposition'
			),
			// Enable to provide file downloads via GET requests to the PHP script:
			'download_via_php' => false,
			// Defines which files can be displayed inline when downloaded:
			'inline_file_types' => '/\.(gif|jpe?g|png)$/i',
			// Defines which files (based on their names) are accepted for upload:
			'accept_file_types' => '/.+\.(jpe?g|png|gif)$/i',
			// The php.ini settings upload_max_filesize and post_max_size
			// take precedence over the following max_file_size setting:
			'max_file_size' => null,
			'min_file_size' => 1,
			// The maximum number of files for the upload directory:
			'max_number_of_files' => null,
			// Image resolution restrictions:
			'max_width' => null,
			'max_height' => null,
			'min_width' => 1,
			'min_height' => 1,
			// Set the following option to false to enable resumable uploads:
			'discard_aborted_uploads' => true,
			// Set to true to rotate images based on EXIF meta data, if available:
			'orient_image' => true,
			'image_versions' => array(
				// Uncomment the following version to restrict the size of
				// uploaded images:
				'' => array(
				  'max_width' => 3000,
				  'max_height' => 1875,
				  'jpeg_quality' => 90,						
				),

				'optimized' => array(
				  'max_width' => 1920,
				  'max_height' => 1200,
				  'jpeg_quality' => 85,
				 ),

				'w200' => array(
					'max_height' => 400,
					'max_width' => 200,
				),
				'thumbnail' => array(
					'max_width' => 80,
					'max_height' => 80
				),
			)
		);

		$upload_handler = new UploadHandler($options);
	}
	
}

/* end of file */