<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class WebService extends CI_Controller {

	
	public function index() {
		$out['message'] = 'running';
		$this->_to_output($out);
	}
	
	public function get_ads_ids($source = false) {
		
		$out = array();
		
		$out['message'] = 'no source';
		
		if ($source) {
			$this->db->where('adSource', $source);
			$results = $this->db->get('ads');
		
			if ($results->num_rows()) {
				$ids = array();
			
				foreach( $results->result_array() as $result_row ) {
					$ids[] = $result_row['adSourceAdId'];
				}
				
				$out['data'] = $ids;
				$out['message'] = 'success';
				
			} else {
				$out['message'] = 'no results';
			}
		}
		
		$this->_to_output($out);
		
	
	}
	
	public function get_ad_material($user=false) {
		$this->load->library('adsbuilder');
		
		$data = false;
		$out['message'] = 'no user';
		
		if ($user) {
			$this->load->model('profile_model');
			$data = $this->profile_model->get_profile($user);
		}
		
		if ($data) {
			$this->load->helper('text');
			
			$title = $this->adsbuilder->mix_title( array( $data['title'], $data['subtitle'] ) );
			$title = $this->adsbuilder->delete_phone($title);
			
			$data['title'] = $title;
			
			$description = $data['about'] . ' ' . $data['services'];
			
			$data['text'] = character_limiter( $description, 950, '…');
			
			$data['falsemail'] = $this->adsbuilder->random_mail();
			$data['site'] = 'www.pumbate.com/' . $user;
			
			$data['pictures'] = $this->adsbuilder->watermark_gallery($user);
			
			$promo = 'img/promos/' . $data['country'] . '_optimized.jpg';
			
			if (file_exists($promo)) {
				$data['promo'] = site_url($promo);
			} else {
				$data['promo'] = false;
			}
			
			$out['data'] = $data;
			$out['message'] = 'success';
		} else {
			$out['message'] = 'no user data found';
		}
		
		$this->_to_output($out);
	}
	
	public function output_optimized_image() {
		$path = $this->input->get('path');
		
		if ($path) {
			$path = preg_replace('!.*/(userdata/)!i', '$1', $path);
			$this->load->library('adsbuilder');
			
			$limit_width = $this->input->get('limit_width');
			$limit_height = $this->input->get('limit_height');
			$quality = $this->input->get('quality');
			
			$this->adsbuilder->output_optimized_image($path, $limit_width, $limit_height, $quality);
		} else {
			echo 'please add GET parameter "path", optionals: "limit_width", "limit_height", "quality"';
		}
	}
	
	public function insert_ad() {
		
		$this->form_validation->set_rules('id', 'Id', 'trim|required');
		$this->form_validation->set_rules('url', 'Url', 'trim|required');
		$this->form_validation->set_rules('datetime', 'Datetime', 'trim|required');
		$this->form_validation->set_rules('source', 'Source', 'trim|required');
		$this->form_validation->set_rules('user', 'User', 'trim|required');
		
		if ($this->form_validation->run() !== false) {
			$post = $this->input->post();
			
			$this->db->set('adSourceAdId', $post['id']);
			$this->db->set('adUrl', $post['url']);
			$this->db->set('adDatetime', $post['datetime']);
			$this->db->set('adSource', "'" . $post['source'] . "'", false);
			$this->db->set('proUser', $post['user']);
			
			$this->db->insert('ads');
			
			$out['message'] = 'success';
		} else {
			$out['message'] = 'missing fields';
		}
		
		$this->_to_output($out);
	}
	
	
	public function insert_horoscope() {
		
		$this->form_validation->set_rules('date', 'Date', 'trim|required');
		$this->form_validation->set_rules('text', 'Text', 'trim|required');
		$this->form_validation->set_rules('sign', 'Sign', 'trim|required');
		
		if ($this->form_validation->run() !== false) {
			$post = $this->input->post();
			
			$this->db->set('horSign', $post['sign']);
			$this->db->set('horText', $post['text']);
			$this->db->set('horDate', "'" . $post['date'] . "'", false);
			$this->db->set('horHash', md5($post['text']));
			
			$this->db->insert('horoscopes');
			
			$out['message'] = 'success';
		} else {
			$out['message'] = 'missing fields';
		}
		
		$this->_to_output($out);
		
	}
	
	public function get_horoscope($sign, $date) {
		
		$this->db->where('horSign', $sign);
		$this->db->where('horDate', $date);
		$res = $this->db->get('horoscopes');
		
		if ($res->num_rows()) {
			$dbrow = $res->row_array();
			
			$out['data'] = $dbrow['horText'];
			$out['message'] = 'success';
			
		} else {
			$out['message'] = 'no results';
		}
		
		$this->_to_output($out);
	}
	
	public function get_phones($country = false, $gender = false) {
		$this->db->select('proPhone');
		
		if ($country) {
			$this->db->where('proCountry', $country);
		}
		
		if ($gender) {
			$this->db->where('proGender', $gender);
		}		
		
		$results = $this->db->get('profiles');
		
		if ($results->num_rows()) {
			
			$phones = array();
			
			foreach($results->result_array() as $result_row) {				
				if (preg_match_all('![0-9\s]+!', $result_row['proPhone'], $mphones)) {
					foreach ($mphones[0] as $mphone) {
						$mphone = str_replace(' ', '', $mphone);
						
						if (!in_array($mphone, $phones)) {
							$phones[] = $mphone;
						}
					}
				}
			}
			
			$out['data'] = $phones;
			$out['message'] = 'success';

		} else {
			$out['message'] = 'no results';
		}
		
		$this->_to_output($out);		
	}
	
	
	public function get_all($country = false, $gender = false, $include_unverified = false, $include_absent = false, $account_level = false) {		

		$this->load->model('profile_model');
		
		$fields = $this->input->get('fields');
		
		if ($fields !== false) {
			$fields = explode(",", $fields);
		}
		
		if ($this->input->get('full') !== false) {
			$profiles = $this->profile_model->full_get_all($country, $fields);
		} else {
			$profiles = $this->profile_model->get_all($country, $gender, $include_unverified, $include_absent, $account_level);			
		}
		
		$cp = count($profiles);
		
		if ($cp) {
			$include_pictures = empty($fields) || (!empty($fields) && in_array('pictures', $fields));
			
			if ($include_pictures) {
				for ($p=0; $p < $cp; $p++) {
					$profiles[$p]['pictures'] = $this->_get_profile_pictures($profiles[$p]['user']);
				}
			}
			
			$out['data'] = $profiles;
			$out['message'] = 'success';

		} else {
			$out['message'] = 'no results';
		}
		
		$this->_to_output($out);
	}
	
	
	public function get_profile($user=false) {
		$out = array(
			'data' => array(),
			'message' => 'no user',
		);
		
		if ($user) {
			$this->load->model("profile_model");
			$profile = $this->profile_model->get_full_profile($user, true);
			
			if ($profile) {
				$pictures_version = $this->input->get("pictures_version");
				$profile['pictures'] = $this->_get_profile_pictures($user,$pictures_version);
				
				$out['data'] = $profile;
				$out['message'] = 'success';
			} else {
				$out['message'] = 'no results';
			}
		}
		
		$this->_to_output($out);
	}
	
	
	public function get_user_remote_files_md5($user = false, $subdir = false) {
		
		$out = array(
			'data' => array(),
			'message' => 'no user',
		);
		
		if ($user) {
			$user_dir = 'userdata/'.$user.'/';
			
			if ($subdir) {
				$user_dir .= $subdir . "/";
			}
			
			if (is_dir($user_dir)) {
				foreach (scandir($user_dir) as $f) {
					if (!is_dir($user_dir . "/" . $f)) {
						$out['data'][] = array(
							'filename' => $f,
							'md5' => md5_file($user_dir .  $f)
						);
					}
				}
			}
			
			if (count($out['data']) > 0 ) {
				$out['message'] = 'success';
			} else {
				$out['message'] = 'no files';
			}
			
		}
		
		$this->output
		->set_content_type('application/json')
		->set_output(json_encode($out));
	}
	
	
	private function _get_profile_pictures($user, $pictures_version=false) {
		$pics = array();
		
		$user_dir = 'userdata/'.$user.'/';
		
		if (is_dir($user_dir)) {
			foreach (scandir($user_dir) as $f) {
				if (preg_match('!\.(jpe?g|png|gif)!i', $f)) {
					
					if ($pictures_version) {
						$src = base_url('/userdata/'.$user.'/'. $pictures_version . '/'.$f);
					} else {
						$src = base_url('/userdata/'.$user.'/'.$f);
					}
					
					$pics[] = $src;
				}
			}
		}
		
		return $pics;
	}
	
	
	private function _to_output ($data) {
		$this->output
		->set_content_type('application/json')
		->set_output(json_encode($data));
	}
	
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */