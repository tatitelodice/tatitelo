<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class PromoMain extends CI_Controller {

	public function __construct() {
		parent::__construct();
		$this->load->library('session');
		$this->load->library('country');
		$this->load->library('gender');
		$this->load->library('contact');
		
		$this->load->model("flashsession_model");
		$this->load->model("security_model");
		$this->load->model("admin_model");
		$this->load->model("blacklist_model");
		
		if ($this->input->post('action') === 'city') {
			$this->_update_cities();
			die;
		}
	}
	
	
	public function index() {
		$this->_show();
	}
	
	public function country($iso = false) {
		
		if (strlen($iso) == 2) {
			$this->session->set_userdata('country', $iso);
			$this->_show($iso);
		} else {
			redirect('');
		}
	}

	private function _look_for_country_iso() {
		$country_iso = $this->session->userdata('country');

		if ($country_iso === false) {
			$country_iso = 'uy';
		}
		
		return $country_iso;
	}	
	
	
	public function gallery($country_iso = false, $gender = false) {
		
		if ($country_iso && $gender && ($this->gender->is_gender($gender) || $gender == 'duo') ) {
			
			if (strlen($country_iso) > 2) {
				$country_iso = $this->country->get_country_iso($country_iso);
			}
			
			$this->session->set_userdata('country', $country_iso);
			$this->session->set_userdata('gender', $gender);
			
			$this->_show($country_iso, $gender);
		} else {
			redirect('');
		}
		
	}
		
	public function clean_help() {
		$this->flashsession_model->unset_var("wait_for_login_user");
		$this->flashsession_model->unset_var("wait_for_login_pass");
		$this->flashsession_model->unset_var("profile_complete");
		$this->flashsession_model->unset_var("profile_help");
		$this->flashsession_model->unset_var("profile_is_garbage");

		$this->flashsession_model->save();
		
		if ($this->admin_model->login_exists()) {
			$this->security_model->unlock_request("profile_create");
		}
					
		redirect('promomain#advertise');
	}
	
	private function _show($country_iso = false, $gender = false) {
		
		if ($country_iso === false) {
			$country_iso = $this->_look_for_country_iso();
		}
		
		$country_name = $this->country->name($country_iso);
		
		if ($gender === false) {
			$gender = $this->session->userdata('gender');

			if ($gender === false) {
				$gender = 'mujer';
			}
		}
		
		$city = $this->input->get('city');
		$region = $this->input->get('region');
		$keywords = trim($this->input->get('keywords'));

		$page_params = [
			'gender' => $gender
		,	'city' => $city
		,	'region' => $region
		,	'country' => $country_iso
		,	'keywords' => $keywords
		,	'limit' => 400
		];
		
		$escorts_by_type = $this->_get_escorts_data($page_params);

		if (function_exists('patch_elist'))
		{
			$escorts_by_type['basic'] = patch_elist($escorts_by_type['basic'], $country_iso, $gender);
		}
			
		$total_escorts_count = 0;
		foreach ($escorts_by_type as $k => $v) $total_escorts_count += count($v);
			
		$this->_append_demo_escorts($escorts_by_type['basic'], $gender);
		
		$profile_create_security_lock = $this->security_model->on_request_lock("profile_create");
		//var_dump($profile_create_security_lock); die;
		
		$profile_help = $this->flashsession_model->get_var("profile_help") || $profile_create_security_lock;
		$profile_complete = $this->flashsession_model->get_var("profile_complete") && $profile_create_security_lock === false;
		$profile_user = $this->flashsession_model->get_var("wait_for_login_user");
		$profile_is_garbage = $this->flashsession_model->get_var("profile_is_garbage");

		$banners = $this->_get_banners($page_params, $escorts_by_type);
		$escorts_by_type = $this->_adjust_escorts($escorts_by_type, $banners);

		$main_data = array(
			'admin_login' => $this->admin_model->login_exists()
		,	'banners'     => $banners
		,	'profile_user' => $profile_user
		,	'profile_help' => $profile_help
		,	'profile_complete' => $profile_complete
		,	'profile_is_garbage' => $profile_is_garbage
		,	'maintenance' => $this->config->item('maintenance_mode')
		,	'suscribe_enabled' => $this->config->item('suscribe_enabled')
		,	'blog_entries' => $this->_blog_entries($country_iso)
		,	'escorts_top' => $escorts_by_type['top']
		,	'escorts_premium' => $escorts_by_type['premium']
		,	'escorts_basic' => $escorts_by_type['basic']
		,	'escorts_gender' => $gender
		,	'promo' => true
		,	'country' => $country_name
		,	'country_iso' => $country_iso
		,	'city' => $city
		,	'region' => $region
		,	'keywords' => $keywords
		,	'total_results' => $total_escorts_count
		,	'pumbate_phone' => $this->contact->get_phone($country_iso)
		,	'contact_phone_sms' => $this->contact->get_phone($country_iso, 'sms')			
		,	'pumbate_mail' => $this->contact->get_mail($country_iso)
		);
		
		$regions = $this->country->get_active_regions($country_iso, $gender);

		$main_data['regions'] = array();
		$main_data['regions'][''] = 'Elige ' . (( $country_iso == 'ar')? 'provincia' : 'departamento');
		
		if (is_array($regions) || is_object($regions))
		{
			foreach ($regions as $reg) {
				$main_data['regions'][$reg] = $reg;
			}
		}
		
		
		$main_data['region'] = $this->input->get('region');

		$main_data['cities'] = array();

		if ($main_data['region']) {
			$cities = $this->country->get_active_cities($country_iso, $main_data['region'], $gender );
			
			if (count($cities)) {
				$main_data['cities'][''] = 'Cualquier Zona/Ciudad';
				
				foreach ($cities as $cit) {
					$main_data['cities'][$cit] = $cit;
				}
			}
		}
		
		$main_data['city'] = $this->input->get('city');
				
		//$main_data['promo'] = false; $main_data['form_success'] = true; $main_data['user'] = 'usuario';
		
		if ($keywords) {
			$seo_start = ucfirst($keywords) . ' en Pumbate';
		} else {
			$seo_start = 'Pumbate ';
			
			if ($country_iso == 'co') {
				$seo_start= 'Prepagos';
			} else {
				$seo_start .= 'pura Pasión  de putas y Yirantas';
			}		
		}
		
		$seo_start .= ' ' . ucfirst($country_name);
		
		$seo_gender = 'chicas, travestis y chicos';
		
		if ($gender != 'mujer') {
			$seo_gender = str_replace('-', ' ', $this->gender->get_gender_seo($gender));
		}
		
		$seo_place = '';
		
		if ($city){
			$seo_place = $city . ', ' . $region;
		} elseif ($region) {	
			$seo_place = $region;
		} elseif ($country_iso == 'uy') {
			$seo_place = 'Montevideo, Punta del Este';
		}
		
		$header_title =  $seo_start . ' - ' . ucfirst($seo_gender) . (($seo_place)? ' en '. $seo_place : '');
		
		$country_uri_name = strtolower($country_name);
		$archive_url_map = [
			'mujer'  => '/archivo/sexo/' . $country_uri_name . '/mujeres',
			'trans'  => '/archivo/sexo/' . $country_uri_name . '/travestis-y-trans',
			'hombre' => '/archivo/sexo/' . $country_uri_name . '/hombres',
			'duo'    => '/archivo/sexo/' . $country_uri_name . '/duos-y-trios'
		];

		$archive_url = site_url($archive_url_map[$gender]);
		$main_data["archive_url"] = $archive_url;

		$this->load->view('site/header', array('title' => $header_title));
		$this->load->view('site/main/main', $main_data);
		$this->load->view('site/footer');
	}

	
	public function valid_username($username) {
		$username = strtolower($username);
		
		$valid = true;
		
		$banned_list = array(
			'index', 'application', 'system', 'userdata', 'admin', 'padmin', 'panel', 'cpanel', 'webmail',
			'main', 'promomain', 'escort', 'profile', 'site', 'login', 'welcome', 'comingsoon', 
			'chile', 'uruguay', 'colombia', 'argentina', 'brasil', 'mipagina', 'mujeres', 'hombres',
			'trans', 'chicos', 'chicas', 'pumbate', 'sexo', 'duos', 'trios',
		);
		
		$valid = $valid && !in_array($username, $banned_list);
		$valid = $valid && !file_exists( APPPATH . 'controllers/' . strtolower($username). '.php');
		$valid = $valid && !file_exists( APPPATH . 'controllers/' . strtolower($username));
		
		return $valid;
	}
	
	
	public function valid_username_chars($username) {
		$username = strtolower($username);
		return strlen($username) > 2 && preg_match('![a-z]!i', $username);
	}
	
	public function valid_phone($phone) {
		$phone = preg_replace('![^0-9]!', '', $phone);
		
		if ( strlen($phone) > 7 ) {
			return true;
		} else {
			return false;
		}
	}

	private function _adjust_escorts($escorts_by_type, $banners)
	{
		$min_banners = $this->config->item('banner_min_count');
 		$max_banners = $this->config->item('banner_max_count');

 		if (count($banners) < $min_banners)
 		{
 			$escorts_by_type['top'] = array_merge(
 				$escorts_by_type['superstar']
 			,	$escorts_by_type['top']
 			);
 		}

		return $escorts_by_type;
	}
		
 	private function _get_banners($page_params, $escorts_by_type)
 	{
 		$min_banners = $this->config->item('banner_min_count');
 		$max_banners = $this->config->item('banner_max_count');

 		$escorts = $escorts_by_type['superstar'];

 		if (count($escorts) < $min_banners)
 		{
 			return [];
 		}

 		$this->load->library('webfront/banner_service');

 		// CHECK FOR THIS FEATURE
 		$banners_enabled = $this->banner_service->has_page_escort_banners(
 			$page_params
 		);

 		if (!$banners_enabled)
 		{
 			return [];
 		}
 		//$banners = $this->banner_service->get_banners(
 		//	$page_params['country'], 'main'
 		//);
 		$escort_banners = $this->banner_service->escorts_to_banners($escorts);
 		//$banners = array_merge($escort_banners, $banners);
 		
 		return $escort_banners;
 	}
	
	private function _get_escorts_data($page_params=[]) {
		$this->load->library('webfront/profile_gallery_service');

		$escorts_data = $this->profile_gallery_service
			->get_gallery_data_by_account_type([
				'country' => $page_params['country']
			,	'gender'  => $page_params['gender']
			,	'region'  => $page_params['region']
			,	'city'    => $page_params['city']
			,	'keywords'=> $page_params['keywords']
			,	'limit'   => $page_params['limit']
			]
		);
		
		return $escorts_data;
	}	

	private function _append_demo_escorts(&$escorts, $gender)
	{
		$limit = count($escorts);
		
		//print_r(array( count($woman_escorts), count($trans_escorts) )); die;
		
		if ($limit == 0) {
			$limit = 1;
		}
		
		while($limit % 8 != 0) {
			$limit++;
		}
		
		//echo $limit; die;
		
		$demo_escorts = $this->_demo_escorts_data($gender);
		
		while (count($escorts) < $limit) {
			$escorts[] = $demo_escorts[rand(0,2)];
		}
	}

	private function _demo_escorts_data($gender = false, $name = false) {
		$escorts_data = array();
		
		$gender_letter = '';
		
		if ($gender == 'hombre') {
			$gender_letter = 'h';
		}
		
		if (!$name) {
			$name = 'Anunciate gratis';
		}
		
		for ($i=1; $i < 4; $i++) {
			$escorts_data[] = array(
				'user' => '',
				'domain' => '',
				'title' => 'Se visible para tus clientes.',
				'name' => $name,
				'location' => 'en pumbate.com.',
				'about' => 'Tu lugar puede estar aquí.<br>En pumbate.com<br> TOTALMENTE GRATIS',
				'site' => '#advertise',
				'absent' => 0,
				'picture' => base_url('img/muestra/'.$gender_letter.$i.'.jpg'),	
				'picture_type' => 'vertical',
				'verified_pictures' => false,
				'account_type' => PROFILE_FREE,
			);
		}
		
		return $escorts_data;
	}
	
	private function _blog_entries($country_iso = 'uy') {
		$entries = array();
	
		$this->db->where('posCountry', $country_iso);
		$res = $this->db->get('posts');
		
		if ( $res->num_rows() ) {
			foreach ($res->result_array() as $post_row) {
				$entries[] = array(
					'url' => $post_row['posUrl'],
					'title' => mb_strtoupper($post_row['posTitle']),
				);
			}
		}
		
		return $entries;
	}
	
	private function _update_cities() {
		$region = $this->input->post('region');
		$country = $this->input->post('country');
		$gender = $this->input->post('gender');
		
		if ($region && $country) {
			
			$cities = $this->country->get_active_cities($country, $region, $gender);
			
			if (count($cities)) {
				echo '<option value="">Cualquier Zona/Ciudad</option>';
				foreach ($cities as $cit) {
					echo '<option value="'.$cit.'">'.$cit.'</option>';
				}
			}			
		}
	}
	
}

/* End of file main.php */
