<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Suscribe extends CI_Controller {

	
	public function __construct() {
		parent::__construct();
		$this->load->library('session');
		$this->load->library('country');
	}

	public function index() {
		
		$country_iso = $this->session->userdata('country');
		
		if ($this->input->post('country')) {
			$country_iso = $this->input->post('country');
		}
		
		if ($this->country->is_valid($country_iso)) {
			
			$country = $this->country->name($country_iso);
			
			$this->load->view('site/header', array('title' => 'Suscribite con tu e-mail pirata y recibí noticias de chicas, chicos y trans en '.$country));
			
			$suscribe_view = array();
			
			$suscribe_view['maintenance'] = $this->config->item('maintenance_mode') || !$this->config->item('suscribe_enabled');
			$suscribe_view['country'] = $country;
			$suscribe_view['country_iso'] = $country_iso;
			$suscribe_view['email'] = '';
			$suscribe_view['success'] = false;
			$suscribe_view['limit_error'] = false;
			
			$this->form_validation->set_message('required', 'Escribe una dirección de e-mail para continuar.');
			$this->form_validation->set_message('valid_email', 'La dirección de e-mail no está bien escrita.');
			$this->form_validation->set_rules('email', 'E-mail', 'trim|required|valid_email');
			
			
			
			if ( $this->form_validation->run() !== false ) {
				
				$limit_error = $this->_limit_per_ip();
				
				if ($limit_error) {
					$suscribe_view['limit_error'] = true;
				} else {
					$email = $this->input->post('email');
					$suscribe_view['email'] = $email;

					$this->db->where('susEmail', $email);
					$result = $this->db->get('suscribers');

					if ($result->num_rows() == 0) {

						$this->db->set('susCountry', $country_iso);
						$this->db->set('susHash', sha1(time() . $email ));
						$this->db->set('susEmail', $email);
						$this->db->set('susDeleted', 0);

						if ($this->db->insert('suscribers')) {							
							$this->load->model('log_model');
							$this->log_model->insert_log($country_iso, 'suscript');
						}
					}

					$suscribe_view['success'] = true;
				}
			}	
			
			$this->load->view('site/suscribe', $suscribe_view);
			
			$this->load->view('site/footer');
		} else {
			redirect('');
		}
		
	}
	
	
	private function _limit_per_ip() {
		$this->db->where('logDescription', 'suscript');
		$this->db->where('logAddress', $this->input->ip_address());
		$this->db->where('logDate >=', "'" . date('Y-m-d H:i:s', strtotime('-1 day')) . "'", false);

		$results = $this->db->get('logs');
		
		return $results->num_rows() > 4;
	}
	
	private function _limit_per_day() {		
		$this->db->where('logDate >=', "'" . date('Y-m-d H:i:s', strtotime('-1 day')) . "'", false);
		$results = $this->db->get('logs');
		return $results->num_rows() > 2000;
	}
	
	
}




/* End of file main.php */