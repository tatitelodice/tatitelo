<?php 
if ( ! defined('BASEPATH')) exit('No direct script access allowed');
if (isset($_SERVER['REMOTE_ADDR'])) die('Permission denied.');

class Profile_Locations_Cron extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->library('country');
    }

    public function index()
    {
        $this->_to_output([
            'status'  => 'error'
        ,   'message' => 'no_method_specified'
        ]);
    }

    public function refresh_locations_list()
    {
        $this->country->update_locations_cache();

        $this->_to_output([
            'status'  => 'success'
        ,   'message' => 'updated locations list successfuly'
        ]);
    }

    public function refresh_locations_active()
    {
        $this->country->update_active_locations_cache();

        $this->_to_output([
            'status'  => 'success'
        ,   'message' => 'updated active locations successfuly'
        ]);
    }

    private function _to_output ($data) {
		$this->output
		->set_content_type('application/json')
		->set_output(json_encode($data));
	}

}


/* eof */