<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');


class Pages extends CI_Controller {
	
	public function __construct() {
		parent::__construct();
		$this->load->library('session');
		$this->load->library('country');
	}

	public function index() {
		redirect();
	}
	
	public function show($page) {
		if (@file_exists( APPPATH . 'views/pages/'.$page.'.php')){
			
			$country_iso = $this->session->userdata('country');
			
			$data['country_iso'] = $country_iso;
			$data['country'] = $this->country->name($country_iso);
			
			$this->load->view('pages/' . $page, $data );
			$this->load->view('footer');
			
		} else {
			redirect('');
		}
	}
	
	
}




/* End of file main.php */