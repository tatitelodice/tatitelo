<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	http://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There area two reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router what URI segments to use if those provided
| in the URL cannot be matched to a valid route.
|
*/

$route['default_controller'] = "welcome";
$route['404_override'] = 'pumbate404';


$route['sitemap.xml'] = "sitemap";


$controllers_dir = scandir(APPPATH."/controllers/");

//print_r($controllers_dir); die;

for ($i=2; $i < count($controllers_dir); $i++) {
	$controller_name = str_replace('.php', '', $controllers_dir[$i]);
	$route[$controller_name] = $controller_name;
}

//print_r($route); die;

$route['pages/(:any)'] = 'site/pages/show/$1';

$route['padmin'] = 'padmin/main';

$route['empomar/(:any)'] = 'empomar/victima/$1';
$route['empomar/(:any)/(:any)'] = 'empomar/victima/$1/$2';
$route['empomar/(:any)/(:any)/(:any)'] = 'empomar/victima/$1/$2/$3';

/* SEO ROUTES */

/* general profiles */
// EJEMPLO DE URI QUE VIENE A PERFIL: sexo/chicas/uruguay/andrea
$route['sexo/:any/:any/(:any)'] = 'profilesite/view/$1';

$route['sexo/(:any)/mujeres'] = 'promomain/gallery/$1/mujer';
$route['sexo/(:any)/trans'] = 'promomain/gallery/$1/trans';
$route['sexo/(:any)/travestis-y-trans'] = 'promomain/gallery/$1/trans';
$route['sexo/(:any)/hombres'] = 'promomain/gallery/$1/hombre';
$route['sexo/(:any)/duos'] = 'promomain/gallery/$1/duo';
$route['sexo/(:any)/duos-y-trios'] = 'promomain/gallery/$1/duo';

// advertiser archive
$route['archivo/sexo/(:any)/mujeres'] = 'advertiser_archive_controller/advertiser_index/$1/mujer';
$route['archivo/sexo/(:any)/trans'] = 'advertiser_archive_controller/advertiser_index/$1/trans';
$route['archivo/sexo/(:any)/travestis-y-trans'] = 'advertiser_archive_controller/advertiser_index/$1/trans';
$route['archivo/sexo/(:any)/hombres'] = 'advertiser_archive_controller/advertiser_index/$1/hombre';
$route['archivo/sexo/(:any)/duos'] = 'advertiser_archive_controller/advertiser_index/$1/duo';
$route['archivo/sexo/(:any)/duos-y-trios'] = 'advertiser_archive_controller/advertiser_index/$1/duo';

/* specific lines */
$route['uruguay'] = 'promomain/country/uy';

$route['chicas-escorts-felinas-en-uruguay'] = 'promomain/gallery/uy/mujer';
$route['mujeres-escorts-y-sexo-en-uruguay'] = 'promomain/gallery/uy/mujer';
$route['chicas-escorts-y-sexo-en-uruguay'] = 'promomain/gallery/uy/mujer';
$route['travestis-escorts-y-sexo-en-uruguay'] = 'promomain/gallery/uy/trans';
$route['hombres-escorts-y-sexo-en-uruguay'] = 'promomain/gallery/uy/hombre';

$route['trios-gay-con-chicos-en-uruguay'] = 'promomain/gallery/uy/duo';
$route['trios-lesbicos-con-chicas-en-uruguay'] = 'promomain/gallery/uy/duo';


$route['argentina'] = 'promomain/country/ar';

$route['chicas-escorts-felinas-en-argentina'] = 'promomain/gallery/ar/mujer';
$route['mujeres-escorts-y-sexo-en-argentina'] = 'promomain/gallery/ar/mujer';
$route['chicas-escorts-y-sexo-en-argentina'] = 'promomain/gallery/ar/mujer';
$route['travestis-escorts-y-sexo-en-argentina'] = 'promomain/gallery/ar/trans';
$route['hombres-escorts-y-sexo-en-argentina'] = 'promomain/gallery/ar/hombre';

$route['trios-gay-con-chicos-en-argentina'] = 'promomain/gallery/ar/duo';
$route['trios-lesbicos-con-chicas-en-argentina'] = 'promomain/gallery/ar/duo';



$route['colombia'] = 'promomain/country/co';

$route['mujeres-escorts-y-sexo-en-colombia'] = 'promomain/gallery/co/mujer';
$route['chicas-escorts-y-sexo-en-colombia'] = 'promomain/gallery/co/mujer';
$route['travestis-escorts-y-sexo-en-colombia'] = 'promomain/gallery/co/trans';
$route['hombres-escorts-y-sexo-en-colombia'] = 'promomain/gallery/co/hombre';

/* END OF SEO ROUTES */


$route['mipagina'] = 'profile/panel';

/*$cached_routes = APPPATH . "cache/routes.php";

if (file_exists($cached_routes)) {
	include_once $cached_routes;
}*/
	
$route['update_gallery/(:any)'] = 'profile/update_gallery/$1';


$route['(?i)([a-z0-9\-_]+)'] = "profilesite/view/$1";
$route['(?i)([a-z0-9\-_]+)/panel'] = "profile/panel/$1";

// controllers from site_billing
$route['profile-panel/(:any)/(:any)/(:any)'] = "profilepanel/$1_controller/$2/$3";
$route['profile-panel/(:any)/(:any)'] = "profilepanel/$1_controller/$2";
$route['profile-panel/(:any)'] = "profilepanel/$1_controller";

/* End of file routes.php */
/* Location: ./application/config/routes.php */