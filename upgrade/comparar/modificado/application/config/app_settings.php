<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/******************************************************************************/
/*  PATHS                                                                     */
/******************************************************************************/
$config['media_dir_path'] = "../../media/";

/******************************************************************************/
/*  MODO MANTENIMIENTO                                                        */
/******************************************************************************/

$config['maintenance_mode'] = false;
$config['suscribe_enabled'] = false;
$config['profile_login_limit'] = 100;


/******************************************************************************/
/*  OPCIONES DE IMAGENES                                                      */
/******************************************************************************/

$config['profile_pictures_versions'] = array(
	'' => array(
	  'max_width' => 1920,
	  'max_height' => 1200,
	  'jpeg_quality' => 80,
	  'force_jpeg' => true,
	),

	'optimized' => array(
	  'max_width' => 1920,
	  'max_height' => 1200,
	  'jpeg_quality' => 75,
	 ),

	'w200' => array(
		'min_height' => 250,
		'min_width' => 200,
		'max_height' => 250,
		'max_width' => 200,
		'jpeg_quality' => 75,
		'center_crop' => true,
	),

	'thumbnail' => array(
		'max_width' => 80,
		'max_height' => 80,
		'jpeg_quality' => 75,
	)

,	'banner-md' => array(
		'min_height' => 300
	,	'min_width' => 360
	,	'max_height' => 300
	,	'max_width' => 360
	,	'jpeg_quality' => 80
	,	'center_crop' => true
	,	'required_account_type' => [PROFILE_SUPERSTAR, PROFILE_TOP]
	)

,	'banner-xs' => array(
		'min_height' => 180
	,	'min_width' => 360
	,	'max_height' => 180
	,	'max_width' => 360
	,	'jpeg_quality' => 75
	,	'center_crop' => true
	,	'required_account_type' => [PROFILE_SUPERSTAR, PROFILE_TOP]
	)

);



/******************************************************************************/
/*  Datos para el envío automatizado de mail de notificacion de CONTACTO     */
/******************************************************************************/

$config['notification_mail_encryption'] = 'ssl';
$config['notification_mail_server'] = 'smtp.fastmail.com'; //'mail.pumbate.com';
$config['notification_mail_port'] = 465; //587;


$config['notification_mail_name'] = 'Pumbate Escorts';
$config['notification_mail_user'] = 'postmaster@pumbate.com'; // 'rrpp@pumbate.com';
$config['notification_mail_password'] = 'wn2ttyr6vyrte5z9'; //'contact0.';

/******************************************************************************/
/* Email users                                                                */
/******************************************************************************/

$config["email_notification_users"] = true;
$config["email_notification_admins"] = true;


/******************************************************************************/
/* SITE VARS                                                                 */
/******************************************************************************/

$config['site_name'] = "Pumbate";

$config['site_domain'] = "www.pumbate.com";
$config['site_domain_simple'] = "pumbate.com";

$config['site_author'] = "Pumbate Escorts Latinoamérica";

$config['site_admin_title_tag'] = "Pumbate Admin";

$config['remote_site_domain'] = 'chicasuy.com';

$config['website_base_url'] = getenv('WEBSITE_BASE_URL');


/******************************************************************************/
/* SITE PHONES                                                                */
/******************************************************************************/

$config['contact_phones'] = array(
	'uy' => array(
		'default' => '094 332 135',
		'sms'     => '094 332 135',
		'whatsapp'=> '094 332 135',
	)
);

/******************************************************************************/
/* SITE EMAILS                                                                */
/******************************************************************************/

$config['contact_emails'] = array(
	'default' => 'rrpp@pumbate.com',
	'uy'      => 'rrpp@pumbate.com',
	'co'      => 'colombia@pumbate.com',
);

	
/******************************************************************************/
/* AGENT EMAILS                                                               */
/******************************************************************************/

$config['agent_emails'] = array(
	
	'default' => array(
		'raveenz.debian@gmail.com' => 'Santiago',
	),
	'uy' => array(
		'raveenz.debian@gmail.com' => 'Santiago',
//		'nicoquint@gmail.com' => 'Nicolás',
	),
	'co' => array(
		'colombia@pumbate.com' => "Pumbate Colombia",
	), 	
);


/******************************************************************************/
/* CATALOG                                                                    */
/******************************************************************************/
$config['catalog_display_profiles_without_pictures'] = false;
$config['catalog_cache_enabled'] = true;

$config['banner_min_count'] = 3;
$config['banner_max_count'] = 5;

$config['banner_sections'] = [
	'uy' => [ 
		['gender' => 'mujer', 'keywords' => '']
	,	['gender' => 'trans', 'keywords' => '']
	//,	['gender' => 'hombre', 'keywords' => '']
	]
];


/******************************************************************************/
/* BILLING                                                                    */
/******************************************************************************/

$config['all_paid_profiles'] = true;


$config['pricing'] = [
	'uy' => [
		'mujer'  => [
			[ 'price' =>  390, 'accountType' => 1, 'days' => 11, 'text' => '10 DÍAS Básica' ]
		,	[ 'price' =>  590, 'accountType' => 1, 'days' => 31, 'text' => '30 DÍAS Básica' ]
		,	[ 'price' => 1250, 'accountType' => 2, 'days' => 31, 'text' => '30 DÍAS Destacada' ]
		,	[ 'price' => 1800, 'accountType' => 3, 'days' => 31, 'text' => '30 DÍAS TOP']
		,	[ 'price' => 2500, 'accountType' => 4, 'days' => 31, 'text' => '30 DÍAS SUPER TOP (banner arriba)']
		]
	,	'hombre' => [
			[ 'price' =>  390, 'accountType' => 1, 'days' => 11, 'text' => '10 DÍAS Básico' ]
		,	[ 'price' =>  590, 'accountType' => 1, 'days' => 31, 'text' => '30 DÍAS MES Básico' ]
		,	[ 'price' => 1000, 'accountType' => 2, 'days' => 31, 'text' => '30 DÍAS MES Destacado' ]
		,	[ 'price' => 1500, 'accountType' => 3, 'days' => 31, 'text' => '30 DÍAS MES TOP' ]
		]
	,	'trans'  => [
			[ 'price' =>  390, 'accountType' => 1, 'days' => 11, 'text' => '10 DÍAS Básica']
		,	[ 'price' =>  590, 'accountType' => 1, 'days' => 31, 'text' => '30 DÍAS Básica',  ]
		,	[ 'price' => 1250, 'accountType' => 2, 'days' => 31, 'text' => '30 DÍAS Destacada' ]
		,	[ 'price' => 1900, 'accountType' => 3, 'days' => 31, 'text' => '30 DÍAS TOP' ]
		,	[ 'price' => 2900, 'accountType' => 4, 'days' => 31, 'text' => '30 DÍAS SUPER TOP (banner arriba)' ]
		]
	,	'default' => [
			[ 'price' =>  390, 'accountType' => 1, 'days' => 11, 'text' => '10 DÍAS Básica' ]
		,	[ 'price' =>  690, 'accountType' => 1, 'days' => 31, 'text' => '30 DÍAS Básica' ]
		]
	]
];

/******************************************************************************/
/* ASSETS                                                                    */
/******************************************************************************/
$config['asset_packages'] = [
	'img' => [
		'base_url' => 'https://www.pumbate.com'
	]
];


/* end of file */
