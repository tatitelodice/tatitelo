<div class="row">

	<?php
		$this->load->view('padmin/menu');
	?>
	
	<div class="span10">
		<?php if ($error == 'nouser'): ?>
			<h3 class="text-center">El usuario <?php echo $user; ?> no existe.</h3>
			
		<?php else: ?>	
			
			<h2 class="text-center"><?php echo anchor($user, $user, 'target="_blank"') ?></h2>
			<hr>
			<blockquote>
				<h4><?php echo $title ?></h4>
				<?php echo $description; ?>
				<br><br>
				Más fotos en mi página de <?php echo $site_name; ?>:<br>
				<?php echo $site; ?>
				
			</blockquote>
			
			<blockquote>
				<b>Ubicación:</b>  <?php echo $location; ?>
			</blockquote>
			
			<blockquote>
				<b>Mail falso:</b>  <?php echo $falsemail; ?>
			</blockquote>
			
			<h3>Imágenes con Marca de <?php echo $site_name; ?></h3>
			
			<?php 
				if ($promo) {
					echo '<div class="img-material img-polaroid"><div class="img-material-inner"><img src="'.$promo.'"></div></div>';
				}
			
				foreach ($pictures as $pic) {
					echo '<div class="img-material img-polaroid"><div class="img-material-inner"><img src="'.$pic.'"></div></div>';
				}
			?>
			
			<div style="margin-bottom: 40px;" class="clearfix"></div>
			
			<h3>Imágenes SIN MARCA de <?php echo $site_name; ?></h3>
			
			<?php 
				foreach ($pictures as $pic) {
					echo '<div class="img-material img-polaroid"><div class="img-material-inner"><img src="'.str_replace('watermark', 'optimized', $pic).'"></div></div>';
				}
			?>
			
			
		<?php endif; ?>
		
	</div>
	
</div>

