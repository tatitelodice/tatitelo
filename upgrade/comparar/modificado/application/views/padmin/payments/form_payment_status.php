<form id="form-info" class="form-horizontal" method="post">
    <input type="hidden" name="action" value="update">
    
    <div class="control-group">
        <label class="control-label" for="status">Estado</label>
        <div class="controls">
            <?php
                echo form_dropdown('status', $status_options, $status, ' id="status" class="input-xlarge"');
                echo form_error('status', '<span class="label label-important">Atención</span><span class="help-inline">', '</span>'); 
            ?>
        </div>          
    </div>

    <div class="control-group">
        <label class="control-label" for="amount">Pago mensual ($)</label>
        <div class="controls">
            <input type="text" class="input-medium" id="amount" name="amount" value="<?php echo set_value('amount', $amount); ?>">
            <?php echo form_error('amount', '<span class="label label-important">Atención</span><span class="help-inline">', '</span>'); ?>
        </div>          
    </div>

    <div class="control-group">
        <label class="control-label" for="day">Día límite</label>
        <div class="controls">
            <?php
                $day_options = array(
                    '1' => '10 de cada mes',
                    '2' => '20 de cada mes',
                    '3' => 'último de cada mes'
                );

                echo form_dropdown('day', $day_options, $day, ' id="day" class="input-large"');
                echo form_error('day', '<span class="label label-important">Atención</span><span class="help-inline">', '</span>'); 
            ?>
        </div>          
    </div>

    <div class="control-group">
        <label class="control-label" for="nextdate">Está al día hasta</label>
        <div class="controls">
            <input type="text" class="input-medium" id="nextdate" name="nextdate" value="<?php echo set_value('nextdate', $nextdate); ?>">
            <?php echo form_error('nextdate', '<span class="label label-important">Atención</span><span class="help-inline">', '</span>'); ?>
        </div>          
    </div>

    <div class="control-group">
        <label class="control-label" for="comments">Comentarios</label>
        <div class="controls">
            <textarea name="comments" style="width: 97%; height: 100px;"><?php echo set_value('comments', $comments); ?></textarea>
            <?php echo form_error('comments', '<span class="label label-important">Atención</span><span class="help-inline">', '</span>'); ?>
        </div>          
    </div>
    
    <div class="control-group">
        <label class="control-label" for="clear_panel_message">Borrar mensaje del panel</label>
        <div class="controls">
            <label class="checkbox">
                <input type="checkbox" name="clear_panel_message" value="1" checked="checked">
            </label>
        </div>          
    </div>

    <div class="control-group">
        <div class="controls">
            <button id="aplicar" type="submit" name="send" value="1" class="btn btn-primary btn-rose">Modificar</button>
        </div>
    </div>
</form>