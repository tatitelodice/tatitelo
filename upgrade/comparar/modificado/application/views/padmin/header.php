<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <title><?php 
		
		if (empty($title_tag)) {
			$title_tag = $this->config->item('site_admin_title_tag');
			
			if (!empty($title)) {
				$title_tag = $title . ' - ' . $title_tag;
			}
		}
		
		echo $title_tag;
		
	?></title>
    
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="<?php echo $this->config->item('site_description'); ?>">
    <meta name="author" content="<?php echo $this->config->item('site_author'); ?>">
    <meta name="keywords" content="<?php echo $this->config->item('site_keywords'); ?>">	

    <!-- Le styles -->
	<link href="<?php echo base_url('css/padmin.css'); ?>" rel="stylesheet">
	

    <!-- Le fav and touch icons -->
    <link rel="shortcut icon" href="<?php echo base_url('img/favicon.png'); ?>">
  
    <script src="<?php echo base_url('js/padmin.js'); ?>"></script>
  </head>

  <body>

    <div class="container spacehead">