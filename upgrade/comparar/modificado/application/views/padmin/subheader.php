<?php
	if ( ! ( isset($btn_back_url) && isset($btn_back_text) ) )  {
		$btn_back_url = site_url('padmin/main');
		$btn_back_text = "Página Principal";
	}
?>
<div class="row">
	<div class="span12">
		<div class="row-fluid">
			<div class="span2 brand-logo-box">
				<img class="brand-logo" src="<?php echo base_url('img/logo100x100.png'); ?>"/>
			</div>

			<h1 class="span6 top-title">				
				<?php echo $title; ?>
				<small><?php echo $subtitle; ?></small>	
			</h1>

			<div class="span4 btn-back-container">
				<?php if (isset($btn_back)): ?>
					<?php echo $btn_back; ?>
				<?php else: ?>
					<a class="btn pull-right" href="<?php echo $btn_back_url ?>">
						<?php echo $btn_back_text ?>
					</a>
				<?php endif; ?>	
			</div>
		</div>
	</div>
</div>

<hr class="subheader-hr">