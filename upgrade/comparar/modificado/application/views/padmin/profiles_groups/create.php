<?php
	$form_error_start = '<span class="label label-important">Atención</span><span class="help-inline">';
	$form_error_end = '</span>';
?>

<div class="row">

	<?php
		$this->load->view('padmin/menu');
	?>

	<div class="span10">
		
		<?php $this->load->view('padmin/partials/status_messages'); ?>
		
		<div class="row-fluid" style="margin-top: 40px;">
			<div class="span6 offset1">
				<form class="form-horizontal" method="POST">
					<div class="control-group">
						<label class="control-label" for="phone">Nombre del grupo</label>
						<div class="controls">
							<input style="width: 95%;" type="text" id="name" name="name" value="<?php echo set_value('name'); ?>">
							<?php echo form_error('name', $form_error_start, $form_error_end); ?>
						</div>			
					</div>
					
					<div class="control-group">
						<label class="control-label"></label>
						<div class="controls text-right">
							<button id="create" type="submit" name="send" value="1" class="btn btn-rose">Crear grupo</button>
						</div>
					</div>

				</form>
			</div>
		</div>
	</div>
	
</div>
