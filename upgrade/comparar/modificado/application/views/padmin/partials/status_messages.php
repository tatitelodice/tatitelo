

<?php if (isset($success_message) && $success_message): ?>
	<div class="alert alert-success text-center">
		<button class="close" data-dismiss="alert">×</button>
		<?php echo $success_message ?>
	</div>

<?php elseif (isset($error_message) && $error_message): ?>
	<div class="alert alert-error text-center">
		<button class="close" data-dismiss="alert">×</button>
		<?php echo $error_message ?>
	</div>

<?php endif; ?>