<div class="row">
	<?php
		$this->load->view('padmin/menu');
	?>
	
	<div class="span10">
		
		<div class="row-fluid">
			<div class="span12 text-right">
				<?php if (!$error && !$values_from_get): ?>
				<button class="btn" onClick="$('#form-new').removeClass('hide'); $(this).addClass('hide')">Nuevo ser indeseable</button>
				<?php endif; ?>
			</div>
		</div>
		
		<div class="row-fluid">
			<div class="span12">
				<form id="form-new" class="form-horizontal <?php if (!$error && !$values_from_get) echo 'hide'?>" method="post" action="">
					<input type="hidden" name="add_action" value="1">
					
					<div class="control-group">
						<label class="control-label" for="name">Nombre</label>
						<div class="controls">
							<input type="text" class="input-xlarge" id="name" name="name" value="<?php echo $name; ?>">
							<?php echo form_error('name', '<span class="label label-important">Atención</span><span class="help-inline">', '</span>'); ?>
						</div>			
					</div>

					<div class="control-group">
						<label class="control-label" for="phone">Teléfono</label>
						<div class="controls">
							<input type="text" class="input-xlarge" id="phone" name="phone" value="<?php echo $phone; ?>">
							<?php echo form_error('phone', '<span class="label label-important">Atención</span><span class="help-inline">', '</span>'); ?>
						</div>			
					</div>

					<div class="control-group">
						<label class="control-label" for="email">E-mail</label>
						<div class="controls">
							<input type="text" class="input-xlarge" id="email" name="email" value="<?php echo $email; ?>">
							<?php echo form_error('email', '<span class="label label-important">Atención</span><span class="help-inline">', '</span>'); ?>
						</div>			
					</div>	


					<div class="control-group">
						<label class="control-label" for="description">Motivo</label>
						<div class="controls">
							<textarea class="input-xlarge" name="description"><?php echo $description ?></textarea>
							<?php echo form_error('description', '<span class="label label-important">Atención</span><span class="help-inline">', '</span>'); ?>
						</div>			
					</div>
					
					<div class="control-group">
						<div class="controls">
							<input type="submit" class="btn btn-primary" value="Añadir">
						</div>
					</div>
				</form>
			</div>
		</div>
		
		<?php 
			if ($table) {
				echo $table;
			} else {
				echo '<h3 class="text-center">¡Felicidades Iosif, el Gulag está vacío!</h3>';
			}
		
		?>
		
	</div>
</div>