<div class="row">

    <?php
        $this->load->view('padmin/menu');
    ?>
    
    <div class="span10">
        <?php foreach ($prices as $country_iso => $country_prices): ?>
            <h3><?php echo $country_iso; ?></h3>

            <table class="table table-condensed">
            <?php foreach ($country_prices as $gender => $gender_prices): ?>
                <tr>
                    <td colspan="2">
                        <h4><?php echo $gender; ?></h4>
                    </td>
                </tr>

                <?php foreach ($gender_prices as $price_item): ?>
                    <tr>
                        <td>$ <?php echo $price_item['price'] ?></td> 
                        <td><?php echo $price_item['text'] ?></td> 
                        
                    </tr>
                <?php endforeach; ?>
                <tr><td colspan="2">&nbsp;</td></tr>
                
                
            <?php endforeach ?>
            </table>

            <hr>
        <?php endforeach ?>
        
    </div>
    
</div>