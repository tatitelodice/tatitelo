<?php if (!empty($alerts)): ?>
    <?php if (!empty($alerts['success'])): ?>
    <div class="alert alert-success text-center">
        <?php 
            echo $alerts['success'];
        ?>
        <button class="close" data-dismiss="alert">×</button>
    </div>
    <?php endif; ?>

    <?php if (!empty($alerts['error'])): ?>
    <div class="alert alert-error text-center">
        <?php 
            echo $alerts['error'];
        ?>
        <button class="close" data-dismiss="alert">×</button>
    </div>
    <?php endif; ?>

    <?php if (!empty($alerts['warning'])): ?>
    <div class="alert alert-error text-center">
        <?php 
            echo $alerts['warning'];
        ?>
        <button class="close" data-dismiss="alert">×</button>
    </div>
    <?php endif; ?>
<?php endif; ?>