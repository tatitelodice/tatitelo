<?php
    $label = (!isset($label)) ? $fieldName : $label;
?>
<div class="control-group">
    <label class="control-label" for="<?php echo $fieldName ?>">
        <?php echo $label ?>
    </label>
    <div class="controls">
        <?php if (!empty($renderHiddenInput)): ?>
        <input 
            type="hidden" 
            name="<?php echo $fieldName ?>" 
            value="<?php echo $fieldValue ?>"
        ?>
        <?php endif ?>
        
        <label class="control-label" style="text-align: left;">
            <?php echo $fieldValue ?>
        </label>

        <?php echo form_error($fieldName, '<span class="label label-important">Atención</span><span class="help-inline">', '</span>'); ?>
    </div>			
</div>