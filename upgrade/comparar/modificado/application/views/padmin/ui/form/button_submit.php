<?php
    $buttonTagClass = (!isset($buttonTagClass)) ? 'btn-large' : $buttonTagClass;
    $label = (!isset($label)) ? $fieldName : $label;
?>

<div class="control-group">
    <div class="controls">
        <button
            class="<?php echo $buttonTagClass ?>" 
            type="submit"
            name="<?php echo $fieldName ?>"
            value="<?php echo $fieldValue ?>"
        >
            <?php echo $buttonText ?>
        </button>
    </div>
</div>