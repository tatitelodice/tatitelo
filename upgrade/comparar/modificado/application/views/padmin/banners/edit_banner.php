<?php
	$form_error_start = '<br><span class="label label-important">Atención</span><span class="help-inline">';
	$form_error_end = '</span><br><br>';
?>

<div class="row">
	
	<?php
		$this->load->view('padmin/menu');
	?>
	

	<div class="span10">

		<?php if (array_filter(array($success_message, $error_message))): ?>
		<div class="row-fluid">
			<div class="span12">
				<?php $this->load->view('padmin/partials/status_messages'); ?>
			</div>
		</div>
		<?php endif; ?>
		
		<form class="form-horizontal" enctype="multipart/form-data" method="POST">
			
			<?php if (!$admin_country): ?>
			<div class="control-group">
				<label class="control-label" for="country">País</label>
				<div class="controls">
					<?php
						echo form_dropdown('country', $country_options, set_value('country', $banner['country']), ' class="input-large"');
						echo form_error('country', $form_error_start, $form_error_end);
					?>
				</div>
			</div>
			<?php else: ?>
				<input type="hidden" name="country" value="<?php echo $banner['country']; ?>">
			<?php endif; ?>
			
			<div class="control-group">
				<label class="control-label" for="category">Categoría</label>
				<div class="controls">
					<?php
						echo form_dropdown('category', $category_options, set_value('category', $banner['category']), ' class="input-large"');
						echo form_error('category', $form_error_start, $form_error_end);
					?>
				</div>
			</div>
			
			<div class="control-group">
				<label class="control-label" for="enabled">Publicado</label>
				<div class="controls">
					<?php
						echo form_dropdown('enabled', $enabled_options, set_value('enabled', $banner['enabled']), ' class="input-large"');
						echo form_error('enabled', $form_error_start, $form_error_end);
					?>
				</div>
			</div>
				
			<div class="control-group">
				<label class="control-label" for="position">Posición</label>
				<div class="controls">
					<input name="position"  type="number" min="1" value="<?php echo set_value("position", $banner['position']); ?>" placeholder="ej: 1">

					<?php
						echo form_error('position', $form_error_start, $form_error_end);
					?>
				</div>
			</div>
				
			<div class="control-group">
				<label class="control-label" for="group">Visible en</label>
				<div class="controls">
				<?php
					foreach ($device_visibility_options as $device_visibility_option => $device_visibility_text) {
						//print_r(in_array($week_day, $days));						
						$device_visibility_option_checked = '';
						
						$prev_visibility_checked = empty($_POST) && in_array($device_visibility_option, $banner['settings']['device_visibility']);
						$post_visibility_checked = !empty($_POST['device_visibility']) && in_array($device_visibility_option, $_POST['device_visibility']);
						
						if ($prev_visibility_checked || $post_visibility_checked) {
							$device_visibility_option_checked = ' checked="checked"';
						}
		
						echo '<label class="checkbox inline" style="margin-right: 5px;">';
						echo '<input type="checkbox" name="device_visibility[]" value="'.$device_visibility_option.'" ' . $device_visibility_option_checked . '>';
						echo $device_visibility_text;
						echo '</label>';						
					}
				?>
				</div>
			</div>
			
			<div class="control-group">
				<label class="control-label" for="title">Titulo</label>
				<div class="controls">
					<input name="title" style="width: 98%;" type="text" value="<?php echo set_value("title", $banner['title']); ?>" placeholder="Titulo">
					<?php echo form_error('title', $form_error_start, $form_error_end); ?>
				</div>
			</div>

			<div class="control-group">
				<label class="control-label" for="description">Descripcion</label>
				<div class="controls">
					<textarea name="description" style="width: 98%;" rows="2" placeholder="Texto del banner"><?php echo set_value("description",  $banner['description']); ?></textarea>
					<?php echo form_error('description', $form_error_start, $form_error_end); ?>
				</div>
			</div>
				
			<div class="control-group">
				<label class="control-label" for="url">Url</label>
				<div class="controls">
					<input name="url" style="width: 98%;" type="text" value="<?php echo set_value("url", $banner['url']); ?>" placeholder="Enlace web">
					<?php echo form_error('url', $form_error_start, $form_error_end); ?>
				</div>
			</div>
				
			<div class="control-group">
				<label class="control-label" for="url">Abrir en</label>
				<div class="controls">
					<?php 
						$open_in_options = array(
							'' => 'dejar que el navegador elija',
							'_blank' => 'nueva pestaña',
							'_self' => 'misma ventana',
						);
						
						$open_in_default = (empty($banner['settings']['open_in']))? '' : $banner['settings']['open_in'];
					
						echo form_dropdown('open_in', $open_in_options, set_value('open_in', $open_in_default), ' class="input-large"');
						echo form_error('open_in', $form_error_start, $form_error_end); 
					?>
				</div>
			</div>	
					
			<div class="control-group">
				<label class="control-label" for="picture">Imagen actual</label>
				<div class="controls">
					<img src="<?php echo site_url($banner['settings']['main_picture']); ?>" style="width: 30%;">
				</div>
			</div>
							
			<div class="control-group">
				<label class="control-label" for="picture">Cambiar imagen</label>
				<div class="controls">
					<input id="banner-pic" type="file" name="picture" value="<?php set_value('picture'); ?>">
					<?php 
						if ($upload_error) {
							echo $form_error_start . $upload_error . $form_error_end;
						}
					?>
				</div>
			</div>
			
			<div class="control-group">
				<div class="controls">
					<input type="submit" class="btn btn-rose" value="Editar Banner">
				</div>
			</div>

		</form>

	</div>
	
</div>


