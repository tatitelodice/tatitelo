<div class="row">

    <?php
        $this->load->view('padmin/menu');
    ?>

    <div class="span10">
        
        <div class="row-fluid">
        <?php
            $this->load->view('padmin/ui/alert_message/message_list');
        ?>
        </div>

        <div class="row-fluid">
            <form 
                class="form-horizontal" 
                method="POST" 
                action="<?php echo site_url('padmin/payment_accounts/edit/'.$paymentAccount['id']); ?>"
            >
                <input type="hidden" name="id" value="<?php echo $paymentAccount['id'] ?>">

                <?php
                    $this->load->view('padmin/ui/form/input_text', [
                        'fieldName' => 'keyname'
                    ,   'fieldValue' => $paymentAccount['keyname']
                    ,   'label' => 'Id Interno'
                    ])
                ?>

                <?php
                    $this->load->view('padmin/ui/form/input_dropdown', [
                        'fieldName' => 'isActive'
                    ,   'fieldValue' => $paymentAccount['isActive']
                    ,   'label' => 'Cuenta Activa'
                    ,   'fieldOptions' => [
                            'sí' => true
                        ,   'no' => false
                        ]
                    ])
                ?>

                <?php
                    $this->load->view('padmin/ui/form/input_dropdown', [
                        'fieldName' => 'isDefault'
                    ,   'fieldValue' => $paymentAccount['isDefault']
                    ,   'label' => 'Cuenta por Defecto'
                    ,   'fieldOptions' => [
                            'sí' => true
                        ,   'no' => false
                        ]
                    ])
                ?>

                <?php
                    $this->load->view('padmin/ui/form/input_text_readonly', [
                        'fieldName' => 'accountService'
                    ,   'fieldValue' => $paymentAccount['accountService']
                    ,   'label' => 'Tipo de Cuenta'
                    ])
                ?>

                <?php
                    $this->load->view('padmin/ui/form/input_text', [
                        'fieldName' => 'accountAlias'
                    ,   'fieldValue' => $paymentAccount['accountId']
                    ,   'label' => 'Id de Cuenta'
                    ])
                ?>

                <?php
                    $this->load->view('padmin/ui/form/input_text', [
                        'fieldName' => 'accountAlias'
                    ,   'fieldValue' => $paymentAccount['accountAlias']
                    ,   'label' => 'Alias de Cuenta'
                    ])
                ?>

                <?php
                    $this->load->view('padmin/ui/form/input_text', [
                        'fieldName' => 'currencyIsoCode'
                    ,   'fieldValue' => $paymentAccount['currencyIsoCode']
                    ,   'label' => 'Moneda'
                    ])
                ?>

                <?php
                    $this->load->view('padmin/ui/form/input_text', [
                        'fieldName' => 'priority'
                    ,   'fieldValue' => $paymentAccount['priority']
                    ,   'label' => 'Prioridad'
                    ])
                ?>

                <?php
                    $this->load->view('padmin/ui/form/input_text', [
                        'fieldName' => 'monthlyLimit'
                    ,   'fieldValue' => $paymentAccount['monthlyLimit']
                    ,   'label' => 'Límite Mensual'
                    ])
                ?>

                <?php
                    $this->load->view('padmin/ui/form/input_text', [
                        'fieldName' => 'tags'
                    ,   'fieldValue' => $paymentAccount['tags']
                    ,   'label' => 'Etiquetas'
                    ])
                ?>

                <?php
                    $this->load->view('padmin/ui/form/button_submit', [
                        'fieldName' => 'onFormSubmit'
                    ,   'fieldValue' => 'edit'
                    ,   'buttonText' => 'Modificar'
                    ,   'buttonTagClass' => 'btn btn-large btn-primary'
                    ])
                ?>
            </form>
        </div>
    </div>
    
</div>