<table class="table">
    <thead>
        <tr>
            <th>Nombre</th>
            <th>Servicio</th>
            <th>Id</th>
            <th class="text-center">Moneda</th>
            <th class="text-center">Balance del Mes</th>
            <th class="text-center">Por Defecto</th>
            <th class="text-center">Prioridad</th>
            <th>Acciones</th>
        </tr>
    </thead>
    <tbody>
    <?php foreach ($paymentAccounts as $paymentAccount): ?>
        <tr>
            <td><?php echo $paymentAccount['name']; ?></td>
            <td><?php echo $paymentAccount['accountService']; ?></td>
            <td><?php echo $paymentAccount['accountId']; ?></td>
            <td class="text-center">
                <?php echo $paymentAccount['currencyIsoCode']; ?>
            </td>
            <td class="text-right">
                <?php echo $paymentAccount['totalDepositsBalance']; ?>
            </td>
            <td class="text-center">
                <?php echo $paymentAccount['isDefault'] ? 'sí' : ''; ?>
            </td>
            <td class="text-right">
                <?php echo $paymentAccount['priority']; ?>
            </td>
            <td class="text-center">
                <a 
                    href="<?php echo site_url("padmin/payment_accounts/details/" . $paymentAccount['id']) ?>"
                    title="Detalles"
                >
                    <span class="glyphicon glyphicon-list-alt"></span>
                </a>

                <a 
                    href="<?php echo site_url("padmin/payment_accounts/edit/" . $paymentAccount['id']) ?>"
                    title="Editar"
                    style="margin-left: 0.3rem"
                >
                    <span class="glyphicon glyphicon-edit"></span>
                </a>

                <a 
                    href="<?php echo site_url("padmin/deposits/list_deposits/?realTotal=T&paymentAccount=" . $paymentAccount['id']) ?>"
                    title="Depósitos"
                    style="margin-left: 0.3rem"
                >
                    <span class="glyphicon glyphicon-usd"></span>
                </a>
            </td>
        </tr>
    <?php endforeach; ?>
    </tbody>
</table>