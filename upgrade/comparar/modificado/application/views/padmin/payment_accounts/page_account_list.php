<div class="row">

    <?php
        $this->load->view('padmin/menu');
    ?>

    <div class="span10">
        
        <div class="row-fluid">
        <?php
            $this->load->view('padmin/ui/alert_message/message_list');
        ?>
        </div>

        <div class="row-fluid">
            <h3>Cuentas Activas</h3>

            <?php
                $this->load->view('padmin/payment_accounts/partial_accounts_table', [
                    'paymentAccounts' => $activePaymentAccounts
                ]);
            ?>

            <h3>Cuentas Inactivas</h3>

            <?php
                $this->load->view('padmin/payment_accounts/partial_accounts_table', [
                    'paymentAccounts' => $inactivePaymentAccounts
                ]);
            ?>

            <h3>Acciones Globales</h3>
            
            <ul>
                <li>
                    <a 
                        href="<?php echo site_url("padmin/payment_accounts/mass_assign_last_payment_account") ?>"
                        title="Asignar última cuenta de pago"
                        style="font-size: 1.1rem"
                    >
                        Asignar a todos última cuenta de pago activa
                    </a>
                </li>
            </ul>
        </div>
    </div>

</div>