<div class="row">
	<div class="span12">
		<form method="POST">
			<textarea name="sql_query" rows="8" style="width: 99%;"><?php echo $current_query; ?></textarea>
			<div class="row-fluid text-right">
				<input class="btn btn-large" type="submit" name="send" value="Ejecutar">
			</div>
		</form>	
	</div>
</div>

<?php if ($total_rows): ?>
<div class="row">
	<div class="span12">
		<h3 class="text-center">Resultados (<?php echo $total_rows; ?>)</h3>
		
		<table class="table table-condensed">
		<?php
			$headers = array_keys($results[0]);
			
			echo '<thead><tr>';
			foreach ($headers as $head) {
				echo '<th>' . $head . '</th>';
			}
			echo '</tr></thead>';
		
			echo '<tbody>';
			
			foreach ($results as $row) {
				echo '<tr>';
				
				foreach ($row as $cell) {
					echo '<td>' . $cell . '</td>';
				}
				
				echo '</tr>';
			}
			
			echo '</body>';
		?>
		</table>	
	</div>
</div>
<?php endif; ?>