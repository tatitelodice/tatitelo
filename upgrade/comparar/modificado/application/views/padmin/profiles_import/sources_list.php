<div class="row">

	<?php
		$this->load->view('padmin/menu');
	?>

	<div class="span10">
		<div class="well well-small">
			<h4>Importar perfil desde <?php echo $from_site; ?></h4>
			<form id="form_import_profile" class="form-inline" method="post">
				<input type="hidden" name="source" value="universal">
				<input id="origin_user" type="text" name="origin_user" placeholder="usuario <?php echo $from_site; ?>" autocomplete="off" value="<?php echo set_value("origin_user"); ?>">
				<input id="rename_user" type="text" name="rename_user" placeholder="nuevo usuario <?php echo $to_site; ?>" autocomplete="off" value="<?php echo set_value("rename_user"); ?>">
				<input id="send_btn" class="btn" type="submit" value="Importar">
			</form>
			
			<?php
				$error_start =  '<span class="label label-important">Atención</span><span class="help-inline">';
				$error_end = '</span><br>';
			
				echo form_error('source', $error_start, $error_end); 
				echo form_error('origin_user', $error_start, $error_end); 
				echo form_error('rename_user', $error_start, $error_end); 
				
				if ($output) {
					echo $output;
				}
			?>
		</div>
	</div>
	
	<script>
		$("#send_btn").click(function() {
			var rename_val = $("#rename_user").val().trim();
			var origin_val = $("#origin_user").val().trim();
			
			if (rename_val === '') {
				$("#rename_user").val(origin_val);
			}
		});
	</script>	
	
</div>
