<div class="row">

	<?php
		$this->load->view('padmin/menu');
	?>
	
	<div class="span10">
					
		<div class="row-fluid">
			<div class="span12">
				<form class="form-inline" method="post" action="<?php echo site_url('padmin/ads') ?>">
					<input placeholder="usuario" type="text" class="input-medium" name="user" value="<?php echo $user ?>">
					<input placeholder="fuente (sitio de anuncios)" type="text" class="input-large" name="source" value="<?php echo $source ?>">
					&nbsp;&nbsp;&nbsp;
					<label class="checkbox"><input id="only_urls" type="checkbox" name="only_urls" value="1"<?php if ($only_urls) echo ' checked="checked"'; ?>>Sólo urls</label>
					&nbsp;&nbsp;&nbsp;
					<button id="btn-filter" class="btn" type="submit">Filtrar</button>
				</form>
			</div>
		</div>	
		
		<div class="row-fluid">
			<div class="span12 text-right">
				<?php if ($ads_full_count > $ads_limit):?>
					Últimos <?php echo $ads_limit; ?> de <?php echo $ads_full_count; ?> anuncios publicados.
				<?php elseif  ($ads_full_count): ?>
					<?php echo $ads_full_count; ?> publicados.
				<?php endif; ?>
			</div>
		</div>
		
		<div class="row-fluid">
			<?php if ($ads_count == 0) : ?>
				<div class="row-fluid">
					<div class="span12">
						<h4 class="text-center" style="padding-top: 20px;">No hay publicaciones de anuncios nuevas.</h4>
					</div>
				</div>

			<?php else: ?>

				<?php echo $ads_table; ?>

			<?php endif; ?>
		</div>
	</div>
	
</div>

