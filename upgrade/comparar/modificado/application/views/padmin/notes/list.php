<div class="row">

	<?php
		$this->load->view('padmin/menu');
	?>

	<div class="span10">
		
		<div class="row-fluid">
			<div class="span8">
				<h4 class="text-left">
					<b><?php echo $total_notes; ?> nota<?php echo ($total_notes > 1)? "s" : "" ?></b>
				</h4>
			</div>
			<div class="span4">
				<p class="text-right">
					<a class="btn" style="margin-top: 4px;" href="<?php echo site_url("padmin/notes/create");?>">
						<span class="glyphicon glyphicon-plus"></span>&nbsp;Crear nota
					</a>
				</p>
			</div>
		</div>	
		
		<div class="row-fluid" style="padding-top: 15px;">
		<?php for($i=0, $ct=count($notes); $i < $ct; $i++): ?>
	
			<?php if ($i > 0 && $i % 4 == 0): ?>
				</div><div class="row-fluid" style="padding-top: 15px;">
			<?php endif; ?>
			
			<?php $note = $notes[$i]; ?>

			<div class="span3">
				<div class="note-well">
					<button onclick="modal_delete(<?php echo $note['id']; ?>)" type="button" class="close" data-toggle="modal">&times;</button>

					<div class="title">
						<a href="<?php echo site_url("padmin/notes/view/" . $note['id']) ?>">
							<span class="glyphicon glyphicon-link"></span>&nbsp;<?php echo $note['title']; ?>
						</a>
					</div>
					<p class="text-justify">
						<?php echo $note["preview"]; ?>
					</p>
				</div>	
			</div>
		<?php endfor; ?>		
		</div>	
	</div>
</div>


<!-- Modal -->
<div id="confirm-delete-modal" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	<div class="modal-header">
		<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
		<h3 id="myModalLabel">Confirmar</h3>
	</div>
	<div class="modal-body">
		<p>¿Seguro que quieres borrar esta nota?</p>
	</div>
	<div class="modal-footer">
		<button class="btn" data-dismiss="modal" aria-hidden="true">No</button>
		<a id="confirm-delete-link" class="btn btn-rose" href="">Sí, borrarla</a>
	</div>
</div>

<script>
	function modal_delete(note_id) {
		$('#confirm-delete-link').attr('href', '<?php echo site_url('padmin/notes/delete/'); ?>' + '/' + note_id);
		$('#confirm-delete-modal').modal('toggle');
	}
</script>	
