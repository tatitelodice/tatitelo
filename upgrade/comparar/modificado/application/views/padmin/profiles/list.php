<div class="row">
	<?php
		$this->load->view('padmin/menu');
	?>
	
	<div class="span10">
		<div class="row-fluid">
			<div class="span12">
				<form class="form-inline" method="post" action="<?php echo site_url('padmin/profiles/change_filters') ?>">
					<label>Mostrar:</label>
					<?php
						echo form_dropdown('filter_gender', $gender_options, $filter_gender, ' class="input-small"');
					?>
					
					<label>de estado</label>
					<?php
						echo form_dropdown('filter_status', $status_options, $filter_status, ' class="input-medium"');
					?>
					
					
					<?php
						if (! (isset($admin_country) && $admin_country) ) {
							echo '<label>&nbsp;en&nbsp;</label>';
							echo form_dropdown('filter_country', $country_options, $filter_country, ' class="input-medium"');
						}
					?>
					
					<label>ordenando por</label>
					<?php
						echo form_dropdown('filter_order', $order_options, $filter_order, ' class="input-large"');
					?>
					
					<input id="terms-hidden" type="hidden" name="terms" value="<?php echo $terms ?>">

					<button id="btn-filter" class="btn" type="submit">Aplicar</button>
				</form>
			</div>
		</div>
		
		<div id="list-start" class="row-fluid" style="margin-top: 15px; margin-bottom: 15px;">
			<div class="span6">
				<form class="form-inline" method="get" action="">
					<input id="terms" placeholder="ej: telefono ó nombre" type="text" class="input-xlarge" name="terms" value="<?php echo $terms ?>">
					<button id="btn-buscar" class="btn" type="submit">Buscar</button>
				</form>
			</div>

			<div class="span6">
				<p class="text-right" style="margin-top: 6px; ">
					<?php $this->load->view("padmin/profiles/pagination") ?>
				</p>				
			</div>

		</div>
		
		<div class="row-fluid">
			<?php if (count($accounts)): ?>
			
				<div class="hidden-phone">
					<?php $this->load->view("padmin/profiles/list_desktop");?>
				</div>					
				
				<div class="hidden-desktop">
					<?php $this->load->view("padmin/profiles/list_phone");?>
				</div>
			
				<hr>
				
				<p class="text-right margin-top-10 margin-bottom-20">
					<?php $this->load->view("padmin/profiles/pagination") ?>
				</p>	
			
			<?php else: ?>
				<h4 class="text-center">
					No se encontraron cuentas registradas
				</h4>
			<?php endif?>
			
		</div>		
	</div>
	
</div>

<script>
	$('#btn-filter').click(function(e) {
		//e.preventDefault();
		var terms_val = $('#terms').val(); // Modify me
		$('#terms-hidden').val(terms_val);
		$('#btn-filter').submit();
	});
</script>


<?php if(isset($_GET["terms"])): ?>
	<script>
		if ( $(window).width() <= 480) {
			$('html, body').scrollTop($("#list-start").offset().top);
		}
	</script>
<?php endif; ?>
