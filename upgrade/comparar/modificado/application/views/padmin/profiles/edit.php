<div class="row">
	
	<div class="span12">
		<?php $this->load->view('padmin/partials/status_messages'); ?>
	</div>
	
	<form class="form-horizontal" method="post" action="<?php echo site_url('padmin/profiles/edit/'.$id); ?>">
		
		<div class="span12">
			<h2 class="text-center">
				<?php echo '<a target="_blank" href="'.site_url($user).'">' . $user . '</a>'; ?>
			</h2>			
			<p class="text-center" style="font-size: 10px; font-weight: bold;">Registrado el <?php echo $createdDate; ?></p>
			
			<div class="account-actions">
				<a class="mini-btn-account-action" target="_blank" href="<?php echo site_url("$user/panel"); ?>">Entrar al panel</a>
				<a class="mini-btn-account-action" href="<?php echo site_url("padmin/profiles/change_username/$user"); ?>">Renombrar cuenta</a>
				<a class="mini-btn-account-action" target="_blank" href="<?php echo site_url("padmin/profiles/reset_password/$user"); ?>">Resetear contraseña</a>
				<a class="mini-btn-account-action" target="_blank" href="<?php echo site_url('padmin/logs?user=' . $user); ?>">Ver logs</a> 
				<a class="mini-btn-account-action" target="_blank" href="<?php echo site_url("padmin/blacklist?name=$user&phone=$phone&email=$email&description=fast+fuck"); ?>">Agregar a lista negra</a>
				<a class="mini-btn-account-action" target="_blank" href="<?php echo site_url("padmin/blacklist/clear_user/$user"); ?>">Eliminar ban de tel/email/ip</a>
			</div>
			<hr>
		</div>
	
		<div class="span6">
		
			<input type="hidden" id="id" name="id" value="<?php echo $id; ?>">
			
			<div class="control-group hide">
				<label class="control-label" for="id">Fuente</label>
				<div class="controls">
					<input type="text" class="input-xlarge editable" disabled="" id="source" name="source" value="<?php echo set_value('source', $source); ?>">
					<?php echo form_error('source', '<span class="label label-important">Atención</span><span class="help-inline">', '</span>'); ?>
				</div>			
			</div>
			
			<div class="control-group">
				<label class="control-label" for="group">Grupo</label>
				<div class="controls">
					<?php
						echo form_dropdown('group', $group_options, set_value('group', $group), ' id="group" class="input-xlarge editable" disabled=""');
						echo form_error('group', '<span class="label label-important">Atención</span><span class="help-inline">', '</span>'); 
					?>
				</div>			
			</div>

			<div class="control-group">
				<label class="control-label" for="phone">Teléfono Original</label>
				<div class="controls">
					<input type="text" class="input-xlarge editable" disabled="" id="phone" name="phone" value="<?php echo set_value('phone', $phone); ?>">
					<?php echo form_error('phone', '<span class="label label-important">Atención</span><span class="help-inline">', '</span>'); ?>
				</div>			
			</div>

			<div class="control-group">
				<label class="control-label" for="email">E-mail</label>
				<div class="controls">
					<input type="text" class="input-xlarge editable" disabled="" id="email" name="email" value="<?php echo set_value('email', $email); ?>">
					<?php echo form_error('email', '<span class="label label-important">Atención</span><span class="help-inline">', '</span>'); ?>
				</div>			
			</div>	
			
			
			<div class="control-group">
				<label class="control-label" for="cliComments">Comentarios del cliente</label>
				<div class="controls">
					<textarea name="comments" class="editable" style="width: 97%; height: 100px;" disabled=""><?php echo $comments; ?></textarea>
				</div>			
			</div>
			
			<div class="control-group">
				<label class="control-label" for="panel_message">Mensaje personalizado a mostrar en el Panel</label>
				<div class="controls">
					<textarea name="panel_message" class="editable" style="width: 97%; height: 100px;" disabled=""><?php echo $panel_message; ?></textarea>
					<?php echo form_error('panel_message', '<span class="label label-important">Atención</span><span class="help-inline">', '</span>'); ?>
				</div>			
			</div>
		</div>
	
		<div class="span6">
						
			<div class="control-group">
				<label class="control-label" for="gender">Género</label>
				<div class="controls">
					<?php
						echo form_dropdown('gender', $gender_options, $gender, ' id="gender" class="input-large editable" disabled=""');
						echo form_error('gender', '<span class="label label-important">Atención</span><span class="help-inline">', '</span>');
					?>
				</div>			
			</div>					

			<div class="control-group">
				<label class="control-label" for="country">País</label>
				<div class="controls">
					<?php
						echo form_dropdown('country', $country_options, $country, ' id="country" class="input-large editable" disabled=""');
						echo form_error('country', '<span class="label label-important">Atención</span><span class="help-inline">', '</span>');
					?>
				</div>			
			</div>
			
			<div class="control-group">
				
				<?php if ($deleted == PROFILE_DELETED): ?>
					<label class="control-label" for="verified">
						Perfil eliminado
					</label>
				
					<div class="controls" style="padding-top: 4px;">
						<a href="<?php echo site_url('padmin/profiles/undelete/'.$user) . '?redirect=edit' ?>" class="btn btn-mini">
							Recuperar
						</a>
					</div>
				<?php else: ?>
					<label class="control-label" for="verified">Verificado</label>
				
					<div class="controls">
						<?php
							echo form_dropdown('verified', $verified_options, $verified, ' id="verified" class="input-large editable" disabled=""');
							echo form_error('verified', '<span class="label label-important">Atención</span><span class="help-inline">', '</span>');
						?>
					</div>
				<?php endif; ?>
			</div>
			
			<div class="control-group">
				<label class="control-label" for="email">Prioridad</label>
				<div class="controls">
					<?php if ($verified != PROFILE_STATUS_VERIFIED): ?>
						<p style="padding-top: 5px;">Debe estar verificado para tener una prioridad en la galería</p>
						<input type="hidden" id="priority" name="priority" value="<?php echo $priority; ?>">
					<?php else: ?>
						<input type="text" class="input-small editable" disabled="" id="priority" name="priority" value="<?php echo $priority; ?>"> de <?php echo $max_priority; ?>
						<?php echo form_error('priority', '<span class="label label-important">Atención</span><span class="help-inline">', '</span>'); ?>
					<?php endif; ?>
				</div>			
			</div>
			
			<div class="control-group">
				<label class="control-label" for="account_type">Tipo de Cuenta</label>
				<div class="controls">
					<?php
						echo form_dropdown('account_type', $account_type_options, $account_type, ' id="account_type" class="input-large editable" disabled=""');
						echo form_error('account_type', '<span class="label label-important">Atención</span><span class="help-inline">', '</span>');
					?>
				</div>			
			</div>
			
			<div class="control-group">
				<label class="control-label" for="verified_pictures">Estado de Galería</label>
				<div class="controls">
					<?php
						echo form_dropdown('verified_pictures', $verified_pictures_options, $verified_pictures, ' id="verified-pictures" class="input-large editable" disabled=""');
						echo form_error('verified_pictures', '<span class="label label-important">Atención</span><span class="help-inline">', '</span>');
					?>
				</div>			
			</div>
				

			<div class="control-group">
				<label class="control-label" for="absent">Ausente</label>
				<div class="controls">
					<?php
						echo form_dropdown('absent', $absent_options, $absent, ' id="absent" class="input-large editable" disabled=""');
						echo form_error('absent', '<span class="label label-important">Atención</span><span class="help-inline">', '</span>');
					?>
				</div>			
			</div>	
			
			<div id="absent_block" class="hidden">			
				<div class="control-group">
					<label class="control-label" for="absent_message">Mensaje de ausencia</label>
					<div class="controls">
						<textarea name="absent_message" class="editable" style="width: 97%; height: 100px;" disabled=""><?php echo $absent_message; ?></textarea>
					</div>			
				</div>
			</div>

		</div>
		
		<div class="span12">
			<div class="well text-center">
				<button id="editar" type="button" class="btn input-xlarge btn-rose">Habilitar edición de datos</button>
				<button id="aplicar" type="submit" name="send" value="1" class="btn input-xlarge btn-rose hide">Aplicar cambios</button>
			</div>
		</div>
	</form>
</div>

<script type="text/javascript">
	$("#editar").show();
	$("#aplicar").hide();
	$('.editable').attr('disabled', 'true');
	
	$("#editar").click(function() {
		$('.editable').removeAttr('disabled');
		$("#editar").hide();
		$("#aplicar").show();
	});
</script>

<script>
	if ( $('#absent').val() === '1' ) {
		$('#absent_block').removeClass('hidden');
	}
	
	$('#absent').change(function() {
		if ( $('#absent').val() === '1' ) {
			$('#absent_block').removeClass('hidden');
		} else {
			$('#absent_block').addClass('hidden');
		}
	});
</script>


<?php if (isset($apply_visible)) : ?>
<script>
	$("#editar").click();
</script>
<?php endif; ?>
