<style>
    .alert-customer-message {
        border: 1px solid #c6c6c6;
        background: transparent;
        color: #414141;
    }

    .alert-customer-message[data-message-status="unread"]
    {
        border: 2px solid #59d178;
    }
</style>

<div class="row">

    <?php
        $this->load->view('padmin/menu');
    ?>
    
    <div class="span10">
        <div class="row-fluid" style="margin-bottom: 1rem;">
            <div class="span12 text-right">
                <a href="<?php echo site_url("padmin/messages/outbox"); ?>" 
                   class="btn btn-default">
                   Enviados
                </a>
                <a href="<?php echo site_url("padmin/messages/outbox"); ?>?status=pending" 
                   class="btn btn-default">
                   Bandeja de Salida
                </a>
                <a href="<?php echo site_url("padmin/messages/write"); ?>" 
                   class="btn btn-default">
                   Nuevo Mensaje
                </a>
            </div>
        </div>

        <?php if (count($customer_messages) == 0) : ?>
            <div class="row-fluid">
                <div class="span12">
                    <h4 class="text-center" style="padding-top: 20px;">No hay mensajes nuevos.</h4>
                </div>
            </div>
        
        <?php else: ?>
            <?php foreach ($customer_messages as $cm): ?>
            <div class="alert-notification-row row-fluid">
                <div class="span12">
                    <div class="alert alert-customer-message"
                         data-message-status="<?php echo ($cm->isPendingRead)? 'unread' : 'read' ?>"
                    >
                        <?php echo $cm->messageText; ?>

                        <?php if (!empty($cm->attributes->deposit)): ?>
                            <a 
                                href="<?php echo site_url('padmin/deposits/view_notification/' . $cm->id); ?>"
                                target="_blank"
                            >
                                Ver Detalles
                            </a>
                            <br>
                        <?php endif; ?>

                        <hr style="margin: 10px 0;">
                        <?php if (!empty($cm->attributes->profiles) && strlen($cm->customerAddress) > 8): ?>
                            Por: 
                            <?php foreach ($cm->attributes->profiles as $cm_profile): ?>
                                <a target="_blank" href=<?php echo site_url('padmin/profiles/edit/' . $cm_profile->id) ?>>
                                    <?php echo $cm_profile->user; ?>
                                </a>
                            <?php endforeach; ?>
                            <br>
                        <?php endif; ?>
                        
                        Recibido desde <?php echo $cm->customerAddress; ?> 
                        el <?php echo $cm->createdAt; ?>

                        <a class="mini-btn-account-action" target="_blank" href="<?php echo site_url("padmin/messages/write") . '?replyTo=' . $cm->id ; ?>">Responder</a>
                    </div>
                </div>  

            </div>
            <?php endforeach; ?>
        <?php endif; ?>

        <div class="row-fluid">
            <div class="span12 text-center">
                <?php if ($offset != 0): ?>
                <a class="btn btn-large" href="<?php echo site_url('padmin/messages') . '?offset=' . ($offset - $offset_limit); ?>">
                    Anteriores 
                </a>
                <?php endif; ?>

                <a class="btn btn-large" href="<?php echo site_url('padmin/messages') . '?offset=' . ($offset + $offset_limit); ?>">
                    Siguientes mensajes
                </a>
            </div>
        </div>

    </div>
    
    <script>
        $('.alert-notification .close').click(function(evt)
        {
            var alert_row = $(this).parents('.alert-notification-row');
            
            var url = $(this).attr('href') + '?ajax=true';
            evt.preventDefault();
            
            $.get(url, function(data) {
                if (data === 'success') {
                    alert_row.remove();
                    
                    var el_notification_count = $('.notification-count');
                    
                    if (el_notification_count) {
                        var prev_val = parseInt(el_notification_count.text());
                        
                        if (prev_val > 0) {
                            el_notification_count.html(prev_val - 1);
                        }
                        
                        if (prev_val === 1) {
                            el_notification_count.remove();
                        }
                    }
                }
            });
        });
    </script>
    
</div>

