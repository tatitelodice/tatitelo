<form name="form-apply-deposit" method="POST" class="form-horizontal form-apply-deposit">
    <div class="alert alert-danger text-center" style="display: none;">
    </div>

    <input type="hidden" name="deposit_id" value="">

    <div class="control-group">
        <label class="control-label" for="deposit_user">
            Usuario:
        </label>
        <div class="controls">
            <input 
                name="deposit_user" 
                type="text"
                value="<?php echo (!empty($defaultUser)) ? $defaultUser : "" ?>"
            >
            <br>
            <label class="checkbox" style="margin-top: 0.8rem;">
                <input name="deposit_from_non_user" type="checkbox" value="1">
                no es un usuario especifico
            </label>
        </div>
    </div>

    <div class="control-group">
        <label class="control-label" for="deposit_concept">
            Concepto:
        </label>
        <div class="controls">
            <?php
                $default_deposit_concept = isset($default_deposit_concept) ? $default_deposit_concept : '';
            ?>
            <input name="deposit_concept" type="text" value="<?php echo set_value('deposit_concept', $default_deposit_concept); ?>">
        </div>
    </div>

    <div class="control-group">
        <label class="control-label" for="deposit_apply_date">
            Fecha acreditado:
        </label>
        <div class="controls">
            <input type="text" class="input-small" placeholder="Fecha" name="deposit_apply_date" value="<?php echo set_value('deposit_apply_date', date('d/m/Y')); ?>">
        </div>
    </div>

</form>