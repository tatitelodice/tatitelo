<div class="row">

    <div class="span6">
        <h4>Información</h4>
        <table class="table">
            <tr>
                <td style="font-weight: bold;">Id Interno:</td>
                <td style="width:50%">
                    <?php echo $deposit->id ?>
                </td>
            </tr>        
            <tr>
                <td style="font-weight: bold;">Id Externo:</td>
                <td style="width:50%">
                    <?php echo $deposit->externalId ?>
                </td>
            </tr>

            <tr>
                <td style="font-weight: bold;">Monto:</td>
                <td style="width:50%">
                    $<?php echo $deposit->amount ?>
                </td>
            </tr>

            <tr>
                <td style="font-weight: bold;">Fecha:</td>
                <td style="width:50%">
                    <?php echo preg_replace("!(\d+)-(\d+)-(\d+)!", "$3/$2/$1", $deposit->date) ?>
                </td>
            </tr>
            <?php if (!empty($deposit->time)): ?>
            <tr>
                <td style="font-weight: bold;">Hora:</td>
                <td style="width:50%">
                    <?php echo $deposit->time ?>
                </td>
            </tr>
            <?php endif; ?>

            <tr>
                <td style="font-weight: bold;">Número de autorización o transacción:</td>
                <td style="width:50%">
                    <?php echo $deposit->name ?>
                </td>
            </tr>
            <tr>
                <td style="font-weight: bold;">Usuario:</td>
                <td style="width:50%">
                    <?php echo $deposit->user ?>
                </td>
            </tr>
            <tr>
                <td style="font-weight: bold;">Comentarios:</td>
                <td style="width:50%">
                <?php if (!empty($deposit->comments)): ?>
                    <?php echo $deposit->comments ?>
                <?php else: ?>
                    ( ninguno )
                <?php endif; ?>
                </td>
            </tr>
        </table>
    </div>

    <div class="span6">
        <h4>Acciones</h4>

        <table class="table">
            <tr>
                <td style="font-weight: bold">
                    <a href="<?php echo $actionLinks['voidDepositValue'] ?>" target="_blank">
                    Poner como valor nulo (0)
                    </a>
                </td>
                <td>Si es un depósito personal que no corresponde al sitio.</td>
            </tr>
        </table>
    </div>

</div>