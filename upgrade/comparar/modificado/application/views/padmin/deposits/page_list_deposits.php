<div class="row">

	<?php
		$this->load->view('padmin/menu');
	?>

	<div class="span10">
		
		<div class="row-fluid text-left" style="margin-bottom: 10px;">
			<form class="form-inline" method="GET">
				Ver depósitos hechos desde el &nbsp;
				<input id="startDate" name="startDate" value="<?php echo $startDate; ?>" type="text" class="input-small"> 
				&nbsp;al&nbsp;
				<input id="endDate" name="endDate" value="<?php echo $endDate; ?>"  type="text" class="input-small">

				<?php foreach (array_diff(array_keys($_GET), ['startDate', 'endDate']) as $field) : ?>
				<input type="hidden" name="<?php echo $field; ?>" value="<?php echo $_GET[$field]; ?>">
				<?php endforeach; ?>

				<button class="btn" type="submit">Cambiar</button>
			</form>
		</div>
		
		<?php if (count($deposits)): ?>
			<?php
				$this->load->view('padmin/deposits/partial_deposit_list_table', [
					'deposits' => $deposits
				,	'totalAmount' => $totalAmount
				]);
			?>
		<?php else: ?>
			<br><br>
			<h4 class="text-center">No se encontraron depósitos entre las fechas seleccionadas.</h4>
		<?php endif; ?>
	</div>
	
</div>


<script>
	var depositsData = <?php echo json_encode($deposits); ?>;
	var applyDepositBaseUrl = <?php echo json_encode(site_url('padmin/deposits/apply_deposit')) ?>; 

	var depositsDataById = {};

	depositsData.forEach(function(dep)
	{
		depositsDataById[dep.id + ''] = dep;
	});

	$(document).ready(function()
	{
		var $formLinkDeposit = $('form[name="form-apply-deposit"]');
		var linkDepositModal;

		$( "#startDate" ).datepicker();
		$( "#endDate" ).datepicker();

		$( '[data-action="apply-deposit"]' ).click(function()
		{
			var $el = $(this);
			var depositId = $el.attr('data-deposit-id');

			$formLinkDeposit.find('[name="deposit_id"]').val(depositId);

			var dep = depositsDataById[depositId];
			//debugger;
			var depositInfoText = dep.date + ' - ' + dep.name + ' - $' + dep.amount;

			$('.modal [data-role="deposit-info"]').html(
				depositInfoText
			);

			linkDepositModal = $('.modal').modal();
		});

		$('[data-action="submit-form-apply-deposit"]').click(function(e)
		{
			e.preventDefault();
			$formLinkDeposit.submit();
		});

		$formLinkDeposit.on('submit', function(evt) {
			evt.preventDefault(); // prevent default form submit

			var $thisForm = $(evt.target);
			var $thisAlert = $thisForm.find('.alert');

			$.ajax({
				url: applyDepositBaseUrl, // form action url
				type: 'POST', // form submit method get/post
				dataType: 'json', // request type html/json/xml
				data: $thisForm.serialize(), // serialize form data 
				beforeSend: function() {
					$thisAlert.fadeOut();
					//submit.html('Sending....'); // change submit button text
				},
				success: function(data) {
					//	alert.html(data).fadeIn(); // fade in response data
					//	submit.html('Send Email'); // reset submit button text
					if (data.success)
					{
						$thisForm.trigger('reset'); // reset form
						window.location.reload();	
					}
					else
					{
						$thisAlert.html(
							'<p style="font-weight: bold;">Errores encontrados:</p>' +
							data.error
						);

						$thisAlert.fadeIn();
					}
				},
				error: function(e) {
				//	console.log(e)
				}
			});
		});
	});
</script>

<div class="modal hide fade">
	<div class="modal-header">
		<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
		<h4 class="text-center">Acreditar pago a usuario</h4>
	</div>

	<div class="modal-body">
		<h3 class="text-center" data-role="deposit-info"></h3>
		<?php
			$this->load->view('padmin/deposits/form_apply_deposit');
		?>
	</div>
	<div class="modal-footer">
		<a data-dismiss="modal" class="btn">Cancelar</a>
		<a data-action="submit-form-apply-deposit" class="btn btn-primary">Acreditar</a>
	</div>
</div>