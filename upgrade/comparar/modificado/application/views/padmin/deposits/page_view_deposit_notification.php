<div class="row">

    <div class="span4">
        <h4>Información</h4>
        <table class="table">
            <tr>
                <td style="font-weight: bold;">Monto:</td>
                <td style="width:50%">
                    $<?php echo $deposit->amount ?>
                </td>
            </tr>

            <tr>
                <td style="font-weight: bold;">Fecha:</td>
                <td style="width:50%">
                    <?php echo preg_replace("!(\d+)-(\d+)-(\d+)!", "$3/$2/$1", $deposit->date) ?>
                </td>
            </tr>
            <?php if (!empty($deposit->time)): ?>
            <tr>
                <td style="font-weight: bold;">Hora:</td>
                <td style="width:50%">
                    <?php echo $deposit->time ?>
                </td>
            </tr>
            <?php endif; ?>

            <tr>
                <td style="font-weight: bold;">Número de autorización o transacción:</td>
                <td style="width:50%">
                    <?php echo $deposit->name ?>
                </td>
            </tr>
            <tr>
                <td style="font-weight: bold;">Telefono:</td>
                <td style="width:50%">
                    <?php echo $deposit->phone ?>
                </td>
            </tr>
            <tr>
                <td style="font-weight: bold;">Usuario:</td>
                <td style="width:50%">
                    <?php echo $deposit->user ?>
                </td>
            </tr>
            <tr>
                <td style="font-weight: bold;">Comentarios:</td>
                <td style="width:50%">
                <?php if (!empty($deposit->comments)): ?>
                    <?php echo $deposit->comments ?>
                <?php else: ?>
                    ( ninguno )
                <?php endif; ?>
                </td>
            </tr>
        </table>
    </div>

    <div class="span4">
        <h4>Foto de ticket</h4>
        <?php if (!empty($deposit->photo)): ?>
        <div class="text-center" style="width: 100%;" >
            <a href="<?php echo base_url($deposit->photo) ?>" target="_blank">
                <img src="<?php echo base_url($deposit->photo) ?>">
            </a>
        </div>
        <?php else: ?>
        <p>
            No se adjuntó foto.
        </p>
        <?php endif; ?>
    </div>

    <div class="span4">
        <h4>Información de Usuarios</h4>
        <hr>
        <ul class="unstyled" style="font-size: 1.1rem; line-height: 1.3rem; margin-bottom: 1rem;">
            <?php foreach ($userBillingLinks as $userName => $userBillingUrl): ?>
            <li>
                <strong><?php echo $userName ?></strong><br>
                <?php if (empty($userBillingStatus[$userName]["nextdate"])): ?>
                <span style="font-weight: bold; color: #007b1d; font-size: 0.9rem;">
                    Paga por primera vez.
                </span>
                <?php endif; ?>
                <a href="<?php echo $userBillingUrl ?>" target="_blank" style="font-weight: bold;font-size: 0.9rem;">
                    Ver detalle de Pagos
                </a>
                <hr>
            </li>
            
            <?php endforeach ?>
        </ul>

        <h4>Acciones</h4>

        <table class="table">
            <?php foreach ($userBillingStatus as $userName => $userBillingStatus): ?>

            <?php if ($userBillingStatus["payment_account"]['accountMovementVerificationType'] === 'automatic_by_bot'): ?>
            <tr>
                <td style="font-weight: bold">
                    <a href="<?php echo site_url("padmin/deposits/list_deposits/?paymentAccount=" . $userBillingStatus["payment_account"]['id']) . "&defaultUser=" . $userName ?>" target="_blank">
                        BUSCAR EL DEPÓSITO
                    </a>
                </td>
                <td class="alert alert-warning" style="font-weight: bold; color: #947237">
                    El depósito correspondiente debería estar AUTOMÁTICAMENTE GENERADO 
                    para la 
                    <span style="white-space: nowrap">
                        cuenta <?php echo $userBillingStatus["payment_account"]["accountAlias"] ?>
                    </span>.
                </td>
            </tr>
            <tr>
                <td style="font-weight: bold">
                    <a href="<?php echo $actionLinks['createDepositPayment'] ?>" target="_blank">
                    EXCEPCIÓN MANUAL
                    </a>
                </td>
                <td>
                    Si el ticket referencia otra cuenta diferente a la mencionada.
                </td>
            </tr>

            <?php else: ?>
            <tr>
                <td style="font-weight: bold">
                    <a href="<?php echo $actionLinks['createDepositPayment'] ?>" target="_blank">
                    REGISTRAR MANUAL
                    </a>
                </td>
                <td class="alert alert-success" style="font-weight: bold;">
                    El ticket debería referenciar la
                    <span style="white-space: nowrap">
                        cuenta <?php echo $userBillingStatus["payment_account"]["accountAlias"] ?>
                    </span>.
                    Si la información es correcta, crea y acredita un nuevo pago.
                </td>
            </tr>
            <?php endif ?>
            <?php endforeach ?>
            <tr>
                <td style="font-weight: bold">
                    <a href="<?php echo $actionLinks['ignoreEventNotification'] ?>" target="_blank">
                    IGNORAR
                    </a>
                </td>
                <td>Sólo se cerrará la notificación, el perfil seguirá publicando normalmente.</td>
            </tr>

            <tr>
                <td style="font-weight: bold">
                    <a href="<?php echo $actionLinks['discardDepositNotification'] ?>" target="_blank">
                    DESCARTAR
                    </a>
                </td>
                <td>Si la información es inválida o falsa y no hay comprobante, se cerrará la notificación y se pedirá volver a subir el ticket.</td>
            </tr>
        </table>
    </div>
    
    
</div>
