<form name="form-new-deposit" id="form-deposit" class="form-inline" method="POST" style="margin-bottom: 0">
    <input type="hidden" name="action" value="add_deposit">
    
    <div class="row-fluid">
        <div class="span8" style="padding-right: 10px;">
            <input type="text" class="" placeholder="A nombre de..." name="deposit_name" value="" style="width: 100%;">
        </div>
    
        <div class="span4">
            <input type="text" class="" placeholder="Concepto" name="deposit_concept" value="<?php echo (isset($default_deposit_concept)) ? $default_deposit_concept : '' ?>" style="width: 93%">
        </div>
    </div>

    <div class="row-fluid" style="margin-top: 10px;">

        <div class="span3">
            <input id="depositdate" type="text" class="input-small" placeholder="Fecha" name="deposit_date" value="<?php echo set_value('deposit_date', date('d/m/Y')); ?>">
        </div>

        <div class="span5">
            <div class="input-prepend">
                <span class="add-on">$</span>
                <input type="text" class="input-small" placeholder="Cantidad" name="deposit_amount" value="<?php echo set_value('deposit_amount', ''); ?>" style="width:83%;">
            </div>
        </div>

        <div class="span4">
            <button type="submit" name="send" value="1" class="btn" style="width:100%;">Añadir</button>
        </div>
    </div>
    
</form>
<?php 
    echo form_error('deposit_date', '<span class="label label-important">Atención</span><span class="help-inline">', '</span>'); 
    echo form_error('deposit_amount', '<span class="label label-important">Atención</span><span class="help-inline">', '</span>'); 
    echo form_error('deposit_concept', '<span class="label label-important">Atención</span><span class="help-inline">', '</span>');
    echo form_error('deposit_name', '<span class="label label-important">Atención</span><span class="help-inline">', '</span>'); 
?>

<script>
    $(document).ready(function()
    {
        $( '[name="deposit_date"]' ).datepicker();
    });
</script>