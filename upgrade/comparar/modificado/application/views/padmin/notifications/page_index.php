<div class="row">

	<?php
		$this->load->view('padmin/menu');
	?>
	
	<div class="span10">
		<?php if (count($notifications) == 0) : ?>
			<div class="row-fluid">
				<div class="span12">
					<h4 class="text-center" style="padding-top: 20px;">No hay notificaciones nuevas.</h4>
				</div>
			</div>
		
		<?php else: ?>
			<?php foreach ($notifications as $notification): ?>
			<div class="alert-notification-row row-fluid">
				<div class="span12">
				<?php
					$notification_view_name = 'padmin/notifications/notification_default';
					$specific_view_name = 'padmin/notifications/notification_' . $notification['description'];

					if (is_file(APPPATH.'views/' . $specific_view_name . EXT))
					{
						$notification_view_name = $specific_view_name;
					}

					$this->load->view($notification_view_name, [
						'notification' => $notification
					]);
				?>
				</div>	

			</div>
			<?php endforeach; ?>
		<?php endif; ?>
	</div>
	
	<script>
		$('.alert-notification .close').click(function(evt)
		{
			var alert_row = $(this).parents('.alert-notification-row');
			
			var url = $(this).attr('href') + '?ajax=true';
			evt.preventDefault();
			
			$.get(url, function(data) {
				if (data === 'success') {
					alert_row.remove();
					
					var el_notification_count = $('.notification-count');
					
					if (el_notification_count) {
						var prev_val = parseInt(el_notification_count.text());
						
						if (prev_val > 0) {
							el_notification_count.html(prev_val - 1);
						}
						
						if (prev_val === 1) {
							el_notification_count.remove();
						}
					}
				}
			});
		});
	</script>
	
</div>

