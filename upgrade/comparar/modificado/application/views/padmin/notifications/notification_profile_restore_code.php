<div class="alert alert-notification alert-notification-blue">
    <a 
        class="close" 
        href="<?php echo site_url('padmin/notifications/mark_as_read/'.$notification['id']) ?>"
    >
        &times;
    </a>

    <strong><?php echo $notification['date']; ?></strong>
    &nbsp;&nbsp;&nbsp;
    ( IP <?php echo $notification['ip']; ?> )
    <hr>
    <img 
        class="flag16" 
        title="<?php echo $notification['country'] ?>" 
        alt="<?php echo $notification['country'] ?>" 
        src="<?php echo site_url('img/flags/'.$notification['country'].'_24.png') ?>"
    >
    &nbsp;
    <?php echo $notification['gender_symbol'] ?>

    &nbsp;Reseteada contraseña a 
    <b><?php echo $notification['meta']['phone'] ?>.</b> 
    usuario: <b><?php echo $notification["user"] ?></b> - 
    contraseña: <b><?php echo $notification['meta']['restore_code'] ?> </b> - 
    <?php echo anchor("padmin/logs/details/".$notification["id"], "Ver detalles"); ?>
</div>