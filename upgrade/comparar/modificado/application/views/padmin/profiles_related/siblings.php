<div class="row">
	<?php
		$this->load->view('padmin/menu');
	?>
	
	<div class="span10">		
		<div class="row-fluid">
			<?php if (count($profiles)): ?>
			
				<table class="table table-bordered table-condensed table-accounts">
					<thead>
						<tr>
							<th class="text-center">Pos.</th>
							<th class="text-center">Cuenta</th>
							<th class="text-center">Grupo</th>
							<th>Usuario</th>
							<th>Nombre</th>
							<th>Tel.</th>
							<th class="text-center">Cuentas <?php echo $remote_site_name; ?></th>
						</tr>
					</thead>
					<tbody>
						<?php foreach ($profiles as $profile): ?>
						<tr>
							<?php
								$colored_class = 'bg-' . strtolower($profile['account_type']);
							
								echo '<td class="text-center '.$colored_class . '">' . $profile['priority'] . '</td>';
									
								echo '<td class="text-center '.$colored_class . '">' . $profile['account_type'] . '</td>';

								if ($profile['group_name']) {
									echo '<td class="text-center bg-grupo">';
								} else {
									echo '<td>';
								}
								
								echo $profile['group_name'] . '</td>';

								$edit_url = site_url('padmin/profiles/edit/'. $profile['id']);
								echo '<td><a target="_blank" href="'. $edit_url . '">' . $profile['user'] . '</a></td>';
								
								echo '<td><a target="_blank" href="'. $edit_url . '">' . $profile['name'] . '</a></td>';
								echo '<td>' . $profile['phone'] . '</td>';
								echo'<td>';
								
								$siblings_text = "";
								
								if ($profile['related_summary']['published']) {
									$siblings_text = $profile['related_summary']['published']. ' EN PORTADA';
								} elseif ($profile['related_summary']['total']) {
									$siblings_text = $profile['related_summary']['unpublished'] . ' OCULTA' . (($profile['related_summary']['unpublished'] > 1)? 'S' : '');
								}
								
								if ($siblings_text) {
									$siblings_details_url = site_url("padmin/profiles_related/siblings_details/".$profile['user']);
									echo '<a href="'.$siblings_details_url.'">' . $siblings_text . '</a>';
								}
								
								echo'</td>';
							?>
						</tr>
						<?php endforeach; ?>
					</tbody>
				</table>
			
			<?php else: ?>
				<h4 class="text-center">
					No se encontraron perfiles relacionados
				</h4>
			<?php endif?>
			
		</div>		
	</div>
	
</div>