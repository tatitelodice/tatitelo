<div class="row">

	<?php
		$this->load->view('padmin/menu');
	?>

	<div class="span10">
		
		<div class="row-fluid" style="margin-bottom: 15px;">
			<div class="span8">
				<h4 class="text-left" style="padding-top: 4px;">
					<b><?php echo $total_pages; ?> pagina<?php echo ($total_pages > 1)? "s" : "" ?></b>
				</h4>
			</div>
			<div class="span4">
				<p class="text-right">
					<a class="btn" style="margin-top: 4px;" href="<?php echo site_url("padmin/contents/create_page");?>">
						<span class="glyphicon glyphicon-plus"></span>&nbsp;Crear página
					</a>
				</p>
			</div>
		</div>	
		
		<?php if (count($pages)): ?>
			<table class="table table-condensed">
				<thead>
					<tr>
						<?php
							$thead = array( 'Titulo', 'Autor', 'Tipo', '<span class="glyphicon glyphicon-comment"></span>', 'Fecha', 'Estado' );
							
							foreach ($thead as $th) {
								echo "<th>" . $th . "</th>";
							}
						?>
					</tr>
				</thead>
				
				<tbody>
					<?php foreach ($pages as $page): ?>
						<tr>
							<td>
								<a href="<?php echo site_url("padmin/contents/edit_page/" . $page["id"]); ?>">
									<b><?php echo $page["title"]; ?></b>
								</a>
							</td>
							
							<td><?php echo $page["author"]; ?></td>
							<td><?php echo $page["type"]; ?></td>
							<td><?php echo $page["commentCount"]; ?></td>
							
							<td>
								<abbr title="<?php echo $page["date"]; ?>">
									<?php echo $page["date_only"]; ?>
								</abbr>
							</td>
							
							<td><?php echo $page["status"]; ?></td>
						</tr>
					<?php endforeach; ?>
				</tbody>
			</table>
		<?php endif; ?>
	</div>
</div>

