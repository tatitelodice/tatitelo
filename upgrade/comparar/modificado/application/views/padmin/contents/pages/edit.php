<?php
	$form_error_start = '<br><span class="label label-important">Atención</span><span class="help-inline">';
	$form_error_end = '</span><br><br>';
?>

<div class="row">

	<?php
		$this->load->view('padmin/menu');
	?>

	<div class="span10">
		<ul id="page-tabs" class="nav nav-tabs">
			<li class="active">
				<a data-target="#page-content" data-toggle="tab">Contenido de página</a>
			</li>
			<li>
				<a data-target="#page-gallery" data-toggle="tab">Galería multimedia</a>
			</li>
			<!--<li>
				<a data-target="#page-preview" data-toggle="tab">Vista previa</a>
			</li>-->
		</ul>
		
		<div class="tab-content">
			<div class="tab-pane active" id="page-content">		
				<form method="POST">
					<?php
						if (!$admin_country) {
							echo form_dropdown('country', $country_options, set_value('country', $page["country"]), ' class="input-large"');
							echo form_error('country', $form_error_start, $form_error_end);
						}
					?>

					<input name="title" style="width: 98%;" type="text" value="<?php echo set_value("title", $page["title"]); ?>" placeholder="Titulo">
					<?php echo form_error('title', $form_error_start, $form_error_end); ?>

					<textarea name="content" style="width: 98%;" rows="25" placeholder="Texto de la nota..."><?php echo set_value("content", $page["content"]); ?></textarea>
					<?php echo form_error('content', $form_error_start, $form_error_end); ?>

					<div class="row-fluid"></div>

					<div class="row-fluid">
						<div class="span12 text-right">
							<input type="submit" class="btn" value="Guardar Cambios">
						</div>
					</div>
				</form>
			</div>

			<!-- GALLERY -->
			<div class="tab-pane" id="page-gallery">
				<?php 
					$this->load->view("padmin/contents/pages/gallery");
				?>
			</div>

			<!-- END OF GALLERY -->

			<div class="tab-pane" id="page-preview">
				preview
			</div
		</div>

	</div>
	
</div>
