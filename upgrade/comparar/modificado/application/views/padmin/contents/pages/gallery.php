	<div class="row-fluid">
		<div class="span4">
			<h4>Directorio / Categoria</h4>
			
			<form class="form-inline">
				<input name="dirpath" type="text" value="">
				<button type="button" style="font-size: 14px; line-height: 25px;" onclick="create_directory()"><span class="glyphicon glyphicon-plus"></span></button>
				<button type="button" style="font-size: 14px; line-height: 25px;" onclick="delete_directory()"><span class="glyphicon glyphicon-trash"></span></button>
			</form>
			
			<div id="directory-selector-box" class="directory-selector-box">
			</div>
		</div>
		
		<div class="span8">
			<h4>Archivos</h4>

			<form id="fileupload" action="<?php echo current_url(); ?>" method="POST" enctype="multipart/form-data">
				<input type="hidden" name="action" value="gallery"> 
				<fieldset>
					<!-- <legend>Tus fotos</legend> -->
					<!-- Redirect browsers with JavaScript disabled to the origin page -->
					<noscript><input type="hidden" name="redirect" value="<?php echo current_url(); ?>"></noscript>


					<!-- The fileupload-buttonbar contains buttons to add/delete files and start/cancel the upload -->
					<div class="fileupload-buttonbar">
						<div class="span5">
							<!-- The fileinput-button span is used to style the file input field as button -->
							<span class="btn fileinput-button" style="font-size: 14px; line-height: 16px;">
								<i class="glyphicon glyphicon-plus"></i>
								<span><b>Subir...</b></span>
								<input type="file" name="files[]" multiple>
							</span>
						</div>
					</div>
					<!-- The loading indicator is shown during file processing -->
					<div class="fileupload-loading"></div>


					<br>
					<!-- The table listing the files available for upload/download -->
					<table role="presentation" class="table table-striped" style="margin-top: 30px;"><tbody class="files" data-toggle="modal-gallery" data-target="#modal-gallery"></tbody></table>
				</fieldset>
			</form>
		</div>
	</div>
		
	<!-- modal-gallery is the modal dialog used for the image gallery -->
	<div id="modal-gallery" class="modal modal-gallery hide fade" data-filter=":odd" tabindex="-1">
		<div class="modal-header">
			<a class="close" data-dismiss="modal">&times;</a>
			<h3 class="modal-title"></h3>
		</div>
		<div class="modal-body"><div class="modal-image"></div></div>
		<div class="modal-footer">
			<a class="btn modal-download" target="_blank">
				<i class="glyphicon glyphicon-download"></i>
				<span>Descargar</span>
			</a>
			<a class="btn btn-success modal-play modal-slideshow" data-slideshow="5000">
				<i class="glyphicon glyphicon-play"></i>
				<span>Pasar galería</span>
			</a>
			<a class="btn btn-info modal-prev">
				<i class="glyphicon glyphicon-arrow-left"></i>
				<span>Anterior</span>
			</a>
			<a class="btn btn-primary modal-next">
				<span>Siguiente</span>
				<i class="glyphicon glyphicon-arrow-right"></i>
			</a>
		</div>
	</div>

	<script id="template-upload" type="text/x-tmpl">
	{% for (var i=0, file; file=o.files[i]; i++) { %}
		<tr class="template-upload fade">
			<td class="preview"><span class="fade"></span></td>
			<td class="name"><span>{%=file.name%}</span></td>
			<td class="size"><span>{%=o.formatFileSize(file.size)%}</span></td>
			{% if (file.error) { %}
				<td class="error" colspan="2"><span class="label label-important">Error</span> {%=file.error%}</td>
			{% } else if (o.files.valid && !i) { %}
				<td>
					<div class="progress progress-success progress-striped active" role="progressbar" aria-valuemin="0" aria-valuemax="100" aria-valuenow="0"><div class="bar" style="width:0%;"></div></div>
				</td>
				<td class="start">{% if (!o.options.autoUpload) { %}
					<button class="btn btn-primary">
						<i class="glyphicon glyphicon-upload"></i>
						<span>Start</span>
					</button>
				{% } %}</td>
			{% } else { %}
				<td colspan="2"></td>
			{% } %}
			<td class="cancel">{% if (!i) { %}
				<button class="btn btn-warning">
					<i class="glyphicon glyphicon-ban-circle"></i>
					<span>Cancel</span>
				</button>
			{% } %}</td>
		</tr>
	{% } %}
	</script>

	<!-- The template to display files available for download -->
	<script id="template-download" type="text/x-tmpl">
	{% for (var i=0, file; file=o.files[i]; i++) { %}
		<tr class="template-download fade">
			{% if (file.error) { %}
				<td></td>
				<td class="name"><span>{%=file.name%}</span></td>
				<td class="size"><span>{%=o.formatFileSize(file.size)%}</span></td>
				<td class="error" colspan="2"><span class="label label-important">Error</span> {%=file.error%}</td>
			{% } else { %}
				<td class="preview">{% if (file.thumbnail_url) { %}
					<a href="{%=file.url%}" title="{%=file.name%}" rel="gallery" download="{%=file.name%}"><img src="{%=file.thumbnail_url%}"></a>
				{% } %}</td>
				<td class="name">
					<a href="{%=file.url%}" title="{%=file.name%}" rel="{%=file.thumbnail_url&&'gallery'%}" download="{%=file.name%}">{%=file.name%}</a>
				</td>
				<td class="size"><span>{%=o.formatFileSize(file.size)%}</span></td>
				<td colspan="2"></td>
			{% } %}
			<td class="delete">
				<button class="btn btn-danger" data-type="{%=file.delete_type%}" data-url="{%= file.delete_url+"&dir="+window["selected_dirpath"] %}"{% if (file.delete_with_credentials) { %} data-xhr-fields='{"withCredentials":true}'{% } %}>
					<i class="glyphicon glyphicon-trash"></i>
					<span>Borrar</span>
				</button>

				<input type="hidden" name="delete" value="1">
			</td>
		</tr>
	{% } %}
	</script>

	<link rel="stylesheet" href="<?php echo base_url('css/bootstrap-image-gallery.min.css'); ?>">
	<!-- CSS to style the file input field as button and adjust the Bootstrap progress bars -->
	<link rel="stylesheet" href="<?php echo base_url('css/jquery.fileupload-ui.css'); ?>">
	<!-- CSS adjustments for browsers with JavaScript disabled -->
	<noscript><link rel="stylesheet" href="<?php echo base_url('css/jquery.fileupload-ui-noscript.css'); ?>"></noscript>

	<script src="<?php echo base_url('js/jquery.ui.widget.js'); ?>"></script>
	<!-- The Templates plugin is included to render the upload/download listings -->
	<script src="<?php echo base_url('js/tmpl.min.js'); ?>"></script>
	<!-- The Load Image plugin is included for the preview images and image resizing functionality -->
	<script src="<?php echo base_url('js/load-image.min.js'); ?>"></script>
	<!-- The Canvas to Blob plugin is included for image resizing functionality -->
	<script src="<?php echo base_url('js/canvas-to-blob.min.js'); ?>"></script>
	<!-- Bootstrap JS and Bootstrap Image Gallery are not required, but included for the demo -->
	<script src="<?php echo base_url('js/bootstrap-image-gallery.min.js'); ?>"></script>
	<!-- The Iframe Transport is required for browsers without support for XHR file uploads -->
	<script src="<?php echo base_url('js/jquery.iframe-transport.js'); ?>"></script>
	<!-- The basic File Upload plugin -->
	<script src="<?php echo base_url('js/jquery.fileupload.js'); ?>"></script>
	<!-- The File Upload file processing plugin -->
	<script src="<?php echo base_url('js/jquery.fileupload-fp.js'); ?>"></script>
	<!-- The File Upload user interface plugin -->
	<script src="<?php echo base_url('js/jquery.fileupload-ui.js'); ?>"></script>

	
	<!-- The XDomainRequest Transport is included for cross-domain file deletion for IE8+ -->
	<!--[if gte IE 8]><script src="<?php echo base_url('js/jquery.xdr-transport.js'); ?>"></script><![endif]-->
	
	
	<script>
		
		function load_directories_table(dir="default") {
			$.getJSON("/padmin/contents/get_upload_dirs", function(data) {
				rows = [];
				
				$.each( data, function( key, val ) {
					rows.push( '<tr><td class="dir-row" data-path="' + val + '">' + val + "</td></tr>" );
				});
				
				html = '<table class="table table-striped table-condensed">' + rows.join("") + "</table>";
				
				$("#directory-selector-box").html(html);
				
				console.log(dir);
				
				$('td[data-path='+ dir + ']').addClass("dir-row-active");
			});
		}
		
		function load_directory(dir="") {
			'use strict';

			// Initialize the jQuery File Upload widget:
			$('#fileupload').fileupload({
				// Uncomment the following to send cross-domain cookies:
				//xhrFields: {withCredentials: true},
				url: '/padmin/contents/file_upload?dir=' + dir,
				autoUpload: true
				//previewCrop: true
			});

			// Load existing files:
			$.ajax({
				// Uncomment the following to send cross-domain cookies:
				//xhrFields: {withCredentials: true},
				url: $('#fileupload').fileupload('option', 'url'),
				dataType: 'json',
				context: $('#fileupload')[0]
			}).done(function (result) {
				if (result && result.length) {
					$(this).fileupload('option', 'done')
						.call(this, null, {result: result});
				}
			});
		}
		
		
		function bind_dir_rows() {
			$("#directory-selector-box").on('click', ".dir-row", function(e) {
				
				$(".dir-row").each(function(i, val) {
					$(this).removeClass("dir-row-active");
				});
					
				$(this).addClass("dir-row-active");
				
				$("table tbody.files").empty();
				
				var new_dir_path = $(this).attr("data-path");
				window["selected_dirpath"] = new_dir_path;
				load_directory(new_dir_path);
				
				$("input[name=dirpath]").val(new_dir_path);
			});
		}
 		
		function create_directory() {
			var dirpath = $("input[name=dirpath]");
			
			if (dirpath !== "") {
				$.getJSON("/padmin/contents/create_upload_dir?dir=" + dirpath.val(), function(data) {
					if (data === true) {
						$("table tbody.files").empty();

						dir_path = dirpath.val();

						window["selected_dirpath"] = dir_path;

						load_directories_table(dir_path);
						load_directory(dir_path);
						dirpath.val("");
					}
				});
			}
		}
		
		function delete_directory() {
			var dirpath = $("input[name=dirpath]");
			
			if (dirpath !== "") {
				$.getJSON("/padmin/contents/delete_upload_dir?dir=" + dirpath.val(), function(data) {
					if (data === true) {
						$("table tbody.files").empty();

						dir_path = "default";

						window["selected_dirpath"] = dir_path;

						load_directories_table(dir_path);
						load_directory(dir_path);
						dirpath.val("");
					} else {
						var wmess = "No se ha podido borrar el directorio '" + dirpath.val();
						wmess += "' asegurate de que este vacio!.";
					
						window.alert(wmess);
					}
				});
			}			
		}
		
		
		$(function() {
			window["selected_dirpath"] = "default";
			load_directories_table();
			load_directory();
			bind_dir_rows();
		});		
	</script>