
<div class="container">
	
	<div class="row margin-top-30">
		<div class="span10 offset1 text-center">
			<img alt="Logo Pumbate Escorts" title="Visita Pumbate Escorts" src="<?php echo $path; ?>/img/logo-complete.png">
		</div>
	</div>
	
	<div class="row row margin-top-10">
		<div class="span10 offset1">
			<div class="well main-well text-center">
				<?php
					$nice_genders = array(
						'muj' => 'otras chicas',
						'hom' => 'otros chicos',
						'tra' => 'otras travestis y trans',
						'duo' => 'otros duos'
					);
					
					$subg = substr($gender, 0, 3);
				?>
				
				<p>Oooops! Parece ser que <?php echo $name?> no está disponible en el sitio temporalmente.</p>
				<p>Puedes encontrar servicios de sexo y acompañantes con <?php echo $nice_genders[$subg] ?> en <a href="<?php echo $url_country; ?>">Pumbate.com</a></p>
				<br>
				<a class="btn btn-main btn-large" title="Ir a visitar la galería principal del sitio" href="<?php echo $url_gallery; ?>">
					<i class="glyphicon glyphicon-chevron-right"></i> Ir a ver <?php echo preg_replace('!.*\s!', '', $nice_genders[$subg]); ?> disponibles en Pumbate.com
				</a>
				<br>
			</div>
		</div>
	</div>
	
	
	<div class="row">
		<div class="span10 offset1">
			<div class="well">
				<h1><?php echo $title; ?></h1>
				<h2><?php echo $subtitle; ?></h2>
				<h3><?php echo $subtitle2; ?></h3>
			
				<p><?php echo $about; ?></p>
				
				<p>
				<?php if ($age > 17): ?>
					<i class="glyphicon glyphicon-user"></i> Edad: <?php echo $age; ?> años.
				<?php endif;?>
					<i class="glyphicon glyphicon-map-marker"></i> Ubicación: <?php echo $location; ?>
				<?php if ($place): ?>
					<i class="glyphicon glyphicon-map-marker"></i>&nbsp;Lugar de atención: <?php echo $place; ?>.
				<?php endif;?>
				</p>
				
				<p><?php echo $services; ?></p>
			</div>
		</div>	
	</div> 	
	
	<div class="row margin-bottom-20">
		<div class="span10 offset1">
			<?php if ( isset($footer_big_text) && $footer_big_text ): ?>
			<h2 class="outer-header"><?php echo $footer_big_text ?></h2>
			<?php endif; ?>
		</div>
	</div>
	
</div> <!-- /container -->
