
		<div class="container header-container <?php if (!$promo) echo ' hidden';?>">
			
			<div class="row">
				<div class="span12 countryinfo">
					Usted está viendo Pumbate Escorts <?php echo $country; ?> <img src="<?php echo base_url('img/flags/' . $country_iso . '_24.png'); ?>"> (<a href="<?php echo site_url('welcome/change'); ?>"> Ver Pumbate Escorts en otros países </a>)
				</div>
			</div>
			
			<div class="row">
				<div class="span5">
					<div id="logo-box">
						<h1 class="logo">
							<a title="Pumbate Escorts <?php echo $country; ?>" href="<?php echo site_url(); ?>"><img title="Pumbate Escorts <?php echo $country; ?>" alt="Pumbate Escorts <?php echo $country; ?>" src="<?php echo base_url('img/logo-complete2.png'); ?>"></a>
						</h1>
						<h2 class="logo">
							Tu guía de <strong>sexo y placer VIP</strong> en <strong><u><?php echo $country; ?></u></strong>.
						</h2>
					</div>
				</div>
				<div id="main-nav" class="span7">
					<h3>
						<a id="gallery-top-link" onClick="moveTo('gallery')" href="#gallery"><strong>Galería de Escorts</strong></a>
						&nbsp;
						<a id="advertise-top-link" ui-effect="scroll" href="#advertise"><strong>Anunciate en Pumbate!</strong></a>
						&nbsp;
						<a id="mypage-top-link" <?php if ($profile_help) echo 'class="yellow-highlight-box" ';?>href="<?php echo site_url('mipagina'); ?>"><strong>Mi Página</strong></a>
					</h3>
				</div>
			</div>
		</div>
		
		<?php
			if ($profile_help) {
				$this->load->view('site/newprofile/remember');
			} else {
				switch ($country_iso) {
					case 'co':
					case 'uy':
					case 'ar':
						$this->load->view('site/news/' . $country_iso);
						break;
					default:
						$this->load->view('site/comingsoon');
						break;
				}				
			}
		?>
		
		<div id="gallery" class="container white-container gallery-container promopop <?php if (!$promo) echo ' hidden';?>">
			<div class="row">
				<div class="span12 gallery-header">
					<img alt="Galería de Escorts" title="A continuación nuestra galería de chicas, chicos y trans mas hot" src="<?php echo base_url('img/galeria.png'); ?>">
				</div>
			</div>
			
			<div class="row-fluid">
				<div class="gender-select-row">
					<?php
						$seo_url = 'sexo/' . strtolower($country); 
						
						$couples = false;
						$tabspan = '4';
						
						if ($country_iso == 'uy') {
							$couples = true;
							$tabspan = '3';
						}
						
					?>
					<div class="span<?php echo $tabspan ?>">
						<a class="gallery-tab <?php echo ($escorts_gender != 'mujer')? 'gallery-tab-disabled' : ''; ?>" href="<?php echo site_url($seo_url . '/mujeres'); ?>#gallery-start">
							 <?php echo ($escorts_gender != 'mujer')? 'Ver' : ''; ?> Mujeres
						</a>
					</div>
					
					<?php if ($couples): ?>
						<div class="span<?php echo $tabspan ?>">
							<a class="gallery-tab gallery-tab-men <?php echo ($escorts_gender != 'duo')? 'gallery-tab-disabled' : ''; ?>" href="<?php echo site_url($seo_url . '/duos-y-trios'); ?>#gallery-start">
								 <?php echo ($escorts_gender != 'duo')? 'Ver' : ''; ?> Dúos
							</a>
						</div>
					<?php endif; ?>
					
					<div class="span<?php echo $tabspan ?>">
						<a class="gallery-tab gallery-tab-trans <?php echo ($escorts_gender != 'trans')? 'gallery-tab-disabled' : ''; ?>" href="<?php echo site_url($seo_url . '/travestis-y-trans'); ?>#gallery-start">
							 <?php echo ($escorts_gender != 'trans')? 'Ver' : ''; ?> Trans
						</a>
					</div>
					<div class="span<?php echo $tabspan ?>">
						<a class="gallery-tab gallery-tab-men <?php echo ($escorts_gender != 'hombre')? 'gallery-tab-disabled' : ''; ?>" href="<?php echo site_url($seo_url . '/hombres'); ?>#gallery-start">
							 <?php echo ($escorts_gender != 'hombre')? 'Ver' : ''; ?> Hombres
						</a>
					</div>
				</div>
			</div>
				
			<div class="row">
				<div class="span12">
					<div class="gallery-wrap">
						
						<div class="row-fluid row-gallery-form">
							<div class="span8">
								<form class="hide">
									<input type="hidden" name="country_iso" value="<?php echo $country_iso ?>">
									<input type="hidden" name="escorts_gender" value="<?php echo $escorts_gender ?>">
								</form>

								<form class="form-inline form-location-filter" method="GET" action="<?php echo current_url('') . '#gallery'; ?>">
									
									<input class="input-large" type="text" name="keywords" value="<?php echo $keywords; ?>" placeholder="Palabras clave...">
									
									<?php
										echo form_dropdown('region', $regions, $region, ' id="region" class="input-large"');

										echo ' ';

										if ($region && count($cities)) {
											echo form_dropdown('city', $cities, $city, ' id="city" class="input-large"');
										} else {
											echo form_dropdown('city', $cities, $city, ' id="city" class="input-large hide"');
										}
									?>

									<button type="submit" class="btn btn-rose">Buscar</button>
								</form>
							</div>
							<div id="gallery-start" class="span4 pictures-notes">
								Dudas de las fotos? Mirá los sellos!
								<a class="violet" title="Saber más acerca de política de Fotos y Sellos" target="_blank" href="https://www.pumbate.com/blog/nuevo-sistema-de-fotografias/">Más info aquí&nbsp;&raquo;</a><br>
								<img class="simple-tooltip" title="Por más que las fotos sean reales, nuestro equipo no conoce personalmente al anunciante para asegurarlo." alt="Sello Fotos Subidas por Anunciante" src="<?php echo base_url('assets/themes/common/veripics/notverifiedclub.png'); ?>">									
								<img class="simple-tooltip" title="Nuestro equipo ha visitado al anunciante y certifica que las fotos expuestas son 100% reales." alt="Sello Fotos Verificadas por Pumbate.com" src="<?php echo base_url('assets/themes/common/veripics/verifiedclub.png'); ?>">
							</div>
						</div>

						<?php 
							if (count($top_escorts)) {
								$this->load->view('site/top_escorts_gallery'); 
							}
							
							$escorts_gallery_data = array();
							$escorts_gallery_data['escorts'] = $escorts;
							$escorts_gallery_data['blocks_by_row'] = 8;

							$this->load->view('site/escorts_gallery', $escorts_gallery_data); 
						?>

						<p class="lead text-center" style="font-size: 1.3rem;">
							<a href="<?php echo $archive_url ?>">
								Ver Archivo
							</a>
						</p>

						<?php if (false && $country_iso == 'uy' && $escorts_gender == "mujer"): ?>
							<div class="row-fluid">
								<h3 class="gallery-normal-title">Buscás más chicas? ¡Probá en CHICASUY.COM!
									<a class="btn btn-large btn-main" href="https://www.chicasuy.com">Ir a ChicasUy.com &gt;&gt;&gt;</a>
								</h3>
							</div>
						<?php endif; ?>	
					</div>
				</div>	
			</div>
		</div>
		
		<div class="container rose-container advertise-container">
			<div class="row">
				<div class="span12 advertise-header">
					<img alt="Anunciate en Pumbate.com" title="Registrarte y anuncia con nosotros!" src="<?php echo base_url('img/anunciate.png'); ?>">
				</div>
			</div>
			<div class="row">
				<?php
					$this->load->view('site/newprofile/welcome');
				?>
			</div>
		</div>

		<?php if ($promo) : ?>

		<!--
		<div class="navbar navbar-panel navbar-tryit hidden-phone">
			<div class="navbar-inner">
				<ul class="nav">
					<li>
						<a id="btn-try" class="btn btn-main btn-large input-xlarge" ui-effect="scroll" href="#advertise">NO ESPERES MÁS! HACE CLICK ACÁ Y EMPEZÁ A TENER TU PROPIO SITIO EN PUMBATE.COM!</a>
					</li>
				</ul>
			</div>
		</div>
		-->
		
		<h5 class="bterms">
			<strong>Chicas escort, mujeres acompañantes, servicios sexuales y eróticos</strong>
		</h5>

		<h6 class="bterms">
			Somos tu referencia y guía definitiva de <strong>SEXO</strong>, <strong>erotismo</strong>, <strong>posiciones sexuales</strong>. En <u><strong>Uruguay</strong></u> y aún más  en <u><strong>Colombia</strong></u>, <strong>Prostitutas</strong>,<strong>mujeres</strong>, <strong>traviesas</strong>, <strong>atención</strong> a <strong>caballeros</strong> en <strong>apartamento privado</strong>, <strong>hotel</strong> ó <strong>domicilio</strong>. Travestis, trans, dotadas.
			jueguetes sexuales, dominación, sado. <strong>Sexo anal</strong>, lluvia dorada. Tacos, cuero. Todos lo que buscás.
		</h6>
		
		<?php endif; ?>

		<script src="<?php echo base_url('js/home.js'); ?>?v=20180708"></script>
		
