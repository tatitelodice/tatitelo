<div id="advertise" class="container margin-top-50 color-white">
	
	<div class="glass-container-dark padding-30 round-10">
		<div class="row-fluid">
			<div class="span6">
				<h2 class="text-center">
					Ya tienes una página en Pumbate
				</h2>
				
				<div class="row visible-desktop margin-top-30"></div>
				
				<h4 class="text-justify">
					Parece ser que ya tienes una página en Pumbate por lo que no
					podemos permitirte crear más páginas. No todas las páginas a prueba
					aparecen en portada ya que están sujetas a la aprobación de nuestro
					equipo en temas de veracidad, profesionalidad y evitar anunciantes duplicados.
				</h4>

				<br>
				<div class="row visible-desktop margin-top-30"></div>
				
				<p class="text-center">
					<a class="btn btn-large btn-main" href="<?php echo site_url(); ?>">Volver a Portada de Pumbate.com</a>
				</p>
			</div>

			<div class="span6">
				<h3 class="text-center">No es tu caso alguno de los anteriores?</h3>
				
				<h4 class="text-center">Contáctanos</h4>
				
				<?php if ($pumbate_phone) : ?>
				<div class="alert alert-panel-success promo-phone">
					<a class="pull-left" target="_blank" href="https://www.facebook.com/pumbate.rrpp"><img style="margin-top: 0px;" src="<?php echo base_url('img/phone.png');?>"></a>
					<h4>	
						Llamando al FONO-PUMBATE:<br><?php echo safe_mailto('rrpp@pumbate.com', $pumbate_phone); ?>
					</h4> 
				</div>
				<?php endif; ?>

				<div class="alert alert-panel-success promo-facebook">
					<a class="pull-left" target="_blank" href="https://www.facebook.com/pumbate.rrpp"><img style="margin-top: 0px;" src="<?php echo base_url('img/facebook.png');?>"></a>
					<h4>	
						Agregando nuestro Facebook, hablanos al chat y resolveremos tus dudas:<br><a target="_blank" href="https://www.facebook.com/pumbate.rrpp">facebook.com/pumbate.rrpp</a>
					</h4> 
				</div>	
			</div>
		</div>
	</div>
</div>
