<div id="contact" class="span5">
	<?php if (false && $pumbate_phone) : ?>
	<div class="alert alert-panel-text alert-panel-text-picture promo-contact">
		<a class="alert-panel-picture-icon" target="_blank" href="https://www.facebook.com/pumbate.rrpp"><img src="<?php echo base_url('img/phone.png');?>"></a>
		<p class="alert-panel-text alert-panel-text-picture margin-top-10">
			<strong>Anunciate GRATIS</strong> ó DESTACATE en un par de minutos. Si tenés alguna duda, contactanos por SMS al: <a title="Llama al Fono Pumbate" href="tel:<?php echo preg_replace('!\D!', '', $pumbate_phone); ?>"><?php echo $pumbate_phone; ?></a>
		</p> 
	</div>
	<?php endif; ?>

<!--
	<div class="alert alert-panel-text alert-panel-text-picture promo-contact">
		<a class="alert-panel-picture-icon" target="_blank" href="https://www.facebook.com/pumbate.rrpp"><img src="<?php echo base_url('img/facebook.png');?>"></a>
		<p class="alert-panel-text alert-panel-text-picture">
			Anunciate GRATIS, agregate nuestro Facebook, hablanos al chat y tendrás tu página al instante:<br><a target="_blank" href="https://www.facebook.com/pumbate.rrpp">facebook.com/pumbate.rrpp</a>
		</p> 
	</div>	

	<div class="alert alert-panel-text alert-panel-text-picture promo-contact">
		<a class="alert-panel-picture-icon" target="_blank" href=""><img src="<?php echo base_url('img/mail.png');?>"></a>
		<p class="alert-panel-text alert-panel-text-picture">
			Siéntete libre también de escribirnos ante cualquier duda o sugerencia a nuestro e-mail:<br><?php echo safe_mailto($pumbate_mail, $pumbate_mail); ?>
		</p> 
	</div>
-->

	<img class="register-promo hidden-phone" alt="Afiche promocional, crea tu propia página, publica tus fotos, ten tu propio .com" title="Ofrece tus servicios eróticos y sexuales en Pumbate.com" src="<?php echo base_url('img/register/neutral.jpg'); ?>">

</div>

<div id="advertise" class="span7">
	<?php if ($maintenance) : ?>
		<p class="big margin-top-30">Por qué no <strong>probarlo GRATIS</strong>?<br> Con Pumbate tendrás una página <strong>TOTALMENTE GRATIS</strong> de prueba sólo para ti.</p>

		<div class="alert alert-panel-warning text-justify">
			Estamos haciendo mantenimiento al sitio para mejorar su funcionamiento por lo
			que debemos pedirte que vuelvas en unas horas para encontrar en este mismo
			lugar el formulario para probar el servicio y crear tu página en minutos!
		</div>

	<?php elseif ($profile_help || $profile_is_garbage): ?>
		<div style="padding: 0px 20px 20px 20px">
			<h2 class="text-center">Completa tu página!</h2>
			
			<p class="big margin-top-20">
				Ya tienes un perfil pero está vacío! Por favor, entra a la sección "Mi página" arriba del todo para terminar de crear tu página
				y subir tus fotos!
			</p>
			
			<span class="visible-desktop round-10" style="background: #27070e;">
				<img id="ubicacion-mipagina" class="round-10" onClick="show_mypage_location()" style="cursor: pointer;" src="<?php echo base_url('img/info/mipagina.png')?>">
			</span>

			<span class="hidden-desktop" style="background: #27070e;">
				<img id="ubicacion-mipagina-mobile" class="round-10" onClick="show_mypage_location()" style="cursor: pointer;" src="<?php echo base_url('img/info/mipagina_mobile.png')?>">
			</span>
			
			<p class="text-center margin-top-30">
				<button onClick="show_mypage_location()" id="show-me-mypage" class="btn btn-large btn-rose">Muéstrame donde está "Mi Página"</button>
			</p>
			
			<?php if (($profile_complete && !$profile_is_garbage) || $admin_login): ?>
			<p class="text-center">
				<a class="btn btn-large btn-main" href="<?php echo site_url('promomain/clean_help'); ?>">Ya he terminado, quiero crear otra cuenta</a>
			</p>
			<?php else: ?>
			<p class="medium text-center margin-top-20">
				Si quieres crear el perfil de alguien más, no podrás hacerlo hasta que completes y subas fotos al que acabas de registrar.
			</p>	
			<?php endif; ?>
			
		</div>
		
		<script>
			function show_mypage_location() {
				var mypage = $('#mypage-top-link');
				var offset = mypage.offset();
				
				$.scrollTo( offset.top - 100, 600, function() {
					$('#logo-box').animate({ opacity: 0.4, duration: 3000});
					$('#gallery-top-link').animate({ opacity: 0.8, duration: 3000,});
					$('#advertise-top-link').animate({ opacity: 0.8, duration: 3000});
					$('.front-container').animate({ opacity: 0.4, duration: 3000});
				});
			}
		</script>
		
	<?php else:
		$this->load->view('site/newprofile/form_advertise');
	endif; ?>

</div>
