<?php
	$remember_texts = array();
	$remember_texts["user"] = ($profile_user)? $profile_user : "(tu usuario)";
?>

<div id="remember" class="container rose-container advertise-container">	
	<div class="row">
		<div class="span12">
			<div class="promo-result" style="padding: 10px;">
				<?php if ($profile_is_garbage): ?>
					<h3 class="text-center">
						¡REVISAREMOS TU PERFIL PRONTO!
					</h3>
								
					<h4 class="text-center">
						Es importante que sepas que NO APROBAMOS TODOS LOS PERFILES que se registran.
						Si en el correr de la semana tu perfil no aparece publicado es porque no lo
						hemos aprobado.
						<br><br>
						Para contactar o contratar los servicios de las chicas NO ES NECESARIO TENER UN PERFIL,
						puedes hacer click en cada chica más abajo y ver su número de contacto.
						<br><br>
						¡Gracias! El equipo de Pumbate.com!
					</h4>
					
				
				<?php elseif ($profile_complete): ?>
					<h2 class="text-center">
	                    Tu página está completa!
	                </h2>

	                <p class="lead text-center">
	                    <strong>
	                        Recuerda que puedes editarla o revisar el estado de publicación todas las veces que quieras accediendo a <a target="_self" href="#mipagina" onclick="show_mypage_location()" class=" nobreak" style="color: yellow;">"MI PÁGINA"</a>
	                    </strong>
	                </p>
				
				<?php else: ?>
					<h2 class="text-center">
                    	Tu página ha sido creada con éxito!
                	</h2>

	                <p class="lead text-center">
	                    <strong>
	                        Para verla publicada, sube más fotos y ACTIVALA entrando a la sección <a target="_self" href="#mipagina" onclick="show_mypage_location()" class=" nobreak" style="color: yellow;">"MI PÁGINA"</a> ubicada arriba del todo.
	                    </strong>
	                </p>
				<?php endif; ?>
			</div>
		</div>
	</div>
</div>