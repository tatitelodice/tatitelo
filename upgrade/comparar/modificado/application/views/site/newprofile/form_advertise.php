<?php
	if (!isset($gender_options))
	{
		$this->load->library('gender');
		$form_register_gender_country = (isset($country_iso)) ? $country_iso : false;
		$gender_options = array_merge(
			array( '' => 'elige una opción')
		,	$this->gender->get_available_genders($form_register_gender_country)
		);
	}
?>

<form class="form-horizontal form-welcome" method="post" enctype="multipart/form-data" action="<?php echo site_url('register'); ?>">
	<p class="medium">
		Registrarte no tiene costo alguno y crea automáticamente un perfil para mostrar tus servicios de <strong>sexo</strong> y <strong>compañía</strong>. 
	</p>
	
	<p class="medium">
		El usuario y contraseña que registres te servirá para editar tu página o subir fotos todas las veces que quieras.
	</p>
	<br>
	<?php
		$error_start = '<br><span class="help-inline"><i class="glyphicon glyphicon-warning-sign"></i>&nbsp;Hay un error aquí!<br>';
	?>

	<div class="control-group">
		<label class="control-label" for="user">Elige un usuario</label>
		<div class="controls">
			<!--<div class="input-prepend">
				<span class="add-on">pumbate.com/</span>-->
				<input 
					type="text" class="input-large" id="user" name="user" placeholder="Ej: chicasexy2016" 
					value="<?php echo set_value('user'); ?>"
					autocomplete="off" 
					autocorrect="off" 
					autocapitalize="off" 
					spellcheck="false">
			<!--</div>-->
			<?php 
				if (form_error('user')) {
					echo form_error('user', $error_start, '</span>');
				} else {
					echo '<span class="help-inline">(sólo letras, números ó guiones)</span>';
				}
			?>

		</div>			
	</div>						

	<div class="control-group">
		<label class="control-label" for="password1">Contraseña para hacer cambios</label>
		<div class="controls">
			<input 
				type="text" class="input-large" id="password1" name="password1" 
				value="<?php echo set_value('password1'); ?>"    
				autocomplete="off" 
				autocorrect="off" 
				autocapitalize="off" 
				spellcheck="false">
			<?php echo form_error('password1', $error_start, '</span>'); ?>
		</div>			
	</div>

	<div class="control-group">
		<label class="control-label" for="password2">Repite tu contraseña</label>
		<div class="controls">
			<input 
				type="text" class="input-large" id="password2" name="password2" 
				value="<?php echo (form_error('password1'))? '' : set_value('password2'); ?>"
				autocomplete="off" 
				autocorrect="off" 
				autocapitalize="off" 
				spellcheck="false">
			<?php if (!form_error('password1')) { echo form_error('password2', $error_start, '</span>'); } ?>
		</div>			
	</div>

	<div class="control-group">
		<label class="control-label" for="email">E-mail</label>
		<div class="controls">
			<input 
				type="text" class="input-xlarge" id="email" name="email" placeholder="Ej: mailactual@ejemplo.com" 
				value="<?php echo set_value('email'); ?>"
				autocomplete="off" 
				autocorrect="off" 
				autocapitalize="off" 
				spellcheck="false">
				
			<?php echo form_error('email', $error_start, '</span>'); ?>
		</div>			
	</div>

	<div class="control-group">
		<label class="control-label" for="phone">Teléfono ó celular</label>
		<div class="controls">
			<input type="text" class="input-large" id="phone" name="phone" placeholder="Ej: 09x XXX XXX" value="<?php echo set_value('phone'); ?>">
			<?php echo form_error('phone', $error_start, '</span><br>'); ?>
		</div>			
	</div>

	<div class="control-group">
		<label class="control-label" for="gender">Soy</label>
		<div class="controls">
			<?php
				echo form_dropdown('gender', $gender_options, set_value('gender'), ' id="gender" class="input-large"');
				echo form_error('gender', $error_start, '</span><br>');
			?>
		</div>			
	</div>	
	
	<div id="clients_type_block" class="control-group hide">
		<label class="control-label" for="clients_type">Atiendo</label>
		<div class="controls">
			<?php
				$clients_type_options = array(
					'hombres' => 'Sólo hombres',
					'mujeres' => 'Sólo mujeres',
					'mujeres2' => 'Sólo mujeres y parejas',
					'todo'    => 'Hombres, mujeres y parejas',
				);

				echo form_dropdown('clients_type', $clients_type_options, set_value('clients_type'), ' id="clients_type" class="input-large"');
				echo form_error('clients_type', $error_start, '</span><br>');
			?>
			<p id="woman-truth" class="help-inline hide">
				NOTA: El escort masculino trabaja casi exclusivamente con hombres, LAS MUJERES NO TE LLAMARAN.
				<br><br>
				NO PIERDAS TU TIEMPO, NO TE REGISTRES.
				
			</p>
		</div>
	</div>
	
	<div class="control-group">
		<label class="control-label" for="about">Cuéntanos acerca de ti y tus servicios</label>
		<div class="controls">
			<textarea rows="6" class="input-xlarge" id="about" name="about" placeholder="Escribe aquí acerca de tu aspecto físico, días, horarios, si tienes lugar propio, que público atiendes, tarifas. Cuanto más completes, más llamados podrás tener!!!"><?php 
				echo set_value('about'); 
			?></textarea>
			<?php echo form_error('about', $error_start, '</span><br>'); ?>
		</div>			
	</div>
	
	<?php if (empty($picture_uploaded)): ?>
	<div class="control-group">
		<label class="control-label" for="picture">Sube una foto</label>
		<div class="controls">
			<input id="advertise-pic" type="file" name="picture" value="<?php set_value('picture'); ?>">
			<span class="help-inline">
				<?php echo (form_error('picture'))? '<i class="glyphicon glyphicon-warning-sign"></i>&nbsp;Hay un error aquí!<br>' : ''; ?>
				Debes tener al menos una foto para publicar con nosotros.
			</span>
		</div>
	</div>
	<?php endif; ?>

	<?php if ($admin_login): ?>
	<div class="control-group">
		<label class="control-label" for="comments">Comentarios de uso interno</label>
		<div class="controls">
			<textarea rows="3" class="input-xlarge" id="comments" name="comments" placeholder="Ej: Lu de adoos, contraseña tal."><?php echo set_value('comments'); ?></textarea>
			<?php echo form_error('texto', '<br><div class="help-block">' . $error_start, '</span></div>'); ?>
		</div>			
	</div>
	<?php else: ?>
	<div class="control-group">
		<label class="control-label" for="comments">Condiciones</label>
		<div class="controls">
			<input type="checkbox" checked="checked">
			Soy mayor de 18 años y entiendo que Pumbate sólo es un sitio de anuncios.
		</div>
	</div>
	<?php endif; ?>
	
	<div class="control-group text-center margin-top-30">
		<button name="send_form" value="1" type="submit" class="btn btn-large btn-main">Empezar a crear mi página</button>
	</div>
</form>
