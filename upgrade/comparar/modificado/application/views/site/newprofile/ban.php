<div id="advertise" class="container margin-top-50 color-white">	
	<div class="glass-container-dark padding-30 round-10">
		<div class="row-fluid">
			<div class="span6">
				<h2 class="text-center">
					Oops! 
				</h2>
				<h3 class="text-center">
					Parece ser que ya has creado dos páginas personales en
					pumbate.com. No te preocupes! Ya verificaremos alguna
					de tus cuentas ya creadas para que aparezcas en portada
					en las próximas 24 horas!<br><br>

					<a class="btn btn-large btn-main" href="<?php echo site_url(); ?>">Volver a Portada de Pumbate.com</a>
				</h3>
			</div>
			<div class="span6">
				
				<div class="row visible-desktop margin-top-0"></div>
				
				<h3 class="text-center">
					Ante la duda, contáctate con nosotros:
				</h3>
				<?php if ($pumbate_phone) : ?>
				<div class="alert alert-panel-success promo-phone">
					<a class="pull-left" target="_blank" href="https://www.facebook.com/pumbate.rrpp"><img style="margin-top: 0px;" src="<?php echo base_url('img/phone.png');?>"></a>
					<h4>	
						Llamando al FONO-PUMBATE:<br><?php echo safe_mailto('rrpp@pumbate.com', $pumbate_phone); ?>
					</h4> 
				</div>
				<?php endif; ?>

				<div class="alert alert-panel-success promo-facebook">
					<a class="pull-left" target="_blank" href="https://www.facebook.com/pumbate.rrpp"><img style="margin-top: 0px;" src="<?php echo base_url('img/facebook.png');?>"></a>
					<h4>	
						Agregando nuestro Facebook, hablanos al chat y tendrás tu página al instante:<br><a target="_blank" href="https://www.facebook.com/pumbate.rrpp">facebook.com/pumbate.rrpp</a>
					</h4> 
				</div>	
			</div>
		</div>
	</div>
</div>