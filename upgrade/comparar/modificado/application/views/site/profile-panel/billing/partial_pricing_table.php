<table class="table table-condensed table-visits" style="background: white;">
    <tbody>
    <?php if (count($pricing) === 1): ?>
        <tr>
            <td style="font-weight: bold; text-align: center;">
                Publicación <?php 
                    echo $pricing[0]['text'] . ' $' . $pricing[0]['price'];
                ?>
            </td>
        </tr>
    <?php else: ?>
        <?php foreach ($pricing as $pricing_item): ?>
            <tr>
                <td>$<?php echo $pricing_item['price']; ?></td>
                <td><?php echo $pricing_item['text']; ?></td>  
            </tr>
        <?php endforeach; ?>
    <?php endif; ?>
    </tbody>
</table>