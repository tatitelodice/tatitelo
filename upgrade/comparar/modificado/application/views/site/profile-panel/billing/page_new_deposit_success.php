<div class="container">	
	<div class="row">
		<div class="span6 offset3 section-panel section-panel-light">
            <h3 class="section-panel-title">
                Recibimos tu aviso de pago correctamente
            </h3>

            <p style="margin-top: 1.5rem;">
                Pronto verificaremos tu pago y si está todo correcto, aparecéras en portada en el día. Si pasa mas de un día y no apareces, por favor contáctanos.
            </p>

            <p class="text-center" style="margin-top: 1.5rem;">
                <a 
                    class="btn btn-main"
                    href="<?php echo site_url('/mipagina')?>"
                >
                    Volver al Panel
                </a>
            </p>
		</div>
	</div>
</div>