<script type="text-template" id="template_message_extend_publication">
<?php
    $this->load->view('site/profile-panel/messages/message_extend_publication');
?>
</script>

<script>

$(document).ready(function()
{
    var user = <?php echo json_encode($user); ?>;

    $.get('/profile-panel/billing/get_status/' + user).then(function(data)
    {
        if (data.notify_due_date)
        {
            var dueDateMessageHtml = $("#template_message_extend_publication").html();

            var $dueDateMessage = $(dueDateMessageHtml);

            if (data.next_payment_date && data.days_remaining > 0)
            {
                var daysCountText = data.days_remaining + ' dias';
                daysCountText = parseInt(data.days_remaining) === 1 ? 'día' : daysCountText;

                $dueDateMessage.find('[data-ui-text="due-date"]').text(
                    'acabará en ' + daysCountText + ' el ' + data.next_payment_date
                );
            }
            console.log(data.next_payment_date);
            data.days_remaining
            //

            $('[data-ui-role="message-inbox"]').append($dueDateMessage);
        }
    });
});

</script>