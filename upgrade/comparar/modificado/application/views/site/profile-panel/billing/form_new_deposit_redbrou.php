<?php 
    $form_error_start = '<br><span class="help-inline">';
    $form_error_end = '</span><br><br>';
?>

<form
    class="form-horizontal" 
    method="post" 
    enctype="multipart/form-data" 
    action="<?php echo current_url(); ?>">

    <p class="text-center" style="margin: 1.5rem 0">
        Foto del ticket <b>recibido en caja</b>
    </p>

    <div class="control-group">
        <label class="control-label" for="deposit_photo">
            Foto:
        </label>
        <div class="controls">
            <input 
                name="deposit_photo" 
                value="" 
                type="file"
            >
            <?php echo form_error('deposit_photo', $form_error_start, $form_error_end); ?>
        </div>
    </div>

    <p class="text-center" style="margin: 1.5rem 0">
        Datos tal cual los ves en el <b>ticket recibido en caja</b>
    </p>

    <div class="control-group">
        <?php if (false): ?>
        <div class="text-center" style="margin-bottom: 1rem;">
            <img src="/img/payments/redbrou/transactionid.png" style="width: 100%;">
        </div>
        <?php endif; ?>

        <label class="control-label" for="deposit_name">Nro. de Autorización:</label>
        
        <div class="controls">
            <input 
                name="deposit_name"
                type="text"
                class="input-large" 
                placeholder="" 
                value="<?php echo set_value('deposit_name'); ?>"
            >
            <?php echo form_error('deposit_name', $form_error_start, $form_error_end); ?>
        </div>
    </div>						

    <div class="control-group">
        <?php if (false): ?>
        <div class="text-center" style="margin-bottom: 1rem;">
            <img src="/img/payments/redbrou/date.png" style="width: 100%;">
        </div>
        <?php endif; ?>

        <label class="control-label" for="deposit_date">Fecha:</label>
        
        <div class="controls">
            <input 
                name="deposit_date"
                class="input-large"  
                placeholder="Ej: <?php echo $deposit_date_default; ?>" 
                value="<?php echo set_value('deposit_date', $deposit_date_default); ?>" 
                type="text"
            >
            <?php echo form_error('deposit_date', $form_error_start, $form_error_end); ?>
        </div>
    </div>	


    <div class="control-group">
        <label class="control-label" for="deposit_time">Hora:</label>
        
        <div class="controls">
            <input 
                name="deposit_time"
                class="input-large"  
                placeholder="" 
                value="<?php echo set_value('deposit_time'); ?>" 
                type="text"
            >
            <?php echo form_error('deposit_time', $form_error_start, $form_error_end); ?>
        </div>
    </div>  


    <div class="control-group">
        <label class="control-label" for="deposit_amount">Monto ($):</label>
        <div class="controls">
            <input 
                name="deposit_amount" 
                placeholder="Ej: 500" 
                type="number" 
                min="50" 
                max="10000"
                class="input-large" 
                value="<?php echo set_value('deposit_amount', $deposit_amount_default); ?>"
            >
            <?php echo form_error('deposit_amount', $form_error_start, $form_error_end); ?>
        </div>
    </div>

    <p class="text-center" style="margin: 1.5rem 0">
        Otros datos
    </p>

    <div class="control-group">
        <label class="control-label" for="deposit_user">Usuario de Pumbate:</label>
        <div class="controls">
            <input 
                class="input-large" 
                name="deposit_user" 
                placeholder="Ej: <?php echo $deposit_user_default ?>" 
                value="<?php echo set_value('deposit_user', $deposit_user_default); ?>" 
                type="text"
            >
            <?php echo form_error('deposit_user', $form_error_start, $form_error_end); ?>
        </div>
    </div>

    <div class="control-group">
        <label class="control-label" for="deposit_phone">Cel. con el que publicas:</label>
        <div class="controls">
            <input 
                class="input-large" 
                name="deposit_phone" 
                placeholder="Ej: 099 999 999" 
                value="<?php echo set_value('deposit_phone', $deposit_phone_default); ?>"
                type="text"
            >
            <?php echo form_error('deposit_phone', $form_error_start, $form_error_end); ?>
        </div>
    </div>

    <div class="control-group">
        <label class="control-label" for="deposit_comments">Comentarios:</label>
        <div class="controls">
            <textarea 
                name="deposit_comments"
                rows="2" 
                class="input-xfull"
                placeholder="Cualquier duda, si pagas por otra persona, etc"
            ><?php echo set_value('deposit_comments', ''); ?></textarea>
            <?php echo form_error('deposit_comments', $form_error_start, $form_error_end); ?>
        </div>
    </div>


    <div class="control-group margin-top-30 text-center">
        <button name="send_form" value="1" type="submit" class="btn btn-large btn-full btn-main">Notificar Pago</button>
    </div>
</form>