<div class="alert alert-panel alert-panel-success" style="margin-top: 1rem; margin-right: 10px;">
    <p class="alert-panel-text text-center" style="text-transform: uppercase;">
        Recuerda que para aparecer en la portada del sitio debes activar tu pagina
    </p>
    <div class="text-center">
        <a 
            class="btn btn-main"
            href="<?php echo site_url('/profile-panel/publication/status/' . $user)?>"
        >
            ACTIVAR MI PAGINA
        </a>
    </div>
</div>