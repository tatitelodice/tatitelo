<div class="alert alert-panel alert-panel-success" style="margin-top: 1rem; margin-right: 10px;">
    <p class="alert-panel-text text-center" style="text-transform: uppercase;">
        Hola! Tu mes de publicación en portada

        <span data-ui-text="due-date">
        <?php 
            if ($billing['next_payment_date'] && $billing['days_remaining'] > 0)
            {
                $daysWord = (intval($billing['days_remaining']) === 1) ? 'día' : 'días';
                $daysCountText = $billing['days_remaining'] . ' ' . $daysWord;

                echo 'acabará en ' . $daysCountText . ' el ' . $billing['next_payment_date'];
            }
            else
            {
                echo 'se ha terminado, pronto dejarás de estar en portada';
            } 
        ?>
        <span>
    </p>
    <div class="text-center">
        <a 
            class="btn btn-main"
            href="<?php echo site_url('/profile-panel/publication/status/' . $user); ?>"
        >
            EXTENDER PUBLICACIÓN
        </a>
    </div>
</div>