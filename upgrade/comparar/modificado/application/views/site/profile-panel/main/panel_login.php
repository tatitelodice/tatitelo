	<script>
		function show_fl() {
			$('#recover-text').addClass('hide');
			$('#recover-fl').removeClass('hide');
		}
	</script>

	<div class="container">
		<div class="row">
			<div class="span6 offset3 header-container">
				<h3 class="text-center margin-top-50">
					Accede a tu pumbate.com
				</h3>
			</div>
		</div>
	</div>	
		
	<div class="container">
		
		<div class="row">
			<div class="span6 offset3 white-container round-bottom-10">

				<form class="form-horizontal form-login" method="post" action="">		
					<?php
						$form_error_start = '<div><span class="label label-error label-inline"><i class="glyphicon glyphicon-warning-sign"></i></span><span class="help-inline help-inline-white">';
						$form_error_end = '</span></div>';
					?>
					<?php if (isset($maintenance) && $maintenance): ?>
						<div class="alert alert-panel-warning text-justify">
							Estamos haciendo mantenimiento al sitio para mejorar su funcionamiento, por 
							eso hemos deshabilitado por unas horas la posibilidad de hacer cambios en tu
							página. Si es un tema urgente, puedes llamarnos al fonopumbate.<br><br>
							<div class="text-center">Gracias por tu paciencia!</div>
						</div>
					
						<div class="text-center">
							<a class="btn btn-main" href="<?php echo site_url(); ?>">Volver a Portada de Pumbate.com</a>
						</div>
					
					<?php else: ?>
						<h4>
							Escribe tu usuario de pumbate y su contraseña para editar tu página personal de pumbate:
						</h4>
						<br>

						<div class="control-group">
							<label class="control-label" for="user">Usuario</label>
							<div class="controls">
								<input 
									type="text" style="width: 240px;" id="user" name="user" 
									value="<?php echo set_value('user', '') ?>"
									autocomplete="on" 
									autocorrect="off" 
									autocapitalize="off" 
									spellcheck="false">
								<?php echo form_error('user', $form_error_start, $form_error_end); ?>
							</div>
						</div>

						<div class="control-group">
							<label class="control-label" for="password">Contraseña</label>
							<div class="controls">
								<input 
									type="password" style="width: 240px;" id="password" name="password" value=""
									autocomplete="off" 
									autocorrect="off" 
									autocapitalize="off" 
									spellcheck="false">
								<?php echo form_error('password', $form_error_start, $form_error_end); ?>
							</div>			
						</div>

						<div class="control-group">
							<div class="controls">
								<?php if ($error): ?>
									<div class="alert alert-pink-noborder input-large text-center"><button class="close" data-dismiss="alert">×</button>Intenta de nuevo!<br>Uno ó ambos datos no son correctos.</div>
								<?php endif; ?>
								
								<button id="aplicar" type="submit" name="send" value="1" class="btn input-xlarge btn-main" style="width: 255px;">Acceder a mi panel</button>
							</div>
						</div>
					<?php endif; ?>
				</form>
				
				<?php if (false === (isset($maintenance) && $maintenance)): ?>
					<hr class="margin-bottom-0">

					<div
						class="padding-10 color-violet" 
						style="background: #ffff9f; text-align: center; font-size: 1.3rem; line-height: 2.0rem; font-weight: bold"
					>

						<?php if ($has_first_login): ?>
							<a 
								id="recover-text"
								class="color-violet"
								style="border-bottom: 0.2rem dotted #9C0075;"
								href="#"
								onClick="show_fl()"
							>
								Mostrarme mi nuevo usuario/contraseña
							</a>

							<div id="recover-fl" class="hide">
								<p>
									usuario: <?php echo $fl_user; ?>
								</p>
								<p>
									contraseña: <?php echo $fl_pass; ?>
								</p>
							</div>
						
						<?php else: ?>
							<a 
								class="color-violet"
								style="border-bottom: 0.2rem dotted #9C0075;"
								href="<?php echo site_url('restore'); ?>"
							>
								Perdiste tu Usuario/Contraseña?
							</a>
						<?php endif; ?>
					</div>
				<?php endif; ?>
			</div>
		</div>
	</div>
