	
<div class="row-fluid" style="margin-top: 15px;">	
	<div class="span4">
		<div class="well well-small well-panel well-panel-yellow text-center">
			<h3>HOY RECIBISTE YA</h3>
			<h1 style="font-size: 100px;"><?php echo $today_visits; ?></h1>
			<h2>VISITAS</h2>

			<hr>

			<h3>HAS RECIBIDO</h3>
			<h1 style="font-size: 80px;"><?php echo $total_visits; ?></h1>
			<h2>DESDE QUE ESTÁS EN PUMBATE</h2>
		</div>
	</div>
	
	<div class="span8">
		<form class="form-inline" method="post">
			<label style="font-size: 18px;">Estas viendo tus visitas entre el&nbsp;&nbsp;</label>
			<input style="font-weight: bold;" id="start-date" type="text" class="input-small" name="start_date" value="<?php echo date('01/m/Y'); ?>">
			<label style="font-size: 18px;">&nbsp; y el &nbsp;</label>
			<input style="font-weight: bold;" id="end-date" type="text" class="input-small" name="end_date" value="<?php echo date('d/m/Y'); ?>">
			&nbsp;&nbsp;<button class="btn btn-main" type="button" onClick="update_visits_table()">Cambiar</button>
		</form>
		
		<div id="table-visits">
		<?php
			if ($table_visits == '') {
				echo '<br><br><hr><h3 class="text-center">Pumbate.com no tiene aún datos para mostrarte de las fechas seleccionadas.</h3><hr>'; 
			} else {
				echo $table_visits;
			}
			
		?>
		</div>
	</div>
</div>

<script>
	function update_visits_table() {
		var post_data = {
			user: '<?php echo $user; ?>',
			start_date: $('#start-date').val().replace(/(\d+)\/(\d+)\/(\d+)/, '$3-$2-$1'),
			end_date: $('#end-date').val().replace(/(\d+)\/(\d+)\/(\d+)/, '$3-$2-$1'),
			action: 'visits_table'
		};

		$.post('', post_data, function(html_table_visits) {
			$('#table-visits').html(html_table_visits);
		});
	}
	
	$( "#start-date" ).datepicker();
	$( "#end-date" ).datepicker();
	
</script>