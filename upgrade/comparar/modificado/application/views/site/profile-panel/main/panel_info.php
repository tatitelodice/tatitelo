<form class="form-horizontal" method="POST" action="">
							
	<h4>A continuación podrás cambiar los textos de tu página. 
	No olvides al terminar darle al botón <u>GUARDAR CAMBIOS</u> para que
	se modifique tu página web con los nuevos datos!!!
	</h4>
	<hr>

	<input type="hidden" name="action" value="information">
	<input type="hidden" name="user" value="<?php echo $user; ?>">
	<input type="hidden" name="country" value="<?php echo $country; ?>">
	
	<input id="absent" type="hidden" name="absent" value="<?php echo $absent; ?>">
	<textarea id="absent_message" class="hide" name="absent_message"><?php echo $absent_message; ?></textarea>
	
	<fieldset>
		<!--<legend>Acerca de vos</legend>-->
		<div class="control-group">
			<label class="control-label" for="name">Nombre Artístico:</label>
			<div class="controls">
				<input type="text" name="name" value="<?php echo $name; ?>" placeholder="Ejemplo: Carolina Edén"><br>
				Este es el nombre que ven en portada tus visitantes, aunque lo cambies tu usuario pumbate seguirá siendo <span class="label label-pumbate" style="font-size: 16px; line-height: 18px;"><?php echo $user; ?></span>
				y tu dirección personal <span class="label label-pumbate" style="font-size: 16px; line-height: 18px;">pumbate.com/<?php echo $user; ?></span>
			</div>
		</div>


		<div class="control-group">
			<label class="control-label" for="age">Edad:</label>
			<div class="controls">
				<?php
					echo form_dropdown('age', $ages, $age, ' id="age" class="input-medium"');
				?>
				años.
			</div>
		</div>
		
		<div class="control-group">
			<label class="control-label" for="region">Ubicación:</label>
			<div class="controls">
				<?php
					echo form_dropdown('region', $regions, $region, ' id="region" class="region-select input-xlarge"');
					echo ' ' . form_dropdown('city', $cities, $city, ' id="city" class="input-xlarge"');
				?>
			</div>
		</div>
		
		<div class="control-group" style="margin-top: 30px; margin-bottom: 20px;">
			<label class="control-label" for="region">Viajas a otros sitios?</label>
			<div class="controls">
				<?php
					$regions_extra = array_merge( array('' => 'Elige Departamento'), $regions);
				
					//print_r($extra_locations);
					
					for ($i=0; $i < 4; $i++) {
						
						if (isset($extra_locations[$i])) {	
							echo '<div style="margin-bottom:15px;">';
							echo form_dropdown('extra_region' . $i, $regions_extra, $extra_locations[$i]['region'], ' class="region-select extra-region-select input-xlarge"');
							echo ' ' . form_dropdown('extra_city' . $i, $extra_locations[$i]['cities'], $extra_locations[$i]['city'], ' class="input-xlarge"');
							echo ' <button type="button" class="btn btn-remove-location">Quitar</button>';
							echo '</div>';
							
						}  else {
							echo '<div style="margin-bottom:15px;" class="hide">';
							echo form_dropdown('extra_region' . $i, $regions_extra, '', ' class="region-select extra-region-select input-xlarge"');
							echo ' ' . form_dropdown('extra_city' . $i, array(), '', ' class="input-xlarge hide"');
							echo ' <button type="button" class="btn btn-remove-location">Quitar</button>';
							echo '</div>';
						}
						
					}
				?>
				<button id="add-location" type="button" class="btn btn-mini" style="margin: 5px 0px 10px 0px;">Agregar otras ubicaciones a las que puedo viajar</button>
			</div>
		</div>	

		<script>
			$('.region-select').change( function() {
				var regobj = $(this);
				
				if (regobj.hasClass('extra-region-select') && regobj.val() === '') {
					regobj.next().addClass('hide');
					
				} else {
					var post_data = {
						country: '<?php echo $country; ?>',
						region: regobj.val(),
						action: 'city'
					};

					$.post('', post_data, function(html_cities) {
						regobj.next().html(html_cities);
					});
					
					regobj.next().removeClass('hide');
				}
			});
			
			$('#add-location').click(function() {
				var hiddens = 0;
				
				for (i=0; i < 4; i++) {
					var addobj = $(this);
					var extraobj = $("[name=extra_region" + i + "]");
					
					if ( extraobj.parent().hasClass('hide')) {
						
						if (hiddens === 0) {
							extraobj.parent().removeClass('hide');
						}
						
						hiddens++;
					}
				}
				
				if (hiddens === 1) {
					addobj.addClass('hide');
				}
				
			});
			
			$('.btn-remove-location').click(function() {
				$(this).parent().addClass('hide');	
				$(this).siblings('select').val('');
				
				var addobj = $('#add-location');
				var hiddens = 0;
				
				for (i=0; i < 4; i++) {
					var extraobj = $("[name=extra_region" + i + "]");
					
					if ( !extraobj.parent().hasClass('hide')) {
						hiddens++;
					}
				}
				
				if (addobj.hasClass('hide') && hiddens !== 0) {
					addobj.removeClass('hide');
				}
				
			});
			
			
			
		</script>

		<div class="control-group">
			<label class="control-label" for="place">Lugar de atención:</label>
			<div class="controls">
				<input type="text" class="input-xxlarge" name="place" value="<?php echo $place; ?>" placeholder="Ejemplo: Hotel, Domicilio, Apartamento propio, etc">										
			</div>
		</div>

		<div class="control-group">
			<label class="control-label" for="phone">Celular o tel de contacto:</label>
			<div class="controls">
				<input type="text" class="input-xlarge" name="phone" value="<?php echo $phone; ?>" placeholder="Ejemplo: Llámame al 099 999999 (no sms)">
			</div>
		</div>

		<div class="control-group">
			<label class="control-label" for="days">Días que trabajas</label>
			<div class="controls">
				<?php
					foreach ($week_days_options as $week_day) {
						//print_r(in_array($week_day, $days));
						$week_day_checked = ( in_array($week_day, $days) )? 'checked="checked"' : '';
						echo '<label class="checkbox inline" style="margin-right: 5px;"><input type="checkbox" name="days[]" value="' . $week_day . '" '.$week_day_checked.'>'.$week_day.'</label>';
					}
				?>
			</div>
		</div>
		
		<hr>
		<div class="control-group">
			<label class="control-label" for="title">Titulo en grande:<br></label>
			<div class="controls">
				<input type="text" class="input-xxlarge" name="title" value="<?php echo $title; ?>" placeholder="Ejemplo: Carolina Edén Escort VIP">
			</div>
		</div>

		<div class="control-group">
			<label class="control-label" for="subtitle">Subtitulo 1:</label>
			<div class="controls">
				<input type="text" class="input-xxlarge" name="subtitle" value="<?php echo $subtitle; ?>" placeholder="Ejemplo: Las mejores fantasías para ellos y ellas">
			</div>
		</div>

		<div class="control-group">
			<label class="control-label" for="subtitle2">Subtitulo 2:</label>
			<div class="controls">
				<input type="text" class="input-xxlarge" name="subtitle2" value="<?php echo $subtitle2; ?>" placeholder="Ejemplo: No dudes, llamá al 09X XXX XXX y quedamos.">
			</div>
		</div>


		<div class="control-group">
			<label class="control-label" for="about">Acerca de ti:</label>
			<div class="controls">
				<textarea type="text" class="input-xxlarge" rows="6" name="about" placeholder="Ejemplo: Atrevida, morocha 1,70, ojos verdes."><?php echo $about; ?></textarea>
			</div>
		</div>

		<div class="control-group">
			<label class="control-label" for="services">Acerca de tus servicios:</label>
			<div class="controls">
				<textarea type="text" class="input-xxlarge" rows="6" name="services" placeholder="Ejemplo: Atencion a caballeros y parejas, servicio completo en domicilio u hotel."><?php echo $services; ?></textarea>
			</div>
		</div>
		
		<hr>
		<h4>No olvides marcar las categorías a continuación para que los visitantes
		te encuentren mejor en nuestra galería de anunciantes y también evitarte consultas innecesarias!</h4>
		
		<div class="control-group">
			<label class="control-label" for="categories">Categorías:</label>
			<div class="controls">
				<div class="row-fluid">
				<?php
					$cat_pos = 1;
					$cat_count = count($categories_options);
					
					foreach ($categories_options as $k_category => $v_category) {
						//print_r(in_array($week_day, $days));
						$category_checked = ( in_array($k_category, $categories) )? 'checked="checked"' : '';
						
						echo '<div class="span3">';
						echo '<label class="checkbox inline" style="margin-right: 5px;"><input type="checkbox" name="categories[]" value="' . $k_category . '" '.$category_checked.'>'.$v_category.'</label>';
						echo '</div>';
						
						if ($cat_pos % 4 == 0 && $cat_pos < $cat_count) {
							echo '</div><div class="row-fluid">';
						}
						
						$cat_pos++;
					}
				?>
				</div>
			</div>
		</div>

		<div class="control-group" style="text-align: center; margin-top: 18px;">
			<button type="submit" id="save-changes" class="btn btn-large btn-main hidden"><strong>Guardar Cambios</strong></button>
		</div>
	</fieldset>

</form>