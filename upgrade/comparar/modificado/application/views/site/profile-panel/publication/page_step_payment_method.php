<div class="container"> 
    <div class="row">
        <div class="span6 offset3 section-panel section-panel-light" style="padding: 0.5rem;">
            
            <h3 class="section-panel-title">¿Cómo y dónde se paga?</h3>

            <?php 
                $this->load->view('site/profile-panel/publication/partial_payment_info_' . $payment_method . '.php')
            ?>            

            <table class="table table-condensed table-visits" style="background: white;">
                <tbody>
                    <tr>
                        <td style="text-align: center;">
                            <?php 
                                $this->load->view('site/profile-panel/publication/partial_payment_button.php')
                            ?>
                        </td>

                    </tr>
                </tbody>
            </table>

            <p class="text-center" style="margin-top: 1.5rem">
                <a 
                    class="btn btn-main"
                    href="<?php echo site_url('/profile-panel/publication/status/' . $user)?>"
                >
                    Volver Atrás
                </a>
            </p>
        </div>
    </div>
</div>