<div style="background: #ffff9f; text-align: center; font-size: 1.3rem; line-height: 1.6rem; font-weight: bold; margin-top: 1rem; padding-bottom: 0.1rem; padding-top: 0.5rem">
    <p>En cualquier local de ABITAB:</p>
    <p>
        Depósito RedBROU<br>
        Caja de Ahorro en Pesos<br>
        Número <?php echo $payment_account['accountAlias'] ?>
    </p>
</div>

<table class="table table-condensed table-visits" style="background: white;">
    <tbody>
        <tr>
            <td style="text-align: center;">
                Al depositar, recuerda que el depósito es <strong>SIN TARJETA</strong> y en <strong>CUENTA DIGITADA</strong>
            </td>
        </tr>
        <tr>
            <td style="text-align: center;">
                <strong>Saca FOTO SÓLO DEL COMPROBANTE RECIBIDO EN CAJA</strong>, si no recibimos foto del ticket no activamos publicaciones.
            </td>
        </tr>
    </tbody>
</table>