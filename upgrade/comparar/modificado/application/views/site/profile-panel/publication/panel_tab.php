<?php 
    //$this->load->view('site/profile-panel/publication/partial_absent');
?>

<div class="row-fluid" style="font-weight: bold; font-size: 1.2rem;">
    <div class="span6">
        <h3 style="font-style: underline;">Estado de Publicación</h3>
        <?php if ($publication['is_active']): ?>
            <p style="color: black;">En portada: SÍ</p>
            <p style="color: black;">Tipo de publicación: <?php echo $publication['level']; ?></p>
            
            <?php if ($billing['has_pending_deposit']): ?>
                <p style="color: black; margin: 0">Termina el:</p>
                <div class="alert alert-warning">
                    Tienes un depósito pendiente de aprobación, se actualizará aquí la 
                    nueva fecha en las próximas 24 horas.
                </div>
            <?php elseif ($billing['next_payment_date']): ?>
                <p style="color: black;">
                    Termina el: <?php echo $billing['next_payment_date'] ?>
                </p>
            <?php endif; ?>

            <p style="color: black;">Estado: <?php echo $publication['status']; ?></p>

            <?php if (!$publication['is_available']): ?>
            <p style="color: black;">Mensaje:<br>
                <?php echo $absentMessage; ?>
                <a class="color-violet" href="<?php echo site_url('/profile-panel/availability/modify/' . $user)?>">
                    [ cambiar ]
                </a>

            </p>
            <?php endif; ?>
        <?php else: ?>
            <p style="color: black;">En portada: NO</p>
        <?php endif; ?>
    </div>

    <div class="span6">
        <h3 style="font-style: underline;">Acciones</h3>
        <?php if ($publication['is_available']): ?>
            <p class="color-violet">
                <a class="color-violet" href="<?php echo site_url('/profile-panel/availability/modify/' . $user)?>">
                    Cambiar mi estado a "No Disponible"
                </a>
            </p>
        <?php else: ?>
            <p class="color-violet">
                <a class="color-violet" href="<?php echo site_url('/profile-panel/availability/make_available/' . $user)?>">
                    Cambiar mi estado a "Disponible"
                </a>
            </p>
        <?php endif; ?>

        <?php if ($billing['pricing']): ?>
        <p>
            <a class="color-violet" href="<?php echo site_url('/profile-panel/billing/new_deposit/' . $user)?>">
                Notificar pago y subir foto de ticket
            </a>
        </p>
        <p>
            <a class="color-violet" href="<?php echo site_url('/profile-panel/publication/status/' . $user); ?>">
                Extender o destacar mi publicación
            </a>
        </p>
        <?php endif; ?>

        <p>
            <a class="color-violet" href="<?php echo site_url('/profile-panel/publication/unsuscribe/' . $user)?>">
                Dar de baja mi publicación
            </a>
        </p>

    </div>
</div>

<!--
<div class="row-fluid" style="font-weight: bold; font-size: 1.2rem;">

    <div class="span12">
        <h3 style="font-style: underline;">Perfil</h3>
        <p>
            <a class="color-violet" href="">
                Eliminar definitivamente mi perfil
            </a>
        </p>
    </div>
</div>
-->