<?php
    $this->load->view('site/header', array('title' => $page_title));
?>

<div class="container" style="color:white; background-color: #440925">   
    <div class="row">
        <div class="span10 offset1" style="padding: 1rem;">
            <h1 class="text-center">
                <?php echo $page_header; ?>
            </h1>

            <p class="lead" style="font-weight: bold;">
                Estás viendo el listado antiguo de anuncios en categoría <?php echo $gender ?>,
                que quizá sigan trabajando pero han dado de baja su publicación,
                si todavía estás buscando hacer una colita, un cambio de roles o
                simplemente un blowjob, visita la galería de chicas, chicos y 
                otros ofrecimientos de sexo en el listado principal:
            </p>

            <p class="text-center">
                <a class="btn btn-large btn-main" href="/">
                    Ir a <?php echo $fancy_gender ?> disponibles
                </a>
            </p>
            <hr>
            <?php foreach ($advertisers as $adv): ?>
            <p style="color: white; font-size: 1.8rem; line-height: 2.0rem; margin-top: 1.5rem;">
                <a href="<?php echo $adv["publication_url"]; ?>" style="color: white; font-weight: bold;">
                    <?php
                        //var_dump($adv);
                        echo  $adv["title"] . " ";
                        
                        if ($adv["id"] % 10 === 0)
                        {
                            echo "pasión ";
                        }

                        if ( ($adv["id"] % 5 === 0) && ($adv["id"] % 10 === 5) )
                        {
                            echo "yiranta ";
                        }

                        echo ($adv["city"] ?? $adv["region"] ?: "");

                        if ($adv["user"] !== mb_strtolower($adv["title"]))
                        {
                            echo " escribió " . $adv["user"];
                        }
                    ?>
                </a>
            </p>
            <p style="font-size: 1.3rem; line-height: 1.6rem;">
                <?php 
                    echo preg_replace("!\d\d\d!", "", $adv["services"]);
                ?>
            </p>
            <br>
            <?php endforeach;  ?>
            <hr>
        </div>
    </div>
</div>

<?php
        $this->load->view('site/footer');
?>