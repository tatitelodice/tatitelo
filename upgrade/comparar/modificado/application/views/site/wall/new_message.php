<div class="container header-container">
	<div class="row">
		<div class="span5">
			<h1 class="logo">
				<a title="Muro Pumbate" href="<?php echo site_url('wall'); ?>"><img title="Muro Pumbate" alt="Muro Pumbate Logo" src="<?php echo base_url('img/logomuro.png'); ?>"></a>
			</h1>
			<h2 class="logo">
				Avisos e información de interés
			</h2>
		</div>
		<div id="main-nav" class="span7">
			<h3>
				<a href="<?php echo site_url('wall'); ?>"><strong>Volver a la página principal del Muro</strong></a>
			</h3>
		</div>
	</div>
</div>

<div class="container page-container">
	<div class="row">
		<div class="span12">
			<div class="page-container-inner">
				<h3 class="text-center">Publicar nuevo mensaje en el Muro</h3>
				<hr style="margin-bottom: 10px;">
			</div>
		</div>
		
		<div class="span10 offset1">
			<div class="page-container-inner">
				<form id="form-new" class="form-horizontal" method="post" action="">
					
					<div class="control-group">
						<label class="control-label" for="title">Título</label>
						<div class="controls">
							<input type="text" class="input-xxlarge" id="name" name="title" value="<?php echo set_value('title'); ?>">
							<?php echo form_error('title', '<br><br><span class="label label-error label-inline"><i class="glyphicon glyphicon-warning-sign"></i></span><span class="help-inline help-inline-white">', '</span>'); ?>
						</div>			
					</div>

					<div class="control-group">
						<label class="control-label" for="category">Categoría</label>
						<div class="controls">
							<?php
								$category_options = array(
									'general' => 'Ninguna en particular',
									'clientes' => 'Alertas sobre clientes peligrosos o violentos',
									'lugares' => 'Lugares donde brindar servicios',
								);
								
								if ($admin) {
									$category_options['pumbate'] = 'Avisos del Equipo de Pumbate.com';
								}

								echo form_dropdown('category', $category_options, set_value('category'), 'class="input-xxlarge"');
								echo form_error('category', '<br><br><span class="label label-error label-inline"><i class="glyphicon glyphicon-warning-sign"></i></span><span class="help-inline help-inline-white">', '</span>');
							?>
						</div>			
					</div>
					
					<?php if ($admin && !$country): ?>
					<div class="control-group">
						<label class="control-label" for="country">País</label>
						<div class="controls">
							<?php
								echo form_dropdown('country', $country_options, set_value('country'), ' class="input-large"');
								echo form_error('country', '<br><br><span class="label label-error label-inline"><i class="glyphicon glyphicon-warning-sign"></i></span><span class="help-inline help-inline-white">', '</span>');
							?>
						</div>			
					</div>
					<?php endif; ?>

					<div class="control-group">
						<label class="control-label" for="text">Texto del Mensaje</label>
						<div class="controls">
							<textarea rows="8" class="input-xxlarge" name="text"><?php echo set_value('description'); ?></textarea>
							<?php echo form_error('text', '<br><br><span class="label label-error label-inline"><i class="glyphicon glyphicon-warning-sign"></i></span><span class="help-inline help-inline-white">', '</span>'); ?>
						</div>			
					</div>

					<div class="control-group">
						<div class="controls">
							<input type="submit" class="btn btn-rose btn-large" value="PUBLICAR">
						</div>
					</div>
				</form>
			</div>
		</div>
		
	</div>
</div>	