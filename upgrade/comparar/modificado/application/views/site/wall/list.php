<div class="container header-container">
	<div class="row">
		<div class="span5">
			<h1 class="logo">
				<a title="Muro Pumbate" href="<?php echo site_url('wall'); ?>"><img title="Muro Pumbate" alt="Muro Pumbate Logo" src="<?php echo base_url('img/logomuro.png'); ?>"></a>
			</h1>
			<h2 class="logo">
				Avisos e información de interés
			</h2>
		</div>
		<div id="main-nav" class="span7">
			<h3>
				<?php if ($admin): ?>
					<a href="<?php echo site_url('padmin'); ?>"><strong>Volver a Panel de Administrador</strong></a>
				<?php else: ?>
					<a href="<?php echo site_url('mipagina'); ?>"><strong>Volver a Editar Mi Pagina</strong></a>
				<?php endif; ?>
			</h3>
		</div>
	</div>
</div>

<div class="container page-container">
	<div class="row">
		<div class="span12">
			<div class="page-container-inner">
				
				<?php if ($admin): ?>
					<div class="label label-pumbate">
						<h5 class="text-center">
							*** Glorioso Modo Administrador ***
						</h5>
					</div>
				<?php else: ?>
					<p class="text-justify hidden-phone" style="color: #ffd4d4;">
						Hola <?php echo $user; ?>! Te damos la bienvenida al Muro Pumbate, el Muro es una sección exclusiva para los anunciantes verificados, facilitándoles compartir información relevante
						sobre hoteles, lugares de atención, clientes violentos o peligrosos de los que estar alerta,
						entre cualquier tipo de mensaje que valga la pena compartir y sea de interés general.<br><br>
						Sientete libre de publicar mensajes nuevos o responder a ya existentes, todos los mensajes que escribas tendrán
						como autor tu usuario de pumbate.
					</p>
				<?php endif; ?>	
					
				<hr class="hidden-phone" style="margin-bottom: 0px;">
					
					
				<div class="row-fluid">

					<div class="span9">
						<h3 class="text-left">
							
							<?php 
								if ($category === false) {
									echo 'Mensajes de todas las categorías';
								} else {
									echo 'Mensajes en la categoría "' . $categories[$category] . '"';
								}
							?>						
						</h3>

						<?php if (count($messages) == 0) : ?>
							<div class="row-fluid">
								<div class="span12">
									<h4 class="text-center" style="padding-top: 20px;">No hay mensajes nuevos.</h4>
								</div>
							</div>

						<?php else: ?>
							<?php foreach ($messages as $message): ?>

								<div class="wallmessage <?php echo ($message['deleted'])? ' wallmessage-deleted' : ''; echo ($message['sticky'])? ' wallmessage-sticky' : ''; ?>">
									
									<?php if ($admin): ?>
									<div class="actions">
										<?php if ($message['sticky']) : ?>
											<a title="Despegar" href="<?php echo site_url('wall/unstick_message/'.$message['id']) ?>"><i class="glyphicon glyphicon-minus-sign"></i></a>
										<?php else: ?>
											<a title="Pegar arriba del todo" href="<?php echo site_url('wall/stick_message/'.$message['id']) ?>"><i class="glyphicon glyphicon-plus-sign"></i></a>
										<?php endif; ?>
										
										<?php if ($message['deleted']) : ?>
											<a title="Recuperar" href="<?php echo site_url('wall/restore_message/'.$message['id']) ?>"><i class="glyphicon glyphicon-refresh"></i></a>
										<?php else: ?>
											<a title="Eliminar" href="<?php echo site_url('wall/delete_message/'.$message['id']) ?>"><i class="glyphicon glyphicon-trash"></i></a>
										<?php endif; ?>
									</div>
									<?php endif; ?>
									
									<h4>
										<?php 
											if ($admin && $message['country']) {
												echo '<img src="'.  base_url('img/flags/'.$message['country'].'_24.png') .'">'; 
											}
											
											if ($admin && $message['deleted']) {
												echo '<span class="label label-deleted">Borrado</span>';
											}
										?>										
										<a class="title" href="<?php echo $message['url']; ?>"><?php echo $message['title']; ?></a>
										<small> <b>&mdash; por</b> <?php echo $message['gender_symbol']; ?> <b><?php echo $message['user']; ?></b></small>
									</h4>
									
									<?php if (!$message['deleted']): ?>
									<div class="text">
										<?php echo $message['text']; ?>
										&nbsp;<a class="readmore" href="<?php echo $message['url']; ?>">seguir leyendo &rarr;</a>
									</div>
									<?php endif; ?>
									
									<hr>
								
									<div class="row-fluid">
										<div class="span9 date">
											Publicado el <?php echo $message['date']; ?> 
											
											<?php
												if (isset( $categories[$message['category']] )) {
													echo ' en categoría <b>' . anchor('wall/category/' . $message['category'], $categories[$message['category']]) . '</b>';
												}
											?>
										</div>
										<div class="span3 replies">	
											<a class="label label-comments" href="<?php echo $message['url']; ?>">
												<i class="glyphicon glyphicon-comment"></i>
												<?php if ($message['replies'] > 0): ?>
													<?php echo $message['replies']; ?>&nbsp;comentario<?php echo ($message['replies'] > 1)? 's' : ''; ?>
												<?php else: ?>
													Deja un comentario!
												<?php endif; ?>
											</a>
										</div>
									</div>
									
								</div>

							<?php endforeach; ?>
						<?php endif; ?>
					</div>

					<div class="span3" style="padding-top: 20px;">

						<a class="btn btn-large btn-rose" href="<?php echo site_url('wall/new_message'); ?>">Publicar Nuevo Mensaje</a>

						<h3 class="text-left">Categorías</h3>
						<ul class="wallcategories">
							<li><a href="<?php echo site_url('wall'); ?>">Todas</a></li>
							
							<?php foreach ($categories as $cval => $cname): ?>
								<li>
									<?php echo anchor('wall/category/' . $cval, $cname); ?>
								</li>								
							<?php endforeach; ?>

						</ul>
					</div>


				</div>

			</div>
		</div>
	</div>


</div>	