<span class="hide about">
    <?php echo $about; ?>
</span>

<span class="hide user">
    <?php echo $user ?>
</span>

<span class="hide phone">
    <?php echo $phone ?>
</span>
 
<?php if (!$absent): ?>
<div class="hide thumbs gallery-tooltip-thumbs">
    <?php
        // dejar asi para evitar que los espacios en blanco rompan la
        // fila del tooltip
        foreach ($thumbs as $th)
        {
            echo '<img src="'. $th . '">';
        }
    ?>
</div>
<?php endif; ?>