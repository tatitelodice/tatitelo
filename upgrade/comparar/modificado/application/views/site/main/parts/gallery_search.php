<form class="hide">
    <input type="hidden" name="country_iso" value="<?php echo $country_iso ?>">
    <input type="hidden" name="escorts_gender" value="<?php echo $escorts_gender ?>">
</form>

<form class="form-inline form-location-filter" method="GET" action="<?php echo current_url('') . '#gallery'; ?>">

    <input class="input-large" type="text" name="keywords" value="<?php echo $keywords; ?>" placeholder="Palabras clave...">

    <?php
        echo form_dropdown('region', $regions, $region, ' id="region" class="input-large"');

        echo ' ';

        if ($region && count($cities)) {
            echo form_dropdown('city', $cities, $city, ' id="city" class="input-large"');
        } else {
            echo form_dropdown('city', $cities, $city, ' id="city" class="input-large hide"');
        }
    ?>

    <button type="submit" class="btn btn-rose">Buscar</button>
</form>