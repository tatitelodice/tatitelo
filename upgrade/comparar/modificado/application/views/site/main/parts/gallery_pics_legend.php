<div class="pictures-notes">
    Dudas de las fotos? Mirá los sellos!
    <a class="violet" title="Saber más acerca de política de Fotos y Sellos" target="_blank" href="https://www.pumbate.com/blog/nuevo-sistema-de-fotografias/">Más info aquí&nbsp;&raquo;</a><br>
    <img class="simple-tooltip" title="Por más que las fotos sean reales, nuestro equipo no conoce personalmente al anunciante para asegurarlo." alt="Sello Fotos Subidas por Anunciante" src="<?php echo base_url('assets/themes/common/veripics/notverifiedclub.png'); ?>">                                  
    <img class="simple-tooltip" title="Nuestro equipo ha visitado al anunciante y certifica que las fotos expuestas son 100% reales." alt="Sello Fotos Verificadas por Pumbate.com" src="<?php echo base_url('assets/themes/common/veripics/verifiedclub.png'); ?>">
</div>