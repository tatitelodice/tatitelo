<br> 
<p class="text-center medium">
    No tenemos atención personalizada de ningún tipo, para hacer cambios a tu perfil 
    o darlo de baja es con tu usuario y contraseña, que puedes recuperar en cualquier momento.
</p>
<br>
<p class="text-center medium">
    Si quieres dar de baja tu perfil, entra con tu usuario y contraseña
    y selecciona "Publicación" y "Dar de baja mi Perfil".
</p>
<br>
<p class="text-center medium">
    Si haz perdido tu contraseña 
    <a  class="btn btn-mini"
        href="<?php echo site_url('restore'); ?>"
    >
            entra aquí
    </a>
</p>
<br>
<p class="text-center medium">
    Si tu caso no es ninguno de los anteriores, puedes escribir a:
    <br>
    <?php echo safe_mailto($pumbate_mail, $pumbate_mail, 'style="color:white";'); ?>
    <br><br>
</p>