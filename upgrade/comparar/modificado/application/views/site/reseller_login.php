<div class="row">
		<h2 style="text-align: center;">Modo Reseller<h2>
		<h4 style="text-align: center;">Por favor ingrese su usuario y contraseña a continuación:<h4>		
</div>

<div class="row">
	<div class="span5 offset3">
		<br>
		<form class="form-horizontal" method="post" action="<?php echo site_url('reseller'); ?>">		
			
			
			<div class="control-group">
				<label class="control-label" for="name"><i class="glyphicon glyphicon-user"></i>&nbsp;Usuario</label>
				<div class="controls">
					<input 
						type="text" class="input-xlarge" id="name" name="name" 
						value=""
						autocomplete="off" 
						autocorrect="off" 
						autocapitalize="off" 
						spellcheck="false">
					<?php echo form_error('name', '<span class="label label-error">Atención</span><span class="help-inline">', '</span>'); ?>
				</div>			
			</div>

			<div class="control-group">
				<label class="control-label" for="password"><span class="add-on"><i class="glyphicon glyphicon-lock"></i>&nbsp;Contraseña</label>
				<div class="controls">
					<input 
						type="password" class="input-xlarge" id="password" name="password" 
						value=""
						autocomplete="off" 
						autocorrect="off" 
						autocapitalize="off" 
						spellcheck="false">
					<?php echo form_error('password', '<span class="label label-error">Atención</span><span class="help-inline">', '</span>'); ?>
				</div>			
			</div>
						
			<div class="control-group">
				<div class="controls">
					<?php echo $error; ?>
					<button id="aplicar" type="submit" name="send" value="1" class="btn btn-main input-xlarge">Acceder</button>
				</div>
			</div>
		</form>
	</div>
</div>