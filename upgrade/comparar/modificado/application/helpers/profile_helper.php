<?php

if (!function_exists('get_profile_pictures')) {
	
	function get_profile_pictures($user, $with_info = false) {
		$pictures = array();
		
		$user_dir = 'userdata/'.$user.'/';
		
		if (is_dir($user_dir)) {
			foreach (scandir($user_dir) as $f) {
				if (preg_match('!\.(jpe?g|png|gif|bmp)!i', $f)) {
					$src = '/userdata/'.$user.'/'.$f;

					if ($with_info) {
						
						// HACK TEMPORAL HASTA QUE SE ACOMODEN TODAS LAS MINIATURAS
						if (file_exists('userdata/'.$user.'/w200/'.$f)) {
							$img_info = getimagesize('userdata/'.$user.'/w200/'.$f);
						} else {
							$img_info = getimagesize('userdata/'.$user.'/'.$f);
						}
						
						$type = 'vertical';

						if ($img_info[0] > $img_info[1] && $img_info[1] * 100 / $img_info[0] < 90 ) {	
							$type = 'horizontal';	
						}

						$pictures[] = array(
							'src' => $src,
							'type' => $type,
							'width' => $img_info[0],
							'height' => $img_info[1],
						);
					} else {
						$pictures[] = $src;
					}

				}
			}
		} else {
			@mkdir($user_dir, 0777);
		}
		
		return $pictures;
	}
}	


if (!function_exists('orient_image')) {
	
	function orient_image($file_path) {
        if (!function_exists('exif_read_data')) {
            return false;
        }
        $exif = @exif_read_data($file_path);
        if ($exif === false) {
            return false;
        }
        $orientation = intval(@$exif['Orientation']);
        if (!in_array($orientation, array(3, 6, 8))) {
            return false;
        }
        $image = @imagecreatefromjpeg($file_path);
        switch ($orientation) {
            case 3:
                $image = @imagerotate($image, 180, 0);
                break;
            case 6:
                $image = @imagerotate($image, 270, 0);
                break;
            case 8:
                $image = @imagerotate($image, 90, 0);
                break;
            default:
                return false;
        }
        $success = imagejpeg($image, $file_path);
        // Free up memory (imagedestroy does not delete files):
        @imagedestroy($image);
        return $success;
    }
}


if(!function_exists('create_scaled_image')) {
	/*
	 * version: 'optimized', 'w200', 'thumbnail'
	 * options: array(	'max_width' => 1920, 'max_height' => 1200, 'jpeg_quality' => 90 )
	 */
	
	function create_scaled_image($file_path, $version, $options) {
		$file_name = basename($file_path);
		
		$img_ext = strtolower(substr(strrchr($file_name, '.'), 1));
		$force_jpeg = isset($options["force_jpeg"]) && $options["force_jpeg"];
		$format_change = !in_array($img_ext, array('jpg', 'jpeg'));
		$crop_picture = isset($options["center_crop"]) && $options["center_crop"];
		$expand_picture = !empty($options['min_width']) || !empty($options['min_height']);

		if (!empty($version)) {
			$version_dir = dirname($file_path) . DIRECTORY_SEPARATOR . $version;
			
			if (!is_dir($version_dir)) {
				mkdir($version_dir, 0777, true);
			}
			$new_file_path = $version_dir . '/' . $file_name;
		} else {
			$new_file_path = $file_path;
		}

		list($img_width, $img_height) = @getimagesize($file_path);

		if (!$img_width || !$img_height) {
			return false;
		}
		
		/* GET WIDTH SCALE */

		//print_r($options);

		if (!empty($options['min_width']) && $options['min_width'] > $img_width) {
			$width_scale =  $options['min_width'] / $img_width;
		} else {
			$width_scale =  $options['max_width'] / $img_width;
		}

		/* GET HEIGHT SCALE */
		
		if (!empty($options['min_height']) && $options['min_height'] > $img_height) {
			$height_scale =  $options['min_height'] / $img_height;
		} else {
			$height_scale =  $options['max_height'] / $img_height;
		}


		/* SELECT PREFERED SCALE */

		if ($crop_picture) {
			$scale = ($width_scale < $height_scale) ? $height_scale : $width_scale;
		} else {
			$scale = min($width_scale, $height_scale);
		}
		
		
		if (!$expand_picture && $scale >= 1 && !$format_change) {
			if ($file_path !== $new_file_path) {
				return copy($file_path, $new_file_path);
			}
			return true;
		}
		
		$new_width  = round($img_width * $scale);
		$new_height = round($img_height * $scale);

		$new_img = @imagecreatetruecolor($new_width, $new_height);

		switch ($img_ext) {
			case 'jpg':
			case 'jpeg':
				$src_img = @imagecreatefromjpeg($file_path);
				break;
			case 'gif':
				$src_img = @imagecreatefromgif($file_path);
				break;
			case 'png':
				$src_img = @imagecreatefrompng($file_path);
				break;
			default:
				$src_img = null;
		}

		if ($force_jpeg) {
			$img_ext = "jpg";
		}

		switch ($img_ext) {
			case 'jpg':
			case 'jpeg':
				$write_image = 'imagejpeg';
				$image_quality = isset($options['jpeg_quality']) ?
						$options['jpeg_quality'] : 75;
				break;
			case 'gif':
				@imagecolortransparent($new_img, @imagecolorallocate($new_img, 0, 0, 0));
				$write_image = 'imagegif';
				$image_quality = null;
				break;
			case 'png':
				@imagecolortransparent($new_img, @imagecolorallocate($new_img, 0, 0, 0));
				@imagealphablending($new_img, false);
				@imagesavealpha($new_img, true);
				$write_image = 'imagepng';
				$image_quality = isset($options['png_quality']) ?
						$options['png_quality'] : 9;
				break;
			default:
				$src_img = null;
		}

		$resampling_done = @imagecopyresampled(
			$new_img, $src_img, 0, 0, 0, 0, $new_width, $new_height, $img_width, $img_height
		);

		if ($resampling_done && $crop_picture) {
			$width = $options["max_width"];
			$height = $options["max_height"];

			$new_img2 = @imagecreatetruecolor($width, $height);

			$s = @imagecopyresampled(
				$new_img2, $new_img,
				0, 0,
				floor(($new_width - $width) / 2),
				floor(($new_height - $height) / 2),
				floor(($new_width - $width) / 2) + $width,
				floor(($new_height - $height) / 2) + $height,
				floor(($new_width - $width) / 2) + $width,
				floor(($new_height - $height) / 2) + $height
			);

			@imagedestroy($new_img);
			$new_img = $new_img2;
		}

		$success = $src_img && $resampling_done && $write_image($new_img, $new_file_path, $image_quality);
		// Free up memory (imagedestroy does not delete files):

		@imagedestroy($src_img);
		@imagedestroy($new_img);

		return $success;
	}
	
}

/* end of file */
