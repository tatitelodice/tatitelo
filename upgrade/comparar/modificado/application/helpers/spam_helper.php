<?php

if (!function_exists('find_website_words')) {
	
	// HAY QUE IGNORAR CUANDO TIENEN .COM Y NO SON MAILS
	
	function find_website_words($text) {
		$m_text = '#' . $text . '#';
		$m_text = preg_replace('!\r?\n|\s!', "#", $m_text);
		
		$found_words = array();
				
		if (preg_match_all('!#([^#\@]+[a-z0-9]\.(com|co|net|info|org|wix|uy)(/[^#]+)?)#!i', $m_text, $matches)) {
			foreach ($matches[1] as $m) {
				$found_words[] = $m;
			}
		}
		
		if (preg_match_all('!(?:https?://|www\.)[^#]+!i', $m_text, $matches)) {
			foreach ($matches[0] as $m) {
				$found_words[] = $m;
			}
		}
	
		$unique_found_words = array_unique($found_words);
		
		return $unique_found_words;
	}
	
}	


if (!function_exists('strip_tags_and_crap')) {
	
	function strip_tags_and_crap($text) {
		
		//	"\0"            0           Null Character
		//	"\t"            9           Tab
		//	"\n"           10           New line EXCLUDED
		//	"\x0B"         11           Vertical Tab
		//	"\r"           13           New Line in Mac EXCLUDED
		//	" "            32           Space
		
		$text = str_replace("\xC2\xA0", " ", $text);
		$text = preg_replace( '![\x00-\x09\x0B\x0C\x0E-\x1F]+!' , '' , $text);
		
		$text = strip_tags($text);
		$text = trim($text);
	
		return $text;
	}
	
}



if (!function_exists('replace_website_words')) {
	
	function replace_website_words($text, $replacement='') {
		$found_words = find_website_words($text);
		
		foreach ($found_words as $fw) {
			$text = str_replace($fw, $replacement, $text); 
		}
		
		return $text;
	}
	
}

/* end of file */
