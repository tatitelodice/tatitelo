<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Admin_Payment_Account_Service {
    
    protected $paymentAccountModel;
    protected $permissionService;
    
    public function __construct() {
        $CI = &get_instance();
        $CI->load->model('payment_account_model');
        $CI->load->library('admin/admin_permission_service');

        $this->paymentAccountModel = &$CI->payment_account_model;
        $this->permissionService = &$CI->admin_permission_service;
    }

    public function getAllowedPaymentAccounts($options=false)
    {
        $searchOptions = (is_array($options)) ? $options : [];

        $hasFullPermissions =$this->permissionService->isGranted('payment_account', null, 'full');

        if ($hasFullPermissions)
        {
            return $this->paymentAccountModel->findBy($searchOptions);
        }
        
        $accountPermissions = $this->permissionService->getPermissions('payment_account');

        $accountIds = [];

        foreach ($accountPermissions as $permission)
        {
            if ( empty($permission['resourceId']) ) continue;
            if (!in_array($permission['permission'], ['view', 'full'])) continue ;

            $accountIds[] = $permission['resourceId'];
        }

        if ( empty($accountIds) )
        {
            return [];
        }

        $searchOptions['id'] = $accountIds;
        
        $accounts = $this->paymentAccountModel->findBy($searchOptions);

        return $accounts;
    }

}

/* eof */