<?php

class Asset {
    
    private $CI;
    
    public function __construct() {
        $this->CI = &get_instance();
    }

    public function url($path, $package=false)
    {
        $base_url = $this->get_base_url($package);
        $url = rtrim($base_url, '/') . '/' . ltrim($path, '/');
        return $url;
    }


    protected function get_base_url($package)
    {
        $base_url = '';
        $asset_packages = $this->CI->config->item('asset_packages');

        if ($package && isset($asset_packages[$package]))
        {
            $package_conf = $asset_packages[$package];
            $base_url = (!empty($package_conf['base_url'])) ? $package_conf['base_url'] : $base_url;
        }

        return $base_url;
    }

}