<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of phones
 *
 * @author Santiago
 */
class Pumbatephone {
	
	protected $phones = array(
		'uy' => array(
			'default' => '093 478 107', // 097 081 692 nico
			'sms' => '093 478 107',
		)
		//'co' => '313 531-2513', // oscar
	);
	
	public function get_by_country($country, $tag=false) {
		$found = false;
		
		if (isset($this->phones[$country])) {
			
			if (is_array($this->phones[$country])) {
				if ($tag && isset($this->phones[$country][$tag])){
					$found = $this->phones[$country][$tag];
				} elseif (isset($this->phones[$country]['default'])) {
					$found = $this->phones[$country]['default'];
				}
			} else {
				$found = $this->phones[$country];
			}
		}
		
		return $found;
	}
	
}

/* end of file */
