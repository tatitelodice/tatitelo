<?php

class Human_UserAgent {
	
	public function get_human_ua($ua_string) {
		return $this->get_os($ua_string);
	}
	
	
	public function get_os($ua_string) {
		$found_os = "Propietario Desconocido";
		
		$os_list = array(
			"Windows\s*NT\s*5\.1"      => "Windows XP",
			"Windows\s*NT\s*5\.2"      => "Windows 2003",
			"Windows\s*NT\s*6\.0"      => "Windows Vista",
			"Windows\s*NT\s*6\.1"      => "Windows 7",
			"Windows\s*NT\s*6\.2"      => "Windows 8",
			"Windows\s*NT\s*6\.3"      => "Windows 8.1",
			"Windows\s*NT\s*[67]"      => "Windows 8.1",
			"Android\s*(2[0-9\.]*)"    => "Android 2",
			"Android\s*(3[0-9\.]*)"    => "Android 3",
			"Android\s*([45][0-9\.]*)" => "Android 4",
			"Android"                  => "Android",
			"Windows Phone OS\s*7"     => "Windows Phone 7",
			"Windows Phone\s*8\.0"     => "Windows Phone 8",
			"Windows Phone\s*8\.(\d+)" => "Windows Phone 8.1",
			"iPad"                     => "iOS",
			"iPhone OS\s*(\d+)"        => "iPhone",
			"Mac OS X"                 => "Mac OS X",
			"Linux"                    => "Linux",
			"BlackBerry"               => "BlackBerry",
			"Nokia"                    => "Nokia Propietario",
		);
		
		foreach ($os_list as $os_pattern => $os_name) {
			if (preg_match("!$os_pattern!i", $ua_string)) {
				$found_os = $os_name;
				break;
			}
		}
		
		return $found_os;
	}
	
}


/* end of file */