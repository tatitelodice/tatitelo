<?php

class Pumbate_Api {

	private $CI;
	private $webservice_url;
	
	
	public  $cache_file_enabled = true;
	public  $cache_file_expire = 600;
	
	public  $use_last_query = false;
	private $last_query;

	public function __construct() {
		$this->CI = &get_instance();
		$this->domain = $this->CI->config->item('remote_site_domain');
		$this->cache = array();
	}


	public function make_query($query) {
		$query_url = "http://" . $this->domain . "/webservice/" . $query;

		$result = $this->load_json_from_cache($query_url);
		
		if (!$result) {
			$result = $this->fetch_json($query_url);
		}

		if ($result) {
			if (isset($result['data'])) {
				$result = $result['data'];
			} else {
				$result = false;
			}
		}

		return $result;
	}


	public function fix_url($url) {
		$url = trim($url);
		$url = str_replace(" ", "%20", $url);
		return $url;
	}
	
	
	
	public function load_json_from_cache($url) {
		$query_cache_file = APPPATH.'cache/pumbate_api_' . md5($url) . '.json';
		
		if (file_exists($query_cache_file)) {
			$time_limit = time() - $this->cache_file_expire;
			
			if (filemtime($query_cache_file) > $time_limit) {
				$file_contents = file_get_contents($query_cache_file);
				$json_result = json_decode($file_contents, true);

				if (is_array($json_result)) {
					return $json_result;
				}
			} else {
				unlink($query_cache_file);
			}
		}
		
		return false;
	}
	

	public function fetch_json($url) {
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
		curl_setopt($ch, CURLOPT_URL, $url);

		$result = curl_exec($ch);
		$result_info = curl_getinfo($ch);

		curl_close($ch);
		
		if ($result_info['http_code'] == 200) {
			$json_result = json_decode($result, true);
			
			if (is_array($json_result)) {
				
				$query_cache_file = APPPATH.'cache/pumbate_api_' . md5($url) . '.json';
				file_put_contents($query_cache_file, $result);
				
				return $json_result;
			}
		}

		return false;
	}


	public function get_all_profiles($country=false, $gender=false, $fields=false) {
		$country_str = "";

		if ($country) {
			$country_str = $country . '/';
		}
		
		$gender_str = '';
		
		if ($gender) {
			$gender_str = $gender;
		}

		$fields_str = '';

		if ($fields && is_array($fields)) {
			$fields_str = "?full&fields=" . implode(",", $fields);
		}

		if ($this->use_last_query && $this->last_query) {
			$query_results = $this->last_query;
			$this->use_last_query = false;
		} else {
			$query_results = $this->make_query("get_all/" . $country_str . $gender_str . $fields_str);
			$this->last_query = $query_results;
		}
		
		return $query_results;
	}	

	
	public function get_profiles_by_user($country, $gender, $lite_mode=false) {
		return $this->get_profiles_by('user', $country, $gender, $lite_mode);
	}
	
	
	public function get_profiles_by($field, $country, $gender, $fields=false) {
		$table = array();

		if (!$fields) {
			$fields = array(
				"proId", "proUser", "proName", "proGender","proPhone", "proEmail", "proNumericPhones",
				"proAccountType", "proVerified", 'proModifiedDate', 'proDeleted'
			);
		}

		$profiles = $this->get_all_profiles($country, $gender, $fields);

		foreach ($profiles as $profile) {
			$table[$profile[$field]] = $profile;
		}

		return $table;
	}
	
	public function get_users_by_email($country, $gender=false) {
		$table = array();

		$fields = array("proUser", "proEmail");

		$profiles = $this->get_all_profiles($country, $gender, $fields);
		
		if ($profiles) {
			foreach ($profiles as $profile) {
				$email = $profile['email'];

				if ($email) {
					if (!isset($table[$email])) {
						$table[$email] = array();
					}
					
					$table[$email][] = $profile['user'];
				}
			}
		}
		
		return $table;
	}

	
	public function get_users_by_phone($country, $gender=false) {
		$table = array();

		$fields = array("proUser", "proNumericPhones");

		$profiles = $this->get_all_profiles($country, $gender, $fields);
		
		if ($profiles) {
			foreach ($profiles as $profile) {
				$phones = $profile['numericPhones'];

				if ($phones) {
					$phones = explode(',', $phones);

					foreach ($phones as $phone) {

						if (!isset($table[$phone])) {
							$table[$phone] = array();
						}

						$table[$phone][] = $profile['user'];
					}

				}

			}
		}
		
		return $table;
	}
		

}

/* end of file */
