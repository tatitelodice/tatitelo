<?php

class Site_Cache {

    private $CI;
    private $client;

    public function __construct() {
        $this->CI = &get_instance();
        $this->client = null;
    }

    public function get_client() {
        if ($this->client === null)
        {
            $host = getenv('REDIS_HOST') ? getenv('REDIS_HOST') : '0.0.0.0';
            $port = getenv('REDIS_PORT') ? getenv('REDIS_PORT') : 6379;

            $client = new Predis\Client([
                'scheme' => 'tcp',
                'host'   => $host,
                'port'   => $port,
                'prefix' => 'pumbate:'
            ]);
            
            $this->client = $client;
        }

        return $this->client;
    }

}


/* eof */