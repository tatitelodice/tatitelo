<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Banner_Service {
	
	protected $ci;
	protected $cache_path = APPPATH . "/cache/banner";
	
	
	public function __construct() {
		$this->ci = &get_instance();
		$this->ci->load->model('banner_model');

		if (!file_exists($this->cache_path))
		{
			@mkdir($this->cache_path, 0777, true);
		}
	}
	

	public function has_page_escort_banners($page_params)
	{
		$banner_sections = $this->ci->config->item('banner_sections');

		if (empty($banner_sections)) return false;
		if (empty($banner_sections[$page_params['country']])) return false;

		$has_match = false;
		$matchers = $banner_sections[$page_params['country']];

		foreach ($matchers as $m)
		{
			$r_match = null;

			foreach ($m as $mk => $mv)
			{
				if (!isset($page_params[$mk]))
				{
					break;
				}

				if (empty($mv))
				{
					$r_match = empty($page_params[$mk]);
				}
				else
				{
					$r_match = $page_params[$mk] === $mv;
				}

				if (!$r_match)
				{
					break;
				}
			}

			$has_match = $r_match === true;

			if ($has_match)
			{
				break;
			}
		}

		return $has_match;
	}

	public function escorts_to_banners($escorts)
	{
		$escort_banners = [];

 		foreach ($escorts as $e)
 		{
 			$pic_url = str_replace("/w200/",  "/banner-md/", $e['picture']);
 			$pic_url = str_replace('/muestra/', '/muestra/banner-md/', $pic_url);

 			$escort_banner = array(
 				'title' => $e['name']
 			,	'description' => $e['about']
 			,	'url' => $e['site']
 			,	'settings' => array(
 					'main_picture' => $pic_url
 				)
 			,	'is_escort' => true
 			,	'verified_pictures' => !empty($e['verified_pictures'])
 			);

 			$escort_banners[] = $escort_banner;
 		}

 		return $escort_banners;
	}

	
	public function get_banners($country, $category=false) {
		$banners = $this->get_banners_from_cache($country, $category);
		
		if ($banners === false) {
			$banners = $this->get_banners_from_db($country, $category);
		}

		foreach ($banners as $i => $b)
		{
			if (!empty($b['settings']['main_picture']))
			{
				$b['settings']['main_picture'] = base_url($b['settings']['main_picture']);
			}

			$banners[$i] = $b;
		}

		return $banners;
	}
	
	
	public function get_banners_from_db($country, $category=false) {
		$list_params = array(
			'where'    =>  array(
				'deleted' => 0,
				'enabled' => 1,
				'country' => $country,
			),
			'order_by' => 'country asc, position asc',
		);
		
		if ($category) {
			$list_params['where']['category'] = $category;
		}
		
		$banners = $this->ci->banner_model->get_all_banner($list_params);
		return $banners;
	}
	
	
	public function get_cache_file($country, $category=false) {
		$cache_file = $this->cache_path . '/' . $country;
		
		if ($category) {
			$cache_file .= '_' . $category;
 		}
		
		$cache_file .= '.php';
		
		return $cache_file;
	}
	
	public function get_banners_from_cache($country, $category=false) {
		$filename = $this->get_cache_file($country, $category);
		
		if (!is_file($filename)) {
			return false;
		}
		
		return include $filename;
	}
	
	public function save_banners_into_cache($country, $category=false) {
		$list_data = $this->get_banners_from_db($country, $category);
		
		$code_raw    = var_export($list_data, true);
		$code_string = sprintf('<?php return %s; /* eof */', $code_raw);
		
		$cache_file = $this->get_cache_file($country, $category);
		
		$success = file_put_contents($cache_file, $code_string) !== false;
		
		if ($success) { chmod($cache_file, 0777); }
		
		return $success;
	}
	
}

/* end of file */